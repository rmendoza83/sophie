rmdir clases\backup /S /Q
rmdir datamodule\backup /S /Q
rmdir forms\backup /S /Q
rmdir units\backup /S /Q
rmdir backup /S /Q
rmdir lib /S /Q
del *.~*
del clases\*.~*
del units\*.~*
del datamodule\*.~*
del forms\*.~*
del *.bdsproj*
del *.cfg
del *.identcache
del *.dsk
del *.*.local
del *.stat
rmdir __history /S /Q
rmdir clases\__history /S /Q
rmdir datamodule\__history /S /Q
rmdir forms\__history /S /Q
rmdir units\__history /S /Q
rmdir __recovery /S /Q
rmdir clases\__recovery /S /Q
rmdir datamodule\__recovery /S /Q
rmdir forms\__recovery /S /Q
rmdir units\__recovery /S /Q
del *.ddp
del clases\*.ddp
del units\*.ddp
del datamodule\*.ddp
del forms\*.ddp
del *.dcu
del clases\*.dcu
del units\*.dcu
del datamodule\*.dcu
del forms\*.dcu
del *.bak
del clases\*.bak 
del units\*.bak
del datamodule\*.bak
del forms\*.bak
del *.lrs
del clases\*.lrs
del units\*.lrs
del datamodule\*.lrs
del forms\*.lrs
del *.o
del clases\*.o 
del units\*.o
del datamodule\*.o
del forms\*.o
del *.ppu
del clases\*.ppu
del units\*.ppu
del datamodule\*.ppu
del forms\*.ppu
del *.a
del clases\*.a
del units\*.a
del datamodule\*.a
del forms\*.a
del *.compiled
del *.or
call tools\Clear.bat
pause
