unit ClsRetencionIva;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TRetencionIva }

  TRetencionIva = class(TGenericoBD)
    private
      Numero: string;
      CodigoProveedor: string;
      CodigoZona: string;
      FechaIngreso: TDate;
      FechaContable: TDate;
      Total: Double;
      NumeroComprobante: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArraystring; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetNumero(varNumero: string);
      procedure SetCodigoProveedor(varCodigoProveedor: string);
      procedure SetCodigoZona(varCodigoZona: string);
      procedure SetFechaIngreso(varFechaIngreso: TDate);
      procedure SetFechaContable(varFechaContable: TDate);
      procedure SetTotal(varTotal: Double);
      procedure SetNumeroComprobante(varNumeroComprobante: string);
      function GetNumero: string;
      function GetCodigoProveedor: string;
      function GetCodigoZona: string;
      function GetFechaIngreso: TDate;
      function GetFechaContable: TDate;
      function GetTotal: Double;
      function GetNumeroComprobante: string;
      function BuscarNumero: Boolean;
  end;

implementation

uses
  DB,
  ClsConsecutivo;

{ TRetencionIva }

procedure TRetencionIva.Ajustar;
begin

end;

procedure TRetencionIva.Mover(Ds: TClientDataSet);
begin
  Numero := Ds.FieldValues['numero'];
  CodigoProveedor := Ds.FieldValues['codigo_proveedor'];
  CodigoZona := Ds.FieldValues['codigo_zona'];
  FechaIngreso := Ds.FieldValues['fecha_ingreso'];
  FechaContable := Ds.FieldValues['fecha_contable'];
  Total := Ds.FieldValues['total'];
  NumeroComprobante := Ds.FieldValues['numero_comprobante']
end;

function TRetencionIva.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,7);
  Params[0] := Numero;;
  Params[1] := CodigoProveedor;
  Params[2] := CodigoZona;
  Params[3] := VarFromDateTime(FechaIngreso);
  Params[4] := VarFromDateTime(FechaContable);
  Params[5] := Total;
  Params[6] := NumeroComprobante;
  Result := Params;
end;

function TRetencionIva.GetCampos: TArraystring;
var
  Campos: TArraystring;
begin
  SetLength(Campos,4);
  Campos[0] := 'numero';
  Campos[1] := 'codigo_proveedor';
  Campos[2] := 'codigo_zona';
  Campos[3] := 'numero_comprobante';
  Result := Campos;
end;

function TRetencionIva.GetCondicion: string;
begin
  Result:= '"numero" = ' + QuotedStr(Numero);
end;

procedure TRetencionIva.AntesInsertar;
var
  Consecutivo: TConsecutivo;
begin
  Consecutivo := TConsecutivo.Create;
  Numero := IntToStr(Consecutivo.ObtenerConsecutivo('numero_retencion_iva'));
  NumeroComprobante := FormatDateTime('YYYY-mm-',FechaContable) + FormatFloat('00000000',Consecutivo.ObtenerConsecutivo('numero_comprobante_retencion_iva'));
end;

procedure TRetencionIva.AntesModificar;
begin

end;

procedure TRetencionIva.AntesEliminar;
begin

end;

procedure TRetencionIva.DespuesInsertar;
begin

end;

procedure TRetencionIva.DespuesModificar;
begin

end;

procedure TRetencionIva.DespuesEliminar;
begin

end;

constructor TRetencionIva.Create;
var
  ClavesPrimarias: TArraystring;
begin
  inherited Create;
  SetTabla('tbl_retenciones_iva');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(7)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "codigo_proveedor"=$2, "codigo_zona"=$3, "fecha_ingreso"=$4, "fecha_contable"=$5, "total"=$6, "numero_comprobante"=$7 ' +
                  ' WHERE ("numero"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("numero"=$1)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"numero"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TRetencionIva.SetNumero(varNumero: string);
begin
  Numero := varNumero;
end;

procedure TRetencionIva.SetNumeroComprobante(varNumeroComprobante: string);
begin
  NumeroComprobante := varNumeroComprobante;
end;

procedure TRetencionIva.SetCodigoProveedor(varCodigoProveedor: string);
begin
  CodigoProveedor := varCodigoProveedor;
end;

procedure TRetencionIva.SetCodigoZona(varCodigoZona: string);
begin
  CodigoZona := varCodigoZona;
end;

procedure TRetencionIva.SetFechaIngreso(varFechaIngreso: TDate);
begin
  FechaIngreso := varFechaIngreso;
end;

procedure TRetencionIva.SetFechaContable(varFechaContable: TDate);
begin
  FechaContable := varFechaContable;
end;

procedure TRetencionIva.SetTotal(varTotal: Double);
begin
  Total := varTotal;
end;

function TRetencionIva.GetNumero: string;
begin
  Result := Numero;
end;

function TRetencionIva.GetNumeroComprobante: string;
begin
  Result := NumeroComprobante;
end;

function TRetencionIva.GetCodigoProveedor: string;
begin
  Result := CodigoProveedor;
end;

function TRetencionIva.GetCodigoZona: string;
begin
  Result := CodigoZona;
end;

function TRetencionIva.GetFechaIngreso: TDate;
begin
  Result := FechaIngreso;
end;

function TRetencionIva.GetFechaContable: TDate;
begin
  Result := FechaContable;
end;

function TRetencionIva.GetTotal: Double;
begin
  Result := Total;
end;

function TRetencionIva.BuscarNumero: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("numero" = ' + QuotedStr(GetNumero) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

end.
