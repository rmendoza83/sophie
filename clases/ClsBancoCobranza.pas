unit ClsBancoCobranza;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TBancoCobranza }

  TBancoCobranza = class(TGenericoBD)
    private
      Codigo: string;
      Descripcion: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigo(varCodigo: string);
      procedure SetDescripcion(varDescripcion: string);
      function GetCodigo: string;
      function GetDescripcion: string;
      function ObtenerCombo: TClientDataSet;
  end;

var
  BancoCobranza: TBancoCobranza;

implementation

{ TBancoCobranza }

procedure TBancoCobranza.Ajustar;
begin

end;

procedure TBancoCobranza.Mover(Ds: TClientDataSet);
begin
  Codigo := Ds.FieldValues['codigo'];
  Descripcion := Ds.FieldValues['descripcion'];
end;

function TBancoCobranza.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,2);
  Params[0] := Codigo;
  Params[1] := Descripcion;
  Result := Params;
end;

function TBancoCobranza.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,2);
  Campos[0] := 'codigo';
  Campos[1] := 'descripcion';
  Result := Campos;
end;

function TBancoCobranza.GetCondicion: string;
begin
  Result := '"codigo" = ' + QuotedStr(Codigo);
end;

procedure TBancoCobranza.AntesEliminar;
begin

end;

procedure TBancoCobranza.AntesInsertar;
begin

end;

procedure TBancoCobranza.AntesModificar;
begin

end;

procedure TBancoCobranza.DespuesEliminar;
begin

end;

procedure TBancoCobranza.DespuesInsertar;
begin

end;

procedure TBancoCobranza.DespuesModificar;
begin

end;

constructor TBancoCobranza.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_bancos_cobranzas');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(2)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "descripcion"=$2,WHERE ("codigo"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("codigo"=$1)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"codigo"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TBancoCobranza.SetCodigo(varCodigo: string);
begin
  Codigo := varCodigo;
end;

procedure TBancoCobranza.SetDescripcion(varDescripcion: string);
begin
  Descripcion := varDescripcion;
end;

function TBancoCobranza.GetCodigo: string;
begin
  Result := Codigo;
end;

function TBancoCobranza.GetDescripcion: string;
begin
  Result := Descripcion;
end;

function TBancoCobranza.ObtenerCombo: TClientDataSet;
begin
  Result := BD.EjecutarQueryCliDs('SELECT "codigo", "descripcion" FROM ' + GetTabla);
end;

end.
