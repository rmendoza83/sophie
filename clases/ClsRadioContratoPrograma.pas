unit ClsRadioContratoPrograma;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TRadioContratoPrograma}

  TRadioContratoPrograma = class(TGenericoBD)
    private
      IdRadioContrato: Integer;
      CodigoPrograma: string;
      CodigoEspacio: string;
      CunasDiarias: Integer;
      TotalCunas: Integer;
      Duracion: Integer;
      MontoAgencia: Double;
      Porcentaje: Double;
      MontoEmisora: Double;
      Comision: Double;
      MontoReal: Double;
      Tarifa: Double;
      CostoBruto: Double;
      PDescuento: Double;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetIdRadioContrato(varIdRadioContrato: Integer);
      procedure SetCodigoPrograma(varCodigoPrograma:string);
      procedure SetCodigoEspacio(varCodigoEspacio:string);
      procedure SetCunasDiarias(varCunasDiarias:Integer);
      procedure SetTotalCunas(varTotalCunas:Integer);
      procedure SetDuracion(varDuracion:Integer);
      procedure SetMontoAgencia(varMontoAgencia:Double);
      procedure SetPorcentaje(varPorcentaje:Double);
      procedure SetMontoEmisora(varMontoEmisora:Double);
      procedure SetComision(varComision:Double);
      procedure SetMontoReal(varMontoReal:Double);
      procedure SetTarifa(varTarifa:Double);
      procedure SetCostoBruto(varCostoBruto:Double);
      procedure SetPDescuento(varPDescuento: Double);
      function GetIdRadioContrato: Integer;
      function GetCodigoPrograma: string;
      function GetCodigoEspacio: string;
      function GetCunasDiarias: Integer;
      function GetTotalCunas: Integer;
      function GetDuracion: Integer;
      function GetMontoAgencia: Double;
      function GetPocentaje: Double;
      function GetMontoEmisora: Double;
      function GetComision: Double;
      function GetMontoReal: Double;
      function GetTarifa: Double;
      function GetCostoBruto: Double;
      function GetPDescuento: Double;
      function ObtenerIdRadioContrato(varIdRadioContrato: Integer): TClientDataSet;
      function EliminarIdRadioContrato(varIdRadioContrato: Integer): Boolean;
      function BuscarIdRadioContrato: Boolean;
  end;

implementation

{ TRadioContratoPrograma }

procedure TRadioContratoPrograma.Ajustar;
begin

end;

procedure TRadioContratoPrograma.Mover(Ds: TClientDataSet);
begin
  IdRadioContrato := Ds.FieldValues['id_radio_contrato'];
  CodigoPrograma := Ds.FieldValues['codigo_programa'];
  CodigoEspacio := Ds.FieldValues['codigo_espacio'];
  CunasDiarias := Ds.FieldValues['cunas_diarias'];
  TotalCunas := Ds.FieldValues['total_cunas'];
  Duracion := Ds.FieldValues['duracion'];
  MontoAgencia := Ds.FieldValues['monto_agencia'];
  Porcentaje := Ds.FieldValues['porcentaje'];
  MontoEmisora := Ds.FieldValues['monto_emisora'];
  Comision := Ds.FieldValues['comision'];
  MontoReal := Ds.FieldValues['monto_real'];
  Tarifa := Ds.FieldValues['tarifa'];
  CostoBruto := Ds.FieldValues['costo_bruto'];
  PDescuento := Ds.FieldValues['p_descuento'];
end;

function TRadioContratoPrograma.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,14);
  Params[0] := IdRadioContrato;
  Params[1] := CodigoPrograma;
  Params[2] := CodigoEspacio;
  Params[3] := CunasDiarias;
  Params[4] := TotalCunas;
  Params[5] := Duracion;
  Params[6] := MontoAgencia;
  Params[7] := Porcentaje;
  Params[8] := MontoEmisora;
  Params[9] := Comision;
  Params[10] := MontoReal;
  Params[11] := Tarifa;
  Params[12] := CostoBruto;
  Params[13] := PDescuento;
  Result := Params;
end;

function TRadioContratoPrograma.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,2);
  Campos[0] := 'codigo_programa';
  Campos[1] := 'codigo_espacio';
  Result := Campos;
end;

function TRadioContratoPrograma.GetCondicion: string;
begin
  Result := '("id_radio_contrato" = ' + IntToStr(IdRadioContrato) + ') AND ' +
            '("codigo_programa" = ' + QuotedStr(CodigoPrograma) + ') AND ' +
            '("codigo_espacio" = ' + QuotedStr(CodigoEspacio) +  ')';
end;

procedure TRadioContratoPrograma.AntesInsertar;
begin

end;

procedure TRadioContratoPrograma.AntesModificar;
begin

end;

procedure TRadioContratoPrograma.AntesEliminar;
begin

end;

procedure TRadioContratoPrograma.DespuesInsertar;
begin

end;

procedure TRadioContratoPrograma.DespuesModificar;
begin

end;

procedure TRadioContratoPrograma.DespuesEliminar;
begin

end;

constructor TRadioContratoPrograma.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_radio_contratos_programas');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(14)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "cunas_diarias"=$4, "total_cunas"=$5, "duracion"=$6, "monto_agencia"=$7, "porcentaje"=$8, "monto_emisora"=$9, "comision"=$10, "monto_real"=$11, ' +
                  '"tarifa"=$12, "costo_bruto"=$13, "p_descuento"=$14, WHERE ("id_radio_contrato"=$1) AND ("codigo_programa"=$2) AND ("codigo_espacio"=$3)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("id_radio_contrato"=$1) AND ("codigo_programa"=$2) AND ("codigo_espacio"=$3)');
  SetLength(ClavesPrimarias,3);
  ClavesPrimarias[0] := '"id_radio_contrato"';
  ClavesPrimarias[1] := '"codigo_programa"';
  ClavesPrimarias[2] := '"codigo_espacio"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;


procedure TRadioContratoPrograma.SetCodigoEspacio(varCodigoEspacio: string);
begin
  CodigoEspacio := varCodigoEspacio;
end;

procedure TRadioContratoPrograma.SetCodigoPrograma(varCodigoPrograma: string);
begin
  CodigoPrograma := varCodigoPrograma;
end;

procedure TRadioContratoPrograma.SetComision(varComision: Double);
begin
  Comision := varComision;
end;

procedure TRadioContratoPrograma.SetCostoBruto(varCostoBruto: Double);
begin
  CostoBruto := varCostoBruto;
end;

procedure TRadioContratoPrograma.SetCunasDiarias(varCunasDiarias: Integer);
begin
  CunasDiarias := varCunasDiarias;
end;

procedure TRadioContratoPrograma.SetDuracion(varDuracion: Integer);
begin
  Duracion := varDuracion;
end;

procedure TRadioContratoPrograma.SetIdRadioContrato(
  varIdRadioContrato: Integer);
begin
  IdRadioContrato := varIdRadioContrato;
end;

procedure TRadioContratoPrograma.SetMontoAgencia(varMontoAgencia: Double);
begin
  MontoAgencia := varMontoAgencia;
end;

procedure TRadioContratoPrograma.SetMontoEmisora(varMontoEmisora: Double);
begin
  MontoEmisora := varMontoEmisora;
end;

procedure TRadioContratoPrograma.SetMontoReal(varMontoReal: Double);
begin
  MontoReal := varMontoReal;
end;

procedure TRadioContratoPrograma.SetPDescuento(varPDescuento: Double);
begin
  PDescuento := varpDescuento;
end;

procedure TRadioContratoPrograma.SetPorcentaje(varPorcentaje: Double);
begin
  Porcentaje := varPorcentaje;
end;

procedure TRadioContratoPrograma.SetTarifa(varTarifa: Double);
begin
  Tarifa := varTarifa;
end;

procedure TRadioContratoPrograma.SetTotalCunas(varTotalCunas: Integer);
begin
  TotalCunas := varTotalCunas;
end;

function TRadioContratoPrograma.GetPDescuento: Double;
begin
  Result := PDescuento;
end;

function TRadioContratoPrograma.GetPocentaje: Double;
begin
  Result := Porcentaje;
end;

function TRadioContratoPrograma.GetTarifa: Double;
begin
  Result := Tarifa;
end;

function TRadioContratoPrograma.GetTotalCunas: Integer;
begin
  Result := TotalCunas;
end;

function TRadioContratoPrograma.GetCodigoEspacio: string;
begin
  Result := CodigoEspacio;
end;

function TRadioContratoPrograma.GetCodigoPrograma: string;
begin
  Result := CodigoPrograma;
end;

function TRadioContratoPrograma.GetComision: Double;
begin
  Result := Comision;
end;

function TRadioContratoPrograma.GetCostoBruto: Double;
begin
  Result := CostoBruto;
end;

function TRadioContratoPrograma.GetCunasDiarias: Integer;
begin
  Result := CunasDiarias;
end;

function TRadioContratoPrograma.GetDuracion: Integer;
begin
  Result := Duracion;
end;

function TRadioContratoPrograma.GetIdRadioContrato: Integer;
begin
  Result := IdRadioContrato;
end;

function TRadioContratoPrograma.GetMontoAgencia: Double;
begin
  Result := MontoAgencia;
end;

function TRadioContratoPrograma.GetMontoEmisora: Double;
begin
  Result := MontoEmisora;
end;

function TRadioContratoPrograma.GetMontoReal: Double;
begin
  Result := MontoReal;
end;

function TRadioContratoPrograma.ObtenerIdRadioContrato(varIdRadioContrato: Integer): TClientDataSet;
begin
  Result := ObtenerListaCondicionSQL(' WHERE "id_radio_contrato" = ' + IntToStr(varIdRadioContrato));
end;

function TRadioContratoPrograma.EliminarIdRadioContrato(varIdRadioContrato: Integer): Boolean;
begin
  Result := EjecutarSQL('DELETE FROM ' + GetTabla + ' WHERE "id_radio_contrato" = ' + IntToStr(varIdRadioContrato));
end;

function TRadioContratoPrograma.BuscarIdRadioContrato: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("id_radio_contrato" = ' + IntToStr(GetIdRadioContrato) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

end.
