unit ClsProductoPrecio;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;
type

  { TTProductoPrecio }

  TProductoPrecio = class(TGenericoBD)
    private
      CodigoProducto: string;
      CodigoTipoPrecioProducto: string;
      Descripcion: string;
      Precio: Double;
     protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigoProducto(varCodigoProducto: string);
      procedure SetCodigoTipoPrecioProducto(varCodigoTipoPrecioProducto: string);
      procedure SetDescripcion(varDescripcion: string);
      Procedure SetPrecio(varPrecio: Double);
      function GetCodigoProducto: string;
      function GetCodigoTipoPrecioProducto: string;
      function GetDescripcion: string;
      function GetPrecio: Double;
      function ObtenerCodigoProducto(varCodigoProducto: string): TClientDataSet;
      function EliminarCodigoProducto(varCodigoProducto: string): Boolean;
      function EliminarCodigoTipoPrecioProducto(varCodigoTipoPrecioProducto: string): Boolean;
  end;

implementation


{ TTipoPrecioProducto }

procedure TProductoPrecio.Ajustar;
begin

end;

procedure TProductoPrecio.Mover(Ds: TClientDataSet);
begin
  CodigoProducto := Ds.FieldValues['codigo_producto'];
  CodigoTipoPrecioProducto := Ds.FieldValues['codigo_tipo_precio_producto'];
  Descripcion := Ds.FieldValues['descripcion'];
  Precio := Ds.FieldValues['precio'];
end;

function TProductoPrecio.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,4);
  Params[0] := CodigoProducto;
  Params[1] := CodigoTipoPrecioProducto;
  Params[2] := Descripcion;
  Params[3] := Precio;
  Result := Params;
end;

function TProductoPrecio.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,3);
  Campos[0] := 'codigo_producto';
  Campos[1] := 'codigo_tipo_precio_producto';
  Campos[2] := 'descripcion';
  Result := Campos;
end;

function TProductoPrecio.GetCondicion: string;
begin
  Result := '"codigo_producto" = ' + QuotedStr(CodigoProducto) + ' AND ' +
            '"codigo_tipo_precio_producto" = ' + QuotedStr(CodigoTipoPrecioProducto);
end;

procedure TProductoPrecio.AntesInsertar;
begin

end;

procedure TProductoPrecio.AntesModificar;
begin

end;

procedure TProductoPrecio.AntesEliminar;
begin

end;

procedure TProductoPrecio.DespuesInsertar;
begin

end;

procedure TProductoPrecio.DespuesModificar;
begin

end;

procedure TProductoPrecio.DespuesEliminar;
begin

end;

constructor TProductoPrecio.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_productos_precios');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(4)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "descripcion"=$3, "precio"=$4 WHERE ("codigo_producto"=$1, "codigo_tipo_precio_producto"=$2 )');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("codigo_producto"=$1, "codigo_tipo_precio_producto"=$2 )');
  SetLength(ClavesPrimarias,2);
  ClavesPrimarias[0] := '"codigo_producto"';
  ClavesPrimarias[1] := '"codigo_tipo_precio_producto"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TProductoPrecio.SetCodigoProducto(varCodigoProducto: string);
begin
  CodigoProducto := varCodigoProducto;
end;

procedure TProductoPrecio.SetCodigoTipoPrecioProducto(varCodigoTipoPrecioProducto: string);
begin
  CodigoTipoPrecioProducto := varCodigoTipoPrecioProducto;
end;

procedure TProductoPrecio.SetDescripcion(varDescripcion: string);
begin
  Descripcion := varDescripcion;
end;

procedure TProductoPrecio.SetPrecio(varPrecio: Double);
begin
  Precio := varPrecio;
end;

function TProductoPrecio.GetCodigoProducto: string;
begin
  Result := CodigoProducto;
end;

function TProductoPrecio.GetCodigoTipoPrecioProducto: string;
begin
  Result := CodigoTipoPrecioProducto;
end;

function TProductoPrecio.GetDescripcion: string;
begin
  Result := Descripcion;
end;

function TProductoPrecio.GetPrecio: Double;
begin
  Result := Precio;
end;

function TProductoPrecio.ObtenerCodigoProducto(varCodigoProducto : string): TClientDataSet;
begin
  Result := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("codigo_producto" = ' + QuotedStr(varCodigoProducto) + ') ORDER BY "codigo_producto", "codigo_tipo_precio_producto"');
end;

function TProductoPrecio.EliminarCodigoProducto(
  varCodigoProducto: string): Boolean;
begin
  Result := EjecutarSQL('DELETE FROM ' + GetTabla + ' WHERE ("codigo_producto" = ' + QuotedStr(varCodigoProducto) + ')');
end;

function TProductoPrecio.EliminarCodigoTipoPrecioProducto(
  varCodigoTipoPrecioProducto: string): Boolean;
begin
  Result := EjecutarSQL('DELETE FROM ' + GetTabla + ' WHERE ("codigo_tipo_precio_producto" = ' + QuotedStr(varCodigoTipoPrecioProducto) + ')');
end;

end.
