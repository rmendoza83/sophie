unit ClsNotaCreditoCliente;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TNotaCreditoCliente }

  TNotaCreditoCliente = class(TGenericoBD)
    private
      Numero: string;
      NumeroFacturaAsociada: string;
      CodigoMoneda: string;
      CodigoCliente: string;
      CodigoVendedor: string;
      CodigoZona: string;
      CodigoEstacion: string;
      FechaIngreso: TDate;
      FechaRecepcion: TDate;
      FechaContable: TDate;
      FechaLibros: TDate;
      Concepto: string;
      Observaciones: string;
      MontoNeto: Double;
      MontoDescuento: Double;
      SubTotal: Double;
      Impuesto: Double;
      Total: Double;
      PDescuento: Double;
      PIva: Double;
      TasaDolar: Double;
      FormaLibre: Boolean;
      LoginUsuario: string;
      TiempoIngreso: TDateTime;
      IdNotaCreditoCliente: Integer;
      IdCobranza: Integer;
      IdMovimientoInventario: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArraystring; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetNumero(varNumero: string);
      procedure SetNumeroFacturaAsociada(varNumeroFacturaAsociada: string);
      procedure SetCodigoMoneda(varCodigoMoneda: string);
      procedure SetCodigoCliente(varCodigoCliente: string);
      procedure SetCodigoVendedor(varCodigoVendedor: string);
      procedure SetCodigoZona(varCodigoZona: string);
      procedure SetCodigoEstacion(varCodigoEstacion: string);
      procedure SetFechaIngreso(varFechaIngreso: TDate);
      procedure SetFechaRecepcion(varFechaRecepcion: TDate);
      procedure SetFechaContable(varFechaContable: TDate);
      procedure SetFechaLibros(varFechalibros: TDate);
      procedure SetConcepto(varConcepto: string);
      procedure SetObservaciones(varObservaciones: string);
      procedure SetMontoNeto(varMontoNeto: Double);
      procedure SetMontoDescuento(varMontoDescuento: Double);
      procedure SetSubTotal(varSubTotal: Double);
      procedure SetImpuesto(varImpuesto: Double);
      procedure SetTotal(varTotal: Double);
      procedure SetPDescuento(varPDescuento: Double);
      procedure SetPIva(varPIva: Double);
      procedure SetTasaDolar(varTasaDolar: Double);
      procedure SetFormaLibre(varFormaLibre: Boolean);
      procedure SetLoginUsuario(varLoginUsuario: string);
      procedure SetTiempoIngreso(varTiempodeIngreso: TDateTime);
      procedure SetIdNotaCreditoCliente(varIdNotaCreditoCliente: Integer);
      procedure SetIdCobranza(varIdCobranza: Integer);
      procedure SetIdMovimientoInventario(varIdMovimientoInventario: Integer);
      function GetNumero: string;
      function GetNumeroFacturaAsociada: string;
      function GetCodigoMoneda: string;
      function GetCodigoCliente: string;
      function GetCodigoVendedor: string;
      function GetCodigoZona: string;
      function GetCodigoEstacion: string;
      function GetFechaIngreso: TDate;
      function GetFechaRecepcion: TDate;
      function GetFechaContable: TDate;
      function GetFechaLibros: TDate;
      function GetConcepto: string;
      function GetObservaciones: string;
      function GetMontoNeto: Double;
      function GetMontoDescuento: Double;
      function GetSubTotal: Double;
      function GetImpuesto: Double;
      function GetTotal: Double;
      function GetPDescuento: Double;
      function GetPIva: Double;
      function GetTasaDolar: Double;
      function GetFormaLibre: Boolean;
      function GetLoginUsuario: string;
      function GetTiempoIngreso: TDateTime;
      function GetIdNotaCreditoCliente: Integer;
      function GetIdCobranza: Integer;
      function GetIdMovimientoInventario: Integer;
      function BuscarNumero: Boolean;
      function BuscarNumeroFacturaAsociada: Boolean;
      function BuscarCodigoCliente: Boolean;
      function BuscarCodigoVendedor: Boolean;
  end;

implementation

uses
  DB,
  ClsConsecutivo;

{ TNotaCreditoCliente }

procedure TNotaCreditoCliente.Ajustar;
begin

end;

procedure TNotaCreditoCliente.Mover(Ds: TClientDataSet);
begin
  Numero:= Ds.FieldValues['numero'];
  NumeroFacturaAsociada:= Ds.FieldValues['numero_factura_asociada'];
  CodigoMoneda:= Ds.FieldValues['codigo_moneda'];
  CodigoCliente:= Ds.FieldValues['codigo_cliente'];
  CodigoVendedor:= Ds.FieldValues['codigo_vendedor'];
  CodigoZona:= Ds.FieldValues['codigo_zona'];
  CodigoEstacion:= Ds.FieldValues['codigo_estacion'];
  FechaIngreso:= Ds.FieldByName('fecha_ingreso').AsDateTime;
  FechaRecepcion:= Ds.FieldByName('fecha_recepcion').AsDateTime;
  FechaContable:= Ds.FieldByName('fecha_contable').AsDateTime;
  FechaLibros:=  Ds.FieldByName('fecha_libros').AsDateTime;
  Concepto:= Ds.FieldValues['concepto'];
  Observaciones:= Ds.FieldValues['observaciones'];
  MontoNeto:= Ds.FieldValues['monto_neto'];
  MontoDescuento:= Ds.FieldValues['monto_descuento'];
  SubTotal:= Ds.FieldValues['sub_total'];
  Impuesto:= Ds.FieldValues['impuesto'];
  Total:= Ds.FieldValues['total'];
  PDescuento:= Ds.FieldValues['p_descuento'];
  PIva:= Ds.FieldValues['p_iva'];
  TasaDolar:= Ds.FieldValues['tasa_dolar'];
  FormaLibre:= Ds.FieldValues['forma_libre'];
  LoginUsuario:= Ds.FieldValues['login_usuario'];
  TiempoIngreso:= Ds.FieldByName('tiempo_ingreso').AsDateTime;
  IdNotaCreditoCliente:= Ds.FieldValues['id_nota_credito_cliente'];
  IdCobranza:= Ds.FieldValues['id_cobranza'];
  IdMovimientoInventario:= Ds.FieldValues['id_movimiento_inventario'];
end;

function TNotaCreditoCliente.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,27);
  Params[0] := Numero;
  Params[1] := NumeroFacturaAsociada;
  Params[2] := CodigoMoneda;
  Params[3] := CodigoCliente;
  Params[4] := CodigoVendedor;
  Params[5] := CodigoZona;
  Params[6] := CodigoEstacion;
  Params[7] := VarFromDateTime(FechaIngreso);
  Params[8] := VarFromDateTime(FechaRecepcion);
  Params[9] := VarFromDateTime(FechaContable);
  Params[10] := VarFromDateTime(FechaLibros);
  Params[11] := Concepto;
  Params[12] := Observaciones;
  Params[13] := MontoNeto;
  Params[14] := MontoDescuento;
  Params[15] := SubTotal;
  Params[16] := Impuesto;
  Params[17] := Total;
  Params[18] := PDescuento;
  Params[19] := PIva;
  Params[20] := TasaDolar;
  Params[21] := FormaLibre;
  Params[22] := LoginUsuario;
  Params[23] := VarFromDateTime(TiempoIngreso);
  Params[24] := IdNotaCreditoCliente;
  Params[25] := IdCobranza;
  Params[26] := IdMovimientoInventario;
  Result := Params;
end;

function TNotaCreditoCliente.GetCampos: TArraystring;
var
  Campos: TArraystring;
begin
  SetLength(Campos,10);
  Campos[0] := 'numero';
  Campos[1] := 'numero_factura_asociada';
  Campos[2] := 'codigo_moneda';
  Campos[3] := 'codigo_cliente';
  Campos[4] := 'codigo_vendedor';
  Campos[5] := 'codigo_zona';
  Campos[6] := 'codigo_estacion';
  Campos[7] := 'concepto';
  Campos[8] := 'observaciones';
  Campos[9] := 'login_usuario';
  Result := Campos;
end;

function TNotaCreditoCliente.GetCondicion: string;
begin
  Result:= '"id_nota_credito_cliente" =' + IntToStr(IdNotaCreditoCliente);
end;

procedure TNotaCreditoCliente.AntesInsertar;
var
  Consecutivo: TConsecutivo;
begin
  Consecutivo := TConsecutivo.Create;
  Numero := IntToStr(Consecutivo.ObtenerConsecutivo('numero_nota_credito_cliente'));
  IdNotaCreditoCliente := Consecutivo.ObtenerConsecutivo('id_nota_credito_cliente');
  if (not FormaLibre) then
  begin
    IdMovimientoInventario := Consecutivo.ObtenerConsecutivo('id_movimiento_inventario');
  end;
  IdCobranza := Consecutivo.ObtenerConsecutivo('id_cobranza');
end;

procedure TNotaCreditoCliente.AntesModificar;
begin

end;

procedure TNotaCreditoCliente.AntesEliminar;
begin

end;

procedure TNotaCreditoCliente.DespuesInsertar;
begin

end;

procedure TNotaCreditoCliente.DespuesModificar;
begin

end;

procedure TNotaCreditoCliente.DespuesEliminar;
begin

end;

constructor TNotaCreditoCliente.Create;
var
  ClavesPrimarias: TArraystring;
begin
  inherited Create;
  SetTabla('tbl_nota_credito_clientes');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(27)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "numero"=$1, "numero_factura_asociada"=$2, "codigo_moneda"=$3, "codigo_cliente"=$4, "codigo_vendedor"=$5, "codigo_zona"=$6, "codigo_estacion"=$7, "fecha_ingreso"=$8, ' +
                  '"fecha_recepcion"=$9, "fecha_contable"=$10, "fecha_libros"=$11, "concepto"=$12, "observaciones"=$13, "monto_neto"=$14, "monto_descuento"=$15, "sub_total"=$16, "impuesto"=$17, "total"=$18, "p_descuento"=$19, ' +
                  '"p_iva"=$20, "tasa_dolar"=$21, "forma_libre"=$22, "login_usuario"=$23, "tiempo_ingreso"=$24, "id_cobranza"=$26, "id_movimiento_inventario"=$27 ' +
                  ' WHERE ("id_nota_credito_cliente"=$25)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("id_nota_credito_cliente"=$25)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"id_nota_credito_cliente"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TNotaCreditoCliente.SetCodigoCliente(varCodigoCliente: string);
begin
  CodigoCliente := varCodigoCliente;
end;

procedure TNotaCreditoCliente.SetCodigoEstacion(varCodigoEstacion: string);
begin
  CodigoEstacion := varCodigoEstacion;
end;

procedure TNotaCreditoCliente.SetCodigoMoneda(varCodigoMoneda: string);
begin
  CodigoMoneda := varCodigoMoneda;
end;

procedure TNotaCreditoCliente.SetCodigoVendedor(varCodigoVendedor: string);
begin
  CodigoVendedor := varCodigoVendedor;
end;

procedure TNotaCreditoCliente.SetCodigoZona(varCodigoZona: string);
begin
  CodigoZona := varCodigoZona;
end;

procedure TNotaCreditoCliente.SetConcepto(varConcepto: string);
begin
  Concepto := varConcepto;
end;

procedure TNotaCreditoCliente.SetFechaContable(varFechaContable: TDate);
begin
  FechaContable := varFechaContable;
end;

procedure TNotaCreditoCliente.SetFechaIngreso(varFechaIngreso: TDate);
begin
  FechaIngreso := varFechaIngreso;
end;

procedure TNotaCreditoCliente.SetFechaLibros(varFechalibros: TDate);
begin
  FechaLibros := varFechalibros;
end;

procedure TNotaCreditoCliente.SetFechaRecepcion(varFechaRecepcion: TDate);
begin
  FechaRecepcion := varFechaRecepcion;
end;

procedure TNotaCreditoCliente.SetFormaLibre(varFormaLibre: Boolean);
begin
  FormaLibre := varFormaLibre;
end;

procedure TNotaCreditoCliente.SetIdCobranza(varIdCobranza: Integer);
begin
  IdCobranza := varIdCobranza;
end;

procedure TNotaCreditoCliente.SetIdMovimientoInventario(varIdMovimientoInventario: Integer);
begin
  IdMovimientoInventario := varIdMovimientoInventario;
end;

procedure TNotaCreditoCliente.SetIdNotaCreditoCliente(varIdNotaCreditoCliente: Integer);
begin
  IdNotaCreditoCliente := varIdNotaCreditoCliente;
end;

procedure TNotaCreditoCliente.SetImpuesto(varImpuesto: Double);
begin
  Impuesto := varImpuesto;
end;

procedure TNotaCreditoCliente.SetLoginUsuario(varLoginUsuario: string);
begin
  LoginUsuario := varLoginUsuario;
end;

procedure TNotaCreditoCliente.SetMontoDescuento(varMontoDescuento: Double);
begin
  MontoDescuento := varMontoDescuento;
end;

procedure TNotaCreditoCliente.SetMontoNeto(varMontoNeto: Double);
begin
  MontoNeto := varMontoNeto;
end;

procedure TNotaCreditoCliente.SetNumero(varNumero: string);
begin
  Numero := varNumero;
end;

procedure TNotaCreditoCliente.SetNumeroFacturaAsociada(varNumeroFacturaAsociada: string);
begin
  NumeroFacturaAsociada := varNumeroFacturaAsociada;
end;

procedure TNotaCreditoCliente.SetObservaciones(varObservaciones: string);
begin
  Observaciones := varObservaciones;
end;

procedure TNotaCreditoCliente.SetPDescuento(varPDescuento: Double);
begin
  PDescuento := varPDescuento;
end;

procedure TNotaCreditoCliente.SetPIva(varPIva: Double);
begin
  PIva := varPIva;
end;

procedure TNotaCreditoCliente.SetSubTotal(varSubTotal: Double);
begin
  SubTotal := varSubTotal;
end;

procedure TNotaCreditoCliente.SetTasaDolar(varTasaDolar: Double);
begin
  TasaDolar := varTasaDolar;
end;

procedure TNotaCreditoCliente.SetTiempoIngreso(varTiempodeIngreso: TDateTime);
begin
  TiempoIngreso := varTiempodeIngreso;
end;

procedure TNotaCreditoCliente.SetTotal(varTotal: Double);
begin
  Total := varTotal;
end;

function TNotaCreditoCliente.GetCodigoCliente: string;
begin
  Result := CodigoCliente;
end;

function TNotaCreditoCliente.GetCodigoEstacion: string;
begin
  Result := CodigoEstacion;
end;

function TNotaCreditoCliente.GetCodigoMoneda: string;
begin
  Result := CodigoMoneda;
end;

function TNotaCreditoCliente.GetCodigoVendedor: string;
begin
  Result := CodigoVendedor;
end;

function TNotaCreditoCliente.GetCodigoZona: string;
begin
  Result := CodigoZona;
end;

function TNotaCreditoCliente.GetConcepto: string;
begin
  Result := Concepto;
end;

function TNotaCreditoCliente.GetFechaContable: TDate;
begin
  Result := FechaContable;
end;

function TNotaCreditoCliente.GetFechaIngreso: TDate;
begin
  Result := FechaIngreso;
end;

function TNotaCreditoCliente.GetFechaLibros: TDate;
begin
  Result := FechaLibros;
end;

function TNotaCreditoCliente.GetFechaRecepcion: TDate;
begin
  Result := FechaRecepcion;
end;

function TNotaCreditoCliente.GetFormaLibre: Boolean;
begin
  Result := FormaLibre;
end;

function TNotaCreditoCliente.GetIdCobranza: Integer;
begin
  Result := IdCobranza;
end;

function TNotaCreditoCliente.GetIdMovimientoInventario: Integer;
begin
  Result := IdMovimientoInventario;
end;

function TNotaCreditoCliente.GetIdNotaCreditoCliente: Integer;
begin
  Result := IdNotaCreditoCliente;
end;

function TNotaCreditoCliente.GetImpuesto: Double;
begin
  Result := Impuesto;
end;

function TNotaCreditoCliente.GetLoginUsuario: string;
begin
  Result := LoginUsuario;
end;

function TNotaCreditoCliente.GetMontoDescuento: Double;
begin
  Result := MontoDescuento;
end;

function TNotaCreditoCliente.GetMontoNeto: Double;
begin
  Result := MontoNeto;
end;

function TNotaCreditoCliente.GetNumero: string;
begin
  Result := Numero;
end;

function TNotaCreditoCliente.GetNumeroFacturaAsociada: string;
begin
  Result := NumeroFacturaAsociada;
end;

function TNotaCreditoCliente.GetObservaciones: string;
begin
  Result := Observaciones;
end;

function TNotaCreditoCliente.GetPDescuento: Double;
begin
  Result := PDescuento;
end;

function TNotaCreditoCliente.GetPIva: Double;
begin
  Result := PIva;
end;

function TNotaCreditoCliente.GetSubTotal: Double;
begin
  Result := SubTotal;
end;

function TNotaCreditoCliente.GetTasaDolar: Double;
begin
  Result := TasaDolar;
end;

function TNotaCreditoCliente.GetTiempoIngreso: TDateTime;
begin
  Result := TiempoIngreso;
end;

function TNotaCreditoCliente.GetTotal: Double;
begin
  Result := Total;
end;

function TNotaCreditoCliente.BuscarNumero: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("numero" = ' + QuotedStr(GetNumero) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

function TNotaCreditoCliente.BuscarNumeroFacturaAsociada: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("numero_factura_asociada" = ' + QuotedStr(GetNumeroFacturaAsociada) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

function TNotaCreditoCliente.BuscarCodigoCliente: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("codigo_cliente" = ' + QuotedStr(GetCodigoCliente) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

function TNotaCreditoCliente.BuscarCodigoVendedor: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("codigo_vendedor" = ' + QuotedStr(GetCodigoVendedor) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

end.
