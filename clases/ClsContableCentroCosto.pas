unit ClsContableCentroCosto;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TContableCentroCosto }

  TContableCentroCosto = class(TGenericoBD)
    private
      Codigo: string;
      Descripcion: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigo(varCodigo: string);
      procedure SetDescripcion(varDescripcion: string);
      function GetCodigo: string;
      function GetDescripcion: string;
  end;

implementation

{ TContableCentroCosto }

procedure TContableCentroCosto.Ajustar;
begin

end;

procedure TContableCentroCosto.Mover(Ds: TClientDataSet);
begin
  Codigo := Ds.FieldValues['codigo'];
  Descripcion := Ds.FieldValues['descripcion'];
end;

function TContableCentroCosto.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,2);
  Params[0] := Codigo;
  Params[1] := Descripcion;
  Result := Params;
end;

function TContableCentroCosto.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,2);
  Campos[0] := 'codigo';
  Campos[1] := 'descripcion';
  Result := Campos;
end;

function TContableCentroCosto.GetCondicion: string;
begin
  Result := '"codigo" = ' + QuotedStr(Codigo);
end;

procedure TContableCentroCosto.AntesEliminar;
begin

end;

procedure TContableCentroCosto.AntesInsertar;
begin

end;

procedure TContableCentroCosto.AntesModificar;
begin

end;

procedure TContableCentroCosto.DespuesEliminar;
begin

end;

procedure TContableCentroCosto.DespuesInsertar;
begin

end;

procedure TContableCentroCosto.DespuesModificar;
begin

end;

constructor TContableCentroCosto.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_contable_centro_costo');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(2)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "descripcion"=$2 WHERE ("codigo"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("codigo"=$1)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"codigo"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TContableCentroCosto.SetCodigo(varCodigo: string);
begin
  Codigo := varCodigo;
end;

procedure TContableCentroCosto.SetDescripcion(varDescripcion: string);
begin
  Descripcion := varDescripcion;
end;

function TContableCentroCosto.GetCodigo: string;
begin
  Result := Codigo;
end;

function TContableCentroCosto.GetDescripcion: string;
begin
  Result := Descripcion;
end;

end.

