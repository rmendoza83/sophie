unit ClsContableTipoComprobante;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TContableTipoComprobante }

  TContableTipoComprobante = class(TGenericoBD)
    private
      Anno: Integer;
      Mes: Integer;
      Codigo: string;
      Descripcion: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetAnno(varAnno: Integer);
      procedure SetMes(varMes: Integer);
      procedure SetCodigo(varCodigo: string);
      procedure SetDescripcion(varDescripcion: string);
      function GetAnno: Integer;
      function GetMes: Integer;
      function GetCodigo: string;
      function GetDescripcion: string;
  end;

implementation

{ TContableTipoComprobante }

procedure TContableTipoComprobante.Ajustar;
begin

end;

procedure TContableTipoComprobante.Mover(Ds: TClientDataSet);
begin
  Anno := Ds.FieldValues['anno'];
  Mes := Ds.FieldValues['mes'];
  Codigo := Ds.FieldValues['codigo'];
  Descripcion := Ds.FieldValues['descripcion'];
end;

function TContableTipoComprobante.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,4);
  Params[0] := Anno;
  Params[1] := Mes;
  Params[2] := Codigo;
  Params[3] := Descripcion;
  Result := Params;
end;

function TContableTipoComprobante.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,2);
  Campos[0] := 'codigo';
  Campos[1] := 'descripcion';
  Result := Campos;
end;

function TContableTipoComprobante.GetCondicion: string;
begin
  Result := '"anno" = ' + IntToStr(Anno) + ' AND ' +
            '"mes" = ' + IntToStr(Mes) + ' AND ' +
            '"codigo" = ' + QuotedStr(Codigo);
end;

procedure TContableTipoComprobante.AntesEliminar;
begin

end;

procedure TContableTipoComprobante.AntesInsertar;
begin

end;

procedure TContableTipoComprobante.AntesModificar;
begin

end;

procedure TContableTipoComprobante.DespuesEliminar;
begin

end;

procedure TContableTipoComprobante.DespuesInsertar;
begin

end;

procedure TContableTipoComprobante.DespuesModificar;
begin

end;

constructor TContableTipoComprobante.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_contable_tipo_comprobante');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(4)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "descripcion"=$4 WHERE ("anno"=$1) AND ("mes"=$2) AND ("codigo"=$3)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("anno"=$1) AND ("mes"=$2) AND ("codigo"=$3)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"codigo"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TContableTipoComprobante.SetAnno(varAnno: Integer);
begin
  Anno := varAnno;
end;

procedure TContableTipoComprobante.SetMes(varMes: Integer);
begin
  Mes := varMes;
end;

procedure TContableTipoComprobante.SetCodigo(varCodigo: string);
begin
  Codigo := varCodigo;
end;

procedure TContableTipoComprobante.SetDescripcion(varDescripcion: string);
begin
  Descripcion := varDescripcion;
end;

function TContableTipoComprobante.GetAnno: Integer;
begin
  Result := Anno;
end;

function TContableTipoComprobante.GetMes: Integer;
begin
  Result := Mes;
end;

function TContableTipoComprobante.GetCodigo: string;
begin
  Result := Codigo;
end;

function TContableTipoComprobante.GetDescripcion: string;
begin
  Result := Descripcion;
end;

end.

