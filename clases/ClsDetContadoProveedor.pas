unit ClsDetContadoProveedor;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TDetContadoProveedor }

  TDetContadoProveedor = class(TGenericoBD)
    private
      CodigoFormaPago: string;
      MontoNumeric: Double;
      CodigoBanco: string;
      NumeroCuenta: string;
      NumeroAuxiliar: string;
      Referencia: String;
      Fecha: TDate;
      Usuario: string;
      IdFacturacionProveedor: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      Procedure SetCodigoFormapago(varCodigoFormaPago: string);
      Procedure SetMontoNumeric(varMontoNumeric: Double);
      Procedure SetCodigoBanco(varCodigoBanco: string);
      Procedure SetNumeroCuenta(varNumeroCuenta: string);
      Procedure SetNumeroAuxiliar(varNumeroAuxiliar: string);
      Procedure SetReferencia(varReferencia: string);
      Procedure SetFecha(varFecha: TDate);
      Procedure SetUsuario(varUsuario: string);
      Procedure SetIdFacturacionProveedor(varIdFacturacionProveedor: Integer);
      Function GetCodigoFormaPago: string;
      Function GetMontoNumeric: Double;
      Function GetCodigoBanco: string;
      Function GetNumeroCuenta: string;
      Function GetNumeroAuxiliar: string;
      Function GetReferencia: string;
      Function GetFecha: TDate;
      Function GetUsuario: string;
      Function GetIdFacturacionProveedor: Integer;
  end;

implementation

uses DB;

{ TDetContadoProveedor }

procedure TDetContadoProveedor.Ajustar;
begin

end;

procedure TDetContadoProveedor.Mover(Ds: TClientDataSet);
begin
  CodigoFormaPago := Ds.FieldValues['codigo_forma_pago'];
  MontoNumeric := Ds.FieldValues['monto_numeric'];
  CodigoBanco := Ds.FieldValues['codigo_banco'];
  NumeroCuenta := Ds.FieldValues['numero_cuenta'];
  NumeroAuxiliar := Ds.FieldValues['numero_auxiliar'];
  Referencia := Ds.FieldValues['referencia'];
  Fecha := Ds.FieldByName('fecha').AsDateTime ;
  Usuario := Ds.FieldValues['usuario'];
  IdFacturacionProveedor := Ds.FieldValues['id_facturacion_proveedor'];
end;

function TDetContadoProveedor.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,9);
  Params[0]:= CodigoFormaPago;
  Params[1]:= MontoNumeric;
  Params[2]:= CodigoBanco;
  Params[3]:= NumeroCuenta;
  Params[4]:= NumeroAuxiliar;
  Params[5]:= Referencia;
  Params[6]:= Fecha;
  Params[7]:= Usuario;
  Params[8]:= IdFacturacionProveedor;
  result := Params;
end;

function TDetContadoProveedor.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,8);
  Campos[0] := 'codigo_forma_pago';
  Campos[1] := 'monto_numeric';
  Campos[2] := 'codigo_banco';
  Campos[3] := 'numero_cuenta';
  Campos[4] := 'numero_auxiliar';
  Campos[5] := 'referencia';
  Campos[6] := 'fecha';
  Campos[7] := 'usuario';
  Result := Campos;
end;

function TDetContadoProveedor.GetCondicion: string;
begin
  Result := '"id_facturacion_proveedor" = ' + QuotedStr(IntToStr(IdFacturacionProveedor)) + 'AND "codigo_forma_pago" =' + QuotedStr((CodigoformaPago));
end;

procedure TDetContadoProveedor.AntesInsertar;
begin

end;

procedure TDetContadoProveedor.AntesModificar;
begin

end;

procedure TDetContadoProveedor.AntesEliminar;
begin

end;

procedure TDetContadoProveedor.DespuesInsertar;
begin

end;

procedure TDetContadoProveedor.DespuesModificar;
begin

end;

procedure TDetContadoProveedor.DespuesEliminar;
begin

end;

constructor TDetContadoProveedor.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_det_contado_proveedores');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(9)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "monto_numeric"=$2,"codigo_banco"=$3,"numero_cuenta"=$4,"numero_auxiliar"=$5,"referencia"=$6,"fecha"=$7,"usuario"=$8, WHERE ("id_facturacion_proveedor"=$9) AND ("codigo_forma_pago"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("id_facturacion_proveedor"=$9) AND ("codigo_forma_pago"=$1)');
  SetLength(ClavesPrimarias,2);
  ClavesPrimarias[0]:='"id_facturacion_proveedor"';
  ClavesPrimarias[1]:='"codigo_forma_pago"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TDetContadoProveedor.SetCodigoBanco(varCodigoBanco: string);
begin
  CodigoBanco := varCodigoBanco;
end;

procedure TDetContadoProveedor.SetCodigoFormapago(varCodigoFormaPago: string);
begin
  CodigoFormaPago := varCodigoFormaPago;
end;

procedure TDetContadoProveedor.SetFecha(varFecha: TDate);
begin
  Fecha := varFecha;
end;

procedure TDetContadoProveedor.SetIdFacturacionProveedor(varIdFacturacionProveedor: Integer);
begin
  IdFacturacionProveedor := varIdFacturacionProveedor;
end;

procedure TDetContadoProveedor.SetMontoNumeric(varMontoNumeric: Double);
begin
  MontoNumeric := varMontoNumeric;
end;

procedure TDetContadoProveedor.SetNumeroAuxiliar(varNumeroAuxiliar: string);
begin
  NumeroAuxiliar := varNumeroAuxiliar;
end;

procedure TDetContadoProveedor.SetNumeroCuenta(varNumeroCuenta: string);
begin
  NumeroCuenta := varNumeroCuenta;
end;

procedure TDetContadoProveedor.SetReferencia(varReferencia: string);
begin
  Referencia := varReferencia;
end;

procedure TDetContadoProveedor.SetUsuario(varUsuario: string);
begin
  Usuario := varUsuario;
end;

function TDetContadoProveedor.GetCodigoBanco: string;
begin
  Result := CodigoBanco;
end;

function TDetContadoProveedor.GetCodigoFormaPago: string;
begin
  Result := CodigoFormaPago;
end;

function TDetContadoProveedor.GetFecha: TDate;
begin
  Result := Fecha;
end;

function TDetContadoProveedor.GetIdFacturacionProveedor: Integer;
begin
  Result := IdFacturacionProveedor;
end;

function TDetContadoProveedor.GetMontoNumeric: Double;
begin
  Result := MontoNumeric;
end;

function TDetContadoProveedor.GetNumeroAuxiliar: string;
begin
  Result := NumeroAuxiliar;
end;

function TDetContadoProveedor.GetNumeroCuenta: string;
begin
  Result := NumeroCuenta;
end;

function TDetContadoProveedor.GetReferencia: string;
begin
  Result := Referencia;
end;

function TDetContadoProveedor.GetUsuario: string;
begin
  Result := Usuario;
end;

end.
