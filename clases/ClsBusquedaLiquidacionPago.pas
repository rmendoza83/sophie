unit ClsBusquedaLiquidacionPago;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TBusquedaLiquidacionPago }

  TBusquedaLiquidacionPago = class(TGenericoBD)
    private
      Numero: string;
      CodigoProveedor: string;
      Rif: string;
      Nit: string;
      RazonSocial: string;
      Contacto: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      function GetNumero: string;
      function GetCodigoProveedor: string;
      function GetRif: string;
      function GetNit: string;
      function GetRazonSocial: string;
      function GetContacto: string;
  end;

implementation

{ TBusquedaLiquidacionPago }

procedure TBusquedaLiquidacionPago.Ajustar;
begin

end;

procedure TBusquedaLiquidacionPago.Mover(Ds: TClientDataSet);
begin
  Numero := Ds.FieldValues['numero'];
  CodigoProveedor := Ds.FieldValues['codigo_proveedor'];
  Rif := Ds.FieldValues['rif'];
  Nit := Ds.FieldValues['nit'];
  RazonSocial := Ds.FieldValues['razon_social'];
  Contacto := Ds.FieldValues['contacto'];
end;

function TBusquedaLiquidacionPago.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,6);
  Params[0] := Numero;
  Params[1] := CodigoProveedor;
  Params[2] := Rif;
  Params[3] := Nit;
  Params[4] := RazonSocial;
  Params[5] := Contacto;
  Result := Params;
end;

function TBusquedaLiquidacionPago.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,6);
  Campos[0] := 'numero';
  Campos[1] := 'codigo_proveedor';
  Campos[2] := 'rif';
  Campos[3] := 'nit';
  Campos[4] := 'razon_social';
  Campos[5] := 'contacto';
  Result := Campos;
end;

function TBusquedaLiquidacionPago.GetCondicion: string;
begin
  Result:= '';
end;

procedure TBusquedaLiquidacionPago.AntesInsertar;
begin

end;

procedure TBusquedaLiquidacionPago.AntesModificar;
begin

end;

procedure TBusquedaLiquidacionPago.AntesEliminar;
begin

end;

procedure TBusquedaLiquidacionPago.DespuesInsertar;
begin

end;

procedure TBusquedaLiquidacionPago.DespuesModificar;
begin

end;

procedure TBusquedaLiquidacionPago.DespuesEliminar;
begin

end;

constructor TBusquedaLiquidacionPago.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('v_busqueda_liquidacion_pagos');
  SetLength(ClavesPrimarias,0);
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

function TBusquedaLiquidacionPago.GetCodigoProveedor: string;
begin
  Result := CodigoProveedor;
end;

function TBusquedaLiquidacionPago.GetContacto: string;
begin
  Result := Contacto;
end;

function TBusquedaLiquidacionPago.GetNit: string;
begin
  Result := Nit;
end;

function TBusquedaLiquidacionPago.GetNumero: string;
begin
  Result := Numero;
end;

function TBusquedaLiquidacionPago.GetRazonSocial: String;
begin
  Result := RazonSocial;
end;

function TBusquedaLiquidacionPago.GetRif: String;
begin
  Result := Rif;
end;

end.
