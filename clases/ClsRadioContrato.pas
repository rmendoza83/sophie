unit ClsRadioContrato;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TRadioContrato}

  TRadioContrato = class(TGenericoBD)
    private
      Numero: string;
      CodigoCliente: string;
      CodigoAnunciante: string;
      CodigoAgencia: string;
      CodigoVendedor: string;
      CodigoZona: string;
      FechaInicial: TDate;
      FechaFinal: TDate;
      CodigoRubro: string;
      FechaIngreso: TDate;
      Total: Double;
      VentaNacional: Boolean;
      NumeroOrden: string;
      FechaOrden: TDate;
      Cancelado: Boolean;
      FechaCancelado: TDate;
      Observaciones: string;
      Producto: string;
      NroFacturas: Integer;
      Efectivo: Boolean;
      Intercambio: Boolean;
      IdRadioContrato: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetNumero(varNumero: string);
      procedure SetCodigoCliente(varCodigoCliente: string);
      procedure SetCodigoAnunciante(varCodigoAnunciante: string);
      procedure SetCodigoAgencia(varCodigoAgencia: string);
      procedure SetCodigoVendedor(varCodigoVendedor: string);
      procedure SetCodigoZona(varCodigoZona: string);
      procedure SetFechaInicial(varFechaInicial: TDate);
      procedure SetFechaFinal(varFechaFinal: TDate);
      procedure SetCodigoRubro(varCodigoRubro: string);
      procedure SetFechaIngreso(varFechaIngreso: TDate);
      procedure SetTotal(varTotal: Double);
      procedure SetVentaNacional(varVentaNacional: Boolean);
      procedure SetNumeroOrden(varNumeroOrden: string);
      procedure SetFechaOrden(varFechaOrden: TDate);
      procedure SetCancelado(varCancelado: Boolean);
      procedure SetFechaCancelado(varFechaCancelado: TDate);
      procedure SetObservaciones(varObservaciones: string);
      procedure SetProducto(varProducto: string);
      procedure SetNroFacturas(varNroFacturas: Integer);
      procedure SetEfectivo(varEfectivo: Boolean);
      procedure SetIntercambio(varIntercambio: Boolean);
      procedure SetIdRadioContrato(varIdRadioContrato: Integer);
      function GetNumero: string;
      function GetCodigoCliente: string;
      function GetCodigoAnunciante: string;
      function GetCodigoAgencia: string;
      function GetCodigoVendedor: string;
      function GetCodigoZona: string;
      function GetFechaInicial: TDate;
      function GetFechaFinal: TDate;
      function GetCodigoRubro: string;
      function GetFechaIngreso: TDate;
      function GetTotal: Double;
      function GetVentaNacional: Boolean;
      function GetNumeroOrden: string;
      function GetFechaOrden: TDate;
      function GetCancelado: Boolean;
      function GetFechaCancelado: TDate;
      function GetObservaciones: string;
      function GetProducto: string;
      function GetNroFacturas: Integer;
      function GetEfectivo: Boolean;
      function GetIntercambio: Boolean;
      function GetIdRadioContrato: Integer;
      function BuscarNumero: Boolean;
  end;

implementation

uses
  DB,
  ClsConsecutivo;
  
{ TRadioContrato }

procedure TRadioContrato.Ajustar;
begin

end;

procedure TRadioContrato.Mover(Ds: TClientDataSet);
begin
  Numero := Ds.FieldValues['numero'];
  CodigoCliente := Ds.FieldValues['codigo_cliente'];
  CodigoAnunciante := Ds.FieldValues['codigo_anunciante'];
  CodigoAgencia := Ds.FieldValues['codigo_agencia'];
  CodigoVendedor := Ds.FieldValues['codigo_vendedor'];
  CodigoZona := Ds.FieldValues['codigo_zona'];
  FechaInicial := Ds.FieldValues['fecha_inicial'];
  FechaFinal := Ds.FieldValues['fecha_final'];
  CodigoRubro := Ds.FieldValues['codigo_rubro'];
  FechaIngreso := Ds.FieldValues['fecha_ingreso'];
  Total := Ds.FieldValues['total'];
  VentaNacional := Ds.FieldValues['venta_nacional'];
  NumeroOrden := Ds.FieldValues['numero_orden'];
  FechaOrden := Ds.FieldValues['fecha_orden'];
  Cancelado := Ds.FieldValues['cancelado'];
  FechaCancelado := Ds.FieldValues['fecha_cancelado'];
  Observaciones := Ds.FieldValues['observaciones'];
  Producto := Ds.FieldValues['producto'];
  NroFacturas := Ds.FieldValues['nro_facturas'];
  Efectivo := Ds.FieldValues['efectivo'];
  Intercambio := Ds.FieldValues['intercambio'];
  IdRadioContrato := Ds.FieldValues['id_radio_contrato'];
end;

function TRadioContrato.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,22);
  Params[0] := Numero;
  Params[1] := CodigoCliente;
  Params[2] := CodigoAnunciante;
  Params[3] := CodigoAgencia;
  Params[4] := CodigoVendedor;
  Params[5] := CodigoZona;
  Params[6] := VarFromDateTime(FechaInicial);
  Params[7] := VarFromDateTime(FechaFinal);
  Params[8] := CodigoRubro;
  Params[9] := VarFromDateTime(FechaIngreso);
  Params[10] := Total;
  Params[11] := VentaNacional;
  Params[12] := NumeroOrden;
  Params[13] := VarFromDateTime(FechaOrden);
  Params[14] := Cancelado;
  Params[15] := VarFromDateTime(FechaCancelado);
  Params[16] := Observaciones;
  Params[17] := Producto;
  Params[18] := NroFacturas;
  Params[19] := Efectivo;
  Params[20] := Intercambio;
  Params[21] := IdRadioContrato;
  Result := Params;
end;

function TRadioContrato.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,10);
  Campos[0] := 'numero';
  Campos[1] := 'codigo_cliente';
  Campos[2] := 'codigo_anunciante';
  Campos[3] := 'codigo_agencia';
  Campos[4] := 'codigo_vendedor';
  Campos[5] := 'codigo_zona';
  Campos[6] := 'codigo_rubro';
  Campos[7] := 'numero_orden';
  Campos[8] := 'observaciones';
  Campos[9] := 'producto';
  Result := Campos;
end;

function TRadioContrato.GetCondicion: string;
begin
  Result := '"id_radio_contrato" = ' + IntToStr(IdRadioContrato);
end;

procedure TRadioContrato.AntesInsertar;
var
  Consecutivo: TConsecutivo;
begin
  Consecutivo := TConsecutivo.Create;
  Numero := IntToStr(Consecutivo.ObtenerConsecutivo('numero_radio_contrato'));
  IdRadioContrato := Consecutivo.ObtenerConsecutivo('id_radio_contrato');
end;

procedure TRadioContrato.AntesModificar;
begin

end;

procedure TRadioContrato.AntesEliminar;
begin

end;

procedure TRadioContrato.DespuesInsertar;
begin

end;

procedure TRadioContrato.DespuesModificar;
begin

end;

procedure TRadioContrato.DespuesEliminar;
begin

end;

constructor TRadioContrato.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_radio_contratos');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(22)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "numero"=$1, "codigo_cliente"=$2, "codigo_anunciante"=$3, "codigo_agencia"=$4, "codigo_vendedor"=$5, "codigo_zona"=$6, "fecha_inicial"=$7, "fecha_final"=$8, "codigo_rubro"=$9, "fecha_ingreso"=$10, ' +
                  '"total"=$11, "venta_nacional"=$12, "numero_orden"=$13, "fecha_orden"=$14, "cancelado"=$15, "fecha_cancelado"=$16, "observaciones"=$17, "producto"=$18, "nro_facturas"=$19, "efectivo"=$20, ' +
                  '"intercambio"=$21 WHERE ("id_radio_contrato"=$22)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("id_radio_contrato"=$22)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"id_radio_contrato"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TRadioContrato.SetNumero(varNumero: string);
begin
  Numero := varNumero;
end;

procedure TRadioContrato.SetCodigoCliente(varCodigoCliente: string);
begin
  CodigoCliente := varCodigoCliente;
end;

procedure TRadioContrato.SetCodigoAnunciante(varCodigoAnunciante: string);
begin
  CodigoAnunciante := varCodigoAnunciante;
end;

procedure TRadioContrato.SetCodigoAgencia(varCodigoAgencia: string);
begin
  CodigoAgencia := varCodigoAgencia;
end;

procedure TRadioContrato.SetCodigoVendedor(varCodigoVendedor: string);
begin
  CodigoVendedor := varCodigoVendedor;
end;

procedure TRadioContrato.SetCodigoZona(varCodigoZona: string);
begin
  CodigoZona := varCodigoZona;
end;

procedure TRadioContrato.SetFechaInicial(varFechaInicial: TDate);
begin
  FechaInicial := varFechaInicial;
end;

procedure TRadioContrato.SetFechaFinal(varFechaFinal: TDate);
begin
  FechaFinal := varFechaFinal;
end;

procedure TRadioContrato.SetCodigoRubro(varCodigoRubro: string);
begin
  CodigoRubro := varCodigoRubro;
end;

procedure TRadioContrato.SetFechaIngreso(varFechaIngreso: TDate);
begin
  FechaIngreso := varFechaIngreso;
end;

procedure TRadioContrato.SetTotal(varTotal: Double);
begin
  Total := varTotal;
end;

procedure TRadioContrato.SetVentaNacional(varVentaNacional: Boolean);
begin
  VentaNacional := varVentaNacional;
end;

procedure TRadioContrato.SetNumeroOrden(varNumeroOrden: string);
begin
  NumeroOrden := varNumeroOrden;
end;

procedure TRadioContrato.SetFechaOrden(varFechaOrden: TDate);
begin
  FechaOrden := varFechaOrden;
end;

procedure TRadioContrato.SetCancelado(varCancelado: Boolean);
begin
  Cancelado := varCancelado;
end;

procedure TRadioContrato.SetFechaCancelado(varFechaCancelado: TDate);
begin
  FechaCancelado := varFechaCancelado;
end;

procedure TRadioContrato.SetObservaciones(varObservaciones: string);
begin
  Observaciones := varObservaciones;
end;

procedure TRadioContrato.SetProducto(varProducto: string);
begin
  Producto := varProducto;
end;

procedure TRadioContrato.SetNroFacturas(varNroFacturas: Integer);
begin
  NroFacturas := varNroFacturas;
end;

procedure TRadioContrato.SetEfectivo(varEfectivo: Boolean);
begin
  Efectivo := varEfectivo;
end;

procedure TRadioContrato.SetIntercambio(varIntercambio: Boolean);
begin
  Intercambio := varIntercambio;
end;

procedure TRadioContrato.SetIdRadioContrato(varIdRadioContrato: Integer);
begin
  IdRadioContrato := varIdRadioContrato;
end;

function TRadioContrato.GetNumero: string;
begin
  Result := Numero;
end;

function TRadioContrato.GetCodigoCliente: string;
begin
  Result := CodigoCliente;
end;

function TRadioContrato.GetCodigoAnunciante: string;
begin
  Result := CodigoAnunciante;
end;

function TRadioContrato.GetCodigoAgencia: string;
begin
  Result := CodigoAgencia;
end;

function TRadioContrato.GetCodigoVendedor: string;
begin
  Result := CodigoVendedor;
end;

function TRadioContrato.GetCodigoZona: string;
begin
  Result := CodigoZona;
end;

function TRadioContrato.GetFechaInicial: TDate;
begin
  Result := FechaInicial;
end;

function TRadioContrato.GetFechaFinal: TDate;
begin
  Result := FechaFinal;
end;

function TRadioContrato.GetCodigoRubro: string;
begin
  Result := CodigoRubro;
end;

function TRadioContrato.GetFechaIngreso: TDate;
begin
  Result := FechaIngreso;
end;

function TRadioContrato.GetTotal: Double;
begin
  Result := Total;
end;

function TRadioContrato.GetVentaNacional: Boolean;
begin
  Result := VentaNacional;
end;

function TRadioContrato.GetNumeroOrden: string;
begin
  Result := NumeroOrden;
end;

function TRadioContrato.GetFechaOrden: TDate;
begin
  Result := FechaOrden;
end;

function TRadioContrato.GetCancelado: Boolean;
begin
  Result := Cancelado;
end;

function TRadioContrato.GetFechaCancelado: TDate;
begin
  Result := FechaCancelado;
end;

function TRadioContrato.GetObservaciones: string;
begin
  Result := Observaciones;
end;

function TRadioContrato.GetProducto: string;
begin
  Result := Producto;
end;

function TRadioContrato.GetNroFacturas: Integer;
begin
  Result := NroFacturas;
end;

function TRadioContrato.GetEfectivo: Boolean;
begin
  Result := Efectivo;
end;

function TRadioContrato.GetIntercambio: Boolean;
begin
  Result := Intercambio;
end;

function TRadioContrato.GetIdRadioContrato: Integer;
begin
  Result := IdRadioContrato;
end;

function TRadioContrato.BuscarNumero: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("numero" = ' + QuotedStr(GetNumero) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

end.
