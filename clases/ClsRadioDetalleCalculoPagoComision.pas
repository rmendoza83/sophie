unit ClsRadioDetalleCalculoPagoComision;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TRadioDetalleCalculoPagoComision }

  TRadioDetalleCalculoPagoComision = class(TGenericoBD)
    private
      Desde: string;
      Numero: string;
      CodigoVendedor: string;
      FechaIngreso: TDate;
      MontoBase: Double;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      function GetDesde: string;
      function GetNumero: string;
      function GetCodigoVendedor: string;
      function GetFechaIngreso: TDate;
      function GetMontoBase: Double;
      function ObtenerVendedor(varCodigoVendedor: string; varFechaInicial: TDate; varFechaFinal: TDate): TClientDataSet;
  end;

implementation

uses DB;

{ TRadioDetalleCalculoPagoComision }

procedure TRadioDetalleCalculoPagoComision.Ajustar;
begin

end;

procedure TRadioDetalleCalculoPagoComision.Mover(Ds: TClientDataSet);
begin
  Desde := Ds.FieldValues['desde'];
  Numero := Ds.FieldValues['numero'];
  CodigoVendedor := Ds.FieldValues['codigo_vendedor'];
  FechaIngreso := Ds.FieldByName('fecha_ingreso').AsDateTime;
  MontoBase := Ds.FieldValues['monto_base'];
end;

function TRadioDetalleCalculoPagoComision.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,5);
  Params[0] := Desde;
  Params[1] := Numero;
  Params[2] := CodigoVendedor;
  Params[3] := VarFromDateTime(FechaIngreso);
  Params[4] := MontoBase;
  Result := Params;
end;

function TRadioDetalleCalculoPagoComision.GetCampos: TArrayString;
var
  Campos: TArraystring;
begin
  SetLength(Campos,3);
  Campos[0] := 'desde';
  Campos[1] := 'numero';
  Campos[2] := 'codigo_vendedor';
  Result := Campos;
end;

function TRadioDetalleCalculoPagoComision.GetCondicion: string;
begin
  Result:= '';
end;

procedure TRadioDetalleCalculoPagoComision.AntesInsertar;
begin

end;

procedure TRadioDetalleCalculoPagoComision.AntesModificar;
begin

end;

procedure TRadioDetalleCalculoPagoComision.AntesEliminar;
begin

end;

procedure TRadioDetalleCalculoPagoComision.DespuesInsertar;
begin

end;

procedure TRadioDetalleCalculoPagoComision.DespuesModificar;
begin

end;

procedure TRadioDetalleCalculoPagoComision.DespuesEliminar;
begin

end;

constructor TRadioDetalleCalculoPagoComision.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('v_radio_detalle_calculo_pago_comision');
  SetLength(ClavesPrimarias,0);
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

function TRadioDetalleCalculoPagoComision.GetDesde: string;
begin
  Result := Desde;
end;

function TRadioDetalleCalculoPagoComision.GetCodigoVendedor: string;
begin
  Result := CodigoVendedor;
end;

function TRadioDetalleCalculoPagoComision.GetFechaIngreso: TDate;
begin
  Result := FechaIngreso;
end;

function TRadioDetalleCalculoPagoComision.GetNumero: string;
begin
  Result := Numero;
end;

function TRadioDetalleCalculoPagoComision.GetMontoBase: Double;
begin
  Result := MontoBase;
end;

function TRadioDetalleCalculoPagoComision.ObtenerVendedor(varCodigoVendedor: string;
  varFechaInicial, varFechaFinal: TDate): TClientDataSet;
begin
  Result := ObtenerListaCondicionSQL(' WHERE ("codigo_vendedor" = ' + QuotedStr(varCodigoVendedor) + ') AND "fecha_ingreso" BETWEEN ' + QuotedStr(FormatDateTime('YYYY-mm-dd',varFechaInicial)) + ' AND (' + QuotedStr(FormatDateTime('YYYY-mm-dd',varFechaFinal)) + ')');
end;

end.
