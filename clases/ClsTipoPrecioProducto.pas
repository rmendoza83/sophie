unit ClsTipoPrecioProducto;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;
type

  { TTipoPrecioProducto }

  TTipoPrecioProducto = class(TGenericoBD)
    private
      Codigo: string;
      Descripcion: string;
      CalculoAutomatico: Boolean;
      PCalculoAutomatico: Double;
      CalculoMontoFijo: Boolean;
      MontoFijo: Double;
     protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigo(varCodigo: string);
      procedure SetDescripcion(varDescripcion: string);
      procedure SetCalculoAutomatico(varCalculoAutomatico: Boolean);
      procedure SetPCalculoAutomatico(varPCalculoAutomatico: Double);
      procedure SetCalculoMontoFijo(varCalculoMontoFijo: Boolean);
      procedure SetMontoFijo(varMontoFijo: Double);
      function GetCodigo: string;
      function GetDescripcion: string;
      function GetCalculoAutomatico: Boolean;
      function GetPCalculoAutomatico: Double;
      function GetCalculoMontoFijo: Boolean;
      function GetMontoFijo: Double;
  end;

implementation

uses DB;


{ TTipoPrecioProducto }

procedure TTipoPrecioProducto.Ajustar;
begin

end;

procedure TTipoPrecioProducto.Mover(Ds: TClientDataSet);
begin
  Codigo := Ds.FieldValues['codigo'];
  Descripcion := Ds.FieldValues['descripcion'];
  CalculoAutomatico := Ds.FieldValues['calculo_automatico'];
  PCalculoAutomatico := Ds.FieldValues['p_calculo_automatico'];
  CalculoMontoFijo := Ds.FieldValues['calculo_monto_fijo'];
  MontoFijo := Ds.FieldValues['monto_fijo'];
end;

function TTipoPrecioProducto.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,6);
  Params[0] := Codigo;
  Params[1] := Descripcion;
  Params[2] := CalculoAutomatico;
  Params[3] := PCalculoAutomatico;
  Params[4] := CalculoMontoFijo;
  Params[5] := MontoFijo;
  Result := Params;
end;

function TTipoPrecioProducto.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,2);
  Campos[0] := 'codigo';
  Campos[1] := 'descripcion';
  Result := Campos;
end;

function TTipoPrecioProducto.GetCondicion: string;
begin
  Result := '"codigo" = ' + QuotedStr(Codigo);
end;

procedure TTipoPrecioProducto.AntesInsertar;
begin

end;

procedure TTipoPrecioProducto.AntesModificar;
begin

end;

procedure TTipoPrecioProducto.AntesEliminar;
begin

end;

procedure TTipoPrecioProducto.DespuesInsertar;
begin

end;

procedure TTipoPrecioProducto.DespuesModificar;
begin

end;

procedure TTipoPrecioProducto.DespuesEliminar;
begin

end;

constructor TTipoPrecioProducto.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_tipos_precio_productos');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(6)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "descripcion"=$2, "calculo_automatico"=$3, "p_calculo_automatico"=$4, "calculo_monto_fijo"=$5, "monto_fijo"=$6  WHERE ("codigo"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("codigo"=$1)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"codigo"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TTipoPrecioProducto.SetCodigo(varCodigo: string);
begin
  Codigo := varCodigo;
end;

procedure TTipoPrecioProducto.SetDescripcion(varDescripcion: string);
begin
  Descripcion := varDescripcion;
end;

procedure TTipoPrecioProducto.SetCalculoAutomatico(varCalculoAutomatico: Boolean);
begin
  CalculoAutomatico := varCalculoAutomatico;
end;

procedure TTipoPrecioProducto.SetPCalculoAutomatico(varPCalculoAutomatico: Double);
begin
  PCalculoAutomatico := varPCalculoAutomatico;
end;

procedure TTipoPrecioProducto.SetCalculoMontoFijo(varCalculoMontoFijo: Boolean);
begin
  CalculoMontoFijo := varCalculoMontoFijo;
end;

procedure TTipoPrecioProducto.SetMontoFijo(varMontoFijo: Double);
begin
  MontoFijo := varMontoFijo;
end;

function TTipoPrecioProducto.GetCodigo: string;
begin
  Result := Codigo;
end;

function TTipoPrecioProducto.GetDescripcion: string;
begin
  Result := Descripcion;
end;

function TTipoPrecioProducto.GetCalculoAutomatico: Boolean;
begin
  Result := CalculoAutomatico;
end;

function TTipoPrecioProducto.GetPCalculoAutomatico: Double;
begin
  Result := PCalculoAutomatico;
end;

function TTipoPrecioProducto.GetCalculoMontoFijo: Boolean;
begin
  Result := CalculoMontoFijo;
end;

function TTipoPrecioProducto.GetMontoFijo: Double;
begin
  Result := MontoFijo;
end;

end.
