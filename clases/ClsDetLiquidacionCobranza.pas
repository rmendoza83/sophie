unit ClsDetLiquidacionCobranza;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TDetLiquidacionCobranza }

  TDetLiquidacionCobranza = class(TGenericoBD)
    private
      IdLiquidacionCobranza: Integer;
      IdCobranza: Integer;
      NumeroCuota: Integer;
      OrdenCobranza: Integer;
      Abonado: Double;
      CodigoRetencionIva: string;
      MontoRetencionIva: Double;
      CodigoRetencion1: string;
      MontoRetencion1: Double;
      CodigoRetencion2: string;
      MontoRetencion2: Double;
      CodigoRetencion3: string;
      MontoRetencion3: Double;
      CodigoRetencion4: string;
      MontoRetencion4: Double;
      CodigoRetencion5: string;
      MontoRetencion5: Double;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetIdLiquidacionCobranza(varIdLiquidacionCobranza: Integer);
      procedure SetIdCobranza(varIdCobranza: Integer);
      procedure SetNumeroCuota(varNumeroCuota: Integer);
      procedure SetOrdenCobranza(varOrdenCobranza: Integer);
      procedure SetAbonado(varAbonado: Double);
      procedure SetCodigoRetencionIva(varCodigoRetencionIva: string);
      procedure SetMontoRetencionIva(varMontoRetencionIva: Double);
      procedure SetCodigoRetencion1(varCodigoRetencion1: string);
      procedure SetMontoRetencion1(varMontoRetencion1: Double);
      procedure SetCodigoRetencion2(varCodigoRetencion2: string);
      procedure SetMontoRetencion2(varMontoRetencion2: Double);
      procedure SetCodigoRetencion3(varCodigoRetencion3: string);
      procedure SetMontoRetencion3(varMontoRetencion3: Double);
      procedure SetCodigoRetencion4(varCodigoRetencion4: string);
      procedure SetMontoRetencion4(varMontoRetencion4: Double);
      procedure SetCodigoRetencion5(varCodigoRetencion5: string);
      procedure SetMontoRetencion5(varMontoRetencion5: Double);
      function GetIdLiquidacionCobranza: Integer;
      function GetIdCobranza: Integer;
      function GetNumeroCuota: Integer;
      function GetOrdenCobranza: Integer;
      function GetAbonado: Double;
      function GetCodigoRetencionIva: string;
      function GetMontoRetencionIva: Double;
      function GetCodigoRetencion1: string;
      function GetMontoRetencion1: Double;
      function GetCodigoRetencion2: string;
      function GetMontoRetencion2: Double;
      function GetCodigoRetencion3: string;
      function GetMontoRetencion3: Double;
      function GetCodigoRetencion4: string;
      function GetMontoRetencion4: Double;
      function GetCodigoRetencion5: string;
      function GetMontoRetencion5: Double;
      function ObtenerIdLiquidacionCobranza(varIdLiquidacionCobranza: Integer): TClientDataSet;
      function EliminarIdLiquidacionCobranza(varIdLiquidacionCobranza: Integer): Boolean;
      function VerificarRetencionesISLR(varIdCobranza: Integer): Boolean;
      function VerificarRetencionesIVA(varIdCobranza: Integer): Boolean;
  end;

implementation

uses DB;

{ TDetLiquidacionCobranza }

procedure TDetLiquidacionCobranza.Ajustar;
begin

end;

procedure TDetLiquidacionCobranza.Mover(Ds: TClientDataSet);
begin
  IdLiquidacionCobranza := Ds.FieldValues['id_liquidacion_cobranza'];
  IdCobranza := Ds.FieldValues['id_cobranza'];
  NumeroCuota := Ds.FieldValues['numero_cuota'];
  OrdenCobranza := Ds.FieldValues['orden_cobranza'];
  Abonado := Ds.FieldValues['abonado'];
  CodigoRetencionIva := Ds.FieldValues['codigo_retencion_iva'];
  MontoRetencionIva := Ds.FieldValues['monto_retencion_iva'];
  CodigoRetencion1 := Ds.FieldValues['codigo_retencion_1'];
  MontoRetencion1 := Ds.FieldValues['monto_retencion_1'];
  CodigoRetencion2 := Ds.FieldValues['codigo_retencion_2'];
  MontoRetencion2 := Ds.FieldValues['monto_retencion_2'];
  CodigoRetencion3 := Ds.FieldValues['codigo_retencion_3'];
  MontoRetencion3 := Ds.FieldValues['monto_retencion_3'];
  CodigoRetencion4 := Ds.FieldValues['codigo_retencion_4'];
  MontoRetencion4 := Ds.FieldValues['monto_retencion_4'];
  CodigoRetencion5 := Ds.FieldValues['codigo_retencion_5'];
  MontoRetencion5 := Ds.FieldValues['monto_retencion_5'];
end;

function TDetLiquidacionCobranza.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,17);
  Params[0] := IdLiquidacionCobranza;
  Params[1] := IdCobranza;
  Params[2] := NumeroCuota;
  Params[3] := OrdenCobranza;
  Params[4] := Abonado;
  Params[5] := CodigoRetencionIva;
  Params[6] := MontoRetencionIva;
  Params[7] := CodigoRetencion1;
  Params[8] := MontoRetencion1;
  Params[9] := CodigoRetencion2;
  Params[10] := MontoRetencion2;
  Params[11] := CodigoRetencion3;
  Params[12] := MontoRetencion3;
  Params[13] := CodigoRetencion4;
  Params[14] := MontoRetencion4;
  Params[15] := CodigoRetencion5;
  Params[16] := MontoRetencion5;
  Result := Params;
end;

function TDetLiquidacionCobranza.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,6);
  Campos[0] := 'codigo_retencion_iva';
  Campos[1] := 'codigo_retencion_1';
  Campos[2] := 'codigo_retencion_2';
  Campos[3] := 'codigo_retencion_3';
  Campos[4] := 'codigo_retencion_4';
  Campos[5] := 'codigo_retencion_5';
  Result := Campos;
end;

function TDetLiquidacionCobranza.GetCondicion: string;
begin
  Result := '("id_liquidacion_cobranza" = ' + IntToStr(IdLiquidacionCobranza) + ') AND ("id_cobranza" = ' + IntToStr(IdCobranza) + ') AND ("orden_cobranza" = ' + IntToStr(OrdenCobranza) + ')';
end;

procedure TDetLiquidacionCobranza.AntesInsertar;
begin

end;

procedure TDetLiquidacionCobranza.AntesModificar;
begin

end;

procedure TDetLiquidacionCobranza.AntesEliminar;
begin

end;

procedure TDetLiquidacionCobranza.DespuesInsertar;
begin

end;

procedure TDetLiquidacionCobranza.DespuesEliminar;
begin

end;

procedure TDetLiquidacionCobranza.DespuesModificar;
begin

end;

constructor TDetLiquidacionCobranza.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_det_liquidacion_cobranzas');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(17)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "abonado"=$5, "codigo_retencion_iva"=$6, "monto_retencion_iva"=$7, "codigo_retencion_1"=$8, "monto_retencion_1"=$9, "codigo_retencion_2"=$10, "monto_retencion_2"=$11, ' +
                  '"codigo_retencion_3"=$12, "monto_retencion_3"=$13, "codigo_retencion_4"=$14, "monto_retencion_4"=$15, "codigo_retencion_5"=$16, "monto_retencion_5"=$17 ' +
                  'WHERE ("id_liquidacion_cobranza"=$1) AND ("id_cobranza"=$2) AND ("numero_cuota"=$3) AND ("orden_cobranza"=$4)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("id_liquidacion_cobranza"=$1) AND ("id_cobranza"=$2) AND ("numero_cuota"=$3) AND ("orden_cobranza"=$4)');
  SetLength(ClavesPrimarias,4);
  ClavesPrimarias[0] := '"id_liquidacion_cobranza"';
  ClavesPrimarias[1] := '"id_cobranza"';
  ClavesPrimarias[2] := '"numero_cuota"';
  ClavesPrimarias[3] := '"orden_cobranza"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TDetLiquidacionCobranza.SetIdLiquidacionCobranza(varIdLiquidacionCobranza: Integer);
begin
  IdLiquidacionCobranza := varIdLiquidacionCobranza;
end;

procedure TDetLiquidacionCobranza.SetIdCobranza(varIdCobranza: Integer);
begin
  IdCobranza := varIdCobranza;
end;

procedure TDetLiquidacionCobranza.SetOrdenCobranza(varOrdenCobranza: Integer);
begin
  OrdenCobranza := varOrdenCobranza;
end;

procedure TDetLiquidacionCobranza.SetAbonado(varAbonado: Double);
begin
  Abonado := varAbonado;
end;

procedure TDetLiquidacionCobranza.SetCodigoRetencionIva(varCodigoRetencionIva: string);
begin
  CodigoRetencionIva := varCodigoRetencionIva;
end;

procedure TDetLiquidacionCobranza.SetMontoRetencionIva(varMontoRetencionIva: Double);
begin
  MontoRetencionIva := varMontoRetencionIva;
end;

procedure TDetLiquidacionCobranza.SetNumeroCuota(varNumeroCuota: Integer);
begin
  NumeroCuota := varNumeroCuota;
end;

procedure TDetLiquidacionCobranza.SetCodigoRetencion1(varCodigoRetencion1: string);
begin
  CodigoRetencion1 := varCodigoRetencion1;
end;

procedure TDetLiquidacionCobranza.SetMontoRetencion1(varMontoRetencion1: Double);
begin
  MontoRetencion1 := varMontoRetencion1;
end;

procedure TDetLiquidacionCobranza.SetCodigoRetencion2(varCodigoRetencion2: string);
begin
  CodigoRetencion2 := varCodigoRetencion2;
end;

procedure TDetLiquidacionCobranza.SetMontoRetencion2(varMontoRetencion2: Double);
begin
  MontoRetencion2 := varMontoRetencion2;
end;

procedure TDetLiquidacionCobranza.SetCodigoRetencion3(varCodigoRetencion3: string);
begin
  CodigoRetencion3 := varCodigoRetencion3;
end;

procedure TDetLiquidacionCobranza.SetMontoRetencion3(varMontoRetencion3: Double);
begin
  MontoRetencion3 := varMontoRetencion3;
end;

procedure TDetLiquidacionCobranza.SetCodigoRetencion4(varCodigoRetencion4: string);
begin
  CodigoRetencion4 := varCodigoRetencion4;
end;

procedure TDetLiquidacionCobranza.SetMontoRetencion4(varMontoRetencion4: Double);
begin
  MontoRetencion4 := varMontoRetencion4;
end;

procedure TDetLiquidacionCobranza.SetCodigoRetencion5(varCodigoRetencion5: string);
begin
  CodigoRetencion5 := varCodigoRetencion5;
end;

procedure TDetLiquidacionCobranza.SetMontoRetencion5(varMontoRetencion5: Double);
begin
  MontoRetencion5 := varMontoRetencion5;
end;

function TDetLiquidacionCobranza.GetIdLiquidacionCobranza: Integer;
begin
  Result := IdLiquidacionCobranza;
end;

function TDetLiquidacionCobranza.GetIdCobranza: Integer;
begin
  Result := IdCobranza;
end;

function TDetLiquidacionCobranza.GetOrdenCobranza: Integer;
begin
  Result := OrdenCobranza;
end;

function TDetLiquidacionCobranza.GetAbonado: Double;
begin
  Result := Abonado;
end;

function TDetLiquidacionCobranza.GetCodigoRetencionIva: string;
begin
  Result := CodigoRetencionIva;
end;

function TDetLiquidacionCobranza.GetMontoRetencionIva: Double;
begin
  Result := MontoRetencionIva;
end;

function TDetLiquidacionCobranza.GetNumeroCuota: Integer;
begin
  Result := NumeroCuota;
end;

function TDetLiquidacionCobranza.GetCodigoRetencion1: string;
begin
  Result := CodigoRetencion1;
end;

function TDetLiquidacionCobranza.GetMontoRetencion1: Double;
begin
  Result := MontoRetencion1;
end;

function TDetLiquidacionCobranza.GetCodigoRetencion2: string;
begin
  Result := CodigoRetencion2;
end;

function TDetLiquidacionCobranza.GetMontoRetencion2: Double;
begin
  Result := MontoRetencion2;
end;

function TDetLiquidacionCobranza.GetCodigoRetencion3: string;
begin
  Result := CodigoRetencion3;
end;

function TDetLiquidacionCobranza.GetMontoRetencion3: Double;
begin
  Result := MontoRetencion3;
end;

function TDetLiquidacionCobranza.GetCodigoRetencion4: string;
begin
  Result := CodigoRetencion4;
end;

function TDetLiquidacionCobranza.GetMontoRetencion4: Double;
begin
  Result := MontoRetencion4;
end;

function TDetLiquidacionCobranza.GetCodigoRetencion5: string;
begin
  Result := CodigoRetencion5;
end;

function TDetLiquidacionCobranza.GetMontoRetencion5: Double;
begin
  Result := MontoRetencion5;
end;

function TDetLiquidacionCobranza.ObtenerIdLiquidacionCobranza(varIdLiquidacionCobranza: Integer): TClientDataSet;
begin
  Result := ObtenerListaCondicionSQL(' WHERE "id_liquidacion_cobranza" = ' + IntToStr(varIdLiquidacionCobranza));
end;

function TDetLiquidacionCobranza.EliminarIdLiquidacionCobranza(varIdLiquidacionCobranza: Integer): Boolean;
begin
  Result := EjecutarSQL('DELETE FROM ' + GetTabla + ' WHERE "id_liquidacion_cobranza" = ' + IntToStr(varIdLiquidacionCobranza));
end;

function TDetLiquidacionCobranza.VerificarRetencionesISLR(
  varIdCobranza: Integer): Boolean;
var
  Ds: TClientDataSet;
  sw: Boolean;
  Total: Double;
begin
  Ds := ObtenerListaCondicionSQL('WHERE (id_cobranza = ' + IntToStr(varIdCobranza) + ')');
  sw := False;
  if (Ds.RecordCount > 0) then
  begin
    Ds.First;
    while (not Ds.Eof) do
    begin
      Total := Ds.FieldValues['monto_retencion_1'] +
               Ds.FieldValues['monto_retencion_2'] +
               Ds.FieldValues['monto_retencion_3'] +
               Ds.FieldValues['monto_retencion_4'] +
               Ds.FieldValues['monto_retencion_5'];
      if (Total > 0) then
      begin
        sw := True;
        Break;
      end;
      Ds.Next;
    end;
  end;
  Result := sw;
end;

function TDetLiquidacionCobranza.VerificarRetencionesIVA(
  varIdCobranza: Integer): Boolean;
var
  Ds: TClientDataSet;
  sw: Boolean;
  Total: Double;
begin
  Ds := ObtenerListaCondicionSQL('WHERE (id_cobranza = ' + IntToStr(varIdCobranza) + ')');
  sw := False;
  if (Ds.RecordCount > 0) then
  begin
    Ds.First;
    while (not Ds.Eof) do
    begin
      Total := Ds.FieldValues['monto_retencion_iva'];
      if (Total > 0) then
      begin
        sw := True;
        Break;
      end;
      Ds.Next;
    end;
  end;
  Result := sw;
end;

end.
