unit ClsParametro;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TParametro }

  TParametro = class(TGenericoBD)
    private
      NombreParametro: string;
      Categoria: string;
      TipoDato: string;
      Valor: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetNombreParametro(varNombreParametro: string);
      procedure SetCategoria(varCategoria: string);
      procedure SetTipoDato(varTipoDato: string);
      procedure SetValor(varValor: string);
      function GetNombreParametro: string;
      function GetCategoria: string;
      function GetTipoDato: string;
      function GetValor: string;
      function ObtenerTotalCategorias: Integer;
      function ObtenerCategorias: TClientDataSet;
      function ObtenerValorAsString: string;
      function ObtenerValorAsInteger: Integer;
      function ObtenerValorAsDouble: Double;
      function ObtenerValorAsCurrency: Currency;
      function ObtenerValorAsDateTime: TDateTime;
      function ObtenerValorAsBoolean: Boolean;
      function ObtenerValor(varNombreParametro: string): Variant;
      function ObtenerParametros(varCategoria: string): TClientDataSet;
  end;

var
  Parametro: TParametro;

implementation

uses
  Windows;

{ TParametro }

procedure TParametro.Ajustar;
begin
  Categoria := UpperCase(Categoria);
  TipoDato := UpperCase(TipoDato);
end;

procedure TParametro.Mover(Ds: TClientDataSet);
begin
  NombreParametro := Ds.FieldValues['nombre_parametro'];
  Categoria := Ds.FieldValues['categoria'];
  TipoDato := Ds.FieldValues['tipo_dato'];
  Valor := Ds.FieldValues['valor'];
end;

function TParametro.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,4);
  Params[0] := NombreParametro;
  Params[1] := Categoria;
  Params[2] := TipoDato;
  Params[3] := Valor;
  Result := Params;
end;

function TParametro.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,3);
  Campos[0] := 'nombre_parametro';
  Campos[1] := 'categoria';
  Campos[3] := 'tipo_dato';
  Result := Campos;
end;

function TParametro.GetCondicion: string;
begin
  Result := '"nombre_parametro" = ' + QuotedStr(NombreParametro);
end;

procedure TParametro.AntesEliminar;
begin

end;

procedure TParametro.AntesInsertar;
begin

end;

procedure TParametro.AntesModificar;
begin

end;

procedure TParametro.DespuesEliminar;
begin

end;

procedure TParametro.DespuesInsertar;
begin

end;

procedure TParametro.DespuesModificar;
begin

end;

constructor TParametro.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('sys_parametros');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(4)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "categoria"=$2, "tipo_dato"=$3, "valor"=$4 WHERE ("nombre_parametro"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("nombre_parametro"=$1)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"nombre_parametro"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TParametro.SetNombreParametro(varNombreParametro: string);
begin
  NombreParametro := varNombreParametro;
end;

procedure TParametro.SetCategoria(varCategoria: string);
begin
  Categoria := varCategoria;
end;

procedure TParametro.SetTipoDato(varTipoDato: string);
begin
  TipoDato := varTipoDato;
end;

procedure TParametro.SetValor(varValor: string);
begin
  Valor := varValor;
end;

function TParametro.GetNombreParametro: string;
begin
  Result := NombreParametro;
end;

function TParametro.GetCategoria: string;
begin
  Result := Categoria;
end;

function TParametro.GetTipoDato: string;
begin
  Result := TipoDato;
end;

function TParametro.GetValor: string;
begin
  Result := Valor;
end;

function TParametro.ObtenerTotalCategorias: Integer;
var
  Ds: TClientDataSet;
begin
  Ds := BD.EjecutarQueryCliDs('SELECT DISTINCT ON ("categoria") "categoria" FROM ' + GetTabla);
  Result := Ds.RecordCount;
end;

function TParametro.ObtenerCategorias: TClientDataSet;
begin
  Result := BD.EjecutarQueryCliDs('SELECT DISTINCT ON ("categoria") "categoria" FROM ' + GetTabla);
end;

function TParametro.ObtenerValorAsString: string;
begin
  Result := Valor;
end;

function TParametro.ObtenerValorAsInteger: Integer;
begin
  Result := StrToInt(Valor);
end;

function TParametro.ObtenerValorAsDouble: Double;
begin
  Result := StrToFloat(Valor);
end;

function TParametro.ObtenerValorAsCurrency: Currency;
begin
  Result := StrToCurr(Valor);
end;

function TParametro.ObtenerValorAsDateTime: TDateTime;
begin
  Result := StrToDate(Valor);
end;

function TParametro.ObtenerValorAsBoolean: Boolean;
begin
  Result := StrToBool(Valor);
end;

function TParametro.ObtenerValor(varNombreParametro: string): Variant;
begin
  SetNombreParametro(varNombreParametro);
  Result := '';
  if (Buscar) then
  begin
    if (GetTipoDato = 'STRING') then
    begin
      Result := ObtenerValorAsString;
    end;
    if (GetTipoDato = 'INTEGER') then
    begin
      Result := ObtenerValorAsInteger;
    end;
    if (GetTipoDato = 'DOUBLE') then
    begin
      Result := ObtenerValorAsDouble;
    end;
    if (GetTipoDato = 'CURRENCY') then
    begin
      Result := ObtenerValorAsCurrency;
    end;
    if (GetTipoDato = 'DATETIME') then
    begin
      Result := ObtenerValorAsDateTime;
    end;
    if (GetTipoDato = 'BOOLEAN') then
    begin
      Result := ObtenerValorAsBoolean;
    end;
  end
  else
  begin
    MessageBox(0, PChar('El Parámetro "' + varNombreParametro + '" No Fue Encontrado!!! Revise la configuración del Sistema!!!'+#13+#10+''+#13+#10+'Para Mas Información Consulte Con Servicio Técnico.'), PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end;
end;

function TParametro.ObtenerParametros(varCategoria: string): TClientDataSet;
begin
  Result := BD.EjecutarQueryCliDs('SELECT "nombre_parametro", "tipo_dato", "valor" FROM ' + GetTabla + ' WHERE ("categoria"= ' + QuotedStr(varCategoria) + ');');
end;

end.
