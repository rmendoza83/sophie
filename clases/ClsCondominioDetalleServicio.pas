unit ClsCondominioDetalleServicio;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TCondominioDetalleServicio }

  TCondominioDetalleServicio = class(TGenericoBD)
    private
      CodigoProducto: string;
      Referencia: string;
      Descripcion: string;
      Cantidad: Double;
      Precio: Double;
      PDescuento: Double;
      MontoDescuento: Double;
      Impuesto: Double;
      PIva: Double;
      Total: Double;
      IdCondominioServicio: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigoProducto(varCodigoProducto: string);
      procedure SetReferencia(varReferencia: string);
      procedure SetDescripcion(varDescripcion: string);
      procedure SetCantidad(varCantidad: Double);
      procedure SetPrecio(varPrecio: Double);
      procedure SetPDescuento(varPDescuento: Double);
      procedure SetMontoDescuento(varMontoDescuento: Double);
      procedure SetImpuesto(varImpuesto: Double);
      procedure SetPIva(varPIva: Double);
      procedure SetTotal(varTotal: Double);
      procedure SetIdCondominioServicio(varIdCondominioServicio: Integer);
      function GetCodigoProducto: string;
      function GetReferencia: string;
      function GetDescripcion: string;
      function GetCantidad: Double;
      function GetPrecio: Double;
      function GetPDescuento: Double;
      function GetMontoDescuento: Double;
      function GetImpuesto: Double;
      function GetPIva: Double;
      function GetTotal: Double;
      function GetIdCondominioServicio: Integer;
      function ObtenerIdCondominioServicio(varIdCondominioServicio: Integer): TClientDataSet;
      function EliminarIdCondominioServicio(varIdCondominioServicio: Integer): Boolean;
  end;

implementation

uses DB;

{ TCondominioDetalleServicio }

procedure TCondominioDetalleServicio.Ajustar;
begin
  inherited;

end;

procedure TCondominioDetalleServicio.Mover(Ds: TClientDataSet);
begin
  CodigoProducto := Ds.FieldValues['codigo_producto'];
  Referencia := Ds.FieldValues['referencia'];
  Descripcion := Ds.FieldValues['descripcion'];
  Cantidad := Ds.FieldValues['cantidad'];
  Precio := Ds.FieldValues['precio'];
  PDescuento := Ds.FieldValues['p_descuento'];
  MontoDescuento := Ds.FieldValues['monto_descuento'];
  Impuesto := Ds.FieldValues['impuesto'];
  PIva := Ds.FieldValues['p_iva'];
  Total := Ds.FieldValues['total'];
  IdCondominioServicio := Ds.FieldValues['id_condominio_servicio'];
end;

function TCondominioDetalleServicio.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,11);
  Params[0] := CodigoProducto;
  Params[1] := Referencia;
  Params[2] := Descripcion;
  Params[3] := Cantidad;
  Params[4] := Precio;
  Params[5] := PDescuento;
  Params[6] := MontoDescuento;
  Params[7] := Impuesto;
  Params[8] := PIva;
  Params[9] := Total;
  Params[10] := IdCondominioServicio;
  Result:= Params;
end;

function TCondominioDetalleServicio.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,3);
  Campos[0] := 'codigo_producto';
  Campos[1] := 'referencia';
  Campos[2] := 'descripcion';
  Result:= Campos;
end;

function TCondominioDetalleServicio.GetCondicion: string;
begin
  Result := '"id_condominio_servicio" =' + IntToStr(IdCondominioServicio) + ' AND ' +
            '"codigo_producto" =' + QuotedStr(CodigoProducto);
end;

procedure TCondominioDetalleServicio.AntesEliminar;
begin

end;

procedure TCondominioDetalleServicio.AntesInsertar;
begin

end;

procedure TCondominioDetalleServicio.AntesModificar;
begin

end;

procedure TCondominioDetalleServicio.DespuesEliminar;
begin

end;

procedure TCondominioDetalleServicio.DespuesInsertar;
begin

end;

procedure TCondominioDetalleServicio.DespuesModificar;
begin

end;

constructor TCondominioDetalleServicio.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_condominio_det_servicios');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(11)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "referencia"=$2, "descripcion"=$3, "cantidad"=$4, "precio"=$5, "p_descuento"=$6, "monto_descuento"=$7, "impuesto"=$8, "p_iva"=$9, "total"=$10 ' +
                  'WHERE ("id_condominio_servicio"=$11) AND ("codigo_producto"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("id_condominio_servicio"=$11) AND ("codigo_producto"=$1)');
  SetLength(ClavesPrimarias,2);
  ClavesPrimarias[0] := '"id_condominio_servicio"';
  ClavesPrimarias[1] := '"codigo_producto"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TCondominioDetalleServicio.SetCodigoProducto(varCodigoProducto: string);
begin
  CodigoProducto := varCodigoProducto;
end;

procedure TCondominioDetalleServicio.SetReferencia(varReferencia: string);
begin
  Referencia := varReferencia;
end;

procedure TCondominioDetalleServicio.SetDescripcion(varDescripcion: string);
begin
  Descripcion := varDescripcion;
end;

procedure TCondominioDetalleServicio.SetCantidad(varCantidad: Double);
begin
  Cantidad := varCantidad;
end;

procedure TCondominioDetalleServicio.SetPrecio(varPrecio: Double);
begin
  Precio := varPrecio
end;

procedure TCondominioDetalleServicio.SetPDescuento(varPDescuento: Double);
begin
  PDescuento := varPDescuento;
end;

procedure TCondominioDetalleServicio.SetMontoDescuento(varMontoDescuento: Double);
begin
  MontoDescuento := varMontoDescuento;
end;

procedure TCondominioDetalleServicio.SetImpuesto(varImpuesto: Double);
begin
  Impuesto := varImpuesto;
end;

procedure TCondominioDetalleServicio.SetPIva(varPIva: Double);
begin
  PIva := varPIva;
end;

procedure TCondominioDetalleServicio.SetTotal(varTotal: Double);
begin
  Total := varTotal;
end;

procedure TCondominioDetalleServicio.SetIdCondominioServicio(varIdCondominioServicio: Integer);
begin
  IdCondominioServicio := varIdCondominioServicio;
end;

function TCondominioDetalleServicio.GetCodigoProducto: string;
begin
  Result := CodigoProducto;
end;

function TCondominioDetalleServicio.GetReferencia: string;
begin
  Result := Referencia;
end;

function TCondominioDetalleServicio.GetDescripcion: string;
begin
  Result := Descripcion;
end;

function TCondominioDetalleServicio.GetCantidad: Double;
begin
  Result := Cantidad;
end;

function TCondominioDetalleServicio.GetPrecio: Double;
begin
  Result := Precio;
end;

function TCondominioDetalleServicio.GetPDescuento: Double;
begin
  Result := PDescuento;
end;

function TCondominioDetalleServicio.GetMontoDescuento: Double;
begin
  Result := MontoDescuento;
end;

function TCondominioDetalleServicio.GetImpuesto: Double;
begin
  Result := Impuesto;
end;

function TCondominioDetalleServicio.GetPIva: Double;
begin
  Result := PIva;
end;

function TCondominioDetalleServicio.GetTotal: Double;
begin
  Result := Total;
end;

function TCondominioDetalleServicio.GetIdCondominioServicio: Integer;
begin
  Result := IdCondominioServicio;
end;

function TCondominioDetalleServicio.ObtenerIdCondominioServicio(
  varIdCondominioServicio: Integer): TClientDataSet;
begin
  Result := ObtenerListaCondicionSQL(' WHERE "id_condominio_servicio" = ' + IntToStr(varIdCondominioServicio));
end;

function TCondominioDetalleServicio.EliminarIdCondominioServicio(
  varIdCondominioServicio: Integer): Boolean;
begin
  Result := EjecutarSQL('DELETE FROM ' + GetTabla + ' WHERE "id_condominio_servicio" = ' + IntToStr(varIdCondominioServicio));
end;

end.
