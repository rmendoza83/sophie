unit ClsEstacion;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TEstacion }

  TEstacion = class(TGenericoBD)
    private
      Codigo: string;
      Descripcion: string;
      Mantenimiento: Boolean;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigo(varCodigo: string);
      procedure SetDescripcion(varDescripcion: string);
      procedure SetMantenimiento(varMantenimiento: Boolean);
      function GetCodigo: string;
      function GetDescripcion: string;
      function GetMantenimiento: Boolean;
      function ObtenerCombo: TClientDataSet;
  end;

implementation

{ TEstacion }

procedure TEstacion.Ajustar;
begin

end;

procedure TEstacion.Mover(Ds: TClientDataSet);
begin
  Codigo := Ds.FieldValues['codigo'];
  Descripcion := Ds.FieldValues['descripcion'];
  Mantenimiento := Ds.FieldValues['mantenimiento'];
end;

function TEstacion.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,3);
  Params[0] := Codigo;
  Params[1] := Descripcion;
  Params[2] := Mantenimiento;
  Result := Params;
end;

function TEstacion.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,2);
  Campos[0] := 'codigo';
  Campos[1] := 'descripcion';
  Result := Campos;
end;

function TEstacion.GetCondicion: string;
begin
  Result := '"codigo" = ' + QuotedStr(Codigo);
end;

procedure TEstacion.AntesEliminar;
begin
  inherited;

end;

procedure TEstacion.AntesInsertar;
begin

end;

procedure TEstacion.AntesModificar;
begin

end;

procedure TEstacion.DespuesEliminar;
begin

end;

procedure TEstacion.DespuesInsertar;
begin

end;

procedure TEstacion.DespuesModificar;
begin

end;

constructor TEstacion.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_estaciones');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(3)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "descripcion"=$2,"mantenimiento"=$3 WHERE ("codigo"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("codigo"=$1)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"codigo"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TEstacion.SetCodigo(varCodigo: string);
begin
  Codigo := varCodigo;
end;

procedure TEstacion.SetDescripcion(varDescripcion: string);
begin
  Descripcion := varDescripcion;
end;

procedure TEstacion.SetMantenimiento(varMantenimiento: Boolean);
begin
  Mantenimiento := varMantenimiento;
end;

function TEstacion.GetCodigo: string;
begin
  Result := Codigo;
end;

function TEstacion.GetDescripcion: string;
begin
  Result := Descripcion;
end;

function TEstacion.GetMantenimiento: Boolean;
begin
  Result := Mantenimiento;
end;

function TEstacion.ObtenerCombo: TClientDataSet;
begin
  Result := BD.EjecutarQueryCliDs('SELECT "codigo", "descripcion" FROM ' + GetTabla);
end;

end.
