unit ClsRetencionIslr;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TRetencionIslr }

  TRetencionIslr = class(TGenericoBD)
    private
      Numero: string;
      CodigoProveedor: string;
      CodigoZona: string;
      FechaIngreso: TDate;
      FechaContable: TDate;
      Total: Double;
      NumeroComprobante: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArraystring; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetNumero(varNumero: string);
      procedure SetCodigoProveedor(varCodigoProveedor: string);
      procedure SetCodigoZona(varCodigoZona: string);
      procedure SetFechaIngreso(varFechaIngreso: TDate);
      procedure SetFechaContable(varFechaContable: TDate);
      procedure SetTotal(varTotal: Double);
      procedure SetNumeroComprobante(varNumeroComoprante: string);
      function GetNumero: string;
      function GetCodigoProveedor: string;
      function GetCodigoZona: string;
      function GetFechaIngreso: TDate;
      function GetFechaContable: TDate;
      function GetTotal: Double;
      function GetNumeroComprobante: string;
      function BuscarNumero: Boolean;
  end;

implementation

uses
  DB,
  ClsConsecutivo;

{ TRetencionIslr }

procedure TRetencionIslr.Ajustar;
begin

end;

procedure TRetencionIslr.Mover(Ds: TClientDataSet);
begin
  Numero := Ds.FieldValues['numero'];
  CodigoProveedor := Ds.FieldValues['codigo_proveedor'];
  CodigoZona := Ds.FieldValues['codigo_zona'];
  FechaIngreso := Ds.FieldValues['fecha_ingreso'];
  FechaContable := Ds.FieldValues['fecha_contable'];
  Total := Ds.FieldValues['total'];
  NumeroComprobante := Ds.FieldValues['numero_comprobante'];
end;

function TRetencionIslr.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,7);
  Params[0] := Numero;;
  Params[1] := CodigoProveedor;
  Params[2] := CodigoZona;
  Params[3] := VarFromDateTime(FechaIngreso);
  Params[4] := VarFromDateTime(FechaContable);
  Params[5] := Total;
  Params[6] := NumeroComprobante;
  Result := Params;
end;

function TRetencionIslr.GetCampos: TArraystring;
var
  Campos: TArraystring;
begin
  SetLength(Campos,4);
  Campos[0] := 'numero';
  Campos[1] := 'codigo_proveedor';
  Campos[2] := 'codigo_zona';
  Campos[3] := 'numero_comprobante';
  Result := Campos;
end;

function TRetencionIslr.GetCondicion: string;
begin
  Result:= '"numero" = ' + QuotedStr(Numero);
end;

procedure TRetencionIslr.AntesInsertar;
var
  Consecutivo: TConsecutivo;
begin
  Consecutivo := TConsecutivo.Create;
  Numero := IntToStr(Consecutivo.ObtenerConsecutivo('numero_retencion_islr'));
  NumeroComprobante := FormatDateTime('YYYY-mm-',FechaContable) + FormatFloat('00000000',Consecutivo.ObtenerConsecutivo('numero_comprobante_retencion_islr'));
end;

procedure TRetencionIslr.AntesModificar;
begin

end;

procedure TRetencionIslr.AntesEliminar;
begin

end;

procedure TRetencionIslr.DespuesInsertar;
begin

end;

procedure TRetencionIslr.DespuesModificar;
begin

end;

procedure TRetencionIslr.DespuesEliminar;
begin

end;

constructor TRetencionIslr.Create;
var
  ClavesPrimarias: TArraystring;
begin
  inherited Create;
  SetTabla('tbl_retenciones_islr');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(7)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "codigo_proveedor"=$2, "codigo_zona"=$3, "fecha_ingreso"=$4, "fecha_contable"=$5, "total"=$6, "numero_comprobante"=$7 ' +
                  ' WHERE ("numero"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("numero"=$1)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"numero"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TRetencionIslr.SetNumero(varNumero: string);
begin
  Numero := varNumero;
end;

procedure TRetencionIslr.SetNumeroComprobante(varNumeroComoprante: string);
begin
  NumeroComprobante := varNumeroComoprante;
end;

procedure TRetencionIslr.SetCodigoProveedor(varCodigoProveedor: string);
begin
  CodigoProveedor := varCodigoProveedor;
end;

procedure TRetencionIslr.SetCodigoZona(varCodigoZona: string);
begin
  CodigoZona := varCodigoZona;
end;

procedure TRetencionIslr.SetFechaIngreso(varFechaIngreso: TDate);
begin
  FechaIngreso := varFechaIngreso;
end;

procedure TRetencionIslr.SetFechaContable(varFechaContable: TDate);
begin
  FechaContable := varFechaContable;
end;

procedure TRetencionIslr.SetTotal(varTotal: Double);
begin
  Total := varTotal;
end;

function TRetencionIslr.GetNumero: string;
begin
  Result := Numero;
end;

function TRetencionIslr.GetNumeroComprobante: string;
begin
  Result := NumeroComprobante;
end;

function TRetencionIslr.GetCodigoProveedor: string;
begin
  Result := CodigoProveedor;
end;

function TRetencionIslr.GetCodigoZona: string;
begin
  Result := CodigoZona;
end;

function TRetencionIslr.GetFechaIngreso: TDate;
begin
  Result := FechaIngreso;
end;

function TRetencionIslr.GetFechaContable: TDate;
begin
  Result := FechaContable;
end;

function TRetencionIslr.GetTotal: Double;
begin
  Result := Total;
end;

function TRetencionIslr.BuscarNumero: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("numero" = ' + QuotedStr(GetNumero) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

end.
