unit ClsDetalleContadoPago;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TDetalleContadoPago }

  TDetalleContadoPago = class(TGenericoBD)
    private
      IdContadoPago: Integer;
      CodigoFormaPago: string;
      Monto: Double;
      Desde: string;
      CodigoBanco: String;
      NumeroDocumento: string;
      NumeroAuxiliar: string;
      Referencia: string;
      Fecha: TDate;
      LoginUsuario: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetIdContadoPago(varIdContadoPago: Integer);
      procedure SetCodigoFormaPago(varCodigoFormaPago: string);
      procedure SetMonto(varMonto: Double);
      procedure SetDesde(varDesde: string);
      procedure SetCodigoBanco(varCodigoBanco: string);
      procedure SetNumeroDocumento(varNumeroDocumento: string);
      procedure SetNumeroAuxiliar(varNumeroAuxiliar: string);
      procedure SetReferencia(varReferencia: string);
      procedure SetFecha(varFecha: TDate);
      procedure SetLoginUsuario(varLoginUsuario: string);
      function GetIdContadoPago: Integer;
      function GetCodigoFormaPago: string;
      function GetMonto: Double;
      function GetDesde: string;
      function GetCodigoBanco: string;
      function GetNumeroDocumento: string;
      function GetNumeroAuxiliar: string;
      function GetReferencia: string;
      function GetFecha: TDate;
      function GetLoginUsuario: string;
      function EliminarIdContadoPago(varIdContadoPago: Integer): Boolean;
  end;

implementation

uses DB;

{ TDetalleContadoPago }

procedure TDetalleContadoPago.Ajustar;
begin

end;

procedure TDetalleContadoPago.Mover(Ds: TClientDataSet);
begin
  IdContadoPago := Ds.FieldValues['id_contado_pago'];
  CodigoFormaPago := Ds.FieldValues['codigo_forma_pago'];
  Monto := Ds.FieldValues['monto'];
  Desde := Ds.FieldValues['desde'];
  CodigoBanco := Ds.FieldValues['codigo_banco'];
  NumeroDocumento := Ds.FieldValues['numero_documento'];
  NumeroAuxiliar := Ds.FieldValues['numero_auxiliar'];
  Referencia := Ds.FieldValues['referencia'];
  Fecha := Ds.FieldByName('fecha').AsDateTime ;
  LoginUsuario := Ds.FieldValues['login_usuario'];
end;

function TDetalleContadoPago.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,10);
  Params[0] := IdContadoPago;
  Params[1] := CodigoFormaPago;
  Params[2] := Monto;
  Params[3] := Desde;
  Params[4] := CodigoBanco;
  Params[5] := NumeroDocumento;
  Params[6] := NumeroAuxiliar;
  Params[7] := Referencia;
  Params[8] := VarToDateTime(Fecha);
  Params[9] := LoginUsuario;
  result := Params;
end;

function TDetalleContadoPago.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,7);
  Campos[0] := 'codigo_forma_pago';
  Campos[1] := 'desde';
  Campos[2] := 'codigo_banco';
  Campos[3] := 'numero_documento';
  Campos[4] := 'numero_auxiliar';
  Campos[5] := 'referencia';
  Campos[6] := 'login_usuario';
  Result := Campos;
end;

function TDetalleContadoPago.GetCondicion: string;
begin
  Result := '"id_contado_pago" = ' + IntToStr(IdContadoPago) + ' AND ' +
            '"codigo_forma_pago" =' + QuotedStr(CodigoFormaPago);
end;

procedure TDetalleContadoPago.AntesInsertar;
begin

end;

procedure TDetalleContadoPago.AntesModificar;
begin

end;

procedure TDetalleContadoPago.AntesEliminar;
begin

end;

procedure TDetalleContadoPago.DespuesInsertar;
begin

end;

procedure TDetalleContadoPago.DespuesModificar;
begin

end;

procedure TDetalleContadoPago.DespuesEliminar;
begin

end;

constructor TDetalleContadoPago.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_det_contado_pagos');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(10)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "monto"=$3,"desde"=$4,"codigo_banco"=$5,"numero_documento"=$6,"numero_auxiliar"=$7,"referencia"=$8,"fecha"=$9,"login_usuario"=$10 WHERE ("id_contado_pago"=$1) AND ("codigo_forma_pago"=$2)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("id_contado_cobranza"=$1) AND ("codigo_forma_pago"=$2)');
  SetLength(ClavesPrimarias,2);
  ClavesPrimarias[0]:='"id_contado_pago"';
  ClavesPrimarias[1]:='"codigo_forma_pago"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TDetalleContadoPago.SetIdContadoPago(varIdContadoPago: Integer);
begin
  IdContadoPago := varIdContadoPago;
end;

procedure TDetalleContadoPago.SetCodigoBanco(varCodigoBanco: string);
begin
  CodigoBanco := varCodigoBanco;
end;

procedure TDetalleContadoPago.SetCodigoFormapago(varCodigoFormaPago: string);
begin
  CodigoFormaPago := varCodigoFormaPago;
end;

procedure TDetalleContadoPago.SetFecha(varFecha: TDate);
begin
  Fecha := varFecha;
end;

procedure TDetalleContadoPago.SetMonto(varMonto: Double);
begin
  Monto := varMonto;
end;

procedure TDetalleContadoPago.SetDesde(varDesde: string);
begin
  Desde := varDesde;
end;

procedure TDetalleContadoPago.SetNumeroAuxiliar(varNumeroAuxiliar: string);
begin
  NumeroAuxiliar := varNumeroAuxiliar;
end;

procedure TDetalleContadoPago.SetNumeroDocumento(varNumeroDocumento: string);
begin
  NumeroDocumento := varNumeroDocumento;
end;

procedure TDetalleContadoPago.SetReferencia(varReferencia: string);
begin
  Referencia := varReferencia;
end;

procedure TDetalleContadoPago.SetLoginUsuario(varLoginUsuario: string);
begin
  LoginUsuario := varLoginUsuario;
end;

function TDetalleContadoPago.GetCodigoBanco: string;
begin
  Result := CodigoBanco;
end;

function TDetalleContadoPago.GetCodigoFormaPago: string;
begin
  Result := CodigoFormaPago;
end;

function TDetalleContadoPago.GetFecha: TDate;
begin
  Result := Fecha;
end;

function TDetalleContadoPago.GetIdContadoPago: Integer;
begin
  Result := IdContadoPago;
end;

function TDetalleContadoPago.GetMonto: Double;
begin
  Result := Monto;
end;

function TDetalleContadoPago.GetDesde: string;
begin
  Result := Desde;
end;

function TDetalleContadoPago.GetNumeroAuxiliar: string;
begin
  Result := NumeroAuxiliar;
end;

function TDetalleContadoPago.GetNumeroDocumento: string;
begin
  Result := NumeroDocumento;
end;

function TDetalleContadoPago.GetReferencia: string;
begin
  Result := Referencia;
end;

function TDetalleContadoPago.GetLoginUsuario: string;
begin
  Result := LoginUsuario;
end;

function TDetalleContadoPago.EliminarIdContadoPago(
  varIdContadoPago: Integer): Boolean;
begin
  Result := EjecutarSQL('DELETE FROM ' + GetTabla + ' WHERE "id_contado_pago" = ' + IntToStr(varIdContadoPago));
end;

end.
