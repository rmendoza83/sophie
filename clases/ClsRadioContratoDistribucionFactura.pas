unit ClsRadioContratoDistribucionFactura;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TRadioContratoDistribucionFactura}

  TRadioContratoDistribucionFactura = class(TGenericoBD)
    private
      IdRadioContrato: Integer;
      Item: Integer;
      TotalItems: Integer;
      FechaIngreso: TDate;
      Total: Double;
      FechaInicial: TDate;
      FechaFinal: TDate;
      Observaciones: string;
      IdFacturacionCliente: Integer;
      NumeroDocumento: string;
      Facturado: Boolean;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetIdRadioContrato(varIdRadioContrato: Integer);
      procedure SetItem(varItem: Integer);
      function GetIdRadioContrato: Integer;
      function GetItem: Integer;
      function GetTotalItems: Integer;
      function GetFechaIngreso: TDate;
      function GetTotal: Double;
      function GetFechaInicial: TDate;
      function GetFechaFinal: TDate;
      function GetObservaciones: string;
      function GetIdFacturacionCliente: Integer;
      function GetNumeroDocumento: string;
      function GetFacturado: Boolean;
      function ObtenerIdRadioContrato(varIdRadioContrato: Integer): TClientDataSet;
  end;

implementation

{ TRadioContratoDistribucionFactura }

procedure TRadioContratoDistribucionFactura.Ajustar;
begin

end;

procedure TRadioContratoDistribucionFactura.Mover(Ds: TClientDataSet);
begin
  IdRadioContrato := Ds.FieldValues['id_radio_contrato'];
  Item := Ds.FieldValues['item'];
  TotalItems := Ds.FieldValues['total_items'];
  FechaIngreso := Ds.FieldValues['fecha_ingreso'];
  Total := Ds.FieldValues['total'];
  FechaInicial := Ds.FieldValues['fecha_inicial'];
  FechaFinal := Ds.FieldValues['fecha_final'];
  Observaciones := Ds.FieldValues['observaciones'];
  IdFacturacionCliente := Ds.FieldValues['id_facturacion_cliente'];
  NumeroDocumento := Ds.FieldValues['numero_documento'];
  Facturado := Ds.FieldValues['facturado'];
end;

function TRadioContratoDistribucionFactura.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,11);
  Params[0] := IdRadioContrato;
  Params[1] := Item;
  Params[2] := TotalItems;
  Params[3] := VarFromDateTime(FechaIngreso);
  Params[4] := Total;
  Params[5] := VarFromDateTime(FechaInicial);
  Params[6] := VarFromDateTime(FechaFinal);
  Params[7] := Observaciones;
  Params[8] := IdFacturacionCliente;
  Params[9] := NumeroDocumento;
  Params[10] := Facturado;
  Result := Params;
end;

function TRadioContratoDistribucionFactura.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,2);
  Campos[0] := 'observaciones';
  Campos[1] := 'numero_documento';
  Result := Campos;
end;

function TRadioContratoDistribucionFactura.GetCondicion: string;
begin
  Result := '("id_radio_contrato" = ' + IntToStr(IdRadioContrato) + ') AND ' +
            '("item" = ' + IntToStr(Item) + ')';
end;

procedure TRadioContratoDistribucionFactura.AntesInsertar;
begin

end;

procedure TRadioContratoDistribucionFactura.AntesModificar;
begin

end;

procedure TRadioContratoDistribucionFactura.AntesEliminar;
begin

end;

procedure TRadioContratoDistribucionFactura.DespuesInsertar;
begin

end;

procedure TRadioContratoDistribucionFactura.DespuesModificar;
begin

end;

procedure TRadioContratoDistribucionFactura.DespuesEliminar;
begin

end;

constructor TRadioContratoDistribucionFactura.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('v_radio_contratos_distribucion_facturas');
  SetLength(ClavesPrimarias,2);
  ClavesPrimarias[0] := '"id_radio_contrato"';
  ClavesPrimarias[1] := '"item"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TRadioContratoDistribucionFactura.SetIdRadioContrato(varIdRadioContrato: Integer);
begin
  IdRadioContrato := varIdRadioContrato;
end;

procedure TRadioContratoDistribucionFactura.SetItem(varItem: Integer);
begin
  Item := varItem;
end;

function TRadioContratoDistribucionFactura.GetIdRadioContrato: Integer;
begin
  Result := IdRadioContrato;
end;

function TRadioContratoDistribucionFactura.GetItem: Integer;
begin
  Result := Item;
end;

function TRadioContratoDistribucionFactura.GetTotalItems: Integer;
begin
  Result := TotalItems;
end;

function TRadioContratoDistribucionFactura.GetFechaIngreso: TDate;
begin
  Result := FechaIngreso;
end;

function TRadioContratoDistribucionFactura.GetTotal: Double;
begin
  Result := Total;
end;

function TRadioContratoDistribucionFactura.GetFechaInicial: TDate;
begin
  Result := FechaInicial;
end;

function TRadioContratoDistribucionFactura.GetFechaFinal: TDate;
begin
  Result := FechaFinal;
end;

function TRadioContratoDistribucionFactura.GetObservaciones: string;
begin
  Result := Observaciones;
end;

function TRadioContratoDistribucionFactura.GetIdFacturacionCliente: Integer;
begin
  Result := IdFacturacionCliente;
end;

function TRadioContratoDistribucionFactura.GetNumeroDocumento: string;
begin
  Result := NumeroDocumento;
end;

function TRadioContratoDistribucionFactura.GetFacturado: Boolean;
begin
  Result := Facturado;
end;

function TRadioContratoDistribucionFactura.ObtenerIdRadioContrato(varIdRadioContrato: Integer): TClientDataSet;
begin
  Result := ObtenerListaCondicionSQL(' WHERE "id_radio_contrato" = ' + IntToStr(varIdRadioContrato));
end;

end.
