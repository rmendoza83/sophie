unit ClsReporteDataView;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TReporteDataView }

  TReporteDataView = class(TGenericoBD)
    private
      NombreReporte: string;
      NombreDataview: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetNombreReporte(varNombreReporte: string);
      procedure SetNombreDataview(varNombreDataview: string);
      function GetNombreReporte: string;
      function GetNombreDataview: string;
      function BuscarNombreReporte: Boolean;
  end;

implementation

{ TReporteDataView }

procedure TReporteDataView.Ajustar;
begin

end;

procedure TReporteDataView.Mover(Ds: TClientDataSet);
begin
  NombreReporte := Ds.FieldValues['nombre_reporte'];
  NombreDataview := Ds.FieldValues['nombre_dataview'];
end;

function TReporteDataView.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,2);
  Params[0] := NombreReporte;
  Params[1] := NombreDataview;
  Result := Params;
end;

function TReporteDataView.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,2);
  Campos[0] := 'nombre_reporte';
  Campos[1] := 'nombre_dataview';
  Result := Campos;
end;

procedure TReporteDataView.AntesInsertar;
begin

end;

procedure TReporteDataView.AntesModificar;
begin

end;

procedure TReporteDataView.AntesEliminar;
begin

end;

procedure TReporteDataView.DespuesInsertar;
begin

end;

procedure TReporteDataView.DespuesModificar;
begin

end;

procedure TReporteDataView.DespuesEliminar;
begin

end;

constructor TReporteDataView.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('sys_reportes_dataviews');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(2)) + ')');
  SetStrSQLUpdate('');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("nombre_reporte"=$1) AND ("nombre_dataview"=$2)');
  SetLength(ClavesPrimarias,2);
  ClavesPrimarias[0] := '"nombre_reporte"';
  ClavesPrimarias[1] := '"nombre_dataview"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

function TReporteDataView.GetCondicion: string;
begin
  Result := '("nombre_reporte" = ' + QuotedStr(NombreReporte) + ') AND ' +
            '("nombre_dataview" = ' + QuotedStr(NombreDataview) + ')';
end;

function TReporteDataView.GetNombreDataview: string;
begin
  Result := NombreDataview;
end;

function TReporteDataView.GetNombreReporte: string;
begin
  Result := NombreReporte;
end;

procedure TReporteDataView.SetNombreDataview(varNombreDataview: string);
begin
  NombreDataview := varNombreDataview;
end;

procedure TReporteDataView.SetNombreReporte(varNombreReporte: string);
begin
  NombreReporte := varNombreReporte;
end;

function TReporteDataView.BuscarNombreReporte: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("nombre_reporte" = ' + QuotedStr(GetNombreReporte) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

end.
