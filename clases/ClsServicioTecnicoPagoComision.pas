unit ClsServicioTecnicoPagoComision;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TServicioTecnicoPagoComision }

  TServicioTecnicoPagoComision = class(TGenericoBD)
    private
      Numero: string;
      CodigoTecnico: string;
      CodigoZona: string;
      CodigoMoneda: string;
      FechaIngreso: TDate;
      FechaPago: TDate;
      FechaInicial: TDate;
      FechaFinal: TDate;
      Total: Double;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetNumero(varNumero: string);
      procedure SetCodigoTecnico(varCodigoTecnico: string);
      procedure SetCodigoZona(varCodigoZona: string);
      procedure SetCodigoMoneda(varCodigoMoneda: string);
      procedure SetFechaIngreso(varFechaIngreso: TDate);
      procedure SetFechaPago(varFechaPago: TDate);
      procedure SetFechaInicial(varFechaInicial: TDate);
      procedure SetFechaFinal(varFechaFinal: TDate);
      procedure SetTotal(varTotal: Double);
      function GetNumero: string;
      function GetCodigoTecnico: string;
      function GetCodigoZona: string;
      function GetCodigoMoneda: string;
      function GetFechaIngreso: TDate;
      function GetFechaPago: TDate;
      function GetFechaInicial: TDate;
      function GetFechaFinal: TDate;
      function GetTotal: Double;
  end;

implementation

uses
  DB,
  ClsConsecutivo;

{ TServicioTecnicoPagoComision }

procedure TServicioTecnicoPagoComision.Ajustar;
begin

end;

procedure TServicioTecnicoPagoComision.Mover(Ds: TClientDataSet);
begin
  Numero := Ds.FieldValues['numero'];
  CodigoTecnico := Ds.FieldValues['codigo_tecnico'];
  CodigoZona:= Ds.FieldValues['codigo_zona'];
  CodigoMoneda:= Ds.FieldValues['codigo_moneda'];
  FechaIngreso := Ds.FieldByName('fecha_ingreso').AsDateTime;
  FechaPago := Ds.FieldByName('fecha_pago').AsDateTime;
  FechaInicial := Ds.FieldByName('fecha_inicial').AsDateTime;
  FechaFinal := Ds.FieldByName('fecha_final').AsDateTime;
  Total:= Ds.FieldValues['total'];
end;

function TServicioTecnicoPagoComision.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,9);
  Params[0] := Numero;
  Params[1] := CodigoTecnico;
  Params[2] := CodigoZona;
  Params[3] := CodigoMoneda;
  Params[4] := VarFromDateTime(FechaIngreso);
  Params[5] := VarFromDateTime(FechaPago);
  Params[6] := VarFromDateTime(FechaInicial);
  Params[7] := VarFromDateTime(FechaFinal);
  Params[8] := Total;
  Result := Params;
end;

function TServicioTecnicoPagoComision.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,3);
  Campos[0] := 'codigo_tecnico';
  Campos[1] := 'codigo_zona';
  Campos[2] := 'codigo_moneda';
  Result := Campos;
end;

function TServicioTecnicoPagoComision.GetCondicion: string;
begin
  Result := '"numero" = ' + QuotedStr(Numero) ;
end;

procedure TServicioTecnicoPagoComision.AntesInsertar;
var
  Consecutivo: TConsecutivo;
begin
  Consecutivo := TConsecutivo.Create;
  Numero := IntToStr(Consecutivo.ObtenerConsecutivo('servicio_tecnico_numero_pago_comision'));
end;

procedure TServicioTecnicoPagoComision.AntesModificar;
begin

end;

procedure TServicioTecnicoPagoComision.AntesEliminar;
begin

end;

procedure TServicioTecnicoPagoComision.DespuesInsertar;
begin

end;

procedure TServicioTecnicoPagoComision.DespuesModificar;
begin

end;

procedure TServicioTecnicoPagoComision.DespuesEliminar;
begin

end;

constructor TServicioTecnicoPagoComision.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_servicio_tecnico_pagos_comisiones');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(9)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "codigo_tecnico"=$2,"codigo_zona"=$3,"codigo_moneda"=$4,"fecha_ingreso"=$5,"fecha_pago"=$6,"fecha_inicial"=$7,"fecha_final"=$8,"total"=$9  WHERE ("numero"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + 'WHERE ("numero"=$1)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"numero"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TServicioTecnicoPagoComision.SetNumero(varNumero: string);
begin
  Numero := varNumero;
end;

procedure TServicioTecnicoPagoComision.SetCodigoTecnico(varCodigoTecnico: string);
begin
  CodigoTecnico := varCodigoTecnico;
end;

procedure TServicioTecnicoPagoComision.SetCodigoZona(varCodigoZona: string);
begin
  CodigoZona := varCodigoZona;
end;

procedure TServicioTecnicoPagoComision.SetCodigoMoneda(varCodigoMoneda: string);
begin
  CodigoMoneda := varCodigoMoneda;
end;

procedure TServicioTecnicoPagoComision.SetFechaIngreso(varFechaIngreso: TDate);
begin
  FechaIngreso := varFechaIngreso;
end;

procedure TServicioTecnicoPagoComision.SetFechaPago(varFechaPago: TDate);
begin
  FechaPago := varFechaPago;
end;

procedure TServicioTecnicoPagoComision.SetFechaInicial(varFechaInicial: TDate);
begin
  FechaInicial := varFechaInicial;
end;

procedure TServicioTecnicoPagoComision.SetFechaFinal(varFechaFinal: TDate);
begin
  FechaFinal := varFechaFinal;
end;

procedure TServicioTecnicoPagoComision.SetTotal(varTotal: Double);
begin
  Total := varTotal;
end;

function TServicioTecnicoPagoComision.GetNumero: string;
begin
  Result := Numero;
end;

function TServicioTecnicoPagoComision.GetCodigoTecnico: string;
begin
  Result := CodigoTecnico;
end;

function TServicioTecnicoPagoComision.GetCodigoZona: string;
begin
  Result := CodigoZona;
end;

function TServicioTecnicoPagoComision.GetCodigoMoneda: string;
begin
  Result := CodigoMoneda;
end;

function TServicioTecnicoPagoComision.GetFechaIngreso: TDate;
begin
  Result := FechaIngreso;
end;

function TServicioTecnicoPagoComision.GetFechaPago: TDate;
begin
  Result := FechaPago;
end;

function TServicioTecnicoPagoComision.GetFechaInicial: TDate;
begin
  Result := FechaInicial;
end;

function TServicioTecnicoPagoComision.GetFechaFinal: TDate;
begin
  Result := FechaFinal;
end;

function TServicioTecnicoPagoComision.GetTotal: Double;
begin
  Result := Total;
end;

end.
