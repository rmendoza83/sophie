unit ClsBusquedaMovimientoInventario;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TBusquedaMovimientoInventario }

  TBusquedaMovimientoInventario = class(TGenericoBD)
    private
      Numero: string;
      Concepto: string;
      LoginUsuario: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      function GetNumero: string;
      function GetConcepto: string;
      function GetLoginUsuario: string;
  end;

implementation

{ TBusquedaMovimientoInventario }

procedure TBusquedaMovimientoInventario.Ajustar;
begin

end;

procedure TBusquedaMovimientoInventario.Mover(Ds: TClientDataSet);
begin
  Numero := Ds.FieldValues['numero'];
  Concepto := Ds.FieldValues['concepto'];
  LoginUsuario := Ds.FieldValues['login_usuario'];
end;

function TBusquedaMovimientoInventario.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,3);
  Params[0] := Numero;
  Params[1] := Concepto;
  Params[2] := LoginUsuario;
  Result := Params;
end;

function TBusquedaMovimientoInventario.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,3);
  Campos[0] := 'numero';
  Campos[1] := 'concepto';
  Campos[2] := 'login_usuario';
  Result := Campos;
end;

function TBusquedaMovimientoInventario.GetCondicion: string;
begin
  Result:= '';
end;

procedure TBusquedaMovimientoInventario.AntesInsertar;
begin

end;

procedure TBusquedaMovimientoInventario.AntesModificar;
begin

end;

procedure TBusquedaMovimientoInventario.AntesEliminar;
begin

end;

procedure TBusquedaMovimientoInventario.DespuesInsertar;
begin

end;

procedure TBusquedaMovimientoInventario.DespuesModificar;
begin

end;

procedure TBusquedaMovimientoInventario.DespuesEliminar;
begin

end;

constructor TBusquedaMovimientoInventario.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('v_busqueda_movimientos_inventario');
  SetLength(ClavesPrimarias,0);
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

function TBusquedaMovimientoInventario.GetNumero: string;
begin
  Result := Numero;
end;

function TBusquedaMovimientoInventario.GetConcepto: string;
begin
  Result := Concepto;
end;

function TBusquedaMovimientoInventario.GetLoginUsuario: string;
begin
  Result := LoginUsuario;
end;

end.
