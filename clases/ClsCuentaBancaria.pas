unit ClsCuentaBancaria;
interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TCuentaBancaria }

  TCuentaBancaria = class(TGenericoBD)
    private
      Codigo: string;
      Entidad: string;
      Agencia: string;
      Tipo: string;
      Numero: string;
      FechaApertura: TDate;
      Telefonos: string;
      Fax: string;
      Email: string;
      WebSite: string;
      Contacto: string;
      CodigoZona: string;
      ControlChequera: Boolean;
      NumChequeInicial: Integer;
      NumChequeFinal: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigo(varCodigo: string);
      procedure SetEntidad(varEntidad: string);
      procedure SetAgencia(varAgencia: string);
      procedure SetTipo(varTipo: string);
      procedure SetNumero(varNumero: string);
      procedure SetFechaApertura(varFechaApertura: TDate);
      procedure SetTelefonos(varTelefonos: string);
      procedure SetFax(varFax: string);
      procedure SetEmail(varEmail: string);
      procedure SetWebSite(varWebSite: string);
      procedure SetContacto(varContacto: string);
      procedure SetCodigoZona(varCodigoZona: string);
      procedure SetControlChequera(VarControlChequera: Boolean);
      procedure SetNumChequeInicial(VarNumChequeInicial: Integer);
      procedure SetNumChequeFinal(VarNumChequeFinal: Integer);
      function GetCodigo: string;
      function GetEntidad: string;
      function GetAgencia: string;
      function GetTipo: string;
      function GetNumero: string;
      function GetFechaApertura: TDate;
      function GetTelefonos: string;
      function GetFax: string;
      function GetEmail: string;
      function GetWebSite: string;
      function GetContacto: string;
      function GetCodigoZona: string;
      function GetControlChequera: Boolean;
      function GetNumChequeInicial: Integer;
      function GetNumChequeFinal: Integer;
      function ObtenerCombo: TClientDataSet;
  end;

implementation

uses DB;

{ TCuentaBancaria }

procedure TCuentaBancaria.Ajustar;
begin

end;

procedure TCuentaBancaria.Mover(Ds: TClientDataSet);
begin
  Codigo := Ds.FieldValues['codigo'];
  Entidad := Ds.FieldValues['entidad'];
  Agencia := Ds.FieldValues['agencia'];
  Tipo := Ds.FieldValues['tipo'];
  Numero := Ds.FieldValues['numero'];
  FechaApertura := Ds.FieldByName('fecha_apertura').AsDateTime;
  Telefonos := Ds.FieldValues['telefonos'];
  Fax := Ds.FieldValues['fax'];
  Email := Ds.FieldValues['email'];
  WebSite := Ds.FieldValues['website'];
  Contacto := Ds.FieldValues['contacto'];
  CodigoZona :=Ds.fieldValues['codigo_zona'];
  ControlChequera := Ds.FieldValues['control_chequera'];
  NumChequeInicial := Ds.FieldValues['num_cheque_inicial'];
  NumChequeFinal := Ds.FieldValues['num_cheque_final'];
end;

function TCuentaBancaria.GetParametros: TArrayVariant;
var
 Params : TArrayVariant;
begin
  SetLength(Params,15);
  Params[0] := Codigo;
  Params[1] := Entidad;
  Params[2] := Agencia;
  Params[3] := Tipo;
  Params[4] := Numero;
  Params[5] := VarFromDateTime(FechaApertura);
  Params[6] := Telefonos;
  Params[7] := Fax;
  Params[8] := Email;
  Params[9] := WebSite;
  Params[10] := Contacto;
  Params[11] := CodigoZona;
  Params[12] := ControlChequera;
  Params[13] := NumChequeInicial;
  Params[14] := NumChequeFinal;
  Result := Params;
end;

function TCuentaBancaria.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,15);
  Campos[0] := 'codigo';
  Campos[1] := 'entidad';
  Campos[2] := 'agencia';
  Campos[3] := 'tipo';
  Campos[4] := 'numero';
  Campos[5] := 'fecha_apertura';
  Campos[6] := 'telefonos';
  Campos[7] := 'fax';
  Campos[8] := 'email';
  Campos[9] := 'website';
  Campos[10] := 'contacto';
  Campos[11] := 'codigo_zona';
  Campos[12] := 'control_chequera';
  Campos[13] := 'num_cheque_inicial';
  Campos[14] := 'num_cheque_final';
  Result := Campos;
end;


function TCuentaBancaria.GetCondicion: string;
begin
  Result := '"codigo" = ' + QuotedStr(Codigo);
end;

procedure TCuentaBancaria.AntesInsertar;
begin

end;

procedure TCuentaBancaria.AntesModificar;
begin

end;

procedure TCuentaBancaria.AntesEliminar;
begin

end;

procedure TCuentaBancaria.DespuesInsertar;
begin

end;

procedure TCuentaBancaria.DespuesModificar;
begin

end;

procedure TCuentaBancaria.DespuesEliminar;
begin

end;

constructor TCuentaBancaria.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_cuentas_bancarias');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(15)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "entidad"=$2,"agencia"=$3, "tipo"=$4, "numero"=$5, "fecha_apertura"=$6, "telefonos"=$7, "fax"=$8, "email"=$9, ' +
                  '"website"=$10, "contacto"=$11, "codigo_zona"=$12, "control_chequera"=$13, "num_cheque_inicial"=$14, "num_cheque_final"=$15  WHERE ("codigo"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("codigo"=$1)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"codigo"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TCuentaBancaria.SetAgencia(varAgencia: string);
begin
  Agencia := varAgencia;
end;

procedure TCuentaBancaria.SetCodigo(varCodigo: string);
begin
  Codigo := varCodigo;
end;

procedure TCuentaBancaria.SetCodigoZona(varCodigoZona: string);
begin
  CodigoZona := varCodigoZona;
end;

procedure TCuentaBancaria.SetContacto(varContacto: string);
begin
  Contacto := varContacto;
end;

procedure TCuentaBancaria.SetControlChequera(VarControlChequera: Boolean);
begin
  ControlChequera := VarControlChequera;
end;

procedure TCuentaBancaria.SetEmail(varEmail: string);
begin
  Email := varEmail;
end;

procedure TCuentaBancaria.SetEntidad(varEntidad: string);
begin
  Entidad := varEntidad;
end;

procedure TCuentaBancaria.SetFax(varFax: string);
begin
  Fax := varFax;
end;

procedure TCuentaBancaria.SetFechaApertura(varFechaApertura: TDate);
begin
  FechaApertura := varFechaApertura;
end;

procedure TCuentaBancaria.SetNumChequeFinal(VarNumChequeFinal: Integer);
begin
  NumChequeFinal := VarNumChequeFinal;
end;

procedure TCuentaBancaria.SetNumChequeInicial(VarNumChequeInicial: Integer);
begin
  NumChequeInicial := VarNumChequeInicial;
end;

procedure TCuentaBancaria.SetNumero(varNumero: string);
begin
  Numero := varNumero;
end;

procedure TCuentaBancaria.SetTelefonos(varTelefonos: string);
begin
  Telefonos := varTelefonos;
end;

procedure TCuentaBancaria.SetTipo(varTipo: string);
begin
  Tipo := varTipo;
end;

procedure TCuentaBancaria.SetWebSite(varWebSite: string);
begin
  WebSite := varWebSite;
end;

function TCuentaBancaria.GetAgencia: string;
begin
  Result := Agencia;
end;

function TCuentaBancaria.GetCodigo: string;
begin
  Result := Codigo;
end;

function TCuentaBancaria.GetCodigoZona: string;
begin
  Result := CodigoZona;
end;

function TCuentaBancaria.GetContacto: string;
begin
  Result := Contacto;
end;

function TCuentaBancaria.GetControlChequera: Boolean;
begin
  Result := ControlChequera;
end;

function TCuentaBancaria.GetEmail: string;
begin
  Result := Email;
end;

function TCuentaBancaria.GetEntidad: string;
begin
  Result := Entidad;
end;

function TCuentaBancaria.GetFax: string;
begin
  Result := Fax;
end;

function TCuentaBancaria.GetFechaApertura: TDate;
begin
  Result := FechaApertura;
end;

function TCuentaBancaria.GetNumChequeFinal: Integer;
begin
  Result := NumChequeFinal;
end;

function TCuentaBancaria.GetNumChequeInicial: Integer;
begin
  Result := NumChequeInicial;
end;

function TCuentaBancaria.GetNumero: string;
begin
  Result := Numero;
end;

function TCuentaBancaria.GetTelefonos: string;
begin
  Result := Telefonos;
end;

function TCuentaBancaria.GetTipo: string;
begin
  Result := Tipo;
end;

function TCuentaBancaria.GetWebSite: string;
begin
  Result := WebSite;
end;

function TCuentaBancaria.ObtenerCombo: TClientDataSet;
begin
  Result := BD.EjecutarQueryCliDs('SELECT "codigo", "entidad", "numero"  FROM ' + GetTabla);
end;

end.
