unit ClsMenu;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils;

type

  { TMenuSophie }

  TMenuSophie = class(TGenericoBD)
    private
      LoginUsuario: string;
      Orden: string;
      Titulo: string;
      TextoAyuda: string;
      Activo: Boolean;
      Visible: Boolean;
      MenuPadre: Boolean;
      IdModulo: Integer;
      IdxImagen: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetLoginUsuario(varLoginUsuario: string);
      procedure SetOrden(varOrden: string);
      procedure SetTitulo(varTitulo: string);
      procedure SetTextoAyuda(varTextoAyuda: string);
      procedure SetActivo(varActivo: Boolean);
      procedure SetVisible(varVisible: Boolean);
      procedure SetMenuPadre(varMenuPadre: Boolean);
      procedure SetIdModulo(varIdModulo: Integer);
      procedure SetIdxImagen(varIdxImagen: Integer);
      function GetLoginUsuario: string;
      function GetOrden: string;
      function GetTitulo: String;
      function GetTextoAyuda: String;
      function GetActivo: Boolean;
      function GetVisible: Boolean;
      function GetMenuPadre: Boolean;
      function GetIdModulo: Integer;
      function GetIdxImagen: Integer;
      function ObtenerListaLoginUsuario(varLoginUsuario: string): TClientDataSet;
  end;

implementation

{ TMenuSophie }

procedure TMenuSophie.Ajustar;
begin

end;

procedure TMenuSophie.Mover(Ds: TClientDataSet);
begin
  LoginUsuario := Ds.FieldValues['login_usuario'];
  Orden := Ds.FieldValues['orden'];
  Titulo := Ds.FieldValues['titulo'];
  TextoAyuda := Ds.FieldValues['texto_ayuda'];
  Activo := Ds.FieldValues['activo'];
  Visible := Ds.FieldValues['visible'];
  MenuPadre := Ds.FieldValues['menu_padre'];
  IdModulo := Ds.FieldValues['id_modulo'];
  IdxImagen := Ds.FieldValues['idx_imagen'];
end;

function TMenuSophie.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,9);
  Params[0] := LoginUsuario;
  Params[1] := Orden;
  Params[2] := Titulo;
  Params[3] := TextoAyuda;
  Params[4] := Activo;
  Params[5] := Visible;
  Params[6] := MenuPadre;
  Params[7] := IdModulo;
  Params[8] := IdxImagen;
  Result := Params;
end;

function TMenuSophie.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,3);
  Campos[0] := 'login_usuario';
  Campos[1] := 'orden';
  Campos[2] := 'titulo';
  Result := Campos;
end;

function TMenuSophie.GetCondicion: String;
begin
  Result := '"login_usuario" = ' + QuotedStr(LoginUsuario);
end;

procedure TMenuSophie.AntesInsertar;
begin

end;

procedure TMenuSophie.AntesModificar;
begin

end;

procedure TMenuSophie.AntesEliminar;
begin

end;

procedure TMenuSophie.DespuesInsertar;
begin

end;

procedure TMenuSophie.DespuesModificar;
begin

end;

procedure TMenuSophie.DespuesEliminar;
begin

end;

constructor TMenuSophie.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('sys_menu');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',', CreateDollarsParams(9)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "titulo"=$3, "texto_ayuda"=$4, "activo"=$5, "visible"=$6, "menu_padre"=$7, "id_modulo"=$8, "idx_imagen"=$9 WHERE ("login_usuario"=$1 AND "orden"=$2)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("login_usuario"=$1 AND "orden"=$2)');
  SetLength(ClavesPrimarias,2);
  ClavesPrimarias[0] := '"login_usuario"';
  ClavesPrimarias[1] := '"orden"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TMenuSophie.SetLoginUsuario(varLoginUsuario: String);
begin
  LoginUsuario := varLoginUsuario;
end;

procedure TMenuSophie.SetOrden(varOrden: String);
begin
  Orden := varOrden;
end;

procedure TMenuSophie.SetTitulo(varTitulo: String);
begin
  Titulo := varTitulo;
end;

procedure TMenuSophie.SetTextoAyuda(varTextoAyuda: String);
begin
  TextoAyuda := varTextoAyuda;
end;

procedure TMenuSophie.SetActivo(varActivo: Boolean);
begin
  Activo := varActivo;
end;

procedure TMenuSophie.SetVisible(varVisible: Boolean);
begin
  Visible := varVisible;
end;

procedure TMenuSophie.SetMenuPadre(varMenuPadre: Boolean);
begin
  MenuPadre := varMenuPadre;
end;

procedure TMenuSophie.SetIdModulo(varIdModulo: Integer);
begin
  IdModulo := varIdModulo;
end;

procedure TMenuSophie.SetIdxImagen(varIdxImagen: Integer);
begin
  IdxImagen := varIdxImagen;
end;

function TMenuSophie.GetLoginUsuario: String;
begin
  Result := LoginUsuario;
end;

function TMenuSophie.GetOrden: String;
begin
  Result := Orden;
end;

function TMenuSophie.GetTitulo: String;
begin
  Result := Titulo;
end;

function TMenuSophie.GetTextoAyuda: String;
begin
  Result := TextoAyuda;
end;

function TMenuSophie.GetActivo: Boolean;
begin
  Result := Activo;
end;

function TMenuSophie.GetVisible: Boolean;
begin
  Result := Visible;
end;

function TMenuSophie.GetMenuPadre: Boolean;
begin
  Result := MenuPadre;
end;

function TMenuSophie.GetIdModulo: Integer;
begin
  Result := IdModulo;
end;

function TMenuSophie.GetIdxImagen: Integer;
begin
  Result := IdxImagen;
end;

function TMenuSophie.ObtenerListaLoginUsuario(
  varLoginUsuario: string): TClientDataSet;
begin
  Result := ObtenerListaCondicionSQL(' WHERE "login_usuario" = ' + QuotedStr(varLoginUsuario) + ' ORDER BY orden');
end;

end.

