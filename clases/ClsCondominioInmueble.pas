unit ClsCondominioInmueble;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TCondominioInmueble }

  TCondominioInmueble = class(TGenericoBD)
    private
      CodigoConjuntoAdministracion: string;
      Codigo: string;
      CodigoPropietario: string;
      Descripcion: string;
      CodigoTipoInmueble: string;
      Observaciones: string;
      PAlicuota: Double;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigoConjuntoAdministracion(varCodigoConjuntoAdministracion: string);
      procedure SetCodigo(varCodigo: string);
      procedure SetCodigoPropietario(varCodigoPropietario: string);
      procedure SetDescripcion(varDescripcion: string);
      procedure SetCodigoTipoInmueble(varCodigoTipoInmueble: string);
      procedure SetObservaciones(varObservaciones: string);
      procedure SetPAlicuota(varPAlicuota: Double);
      function GetCodigoConjuntoAdministracion: string;
      function GetCodigo: string;
      function GetCodigoPropietario: string;
      function GetDescripcion: string;
      function GetCodigoTipoInmueble: string;
      function GetObservaciones: string;
      function GetPAlicuota: Double;
      function ObtenerConjuntoAdministracion(varCodigoConjuntoAdministracion: string): TClientDataSet;
  end;

var
  Clase: TCondominioInmueble;

implementation

{ TCondominioInmueble }

procedure TCondominioInmueble.Ajustar;
begin

end;

procedure TCondominioInmueble.Mover(Ds: TClientDataSet);
begin
  CodigoConjuntoAdministracion := Ds.FieldValues['codigo_conjunto_administracion'];
  Codigo := Ds.FieldValues['codigo'];
  CodigoPropietario := Ds.FieldValues['codigo_propietario'];
  Descripcion := Ds.FieldValues['descripcion'];
  CodigoTipoInmueble := Ds.FieldValues['codigo_tipo_inmueble'];
  Observaciones := Ds.FieldValues['observaciones'];
  PAlicuota := Ds.FieldValues['p_alicuota'];
end;

function TCondominioInmueble.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,7);
  Params[0] := CodigoConjuntoAdministracion;
  Params[1] := Codigo;
  Params[2] := CodigoPropietario;
  Params[3] := Descripcion;
  Params[4] := CodigoTipoInmueble;
  Params[5] := Observaciones;
  Params[6] := PAlicuota;
  Result := Params;
end;

function TCondominioInmueble.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,6);
  Campos[0] := 'codigo_conjunto_administracion';
  Campos[1] := 'codigo';
  Campos[2] := 'codigo_propietario';
  Campos[3] := 'descripcion';
  Campos[4] := 'codigo_tipo_inmueble';
  Campos[5] := 'observaciones';
  Result := Campos;
end;

function TCondominioInmueble.GetCondicion: string;
begin
  Result := '"codigo_conjunto_administracion" = ' + QuotedStr(CodigoConjuntoAdministracion) + ' AND ' +
            '"codigo" = ' + QuotedStr(Codigo);
end;

procedure TCondominioInmueble.AntesEliminar;
begin

end;

procedure TCondominioInmueble.AntesInsertar;
begin

end;

procedure TCondominioInmueble.AntesModificar;
begin

end;

procedure TCondominioInmueble.DespuesEliminar;
begin

end;

procedure TCondominioInmueble.DespuesInsertar;
begin

end;

procedure TCondominioInmueble.DespuesModificar;
begin

end;

constructor TCondominioInmueble.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_condominio_inmuebles');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(7)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "codigo_propietario"=$3, "descripcion"=$4, "codigo_tipo_inmueble"=$5, "observaciones"=$6, "p_alicuota"=$7 ' +
                  'WHERE ("codigo_conjunto_administracion"=$1 AND "codigo"=$2)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("codigo_conjunto_administracion"=$1 AND "codigo"=$2)');
  SetLength(ClavesPrimarias,2);
  ClavesPrimarias[0] := '"codigo_conjunto_administracion"';
  ClavesPrimarias[1] := '"codigo"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TCondominioInmueble.SetCodigoConjuntoAdministracion(varCodigoConjuntoAdministracion: string);
begin
  CodigoConjuntoAdministracion := varCodigoConjuntoAdministracion;
end;

procedure TCondominioInmueble.SetCodigo(varCodigo: string);
begin
  Codigo := varCodigo;
end;

procedure TCondominioInmueble.SetCodigoPropietario(varCodigoPropietario: string);
begin
  CodigoPropietario := varCodigoPropietario;
end;

procedure TCondominioInmueble.SetDescripcion(varDescripcion: string);
begin
  Descripcion := varDescripcion;
end;

procedure TCondominioInmueble.SetCodigoTipoInmueble(varCodigoTipoInmueble: string);
begin
  CodigoTipoInmueble := varCodigoTipoInmueble;
end;

procedure TCondominioInmueble.SetObservaciones(varObservaciones: string);
begin
  Observaciones := varObservaciones;
end;

procedure TCondominioInmueble.SetPAlicuota(varPAlicuota: Double);
begin
  PAlicuota := varPAlicuota;
end;

function TCondominioInmueble.GetCodigoConjuntoAdministracion: string;
begin
  Result := CodigoConjuntoAdministracion;
end;

function TCondominioInmueble.GetCodigo: string;
begin
  Result := Codigo;
end;

function TCondominioInmueble.GetCodigoPropietario: string;
begin
  Result := CodigoPropietario;
end;

function TCondominioInmueble.GetDescripcion: string;
begin
  Result := Descripcion;
end;

function TCondominioInmueble.GetCodigoTipoInmueble: string;
begin
  Result := CodigoTipoInmueble;
end;

function TCondominioInmueble.GetObservaciones: string;
begin
  Result := Observaciones;
end;

function TCondominioInmueble.GetPAlicuota: Double;
begin
  Result := PAlicuota;
end;

function TCondominioInmueble.ObtenerConjuntoAdministracion(
  varCodigoConjuntoAdministracion: string): TClientDataSet;
begin
  Result := ObtenerListaCondicionSQL(' WHERE ("codigo_conjunto_administracion" = ' + QuotedStr(varCodigoConjuntoAdministracion) + ')'); 
end;

end.
