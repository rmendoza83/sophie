unit ClsRadioRubro;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TRadioRubro}

  TRadioRubro = class(TGenericoBD)
    private
      Codigo: string;
      Descripcion: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      function ObtenerCombo: TClientDataSet;
      procedure SetCodigo(varCodigo: string);
      procedure SetDescripcion(varDescripcion: string);
      function GetCodigo: string;
      function GetDescripcion: string;
      function ObtenerConsecutivoCodigo: string;
      function BuscarDescripcion: Boolean;
  end;

implementation

{ TRadioRubro }

procedure TRadioRubro.Ajustar;
begin

end;

procedure TRadioRubro.Mover(Ds: TClientDataSet);
begin
  Codigo := Ds.FieldValues['codigo'];
  Descripcion := Ds.FieldValues['descripcion'];
end;

function TRadioRubro.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,2);
  Params[0] := Codigo;
  Params[1] := Descripcion;
  Result := Params;
end;

function TRadioRubro.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,2);
  Campos[0] := 'codigo';
  Campos[1] := 'descripcion';
  Result := Campos;
end;

function TRadioRubro.GetCondicion: string;
begin
  Result := '"codigo" = ' + QuotedStr(Codigo);
end;

procedure TRadioRubro.AntesInsertar;
begin

end;

procedure TRadioRubro.AntesModificar;
begin

end;

procedure TRadioRubro.AntesEliminar;
begin

end;

procedure TRadioRubro.DespuesInsertar;
begin

end;

procedure TRadioRubro.DespuesModificar;
begin

end;

procedure TRadioRubro.DespuesEliminar;
begin

end;

constructor TRadioRubro.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_radio_rubros');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(2)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "descripcion"=$2 WHERE ("codigo"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("codigo"=$1)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"codigo"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

function TRadioRubro.ObtenerCombo: TClientDataSet;
begin
  Result := BD.EjecutarQueryCliDs('SELECT "codigo", "descripcion" FROM ' + GetTabla);
end;

procedure TRadioRubro.SetCodigo(varCodigo: string);
begin
  Codigo := varCodigo;
end;

procedure TRadioRubro.SetDescripcion(varDescripcion: string);
begin
  Descripcion := varDescripcion;
end;

function TRadioRubro.GetCodigo: string;
begin
  Result := Codigo;
end;

function TRadioRubro.GetDescripcion: string;
begin
  Result := Descripcion;
end;

function TRadioRubro.ObtenerConsecutivoCodigo: string;
var
  Ds: TClientDataSet;
  Consecutivo: Integer;
begin
  Ds := BD.EjecutarQueryCliDs('SELECT MAX(CAST("codigo" AS integer)) AS consecutivo FROM ' + GetTabla);
  if (Ds.RecordCount = 0) then
  begin
    Consecutivo := 1;
  end
  else
  begin
    Consecutivo := StrToIntDef(Ds.FieldByName('consecutivo').AsString,0) + 1;
  end;
  Result := FormatFloat('0000000000',Consecutivo);   
end;

function TRadioRubro.BuscarDescripcion: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("descripcion" = ' + QuotedStr(GetDescripcion) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

end.
