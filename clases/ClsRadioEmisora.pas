unit ClsRadioEmisora;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TRadioEmisora }

  TRadioEmisora = class(TGenericoBD)
    private
      Codigo: string;
      PrefijoRif: string;
      Rif: string;
      RazonSocial: string;
      DireccionFiscal1: string;
      DireccionFiscal2: string;
      Telefonos: string;
      Fax: string;
      Website: string;
      FechaIngreso: TDate;
      Contacto: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigo(varCodigo: string);
      procedure SetPrefijoRif(varPrefijoRif: string);
      procedure SetRif(varRif: string);
      procedure SetRazonSocial(varRazonSocial: string);
      procedure SetDireccionFiscal1(varDireccionFiscal1: string);
      procedure SetDireccionFiscal2(varDireccionFiscal2: string);
      procedure SetTelefonos(varTelefonos: string);
      procedure SetFax(varFax: string);
      procedure SetWebsite(varWebsite: string);
      procedure SetFechaIngreso(varFechaIngreso: TDate);
      procedure SetContacto(varContacto: string);
      function GetCodigo: string;
      function GetPrefijoRif: string;
      function GetRif: string;
      function GetRazonSocial: string;
      function GetDireccionFiscal1: string;
      function GetDireccionFiscal2: string;
      function GetTelefonos: string;
      function GetFax: string;
      function GetWebsite: string;
      function GetFechaIngreso: TDate;
      function GetContacto: string;
      function ObtenerCombo: TClientDataSet;
  end;

implementation

uses
  DB,
  ClsConsecutivo;

{ TRadioEmisora }

procedure TRadioEmisora.Ajustar;
begin

end;

procedure TRadioEmisora.Mover(Ds: TClientDataSet);
begin
  Codigo := Ds.FieldValues['codigo'];
  PrefijoRif := Ds.FieldValues['prefijo_rif'];
  Rif := Ds.FieldValues['rif'];
  RazonSocial := Ds.FieldValues['razon_social'];
  DireccionFiscal1 := Ds.FieldValues['direccion_fiscal_1'];
  DireccionFiscal2 := Ds.FieldValues['direccion_fiscal_2'];
  Telefonos := Ds.FieldValues['telefonos'];
  Fax := Ds.FieldValues['fax'];
  Website := Ds.FieldValues['website'];
  FechaIngreso := Ds.FieldByName('fecha_ingreso').AsDateTime;
  Contacto := Ds.FieldValues['contacto'];
end;

function TRadioEmisora.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,11);
  Params[0] := Codigo;
  Params[1] := PrefijoRif;
  Params[2] := Rif;
  Params[3] := RazonSocial;
  Params[4] := DireccionFiscal1;
  Params[5] := DireccionFiscal2;
  Params[6] := Telefonos;
  Params[7] := Fax;
  Params[8] := Website;
  Params[9] := VarFromDateTime(FechaIngreso);
  Params[10] := Contacto;
  Result := Params;
end;

function TRadioEmisora.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,9);
  Campos[0] := 'codigo';
  Campos[1] := 'rif';
  Campos[2] := 'razon_social';
  Campos[3] := 'direccion_fiscal_1';
  Campos[4] := 'direccion_fiscal_2';
  Campos[5] := 'telefonos';
  Campos[6] := 'fax';
  Campos[7] := 'website';
  Campos[8] := 'contacto';
  Result := Campos;
end;

function TRadioEmisora.GetCondicion: string;
begin
  Result := '"codigo" = ' + QuotedStr(Codigo);
end;

procedure TRadioEmisora.AntesInsertar;
begin

end;

procedure TRadioEmisora.AntesModificar;
begin

end;

procedure TRadioEmisora.AntesEliminar;
begin

end;

procedure TRadioEmisora.DespuesInsertar;
begin

end;

procedure TRadioEmisora.DespuesModificar;
begin

end;

procedure TRadioEmisora.DespuesEliminar;
begin

end;

constructor TRadioEmisora.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_radio_emisoras');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(11)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "prefijo_rif"=$2, "rif"=$3, "razon_social"=$4, "direccion_fiscal_1"=$5, "direccion_fiscal_2"=$6, "telefonos"=$7, "fax"=$8, "website"=$9, "fecha_ingreso"=$10, "contacto"=$11 WHERE ("codigo"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("codigo"=$1)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"codigo"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TRadioEmisora.SetCodigo(varCodigo: string);
begin
  Codigo := varCodigo;
end;

procedure TRadioEmisora.SetPrefijoRif(varPrefijoRif: string);
begin
  PrefijoRif := varPrefijoRif;
end;

procedure TRadioEmisora.SetRif(varRif: string);
begin
  Rif := varRif;
end;

procedure TRadioEmisora.SetRazonSocial(varRazonSocial: string);
begin
  RazonSocial := varRazonSocial;
end;

procedure TRadioEmisora.SetDireccionFiscal1(varDireccionFiscal1: string);
begin
  DireccionFiscal1 := varDireccionFiscal1;
end;

procedure TRadioEmisora.SetDireccionFiscal2(varDireccionFiscal2: string);
begin
  DireccionFiscal2 := varDireccionFiscal2;
end;

procedure TRadioEmisora.SetTelefonos(varTelefonos: string);
begin
  Telefonos := varTelefonos;
end;

procedure TRadioEmisora.SetFax(varFax: string);
begin
  Fax := varFax;
end;

procedure TRadioEmisora.SetWebsite(varWebsite: string);
begin
  Website := varWebsite;
end;

procedure TRadioEmisora.SetFechaIngreso(varFechaIngreso: TDate);
begin
  FechaIngreso := varFechaIngreso;
end;

procedure TRadioEmisora.SetContacto(varContacto: string);
begin
  Contacto := varContacto;
end;

function TRadioEmisora.GetCodigo: string;
begin
  Result := Codigo;
end;

function TRadioEmisora.GetPrefijoRif: string;
begin
  Result := PrefijoRif;
end;

function TRadioEmisora.GetRif: string;
begin
  Result := Rif;
end;

function TRadioEmisora.GetRazonSocial: string;
begin
  Result := RazonSocial;
end;

function TRadioEmisora.GetDireccionFiscal1: string;
begin
  Result := DireccionFiscal1;
end;

function TRadioEmisora.GetDireccionFiscal2: string;
begin
  Result := DireccionFiscal2;
end;

function TRadioEmisora.GetTelefonos: string;
begin
  Result := Telefonos;
end;

function TRadioEmisora.GetFax: string;
begin
  Result := Fax;
end;

function TRadioEmisora.GetWebsite: string;
begin
  Result := Website;
end;

function TRadioEmisora.GetFechaIngreso: TDate;
begin
  Result := FechaIngreso;
end;

function TRadioEmisora.GetContacto: string;
begin
  Result := Contacto;
end;

function TRadioEmisora.ObtenerCombo: TClientDataSet;
begin
  Result := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla);
end;

end.

