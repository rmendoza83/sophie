unit ClsDetalleContadoCobranza;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TDetalleContadoCobranza }

  TDetalleContadoCobranza = class(TGenericoBD)
    private
      IdContadoCobranza: Integer;
      CodigoFormaPago: string;
      Monto: Double;
      Desde: string;
      CodigoBanco: string;
      NumeroDocumento: string;
      NumeroAuxiliar: string;
      Referencia: string;
      Fecha: TDate;
      LoginUsuario: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetIdContadoCobranza(varIdContadoCobranza: Integer);
      procedure SetCodigoFormaPago(varCodigoFormaPago: string);
      procedure SetMonto(varMonto: Double);
      procedure SetDesde(VarDesde: string);
      procedure SetCodigoBanco(varCodigoBanco: string);
      procedure SetNumeroDocumento(varNumeroDocumento: string);
      procedure SetNumeroAuxiliar(varNumeroAuxiliar: string);
      procedure SetReferencia(varReferencia: string);
      procedure SetFecha(varFecha: TDate);
      procedure SetLoginUsuario(varLoginUsuario: string);
      function GetIdContadoCobranza: Integer;
      function GetCodigoFormaPago: string;
      function GetMonto: Double;
      function GetDesde: String;
      function GetCodigoBanco: string;
      function GetNumeroDocumento: string;
      function GetNumeroAuxiliar: string;
      function GetReferencia: string;
      function GetFecha: TDate;
      function GetLoginUsuario: string;
      function EliminarIdContadoCobranza(varIdContadoCobranza: Integer): Boolean;
  end;

implementation

uses DB;

{ TDetalleContadoCobranza }

procedure TDetalleContadoCobranza.Ajustar;
begin

end;

procedure TDetalleContadoCobranza.Mover(Ds: TClientDataSet);
begin
  IdContadoCobranza := Ds.FieldValues['id_contado_cobranza'];
  CodigoFormaPago := Ds.FieldValues['codigo_forma_pago'];
  Monto := Ds.FieldValues['monto'];
  Desde := Ds.FieldValues['desde'];
  CodigoBanco := Ds.FieldValues['codigo_banco'];
  NumeroDocumento := Ds.FieldValues['numero_documento'];
  NumeroAuxiliar := Ds.FieldValues['numero_auxiliar'];
  Referencia := Ds.FieldValues['referencia'];
  Fecha := Ds.FieldByName('fecha').AsDateTime ;
  LoginUsuario := Ds.FieldValues['login_usuario'];
end;

function TDetalleContadoCobranza.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,10);
  Params[0] := IdContadoCobranza;
  Params[1] := CodigoFormaPago;
  Params[2] := Monto;
  Params[3] := Desde;
  Params[4] := CodigoBanco;
  Params[5] := NumeroDocumento;
  Params[6] := NumeroAuxiliar;
  Params[7] := Referencia;
  Params[8] := VarToDateTime(Fecha);
  Params[9] := LoginUsuario;
  result := Params;
end;

function TDetalleContadoCobranza.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,7);
  Campos[0] := 'codigo_forma_pago';
  Campos[1] := 'desde';
  Campos[2] := 'codigo_banco';
  Campos[3] := 'numero_documento';
  Campos[4] := 'numero_auxiliar';
  Campos[5] := 'referencia';
  Campos[6] := 'login_usuario';
  Result := Campos;
end;

function TDetalleContadoCobranza.GetCondicion: string;
begin
  Result := '"id_contado_cobranza" = ' + IntToStr(IdContadoCobranza) + 'AND "codigo_forma_pago" =' + QuotedStr(CodigoFormaPago);
end;

procedure TDetalleContadoCobranza.AntesInsertar;
begin

end;

procedure TDetalleContadoCobranza.AntesModificar;
begin

end;

procedure TDetalleContadoCobranza.AntesEliminar;
begin

end;

procedure TDetalleContadoCobranza.DespuesInsertar;
begin

end;

procedure TDetalleContadoCobranza.DespuesModificar;
begin

end;

procedure TDetalleContadoCobranza.DespuesEliminar;
begin

end;

constructor TDetalleContadoCobranza.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_det_contado_cobranzas');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(10)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "monto"=$3, "desde"=$4,"codigo_banco"=$5,"numero_documento"=$6,"numero_auxiliar"=$7,"referencia"=$8,"fecha"=$9,"login_usuario"=$10 WHERE ("id_contado_cobranza"=$1) AND ("codigo_forma_pago"=$2)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("id_contado_cobranza"=$1) AND ("codigo_forma_pago"=$2)');
  SetLength(ClavesPrimarias,2);
  ClavesPrimarias[0]:='"id_contado_cobranza"';
  ClavesPrimarias[1]:='"codigo_forma_pago"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TDetalleContadoCobranza.SetIdContadoCobranza(varIdContadoCobranza: Integer);
begin
  IdContadoCobranza := varIdContadoCobranza;
end;

procedure TDetalleContadoCobranza.SetCodigoBanco(varCodigoBanco: string);
begin
  CodigoBanco := varCodigoBanco;
end;

procedure TDetalleContadoCobranza.SetCodigoFormapago(varCodigoFormaPago: string);
begin
  CodigoFormaPago := varCodigoFormaPago;
end;

procedure TDetalleContadoCobranza.SetFecha(varFecha: TDate);
begin
  Fecha := varFecha;
end;

procedure TDetalleContadoCobranza.SetMonto(varMonto: Double);
begin
  Monto := varMonto;
end;

procedure TDetalleContadoCobranza.SetDesde(varDesde: string);
begin
  Desde := varDesde;
end;

procedure TDetalleContadoCobranza.SetNumeroAuxiliar(varNumeroAuxiliar: string);
begin
  NumeroAuxiliar := varNumeroAuxiliar;
end;

procedure TDetalleContadoCobranza.SetNumeroDocumento(varNumeroDocumento: string);
begin
  NumeroDocumento := varNumeroDocumento;
end;

procedure TDetalleContadoCobranza.SetReferencia(varReferencia: string);
begin
  Referencia := varReferencia;
end;

procedure TDetalleContadoCobranza.SetLoginUsuario(varLoginUsuario: string);
begin
  LoginUsuario := varLoginUsuario;
end;

function TDetalleContadoCobranza.GetCodigoBanco: string;
begin
  Result := CodigoBanco;
end;

function TDetalleContadoCobranza.GetCodigoFormaPago: string;
begin
  Result := CodigoFormaPago;
end;

function TDetalleContadoCobranza.GetFecha: TDate;
begin
  Result := Fecha;
end;

function TDetalleContadoCobranza.GetIdContadoCobranza: Integer;
begin
  Result := IdContadoCobranza;
end;

function TDetalleContadoCobranza.GetMonto: Double;
begin
  Result := Monto;
end;

function TDetalleContadoCobranza.GetDesde: string;
begin
  Result := Desde;
end;

function TDetalleContadoCobranza.GetNumeroAuxiliar: string;
begin
  Result := NumeroAuxiliar;
end;

function TDetalleContadoCobranza.GetNumeroDocumento: string;
begin
  Result := NumeroDocumento;
end;

function TDetalleContadoCobranza.GetReferencia: string;
begin
  Result := Referencia;
end;

function TDetalleContadoCobranza.GetLoginUsuario: string;
begin
  Result := LoginUsuario;
end;

function TDetalleContadoCobranza.EliminarIdContadoCobranza(
  varIdContadoCobranza: Integer): Boolean;
begin
  Result := EjecutarSQL('DELETE FROM ' + GetTabla + ' WHERE "id_contado_cobranza" = ' + IntToStr(varIdContadoCobranza));
end;

end.
