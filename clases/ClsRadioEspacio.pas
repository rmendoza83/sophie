unit ClsRadioEspacio;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TRadioEspacio}

  TRadioEspacio = class(TGenericoBD)
    private
      Codigo: string;
      Descripcion: string;
      Rotativo: Boolean;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      function ObtenerCombo: TClientDataSet;
      procedure SetCodigo(varCodigo: string);
      procedure SetDescripcion(varDescripcion: string);
      procedure SetRotativo(varRotativo: Boolean);
      function GetCodigo: string;
      function GetDescripcion: string;
      function GetRotativo: Boolean;
  end;

implementation

uses DB;

{ TRadioEspacio }

procedure TRadioEspacio.Ajustar;
begin

end;

procedure TRadioEspacio.Mover(Ds: TClientDataSet);
begin
  Codigo := Ds.FieldValues['codigo'];
  Descripcion := Ds.FieldValues['descripcion'];
  Rotativo := Ds.FieldValues['rotativo'];
end;

function TRadioEspacio.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,3);
  Params[0] := Codigo;
  Params[1] := Descripcion;
  Params[2] := Rotativo;
  Result := Params;
end;

function TRadioEspacio.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,2);
  Campos[0] := 'codigo';
  Campos[1] := 'descripcion';
  Result := Campos;
end;

function TRadioEspacio.GetCondicion: string;
begin
  Result := '"codigo" = ' + QuotedStr(Codigo);
end;

procedure TRadioEspacio.AntesInsertar;
begin

end;

procedure TRadioEspacio.AntesModificar;
begin

end;

procedure TRadioEspacio.AntesEliminar;
begin

end;

procedure TRadioEspacio.DespuesInsertar;
begin

end;

procedure TRadioEspacio.DespuesModificar;
begin

end;

procedure TRadioEspacio.DespuesEliminar;
begin

end;

constructor TRadioEspacio.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_radio_espacios');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(3)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "descripcion"=$2, "rotativo"=$3 WHERE ("codigo"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("codigo"=$1)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"codigo"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

function TRadioEspacio.ObtenerCombo: TClientDataSet;
begin
  Result := BD.EjecutarQueryCliDs('SELECT "codigo", "descripcion" FROM ' + GetTabla);
end;

procedure TRadioEspacio.SetCodigo(varCodigo: string);
begin
  Codigo := varCodigo;
end;

procedure TRadioEspacio.SetDescripcion(varDescripcion: string);
begin
  Descripcion := varDescripcion;
end;

procedure TRadioEspacio.SetRotativo(varRotativo: Boolean);
begin
  Rotativo := varRotativo;
end;

function TRadioEspacio.GetCodigo: string;
begin
  Result := Codigo;
end;

function TRadioEspacio.GetDescripcion: string;
begin
  Result := Descripcion;
end;

function TRadioEspacio.GetRotativo: Boolean;
begin
  Result := Rotativo;
end;

end.
