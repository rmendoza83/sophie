unit ClsGenericoBD;

interface

uses
  Classes, SysUtils, DBClient, DMBD, Utils, DB;

type

  { TGenericoBD }

  TGenericoBD = class
    private
      Tabla: string;
      StrSQLInsert: string;
      StrSQLUpdate: string;
      StrSQLDelete: string;
      ArrayClavesPrimarias: TArrayString;
      function ObtenerListado(varQuery: String): TClientDataSet;
      function GetHTMLTable(Ds: TClientDataSet): string; overload;
    protected
      function GenerarExpresionLike(Campos: TArrayString; varCriterio: string): string;
      procedure Ajustar; virtual; abstract;
      procedure Mover(Ds: TClientDataSet); virtual; abstract;
      function GetParametros: TArrayVariant; virtual; abstract;
      function GetCampos: TArrayString; virtual; abstract;
      function GetCondicion: string; virtual; abstract;
      procedure AntesInsertar; virtual; abstract;
      procedure AntesModificar; virtual; abstract;
      procedure AntesEliminar; virtual; abstract;
      procedure DespuesInsertar; virtual; abstract;
      procedure DespuesModificar; virtual; abstract;
      procedure DespuesEliminar; virtual; abstract;
    public
      function CreateDollarsParams(Count: Integer): TArrayString;
      function Buscar: Boolean;
      function Insertar: Boolean;
      function Modificar: Boolean;
      function Eliminar: Boolean;
      function ObtenerLista: TClientDataSet;
      function ObtenerListaCriterio(varCriterio: string): TClientDataSet;
      function ObtenerListaCondicionSQL(varSQL: string): TClientDataSet;
      function ObtenerListaSQL(varSQL: string): TClientDataSet;
      function EjecutarSQL(varSQL: string): Boolean;
      procedure SetTabla(varTabla: string);
      procedure SetStrSQLInsert(varStrSQLInsert: string);
      procedure SetStrSQLUpdate(varStrSQLUpdate: string);
      procedure SetStrSQLDelete(varStrSQLDelete: string);
      procedure SetArrayClavesPrimarias(varArrayClavesPrimaras: TArrayString);
      function GetTabla: string;
      function GetStrSQLInsert: string;
      function GetStrSQLUpdate: string;
      function GetStrSQLDelete: string;
      function GetArrayClavesPrimarias: TArrayString;
      function GetCamposCriterio: TArrayString;
      function GetHTMLTable: string; overload;
      function GetHTMLTableCriterio(varCriterio: string): string;
      function GetHTMLTableCondicionSQL(varSQL: string): string;
      function GetHTMLTableSQL(varSQL: string): string;
  end;

implementation

{ TGenericoBD }

function TGenericoBD.ObtenerListado(varQuery: String): TClientDataSet;
begin
  Result := BD.EjecutarQueryCliDs(varQuery);
end;

function TGenericoBD.GetHTMLTable(Ds: TClientDataSet): string;
var
  html: string;
  i: Integer;
begin
  html := '<table cellpadding="2px" cellspacing="2px" width="90%" rules="cols" style="border: 1px solid black; border-left: 1px solid black; border-right: 1px solid black; font-family: Arial; font-size: 11px;">';
  //Cargando Campos
  html := html + '<tr style="background-color: gray; color: white;">';
  for i := 0 to Ds.FieldCount - 1 do
  begin
    html := html + '<th align="center">' + UpperCase(Ds.Fields[i].FieldName) + '</th>';
  end;
  html := html + '</tr>';
  //Cargando Datos;
  Ds.First;
  while (not Ds.Eof) do
  begin
    html := html + '<tr>';
    for i := 0 to Ds.FieldCount - 1 do
    begin
      case Ds.Fields[i].DataType of
        ftSmallint, ftInteger, ftWord, ftLargeint:
        begin
          html := html + '<td align="right">' + Ds.FieldByName(Ds.Fields[i].FieldName).AsString + '</td>';
        end;
        ftFloat, ftCurrency:
        begin
          html := html + '<td align="right">' + FormatFloat('#,##0.0000',Ds.FieldByName(Ds.Fields[i].FieldName).AsFloat) + '</td>';
        end;
        ftDate:
        begin
          html := html + '<td align="center">' + FormatDateTime('dd/mm/yyyy',Ds.FieldByName(Ds.Fields[i].FieldName).AsDateTime) + '</td>';
        end;
        ftTime:
        begin
          html := html + '<td align="center">' + FormatDateTime('hh:nn:ss',Ds.FieldByName(Ds.Fields[i].FieldName).AsDateTime) + '</td>';
        end;
        ftDateTime:
        begin
          html := html + '<td align="center">' + FormatDateTime('dd/mm/yyyy hh:nn:ss',Ds.FieldByName(Ds.Fields[i].FieldName).AsDateTime) + '</td>';
        end;
        ftTimeStamp:
        begin
          html := html + '<td align="center">' + FormatDateTime('dd/mm/yyyy hh:nn:ss.zzz',Ds.FieldByName(Ds.Fields[i].FieldName).AsDateTime) + '</td>';
        end;
        ftBoolean:
        begin
          html := html + '<td align="center">' + BoolToStr(Ds.FieldByName(Ds.Fields[i].FieldName).AsBoolean) + '</td>';
        end
        else
        begin
          html := html + '<td align="left">' + Ds.FieldByName(Ds.Fields[i].FieldName).AsString + '</td>';
        end;
      end;
      html := html + '</td>';
    end;
    html := html + '</tr>';
    Ds.Next;
  end;
  html := html + '</table>';
  Result := html;
end;

function TGenericoBD.GenerarExpresionLike(Campos: TArrayString;
  varCriterio: string): string;
var
  i: Integer;
  S: TArrayString;
begin
  SetLength(S,0);
  for i := Low(Campos) to High(Campos) do
  begin
    SetLength(S,Length(S) + 1);
    S[Length(S) - 1] := '("' + Campos[i] + '" LIKE ' + QuotedStr('%' + varCriterio + '%') + ')';
  end;
  Result := Implode(' OR ',S);
end;

function TGenericoBD.CreateDollarsParams(Count: Integer): TArrayString;
var
  i: Integer;
  Params: TArrayString;
begin
  SetLength(Params,Count);
  for i := Low(Params) to High(Params) do
  begin
    Params[i] := '$' + IntToStr(i + 1);
  end;
  Result := Params;
end;

function TGenericoBD.Buscar: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + Tabla + ' WHERE ' + GetCondicion);
  if (Ds.RecordCount > 0) then
    begin
      Result := True;
      Mover(Ds);
    end;
end;

function TGenericoBD.Insertar: Boolean;
begin
  AntesInsertar;
  Result := BD.EjecutarNonQuery(StrSQLInsert,GetParametros);
  DespuesInsertar;
end;

function TGenericoBD.Modificar: Boolean;
begin
  AntesModificar;
  Result := BD.EjecutarNonQuery(StrSQLUpdate,GetParametros);
  DespuesModificar;
end;

function TGenericoBD.Eliminar: Boolean;
begin
  AntesEliminar;
  Result := BD.EjecutarNonQuery(StrSQLDelete,GetParametros);
  DespuesEliminar;
end;

function TGenericoBD.ObtenerLista: TClientDataSet;
begin
  Result := ObtenerListado('SELECT * FROM ' + GetTabla + ' ORDER BY 1');
end;

function TGenericoBD.ObtenerListaCriterio(varCriterio: String): TClientDataSet;
begin
  Result := ObtenerListado('SELECT * FROM ' + GetTabla + ' WHERE ' + GenerarExpresionLike(GetCampos,varCriterio));
end;

function TGenericoBD.ObtenerListaCondicionSQL(varSQL: String): TClientDataSet;
begin
  Result := ObtenerListado('SELECT * FROM ' + GetTabla + ' ' + varSQL);
end;

function TGenericoBD.ObtenerListaSQL(varSQL: String): TClientDataSet;
begin
  Result := ObtenerListado(varSQL);
end;

function TGenericoBD.EjecutarSQL(varSQL: String): Boolean;
begin
  Result := BD.EjecutarNonQuery(varSQL);
end;

procedure TGenericoBD.SetTabla(varTabla: string);
begin
  if (Length(BD.GetEsquema) > 0) then
  begin
    Tabla := '"' + BD.GetEsquema + '"."' + varTabla + '"';
  end
  else
  begin
    Tabla := '"' + varTabla + '"';
  end;
end;

procedure TGenericoBD.SetStrSQLInsert(varStrSQLInsert: string);
begin
  StrSQLInsert := varStrSQLInsert;
end;

procedure TGenericoBD.SetStrSQLUpdate(varStrSQLUpdate: string);
begin
  StrSQLUpdate := varStrSQLUpdate;
end;

procedure TGenericoBD.SetStrSQLDelete(varStrSQLDelete: string);
begin
  StrSQLDelete := varStrSQLDelete;
end;

procedure TGenericoBD.SetArrayClavesPrimarias(
  varArrayClavesPrimaras: TArrayString);
begin
  ArrayClavesPrimarias := varArrayClavesPrimaras;
end;

function TGenericoBD.GetTabla: string;
begin
  Result := Tabla;
end;

function TGenericoBD.GetStrSQLInsert: string;
begin
  Result := StrSQLInsert;
end;

function TGenericoBD.GetStrSQLUpdate: string;
begin
  Result := StrSQLUpdate;
end;

function TGenericoBD.GetStrSQLDelete: string;
begin
  Result := StrSQLDelete;
end;

function TGenericoBD.GetArrayClavesPrimarias: TArrayString;
begin
  Result := ArrayClavesPrimarias;
end;

function TGenericoBD.GetCamposCriterio: TArrayString;
begin
  Result := GetCampos;
end;

function TGenericoBD.GetHTMLTable: string;
begin
  Result := GetHTMLTable(ObtenerLista);
end;

function TGenericoBD.GetHTMLTableCriterio(varCriterio: string): string;
begin
  Result := GetHTMLTable(ObtenerListaCriterio(varCriterio));
end;

function TGenericoBD.GetHTMLTableCondicionSQL(varSQL: string): string;
begin
  Result := GetHTMLTable(ObtenerListaCondicionSQL(varSQL));
end;

function TGenericoBD.GetHTMLTableSQL(varSQL: string): string;
begin
  Result := GetHTMLTable(ObtenerListaSQL(varSQL));
end;

end.

