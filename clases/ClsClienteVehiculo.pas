unit ClsClienteVehiculo;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TClienteVehiculo }

  TClienteVehiculo = class(TGenericoBD)
    private
      CodigoCliente : string;
      Placa: string;
      Marca: string;
      Modelo: string;
      Anno: Integer;
      Color: string;
      Kilometraje: Double;
      Observaciones: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigoCliente(varCodigoCliente: string);
      procedure SetPlaca(varPlaca: string);
      procedure SetMarca(varMarca: string);
      procedure SetModelo(varModelo: string);
      procedure SetAnno(varAnno: Integer);
      procedure SetColor(varColor: string);
      procedure SetKilometraje(varKilometraje: Double);
      procedure SetObservaciones(varObservaciones: string);
      function GetCodigoCliente: string;
      function GetPlaca: string;
      function GetMarca: string;
      function GetModelo: string;
      function GetAnno: Integer;
      function GetColor: string;
      function GetKilometraje: Double;
      function GetObservaciones: string;
      function ObtenerCodigoCliente(varCodigoCliente: string): TClientDataSet;
      function EliminarCodigoCliente(varCodigoCliente: string): Boolean;
      function ObtenerComboCodigoCliente(varCodigoCliente: string): TClientDataSet;
  end;

implementation

uses
  ClsConsecutivo, DB;

{ TClienteVehiculo }

procedure TClienteVehiculo.Ajustar;
begin

end;

procedure TClienteVehiculo.Mover(Ds: TClientDataSet);
begin
  CodigoCliente := Ds.FieldValues['codigo_cliente'];
  Placa := Ds.FieldValues['placa'];
  Marca := Ds.FieldValues['marca'];
  Modelo := Ds.FieldValues['modelo'];
  Anno := Ds.FieldValues['anno'];
  Color := Ds.FieldValues['color'];
  Kilometraje := Ds.FieldValues['kilometraje'];
  Observaciones := Ds.FieldValues['observaciones'];
end;

function TClienteVehiculo.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,8);
  Params[0] := CodigoCliente;
  Params[1] := Placa;
  Params[2] := Marca;
  Params[3] := Modelo;
  Params[4] := Anno;
  Params[5] := Color;
  Params[6] := Kilometraje;
  Params[7] := Observaciones;
  Result := Params;
end;

function TClienteVehiculo.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,8);
  Campos[0] := 'codigo_cliente';
  Campos[1] := 'placa';
  Campos[2] := 'marca';
  Campos[3] := 'modelo';
  Campos[4] := 'anno';
  Campos[5] := 'color';
  Campos[6] := 'kilometraje';
  Campos[7] := 'observaciones';
  Result := Campos;
end;

function TClienteVehiculo.GetCondicion: string;
begin
  Result := '"codigo_cliente" = ' + QuotedStr(CodigoCliente) + ' AND ' +
            '"placa" = ' + QuotedStr(Placa);
end;

procedure TClienteVehiculo.AntesInsertar;
begin

end;

procedure TClienteVehiculo.AntesModificar;
begin

end;

procedure TClienteVehiculo.AntesEliminar;
begin

end;

procedure TClienteVehiculo.DespuesInsertar;
begin

end;

procedure TClienteVehiculo.DespuesModificar;
begin

end;

procedure TClienteVehiculo.DespuesEliminar;
begin

end;

constructor TClienteVehiculo.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_clientes_vehiculos');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(8)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "marca"=$3, "modelo"=$4, "anno"=$5, "color"=$6, "kilometraje"=$7, "observaciones"=$8  WHERE ("codigo_cliente"=$1) AND ("placa"=$2)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("codigo_cliente"=$1) AND ("placa"=$2)');
  SetLength(ClavesPrimarias,2);
  ClavesPrimarias[0] := '"codigo_cliente"';
  ClavesPrimarias[1] := '"placa"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TClienteVehiculo.SetCodigoCliente(varCodigoCliente: string);
begin
  CodigoCliente := varCodigoCliente;
end;

procedure TClienteVehiculo.SetPlaca(varPlaca: string);
begin
  Placa := varPlaca;
end;

procedure TClienteVehiculo.SetMarca(varMarca: string);
begin
  Marca := varMarca;
end;

procedure TClienteVehiculo.SetModelo(varModelo: string);
begin
  Modelo := varModelo;
end;

procedure TClienteVehiculo.SetAnno(varAnno: Integer);
begin
  Anno := varAnno;
end;

procedure TClienteVehiculo.SetColor(varColor: string);
begin
  Color := varColor;
end;

procedure TClienteVehiculo.SetKilometraje(varKilometraje: Double);
begin
    Kilometraje := varKilometraje;
end;

procedure TClienteVehiculo.SetObservaciones(varObservaciones: string);
begin
  Observaciones := varObservaciones;
end;

function TClienteVehiculo.GetCodigoCliente: string;
begin
  Result := CodigoCliente;
end;

function TClienteVehiculo.GetPlaca: string;
begin
  Result := Placa;
end;

function TClienteVehiculo.GetMarca: string;
begin
  Result := Marca;
end;

function TClienteVehiculo.GetModelo: string;
begin
  Result := Modelo;
end;

function TClienteVehiculo.GetAnno: Integer;
begin
  Result := Anno;
end;

function TClienteVehiculo.GetColor: string;
begin
  Result := Color;
end;

function TClienteVehiculo.GetKilometraje: Double;
begin
  Result := Kilometraje;
end;

function TClienteVehiculo.GetObservaciones: string;
begin
  Result := Observaciones;
end;

function TClienteVehiculo.ObtenerCodigoCliente(varCodigoCliente : string): TClientDataSet;
begin
  Result := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("codigo_cliente" = ' + QuotedStr(varCodigoCliente) + ')');
end;

function TClienteVehiculo.EliminarCodigoCliente(
  varCodigoCliente: string): Boolean;
begin
  Result := EjecutarSQL('DELETE FROM ' + GetTabla + ' WHERE ("codigo_cliente" = ' + QuotedStr(varCodigoCliente) + ')');
end;

function TClienteVehiculo.ObtenerComboCodigoCliente(
  varCodigoCliente: string): TClientDataSet;
begin
  Result := BD.EjecutarQueryCliDs('SELECT "placa", "marca", "modelo" FROM ' + GetTabla + ' WHERE ("codigo_cliente" = ' + QuotedStr(varCodigoCliente) + ')');
end;

end.

