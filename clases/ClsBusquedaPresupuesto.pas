unit ClsBusquedaPresupuesto;
interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TBusquedaPresupuesto }

  TBusquedaPresupuesto = class(TGenericoBD)
    private
      Numero: string;
      CodigoCliente: string;
      Rif: string;
      Nit: string;
      RazonSocial: string;
      Contacto: string;
      CodigoVendedor: string;
      NombreVendedor: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      Function GetNumero: string;
      Function GetCodigoCliente: string;
      Function GetRif: string;
      Function GetNit: string;
      Function GetRazonSocial: string;
      Function GetContacto: string;
      Function GetcodigoVendedor: string;
      Function GetNombreVendedor: string;
  end;

implementation

{ TBusquedaPresupuesto }

procedure TBusquedaPresupuesto.Ajustar;
begin

end;

procedure TBusquedaPresupuesto.Mover(Ds: TClientDataSet);
begin
  Numero := Ds.FieldValues['numero'];
  CodigoCliente := Ds.FieldValues['codigo_cliente'];
  Rif := Ds.FieldValues['rif'];
  Nit := Ds.FieldValues['nit'];
  RazonSocial := Ds.FieldValues['razon_social'];
  Contacto := Ds.FieldValues['contacto'];
  CodigoVendedor := Ds.FieldValues['codigo_vendedor'];
  NombreVendedor := Ds.FieldValues['nombre_vendedor'];
end;

function TBusquedaPresupuesto.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,8);
  Params[0] := Numero;
  Params[1] := CodigoCliente;
  Params[2] := Rif;
  Params[3] := Nit;
  Params[4] := RazonSocial;
  Params[5] := Contacto;
  Params[6] := CodigoVendedor;
  Params[7] := NombreVendedor;
  Result := Params;
end;

function TBusquedaPresupuesto.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,8);
  Campos[0] := 'numero';
  Campos[1] := 'codigo_cliente';
  Campos[2] := 'rif';
  Campos[3] := 'nit';
  Campos[4] := 'razon_social';
  Campos[5] := 'contacto';
  Campos[6] := 'codigo_vendedor';
  Campos[7] := 'nombre_vendedor';
  Result := Campos;
end;

function TBusquedaPresupuesto.GetCondicion: string;
begin
  Result:= '';
end;

procedure TBusquedaPresupuesto.AntesInsertar;
begin

end;

procedure TBusquedaPresupuesto.AntesModificar;
begin

end;

procedure TBusquedaPresupuesto.AntesEliminar;
begin

end;

procedure TBusquedaPresupuesto.DespuesInsertar;
begin

end;

procedure TBusquedaPresupuesto.DespuesModificar;
begin

end;

procedure TBusquedaPresupuesto.DespuesEliminar;
begin

end;

constructor TBusquedaPresupuesto.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('v_busqueda_presupuesto');
  SetLength(ClavesPrimarias,0);
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

function TBusquedaPresupuesto.GetCodigoCliente: string;
begin
  Result := CodigoCliente;
end;

function TBusquedaPresupuesto.GetcodigoVendedor: string;
begin
  Result := CodigoVendedor;
end;

function TBusquedaPresupuesto.GetContacto: string;
begin
  Result := Contacto;
end;

function TBusquedaPresupuesto.GetNit: string;
begin
  Result := Nit;
end;

function TBusquedaPresupuesto.GetNombreVendedor: string;
begin
  Result := NombreVendedor;
end;

function TBusquedaPresupuesto.GetNumero: string;
begin
  Result := Numero;
end;

function TBusquedaPresupuesto.GetRazonSocial: string;
begin
  Result := RazonSocial;
end;

function TBusquedaPresupuesto.GetRif: string;
begin
  Result := Rif;
end;

end.

