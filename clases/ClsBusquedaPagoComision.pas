unit ClsBusquedaPagoComision;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TBusquedaPagoComision }

  TBusquedaPagoComision = class(TGenericoBD)
    private
      Numero: string;
      CodigoVendedor: string;
      NombreVendedor: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      Function GetNumero: string;
      Function GetCodigoVendedor: string;
      Function GetNombreVendedor: string;
  end;

implementation

{ TBusquedaPagoComision }

procedure TBusquedaPagoComision.Ajustar;
begin

end;

procedure TBusquedaPagoComision.Mover(Ds: TClientDataSet);
begin
  Numero := Ds.FieldValues['numero'];
  CodigoVendedor := Ds.FieldValues['codigo_vendedor'];
  NombreVendedor := Ds.FieldValues['nombre_vendedor'];
end;

function TBusquedaPagoComision.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,3);
  Params[0] := Numero;
  Params[1] := CodigoVendedor;
  Params[2] := NombreVendedor;
  Result := Params;
end;

function TBusquedaPagoComision.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,3);
  Campos[0] := 'numero';
  Campos[1] := 'codigo_vendedor';
  Campos[2] := 'nombre_vendedor';
  Result := Campos;
end;

function TBusquedaPagoComision.GetCondicion: string;
begin
  Result:= '';
end;

procedure TBusquedaPagoComision.AntesInsertar;
begin

end;

procedure TBusquedaPagoComision.AntesModificar;
begin

end;

procedure TBusquedaPagoComision.AntesEliminar;
begin

end;

procedure TBusquedaPagoComision.DespuesInsertar;
begin

end;

procedure TBusquedaPagoComision.DespuesModificar;
begin

end;

procedure TBusquedaPagoComision.DespuesEliminar;
begin

end;

constructor TBusquedaPagoComision.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('v_busqueda_pago_comision');
  SetLength(ClavesPrimarias,0);
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

function TBusquedaPagoComision.GetCodigoVendedor: string;
begin
  Result := CodigoVendedor;
end;

function TBusquedaPagoComision.GetNombreVendedor: string;
begin
  Result := NombreVendedor;
end;

function TBusquedaPagoComision.GetNumero: string;
begin
  Result := Numero;
end;

end.
