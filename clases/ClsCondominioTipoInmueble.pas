unit ClsCondominioTipoInmueble;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TCondominioTipoInmueble }

  TCondominioTipoInmueble = class(TGenericoBD)
    private
      Codigo: string;
      Descripcion: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigo(varCodigo: string);
      procedure SetDescripcion(varDescripcion: string);
      function GetCodigo: string;
      function GetDescripcion: string;
  end;

var
  Clase: TCondominioTipoInmueble;

implementation

{ TCondominioTipoInmueble }

procedure TCondominioTipoInmueble.Ajustar;
begin

end;

procedure TCondominioTipoInmueble.Mover(Ds: TClientDataSet);
begin
  Codigo := Ds.FieldValues['codigo'];
  Descripcion := Ds.FieldValues['descripcion'];
end;

function TCondominioTipoInmueble.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,2);
  Params[0] := Codigo;
  Params[1] := Descripcion;
  Result := Params;
end;

function TCondominioTipoInmueble.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,2);
  Campos[0] := 'codigo';
  Campos[1] := 'descripcion';
  Result := Campos;
end;

function TCondominioTipoInmueble.GetCondicion: string;
begin
  Result := '"codigo" = ' + QuotedStr(Codigo);
end;

procedure TCondominioTipoInmueble.AntesEliminar;
begin

end;

procedure TCondominioTipoInmueble.AntesInsertar;
begin

end;

procedure TCondominioTipoInmueble.AntesModificar;
begin

end;

procedure TCondominioTipoInmueble.DespuesEliminar;
begin

end;

procedure TCondominioTipoInmueble.DespuesInsertar;
begin

end;

procedure TCondominioTipoInmueble.DespuesModificar;
begin

end;

constructor TCondominioTipoInmueble.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_condominio_tipos_inmuebles');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(2)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "descripcion"=$2 WHERE ("codigo"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("codigo"=$1)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"codigo"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TCondominioTipoInmueble.SetCodigo(varCodigo: string);
begin
  Codigo := varCodigo;
end;

procedure TCondominioTipoInmueble.SetDescripcion(varDescripcion: string);
begin
  Descripcion := varDescripcion;
end;

function TCondominioTipoInmueble.GetCodigo: string;
begin
  Result := Codigo;
end;

function TCondominioTipoInmueble.GetDescripcion: string;
begin
  Result := Descripcion;
end;

end.
