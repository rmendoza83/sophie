unit ClsPagoComision;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TPagoComision }

  TPagoComision = class(TGenericoBD)
    private
      Numero: string;
      CodigoVendedor: string;
      CodigoZona: string;
      CodigoMoneda: string;
      FechaIngreso: TDate;
      FechaPago: TDate;
      FechaInicial: TDate;
      FechaFinal: TDate;
      Total: Double;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetNumero(varNumero: string);
      procedure SetCodigoVendedor(varCodigoVendedor: string);
      procedure SetCodigoZona(varCodigoZona: string);
      procedure SetCodigoMoneda(varCodigoMoneda: string);
      procedure SetFechaIngreso(varFechaIngreso: TDate);
      procedure SetFechaPago(varFechaPago: TDate);
      procedure SetFechaInicial(varFechaInicial: TDate);
      procedure SetFechaFinal(varFechaFinal: TDate);
      procedure SetTotal(varTotal: Double);
      function GetNumero: string;
      function GetCodigoVendedor: string;
      function GetCodigoZona: string;
      function GetCodigoMoneda: string;
      function GetFechaIngreso: TDate;
      function GetFechaPago: TDate;
      function GetFechaInicial: TDate;
      function GetFechaFinal: TDate;
      function GetTotal: Double;
  end;

implementation

uses
  DB,
  ClsConsecutivo;

{ TPagoComision }

procedure TPagoComision.Ajustar;
begin

end;

procedure TPagoComision.Mover(Ds: TClientDataSet);
begin
  Numero := Ds.FieldValues['numero'];
  CodigoVendedor := Ds.FieldValues['codigo_vendedor'];
  CodigoZona:= Ds.FieldValues['codigo_zona'];
  CodigoMoneda:= Ds.FieldValues['codigo_moneda'];
  FechaIngreso := Ds.FieldByName('fecha_ingreso').AsDateTime;
  FechaPago := Ds.FieldByName('fecha_pago').AsDateTime;
  FechaInicial := Ds.FieldByName('fecha_inicial').AsDateTime;
  FechaFinal := Ds.FieldByName('fecha_final').AsDateTime;
  Total:= Ds.FieldValues['total'];
end;

function TPagoComision.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,9);
  Params[0] := Numero;
  Params[1] := CodigoVendedor;
  Params[2] := CodigoZona;
  Params[3] := CodigoMoneda;
  Params[4] := VarFromDateTime(FechaIngreso);
  Params[5] := VarFromDateTime(FechaPago);
  Params[6] := VarFromDateTime(FechaInicial);
  Params[7] := VarFromDateTime(FechaFinal);
  Params[8] := Total;
  Result := Params;
end;

function TPagoComision.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,4);
  Campos[0] := 'desde';
  Campos[1] := 'codigo_vendedor';
  Campos[2] := 'codigo_zona';
  Campos[3] := 'codigo_moneda';
  Result := Campos;
end;

function TPagoComision.GetCondicion: string;
begin
  Result := '"numero" = ' + QuotedStr(Numero) ;
end;

procedure TPagoComision.AntesInsertar;
var
  Consecutivo: TConsecutivo;
begin
  Consecutivo := TConsecutivo.Create;
  Numero := IntToStr(Consecutivo.ObtenerConsecutivo('numero_pago_comision'));
end;

procedure TPagoComision.AntesModificar;
begin

end;

procedure TPagoComision.AntesEliminar;
begin

end;

procedure TPagoComision.DespuesInsertar;
begin

end;

procedure TPagoComision.DespuesModificar;
begin

end;

procedure TPagoComision.DespuesEliminar;
begin

end;

constructor TPagoComision.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_pagos_comisiones');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(9)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "codigo_vendedor"=$2,"codigo_zona"=$3,"codigo_moneda"=$4,"fecha_ingreso"=$5,"fecha_pago"=$6,"fecha_inicial"=$7,"fecha_final"=$8,"total"=$9  WHERE ("numero"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + 'WHERE ("numero"=$1)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"numero"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TPagoComision.SetCodigoMoneda(varCodigoMoneda: string);
begin
  CodigoMoneda := varCodigoMoneda;
end;

procedure TPagoComision.SetCodigoVendedor(varCodigoVendedor: string);
begin
  CodigoVendedor := varCodigoVendedor;
end;

procedure TPagoComision.SetCodigoZona(varCodigoZona: string);
begin
  CodigoZona := varCodigoZona;
end;

procedure TPagoComision.SetFechaFinal(varFechaFinal: TDate);
begin
  FechaFinal := varFechaFinal;
end;

procedure TPagoComision.SetFechaIngreso(varFechaIngreso: TDate);
begin
  FechaIngreso := varFechaIngreso;
end;

procedure TPagoComision.SetFechaInicial(varFechaInicial: TDate);
begin
  FechaInicial := varFechaInicial;
end;

procedure TPagoComision.SetFechaPago(varFechaPago: TDate);
begin
  FechaPago := varFechaPago;
end;

procedure TPagoComision.SetNumero(varNumero: string);
begin
  Numero := varNumero;
end;

procedure TPagoComision.SetTotal(varTotal: Double);
begin
  Total := varTotal;
end;

function TPagoComision.GetCodigoMoneda: string;
begin
  Result := CodigoMoneda;
end;

function TPagoComision.GetCodigoVendedor: string;
begin
  Result := CodigoVendedor;
end;

function TPagoComision.GetCodigoZona: string;
begin
  Result := CodigoZona;
end;

function TPagoComision.GetFechaFinal: TDate;
begin
  Result := FechaFinal;
end;

function TPagoComision.GetFechaIngreso: TDate;
begin
  Result := FechaIngreso;
end;

function TPagoComision.GetFechaInicial: TDate;
begin
  Result := FechaInicial;
end;

function TPagoComision.GetFechaPago: TDate;
begin
  Result := FechaPago;
end;

function TPagoComision.GetNumero: string;
begin
  Result := Numero;
end;

function TPagoComision.GetTotal: Double;
begin
  Result := Total;
end;

end.
