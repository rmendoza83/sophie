unit ClsDocumentoLegal;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TDocumentoLegal }

  TDocumentoLegal = class(TGenericoBD)
    private
      Desde: string;
      NumeroDocumento: string;
      Serie: string;
      NumeroControl: string;
      LoginUsuario: string;
      Fecha: TDate;
      Hora: TTime;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArraystring; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetDesde(varDesde: string);
      procedure SetNumeroDocumento(varNumeroDocumento: string);
      procedure SetSerie(varSerie: string);
      procedure SetNumeroControl(varNumeroControl: string);
      procedure SetLoginUsuario(varLoginUsuario: string);
      procedure SetFecha(varFecha: TDate);
      procedure SetHora(varHora: TTime);
      function GetDesde: string;
      function GetNumeroDocumento: string;
      function GetSerie: string;
      function GetNumeroControl: string;
      function GetLoginUsuario: string;
      function GetFecha: TDate;
      function GetHora: TTime;
  end;

implementation

{ TDocumentoLegal }

procedure TDocumentoLegal.Ajustar;
begin

end;

procedure TDocumentoLegal.Mover(Ds: TClientDataSet);
begin
  Desde := Ds.FieldValues['desde'];
  NumeroDocumento := Ds.FieldValues['numero_documento'];
  Serie := Ds.FieldValues['serie'];
  NumeroControl := Ds.FieldValues['numero_control'];
  LoginUsuario := Ds.FieldValues['login_usuario'];
  Fecha := Ds.FieldValues['fecha'];
  Hora := Ds.FieldValues['hora'];
end;

function TDocumentoLegal.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,7);
  Params[0] := Desde;;
  Params[1] := NumeroDocumento;
  Params[2] := Serie;
  Params[3] := NumeroControl;
  Params[4] := LoginUsuario;
  Params[5] := VarFromDateTime(Fecha);
  Params[6] := VarFromDateTime(Hora);
  Result := Params;
end;

function TDocumentoLegal.GetCampos: TArraystring;
var
  Campos: TArraystring;
begin
  SetLength(Campos,5);
  Campos[0] := 'desde';
  Campos[1] := 'numero_documento';
  Campos[2] := 'serie';
  Campos[3] := 'numero_control';
  Campos[4] := 'login_usuario';
  Result := Campos;
end;

function TDocumentoLegal.GetCondicion: string;
begin
  Result:= '("desde" = ' + QuotedStr(Desde) + ') AND' + '("numero_documento" = ' + QuotedStr(NumeroDocumento) + ')';
end;

procedure TDocumentoLegal.AntesInsertar;
begin

end;

procedure TDocumentoLegal.AntesModificar;
begin

end;

procedure TDocumentoLegal.AntesEliminar;
begin

end;

procedure TDocumentoLegal.DespuesInsertar;
begin

end;

procedure TDocumentoLegal.DespuesModificar;
begin

end;

procedure TDocumentoLegal.DespuesEliminar;
begin

end;

constructor TDocumentoLegal.Create;
var
  ClavesPrimarias: TArraystring;
begin
  inherited Create;
  SetTabla('tbl_documentos_legales');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(7)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "serie"=$3, "numero_control"=$4, "login_usuario"=$5, "fecha"=$6, "hora"=$7' +
                  ' WHERE ("desde"=$1) AND ("numero_documento"=$2)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("desde"=$1) AND ("numero_documento"=$2)');
  SetLength(ClavesPrimarias,4);
  ClavesPrimarias[0] := '"desde"';
  ClavesPrimarias[1] := '"numero_documento"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TDocumentoLegal.SetDesde(varDesde: string);
begin
  Desde := varDesde;
end;

procedure TDocumentoLegal.SetFecha(varFecha: TDate);
begin
  Fecha := varFecha;
end;

procedure TDocumentoLegal.SetHora(varHora: TTime);
begin
  Hora := varHora;
end;

procedure TDocumentoLegal.SetLoginUsuario(varLoginUsuario: string);
begin
  LoginUsuario := varLoginUsuario;
end;

procedure TDocumentoLegal.SetNumeroControl(varNumeroControl: string);
begin
  NumeroControl := varNumeroControl;
end;

procedure TDocumentoLegal.SetNumeroDocumento(varNumeroDocumento: string);
begin
  NumeroDocumento := varNumeroDocumento;
end;

procedure TDocumentoLegal.SetSerie(varSerie: string);
begin
  Serie := varSerie;
end;

function TDocumentoLegal.GetDesde: string;
begin
  Result := Desde;
end;

function TDocumentoLegal.GetFecha: TDate;
begin
  Result := Fecha;
end;

function TDocumentoLegal.GetHora: TTime;
begin
  Result := Hora;
end;

function TDocumentoLegal.GetLoginUsuario: string;
begin
  Result := LoginUsuario;
end;

function TDocumentoLegal.GetNumeroControl: string;
begin
  Result := NumeroControl;
end;

function TDocumentoLegal.GetNumeroDocumento: string;
begin
  Result := NumeroDocumento;
end;

function TDocumentoLegal.GetSerie: string;
begin
  Result := Serie;
end;

end.
