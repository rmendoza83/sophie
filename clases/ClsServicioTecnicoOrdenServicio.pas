unit ClsServicioTecnicoOrdenServicio;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TServicioTecnicoOrdenServicio }

  TServicioTecnicoOrdenServicio = class(TGenericoBD)
    private
      Numero: string;
      CodigoMoneda: string;
      CodigoCliente: string;
      CodigoVendedor: string;
      CodigoZona: string;
      FechaIngreso: TDate;
      FechaEntrega: TDate;
      Concepto: string;
      Observaciones: string;
      MontoNeto: Double;
      MontoDescuento: Double;
      SubTotal: Double;
      Total: Double;
      LoginUsuario: string;
      TiempoIngreso: TDateTime;
      Cerrada: Boolean;
      Facturada: Boolean;
      FechaFacturacion: TDate;
      IdFacturacionCliente: Integer;
      IdOrdenServicio: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArraystring; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetNumero(varNumero: string);
      procedure SetCodigoMoneda(varCodigoMoneda: string);
      procedure SetCodigoCliente(varCodigoCliente: string);
      procedure SetCodigoVendedor(varCodigoVendedor: string);
      procedure SetCodigoZona(varCodigoZona: string);
      procedure SetFechaIngreso(varFechaIngreso: TDate);
      procedure SetFechaEntrega(varFechaEntrega: TDate);
      procedure SetConcepto(varConcepto: string);
      procedure SetObservaciones(varObservaciones: string);
      procedure SetMontoNeto(varMontoNeto: Double);
      procedure SetMontoDescuento(varMontoDescuento: Double);
      procedure SetTotal(varTotal: Double);
      procedure SetLoginUsuario(varLoginUsuario: string);
      procedure SetTiempoIngreso(varTiempodeIngreso: TDateTime);
      procedure SetCerrada(varCerrada: Boolean);
      procedure SetFacturada(varFacturada: Boolean);
      procedure SetFechaFacturacion(varFechaFacturacion: TDate);
      procedure SetIdFacturacionCliente(varIdFacturacionCliente: Integer);
      procedure SetIdOrdenServicio(varIdOrdenServicio: Integer);
      function GetNumero: string;
      function GetCodigoMoneda: string;
      function GetCodigoCliente: string;
      function GetCodigoVendedor: string;
      function GetCodigoZona: string;
      function GetFechaIngreso: TDate;
      function GetFechaEntrega: TDate;
      function GetConcepto: string;
      function GetObservaciones: string;
      function GetMontoNeto: Double;
      function GetMontoDescuento: Double;
      function GetTotal: Double;
      function GetLoginUsuario: string;
      function GetTiempoIngreso: TDateTime;
      function GetCerrada: Boolean;
      function GetFacturada: Boolean;
      function GetFechaFacturacion: TDate;
      function GetIdFacturacionCliente: Integer;
      function GetIdOrdenServicio: Integer;
      function BuscarNumero: Boolean;
      function BuscarCodigoCliente: Boolean;
      function BuscarCodigoVendedor: Boolean;
  end;

implementation

uses
  DB,
  ClsConsecutivo;

{ TServicioTecnicoOrdenServicio }

procedure TServicioTecnicoOrdenServicio.Ajustar;
begin

end;

procedure TServicioTecnicoOrdenServicio.Mover(Ds: TClientDataSet);
begin
  Numero := Ds.FieldValues['numero'];
  CodigoMoneda := Ds.FieldValues['codigo_moneda'];
  CodigoCliente := Ds.FieldValues['codigo_cliente'];
  CodigoVendedor := Ds.FieldValues['codigo_vendedor'];
  CodigoZona := Ds.FieldValues['codigo_zona'];
  FechaIngreso := Ds.FieldByName('fecha_ingreso').AsDateTime;
  FechaEntrega := Ds.FieldByName('fecha_entrega').AsDateTime;
  Concepto := Ds.FieldValues['concepto'];
  Observaciones := Ds.FieldValues['observaciones'];
  MontoNeto := Ds.FieldValues['monto_neto'];
  MontoDescuento := Ds.FieldValues['monto_descuento'];
  SubTotal := Ds.FieldValues['sub_total'];
  Total := Ds.FieldValues['total'];
  LoginUsuario:= Ds.FieldValues['login_usuario'];
  TiempoIngreso:= Ds.FieldByName('tiempo_ingreso').AsDateTime;
  Cerrada := Ds.FieldValues['cerrada'];
  Facturada := Ds.FieldValues['facturada'];
  FechaFacturacion := Ds.FieldByName('fecha_entrega').AsDateTime;
  IdFacturacionCliente := Ds.FieldValues['id_facturacion_cliente'];
  IdOrdenServicio := Ds.FieldValues['id_orden_servicio'];
end;

function TServicioTecnicoOrdenServicio.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,19);
  Params[0] := Numero;
  Params[1] := CodigoMoneda;
  Params[2] := CodigoCliente;
  Params[3] := CodigoVendedor;
  Params[4] := CodigoZona;
  Params[5] := VarFromDateTime(FechaIngreso);
  Params[6] := VarFromDateTime(FechaEntrega);
  Params[7] := Concepto;
  Params[8] := Observaciones;
  Params[9] := MontoNeto;
  Params[10] := MontoDescuento;
  Params[11] := Total;
  Params[12] := LoginUsuario;
  Params[13] := VarFromDateTime(TiempoIngreso);
  Params[14] := Cerrada;
  Params[15] := Facturada;
  Params[16] := VarFromDateTime(FechaFacturacion);
  Params[17] := IdFacturacionCliente;
  Params[18] := IdOrdenServicio;
  Result := Params;
end;

function TServicioTecnicoOrdenServicio.GetCampos: TArraystring;
var
  Campos: TArraystring;
begin
  SetLength(Campos,8);
  Campos[0] := 'numero';
  Campos[1] := 'codigo_moneda';
  Campos[2] := 'codigo_cliente';
  Campos[3] := 'codigo_vendedor';
  Campos[4] := 'codigo_zona';
  Campos[5] := 'concepto';
  Campos[6] := 'observaciones';
  Campos[7] := 'login_usuario';
  Result := Campos;
end;

function TServicioTecnicoOrdenServicio.GetCondicion: string;
begin
  Result:= '"id_orden_servicio" =' + IntToStr(IdOrdenServicio);
end;

procedure TServicioTecnicoOrdenServicio.AntesInsertar;
var
  Consecutivo: TConsecutivo;
begin
  Consecutivo := TConsecutivo.Create;
  Numero := IntToStr(Consecutivo.ObtenerConsecutivo('servicio_tecnico_numero_orden_servicio'));
  IdOrdenServicio := Consecutivo.ObtenerConsecutivo('servicio_tecnico_id_orden_servicio');
end;

procedure TServicioTecnicoOrdenServicio.AntesModificar;
begin

end;

procedure TServicioTecnicoOrdenServicio.AntesEliminar;
begin

end;

procedure TServicioTecnicoOrdenServicio.DespuesInsertar;
begin

end;

procedure TServicioTecnicoOrdenServicio.DespuesModificar;
begin

end;

procedure TServicioTecnicoOrdenServicio.DespuesEliminar;
begin

end;

constructor TServicioTecnicoOrdenServicio.Create;
var
  ClavesPrimarias: TArraystring;
begin
  inherited Create;
  SetTabla ('tbl_servicio_tecnico_orden_servicios');
  SetStrSQLInsert ('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(19)) + ')');
  SetStrSQLUpdate ('UPDATE ' + GetTabla + ' SET "numero"=$1, "codigo_moneda"=$2, "codigo_cliente"=$3, "codigo_vendedor"=$4, "codigo_zona"=$5, "fecha_ingreso"=$6, "fecha_entrega"=$7, "concepto"=$8, "observaciones"=$9, "monto_neto"=$10, ' +
                   '"monto_descuento"=$11, "total"=$12, "login_usuario"=$13, "tiempo_ingreso"=$14, "cerrada"=$15, "facturada"=$16, "fecha_facturacion"=$17, "id_facturacion_cliente"=$18 ' +
                   'WHERE ("id_orden_servicio"=$19)');
  SetStrSQLDelete ('DELETE FROM ' + GetTabla + ' WHERE ("id_orden_servicio"=$19)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"id_orden_servicio"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TServicioTecnicoOrdenServicio.SetNumero(varNumero: string);
begin
  Numero := varNumero;
end;

procedure TServicioTecnicoOrdenServicio.SetCodigoMoneda(varCodigoMoneda: string);
begin
  CodigoMoneda := varCodigoMoneda;
end;

procedure TServicioTecnicoOrdenServicio.SetCodigoCliente(varCodigoCliente: string);
begin
 CodigoCliente := varCodigoCliente;
end;

procedure TServicioTecnicoOrdenServicio.SetCodigoVendedor(varCodigoVendedor: string);
begin
  CodigoVendedor := varCodigoVendedor;
end;

procedure TServicioTecnicoOrdenServicio.SetCodigoZona(varCodigoZona: string);
begin
  CodigoZona := varCodigoZona;
end;

procedure TServicioTecnicoOrdenServicio.SetFechaIngreso(varFechaIngreso: TDate);
begin
  FechaIngreso := varFechaIngreso;
end;

procedure TServicioTecnicoOrdenServicio.SetFechaEntrega(varFechaEntrega: TDate);
begin
  FechaEntrega := varFechaEntrega;
end;

procedure TServicioTecnicoOrdenServicio.SetConcepto(varConcepto: string);
begin
  Concepto := varConcepto;
end;

procedure TServicioTecnicoOrdenServicio.SetObservaciones(varObservaciones: string);
begin
  Observaciones := varObservaciones;
end;

procedure TServicioTecnicoOrdenServicio.SetMontoNeto(varMontoNeto: Double);
begin
  MontoNeto := varMontoNeto;
end;

procedure TServicioTecnicoOrdenServicio.SetMontoDescuento(varMontoDescuento: Double);
begin
  MontoDescuento := varMontoDescuento;
end;

procedure TServicioTecnicoOrdenServicio.SetTotal(varTotal: Double);
begin
  Total := varTotal;
end;

procedure TServicioTecnicoOrdenServicio.SetLoginUsuario(varLoginUsuario: string);
begin
  LoginUsuario := varLoginUsuario;
end;

procedure TServicioTecnicoOrdenServicio.SetTiempoIngreso(varTiempodeIngreso: TDateTime);
begin
  TiempoIngreso := varTiempodeIngreso;
end;

procedure TServicioTecnicoOrdenServicio.SetCerrada(varCerrada: Boolean);
begin
  Cerrada := varCerrada;
end;

procedure TServicioTecnicoOrdenServicio.SetFacturada(varFacturada: Boolean);
begin
  Facturada := varFacturada;
end;

procedure TServicioTecnicoOrdenServicio.SetFechaFacturacion(varFechaFacturacion: TDate);
begin
  FechaFacturacion := varFechaFacturacion;
end;

procedure TServicioTecnicoOrdenServicio.SetIdFacturacionCliente(varIdFacturacionCliente: Integer);
begin
  IdFacturacionCliente := varIdFacturacionCliente;
end;

procedure TServicioTecnicoOrdenServicio.SetIdOrdenServicio(varIdOrdenServicio: Integer);
begin
  IdOrdenServicio := varIdOrdenServicio;
end;

function TServicioTecnicoOrdenServicio.GetNumero: string;
begin
  Result := Numero;
end;

function TServicioTecnicoOrdenServicio.GetCodigoMoneda: string;
begin
  Result := CodigoMoneda;
end;

function TServicioTecnicoOrdenServicio.GetCodigoCliente: string;
begin
  Result := CodigoCliente
end;

function TServicioTecnicoOrdenServicio.GetCodigoVendedor: string;
begin
  Result := CodigoVendedor;
end;

function TServicioTecnicoOrdenServicio.GetCodigoZona: string;
begin
  Result := CodigoZona;
end;

function TServicioTecnicoOrdenServicio.GetFechaIngreso: TDate;
begin
  Result := FechaIngreso;
end;

function TServicioTecnicoOrdenServicio.GetFechaEntrega: TDate;
begin
  Result := FechaEntrega;
end;

function TServicioTecnicoOrdenServicio.GetConcepto: string;
begin
  Result := Concepto;
end;

function TServicioTecnicoOrdenServicio.GetObservaciones: string;
begin
  Result := Observaciones;
end;

function TServicioTecnicoOrdenServicio.GetMontoNeto: Double;
begin
  Result := MontoNeto;
end;

function TServicioTecnicoOrdenServicio.GetMontoDescuento: Double;
begin
  Result := MontoDescuento;
end;

function TServicioTecnicoOrdenServicio.GetTotal: Double;
begin
  Result := Total;
end;

function TServicioTecnicoOrdenServicio.GetLoginUsuario: string;
begin
  Result := LoginUsuario;
end;

function TServicioTecnicoOrdenServicio.GetTiempoIngreso: TDateTime;
begin
  Result := TiempoIngreso;
end;

function TServicioTecnicoOrdenServicio.GetCerrada: Boolean;
begin
  Result := Cerrada;
end;

function TServicioTecnicoOrdenServicio.GetFacturada: Boolean;
begin
  Result := Facturada;
end;

function TServicioTecnicoOrdenServicio.GetFechaFacturacion: TDate;
begin
  Result := FechaFacturacion;
end;

function TServicioTecnicoOrdenServicio.GetIdFacturacionCliente: Integer;
begin
  Result := IdFacturacionCliente;
end;

function TServicioTecnicoOrdenServicio.GetIdOrdenServicio: Integer;
begin
  Result := IdOrdenServicio;
end;

function TServicioTecnicoOrdenServicio.BuscarNumero: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("numero" = ' + QuotedStr(GetNumero) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

function TServicioTecnicoOrdenServicio.BuscarCodigoCliente: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("codigo_cliente" = ' + QuotedStr(GetCodigoCliente) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

function TServicioTecnicoOrdenServicio.BuscarCodigoVendedor: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("codigo_vendedor" = ' + QuotedStr(GetCodigoVendedor) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

end.
