unit ClsServicioTecnicoDetOrdenServicio;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TServicioTecnicoDetOrdenServicio }

  TServicioTecnicoDetOrdenServicio = class(TGenericoBD)
    private
      CodigoServicio: string;
      Descripcion: string;
      Precio: Double;
      PDescuento: Double;
      MontoDescuento: Double;
      Total: Double;
      CodigoTecnico: string;
      Observaciones: string;
      IdOrdenServicio: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigoServicio(varCodigoServicio: string);
      procedure SetDescripcion(varDescripcion: string);
      procedure SetPrecio(varPrecio: Double);
      procedure SetPDescuento(varPDescuento: Double);
      procedure SetMontoDescuento(varMontoDescuento: Double);
      procedure SetTotal(varTotal: Double);
      procedure SetCodigoTecnico(varCodigoTecnico: string);
      procedure SetObservaciones(varObservaciones: string);
      procedure SetIdOrdenServicio(varIdOrdenServicio: Integer);
      function GetCodigoServicio: string;
      function GetDescripcion: string;
      function GetPrecio: Double;
      function GetPDescuento: Double;
      function GetMontoDescuento: Double;
      function GetTotal: Double;
      function GetCodigoTecnico: string;
      function GetObservaciones: string;
      function GetIdOrdenServicio: Integer;
      function ObtenerIdOrdenServicio(varIdOrdenServicio: Integer): TClientDataSet;
      function EliminarIdOrdenServicio(varIdOrdenServicio: Integer): Boolean;
  end;

implementation

uses DB;

{ TServicioTecnicoDetOrdenServicio }

procedure TServicioTecnicoDetOrdenServicio.Ajustar;
begin
  inherited;

end;

procedure TServicioTecnicoDetOrdenServicio.Mover(Ds: TClientDataSet);
begin
  CodigoServicio := Ds.FieldValues['codigo_servicio'];
  Descripcion := Ds.FieldValues['descripcion'];
  Precio := Ds.FieldValues['precio'];
  PDescuento := Ds.FieldValues['p_descuento'];
  MontoDescuento := Ds.FieldValues['monto_descuento'];
  Total := Ds.FieldValues['total'];
  CodigoTecnico := Ds.FieldValues['codigo_tecnico'];
  Observaciones := Ds.FieldValues['observaciones'];
  IdOrdenServicio := Ds.FieldValues['id_orden_servicio'];
end;

function TServicioTecnicoDetOrdenServicio.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,9);
  Params[0] := CodigoServicio;
  Params[1] := Descripcion;
  Params[2] := Precio;
  Params[3] := PDescuento;
  Params[4] := MontoDescuento;
  Params[5] := Total;
  Params[6] := CodigoTecnico;
  Params[7] := Observaciones;
  Params[8] := IdOrdenServicio;
  Result:= Params;
end;

function TServicioTecnicoDetOrdenServicio.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,4);
  Campos[0] := 'codigo_servicio';
  Campos[1] := 'descripcion';
  Campos[2] := 'codigo_tecnico';
  Campos[3] := 'observaciones';
  Result:= Campos;
end;

function TServicioTecnicoDetOrdenServicio.GetCondicion: string;
begin
  Result:= '"id_orden_servicio" =' + IntToStr(IdOrdenServicio) + ' AND ' +
           '"codigo_servicio" =' + QuotedStr(CodigoServicio)
end;

procedure TServicioTecnicoDetOrdenServicio.AntesEliminar;
begin

end;

procedure TServicioTecnicoDetOrdenServicio.AntesInsertar;
begin

end;

procedure TServicioTecnicoDetOrdenServicio.AntesModificar;
begin

end;

procedure TServicioTecnicoDetOrdenServicio.DespuesEliminar;
begin

end;

procedure TServicioTecnicoDetOrdenServicio.DespuesInsertar;
begin

end;

procedure TServicioTecnicoDetOrdenServicio.DespuesModificar;
begin

end;

constructor TServicioTecnicoDetOrdenServicio.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_servicio_tecnico_orden_servicios');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(9)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "descripcion"=$2, "precio"=$3, "p_descuento"=$4, "monto_descuento"=$5, "total"=$6, "codigo_tecnico"=$7, "observaciones"=$8 WHERE ("id_orden_servicio"=$9) AND ("codigo_servicio"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("id_orden_servicio"=$9) AND ("codigo_servicio"=$1)');
  SetLength(ClavesPrimarias,2);
  ClavesPrimarias[0] := '"id_orden_servicio"';
  ClavesPrimarias[1] := '"codigo_servicio"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TServicioTecnicoDetOrdenServicio.SetCodigoServicio(varCodigoServicio: string);
begin
  CodigoServicio := varCodigoServicio;
end;

procedure TServicioTecnicoDetOrdenServicio.SetDescripcion(varDescripcion: string);
begin
  Descripcion := varDescripcion;
end;

procedure TServicioTecnicoDetOrdenServicio.SetPrecio(varPrecio: Double);
begin
  Precio := varPrecio
end;

procedure TServicioTecnicoDetOrdenServicio.SetPDescuento(varPDescuento: Double);
begin
  PDescuento := varPDescuento;
end;

procedure TServicioTecnicoDetOrdenServicio.SetMontoDescuento(varMontoDescuento: Double);
begin
  MontoDescuento := varMontoDescuento;
end;

procedure TServicioTecnicoDetOrdenServicio.SetTotal(varTotal: Double);
begin
  Total := varTotal;
end;

procedure TServicioTecnicoDetOrdenServicio.SetCodigoTecnico(varCodigoTecnico: string);
begin
  CodigoTecnico := varCodigoTecnico;
end;

procedure TServicioTecnicoDetOrdenServicio.SetObservaciones(varObservaciones: string);
begin
  Observaciones := varObservaciones;
end;

procedure TServicioTecnicoDetOrdenServicio.SetIdOrdenServicio(varIdOrdenServicio: Integer);
begin
  IdOrdenServicio := varIdOrdenServicio;
end;

function TServicioTecnicoDetOrdenServicio.GetCodigoServicio: string;
begin
  Result := CodigoServicio;
end;

function TServicioTecnicoDetOrdenServicio.GetDescripcion: string;
begin
  Result := Descripcion;
end;

function TServicioTecnicoDetOrdenServicio.GetPrecio: Double;
begin
  Result := Precio;
end;

function TServicioTecnicoDetOrdenServicio.GetPDescuento: Double;
begin
  Result := PDescuento;
end;

function TServicioTecnicoDetOrdenServicio.GetMontoDescuento: Double;
begin
  Result := MontoDescuento;
end;

function TServicioTecnicoDetOrdenServicio.GetTotal: Double;
begin
  Result := Total;
end;

function TServicioTecnicoDetOrdenServicio.GetCodigoTecnico: string;
begin
  Result := CodigoTecnico;
end;

function TServicioTecnicoDetOrdenServicio.GetObservaciones: string;
begin
  Result := Observaciones;
end;

function TServicioTecnicoDetOrdenServicio.GetIdOrdenServicio: Integer;
begin
  Result := IdOrdenServicio;
end;

function TServicioTecnicoDetOrdenServicio.ObtenerIdOrdenServicio(
  varIdOrdenServicio: Integer): TClientDataSet;
begin
  Result := ObtenerListaCondicionSQL(' WHERE "id_orden_servicio" = ' + IntToStr(varIdOrdenServicio));
end;

function TServicioTecnicoDetOrdenServicio.EliminarIdOrdenServicio(
  varIdOrdenServicio: Integer): Boolean;
begin
  Result := EjecutarSQL('DELETE FROM ' + GetTabla + ' WHERE "id_orden_servicio" = ' + IntToStr(varIdOrdenServicio));
end;

end.
