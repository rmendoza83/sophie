unit ClsConteoInventario;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TConteoInventario }

  TConteoInventario = class(TGenericoBD)
    private
      Numero: string;
      Fecha: TDate;
      CantidadConteo: Integer;
      CodigoZona: string;
      CodigoEstacion: string;
      Observaciones: string;
      IdConteoInventario: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArraystring; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetNumero(varNumero: string);
      procedure SetFecha(varFecha: TDate);
      procedure SetCantidadConteo(varCantidadConteo: Integer);
      procedure SetCodigoZona(varCodigoZona: string);
      procedure SetCodigoEstacion(varCodigoEstacion: string);
      procedure SetObservaciones(varObservaciones: string);
      procedure SetIdConteoInventario(varIdConteoInventario: Integer);
      function GetNumero: string;
      function GetFecha: TDate;
      function GetCantidadConteo: Integer;
      function GetCodigoZona: string;
      function GetCodigoEstacion: string;
      function GetObservaciones: string;
      function GetIdConteoInventario: Integer;
      function BuscarNumero: Boolean;
  end;

implementation

uses
  ClsConsecutivo, DB;

{ TConteoInventario }

procedure TConteoInventario.Ajustar;
begin

end;

procedure TConteoInventario.Mover(Ds: TClientDataSet);
begin
  Numero := Ds.FieldValues['numero'];
  Fecha := Ds.FieldValues['fecha'];
  CantidadConteo := Ds.FieldValues['cantidad_conteo'];
  CodigoZona:= Ds.FieldValues['codigo_zona'];
  CodigoEstacion:= Ds.FieldValues['codigo_estacion'];
  Observaciones:= Ds.FieldValues['observaciones'];
  IdConteoInventario := Ds.FieldValues['id_conteo_inventario'];
end;

function TConteoInventario.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,7);
  Params[0] := Numero;
  Params[1] := VarFromDateTime(Fecha);
  Params[2] := CantidadConteo;
  Params[3] := CodigoZona;
  Params[4] := CodigoEstacion;
  Params[5] := Observaciones;
  Params[6] := IdConteoInventario;
  Result := Params;
end;

function TConteoInventario.GetCampos: TArraystring;
var
  Campos: TArraystring;
begin
  SetLength(Campos,4);
  Campos[0] := 'numero';
  Campos[1] := 'codigo_zona';
  Campos[2] := 'codigo_estacion';
  Campos[3] := 'observaciones';
  Result := Campos;
end;

function TConteoInventario.GetCondicion: string;
begin
  Result:= '"id_conteo_inventario" =' + IntToStr(IdConteoInventario);
end;

procedure TConteoInventario.AntesInsertar;
var
  Consecutivo: TConsecutivo;
begin
  Consecutivo := TConsecutivo.Create;
  Numero := IntToStr(Consecutivo.ObtenerConsecutivo('numero_conteo_inventario'));
  IdConteoInventario := Consecutivo.ObtenerConsecutivo('id_conteo_inventario');
end;

procedure TConteoInventario.AntesModificar;
begin

end;

procedure TConteoInventario.AntesEliminar;
begin

end;

procedure TConteoInventario.DespuesInsertar;
begin

end;

procedure TConteoInventario.DespuesModificar;
begin

end;

procedure TConteoInventario.DespuesEliminar;
begin

end;

constructor TConteoInventario.Create;
var
  ClavesPrimarias: TArraystring;
begin
  inherited Create;
  SetTabla ('tbl_conteos_inventario');
  SetStrSQLInsert ('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(7)) + ')');
  SetStrSQLUpdate ('UPDATE ' + GetTabla + ' SET "numero"=$1, "fecha"=$2, "cantidad_conteo"=$3, "codigo_zona"=$4, "codigo_estacion"=$5, "observaciones"=$6 WHERE ("id_conteo_inventario"=$7)');
  SetStrSQLDelete ('DELETE FROM ' + GetTabla + ' WHERE ("id_conteo_inventario"=$7)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"id_conteo_inventario"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TConteoInventario.SetNumero(varNumero: string);
begin
  Numero := varNumero;
end;

procedure TConteoInventario.SetFecha(varFecha: TDate);
begin
  Fecha := varFecha;
end;

procedure TConteoInventario.SetCantidadConteo(varCantidadConteo: Integer);
begin
  CantidadConteo := varCantidadConteo;
end;

procedure TConteoInventario.SetCodigoZona(varCodigoZona: string);
begin
  CodigoZona := varCodigoZona;
end;

procedure TConteoInventario.SetCodigoEstacion(varCodigoEstacion: string);
begin
  CodigoEstacion := varCodigoEstacion;
end;

procedure TConteoInventario.SetObservaciones(varObservaciones: string);
begin
  Observaciones := varObservaciones;
end;

procedure TConteoInventario.SetIdConteoInventario(varIdConteoInventario: Integer);
begin
  IdConteoInventario := varIdConteoInventario;
end;

function TConteoInventario.GetNumero: string;
begin
  Result := Numero;
end;

function TConteoInventario.GetFecha: TDate;
begin
  Result := Fecha;
end;

function TConteoInventario.GetCantidadConteo: Integer;
begin
  Result := CantidadConteo;
end;

function TConteoInventario.GetCodigoZona: string;
begin
  Result := CodigoZona;
end;

function TConteoInventario.GetCodigoEstacion: string;
begin
  Result := CodigoEstacion;
end;

function TConteoInventario.GetObservaciones: string;
begin
  Result := Observaciones;
end;

function TConteoInventario.GetIdConteoInventario: Integer;
begin
  Result := IdConteoInventario;
end;

function TConteoInventario.BuscarNumero: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("numero" = ' + QuotedStr(GetNumero) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

end.
