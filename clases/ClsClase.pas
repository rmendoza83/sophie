unit ClsClase;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TClase }

  TClase = class(TGenericoBD)
    private
      Codigo: string;
      Descripcion: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigo(varCodigo: string);
      procedure SetDescripcion(varDescripcion: string);
      function GetCodigo: string;
      function GetDescripcion: string;
  end;

var
  Clase: TClase;

implementation

{ TClase }

procedure TClase.Ajustar;
begin

end;

procedure TClase.Mover(Ds: TClientDataSet);
begin
  Codigo := Ds.FieldValues['codigo'];
  Descripcion := Ds.FieldValues['descripcion'];
end;

function TClase.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,2);
  Params[0] := Codigo;
  Params[1] := Descripcion;
  Result := Params;
end;

function TClase.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,2);
  Campos[0] := 'codigo';
  Campos[1] := 'descripcion';
  Result := Campos;
end;

function TClase.GetCondicion: string;
begin
  Result := '"codigo" = ' + QuotedStr(Codigo);
end;

procedure TClase.AntesEliminar;
begin

end;

procedure TClase.AntesInsertar;
begin

end;

procedure TClase.AntesModificar;
begin

end;

procedure TClase.DespuesEliminar;
begin

end;

procedure TClase.DespuesInsertar;
begin

end;

procedure TClase.DespuesModificar;
begin

end;

constructor TClase.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_clases');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(2)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "descripcion"=$2 WHERE ("codigo"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("codigo"=$1)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"codigo"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TClase.SetCodigo(varCodigo: string);
begin
  Codigo := varCodigo;
end;

procedure TClase.SetDescripcion(varDescripcion: string);
begin
  Descripcion := varDescripcion;
end;

function TClase.GetCodigo: string;
begin
  Result := Codigo;
end;

function TClase.GetDescripcion: string;
begin
  Result := Descripcion;
end;

end.
