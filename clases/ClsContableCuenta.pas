unit ClsContableCuenta;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TContableCuenta }

  TContableCuenta = class(TGenericoBD)
    private
      Anno: Integer;
      Mes: Integer;
      Codigo: string;
      CodigoAuxiliar: string;
      Descripcion: string;
      FechaRegistro: TDate;
      FechaApertura: TDate;
      LoginUsuarioApertura: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetAnno(varAnno: Integer);
      procedure SetMes(varMes: Integer);
      procedure SetCodigo(varCodigo: string);
      procedure SetCodigoAuxiliar(varCodigoAuxiliar: string);
      procedure SetDescripcion(varDescripcion: string);
      procedure SetFechaRegistro(varFechaRegistro: TDate);
      procedure SetFechaApertura(varFechaApertura: TDate);
      procedure SetLoginUsuarioApertura(varLoginUsuarioApertura: string);
      function GetAnno: Integer;
      function GetMes: Integer;
      function GetCodigo: string;
      function GetCodigoAuxiliar: string;
      function GetDescripcion: string;
      function GetFechaRegistro: TDate;
      function GetFechaApertura: TDate;
      function GetLoginUsuarioApertura: string;
  end;

var
  ContableCuenta: TContableCuenta;

implementation

uses DB;

{ TContableCuenta }

procedure TContableCuenta.Ajustar;
begin

end;

procedure TContableCuenta.Mover(Ds: TClientDataSet);
begin
  Anno := Ds.FieldValues['anno'];
  Mes := Ds.FieldValues['mes'];
  Codigo := Ds.FieldValues['codigo'];
  CodigoAuxiliar := Ds.FieldValues['codigo_auxiliar'];
  FechaRegistro := Ds.FieldByName('fecha_registro').AsDateTime;
  FechaApertura := Ds.FieldByName('fecha_apertura').AsDateTime;
  LoginUsuarioApertura := Ds.FieldValues['login_usuario_apertura'];
end;

function TContableCuenta.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,8);
  Params[0] := Anno;
  Params[1] := Mes;
  Params[2] := Codigo;
  Params[3] := CodigoAuxiliar;
  Params[4] := Descripcion;
  Params[5] := VarFromDateTime(FechaRegistro);
  Params[6] := VarFromDateTime(FechaApertura);
  Params[7] := LoginUsuarioApertura;
  Result := Params;
end;

function TContableCuenta.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,4);
  Campos[0] := 'codigo';
  Campos[1] := 'codigo_auxiliar';
  Campos[2] := 'descripcion';
  Campos[3] := 'login_usuario_apertura';
  Result := Campos;
end;

function TContableCuenta.GetCondicion: string;
begin
  Result := '"anno" = ' + IntToStr(Anno) + ' AND ' +
            '"mes" = ' + IntToStr(Mes) + ' AND ' +
            '"codigo" = ' + QuotedStr(Codigo);
end;

procedure TContableCuenta.AntesEliminar;
begin

end;

procedure TContableCuenta.AntesInsertar;
begin

end;

procedure TContableCuenta.AntesModificar;
begin

end;

procedure TContableCuenta.DespuesEliminar;
begin

end;

procedure TContableCuenta.DespuesInsertar;
begin

end;

procedure TContableCuenta.DespuesModificar;
begin

end;

constructor TContableCuenta.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_contable_cuentas');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(8)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "codigo_auxiliar"=$4, "descripcion"=$5, "fecha_registro"=$6, "fecha_apertura"=$7, "login_usuario_apertura"=$8 WHERE ("anno"=$1) AND ("mes"=$2) AND ("codigo=$3)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("anno"=$1) AND ("mes"=$2) AND ("codigo=$3)');
  SetLength(ClavesPrimarias,3);
  ClavesPrimarias[0] := '"anno"';
  ClavesPrimarias[1] := '"mes"';
  ClavesPrimarias[2] := '"codigo"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TContableCuenta.SetAnno(varAnno: Integer);
begin
  Anno := varAnno;
end;

procedure TContableCuenta.SetMes(varMes: Integer);
begin
  Mes := varMes;
end;

procedure TContableCuenta.SetCodigo(varCodigo: string);
begin
  Codigo := varCodigo;
end;

procedure TContableCuenta.SetCodigoAuxiliar(varCodigoAuxiliar: string);
begin
  CodigoAuxiliar := varCodigoAuxiliar;
end;

procedure TContableCuenta.SetDescripcion(varDescripcion: string);
begin
  Descripcion := varDescripcion;
end;

procedure TContableCuenta.SetFechaRegistro(varFechaRegistro: TDate);
begin
  FechaRegistro := varFechaRegistro;
end;

procedure TContableCuenta.SetFechaApertura(varFechaApertura: TDate);
begin
  FechaApertura := varFechaApertura;
end;

procedure TContableCuenta.SetLoginUsuarioApertura(varLoginUsuarioApertura: string);
begin
  LoginUsuarioApertura := varLoginUsuarioApertura;
end;

function TContableCuenta.GetAnno: Integer;
begin
  Result := Anno;
end;

function TContableCuenta.GetMes: Integer;
begin
  Result := Mes;
end;

function TContableCuenta.GetCodigo: string;
begin
  Result := Codigo;
end;

function TContableCuenta.GetCodigoAuxiliar: string;
begin
  Result := CodigoAuxiliar;
end;

function TContableCuenta.GetDescripcion: string;
begin
  Result := Descripcion;
end;

function TContableCuenta.GetFechaRegistro: TDate;
begin
  Result := FechaRegistro;
end;

function TContableCuenta.GetFechaApertura: TDate;
begin
  Result := FechaApertura;
end;

function TContableCuenta.GetLoginUsuarioApertura: string;
begin
  Result := LoginUsuarioApertura;
end;

end.

