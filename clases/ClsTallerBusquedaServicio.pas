unit ClsTallerBusquedaServicio;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TTallerBusquedaServicio }

  TTallerBusquedaServicio = class(TGenericoBD)
    private
      Numero: string;
      CodigoCliente: string;
      Rif: string;
      Nit: string;
      RazonSocial: string;
      Contacto: string;
      PlacaVehiculo: string;
      MarcaVehiculo: string;
      ModeloVehiculo: string;
      CodigoMecanico: string;
      NombreMecanico: string;
      IdFacturacionCliente: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      function GetNumero: string;
      function GetCodigoCliente: string;
      function GetRif: string;
      function GetNit: string;
      function GetRazonSocial: string;
      function GetContacto: string;
      function GetPlacaVehiculo: string;
      function GetMarcaVehiculo: string;
      function GetModeloVehiculo: string;
      function GetCodigoMecanico: string;
      function GetNombreMecanico: string;
      function GetIdFacturacionCliente: Integer;
  end;

implementation

{ TTallerBusquedaServicio }

procedure TTallerBusquedaServicio.Ajustar;
begin

end;

procedure TTallerBusquedaServicio.Mover(Ds: TClientDataSet);
begin
  Numero := Ds.FieldValues['numero'];
  CodigoCliente := Ds.FieldValues['codigo_cliente'];
  Rif := Ds.FieldValues['rif'];
  Nit := Ds.FieldValues['nit'];
  RazonSocial := Ds.FieldValues['razon_social'];
  Contacto := Ds.FieldValues['contacto'];
  PlacaVehiculo := Ds.FieldValues['placa_vehiculo'];
  MarcaVehiculo := Ds.FieldValues['marca_vehiculo'];
  ModeloVehiculo := Ds.FieldValues['modelo_vehiculo'];
  CodigoMecanico := Ds.FieldValues['codigo_mecanico'];
  NombreMecanico := Ds.FieldValues['nombre_mecanico'];
  IdFacturacionCliente := Ds.FieldValues['id_facturacion_cliente'];
end;

function TTallerBusquedaServicio.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,12);
  Params[0] := Numero;
  Params[1] := CodigoCliente;
  Params[2] := Rif;
  Params[3] := Nit;
  Params[4] := RazonSocial;
  Params[5] := Contacto;
  Params[6] := PlacaVehiculo;
  Params[7] := MarcaVehiculo;
  Params[8] := ModeloVehiculo;
  Params[9] := CodigoMecanico;
  Params[10] := NombreMecanico;
  Params[11] := IdFacturacionCliente;
  Result := Params;
end;

function TTallerBusquedaServicio.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,11);
  Campos[0] := 'numero';
  Campos[1] := 'codigo_cliente';
  Campos[2] := 'rif';
  Campos[3] := 'nit';
  Campos[4] := 'razon_social';
  Campos[5] := 'contacto';
  Campos[6] := 'placa_vehiculo';
  Campos[7] := 'marca_vehiculo';
  Campos[8] := 'modelo_vehiculo';
  Campos[9] := 'codigo_mecanico';
  Campos[10] := 'nombre_mecanico';
  Result := Campos;
end;

function TTallerBusquedaServicio.GetCondicion: string;
begin
  Result:= '';
end;

procedure TTallerBusquedaServicio.AntesInsertar;
begin

end;

procedure TTallerBusquedaServicio.AntesModificar;
begin

end;

procedure TTallerBusquedaServicio.AntesEliminar;
begin

end;

procedure TTallerBusquedaServicio.DespuesInsertar;
begin

end;

procedure TTallerBusquedaServicio.DespuesModificar;
begin

end;

procedure TTallerBusquedaServicio.DespuesEliminar;
begin

end;

constructor TTallerBusquedaServicio.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('v_taller_busqueda_servicio');
  SetLength(ClavesPrimarias,0);
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

function TTallerBusquedaServicio.GetCodigoCliente: string;
begin
  Result := CodigoCliente;
end;

function TTallerBusquedaServicio.GetCodigoMecanico: string;
begin
  Result := CodigoMecanico;
end;

function TTallerBusquedaServicio.GetContacto: string;
begin
  Result := Contacto;
end;

function TTallerBusquedaServicio.GetNit: string;
begin
  Result := Nit;
end;

function TTallerBusquedaServicio.GetNombreMecanico: string;
begin
  Result := NombreMecanico;
end;

function TTallerBusquedaServicio.GetNumero: string;
begin
  Result := Numero;
end;

function TTallerBusquedaServicio.GetRazonSocial: string;
begin
  Result := RazonSocial;
end;

function TTallerBusquedaServicio.GetRif: string;
begin
  Result := Rif;
end;

function TTallerBusquedaServicio.GetPlacaVehiculo: string;
begin
  Result := PlacaVehiculo;
end;

function TTallerBusquedaServicio.GetMarcaVehiculo: string;
begin
  Result := MarcaVehiculo;
end;

function TTallerBusquedaServicio.GetModeloVehiculo: string;
begin
  Result := ModeloVehiculo;
end;

function TTallerBusquedaServicio.GetIdFacturacionCliente: Integer;
begin
  Result := IdFacturacionCliente;
end;

end.
