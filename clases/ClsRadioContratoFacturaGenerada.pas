unit ClsRadioContratoFacturaGenerada;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TRadioContratoFacturaGenerada}

  TRadioContratoFacturaGenerada = class(TGenericoBD)
    private
      IdRadioContrato: Integer;
      Item: Integer;
      TotalItems: Integer;
      FechaIngreso: TDate;
      Total: Double;
      FechaInicial: TDate;
      FechaFinal: TDate;
      Observaciones: string;
      IdFacturacionCliente: Integer;
      NumeroDocumento: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetIdRadioContrato(varIdRadioContrato: Integer);
      procedure SetItem(varItem: Integer);
      procedure SetTotalItems(varTotalItems: Integer);
      procedure SetFechaIngreso(varFechaIngreso: TDate);
      procedure SetTotal(varTotal: Double);
      procedure SetFechaInicial(varFechaInicial: TDate);
      procedure SetFechaFinal(varFechaFinal: TDate);
      procedure SetObservaciones(varObservaciones: string);
      procedure SetIdFacturacionCliente(varIdFacturacionCliente: Integer);
      procedure SetNumeroDocumento(varNumeroDocumento: string);
      function GetIdRadioContrato: Integer;
      function GetItem: Integer;
      function GetTotalItems: Integer;
      function GetFechaIngreso: TDate;
      function GetTotal: Double;
      function GetFechaInicial: TDate;
      function GetFechaFinal: TDate;
      function GetObservaciones: string;
      function GetIdFacturacionCliente: Integer;
      function GetNumeroDocumento: string;
      function ObtenerIdRadioContrato(varIdRadioContrato: Integer): TClientDataSet;
      function EliminarIdRadioContrato(varIdRadioContrato: Integer): Boolean;
  end;

implementation

{ TRadioContratoFacturaGenerada }

procedure TRadioContratoFacturaGenerada.Ajustar;
begin

end;

procedure TRadioContratoFacturaGenerada.Mover(Ds: TClientDataSet);
begin
  IdRadioContrato := Ds.FieldValues['id_radio_contrato'];
  Item := Ds.FieldValues['item'];
  TotalItems := Ds.FieldValues['total_items'];
  FechaIngreso := Ds.FieldValues['fecha_ingreso'];
  Total := Ds.FieldValues['total'];
  FechaInicial := Ds.FieldValues['fecha_inicial'];
  FechaFinal := Ds.FieldValues['fecha_final'];
  Observaciones := Ds.FieldValues['observaciones'];
  IdFacturacionCliente := Ds.FieldValues['id_facturacion_cliente'];
  NumeroDocumento := Ds.FieldValues['numero_documento'];
end;

function TRadioContratoFacturaGenerada.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,10);
  Params[0] := IdRadioContrato;
  Params[1] := Item;
  Params[2] := TotalItems;
  Params[3] := VarFromDateTime(FechaIngreso);
  Params[4] := Total;
  Params[5] := VarFromDateTime(FechaInicial);
  Params[6] := VarFromDateTime(FechaFinal);
  Params[7] := Observaciones;
  Params[8] := IdFacturacionCliente;
  Params[9] := NumeroDocumento;
  Result := Params;
end;

function TRadioContratoFacturaGenerada.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,2);
  Campos[0] := 'observaciones';
  Campos[1] := 'numero_documento';
  Result := Campos;
end;

function TRadioContratoFacturaGenerada.GetCondicion: string;
begin
  Result := '("id_radio_contrato" = ' + IntToStr(IdRadioContrato) + ') AND ' +
            '("item" = ' + IntToStr(Item) + ')';
end;

procedure TRadioContratoFacturaGenerada.AntesInsertar;
begin

end;

procedure TRadioContratoFacturaGenerada.AntesModificar;
begin

end;

procedure TRadioContratoFacturaGenerada.AntesEliminar;
begin

end;

procedure TRadioContratoFacturaGenerada.DespuesInsertar;
begin

end;

procedure TRadioContratoFacturaGenerada.DespuesModificar;
begin

end;

procedure TRadioContratoFacturaGenerada.DespuesEliminar;
begin

end;

constructor TRadioContratoFacturaGenerada.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_radio_contratos_facturas_generadas');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(10)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "total_items"=$3, "fecha_ingreso"=$4, "total"=$5, "fecha_inicial"=$6, "fecha_final"=$7, "observaciones"=$8, "id_facturacion_cliente"=$9, "numero_documento"=$10 WHERE ("id_radio_contrato"=$1) AND ("item"=$2)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("id_radio_contrato"=$1) AND ("item"=$2)');
  SetLength(ClavesPrimarias,2);
  ClavesPrimarias[0] := '"id_radio_contrato"';
  ClavesPrimarias[1] := '"item"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TRadioContratoFacturaGenerada.SetIdRadioContrato(varIdRadioContrato: Integer);
begin
  IdRadioContrato := varIdRadioContrato;
end;

procedure TRadioContratoFacturaGenerada.SetItem(varItem: Integer);
begin
  Item := varItem;
end;

procedure TRadioContratoFacturaGenerada.SetTotalItems(varTotalItems: Integer);
begin
  TotalItems := varTotalItems;
end;

procedure TRadioContratoFacturaGenerada.SetFechaIngreso(varFechaIngreso: TDate);
begin
  FechaIngreso := varFechaIngreso;
end;

procedure TRadioContratoFacturaGenerada.SetTotal(varTotal: Double);
begin
  Total := varTotal;
end;

procedure TRadioContratoFacturaGenerada.SetFechaInicial(varFechaInicial: TDate);
begin
  FechaInicial := varFechaInicial;
end;

procedure TRadioContratoFacturaGenerada.SetFechaFinal(varFechaFinal: TDate);
begin
  FechaFinal := varFechaFinal;
end;

procedure TRadioContratoFacturaGenerada.SetObservaciones(varObservaciones: string);
begin
  Observaciones := varObservaciones;
end;

procedure TRadioContratoFacturaGenerada.SetIdFacturacionCliente(varIdFacturacionCliente: Integer);
begin
  IdFacturacionCliente := varIdFacturacionCliente;
end;

procedure TRadioContratoFacturaGenerada.SetNumeroDocumento(varNumeroDocumento: string);
begin
  NumeroDocumento := varNumeroDocumento;
end;

function TRadioContratoFacturaGenerada.GetIdRadioContrato: Integer;
begin
  Result := IdRadioContrato;
end;

function TRadioContratoFacturaGenerada.GetItem: Integer;
begin
  Result := Item;
end;

function TRadioContratoFacturaGenerada.GetTotalItems: Integer;
begin
  Result := TotalItems;
end;

function TRadioContratoFacturaGenerada.GetFechaIngreso: TDate;
begin
  Result := FechaIngreso;
end;

function TRadioContratoFacturaGenerada.GetTotal: Double;
begin
  Result := Total;
end;

function TRadioContratoFacturaGenerada.GetFechaInicial: TDate;
begin
  Result := FechaInicial;
end;

function TRadioContratoFacturaGenerada.GetFechaFinal: TDate;
begin
  Result := FechaFinal;
end;

function TRadioContratoFacturaGenerada.GetObservaciones: string;
begin
  Result := Observaciones;
end;

function TRadioContratoFacturaGenerada.GetIdFacturacionCliente: Integer;
begin
  Result := IdFacturacionCliente;
end;

function TRadioContratoFacturaGenerada.GetNumeroDocumento: string;
begin
  Result := NumeroDocumento;
end;

function TRadioContratoFacturaGenerada.ObtenerIdRadioContrato(varIdRadioContrato: Integer): TClientDataSet;
begin
  Result := ObtenerListaCondicionSQL(' WHERE "id_radio_contrato" = ' + IntToStr(varIdRadioContrato));
end;

function TRadioContratoFacturaGenerada.EliminarIdRadioContrato(varIdRadioContrato: Integer): Boolean;
begin
  Result := EjecutarSQL('DELETE FROM ' + GetTabla + ' WHERE "id_radio_contrato" = ' + IntToStr(varIdRadioContrato));
end;

end.
