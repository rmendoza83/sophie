unit ClsDivision;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TDivision }

  TDivision = class(TGenericoBD)
    private
      Codigo: string;
      Descripcion: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigo(varCodigo: string);
      procedure SetDescripcion(varDescripcion: string);
      function GetCodigo: string;
      function GetDescripcion: string;
  end;

implementation

{ TDivision }

procedure TDivision.Ajustar;
begin

end;

procedure TDivision.Mover(Ds: TClientDataSet);
begin
  Codigo := Ds.FieldValues['codigo'];
  Descripcion := Ds.FieldValues['descripcion'];
end;

function TDivision.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,2);
  Params[0] := Codigo;
  Params[1] := Descripcion;
  Result := Params;
end;

function TDivision.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,2);
  Campos[0] := 'codigo';
  Campos[1] := 'descripcion';
  Result := Campos;
end;

function TDivision.GetCondicion: string;
begin
  Result := '"codigo" = ' + QuotedStr(Codigo);
end;

procedure TDivision.AntesEliminar;
begin

end;

procedure TDivision.AntesInsertar;
begin

end;

procedure TDivision.AntesModificar;
begin

end;

procedure TDivision.DespuesEliminar;
begin

end;

procedure TDivision.DespuesInsertar;
begin

end;

procedure TDivision.DespuesModificar;
begin

end;

constructor TDivision.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_divisiones');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(2)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "descripcion"=$2 WHERE ("codigo"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("codigo"=$1)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"codigo"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TDivision.SetCodigo(varCodigo: string);
begin
  Codigo := varCodigo;
end;

procedure TDivision.SetDescripcion(varDescripcion: string);
begin
  Descripcion := varDescripcion;
end;

function TDivision.GetCodigo: string;
begin
  Result := Codigo;
end;

function TDivision.GetDescripcion: string;
begin
  Result := Descripcion;
end;

end.

