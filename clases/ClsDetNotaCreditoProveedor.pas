unit ClsDetNotaCreditoProveedor;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TDetNotaCreditoProveedor }

  TDetNotaCreditoProveedor = class(TGenericoBD)
    private
      CodigoProducto: string;
      Referencia: string;
      Descripcion: string;
      Cantidad: Double;
      CantidadPendiente: Double;
      Precio: Double;
      PDescuento: Double;
      MontoDescuento: Double;
      Impuesto: Double;
      PIva: Double;
      Total: Double;
      IdNotaCreditoProveedor: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigoProducto(varCodigoProducto: string);
      procedure SetReferencia(varReferencia: string);
      procedure SetDescripcion(varDescripcion: string);
      procedure SetCantidad(varCantidad: Double);
      procedure SetCantidadPendiente(varCantidadPendiente: Double);
      procedure SetPrecio(varPrecio: Double);
      procedure SetPDescuento(varPDescuento: Double);
      procedure SetMontoDescuento(varMontoDescuento: Double);
      procedure SetImpuesto(varImpuesto: Double);
      procedure SetPIva(varPIva: Double);
      procedure SetTotal(varTotal: Double);
      procedure SetIdNotaCreditoProveedor(varIdNotaCreditoProveedor: Integer);
      function GetCodigoProducto: string;
      function GetReferencia: string;
      function GetDescripcion: string;
      function GetCantidad: Double;
      function GetCantidadPendiente: Double;
      function GetPrecio: Double;
      function GetPDescuento: Double;
      function GetMontoDescuento: Double;
      function GetImpuesto: Double;
      function GetPIva: Double;
      function GetTotal: Double;
      function GetIdNotaCreditoProveedor: Integer;
      function ObtenerIdNotaCreditoProveedor(varIdNotaCreditoProveedor: Integer): TClientDataSet;
      function EliminarIdNotaCreditoProveedor(varIdNotaCreditoProveedor: Integer): Boolean;
  end;

implementation

uses DB;

{ TDetNotaCreditoProveedor }

procedure TDetNotaCreditoProveedor.Ajustar;
begin

end;

procedure TDetNotaCreditoProveedor.Mover(Ds: TClientDataSet);
begin
  CodigoProducto := Ds.FieldValues['codigo_producto'];
  Referencia := Ds.FieldValues['referencia'];
  Descripcion := Ds.FieldValues['descripcion'];
  Cantidad := Ds.FieldValues['cantidad'];
  CantidadPendiente := Ds.FieldValues['cantidad_pendiente'];
  Precio := Ds.FieldValues['precio'];
  PDescuento := Ds.FieldValues['p_descuento'];
  MontoDescuento := Ds.FieldValues['monto_descuento'];
  Impuesto := Ds.FieldValues['impuesto'];
  PIva := Ds.FieldValues['p_iva'];
  Total := Ds.FieldValues['total'];
  IdNotaCreditoProveedor := Ds.FieldValues['id_nota_credito_proveedor'];
end;

function TDetNotaCreditoProveedor.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,12);
  Params[0] := CodigoProducto;
  Params[1] := Referencia;
  Params[2] := Descripcion;
  Params[3] := Cantidad;
  Params[4] := CantidadPendiente;
  Params[5] := Precio;
  Params[6] := PDescuento;
  Params[7] := MontoDescuento;
  Params[8] := Impuesto;
  Params[9] := PIva;
  Params[10] := Total;
  Params[11] := IdNotaCreditoProveedor;
  Result:= Params;
end;

function TDetNotaCreditoProveedor.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,3);
  Campos[0] := 'codigo_producto';
  Campos[1] := 'referencia';
  Campos[2] := 'descripcion';
  Result:= Campos;
end;

function TDetNotaCreditoProveedor.GetCondicion: string;
begin
  Result:= '"id_nota_credito_proveedor" =' + IntToStr(IdNotaCreditoProveedor) + ' AND "codigo_producto" =' + QuotedStr(CodigoProducto);
end;

procedure TDetNotaCreditoProveedor.AntesInsertar;
begin

end;

procedure TDetNotaCreditoProveedor.AntesModificar;
begin

end;

procedure TDetNotaCreditoProveedor.AntesEliminar;
begin

end;

procedure TDetNotaCreditoProveedor.DespuesInsertar;
begin

end;

procedure TDetNotaCreditoProveedor.DespuesModificar;
begin

end;

procedure TDetNotaCreditoProveedor.DespuesEliminar;
begin

end;

constructor TDetNotaCreditoProveedor.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_det_nota_credito_proveedores');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(12)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "referencia"=$2,"descripcion"=$3,"cantidad"=$4,"cantidad_pendiente"=$5,"precio"=$6,"p_descuento"=$7,"monto_descuento"=$8,"impuesto"=$9,"p_iva"=$10,"total"=$11 WHERE ("id_nota_credito_proveedor"=$12) AND ("codigo_producto"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("id_nota_credito_proveedor"=$12) AND ("codigo_producto"=$1)');
  SetLength(ClavesPrimarias,2);
  ClavesPrimarias[0] := '"id_nota_credito_proveedor"';
  ClavesPrimarias[1] := '"codigo_producto"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TDetNotaCreditoProveedor.SetCantidad(varCantidad: Double);
begin
  Cantidad := varCantidad;
end;

procedure TDetNotaCreditoProveedor.SetCantidadPendiente(varCantidadPendiente: Double);
begin
  CantidadPendiente := varCantidadPendiente;
end;

procedure TDetNotaCreditoProveedor.SetCodigoProducto(varCodigoProducto: string);
begin
  CodigoProducto := varCodigoProducto;
end;

procedure TDetNotaCreditoProveedor.SetDescripcion(varDescripcion: string);
begin
  Descripcion := varDescripcion;
end;

procedure TDetNotaCreditoProveedor.SetIdNotaCreditoProveedor(varIdNotaCreditoProveedor: Integer);
begin
  IdNotaCreditoProveedor := varIdNotaCreditoProveedor;
end;

procedure TDetNotaCreditoProveedor.SetImpuesto(varImpuesto: Double);
begin
  Impuesto := varImpuesto;
end;

procedure TDetNotaCreditoProveedor.SetMontoDescuento(varMontoDescuento: Double);
begin
  MontoDescuento := varMontoDescuento;
end;

procedure TDetNotaCreditoProveedor.SetPDescuento(varPDescuento: Double);
begin
  PDescuento := varPDescuento;
end;

procedure TDetNotaCreditoProveedor.SetPIva(varPIva: Double);
begin
  PIva := varPIva;
end;

procedure TDetNotaCreditoProveedor.SetPrecio(varPrecio: Double);
begin
  Precio := varPrecio;
end;

procedure TDetNotaCreditoProveedor.SetReferencia(varReferencia: string);
begin
  Referencia := varReferencia;
end;

procedure TDetNotaCreditoProveedor.SetTotal(varTotal: Double);
begin
  Total := varTotal;
end;

function TDetNotaCreditoProveedor.GetCantidad: Double;
begin
  Result := Cantidad;
end;

function TDetNotaCreditoProveedor.GetCantidadPendiente: Double;
begin
  Result := CantidadPendiente;
end;

function TDetNotaCreditoProveedor.GetCodigoProducto: string;
begin
  Result := CodigoProducto;
end;

function TDetNotaCreditoProveedor.GetDescripcion: string;
begin
  Result := Descripcion;
end;

function TDetNotaCreditoProveedor.GetIdNotaCreditoProveedor: Integer;
begin
  Result := IdNotaCreditoProveedor;
end;

function TDetNotaCreditoProveedor.GetImpuesto: Double;
begin
  Result := Impuesto;
end;

function TDetNotaCreditoProveedor.GetMontoDescuento: Double;
begin
  Result := MontoDescuento;
end;

function TDetNotaCreditoProveedor.GetPDescuento: Double;
begin
  Result := PDescuento;
end;

function TDetNotaCreditoProveedor.GetPIva: Double;
begin
  Result := PIva;
end;

function TDetNotaCreditoProveedor.GetPrecio: Double;
begin
  Result := Precio;
end;

function TDetNotaCreditoProveedor.GetReferencia: string;
begin
  Result := Referencia;
end;

function TDetNotaCreditoProveedor.GetTotal: Double;
begin
  Result := Total;
end;

function TDetNotaCreditoProveedor.ObtenerIdNotaCreditoProveedor(varIdNotaCreditoProveedor: Integer): TClientDataSet;
begin
  Result := ObtenerListaCondicionSQL(' WHERE "id_nota_credito_proveedor" = ' + IntToStr(varIdNotaCreditoProveedor));
end;

function TDetNotaCreditoProveedor.EliminarIdNotaCreditoProveedor(varIdNotaCreditoProveedor: Integer): Boolean;
begin
  Result := EjecutarSQL('DELETE FROM ' + GetTabla + ' WHERE "id_nota_credito_proveedor" = ' + IntToStr(varIdNotaCreditoProveedor));
end;

end.
