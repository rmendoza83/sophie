unit ClsBusquedaVendedor;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TBusquedaVendedor }

  TBusquedaVendedor = class(TGenericoBD)
    private
      Codigo: String;
      Descripcion: String;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      Function GetCodigo: String;
      Function GetDescripcion: String;
  end;

implementation

{ TBusquedaVendedor }

procedure TBusquedaVendedor.Ajustar;
begin

end;

procedure TBusquedaVendedor.Mover(Ds: TClientDataSet);
begin
  Codigo := Ds.FieldValues['codigo'];
  Descripcion:= Ds.FieldValues['descripcion'];
end;

function TBusquedaVendedor.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,2);
  Params[0] := Codigo;
  Params[1] := Descripcion;
  Result := Params;
end;

function TBusquedaVendedor.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,2);
  Campos[0] := 'codigo';
  Campos[1] := 'descripcion';
  Result := Campos;
end;

function TBusquedaVendedor.GetCondicion: string;
begin
  Result:= '';
end;

procedure TBusquedaVendedor.AntesInsertar;
begin

end;

procedure TBusquedaVendedor.AntesModificar;
begin

end;

procedure TBusquedaVendedor.AntesEliminar;
begin

end;

procedure TBusquedaVendedor.DespuesInsertar;
begin

end;

procedure TBusquedaVendedor.DespuesModificar;
begin

end;

procedure TBusquedaVendedor.DespuesEliminar;
begin

end;

constructor TBusquedaVendedor.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('v_busqueda_vendedor');
  SetLength(ClavesPrimarias,0);
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

function TBusquedaVendedor.GetCodigo: String;
begin
  Result := Codigo;
end;

function TBusquedaVendedor.GetDescripcion: String;
begin
  Result := Descripcion;
end;

end.
