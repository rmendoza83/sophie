unit ClsNotaCreditoProveedor;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TNotaCreditoProveedor }

  TNotaCreditoProveedor = class(TGenericoBD)
    private
      Numero: string;
      CodigoMoneda: string;
      CodigoProveedor: string;
      CodigoZona: string;
      CodigoEstacion: string;
      FechaIngreso: TDate;
      FechaRecepcion: TDate;
      FechaContable: TDate;
      FechaLibros: TDate;
      Concepto: string;
      Observaciones: string;
      MontoNeto: Double;
      MontoDescuento: Double;
      SubTotal: Double;
      Impuesto: Double;
      Total: Double;
      PDescuento: Double;
      PIva: Double;
      TasaDolar: Double;
      FormaLibre: Boolean;
      LoginUsuario: string;
      TiempoIngreso: TDateTime;
      IdNotaCreditoProveedor: Integer;
      IdPago: Integer;
      IdMovimientoInventario: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArraystring; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetNumero(varNumero: string);
      procedure SetCodigoMoneda(varCodigoMoneda: string);
      procedure SetCodigoProveedor(varCodigoProveedor: string);
      procedure SetCodigoZona(varCodigoZona: string);
      procedure SetCodigoEstacion(varCodigoEstacion: string);
      procedure SetFechaIngreso(varFechaIngreso: TDate);
      procedure SetFechaRecepcion(varFechaRecepcion: TDate);
      procedure SetFechaContable(varFechaContable: TDate);
      procedure SetFechaLibros(varFechalibros: TDate);
      procedure SetConcepto(varConcepto: string);
      procedure SetObservaciones(varObservaciones: string);
      procedure SetMontoNeto(varMontoNeto: Double);
      procedure SetMontoDescuento(varMontoDescuento: Double);
      procedure SetSubTotal(varSubTotal: Double);
      procedure SetImpuesto(varImpuesto: Double);
      procedure SetTotal(varTotal: Double);
      procedure SetPDescuento(varPDescuento: Double);
      procedure SetPIva(varPIva: Double);
      procedure SetTasaDolar(varTasaDolar: Double);
      procedure SetFormaLibre(varFormaLibre: Boolean);
      procedure SetLoginUsuario(varLoginUsuario: string);
      procedure SetTiempoIngreso(varTiempodeIngreso: TDateTime);
      procedure SetIdNotaCreditoProveedor(varIdNotaCreditoProveedor: Integer);
      procedure SetIdPago(varIdPago: Integer);
      procedure SetIdMovimientoInventario(varIdMovimientoInventario: Integer);
      function GetNumero: string;
      function GetCodigoMoneda: string;
      function GetCodigoProveedor: string;
      function GetCodigoZona: string;
      function GetCodigoEstacion: string;
      function GetFechaIngreso: TDate;
      function GetFechaRecepcion: TDate;
      function GetFechaContable: TDate;
      function GetFechaLibros: TDate;
      function GetConcepto: string;
      function GetObservaciones: string;
      function GetMontoNeto: Double;
      function GetMontoDescuento: Double;
      function GetSubTotal: Double;
      function GetImpuesto: Double;
      function GetTotal: Double;
      function GetPDescuento: Double;
      function GetPIva: Double;
      function GetTasaDolar: Double;
      function GetFormaLibre: Boolean;
      function GetLoginUsuario: string;
      function GetTiempoIngreso: TDateTime;
      function GetIdNotaCreditoProveedor: Integer;
      function GetIdPago: Integer;
      function GetIdMovimientoInventario: Integer;
      function BuscarNumero: Boolean;
      function BuscarCodigoProveedor: Boolean;
  end;

implementation

uses
  DB,
  ClsConsecutivo,
  ClsEsquemaPago;

{ TNotaCreditoProveedor }

procedure TNotaCreditoProveedor.Ajustar;
begin

end;

procedure TNotaCreditoProveedor.Mover(Ds: TClientDataSet);
begin
  Numero:= Ds.FieldValues['numero'];
  CodigoMoneda:= Ds.FieldValues['codigo_moneda'];
  CodigoProveedor:= Ds.FieldValues['codigo_proveedor'];
  CodigoZona:= Ds.FieldValues['codigo_zona'];
  CodigoEstacion:= Ds.FieldValues['codigo_estacion'];
  FechaIngreso:= Ds.FieldByName('fecha_ingreso').AsDateTime;
  FechaRecepcion:= Ds.FieldByName('fecha_recepcion').AsDateTime;
  FechaContable:= Ds.FieldByName('fecha_contable').AsDateTime;
  FechaLibros:=  Ds.FieldByName('fecha_libros').AsDateTime;
  Concepto:= Ds.FieldValues['concepto'];
  Observaciones:= Ds.FieldValues['observaciones'];
  MontoNeto:= Ds.FieldValues['monto_neto'];
  MontoDescuento:= Ds.FieldValues['monto_descuento'];
  SubTotal:= Ds.FieldValues['sub_total'];
  Impuesto:= Ds.FieldValues['impuesto'];
  Total:= Ds.FieldValues['total'];
  PDescuento:= Ds.FieldValues['p_descuento'];
  PIva:= Ds.FieldValues['p_iva'];
  TasaDolar:= Ds.FieldValues['tasa_dolar'];
  FormaLibre:= Ds.FieldValues['forma_libre'];
  LoginUsuario:= Ds.FieldValues['login_usuario'];
  TiempoIngreso:= Ds.FieldByName('tiempo_ingreso').AsDateTime;
  IdNotaCreditoProveedor:= Ds.FieldValues['id_nota_credito_proveedor'];
  IdPago:= Ds.FieldValues['id_pago'];
  IdMovimientoInventario:= Ds.FieldValues['id_movimiento_inventario'];
end;

function TNotaCreditoProveedor.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,25);
  Params[0] := Numero;
  Params[1] := CodigoMoneda;
  Params[2] := CodigoProveedor;
  Params[3] := CodigoZona;
  Params[4] := CodigoEstacion;
  Params[5] := VarFromDateTime(FechaIngreso);
  Params[6] := VarFromDateTime(FechaRecepcion);
  Params[7] := VarFromDateTime(FechaContable);
  Params[8] := VarFromDateTime(FechaLibros);
  Params[9] := Concepto;
  Params[10] := Observaciones;
  Params[11] := MontoNeto;
  Params[12] := MontoDescuento;
  Params[13] := SubTotal;
  Params[14] := Impuesto;
  Params[15] := Total;
  Params[16] := PDescuento;
  Params[17] := PIva;
  Params[18] := TasaDolar;
  Params[19] := FormaLibre;
  Params[20] := LoginUsuario;
  Params[21] := VarFromDateTime(TiempoIngreso);
  Params[22] := IdNotaCreditoProveedor;
  Params[23] := IdPago;
  Params[24] := IdMovimientoInventario;
  Result := Params;
end;

function TNotaCreditoProveedor.GetCampos: TArraystring;
var
  Campos: TArraystring;
begin
  SetLength(Campos,8);
  Campos[0] := 'numero';
  Campos[1] := 'codigo_moneda';
  Campos[2] := 'codigo_proveedor';
  Campos[3] := 'codigo_zona';
  Campos[4] := 'codigo_estacion';
  Campos[5] := 'concepto';
  Campos[6] := 'observaciones';
  Campos[7] := 'login_usuario';
  Result := Campos;
end;

function TNotaCreditoProveedor.GetCondicion: string;
begin
  Result:= '"id_nota_credito_proveedor" =' + IntToStr(IdNotaCreditoProveedor);
end;

procedure TNotaCreditoProveedor.AntesInsertar;
var
  Consecutivo: TConsecutivo;
begin
  Consecutivo := TConsecutivo.Create;
  //Numero := IntToStr(Consecutivo.ObtenerConsecutivo('numero_nota_credito_proveedor'));
  IdNotaCreditoProveedor := Consecutivo.ObtenerConsecutivo('id_nota_credito_proveedor');
  if (not FormaLibre) then
  begin
    IdMovimientoInventario := Consecutivo.ObtenerConsecutivo('id_movimiento_inventario');
  end;
  IdPago := Consecutivo.ObtenerConsecutivo('id_pago');
end;

procedure TNotaCreditoProveedor.AntesModificar;
begin

end;

procedure TNotaCreditoProveedor.AntesEliminar;
begin

end;

procedure TNotaCreditoProveedor.DespuesInsertar;
begin

end;

procedure TNotaCreditoProveedor.DespuesModificar;
begin

end;

procedure TNotaCreditoProveedor.DespuesEliminar;
begin

end;

constructor TNotaCreditoProveedor.Create;
var
  ClavesPrimarias: TArraystring;
begin
  inherited Create;
  SetTabla('tbl_nota_credito_proveedores');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(25)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "numero"=$1, "codigo_moneda"=$2, "codigo_proveedor"=$3, "codigo_zona"=$4, "codigo_estacion"=$5, "fecha_ingreso"=$6, "fecha_recepcion"=$7, ' +
                  '"fecha_contable"=$8, "fecha_libros"=$9, "concepto"=$10, "observaciones"=$11, "monto_neto"=$12, "monto_descuento"=$13, "sub_total"=$14, "impuesto"=$15, "total"=$16, "p_descuento"=$17, "p_iva"=$18, ' +
                  '"tasa_dolar"=$19, "forma_libre"=$20, "login_usuario"=$21, "tiempo_ingreso"=$22, "id_pago"=$24, "id_movimiento_inventario"=$25 ' +
                  'WHERE ("id_nota_credito_proveedor"=$23)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("id_nota_credito_proveedor"=$23');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"id_nota_credito_proveedor"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TNotaCreditoProveedor.SetCodigoEstacion(varCodigoEstacion: string);
begin
   CodigoEstacion := varCodigoEstacion;
end;

procedure TNotaCreditoProveedor.SetCodigoMoneda(varCodigoMoneda: string);
begin
  CodigoMoneda := varCodigoMoneda;
end;

procedure TNotaCreditoProveedor.SetCodigoProveedor(varCodigoProveedor: string);
begin
  CodigoProveedor := varCodigoProveedor;
end;

procedure TNotaCreditoProveedor.SetCodigoZona(varCodigoZona: string);
begin
  CodigoZona := varCodigoZona;
end;

procedure TNotaCreditoProveedor.SetConcepto(varConcepto: string);
begin
  Concepto := varConcepto;
end;

procedure TNotaCreditoProveedor.SetFechaContable(varFechaContable: TDate);
begin
  FechaContable := varFechaContable;
end;

procedure TNotaCreditoProveedor.SetFechaIngreso(varFechaIngreso: TDate);
begin
  FechaIngreso := varFechaIngreso;
end;

procedure TNotaCreditoProveedor.SetFechaLibros(varFechalibros: TDate);
begin
  FechaLibros := varFechalibros;
end;

procedure TNotaCreditoProveedor.SetFechaRecepcion(varFechaRecepcion: TDate);
begin
  FechaRecepcion := varFechaRecepcion;
end;

procedure TNotaCreditoProveedor.SetFormaLibre(varFormaLibre: Boolean);
begin
  FormaLibre := varFormaLibre;
end;

procedure TNotaCreditoProveedor.SetIdMovimientoInventario(varIdMovimientoInventario: Integer);
begin
  IdMovimientoInventario := varIdMovimientoInventario;
end;

procedure TNotaCreditoProveedor.SetIdNotaCreditoProveedor(varIdNotaCreditoProveedor: Integer);
begin
  IdNotaCreditoProveedor := varIdNotaCreditoProveedor;
end;

procedure TNotaCreditoProveedor.SetIdPago(varIdPago: Integer);
begin
  IdPago := varIdPago;
end;

procedure TNotaCreditoProveedor.SetImpuesto(varImpuesto: Double);
begin
  Impuesto := varImpuesto;
end;

procedure TNotaCreditoProveedor.SetLoginUsuario(varLoginUsuario: string);
begin
  LoginUsuario := varLoginUsuario;
end;

procedure TNotaCreditoProveedor.SetMontoDescuento(varMontoDescuento: Double);
begin
  MontoDescuento := varMontoDescuento;
end;

procedure TNotaCreditoProveedor.SetMontoNeto(varMontoNeto: Double);
begin
  MontoNeto := varMontoNeto;
end;

procedure TNotaCreditoProveedor.SetNumero(varNumero: string);
begin
  Numero := varNumero;
end;

procedure TNotaCreditoProveedor.SetObservaciones(varObservaciones: string);
begin
  Observaciones := varObservaciones;
end;

procedure TNotaCreditoProveedor.SetPDescuento(varPDescuento: Double);
begin
  PDescuento := varPDescuento;
end;

procedure TNotaCreditoProveedor.SetPIva(varPIva: Double);
begin
  PIva := varPIva;
end;

procedure TNotaCreditoProveedor.SetSubTotal(varSubTotal: Double);
begin
  SubTotal := varSubTotal;
end;

procedure TNotaCreditoProveedor.SetTasaDolar(varTasaDolar: Double);
begin
  TasaDolar := varTasaDolar;
end;

procedure TNotaCreditoProveedor.SetTiempoIngreso(varTiempodeIngreso: TDateTime);
begin
  TiempoIngreso := varTiempodeIngreso;
end;

procedure TNotaCreditoProveedor.SetTotal(varTotal: Double);
begin
  Total := varTotal;
end;

function TNotaCreditoProveedor.GetCodigoEstacion: string;
begin
  Result := CodigoEstacion;
end;

function TNotaCreditoProveedor.GetCodigoMoneda: string;
begin
  Result := CodigoMoneda;
end;

function TNotaCreditoProveedor.GetCodigoProveedor: string;
begin
  Result := CodigoProveedor;
end;

function TNotaCreditoProveedor.GetCodigoZona: string;
begin
  Result := CodigoZona;
end;

function TNotaCreditoProveedor.GetConcepto: string;
begin
  Result := Concepto;
end;

function TNotaCreditoProveedor.GetFechaContable: TDate;
begin
  Result := FechaContable;
end;

function TNotaCreditoProveedor.GetFechaIngreso: TDate;
begin
  Result := FechaIngreso;
end;

function TNotaCreditoProveedor.GetFechaLibros: TDate;
begin
  Result := FechaLibros;
end;

function TNotaCreditoProveedor.GetFechaRecepcion: TDate;
begin
  Result := FechaRecepcion;
end;

function TNotaCreditoProveedor.GetFormaLibre: Boolean;
begin
  Result := FormaLibre;
end;

function TNotaCreditoProveedor.GetIdMovimientoInventario: Integer;
begin
  Result := IdMovimientoInventario;
end;

function TNotaCreditoProveedor.GetIdNotaCreditoProveedor: Integer;
begin
  Result := IdNotaCreditoProveedor;
end;

function TNotaCreditoProveedor.GetIdPago: Integer;
begin
  Result := IdPago;
end;

function TNotaCreditoProveedor.GetImpuesto: Double;
begin
  Result := Impuesto;
end;

function TNotaCreditoProveedor.GetLoginUsuario: string;
begin
  Result := LoginUsuario;
end;

function TNotaCreditoProveedor.GetMontoDescuento: Double;
begin
  Result := MontoDescuento;
end;

function TNotaCreditoProveedor.GetMontoNeto: Double;
begin
  Result := MontoNeto;
end;

function TNotaCreditoProveedor.GetNumero: string;
begin
  Result := Numero;
end;

function TNotaCreditoProveedor.GetObservaciones: string;
begin
  Result := Observaciones;
end;

function TNotaCreditoProveedor.GetPDescuento: Double;
begin
  Result := PDescuento;
end;

function TNotaCreditoProveedor.GetPIva: Double;
begin
  Result := PIva;
end;

function TNotaCreditoProveedor.GetSubTotal: Double;
begin
  Result := SubTotal;
end;

function TNotaCreditoProveedor.GetTasaDolar: Double;
begin
  Result := TasaDolar;
end;

function TNotaCreditoProveedor.GetTiempoIngreso: TDateTime;
begin
  Result := TiempoIngreso;
end;

function TNotaCreditoProveedor.GetTotal: Double;
begin
  Result := Total;
end;

function TNotaCreditoProveedor.BuscarNumero: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("numero" = ' + QuotedStr(GetNumero) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

function TNotaCreditoProveedor.BuscarCodigoProveedor: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("codigo_proveedor" = ' + QuotedStr(GetCodigoProveedor) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

end.
