unit ClsCondominioDetalleGastoOrdinario;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TCondominioDetalleGastoOrdinario }

  TCondominioDetalleGastoOrdinario = class(TGenericoBD)
    private
      CodigoConjuntoAdministracion: string;
      CodigoGasto: string;
      Descripcion: string;
      Monto: Double;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigoConjuntoAdministracion(varCodigoConjuntoAdministracion: string);
      procedure SetCodigoGasto(varCodigoGasto: string);
      procedure SetDescripcion(varDescripcion: string);
      procedure SetMonto(varMonto: Double);
      function GetCodigoConjuntoAdministracion: string;
      function GetCodigoGasto: string;
      function GetMonto: Double;
      function GetDescripcion: string;
      function ObtenerCodigoConjuntoAdministracion(varCodigoConjuntoAdministracion: string): TClientDataSet;
  end;

var
  Clase: TCondominioDetalleGastoOrdinario;

implementation

{ TCondominioDetalleGastoOrdinario }

procedure TCondominioDetalleGastoOrdinario.Ajustar;
begin

end;

procedure TCondominioDetalleGastoOrdinario.Mover(Ds: TClientDataSet);
begin
  CodigoConjuntoAdministracion := Ds.FieldValues['codigo_conjunto_administracion'];
  CodigoGasto := Ds.FieldValues['codigo_gasto'];
  Descripcion := Ds.FieldValues['descripcion'];
  Monto := Ds.FieldValues['monto'];
end;

function TCondominioDetalleGastoOrdinario.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,4);
  Params[0] := CodigoConjuntoAdministracion;
  Params[1] := CodigoGasto;
  Params[2] := Descripcion;
  Params[3] := Monto;
  Result := Params;
end;

function TCondominioDetalleGastoOrdinario.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,3);
  Campos[0] := 'codigo_conjunto_administracion';
  Campos[1] := 'codigo_gasto';
  Campos[2] := 'descripcion';
  Result := Campos;
end;

function TCondominioDetalleGastoOrdinario.GetCondicion: string;
begin
  Result := '"codigo_conjunto_administracion" = ' + QuotedStr(CodigoConjuntoAdministracion) + ' AND ' +
            '"codigo_gasto" = ' + QuotedStr(CodigoGasto);
end;

procedure TCondominioDetalleGastoOrdinario.AntesEliminar;
begin

end;

procedure TCondominioDetalleGastoOrdinario.AntesInsertar;
begin

end;

procedure TCondominioDetalleGastoOrdinario.AntesModificar;
begin

end;

procedure TCondominioDetalleGastoOrdinario.DespuesEliminar;
begin

end;

procedure TCondominioDetalleGastoOrdinario.DespuesInsertar;
begin

end;

procedure TCondominioDetalleGastoOrdinario.DespuesModificar;
begin

end;

constructor TCondominioDetalleGastoOrdinario.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('v_condominio_detalle_gastos_ordinarios');
  SetStrSQLInsert('');
  SetStrSQLUpdate('');
  SetStrSQLDelete('');
  SetLength(ClavesPrimarias,2);
  ClavesPrimarias[0] := '"codigo_conjunto_administracion"';
  ClavesPrimarias[0] := '"codigo_gasto"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TCondominioDetalleGastoOrdinario.SetCodigoConjuntoAdministracion(varCodigoConjuntoAdministracion: string);
begin
  CodigoConjuntoAdministracion := varCodigoConjuntoAdministracion;
end;

procedure TCondominioDetalleGastoOrdinario.SetCodigoGasto(varCodigoGasto: string);
begin
  CodigoGasto := varCodigoGasto;
end;

procedure TCondominioDetalleGastoOrdinario.SetDescripcion(varDescripcion: string);
begin
  Descripcion := varDescripcion;
end;

procedure TCondominioDetalleGastoOrdinario.SetMonto(varMonto: Double);
begin
  Monto := varMonto;
end;

function TCondominioDetalleGastoOrdinario.GetCodigoConjuntoAdministracion: string;
begin
  Result := CodigoConjuntoAdministracion;
end;

function TCondominioDetalleGastoOrdinario.GetCodigoGasto: string;
begin
  Result := CodigoGasto;
end;

function TCondominioDetalleGastoOrdinario.GetDescripcion: string;
begin
  Result := Descripcion;
end;

function TCondominioDetalleGastoOrdinario.GetMonto: Double;
begin
  Result := Monto;
end;

function TCondominioDetalleGastoOrdinario.ObtenerCodigoConjuntoAdministracion(
  varCodigoConjuntoAdministracion: string): TClientDataSet;
begin
  Result := ObtenerListaCondicionSQL(' WHERE "codigo_conjunto_administracion" = ' + QuotedStr(varCodigoConjuntoAdministracion));
end;

end.
