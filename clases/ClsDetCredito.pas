unit ClsDetCredito;
interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TDetCredito }

  TDetCredito = class(TGenericoBD)
    private
      IdCredito: Integer;
      Inicial: Boolean;
      PInicial: Double;
      Cuotas: Boolean;
      DiasCuota: Integer;
      CantidadCuotas: Integer;
      Dias: Boolean;
      CantidadDias: Integer;
      Interes: Boolean;
      TasaInteres: Double;
      TasaOrdinal: Boolean;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      Procedure SetIdCredito(varIdCredito: Integer);
      Procedure SetInicial(varInicial: Boolean);
      Procedure SetPInicial(varPInicial: Double);
      Procedure SetCuotas(varCuotas: Boolean);
      Procedure SetDiasCuota(varDiasCuota: Integer);
      Procedure SetCantidadCuotas(varCantidadCuotas: Integer);
      Procedure SetDias(varDias: Boolean);
      Procedure SetCantidadDias(varCantidadDias: Integer);
      Procedure SetInteres(varInteres: Boolean);
      Procedure SetTasaInteres(varTasainteres: Double);
      Procedure SetTasaOrdinal(varTasaOrdinal: Boolean);
      Function GetIdCredito: Integer;
      Function GetInicial: Boolean;
      Function GetPInicial: Double;
      Function GetCuotas: Boolean;
      Function GetDiasCuotas: Integer;
      Function GetCantidadCuotas: Integer;
      Function GetDias: Boolean;
      Function GetCantidadDias: Integer;
      Function GetInteres: Boolean;
      Function GetTasaInteres: Double;
      Function GetTasaOrdinal: Boolean;
  end;

implementation

{ TDetCredito }

procedure TDetCredito.Ajustar;
begin

end;

procedure TDetCredito.Mover(Ds: TClientDataSet);
begin
  IdCredito:= Ds.FieldValues['id_credito'];
  Inicial:= Ds.FieldValues['inicial'];
  PInicial:= Ds.FieldValues['p_inicial'];
  Cuotas:= Ds.FieldValues['cuotas'];
  DiasCuota:= Ds.FieldValues['dias_cuota'];
  CantidadCuotas:= Ds.FieldValues['cantidad_cuotas'];
  Dias:= Ds.FieldValues['dias'];
  CantidadDias:= Ds.FieldValues['cantidad_dias'];
  Interes:= Ds.FieldValues['interes'];
  TasaInteres:= Ds.FieldValues['tasa_interes'];
  TasaOrdinal:=  Ds.FieldValues['tasa_ordinal'];
end;

function TDetCredito.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,11);
  Params[0] := IdCredito;
  Params[1] := Inicial;
  Params[2] := PInicial;
  Params[3] := Cuotas;
  Params[4] := DiasCuota;
  Params[5] := CantidadCuotas;
  Params[6] := Dias;
  Params[7] := CantidadDias;
  Params[8] := Interes;
  Params[9] := TasaInteres;
  Params[10] := TasaOrdinal;
  Result:= Params;
end;

function TDetCredito.GetCampos: TArrayString;
begin

end;

procedure TDetCredito.AntesInsertar;
begin

end;

procedure TDetCredito.AntesModificar;
begin

end;

procedure TDetCredito.AntesEliminar;
begin

end;

procedure TDetCredito.DespuesInsertar;
begin

end;

procedure TDetCredito.DespuesModificar;
begin

end;

procedure TDetCredito.DespuesEliminar;
begin

end;

function TDetCredito.GetCondicion: string;
begin
  Result:= '"id_credito" = ' + QuotedStr(IntToStr(IdCredito));
end;

constructor TDetCredito.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_det_credito');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(11)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "inicial"=$2,"p_inicial"=$3,"cuotas"=$4,"dias_cuota"=$5,"cantidad_cuotas"=$6,"dias"=$7,"cantidad_dias"=$8,"interes"=$9,"tasa_interes"=$10,"tasa_ordinal"=$11, WHERE ("id_credito"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("id_credito"=$1)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0]:='"id_credito"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TDetCredito.SetCantidadCuotas(varCantidadCuotas: Integer);
begin
  CantidadCuotas := varCantidadCuotas;
end;

procedure TDetCredito.SetCantidadDias(varCantidadDias: Integer);
begin
  CantidadDias := varCantidadDias;
end;

procedure TDetCredito.SetCuotas(varCuotas: Boolean);
begin
  Cuotas := varCuotas;
end;

procedure TDetCredito.SetDias(varDias: Boolean);
begin
  dias := varDias;
end;

procedure TDetCredito.SetDiasCuota(varDiasCuota: Integer);
begin
  DiasCuota := varDiasCuota;
end;

procedure TDetCredito.SetIdCredito(varIdCredito: Integer);
begin
  IdCredito := varIdCredito;
end;

procedure TDetCredito.SetInicial(varInicial: Boolean);
begin
  Inicial := varInicial;
end;

procedure TDetCredito.SetInteres(varInteres: Boolean);
begin
  Interes := varInteres;
end;

procedure TDetCredito.SetPInicial(varPInicial: Double);
begin
  PInicial := varPInicial
end;

procedure TDetCredito.SetTasaInteres(varTasainteres: Double);
begin
  TasaInteres := varTasainteres;
end;

procedure TDetCredito.SetTasaOrdinal(varTasaOrdinal: Boolean);
begin
  TasaOrdinal := varTasaOrdinal;
end;

function TDetCredito.GetPInicial: Double;
begin
  Result := PInicial;
end;

function TDetCredito.GetTasaInteres: Double;
begin
  Result := TasaInteres;
end;

function TDetCredito.GetTasaOrdinal: Boolean;
begin
  Result := TasaOrdinal;
end;

function TDetCredito.GetCantidadCuotas: Integer;
begin
  Result := CantidadCuotas;
end;

function TDetCredito.GetCantidadDias: Integer;
begin
  Result := CantidadDias;
end;

function TDetCredito.GetCuotas: Boolean;
begin
  Result := Cuotas;
end;

function TDetCredito.GetDias: Boolean;
begin
  Result := Dias;
end;

function TDetCredito.GetDiasCuotas: Integer;
begin
  Result := DiasCuota;
end;

function TDetCredito.GetIdCredito: Integer;
begin
  Result := IdCredito;
end;

function TDetCredito.GetInicial: Boolean;
begin
  Result := Inicial;
end;

function TDetCredito.GetInteres: Boolean;
begin
  Result := Interes;
end;

end.
