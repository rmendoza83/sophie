unit ClsZona;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TZona }

  TZona = class(TGenericoBD)
    private
      Codigo: string;
      Descripcion: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      function ObtenerCombo: TClientDataSet;
      procedure SetCodigo(varCodigo: string);
      procedure SetDescripcion(varDescripcion: string);
      function GetCodigo: string;
      function GetDescripcion: string;
  end;

implementation

{ TZona }

procedure TZona.Ajustar;
begin

end;

procedure TZona.Mover(Ds: TClientDataSet);
begin
  Codigo := Ds.FieldValues['codigo'];
  Descripcion := Ds.FieldValues['descripcion'];
end;

function TZona.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,2);
  Params[0] := Codigo;
  Params[1] := Descripcion;
  Result := Params;
end;

function TZona.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,2);
  Campos[0] := 'codigo';
  Campos[1] := 'descripcion';
  Result := Campos;
end;

function TZona.GetCondicion: string;
begin
  Result := '"codigo" = ' + QuotedStr(Codigo);
end;

procedure TZona.AntesInsertar;
begin

end;

procedure TZona.AntesModificar;
begin

end;

procedure TZona.AntesEliminar;
begin

end;

procedure TZona.DespuesInsertar;
begin

end;

procedure TZona.DespuesModificar;
begin

end;

procedure TZona.DespuesEliminar;
begin

end;

constructor TZona.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_zonas');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(2)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "descripcion"=$2 WHERE ("codigo"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("codigo"=$1)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"codigo"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

function TZona.ObtenerCombo: TClientDataSet;
begin
  Result := BD.EjecutarQueryCliDs('SELECT "codigo", "descripcion" FROM ' + GetTabla);
end;

procedure TZona.SetCodigo(varCodigo: string);
begin
  Codigo := varCodigo;
end;

procedure TZona.SetDescripcion(varDescripcion: string);
begin
  Descripcion := varDescripcion;
end;

function TZona.GetCodigo: string;
begin
  Result := Codigo;
end;

function TZona.GetDescripcion: string;
begin
  Result := Descripcion;
end;

end.
