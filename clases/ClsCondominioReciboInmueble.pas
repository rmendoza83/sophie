unit ClsCondominioReciboInmueble;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TCondominioReciboInmueble }

  TCondominioReciboInmueble = class(TGenericoBD)
    private
      CodigoConjuntoAdministracion: string;
      PeriodoA: Integer;
      PeriodoM: Integer;
      CodigoInmueble: string;
      MontoBase: Double;
      PReserva: Double;
      MontoReserva: Double;
      MontoTotal: Double;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigoConjuntoAdministracion(varCodigoConjuntoAdministracion: string);
      procedure SetPeriodoA(varPeriodoA: Integer);
      procedure SetPeriodoM(varPeriodoM: Integer);
      procedure SetCodigoInmueble(varCodigoInmueble: string);
      procedure SetMontoBase(varMontoBase: Double);
      procedure SetPReserva(varPReserva: Double);
      procedure SetMontoReserva(varMontoReserva: Double);
      procedure SetMontoTotal(varMontoTotal: Double);
      function GetCodigoConjuntoAdministracion: string;
      function GetPeriodoA: Integer;
      function GetPeriodoM: Integer;
      function GetCodigoInmueble: string;
      function GetMontoBase: Double;
      function GetPReserva: Double;
      function GetMontoReserva: Double;
      function GetMontoTotal: Double;
      function ObtenerRecibos(varCodigoConjuntoAdministracion: string; varPeriodoA: Integer; varPeriodoM: Integer): TClientDataSet;
      function EliminarRecibos(varCodigoConjuntoAdministracion: string; varPeriodoA: Integer; varPeriodoM: Integer): Boolean;
  end;

var
  Clase: TCondominioReciboInmueble;

implementation

{ TCondominioReciboInmueble }

procedure TCondominioReciboInmueble.Ajustar;
begin

end;

procedure TCondominioReciboInmueble.Mover(Ds: TClientDataSet);
begin
  CodigoConjuntoAdministracion := Ds.FieldValues['codigo_conjunto_administracion'];
  PeriodoA := Ds.FieldValues['periodo_a'];
  PeriodoM := Ds.FieldValues['periodo_m'];
  CodigoInmueble := Ds.FieldValues['codigo_inmueble'];
  MontoBase := Ds.FieldValues['monto_base'];
  PReserva := Ds.FieldValues['p_reserva'];
  MontoReserva := Ds.FieldValues['monto_reserva'];
  MontoTotal := Ds.FieldValues['monto_total'];
end;

function TCondominioReciboInmueble.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,8);
  Params[0] := CodigoConjuntoAdministracion;
  Params[1] := PeriodoA;
  Params[2] := PeriodoM;
  Params[3] := CodigoInmueble;
  Params[4] := MontoBase;
  Params[5] := PReserva;
  Params[6] := MontoReserva;
  Params[7] := MontoTotal;
  Result := Params;
end;

function TCondominioReciboInmueble.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,4);
  Campos[0] := 'codigo_conjunto_administracion';
  Campos[1] := 'periodo_a';
  Campos[2] := 'periodo_m';
  Campos[3] := 'codigo_inmueble';
  Result := Campos;
end;

function TCondominioReciboInmueble.GetCondicion: string;
begin
  Result := '"codigo_conjunto_administracion" = ' + QuotedStr(CodigoConjuntoAdministracion) + ' AND ' +
            '"periodo_a" = ' + IntToStr(PeriodoA) + ' AND ' +
            '"periodo_m" = ' + IntToStr(PeriodoM) + ' AND ' +
            '"codigo_inmueble" = ' + QuotedStr(CodigoInmueble);
end;

procedure TCondominioReciboInmueble.AntesEliminar;
begin

end;

procedure TCondominioReciboInmueble.AntesInsertar;
begin

end;

procedure TCondominioReciboInmueble.AntesModificar;
begin

end;

procedure TCondominioReciboInmueble.DespuesEliminar;
begin

end;

procedure TCondominioReciboInmueble.DespuesInsertar;
begin

end;

procedure TCondominioReciboInmueble.DespuesModificar;
begin

end;

constructor TCondominioReciboInmueble.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_condominio_recibos_inmuebles');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(8)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "monto_base"=$5, "p_reserva"=$6, "monto_reserva"=$7, "monto_total"=$8 ' +
                  'WHERE ("codigo_conjunto_administracion"=$1 AND "periodo_a"=$2 AND "periodo_m"=$3 AND "codigo_inmueble"=$4)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("codigo_conjunto_administracion"=$1 AND "periodo_a"=$2 AND "periodo_m"=$3 AND "codigo_inmueble"=$4)');
  SetLength(ClavesPrimarias,4);
  ClavesPrimarias[0] := '"codigo_conjunto_administracion"';
  ClavesPrimarias[1] := '"periodo_a"';
  ClavesPrimarias[2] := '"periodo_m"';
  ClavesPrimarias[3] := '"codigo_inmueble"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TCondominioReciboInmueble.SetCodigoConjuntoAdministracion(varCodigoConjuntoAdministracion: string);
begin
  CodigoConjuntoAdministracion := varCodigoConjuntoAdministracion;
end;

procedure TCondominioReciboInmueble.SetPeriodoA(varPeriodoA: Integer);
begin
  PeriodoA := varPeriodoA;
end;

procedure TCondominioReciboInmueble.SetPeriodoM(varPeriodoM: Integer);
begin
  PeriodoM := varPeriodoM;
end;

procedure TCondominioReciboInmueble.SetCodigoInmueble(varCodigoInmueble: string);
begin
  CodigoInmueble := varCodigoInmueble;
end;

procedure TCondominioReciboInmueble.SetMontoBase(varMontoBase: Double);
begin
  MontoBase := varMontoBase;
end;

procedure TCondominioReciboInmueble.SetPReserva(varPReserva: Double);
begin
  PReserva := varPReserva;
end;

procedure TCondominioReciboInmueble.SetMontoReserva(varMontoReserva: Double);
begin
  MontoReserva := varMontoReserva;
end;

procedure TCondominioReciboInmueble.SetMontoTotal(varMontoTotal: Double);
begin
  MontoTotal := varMontoTotal;
end;

function TCondominioReciboInmueble.GetCodigoConjuntoAdministracion: string;
begin
  Result := CodigoConjuntoAdministracion;
end;

function TCondominioReciboInmueble.GetPeriodoA: Integer;
begin
  Result := PeriodoA;
end;

function TCondominioReciboInmueble.GetPeriodoM: Integer;
begin
  Result := PeriodoM;
end;

function TCondominioReciboInmueble.GetCodigoInmueble: string;
begin
  Result := CodigoInmueble;
end;

function TCondominioReciboInmueble.GetMontoBase: Double;
begin
  Result := MontoBase;
end;

function TCondominioReciboInmueble.GetPReserva: Double;
begin
  Result := PReserva;
end;

function TCondominioReciboInmueble.GetMontoReserva: Double;
begin
  Result := MontoReserva;
end;

function TCondominioReciboInmueble.GetMontoTotal: Double;
begin
  Result := MontoTotal;
end;

function TCondominioReciboInmueble.ObtenerRecibos(
  varCodigoConjuntoAdministracion: string; varPeriodoA,
  varPeriodoM: Integer): TClientDataSet;
begin
  Result := ObtenerListaCondicionSQL(' WHERE ("codigo_conjunto_administracion" = ' + QuotedStr(varCodigoConjuntoAdministracion) + ') AND ("periodo_a" = ' + IntToStr(varPeriodoA) + ') AND ("periodo_m" = ' + IntToStr(varPeriodoM) + ')');
end;

function TCondominioReciboInmueble.EliminarRecibos(
  varCodigoConjuntoAdministracion: string; varPeriodoA,
  varPeriodoM: Integer): Boolean;
begin
  Result := EjecutarSQL('DELETE FROM ' + GetTabla + ' WHERE ("codigo_conjunto_administracion" = ' + QuotedStr(varCodigoConjuntoAdministracion) + ') AND ("periodo_a" = ' + IntToStr(varPeriodoA) + ') AND ("periodo_m" = ' + IntToStr(varPeriodoM) + ')');
end;

end.
