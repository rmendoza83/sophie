unit ClsConsecutivo;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TConsecutivo }

  TConsecutivo = class(TGenericoBD)
    private
      Nombre: string;
      Consecutivo: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetNombre(varNombre: string);
      procedure SetConsecutivo(varConsecutivo: Integer);
      function GetNombre: string;
      function GetConsecutivo: Integer;
      function ObtenerConsecutivo(varNombre: string): Integer;
  end;

implementation

{ TConsecutivo }

procedure TConsecutivo.Ajustar;
begin

end;

procedure TConsecutivo.Mover(Ds: TClientDataSet);
begin
  Nombre := Ds.FieldValues['nombre'];
  Consecutivo := Ds.FieldValues['consecutivo'];
end;

function TConsecutivo.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,2);
  Params[0] := Nombre;
  Params[1] := Consecutivo;
  Result := Params;
end;

function TConsecutivo.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,2);
  Campos[0] := 'nombre';
  Campos[1] := 'consecutivo';
  Result := Campos;
end;

function TConsecutivo.GetCondicion: string;
begin
  Result := '"nombre" = ' + QuotedStr(Nombre);
end;

procedure TConsecutivo.AntesEliminar;
begin

end;

procedure TConsecutivo.AntesInsertar;
begin

end;

procedure TConsecutivo.AntesModificar;
begin

end;

procedure TConsecutivo.DespuesEliminar;
begin

end;

procedure TConsecutivo.DespuesInsertar;
begin

end;

procedure TConsecutivo.DespuesModificar;
begin

end;

constructor TConsecutivo.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('sys_consecutivos');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(2)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "consecutivo"=$2 WHERE ("nombre"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("nombre"=$1)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"nombre"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TConsecutivo.SetNombre(varNombre: string);
begin
  Nombre := varNombre;
end;

procedure TConsecutivo.SetConsecutivo(varConsecutivo: Integer);
begin
  Consecutivo := varConsecutivo;
end;

function TConsecutivo.GetNombre: string;
begin
  Result := Nombre;
end;

function TConsecutivo.GetConsecutivo: Integer;
begin
  Result := Consecutivo;
end;

function TConsecutivo.ObtenerConsecutivo(varNombre: string): Integer;
begin
  SetNombre(varNombre);
  Buscar;
  Result := GetConsecutivo;
  SetConsecutivo(GetConsecutivo + 1);
  Modificar;
end;

end.
