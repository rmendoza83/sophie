unit ClsRadioAnunciante;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TRadioAnunciante }

  TRadioAnunciante = class(TGenericoBD)
    private
      Codigo: string;
      Descripcion: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      function ObtenerCombo: TClientDataSet;
      procedure SetCodigo(varCodigo: string);
      procedure SetDescripcion(varDescripcion: string);
      function GetCodigo: string;
      function GetDescripcion: string;
      function ObtenerConsecutivoCodigo: string;
      function BuscarDescripcion: Boolean; 
  end;

implementation

{ TRadioAnunciante }

procedure TRadioAnunciante.Ajustar;
begin

end;

procedure TRadioAnunciante.Mover(Ds: TClientDataSet);
begin
  Codigo := Ds.FieldValues['codigo'];
  Descripcion := Ds.FieldValues['descripcion'];
end;

function TRadioAnunciante.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,2);
  Params[0] := Codigo;
  Params[1] := Descripcion;
  Result := Params;
end;

function TRadioAnunciante.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,2);
  Campos[0] := 'codigo';
  Campos[1] := 'descripcion';
  Result := Campos;
end;

function TRadioAnunciante.GetCondicion: string;
begin
  Result := '"codigo" = ' + QuotedStr(Codigo);
end;

procedure TRadioAnunciante.AntesInsertar;
begin

end;

procedure TRadioAnunciante.AntesModificar;
begin

end;

procedure TRadioAnunciante.AntesEliminar;
begin

end;

procedure TRadioAnunciante.DespuesInsertar;
begin

end;

procedure TRadioAnunciante.DespuesModificar;
begin

end;

procedure TRadioAnunciante.DespuesEliminar;
begin

end;

constructor TRadioAnunciante.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_radio_anunciantes');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(2)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "descripcion"=$2 WHERE ("codigo"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("codigo"=$1)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"codigo"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

function TRadioAnunciante.ObtenerCombo: TClientDataSet;
begin
  Result := BD.EjecutarQueryCliDs('SELECT "codigo", "descripcion" FROM ' + GetTabla);
end;

procedure TRadioAnunciante.SetCodigo(varCodigo: string);
begin
  Codigo := varCodigo;
end;

procedure TRadioAnunciante.SetDescripcion(varDescripcion: string);
begin
  Descripcion := varDescripcion;
end;

function TRadioAnunciante.GetCodigo: string;
begin
  Result := Codigo;
end;

function TRadioAnunciante.GetDescripcion: string;
begin
  Result := Descripcion;
end;

function TRadioAnunciante.ObtenerConsecutivoCodigo: string;
var
  Ds: TClientDataSet;
  Consecutivo: Integer;
begin
  Ds := BD.EjecutarQueryCliDs('SELECT MAX(CAST("codigo" AS integer)) AS consecutivo FROM ' + GetTabla);
  if (Ds.RecordCount = 0) then
  begin
    Consecutivo := 1;
  end
  else
  begin
    Consecutivo := StrToIntDef(Ds.FieldByName('consecutivo').AsString,0) + 1;
  end;
  Result := FormatFloat('0000000000',Consecutivo);   
end;

function TRadioAnunciante.BuscarDescripcion: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("descripcion" = ' + QuotedStr(GetDescripcion) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

end.
