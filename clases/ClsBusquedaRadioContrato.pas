unit ClsBusquedaRadioContrato;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TBusquedaRadioContrato }

  TBusquedaRadioContrato = class(TGenericoBD)
    private
      Numero: string;
      CodigoCliente: string;
      RifCliente: string;
      NitCliente: string;
      RazonSocialCliente: string;
      ContactoCliente: string;
      CodigoAnunciante: string;
      DescripcionAnunciante: string;
      CodigoAgencia: string;
      RifAgencia: string;
      NitAgencia: string;
      RazonSocialAgencia: string;
      Contactoagencia: string;
      CodigoVendedor: string;
      DescripcionVendedor: string;
      CodigoRubro: string;
      DescripcionRubro: string;
      NumeroOrden: string;
      Producto: string;
      Observaciones: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      Function GetNumero: string;
      Function GetCodigoCliente: string;
      Function GetRifCliente: string;
      Function GetNitCliente: string;
      Function GetRazonSocialCliente: string;
      Function GetContactoCliente: string;
      Function GetCodigoAnunciante: string;
      Function GetDescripcionAnunciante: string;
      Function GetCodigoAgencia: string;
      Function GetRifAgencia: string;
      Function GetNitAgencia: string;
      Function GetRazonSocialAgencia: string;
      Function GetContactoagencia: string;
      Function GetCodigoVendedor: string;
      Function GetDescripcionVendedor: string;
      Function GetCodigoRubro: string;
      Function GetDescripcionRubro: string;
      Function GetNumeroOrden: string;
      Function GetProducto: string;
      Function GetObservaciones: string;
  end;

implementation

{ TBusquedaRadioContrato }

procedure TBusquedaRadioContrato.Ajustar;
begin

end;

procedure TBusquedaRadioContrato.Mover(Ds: TClientDataSet);
begin
  Numero:= Ds.FieldValues['numero'];
  CodigoCliente:= Ds.FieldValues['codigo_cliente'];
  RifCliente:= Ds.FieldValues['rif_cliente'];
  NitCliente:= Ds.FieldValues['nit_cliente'];
  RazonSocialCliente:= Ds.FieldValues['razon_social_cliente'];
  ContactoCliente:= Ds.FieldValues['contacto_cliente'];
  CodigoAnunciante:= Ds.FieldValues['codigo_anunciante'];
  DescripcionAnunciante:= Ds.FieldValues['descripcion_anunciante'];
  CodigoAgencia:= Ds.FieldValues['codigo_agencia'];
  RifAgencia:= Ds.FieldValues['rif_agencia'];
  NitAgencia:= Ds.FieldValues['nit_agencia'];
  RazonSocialAgencia:= Ds.FieldValues['razon_social_agencia'];
  Contactoagencia:= Ds.FieldValues['contacto_agencia'];
  CodigoVendedor:= Ds.FieldValues['codigo_vendedor'];
  DescripcionVendedor:= Ds.FieldValues['descripcion_vendedor'];
  CodigoRubro:= Ds.FieldValues['codigo_rubro'];
  DescripcionRubro:= Ds.FieldValues['decripcion_rubro'];
  NumeroOrden:= Ds.FieldValues['numero_orden'];
  Producto:= Ds.FieldValues['producto'];
  Observaciones:= Ds.FieldValues['observaciones'];
end;

function TBusquedaRadioContrato.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,20);
  Params[0] := Numero;
  Params[1] := CodigoCliente;
  Params[2] := RifCliente;
  Params[3] := NitCliente;
  Params[4] := RazonSocialCliente;
  Params[5] := ContactoCliente;
  Params[6] := CodigoAnunciante;
  Params[7] := DescripcionAnunciante;
  Params[8] := CodigoAgencia;
  Params[9] := RifAgencia;
  Params[10] := NitAgencia;
  Params[11] := RazonSocialAgencia;
  Params[12] := Contactoagencia;
  Params[13] := CodigoVendedor;
  Params[14] := DescripcionVendedor;
  Params[15] := CodigoRubro;
  Params[16] := DescripcionRubro;
  Params[17] := NumeroOrden;
  Params[18] := Producto;
  Params[19] := Observaciones;
  Result := Params;
end;

function TBusquedaRadioContrato.GetProducto: string;
begin

end;

function TBusquedaRadioContrato.GetRazonSocialAgencia: string;
begin

end;

function TBusquedaRadioContrato.GetRazonSocialCliente: string;
begin

end;

function TBusquedaRadioContrato.GetRifAgencia: string;
begin

end;

function TBusquedaRadioContrato.GetRifCliente: string;
begin

end;

function TBusquedaRadioContrato.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,20);
  Campos[0] := 'numero';
  Campos[1] := 'codigo_cliente';
  Campos[2] := 'rif_cliente';
  Campos[3] := 'nit_cliente';
  Campos[4] := 'razon_social_cliente';
  Campos[5] := 'contacto_cliente';
  Campos[6] := 'codigo_anunciante';
  Campos[7] := 'descripcion_anunciante';
  Campos[8] := 'codigo_agencia';
  Campos[9] := 'rif_agencia';
  Campos[10] := 'nit_agencia';
  Campos[11] := 'razon_social_agencia';
  Campos[12] := 'contacto_agencia';
  Campos[13] := 'codigo_vendedor';
  Campos[14] := 'descripcion_vendedor';
  Campos[15] := 'codigo_rubro';
  Campos[16] := 'descripcion_rubro';
  Campos[17] := 'numero_orden';
  Campos[18] := 'producto';
  Campos[19] := 'observaciones';
  Result := Campos;
end;

function TBusquedaRadioContrato.GetCondicion: string;
begin
  Result:= '';
end;

procedure TBusquedaRadioContrato.AntesInsertar;
begin

end;

procedure TBusquedaRadioContrato.AntesModificar;
begin

end;

procedure TBusquedaRadioContrato.AntesEliminar;
begin

end;

procedure TBusquedaRadioContrato.DespuesInsertar;
begin

end;

procedure TBusquedaRadioContrato.DespuesModificar;
begin

end;

procedure TBusquedaRadioContrato.DespuesEliminar;
begin

end;

constructor TBusquedaRadioContrato.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('v_busqueda_radio_contratos');
  SetLength(ClavesPrimarias,0);
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

function TBusquedaRadioContrato.GetCodigoAgencia: string;
begin
  Result := CodigoAgencia;
end;

function TBusquedaRadioContrato.GetCodigoAnunciante: string;
begin
  Result := CodigoAnunciante;
end;

function TBusquedaRadioContrato.GetCodigoCliente: string;
begin
  Result := CodigoCliente;
end;

function TBusquedaRadioContrato.GetCodigoRubro: string;
begin
  Result := CodigoRubro;
end;

function TBusquedaRadioContrato.GetCodigoVendedor: string;
begin
  Result := CodigoVendedor;
end;

function TBusquedaRadioContrato.GetContactoagencia: string;
begin
  Result := Contactoagencia;
end;

function TBusquedaRadioContrato.GetContactoCliente: string;
begin
  Result := ContactoCliente;
end;

function TBusquedaRadioContrato.GetDescripcionAnunciante: string;
begin
  Result := DescripcionAnunciante;
end;

function TBusquedaRadioContrato.GetDescripcionRubro: string;
begin
  Result := DescripcionRubro;
end;

function TBusquedaRadioContrato.GetDescripcionVendedor: string;
begin
  Result := DescripcionVendedor;
end;

function TBusquedaRadioContrato.GetNitAgencia: string;
begin
  Result := NitAgencia;
end;

function TBusquedaRadioContrato.GetNitCliente: string;
begin
  Result := NitCliente;
end;

function TBusquedaRadioContrato.GetNumero: string;
begin
  Result := Numero;
end;

function TBusquedaRadioContrato.GetNumeroOrden: string;
begin
  Result := NumeroOrden;
end;

function TBusquedaRadioContrato.GetObservaciones: string;
begin
  Result := Observaciones;
end;

end.
