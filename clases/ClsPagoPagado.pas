unit ClsPagoPagado;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TPagoPagado }

  TPagoPagado = class(TGenericoBD)
    private
      Desde: string;
      NumeroDocumento: string;
      CodigoMoneda: string;
      CodigoProveedor: string;
      CodigoZona: string;
      FechaIngreso: TDate;
      FechaRecepcion: TDate;
      FechaVencIngreso: TDate;
      FechaVencRecepcion: TDate;
      FechaLibros: TDate;
      FechaContable: TDate;
      FechaPago: TDate;
      NumeroCuota: Integer;
      TotalCuotas: Integer;
      OrdenPago: Integer;
      MontoNeto: Double;
      Impuesto: Double;
      Total: Double;
      PIva: Double;
      IdPago: Integer;
      IdContadoPago: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArraystring; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetDesde(varDesde: string);
      procedure SetNumeroDocumento(varNumeroDocumento: string);
      procedure SetCodigoMoneda(varCodigoMoneda: string);
      procedure SetCodigoProveedor(varCodigoProveedor: string);
      procedure SetCodigoZona(varCodigoZona:  string);
      procedure SetFechaIngreso(varFechaIngreso: TDate);
      procedure SetFechaRecepcion(varFechaRecepcion: TDate);
      procedure SetFechaVencIngreso(varFechaVencIngreso: TDate);
      procedure SetFechaVencRecepcion(varFechaVencRecepcion: TDate);
      procedure SetFechaLibros(varFechaLibros: TDate);
      procedure SetFechaContable(varFechaContable: TDate);
      procedure SetFechaPago(varFechaPago: TDate);
      procedure SetNumeroCuota(varNumeroCuota: Integer);
      procedure SetTotalCuotas(varTotalCuota: Integer);
      procedure SetOrdenPago(varOrdenPago: Integer);
      procedure SetMontoNeto(varMontoNeto: Double);
      procedure SetImpuesto(varImpuesto: Double);
      procedure SetTotal(varTotal: Double);
      procedure SetPIva(varPIva: Double);
      procedure SetIdPago(varIdPago: Integer);
      procedure SetIdContadoPago(varIdContadoPago: Integer);
      function GetDesde: string;
      function GetNumeroDocumento: string;
      function GetCodigoMoneda: string;
      function GetCodigoProveedor: string;
      function GetCodigoZona:  string;
      function GetFechaIngreso: TDate;
      function GetFechaRecepcion: TDate;
      function GetFechaVencIngreso: TDate;
      function GetFechaVencRecepcion: TDate;
      function GetFechaLibros: TDate;
      function GetFechaContable: TDate;
      function GetFechaPago: TDate;
      function GetNumeroCuota: Integer;
      function GetTotalCuotas: Integer;
      function GetOrdenPago: Integer;
      function GetMontoNeto: Double;
      function GetImpuesto: Double;
      function GetTotal: Double;
      function GetPIva: Double;
      function GetIdPago: Integer;
      function GetIdContadoPago: Integer;
      function EliminarIdPago(varIdPago: Integer): Boolean;
      function BuscarIdPago: Boolean;
      function VerificarOrdenPago: Boolean;
  end;

implementation

{ TPagoPagado }

procedure TPagoPagado.Ajustar;
begin

end;

procedure TPagoPagado.Mover(Ds: TClientDataSet);
begin
  Desde := Ds.FieldValues['desde'];
  NumeroDocumento := Ds.FieldValues['numero_documento'];
  CodigoMoneda := Ds.FieldValues['codigo_moneda'];
  CodigoProveedor := Ds.FieldValues['codigo_proveedor'];
  CodigoZona := Ds.FieldValues['codigo_zona'];
  FechaIngreso := Ds.FieldByName('fecha_ingreso').AsDateTime;
  FechaRecepcion := Ds.FieldByName('fecha_recepcion').AsDateTime;
  FechaVencIngreso := Ds.FieldByName('fecha_venc_ingreso').AsDateTime;
  FechaVencRecepcion := Ds.FieldByName('fecha_venc_recepcion').AsDateTime;
  FechaLibros := Ds.FieldByName('fecha_libros').AsDateTime;
  FechaContable := Ds.FieldByName('fecha_contable').AsDateTime;
  FechaPago := Ds.FieldByName('fecha_pago').AsDateTime;
  NumeroCuota := Ds.FieldValues['numero_cuota'];
  TotalCuotas := Ds.FieldValues['total_cuotas'];
  OrdenPago := Ds.FieldValues['orden_pago'];
  MontoNeto := Ds.FieldValues['monto_neto'];
  Impuesto := Ds.FieldValues['impuesto'];
  Total := Ds.FieldValues['total'];
  PIva := Ds.FieldValues['p_iva'];
  IdPago := Ds.FieldValues['id_pago'];
  IdContadoPago := Ds.FieldValues['id_contado_pago'];
end;

function TPagoPagado.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,21);
  Params[0] := Desde;
  Params[1] := NumeroDocumento;
  Params[2] := CodigoMoneda;
  Params[3] := CodigoProveedor;
  Params[4] := CodigoZona;
  Params[5] := VarFromDateTime(FechaIngreso);
  Params[6] := VarFromDateTime(FechaRecepcion);
  Params[7] := VarFromDateTime(FechaVencIngreso);
  Params[8] := VarFromDateTime(FechaVencRecepcion);
  Params[9] := VarFromDateTime(FechaLibros);
  Params[10] := VarFromDateTime(FechaContable);
  Params[11] := VarFromDateTime(FechaPago);
  Params[12] := NumeroCuota;
  Params[13] := TotalCuotas;
  Params[14] := OrdenPago;
  Params[15] := MontoNeto;
  Params[16] := Impuesto;
  Params[17] := Total;
  Params[18] := PIva;
  Params[19] := IdPago;
  Params[20] := IdContadoPago;
  Result := Params;
end;

function TPagoPagado.GetCampos: TArraystring;
var
  Campos: TArraystring;
begin
  SetLength(Campos,5);
  Campos[0] := 'desde';
  Campos[1] := 'numero_documento';
  Campos[2] := 'codigo_moneda';
  Campos[3] := 'codigo_proveedor';
  Campos[4] := 'codigo_zona';
  Result := Campos;
end;

function TPagoPagado.GetCondicion: string;
begin
  Result := '"id_pago" = ' + IntToStr(IdPago) + ' AND "numero_cuota" =' + IntToStr(NumeroCuota) + ' AND "orden_pago" = ' + IntToStr(OrdenPago);
end;

procedure TPagoPagado.AntesInsertar;
begin

end;

procedure TPagoPagado.AntesModificar;
begin

end;

procedure TPagoPagado.AntesEliminar;
begin

end;

procedure TPagoPagado.DespuesInsertar;
begin

end;

procedure TPagoPagado.DespuesModificar;
begin

end;

procedure TPagoPagado.DespuesEliminar;
begin

end;

constructor TPagoPagado.Create;
var
  ClavesPrimarias: TArraystring;
begin
  inherited Create;
  SetTabla('tbl_pagos_pagados');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(21)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "desde"=$1,"numero_documento"=$2,"codigo_moneda"=$3,"codigo_proveedor"=$4,"codigo_zona"=$5,"fecha_ingreso"=$6,"fecha_recepcion"=$7,"fecha_venc_ingreso"=$8,"fecha_venc_recepcion"=$9,"fecha_libros"=$10,' +
                  '"fecha_contable"=$11,"fecha_pago"=$12,"total_cuotas"=$14,"monto_neto"=$16,"impuesto"=$17,"total"=$18,"p_iva"=$19,"id_contado_pago"=$21' +
                  ' WHERE ("id_pago"=$20) AND ("numero_cuota"=$13) AND ("orden_pago"=$15)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("id_pago"=$20) AND ("numero_cuota"=$13) AND ("orden_pago"=$15)');
  SetLength(ClavesPrimarias,3);
  ClavesPrimarias[0] := '"id_pago"';
  ClavesPrimarias[1] := '"numero_cuota"';
  ClavesPrimarias[2] := '"orden_pago"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TPagoPagado.SetCodigoProveedor(varCodigoProveedor: string);
begin
  CodigoProveedor := varCodigoProveedor;
end;

procedure TPagoPagado.SetCodigoMoneda(varCodigoMoneda: string);
begin
  CodigoMoneda := varCodigoMoneda;
end;

procedure TPagoPagado.SetCodigoZona(varCodigoZona: string);
begin
  CodigoZona := varCodigoZona;
end;

procedure TPagoPagado.SetDesde(varDesde: string);
begin
  Desde := varDesde;
end;

procedure TPagoPagado.SetOrdenPago(varOrdenPago: Integer);
begin
  OrdenPago := varOrdenPago;
end;

procedure TPagoPagado.SetFechaContable(varFechaContable: TDate);
begin
  FechaContable := varFechaContable;
end;

procedure TPagoPagado.SetFechaIngreso(varFechaIngreso: TDate);
begin
  FechaIngreso := varFechaIngreso;
end;

procedure TPagoPagado.SetFechaLibros(varFechaLibros: TDate);
begin
  FechaLibros := varFechaLibros;
end;

procedure TPagoPagado.SetFechaPago(varFechaPago: TDate);
begin
  FechaPago := varFechaPago;
end;

procedure TPagoPagado.SetFechaRecepcion(varFechaRecepcion: TDate);
begin
  FechaRecepcion := varFechaRecepcion;
end;

procedure TPagoPagado.SetFechaVencIngreso(varFechaVencIngreso: TDate);
begin
  FechaVencIngreso := varFechaVencIngreso;
end;

procedure TPagoPagado.SetFechaVencRecepcion(varFechaVencRecepcion: TDate);
begin
  FechaVencRecepcion := varFechaVencRecepcion;
end;

procedure TPagoPagado.SetIdContadoPago(varIdContadoPago: Integer);
begin
  IdContadoPago := varIdContadoPago;
end;

procedure TPagoPagado.SetIdPago(varIdPago: Integer);
begin
  IdPago := varIdPago;
end;

procedure TPagoPagado.SetImpuesto(varImpuesto: Double);
begin
  Impuesto := varImpuesto;
end;

procedure TPagoPagado.SetMontoNeto(varMontoNeto: Double);
begin
  MontoNeto := varMontoNeto;
end;

procedure TPagoPagado.SetNumeroDocumento(varNumeroDocumento: string);
begin
  NumeroDocumento := varNumeroDocumento;
end;

procedure TPagoPagado.SetNumeroCuota(varNumeroCuota: Integer);
begin
  NumeroCuota := varNumeroCuota;
end;

procedure TPagoPagado.SetPIva(varPIva: Double);
begin
  PIva := varPIva;
end;

procedure TPagoPagado.SetTotal(varTotal: Double);
begin
  Total := varTotal;
end;

procedure TPagoPagado.SetTotalCuotas(varTotalCuota: Integer);
begin
  TotalCuotas :=  varTotalCuota;
end;

function TPagoPagado.GetCodigoProveedor: string;
begin
  Result := CodigoProveedor;
end;

function TPagoPagado.GetCodigoMoneda: string;
begin
  Result := CodigoMoneda;
end;

function TPagoPagado.GetCodigoZona: string;
begin
  Result := CodigoZona;
end;

function TPagoPagado.GetDesde: string;
begin
  Result := Desde;
end;

function TPagoPagado.GetOrdenPago: Integer;
begin
  Result := OrdenPago;
end;

function TPagoPagado.GetFechaContable: TDate;
begin
  Result := FechaContable;
end;

function TPagoPagado.GetFechaIngreso: TDate;
begin
  Result := FechaIngreso;
end;

function TPagoPagado.GetFechaLibros: TDate;
begin
  Result := FechaLibros;
end;

function TPagoPagado.GetFechaPago: TDate;
begin
  Result := FechaPago;
end;

function TPagoPagado.GetFechaRecepcion: TDate;
begin
  Result := FechaRecepcion;
end;

function TPagoPagado.GetFechaVencIngreso: TDate;
begin
  Result := FechaVencIngreso;
end;

function TPagoPagado.GetFechaVencRecepcion: TDate;
begin
  Result := FechaVencRecepcion;
end;

function TPagoPagado.GetIdContadoPago: Integer;
begin
  Result := IdContadoPago;
end;

function TPagoPagado.GetIdPago: Integer;
begin
  Result := IdPago;
end;

function TPagoPagado.GetImpuesto: Double;
begin
  Result := Impuesto;
end;

function TPagoPagado.GetMontoNeto: Double;
begin
  Result :=MontoNeto;
end;

function TPagoPagado.GetNumeroDocumento: string;
begin
  Result := NumeroDocumento;
end;

function TPagoPagado.GetNumeroCuota: Integer;
begin
  Result := NumeroCuota;
end;

function TPagoPagado.GetPIva: Double;
begin
  Result := PIva;
end;

function TPagoPagado.GetTotal: Double;
begin
  Result := Total;
end;

function TPagoPagado.GetTotalCuotas: Integer;
begin
  Result := TotalCuotas;
end;

function TPagoPagado.EliminarIdPago(varIdPago: Integer): Boolean;
begin
  Result := EjecutarSQL('DELETE FROM ' + GetTabla + ' WHERE "id_pago" = ' + IntToStr(varIdPago));
end;

function TPagoPagado.BuscarIdPago: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("id_pago" = ' + IntToStr(GetIdPago) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

function TPagoPagado.VerificarOrdenPago: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("id_pago" = ' + IntToStr(GetIdPago) + ') AND ("numero_cuota" = ' + IntToStr(GetNumeroCuota) + ') AND ("orden_pago" > ' + IntToStr(GetOrdenPago) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
  end;
end;

end.
