unit ClsCliente;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TCliente }

  TCliente = class(TGenericoBD)
    private
      Codigo: string;
      PrefijoRif: string;
      Rif: string;
      Nit: string;
      RazonSocial: string;
      Sexo: Char;
      FechaNacimiento: TDate;
      DireccionFiscal1: string;
      DireccionFiscal2: string;
      DireccionDomicilio1: string;
      DireccionDomicilio2: string;
      Telefonos: string;
      Fax: string;
      Email: string;
      Website: string;
      FechaIngreso: TDate;
      Contacto: string;
      CodigoVendedor: string;
      CodigoEsquemaPago: string;
      CodigoZona: string;
      UniqueId: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigo(varCodigo: string);
      procedure SetPrefijoRif(varPrefijoRif: string);
      procedure SetRif(varRif: string);
      procedure SetNit(varNit: string);
      procedure SetRazonSocial(varRazonSocial: string);
      procedure SetSexo(varSexo: Char);
      procedure SetFechaNacimiento(varFechaNacimiento: TDate);
      procedure SetDireccionFiscal1(varDireccionFiscal1: string);
      procedure SetDireccionFiscal2(varDireccionFiscal2: string);
      procedure SetDireccionDomicilio1(varDireccionDomicilio1: string);
      procedure SetDireccionDomicilio2(varDireccionDomicilio2: string);
      procedure SetTelefonos(varTelefonos: string);
      procedure SetFax(varFax: string);
      procedure SetEmail(varEmail: string);
      procedure SetWebsite(varWebsite: string);
      procedure SetFechaIngreso(varFechaIngreso: TDate);
      procedure SetContacto(varContacto: string);
      procedure SetCodigoVendedor(varCodigoVendedor: string);
      procedure SetCodigoEsquemaPago(varCodigoEsquemaPago: string);
      procedure SetCodigoZona(varCodigoZona: string);
      procedure SetUniqueId(varUniqueId: Integer);
      function GetCodigo: string;
      function GetPrefijoRif: string;
      function GetRif: string;
      function GetNit: string;
      function GetRazonSocial: string;
      function GetSexo: Char;
      function GetFechaNacimiento: TDate;
      function GetDireccionFiscal1: string;
      function GetDireccionFiscal2: string;
      function GetDireccionDomicilio1: string;
      function GetDireccionDomicilio2: string;
      function GetTelefonos: string;
      function GetFax: string;
      function GetEmail: string;
      function GetWebsite: string;
      function GetFechaIngreso: TDate;
      function GetContacto: string;
      function GetCodigoVendedor: string;
      function GetCodigoEsquemaPago: string;
      function GetCodigoZona: string;
      function GetUniqueId: Integer;
      function ObtenerCombo: TClientDataSet;
      function BuscarCodigo: Boolean;
  end;

implementation

uses
  ClsConsecutivo;
  
{ TCliente }

procedure TCliente.Ajustar;
begin

end;

procedure TCliente.Mover(Ds: TClientDataSet);
begin
  Codigo := Ds.FieldValues['codigo'];
  PrefijoRif := Ds.FieldValues['prefijo_rif'];
  Rif := Ds.FieldValues['rif'];
  Nit := Ds.FieldValues['nit'];
  RazonSocial := Ds.FieldValues['razon_social'];
  Sexo := Ds.FieldByName('sexo').AsString[1];
  FechaNacimiento := Ds.FieldByName('fecha_nacimiento').AsDateTime;
  DireccionFiscal1 := Ds.FieldValues['direccion_fiscal_1'];
  DireccionFiscal2 := Ds.FieldValues['direccion_fiscal_2'];
  DireccionDomicilio1 := Ds.FieldValues['direccion_domicilio_1'];
  DireccionDomicilio2 := Ds.FieldValues['direccion_domicilio_2'];
  Telefonos := Ds.FieldValues['telefonos'];
  Fax := Ds.FieldValues['fax'];
  Email := Ds.FieldValues['email'];
  Website := Ds.FieldValues['website'];
  FechaIngreso := Ds.FieldByName('fecha_ingreso').AsDateTime;
  Contacto := Ds.FieldValues['contacto'];
  CodigoVendedor := Ds.FieldValues['codigo_vendedor'];
  CodigoEsquemaPago := Ds.FieldValues['codigo_esquema_pago'];
  CodigoZona := Ds.FieldValues['codigo_zona'];
  UniqueId := Ds.FieldValues['unique_id'];
end;

function TCliente.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,21);
  Params[0] := Codigo;
  Params[1] := PrefijoRif;
  Params[2] := Rif;
  Params[3] := Nit;
  Params[4] := RazonSocial;
  Params[5] := Sexo;
  Params[6] := VarFromDateTime(FechaNacimiento);
  Params[7] := DireccionFiscal1;
  Params[8] := DireccionFiscal2;
  Params[9] := DireccionDomicilio1;
  Params[10] := DireccionDomicilio2;
  Params[11] := Telefonos;
  Params[12] := Fax;
  Params[13] := Email;
  Params[14] := Website;
  Params[15] := VarFromDateTime(FechaIngreso);
  Params[16] := Contacto;
  Params[17] := CodigoVendedor;
  Params[18] := CodigoEsquemaPago;
  Params[19] := CodigoZona;
  Params[20] := UniqueId;
  Result := Params;
end;

function TCliente.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,16);
  Campos[0] := 'codigo';
  Campos[1] := 'rif';
  Campos[2] := 'nit';
  Campos[3] := 'razon_social';
  Campos[4] := 'direccion_fiscal_1';
  Campos[5] := 'direccion_fiscal_2';
  Campos[6] := 'direccion_domicilio_1';
  Campos[7] := 'direccion_domicilio_2';
  Campos[8] := 'telefonos';
  Campos[9] := 'fax';
  Campos[10] := 'email';
  Campos[11] := 'website';
  Campos[12] := 'contacto';
  Campos[13] := 'codigo_vendedor';
  Campos[14] := 'codigo_esquema_pago';
  Campos[15] := 'codigo_zona';
  Result := Campos;
end;

function TCliente.GetCondicion: string;
begin
  Result := '"unique_id" = ' + IntToStr(UniqueId);
end;

procedure TCliente.AntesInsertar;
var
  Consecutivo: TConsecutivo;
begin
  Consecutivo := TConsecutivo.Create;
  UniqueId := Consecutivo.ObtenerConsecutivo('id_clientes');
end;

procedure TCliente.AntesModificar;
begin

end;

procedure TCliente.AntesEliminar;
begin

end;

procedure TCliente.DespuesInsertar;
begin

end;

procedure TCliente.DespuesModificar;
begin

end;

procedure TCliente.DespuesEliminar;
begin

end;

constructor TCliente.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_clientes');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(21)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "codigo"=$1,"prefijo_rif"=$2, "rif"=$3, "nit"=$4, "razon_social"=$5, "sexo"=$6, "fecha_nacimiento"=$7, "direccion_fiscal_1"=$8, "direccion_fiscal_2"=$9, "direccion_domicilio_1"=$10, ' +
                   '"direccion_domicilio_2"=$11, "telefonos"=$12, "fax"=$13, "email"=$14, "website"=$15, "fecha_ingreso"=$16, "contacto"=$17, "codigo_vendedor"=$18, "codigo_esquema_pago"=$19, "codigo_zona"=$20 WHERE ("unique_id"=$21)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("unique_id"=$21)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"unique_id"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TCliente.SetCodigo(varCodigo: string);
begin
  Codigo := varCodigo;
end;

procedure TCliente.SetPrefijoRif(varPrefijoRif: string);
begin
  PrefijoRif := varPrefijoRif;
end;

procedure TCliente.SetRif(varRif: string);
begin
  Rif := varRif;
end;

procedure TCliente.SetNit(varNit: string);
begin
  Nit := varNit;
end;

procedure TCliente.SetRazonSocial(varRazonSocial: string);
begin
  RazonSocial := varRazonSocial;
end;

procedure TCliente.SetSexo(varSexo: Char);
begin
  Sexo := varSexo;
end;

procedure TCliente.SetFechaNacimiento(varFechaNacimiento: TDate);
begin
  FechaNacimiento := varFechaNacimiento;
end;

procedure TCliente.SetDireccionFiscal1(varDireccionFiscal1: string);
begin
  DireccionFiscal1 := varDireccionFiscal1;
end;

procedure TCliente.SetDireccionFiscal2(varDireccionFiscal2: string);
begin
  DireccionFiscal2 := varDireccionFiscal2;
end;

procedure TCliente.SetDireccionDomicilio1(varDireccionDomicilio1: string);
begin
  DireccionDomicilio1 := varDireccionDomicilio1;
end;

procedure TCliente.SetDireccionDomicilio2(varDireccionDomicilio2: string);
begin
  DireccionDomicilio2 := varDireccionDomicilio2;
end;

procedure TCliente.SetTelefonos(varTelefonos: string);
begin
  Telefonos := varTelefonos;
end;

procedure TCliente.SetFax(varFax: string);
begin
  Fax := varFax;
end;

procedure TCliente.SetEmail(varEmail: string);
begin
  Email := varEmail;
end;

procedure TCliente.SetWebsite(varWebsite: string);
begin
  Website := varWebsite;
end;

procedure TCliente.SetFechaIngreso(varFechaIngreso: TDate);
begin
  FechaIngreso := varFechaIngreso;
end;

procedure TCliente.SetContacto(varContacto: string);
begin
  Contacto := varContacto;
end;

procedure TCliente.SetCodigoVendedor(varCodigoVendedor: string);
begin
  CodigoVendedor := varCodigoVendedor;
end;

procedure TCliente.SetCodigoEsquemaPago(varCodigoEsquemaPago: string);
begin
  CodigoEsquemaPago := varCodigoEsquemaPago;
end;

procedure TCliente.SetCodigoZona(varCodigoZona: string);
begin
  CodigoZona := varCodigoZona;
end;

procedure TCliente.SetUniqueId(varUniqueId: Integer);
begin
  UniqueId := varUniqueId;
end;

function TCliente.GetCodigo: string;
begin
  Result := Codigo;
end;

function TCliente.GetPrefijoRif: string;
begin
  Result := PrefijoRif;
end;

function TCliente.GetRif: string;
begin
  Result := Rif;
end;

function TCliente.GetNit: string;
begin
  Result := Nit;
end;

function TCliente.GetRazonSocial: string;
begin
  Result := RazonSocial;
end;

function TCliente.GetSexo: Char;
begin
  Result := Sexo;
end;

function TCliente.GetFechaNacimiento: TDate;
begin
  Result := FechaNacimiento;
end;

function TCliente.GetDireccionFiscal1: string;
begin
  Result := DireccionFiscal1;
end;

function TCliente.GetDireccionFiscal2: string;
begin
  Result := DireccionFiscal2;
end;

function TCliente.GetDireccionDomicilio1: string;
begin
  Result := DireccionDomicilio1;
end;

function TCliente.GetDireccionDomicilio2: string;
begin
  Result := DireccionDomicilio2;
end;

function TCliente.GetTelefonos: string;
begin
  Result := Telefonos;
end;

function TCliente.GetFax: string;
begin
  Result := Fax;
end;

function TCliente.GetEmail: string;
begin
  Result := Email;
end;

function TCliente.GetWebsite: string;
begin
  Result := Website;
end;

function TCliente.GetFechaIngreso: TDate;
begin
  Result := FechaIngreso;
end;

function TCliente.GetContacto: string;
begin
  Result := Contacto;
end;

function TCliente.GetCodigoVendedor: string;
begin
  Result := CodigoVendedor;
end;

function TCliente.GetCodigoEsquemaPago: string;
begin
  Result := CodigoEsquemaPago;
end;

function TCliente.GetCodigoZona: string;
begin
  Result := CodigoZona;
end;

function TCliente.GetUniqueId: Integer;
begin
  Result := UniqueId;
end;

function TCliente.ObtenerCombo: TClientDataSet;
begin
  Result := BD.EjecutarQueryCliDs('SELECT "codigo", "razon_social" FROM ' + GetTabla + ' ORDER BY "codigo"');
end;

function TCliente.BuscarCodigo: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("codigo" = ' + QuotedStr(GetCodigo) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

end.

