unit ClsCondominioDetalleLiquidacionCobranza;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TCondominioDetalleLiquidacionCobranza }

  TCondominioDetalleLiquidacionCobranza = class(TGenericoBD)
    private
      IdCondominioLiquidacionCobranza: Integer;
      CodigoConjuntoAdministracion: string;
      PeriodoA: Integer;
      PeriodoM: Integer;
      CodigoInmueble: string;
      OrdenCobranza: Integer;
      Abonado: Double;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetIdCondominioLiquidacionCobranza(varIdCondominioLiquidacionCobranza: Integer);
      procedure SetCodigoConjuntoAdministracion(varCodigoConjuntoAdministracion: string);
      procedure SetPeriodoA(varPeriodoA: Integer);
      procedure SetPeriodoM(varPeriodoM: Integer);
      procedure SetCodigoInmueble(varCodigoInmueble: string);
      procedure SetOrdenCobranza(varOrdenCobranza: Integer);
      procedure SetAbonado(varAbonado: Double);
      function GetIdCondominioLiquidacionCobranza: Integer;
      function GetCodigoConjuntoAdministracion: string;
      function GetPeriodoA: Integer;
      function GetPeriodoM: Integer;
      function GetCodigoInmueble: string;
      function GetOrdenCobranza: Integer;
      function GetAbonado: Double;
  end;

var
  Clase: TCondominioDetalleLiquidacionCobranza;

implementation

{ TCondominioDetalleLiquidacionCobranza }

procedure TCondominioDetalleLiquidacionCobranza.Ajustar;
begin

end;

procedure TCondominioDetalleLiquidacionCobranza.Mover(Ds: TClientDataSet);
begin
  IdCondominioLiquidacionCobranza := Ds.FieldValues['id_condominio_liquidacion_cobranza'];
  CodigoConjuntoAdministracion := Ds.FieldValues['codigo_conjunto_administracion'];
  PeriodoA := Ds.FieldValues['periodo_a'];
  PeriodoM := Ds.FieldValues['periodo_m'];
  CodigoInmueble := Ds.FieldValues['codigo_inmueble'];
  OrdenCobranza := Ds.FieldValues['orden_cobranza'];
  Abonado := Ds.FieldValues['abonado'];
end;

function TCondominioDetalleLiquidacionCobranza.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,7);
  Params[0] := IdCondominioLiquidacionCobranza;
  Params[1] := CodigoConjuntoAdministracion;
  Params[2] := PeriodoA;
  Params[3] := PeriodoM;
  Params[4] := CodigoInmueble;
  Params[5] := OrdenCobranza;
  Params[6] := Abonado;
  Result := Params;
end;

function TCondominioDetalleLiquidacionCobranza.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,4);
  Campos[0] := 'codigo_conjunto_administracion';
  Campos[1] := 'periodo_a';
  Campos[2] := 'periodo_m';
  Campos[3] := 'codigo_inmueble';
  Result := Campos;
end;

function TCondominioDetalleLiquidacionCobranza.GetCondicion: string;
begin
  Result := '"id_condominio_liquidacion_cobranza" = ' + IntToStr(IdCondominioLiquidacionCobranza) + ' AND ' +
            '"codigo_conjunto_administracion" = ' + QuotedStr(CodigoConjuntoAdministracion) + ' AND ' +
            '"periodo_a" = ' + IntToStr(PeriodoA) + ' AND ' +
            '"periodo_m" = ' + IntToStr(PeriodoM) + ' AND ' +
            '"codigo_inmueble" = ' + QuotedStr(CodigoInmueble) + ' AND ' +
            '"orden_cobranza" = ' + IntToStr(OrdenCobranza);
end;

procedure TCondominioDetalleLiquidacionCobranza.AntesEliminar;
begin

end;

procedure TCondominioDetalleLiquidacionCobranza.AntesInsertar;
begin

end;

procedure TCondominioDetalleLiquidacionCobranza.AntesModificar;
begin

end;

procedure TCondominioDetalleLiquidacionCobranza.DespuesEliminar;
begin

end;

procedure TCondominioDetalleLiquidacionCobranza.DespuesInsertar;
begin

end;

procedure TCondominioDetalleLiquidacionCobranza.DespuesModificar;
begin

end;

constructor TCondominioDetalleLiquidacionCobranza.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_condominio_det_liquidacion_cobranzas');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(7)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "abonado"=$7 ' +
                  'WHERE ("id_condominio_liquidacion_cobranza"=$1 AND "codigo_conjunto_administracion"=$2 AND "periodo_a"=$3 AND "periodo_m"=$4 AND "codigo_inmueble"=$5 AND "orden_cobranza"=$6)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("id_condominio_liquidacion_cobranza"=$1 AND "codigo_conjunto_administracion"=$2 AND "periodo_a"=$3 AND "periodo_m"=$4 AND "codigo_inmueble"=$5 AND "orden_cobranza"=$6)');
  SetLength(ClavesPrimarias,6);
  ClavesPrimarias[0] := '"id_condominio_liquidacion_cobranza"';
  ClavesPrimarias[1] := '"codigo_conjunto_administracion"';
  ClavesPrimarias[2] := '"periodo_a"';
  ClavesPrimarias[3] := '"periodo_m"';
  ClavesPrimarias[4] := '"codigo_inmueble"';
  ClavesPrimarias[5] := '"orden_cobranza"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TCondominioDetalleLiquidacionCobranza.SetIdCondominioLiquidacionCobranza(varIdCondominioLiquidacionCobranza: Integer);
begin
  IdCondominioLiquidacionCobranza := varIdCondominioLiquidacionCobranza;
end;

procedure TCondominioDetalleLiquidacionCobranza.SetCodigoConjuntoAdministracion(varCodigoConjuntoAdministracion: string);
begin
  CodigoConjuntoAdministracion := varCodigoConjuntoAdministracion;
end;

procedure TCondominioDetalleLiquidacionCobranza.SetPeriodoA(varPeriodoA: Integer);
begin
  PeriodoA := varPeriodoA;
end;

procedure TCondominioDetalleLiquidacionCobranza.SetPeriodoM(varPeriodoM: Integer);
begin
  PeriodoM := varPeriodoM;
end;

procedure TCondominioDetalleLiquidacionCobranza.SetCodigoInmueble(varCodigoInmueble: string);
begin
  CodigoInmueble := varCodigoInmueble;
end;

procedure TCondominioDetalleLiquidacionCobranza.SetOrdenCobranza(varOrdenCobranza: Integer);
begin
  OrdenCobranza := varOrdenCobranza;
end;

procedure TCondominioDetalleLiquidacionCobranza.SetAbonado(varAbonado: Double);
begin
  Abonado := varAbonado;
end;
           
function TCondominioDetalleLiquidacionCobranza.GetIdCondominioLiquidacionCobranza: Integer;
begin
  Result := IdCondominioLiquidacionCobranza;
end;

function TCondominioDetalleLiquidacionCobranza.GetCodigoConjuntoAdministracion: string;
begin
  Result := CodigoConjuntoAdministracion;
end;

function TCondominioDetalleLiquidacionCobranza.GetPeriodoA: Integer;
begin
  Result := PeriodoA;
end;

function TCondominioDetalleLiquidacionCobranza.GetPeriodoM: Integer;
begin
  Result := PeriodoM;
end;

function TCondominioDetalleLiquidacionCobranza.GetCodigoInmueble: string;
begin
  Result := CodigoInmueble;
end;

function TCondominioDetalleLiquidacionCobranza.GetOrdenCobranza: Integer;
begin
  Result := OrdenCobranza;
end;

function TCondominioDetalleLiquidacionCobranza.GetAbonado: Double;
begin
  Result := Abonado;
end;

end.
