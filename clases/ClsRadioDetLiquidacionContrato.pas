unit ClsRadioDetLiquidacionContrato;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TRadioDetLiquidacionContrato}

  TRadioDetLiquidacionContrato = class(TGenericoBD)
    private
      IdRadioLiquidacionContrato: Integer;
      IdRadioContrato: Integer;
      Item: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetIdRadioContrato(varIdRadioContrato: Integer);
      procedure SetIdRadioLiquidacionContrato(varIdRadioLiquidacionContrato: Integer);
      procedure SetItem(varItem: Integer);
      function GetIdRadioLiquidacionContrato: Integer;
      function GetIdRadioContrato: Integer;
      function GetItem: Integer;
      function ObtenerIdRadioLiquidacionContrato(varIdRadioLiquidacionContrato: Integer): TClientDataSet;
      function EliminarIdRadioLiquidacionContrato(varIdRadioLiquidacionContrato: Integer): Boolean;
  end;

implementation

{ TRadioDetLiquidacionContrato }

procedure TRadioDetLiquidacionContrato.Ajustar;
begin

end;

procedure TRadioDetLiquidacionContrato.Mover(Ds: TClientDataSet);
begin
  IdRadioLiquidacionContrato := Ds.FieldValues['id_radio_liquidacion_contrato'];
  IdRadioContrato := Ds.FieldValues['id_radio_contrato'];
  Item := Ds.FieldValues['item'];
end;

function TRadioDetLiquidacionContrato.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,3);
  Params[0] := IdRadioLiquidacionContrato;
  Params[1] := IdRadioContrato;
  Params[2] := Item;
  Result := Params;
end;

function TRadioDetLiquidacionContrato.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,0);
  Result := Campos;
end;

function TRadioDetLiquidacionContrato.GetCondicion: string;
begin
  Result := '("id_radio_liquidacion_contrato" = ' + IntToStr(IdRadioLiquidacionContrato) + ') AND ' +
            '("id_radio_contrato" = ' + IntToStr(IdRadioContrato) + ') AND ' +
            '("item" = ' + IntToStr(Item) + ')';
end;

procedure TRadioDetLiquidacionContrato.AntesInsertar;
begin

end;

procedure TRadioDetLiquidacionContrato.AntesModificar;
begin

end;

procedure TRadioDetLiquidacionContrato.AntesEliminar;
begin

end;

procedure TRadioDetLiquidacionContrato.DespuesInsertar;
begin

end;

procedure TRadioDetLiquidacionContrato.DespuesModificar;
begin

end;

procedure TRadioDetLiquidacionContrato.DespuesEliminar;
begin

end;

constructor TRadioDetLiquidacionContrato.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_radio_det_liquidacion_contratos');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(3)) + ')');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("id_radio_liquidacion_contrato"=$1) AND ("id_radio_contrato"=$2) AND ("item = $3")');
  SetLength(ClavesPrimarias,3);
  ClavesPrimarias[0] := '"id_radio_liquidacion_contrato"';
  ClavesPrimarias[1] := '"id_radio_contrato"';
  ClavesPrimarias[2] := '"item"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TRadioDetLiquidacionContrato.SetIdRadioLiquidacionContrato(varIdRadioLiquidacionContrato: Integer);
begin
  IdRadioLiquidacionContrato := varIdRadioLiquidacionContrato;
end;

procedure TRadioDetLiquidacionContrato.SetIdRadioContrato(varIdRadioContrato: Integer);
begin
  IdRadioContrato := varIdRadioContrato;
end;

procedure TRadioDetLiquidacionContrato.SetItem(varItem: Integer);
begin
  Item := varItem;
end;

function TRadioDetLiquidacionContrato.GetIdRadioContrato: Integer;
begin
  Result := IdRadioContrato;
end;

function TRadioDetLiquidacionContrato.GetIdRadioLiquidacionContrato: Integer;
begin
  Result := IdRadioLiquidacionContrato;
end;

function TRadioDetLiquidacionContrato.GetItem: Integer;
begin
  Result := Item;
end;

function TRadioDetLiquidacionContrato.ObtenerIdRadioLiquidacionContrato(varIdRadioLiquidacionContrato: Integer): TClientDataSet;
begin
  Result := ObtenerListaCondicionSQL(' WHERE "id_radio_liquidacion_contrato" = ' + IntToStr(varIdRadioLiquidacionContrato));
end;

function TRadioDetLiquidacionContrato.EliminarIdRadioLiquidacionContrato(varIdRadioLiquidacionContrato: Integer): Boolean;
begin
  Result := EjecutarSQL('DELETE FROM ' + GetTabla + ' WHERE "id_radio_liquidacion_contrato" = ' + IntToStr(varIdRadioLiquidacionContrato));
end;

end.
