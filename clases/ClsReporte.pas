unit ClsReporte;

{$I sophie.inc}

interface

uses
  Classes, SysUtils, Controls, Forms, variants, Dialogs,
  {$IFDEF USE_RAVE_REPORTS} DMRR, {$ENDIF}
  {$IFDEF USE_FAST_REPORTS} DMFR, {$ENDIF}
   Windows;

{$IFDEF USE_RAVE_REPORTS}
const
  Proyecto = 'sophie.rav';
{$ENDIF}

type
  { TReporte }

  TReporte = class
  private
    {$IFDEF USE_RAVE_REPORTS}
    NombreProyecto: string;
    {$ENDIF}
  public
    constructor Create;
    destructor Destroy; override;
    function EmiteReporte(varNombreReporte: string; varTipoReporte: TTipoReporte): Boolean;
    function ImprimeReporte(varNombreReporte: string; varTipoReporte: TTipoReporte): Boolean;
    procedure AgregarParametro(varNombre: string; varValor: string);
    procedure ModificarSQLDriverDataView(varNombre: string; varSQL: string);
  end;

implementation

{ TReporte }                                    

constructor TReporte.Create;
begin
  inherited Create;
  {$IFDEF USE_RAVE_REPORTS}
  NombreProyecto := ExtractFilePath(Application.ExeName) + Proyecto;
  if (not Assigned(DMRave)) then
  begin
    DMRave := TDMRave.Construir(Application,NombreProyecto);
  end;
  {$ELSE}
    {$IFDEF USE_FAST_REPORTS}
    if (not Assigned(DMFastReport)) then
    begin
      DMFastReport := TDMFastReport.Construir(Application);
    end;
    {$ENDIF}
  {$ENDIF}
end;

destructor TReporte.Destroy;
begin
  inherited Destroy;
end;

function TReporte.EmiteReporte(varNombreReporte: string; varTipoReporte: TTipoReporte): Boolean;
begin
  {$IFDEF USE_RAVE_REPORTS}
  Result := DMRave.EmiteReporte(varNombreReporte,varTipoReporte);
  {$ELSE}
    {$IFDEF USE_FAST_REPORTS}
    Result := DMFastReport.EmiteReporte(varNombreReporte,varTipoReporte);
    {$ELSE}
    Result := False; //Another Report
    {$ENDIF}
  {$ENDIF}
end;

function TReporte.ImprimeReporte(varNombreReporte: string; varTipoReporte: TTipoReporte): Boolean;
begin
  {$IFDEF USE_RAVE_REPORTS}
  Result := DMRave.ImprimirReporte(varNombreReporte,varTipoReporte);
  {$ELSE}
    {$IFDEF USE_FAST_REPORTS}
    Result := DMFastReport.ImprimirReporte(varNombreReporte,varTipoReporte);
    {$ELSE}
    Result := False; //Another Report
    {$ENDIF}
  {$ENDIF}
end;

procedure TReporte.ModificarSQLDriverDataView(varNombre: string; varSQL: string);
begin
  {$IFDEF USE_RAVE_REPORTS}
  DMRave.AgregarModDDVSQL(varNombre,varSQL);
  {$ELSE}
    {$IFDEF USE_FAST_REPORTS}
    DMFastReport.AgregarModDDVSQL(varNombre,varSQL);
    {$ENDIF}
  {$ENDIF}

end;

procedure TReporte.AgregarParametro(varNombre: string; varValor: string);
begin
  {$IFDEF USE_RAVE_REPORTS}
  DMRave.AgregarParametro(varNombre,varValor);
  {$ELSE}
    {$IFDEF USE_FAST_REPORTS}
    DMFastReport.AgregarParametro(varNombre,varValor);
    {$ENDIF}
  {$ENDIF}
end;

end.

