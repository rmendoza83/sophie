unit ClsBusquedaAnticipoCliente;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TBusquedaAnticipoCliente }

  TBusquedaAnticipoCliente = class(TGenericoBD)
    private
      Numero: string;
      CodigoCliente: string;
      Rif: string;
      Nit: string;
      RazonSocial: string;
      Contacto: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      Function GetNumero: string;
      Function GetCodigoCliente: string;
      Function GetRif: string;
      Function GetNit: string;
      Function GetRazonSocial: string;
      Function GetContacto: string;
  end;

implementation

{ TBusquedaAnticipoCliente }

procedure TBusquedaAnticipoCliente.Ajustar;
begin

end;

procedure TBusquedaAnticipoCliente.Mover(Ds: TClientDataSet);
begin
  Numero := Ds.FieldValues['numero'];
  CodigoCliente := Ds.FieldValues['codigo_cliente'];
  Rif := Ds.FieldValues['rif'];
  Nit := Ds.FieldValues['nit'];
  RazonSocial := Ds.FieldValues['razon_social'];
  Contacto := Ds.FieldValues['contacto'];
end;

function TBusquedaAnticipoCliente.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,6);
  Params[0] := Numero;
  Params[1] := CodigoCliente;
  Params[2] := Rif;
  Params[3] := Nit;
  Params[4] := RazonSocial;
  Params[5] := Contacto;
  Result := Params;
end;

function TBusquedaAnticipoCliente.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,6);
  Campos[0] := 'numero';
  Campos[1] := 'codigo_cliente';
  Campos[2] := 'rif';
  Campos[3] := 'nit';
  Campos[4] := 'razon_social';
  Campos[5] := 'contacto';
  Result := Campos;
end;

function TBusquedaAnticipoCliente.GetCondicion: string;
begin
  Result:= '';
end;

procedure TBusquedaAnticipoCliente.AntesInsertar;
begin

end;

procedure TBusquedaAnticipoCliente.AntesModificar;
begin

end;

procedure TBusquedaAnticipoCliente.AntesEliminar;
begin

end;

procedure TBusquedaAnticipoCliente.DespuesInsertar;
begin

end;

procedure TBusquedaAnticipoCliente.DespuesModificar;
begin

end;

procedure TBusquedaAnticipoCliente.DespuesEliminar;
begin

end;

constructor TBusquedaAnticipoCliente.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('v_busqueda_anticipo_clientes');
  SetLength(ClavesPrimarias,0);
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

function TBusquedaAnticipoCliente.GetCodigoCliente: String;
begin
  Result := CodigoCliente;
end;

function TBusquedaAnticipoCliente.GetContacto: String;
begin
  Result := Contacto;
end;

function TBusquedaAnticipoCliente.GetNit: String;
begin
  Result := Nit;
end;

function TBusquedaAnticipoCliente.GetNumero: String;
begin
  Result := Numero;
end;

function TBusquedaAnticipoCliente.GetRazonSocial: String;
begin
  Result := RazonSocial;
end;

function TBusquedaAnticipoCliente.GetRif: String;
begin
  Result := Rif;
end;

end.
