unit ClsTallerDetServicio;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TTallerDetServicio }

  TTallerDetServicio = class(TGenericoBD)
    private
      CodigoProducto: string;
      Referencia: string;
      Descripcion: string;
      Cantidad: Double;
      Precio: Double;
      PDescuento: Double;
      MontoDescuento: Double;
      Impuesto: Double;
      PIva: Double;
      Total: Double;
      Facturable: Boolean;
      Observaciones: String;
      IdTallerServicio: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigoProducto(varCodigoProducto: string);
      procedure SetReferencia(varReferencia: string);
      procedure SetDescripcion(varDescripcion: string);
      procedure SetCantidad(varCantidad: Double);
      procedure SetPrecio(varPrecio: Double);
      procedure SetPDescuento(varPDescuento: Double);
      procedure SetMontoDescuento(varMontoDescuento: Double);
      procedure SetImpuesto(varImpuesto: Double);
      procedure SetPIva(varPIva: Double);
      procedure SetTotal(varTotal: Double);
      procedure SetFacturable(varFacturable: Boolean);
      procedure SetObservaciones(varObservaciones: string);
      procedure SetIdTallerServicio(varIdTallerServicio : Integer);
      function GetCodigoProducto: string;
      function GetReferencia: string;
      function GetDescripcion: string;
      function GetCantidad: Double;
      function GetPrecio: Double;
      function GetPDescuento: Double;
      function GetMontoDescuento: Double;
      function GetImpuesto: Double;
      function GetPIva: Double;
      function GetTotal: Double;
      function GetFacturable: Boolean;
      function GetObservaciones: String;
      function GetIdTallerServicio: Integer;
      function ObtenerIdTallerServicio(varIdTallerServicio: Integer): TClientDataSet;
      function EliminarIdTallerServicio(varIdTallerServicio: Integer): Boolean;
  end;

implementation

uses DB;

{ TTallerDetServicio }

procedure TTallerDetServicio.Ajustar;
begin
  inherited;

end;

procedure TTallerDetServicio.Mover(Ds: TClientDataSet);
begin
  CodigoProducto := Ds.FieldValues['codigo_producto'];
  Referencia := Ds.FieldValues['referencia'];
  Descripcion := Ds.FieldValues['descripcion'];
  Cantidad := Ds.FieldValues['cantidad'];
  Precio := Ds.FieldValues['precio'];
  PDescuento := Ds.FieldValues['p_descuento'];
  MontoDescuento := Ds.FieldValues['monto_descuento'];
  Impuesto := Ds.FieldValues['impuesto'];
  PIva := Ds.FieldValues['p_iva'];
  Total := Ds.FieldValues['total'];
  Facturable := Ds.FieldValues['facturable'];
  Observaciones := Ds.FieldValues['observaciones'];
  IdTallerServicio := Ds.FieldValues['id_taller_servicio'];
end;

function TTallerDetServicio.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,13);
  Params[0] := CodigoProducto;
  Params[1] := Referencia;
  Params[2] := Descripcion;
  Params[3] := Cantidad;
  Params[4] := Precio;
  Params[5] := PDescuento;
  Params[6] := MontoDescuento;
  Params[7] := Impuesto;
  Params[8] := PIva;
  Params[9] := Total;
  Params[10] := Facturable;
  Params[11] := Observaciones;
  Params[12] := IdTallerServicio;
  Result:= Params;
end;

function TTallerDetServicio.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,4);
  Campos[0] := 'codigo_producto';
  Campos[1] := 'referencia';
  Campos[2] := 'descripcion';
  Campos[3] := 'observaciones';
  Result:= Campos;
end;

function TTallerDetServicio.GetCondicion: string;
begin
  Result:= '"id_taller_servicio" =' + IntToStr(IdTallerServicio) + ' AND "codigo_producto" =' + QuotedStr(CodigoProducto)
end;

procedure TTallerDetServicio.AntesEliminar;
begin

end;

procedure TTallerDetServicio.AntesInsertar;
begin

end;

procedure TTallerDetServicio.AntesModificar;
begin

end;

procedure TTallerDetServicio.DespuesEliminar;
begin

end;

procedure TTallerDetServicio.DespuesInsertar;
begin

end;

procedure TTallerDetServicio.DespuesModificar;
begin

end;

constructor TTallerDetServicio.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_taller_det_servicio');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(13)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "referencia"=$2,"descripcion"=$3,"cantidad"=$4,"precio"=$5,"p_descuento"=$6,"monto_descuento"=$7,"impuesto"=$8,"p_iva"=$9,"total"=$10, "facturable"=$11, "observaciones"=$12 WHERE ("id_taller_servicio"=$13) AND ("codigo_producto"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("id_taller_servicio  "=$13) AND ("codigo_producto"=$1)');
  SetLength(ClavesPrimarias,2);
  ClavesPrimarias[0] := '"id_taller_servicio"';
  ClavesPrimarias[1] := '"codigo_producto"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TTallerDetServicio.SetCantidad(varCantidad: Double);
begin
  Cantidad := varCantidad;
end;

procedure TTallerDetServicio.SetCodigoProducto(varCodigoProducto: string);
begin
  CodigoProducto := varCodigoProducto;
end;

procedure TTallerDetServicio.SetDescripcion(varDescripcion: string);
begin
  Descripcion := varDescripcion;
end;

procedure TTallerDetServicio.SetIdTallerServicio(varIdTallerServicio: Integer);
begin
  IdTallerServicio := varIdTallerServicio;
end;

procedure TTallerDetServicio.SetImpuesto(varImpuesto: Double);
begin
  Impuesto := varImpuesto;
end;

procedure TTallerDetServicio.SetMontoDescuento(varMontoDescuento: Double);
begin
  MontoDescuento := varMontoDescuento;
end;

procedure TTallerDetServicio.SetPDescuento(varPDescuento: Double);
begin
  PDescuento := varPDescuento;
end;

procedure TTallerDetServicio.SetPIva(varPIva: Double);
begin
  PIva := varPIva;
end;

procedure TTallerDetServicio.SetPrecio(varPrecio: Double);
begin
  Precio := varPrecio
end;

procedure TTallerDetServicio.SetReferencia(varReferencia: string);
begin
  Referencia := varReferencia;
end;

procedure TTallerDetServicio.SetTotal(varTotal: Double);
begin
  Total := varTotal;
end;

procedure TTallerDetServicio.SetFacturable(varFacturable: Boolean);
begin
  Facturable := varFacturable;
end;

procedure TTallerDetServicio.SetObservaciones(varObservaciones: string);
begin
  Observaciones := varObservaciones;
end;

function TTallerDetServicio.GetCantidad: Double;
begin
  Result := Cantidad;
end;

function TTallerDetServicio.GetCodigoProducto: string;
begin
  Result := CodigoProducto;
end;

function TTallerDetServicio.GetDescripcion: string;
begin
  Result := Descripcion;
end;

function TTallerDetServicio.GetIdTallerServicio: Integer;
begin
  Result := IdTallerServicio;
end;

function TTallerDetServicio.GetImpuesto: Double;
begin
  Result := Impuesto;
end;

function TTallerDetServicio.GetMontoDescuento: Double;
begin
  Result := MontoDescuento;
end;

function TTallerDetServicio.GetPDescuento: Double;
begin
  Result := PDescuento;
end;

function TTallerDetServicio.GetPIva: Double;
begin
  Result := PIva;
end;

function TTallerDetServicio.GetPrecio: Double;
begin
  Result := Precio;
end;

function TTallerDetServicio.GetReferencia: string;
begin
  Result := Referencia;
end;

function TTallerDetServicio.GetTotal: Double;
begin
  Result := Total;
end;

function TTallerDetServicio.GetFacturable: Boolean;
begin
  Result := Facturable;
end;

function TTallerDetServicio.GetObservaciones: String;
begin
  Result := Observaciones;
end;

function TTallerDetServicio.ObtenerIdTallerServicio(
  varIdTallerServicio: Integer): TClientDataSet;
begin
  Result := ObtenerListaCondicionSQL(' WHERE "id_taller_servicio" = ' + IntToStr(varIdTallerServicio));
end;

function TTallerDetServicio.EliminarIdTallerServicio(
  varIdTallerServicio: Integer): Boolean;
begin
  Result := EjecutarSQL('DELETE FROM ' + GetTabla + ' WHERE "id_taller_servicio" = ' + IntToStr(varIdTallerServicio));
end;

end.
