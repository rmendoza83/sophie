unit ClsRadioPrograma;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TRadioPrograma}

  TRadioPrograma = class(TGenericoBD)
    private
      Codigo: string;
      CodigoEmisora: string;
      Descripcion: string;
      HoraInicial: TTime;
      HoraFinal: TTime;
      TipoVenta: string;
      Locutor: string;
      Dias: string;
      TarifaNacional: Double;
      TarifaDia: Double;
      TarifaSegundo: Double;
      Activo: Boolean;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigo(varCodigo: string);
      procedure SetCodigoEmisora(varCodigoEmisora: string);
      procedure SetDescripcion(varDescripcion: string);
      procedure SetHoraInicial(varHoraInicial: TTime);
      procedure SetHoraFinal(varHoraFinal: TTime);
      procedure SetTipoVenta(varTipoVenta: string);
      procedure SetLocutor(varLocutor: string);
      procedure SetDias(varDias: string);
      procedure SetTarifaNacional(varTarifaNacional: Double);
      procedure SetTarifaDia(varTarifaDia: Double);
      procedure SetTarifaSegundo(varTarifaSegundo: Double);
      procedure SetActivo(varActivo: Boolean);
      function GetCodigo: string;
      function GetCodigoEmisora: string;
      function GetDescripcion: string;
      function GetHoraInicial: TTime;
      function GetHoraFinal: TTime;
      function GetTipoVenta: string;
      function GetLocutor: string;
      function GetDias: string;
      function GetTarifaNacional: Double;
      function GetTarifaDia: Double;
      function GetTarifaSegundo: Double;
      function GetActivo: Boolean;
      function ObtenerCombo: TClientDataSet;
      function ObtenerComboActivo: TClientDataSet;
  end;

implementation

{ TRadioPrograma }

procedure TRadioPrograma.Ajustar;
begin

end;

procedure TRadioPrograma.Mover(Ds: TClientDataSet);
begin
  Codigo := Ds.FieldValues['codigo'];
  CodigoEmisora := Ds.FieldValues['codigo_emisora'];
  Descripcion := Ds.FieldValues['descripcion'];
  HoraInicial := Ds.FieldValues['hora_inicial'];
  HoraFinal := Ds.FieldValues['hora_final'];
  TipoVenta := Ds.FieldValues['tipo_venta'];
  Locutor := Ds.FieldValues['locutor'];
  Dias := Ds.FieldValues['dias'];
  TarifaNacional := Ds.FieldValues['tarifa_nacional'];
  TarifaDia := Ds.FieldValues['tarifa_dia'];
  TarifaSegundo := Ds.FieldValues['tarifa_segundo'];
  Activo := Ds.FieldValues['activo'];
end;

function TRadioPrograma.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,12);
  Params[0] := Codigo;
  Params[1] := CodigoEmisora;
  Params[2] := Descripcion;
  Params[3] := VarFromDateTime(HoraInicial);
  Params[4] := VarFromDateTime(HoraFinal);
  Params[5] := TipoVenta;
  Params[6] := Locutor;
  Params[7] := Dias;
  Params[8] := TarifaNacional;
  Params[9] := TarifaDia;
  Params[10] := TarifaSegundo;
  Params[11] := Activo;
  Result := Params;
end;

function TRadioPrograma.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,6);
  Campos[0] := 'codigo';
  Campos[1] := 'codigo_emisora';
  Campos[2] := 'descripcion';
  Campos[3] := 'tipo_venta';
  Campos[4] := 'locutor';
  Campos[5] := 'dias';
  Result := Campos;
end;

function TRadioPrograma.GetCondicion: string;
begin
  Result := '"codigo" = ' + QuotedStr(Codigo);
end;

procedure TRadioPrograma.AntesInsertar;
var
  H, M, S, Ms: Word;
begin
  DecodeTime(HoraInicial,H,M,S,Ms);
  HoraInicial := EncodeTime(H,M,0,0);
  DecodeTime(HoraFinal,H,M,S,Ms);
  HoraFinal := EncodeTime(H,M,0,0);
end;

procedure TRadioPrograma.AntesModificar;
var
  H, M, S, Ms: Word;
begin
  DecodeTime(HoraInicial,H,M,S,Ms);
  HoraInicial := EncodeTime(H,M,0,0);
  DecodeTime(HoraFinal,H,M,S,Ms);
  HoraFinal := EncodeTime(H,M,0,0);
end;

procedure TRadioPrograma.AntesEliminar;
begin

end;

procedure TRadioPrograma.DespuesInsertar;
begin

end;

procedure TRadioPrograma.DespuesModificar;
begin

end;

procedure TRadioPrograma.DespuesEliminar;
begin

end;

constructor TRadioPrograma.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_radio_programas');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(12)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "codigo_emisora"=$2,"descripcion"=$3, "hora_inicial"=$4, "hora_final"=$5, "tipo_venta"=$6, "locutor"=$7, "dias"=$8, "tarifa_nacional"=$9, "tarifa_dia"=$10, "tarifa_segundo"=$11, "activo"=$12 WHERE ("codigo"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("codigo"=$1)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"codigo"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TRadioPrograma.SetActivo(varActivo: Boolean);
begin
  Activo := varActivo;
end;

procedure TRadioPrograma.SetCodigo(varCodigo: string);
begin
  Codigo := varCodigo;
end;

procedure TRadioPrograma.SetCodigoEmisora(varCodigoEmisora: string);
begin
  CodigoEmisora := varCodigoEmisora;
end;

procedure TRadioPrograma.SetDescripcion(varDescripcion: string);
begin
  Descripcion := varDescripcion;
end;

procedure TRadioPrograma.SetDias(varDias: string);
begin
  Dias := varDias;
end;

procedure TRadioPrograma.SetHoraFinal(varHoraFinal: TTime);
begin
  HoraFinal := varHoraFinal;
end;

procedure TRadioPrograma.SetHoraInicial(varHoraInicial: TTime);
begin
  HoraInicial := varHoraInicial;
end;

procedure TRadioPrograma.SetLocutor(varLocutor: string);
begin
  Locutor := varLocutor;
end;

procedure TRadioPrograma.SetTipoVenta(varTipoVenta: string);
begin
  TipoVenta := varTipoVenta;
end;

procedure TRadioPrograma.SetTarifaDia(varTarifaDia: Double);
begin
  TarifaDia := varTarifaDia;
end;

procedure TRadioPrograma.SetTarifaNacional(varTarifaNacional: Double);
begin
  TarifaNacional := varTarifaNacional;
end;

procedure TRadioPrograma.SetTarifaSegundo(varTarifaSegundo: Double);
begin
  TarifaSegundo := varTarifaSegundo;
end;

function TRadioPrograma.GetActivo: Boolean;
begin
  Result := Activo;
end;

function TRadioPrograma.GetTipoVenta: string;
begin
  Result := TipoVenta;
end;

function TRadioPrograma.GetTarifaSegundo: Double;
begin
  Result := TarifaSegundo;
end;

function TRadioPrograma.GetTarifaDia: Double;
begin
  Result := TarifaDia;
end;

function TRadioPrograma.GetTarifaNacional: Double;
begin
  Result := TarifaNacional;
end;

function TRadioPrograma.GetCodigo: string;
begin
  Result := Codigo;
end;

function TRadioPrograma.GetCodigoEmisora: string;
begin
  Result := CodigoEmisora;
end;

function TRadioPrograma.GetDescripcion: string;
begin
  Result := Descripcion;
end;

function TRadioPrograma.GetDias: string;
begin
  Result := Dias;
end;

function TRadioPrograma.GetHoraFinal: TTime;
begin
  Result := HoraFinal;
end;

function TRadioPrograma.GetHoraInicial: TTime;
begin
  Result := HoraInicial;
end;

function TRadioPrograma.GetLocutor: string;
begin
  Result := Locutor;
end;

function TRadioPrograma.ObtenerCombo: TClientDataSet;
begin
  Result := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla);
end;

function TRadioPrograma.ObtenerComboActivo: TClientDataSet;
begin
  Result := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE (activo = TRUE)');
end;

end.
