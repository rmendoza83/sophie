unit ClsFormaPago;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TFormaPago }

  TFormaPago = class(TGenericoBD)
    private
      Codigo: string;
      Descripcion: string;
      CodigoBanco: Boolean;
      Cheque: Boolean;
      Anticipo: Boolean;
      Nc: Boolean;
      Nd: Boolean;
      Cobranza: Boolean;
      Pago: Boolean;
      OperacionBancaria: Boolean;
      CodigoTipoOperacionBancaria: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigo(varCodigo: string);
      procedure SetDescripcion(varDescripcion: string);
      procedure SetCodigoBanco(varCodigoBanco: Boolean);
      procedure SetCheque(varCheque: Boolean);
      procedure SetAnticipo(varAnticipo: Boolean);
      procedure SetNc(varNc: Boolean);
      procedure SetNd(varNd: Boolean);
      procedure SetCobranza(varCobranza: Boolean);
      procedure SetPago(varPago: Boolean);
      procedure SetOperacionBancaria(varOperacionBancaria: Boolean);
      procedure SetCodigoTipoOperacionBancaria (varCodigoOperacioBancaria: string);
      function GetCodigo: string;
      function GetDescripcion: string;
      function GetCodigoBanco: Boolean;
      function GetCheque: Boolean;
      function GetAnticipo: Boolean;
      function GetNc: Boolean;
      function GetNd: Boolean;
      function GetCobranza: Boolean;
      function GetPago: Boolean;
      function GetOperacionBancaria: Boolean;
      function GetCodigoTipoOperacionBancaria: string;
      function ObtenerCombo: TClientDataSet;
      function ObtenerComboCobranza: TClientDataSet;
      function ObtenerComboPago: TClientDataSet;
  end;

implementation

{ TFormaPago }

procedure TFormaPago.Ajustar;
begin

end;

procedure TFormaPago.Mover(Ds: TClientDataSet);
begin
  Codigo := Ds.FieldValues['codigo'];
  Descripcion := Ds.FieldValues['descripcion'];
  CodigoBanco := Ds.FieldValues['codigo_banco'];
  Cheque := Ds.FieldValues['cheque'];
  Anticipo := Ds.FieldValues['anticipo'];
  Nc := Ds.FieldValues['nc'];
  Nd := Ds.FieldValues['nd'];
  Cobranza := Ds.FieldValues['cobranza'];
  Pago := Ds.FieldValues['pago'];
  OperacionBancaria := Ds.FieldValues['operacion_bancaria'];
  CodigoTipoOperacionBancaria := Ds.FieldValues['codigo_tipo_operacion_bancaria'];
end;

function TFormaPago.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,11);
  Params[0] := Codigo;
  Params[1] := Descripcion;
  Params[2] := CodigoBanco;
  Params[3] := cheque;
  Params[4] := Anticipo;
  Params[5] := Nc;
  Params[6] := Nd;
  Params[7] := Cobranza;
  Params[8] := Pago;
  Params[9] := OperacionBancaria;
  Params[10] := CodigoTipoOperacionBancaria;
  Result := Params;
end;

function TFormaPago.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,2);
  Campos[0]:= 'codigo';
  Campos[1]:= 'descripcion';
  Result:= Campos;
end;

function TFormaPago.GetCondicion: string;
begin
  Result:= '"codigo" = ' + QuotedStr(Codigo);
end;

procedure TFormaPago.AntesInsertar;
begin

end;

procedure TFormaPago.AntesModificar;
begin

end;

procedure TFormaPago.AntesEliminar;
begin

end;

procedure TFormaPago.DespuesInsertar;
begin

end;

procedure TFormaPago.DespuesModificar;
begin

end;

procedure TFormaPago.DespuesEliminar;
begin

end;

constructor TFormaPago.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_formas_pago');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(11)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "descripcion"=$2, "codigo_banco"=$3, "cheque"=$4, "anticipo"=$5, "nc"=$6, "nd"=$7, "cobranza"=$8, "pago"=$9, "operacion_bancaria"=$10, "codigo_tipo_operacion_bancaria"=$11 WHERE ("codigo"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("codigo"=$1)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"codigo"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TFormaPago.SetAnticipo(varAnticipo: Boolean);
begin
  Anticipo := varAnticipo;
end;

procedure TFormaPago.SetCheque(varCheque: Boolean);
begin
  Cheque := varcheque;
end;

procedure TFormaPago.SetCobranza(varCobranza: Boolean);
begin
  Cobranza := varCobranza;
end;

procedure TFormaPago.SetCodigo(varCodigo: string);
begin
  Codigo := varCodigo;
end;

procedure TFormaPago.SetCodigoBanco(varCodigoBanco: Boolean);
begin
  CodigoBanco := varCodigoBanco;
end;

procedure TFormaPago.SetCodigoTipoOperacionBancaria(
  varCodigoOperacioBancaria: string);
begin
  CodigoTipoOperacionBancaria := varCodigoOperacioBancaria;
end;

procedure TFormaPago.SetDescripcion(varDescripcion: string);
begin
  Descripcion := varDescripcion;
end;

procedure TFormaPago.SetNc(varNc: Boolean);
begin
  Nc := varNc;
end;

procedure TFormaPago.SetNd(varNd: Boolean);
begin
  Nd := varNd;
end;

procedure TFormaPago.SetOperacionBancaria(varOperacionBancaria: Boolean);
begin
  OperacionBancaria := varOperacionBancaria;
end;

procedure TFormaPago.SetPago(varPago: Boolean);
begin
  Pago := varPago;
end;

function TFormaPago.GetAnticipo: Boolean;
begin
  Result := Anticipo;
end;

function TFormaPago.GetCheque: Boolean;
begin
  Result := Cheque;
end;

function TFormaPago.GetCobranza: Boolean;
begin
  Result := Cobranza;
end;

function TFormaPago.GetCodigo: string;
begin
  Result := Codigo;
end;

function TFormaPago.GetCodigoBanco: Boolean;
begin
  Result := CodigoBanco;
end;

function TFormaPago.GetCodigoTipoOperacionBancaria: string;
begin
  Result := CodigoTipoOperacionBancaria;
end;

function TFormaPago.GetDescripcion: string;
begin
  Result := Descripcion;
end;

function TFormaPago.GetNc: Boolean;
begin
  Result := Nc;
end;

function TFormaPago.GetNd: Boolean;
begin
  Result := Nd;
end;

function TFormaPago.GetOperacionBancaria: Boolean;
begin
  Result := OperacionBancaria;
end;

function TFormaPago.GetPago: Boolean;
begin
  Result := Pago;
end;

function TFormaPago.ObtenerCombo: TClientDataSet;
begin
  Result := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla);
end;

function TFormaPago.ObtenerComboCobranza: TClientDataSet;
begin
  Result := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE "cobranza"');
end;

function TFormaPago.ObtenerComboPago: TClientDataSet;
begin
  Result := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE "pago"');
end;

end.
