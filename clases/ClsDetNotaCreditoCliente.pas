unit ClsDetNotaCreditoCliente;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TDetNotaCreditoCliente }

  TDetNotaCreditoCliente = class(TGenericoBD)
    private
      CodigoProducto: string;
      Referencia: string;
      Descripcion: string;
      Cantidad: Double;
      Precio: Double;
      PDescuento: Double;
      MontoDescuento: Double;
      Impuesto: Double;
      PIva: Double;
      Total: Double;
      IdNotaCreditoCliente: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigoProducto(varCodigoProducto: string);
      procedure SetReferencia(varReferencia: string);
      procedure SetDescripcion(varDescripcion: string);
      procedure SetCantidad(varCantidad: Double);
      procedure SetPrecio(varPrecio: Double);
      procedure SetPDescuento(varPDescuento: Double);
      procedure SetMontoDescuento(varMontoDescuento: Double);
      procedure SetImpuesto(varImpuesto: Double);
      procedure SetPIva(varPIva: Double);
      procedure SetTotal(varTotal: Double);
      procedure SetIdNotaCreditoCliente(varIdNotaCreditoCliente: Integer);
      function GetCodigoProducto: string;
      function GetReferencia: string;
      function GetDescripcion: string;
      function GetCantidad: Double;
      function GetPrecio: Double;
      function GetPDescuento: Double;
      function GetMontoDescuento: Double;
      function GetImpuesto: Double;
      function GetPIva: Double;
      function GetTotal: Double;
      function GetIdNotaCreditoCliente: Integer;
      function ObtenerIdNotaCreditoCliente(varIdNotaCreditoCliente: Integer): TClientDataSet;
      function EliminarIdNotaCreditoCliente(varIdNotaCreditoCliente: Integer): Boolean;
  end;

implementation

uses DB;

{ TDetNotaCreditoCliente }

procedure TDetNotaCreditoCliente.Ajustar;
begin

end;

procedure TDetNotaCreditoCliente.Mover(Ds: TClientDataSet);
begin
  CodigoProducto := Ds.FieldValues['codigo_producto'];
  Referencia := Ds.FieldValues['referencia'];
  Descripcion := Ds.FieldValues['descripcion'];
  Cantidad := Ds.FieldValues['cantidad'];
  Precio := Ds.FieldValues['precio'];
  PDescuento := Ds.FieldValues['p_descuento'];
  MontoDescuento := Ds.FieldValues['monto_descuento'];
  Impuesto := Ds.FieldValues['impuesto'];
  PIva := Ds.FieldValues['p_iva'];
  Total := Ds.FieldValues['total'];
  IdNotaCreditoCliente := Ds.FieldValues['id_nota_credito_cliente'];
end;

function TDetNotaCreditoCliente.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,11);
  Params[0] := CodigoProducto;
  Params[1] := Referencia;
  Params[2] := Descripcion;
  Params[3] := Cantidad;
  Params[4] := Precio;
  Params[5] := PDescuento;
  Params[6] := MontoDescuento;
  Params[7] := Impuesto;
  Params[8] := PIva;
  Params[9] := Total;
  Params[10] := IdNotaCreditoCliente;
  Result:= Params;
end;

function TDetNotaCreditoCliente.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,3);
  Campos[0] := 'codigo_producto';
  Campos[1] := 'referencia';
  Campos[2] := 'descripcion';
  Result:= Campos;
end;

function TDetNotaCreditoCliente.GetCondicion: string;
begin
  Result:= '"id_nota_credito_cliente" =' + IntToStr(IdNotaCreditoCliente) + ' AND "codigo_producto" =' + QuotedStr(CodigoProducto)
end;

procedure TDetNotaCreditoCliente.AntesInsertar;
begin

end;

procedure TDetNotaCreditoCliente.AntesModificar;
begin

end;

procedure TDetNotaCreditoCliente.AntesEliminar;
begin

end;

procedure TDetNotaCreditoCliente.DespuesInsertar;
begin

end;

procedure TDetNotaCreditoCliente.DespuesModificar;
begin

end;

procedure TDetNotaCreditoCliente.DespuesEliminar;
begin

end;

constructor TDetNotaCreditoCliente.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_det_nota_credito_clientes');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(11)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "referencia"=$2,"descripcion"=$3,"cantidad"=$4,"precio"=$5,"p_descuento"=$6,"monto_descuento"=$7,"impuesto"=$8,"p_iva"=$9,"total"=$10 WHERE ("id_facturacion_cliente"=$11) AND ("codigo_producto"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("id_nota_credito_cliente"=$11) AND ("codigo_producto"=$1)');
  SetLength(ClavesPrimarias,2);
  ClavesPrimarias[0] := '"id_nota_credito_cliente"';
  ClavesPrimarias[1] := '"codigo_producto"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TDetNotaCreditoCliente.SetCantidad(varCantidad: Double);
begin
  Cantidad := varCantidad;
end;

procedure TDetNotaCreditoCliente.SetCodigoProducto(varCodigoProducto: string);
begin
  CodigoProducto := varCodigoProducto;
end;

procedure TDetNotaCreditoCliente.SetDescripcion(varDescripcion: string);
begin
  Descripcion := varDescripcion;
end;

procedure TDetNotaCreditoCliente.SetIdNotaCreditoCliente(varIdNotaCreditoCliente: Integer);
begin
  IdNotaCreditoCliente := varIdNotaCreditoCliente;
end;

procedure TDetNotaCreditoCliente.SetImpuesto(varImpuesto: Double);
begin
  Impuesto := varImpuesto;
end;

procedure TDetNotaCreditoCliente.SetMontoDescuento(varMontoDescuento: Double);
begin
  MontoDescuento := varMontoDescuento;
end;

procedure TDetNotaCreditoCliente.SetPDescuento(varPDescuento: Double);
begin
  PDescuento := varPDescuento;
end;

procedure TDetNotaCreditoCliente.SetPIva(varPIva: Double);
begin
  PIva := varPIva;
end;

procedure TDetNotaCreditoCliente.SetPrecio(varPrecio: Double);
begin
  Precio := varPrecio;
end;

procedure TDetNotaCreditoCliente.SetReferencia(varReferencia: string);
begin
  Referencia := varReferencia;
end;

procedure TDetNotaCreditoCliente.SetTotal(varTotal: Double);
begin
  Total := varTotal;
end;

function TDetNotaCreditoCliente.GetCantidad: Double;
begin
  Result := Cantidad;
end;

function TDetNotaCreditoCliente.GetCodigoProducto: string;
begin
  Result := CodigoProducto;
end;

function TDetNotaCreditoCliente.GetDescripcion: string;
begin
  Result := Descripcion;
end;

function TDetNotaCreditoCliente.GetIdNotaCreditoCliente: Integer;
begin
  Result := IdNotaCreditoCliente;
end;

function TDetNotaCreditoCliente.GetImpuesto: Double;
begin
  Result := Impuesto;
end;

function TDetNotaCreditoCliente.GetMontoDescuento: Double;
begin
  Result := MontoDescuento;
end;

function TDetNotaCreditoCliente.GetPDescuento: Double;
begin
  Result := PDescuento;
end;

function TDetNotaCreditoCliente.GetPIva: Double;
begin
  Result := PIva;
end;

function TDetNotaCreditoCliente.GetPrecio: Double;
begin
  Result := Precio;
end;

function TDetNotaCreditoCliente.GetReferencia: string;
begin
  Result := Referencia;
end;

function TDetNotaCreditoCliente.GetTotal: Double;
begin
  Result := Total;
end;

function TDetNotaCreditoCliente.ObtenerIdNotaCreditoCliente(varIdNotaCreditoCliente: Integer): TClientDataSet;
begin
  Result := ObtenerListaCondicionSQL(' WHERE "id_nota_credito_cliente" = ' + IntToStr(varIdNotaCreditoCliente));
end;

function TDetNotaCreditoCliente.EliminarIdNotaCreditoCliente(varIdNotaCreditoCliente: Integer): Boolean;
begin
  Result := EjecutarSQL('DELETE FROM ' + GetTabla + ' WHERE "id_nota_credito_cliente" = ' + IntToStr(varIdNotaCreditoCliente));
end;

end.
