unit ClsCondominioReciboInmuebleGasto;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TCondominioReciboInmuebleGasto }

  TCondominioReciboInmuebleGasto = class(TGenericoBD)
    private
      CodigoConjuntoAdministracion: string;
      PeriodoA: Integer;
      PeriodoM: Integer;
      CodigoInmueble: string;
      CodigoGasto: string;
      Monto: Double;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigoConjuntoAdministracion(varCodigoConjuntoAdministracion: string);
      procedure SetPeriodoA(varPeriodoA: Integer);
      procedure SetPeriodoM(varPeriodoM: Integer);
      procedure SetCodigoInmueble(varCodigoInmueble: string);
      procedure SetCodigoGasto(varCodigoGasto: string);
      procedure SetMonto(varMonto: Double);
      function GetCodigoConjuntoAdministracion: string;
      function GetPeriodoA: Integer;
      function GetPeriodoM: Integer;
      function GetCodigoInmueble: string;
      function GetCodigoGasto: string;
      function GetMonto: Double;
      function ObtenerRecibos(varCodigoConjuntoAdministracion: string; varPeriodoA: Integer; varPeriodoM: Integer): TClientDataSet;
      function EliminarRecibos(varCodigoConjuntoAdministracion: string; varPeriodoA: Integer; varPeriodoM: Integer): Boolean;
  end;

var
  Clase: TCondominioReciboInmuebleGasto;

implementation

{ TCondominioReciboInmuebleGasto }

procedure TCondominioReciboInmuebleGasto.Ajustar;
begin

end;

procedure TCondominioReciboInmuebleGasto.Mover(Ds: TClientDataSet);
begin
  CodigoConjuntoAdministracion := Ds.FieldValues['codigo_conjunto_administracion'];
  PeriodoA := Ds.FieldValues['periodo_a'];
  PeriodoM := Ds.FieldValues['periodo_m'];
  CodigoInmueble := Ds.FieldValues['codigo_inmueble'];
  CodigoGasto := Ds.FieldValues['codigo_gasto'];
  Monto := Ds.FieldValues['monto'];
end;

function TCondominioReciboInmuebleGasto.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,6);
  Params[0] := CodigoConjuntoAdministracion;
  Params[1] := PeriodoA;
  Params[2] := PeriodoM;
  Params[3] := CodigoInmueble;
  Params[4] := CodigoGasto;
  Params[5] := Monto;
  Result := Params;
end;

function TCondominioReciboInmuebleGasto.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,5);
  Campos[0] := 'codigo_conjunto_administracion';
  Campos[1] := 'periodo_a';
  Campos[2] := 'periodo_m';
  Campos[3] := 'codigo_inmueble';
  Campos[4] := 'codigo_gasto';
  Result := Campos;
end;

function TCondominioReciboInmuebleGasto.GetCondicion: string;
begin
  Result := '"codigo_conjunto_administracion" = ' + QuotedStr(CodigoConjuntoAdministracion) + ' AND ' +
            '"periodo_a" = ' + IntToStr(PeriodoA) + ' AND ' +
            '"periodo_m" = ' + IntToStr(PeriodoM) + ' AND ' +
            '"codigo_inmueble" = ' + QuotedStr(CodigoInmueble) + ' AND ' +
            '"codigo_gasto" = ' + QuotedStr(CodigoGasto);
end;

procedure TCondominioReciboInmuebleGasto.AntesEliminar;
begin

end;

procedure TCondominioReciboInmuebleGasto.AntesInsertar;
begin

end;

procedure TCondominioReciboInmuebleGasto.AntesModificar;
begin

end;

procedure TCondominioReciboInmuebleGasto.DespuesEliminar;
begin

end;

procedure TCondominioReciboInmuebleGasto.DespuesInsertar;
begin

end;

procedure TCondominioReciboInmuebleGasto.DespuesModificar;
begin

end;

constructor TCondominioReciboInmuebleGasto.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_condominio_recibos_inmuebles_gastos');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(6)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "monto"=$6 ' +
                  'WHERE ("codigo_conjunto_administracion"=$1 AND "periodo_a"=$2 AND "periodo_m"=$3 AND "codigo_inmueble"=$4 AND "codigo_gasto"=$5)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("codigo_conjunto_administracion"=$1 AND "periodo_a"=$2 AND "periodo_m"=$3 AND "codigo_inmueble"=$4 AND "codigo_gasto"=$5)');
  SetLength(ClavesPrimarias,5);
  ClavesPrimarias[0] := '"codigo_conjunto_administracion"';
  ClavesPrimarias[1] := '"periodo_a"';
  ClavesPrimarias[2] := '"periodo_m"';
  ClavesPrimarias[3] := '"codigo_inmueble"';
  ClavesPrimarias[4] := '"codigo_gasto"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TCondominioReciboInmuebleGasto.SetCodigoConjuntoAdministracion(varCodigoConjuntoAdministracion: string);
begin
  CodigoConjuntoAdministracion := varCodigoConjuntoAdministracion;
end;

procedure TCondominioReciboInmuebleGasto.SetPeriodoA(varPeriodoA: Integer);
begin
  PeriodoA := varPeriodoA;
end;

procedure TCondominioReciboInmuebleGasto.SetPeriodoM(varPeriodoM: Integer);
begin
  PeriodoM := varPeriodoM;
end;

procedure TCondominioReciboInmuebleGasto.SetCodigoInmueble(varCodigoInmueble: string);
begin
  CodigoInmueble := varCodigoInmueble;
end;

procedure TCondominioReciboInmuebleGasto.SetCodigoGasto(varCodigoGasto: string);
begin
  CodigoGasto := varCodigoGasto;
end;

procedure TCondominioReciboInmuebleGasto.SetMonto(varMonto: Double);
begin
  Monto := varMonto;
end;

function TCondominioReciboInmuebleGasto.GetCodigoConjuntoAdministracion: string;
begin
  Result := CodigoConjuntoAdministracion;
end;

function TCondominioReciboInmuebleGasto.GetPeriodoA: Integer;
begin
  Result := PeriodoA;
end;

function TCondominioReciboInmuebleGasto.GetPeriodoM: Integer;
begin
  Result := PeriodoM;
end;

function TCondominioReciboInmuebleGasto.GetCodigoInmueble: string;
begin
  Result := CodigoInmueble;
end;

function TCondominioReciboInmuebleGasto.GetCodigoGasto: string;
begin
  Result := CodigoGasto;
end;

function TCondominioReciboInmuebleGasto.GetMonto: Double;
begin
  Result := Monto;
end;

function TCondominioReciboInmuebleGasto.ObtenerRecibos(
  varCodigoConjuntoAdministracion: string; varPeriodoA,
  varPeriodoM: Integer): TClientDataSet;
begin
  Result := ObtenerListaCondicionSQL(' WHERE ("codigo_conjunto_administracion" = ' + QuotedStr(varCodigoConjuntoAdministracion) + ') AND ("periodo_a" = ' + IntToStr(varPeriodoA) + ') AND ("periodo_m" = ' + IntToStr(varPeriodoM) + ')');
end;

function TCondominioReciboInmuebleGasto.EliminarRecibos(
  varCodigoConjuntoAdministracion: string; varPeriodoA,
  varPeriodoM: Integer): Boolean;
begin
  Result := EjecutarSQL('DELETE FROM ' + GetTabla + ' WHERE ("codigo_conjunto_administracion" = ' + QuotedStr(varCodigoConjuntoAdministracion) + ') AND ("periodo_a" = ' + IntToStr(varPeriodoA) + ') AND ("periodo_m" = ' + IntToStr(varPeriodoM) + ')');
end;

end.
