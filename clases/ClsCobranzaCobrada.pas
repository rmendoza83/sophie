unit ClsCobranzaCobrada;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TCobranzaCobrada }

  TCobranzaCobrada = class(TGenericoBD)
    private
      Desde: string;
      NumeroDocumento: string;
      CodigoMoneda: string;
      CodigoCliente: string;
      CodigoVendedor: string;
      CodigoZona: string;
      FechaIngreso: TDate;
      FechaRecepcion: TDate;
      FechaVencIngreso: TDate;
      FechaVencRecepcion: TDate;
      FechaLibros: TDate;
      FechaContable: TDate;
      FechaCobranza: TDate;
      NumeroCuota: Integer;
      TotalCuotas: Integer;
      OrdenCobranza: Integer;
      MontoNeto: Double;
      Impuesto: Double;
      Total: Double;
      PIva: Double;
      IdCobranza: Integer;
      IdContadoCobranza: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArraystring; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetDesde(varDesde: string);
      procedure SetNumeroDocumento(varNumeroDocumento: string);
      procedure SetCodigoMoneda(varCodigoMoneda: string);
      procedure SetCodigoCliente(varCodigoCliente: string);
      procedure SetCodigoVendedor(varCodigoVendedor: string);
      procedure SetCodigoZona(varCodigoZona: string);
      procedure SetFechaIngreso(varFechaIngreso: TDate);
      procedure SetFechaRecepcion(varFechaRecepcion: TDate);
      procedure SetFechaVencIngreso(varFechaVencIngreso: TDate);
      procedure SetFechaVencRecepcion(varFechaVencRecepcion: TDate);
      procedure SetFechaLibros(varFechaLibros: TDate);
      procedure SetFechaContable(varFechaContable: TDate);
      procedure SetFechaCobranza(varFechaCobranza: TDate);
      procedure SetNumeroCuota(varNumeroCuota: Integer);
      procedure SetTotalCuotas(varTotalCuota: Integer);
      procedure SetOrdenCobranza(varOrdenCobranza: Integer);
      procedure SetMontoNeto(varMontoNeto: Double);
      procedure SetImpuesto(varImpuesto: Double);
      procedure SetTotal(varTotal: Double);
      procedure SetPIva(varPIva: Double);
      procedure SetIdCobranza(varIdCobranza: Integer);
      procedure SetIdContadoCobranza(varIdContadoCobranza: Integer);
      function GetDesde: string;
      function GetNumeroDocumento: string;
      function GetCodigoMoneda: string;
      function GetCodigoCliente: string;
      function GetCodigoVendedor: string;
      function GetCodigoZona: string;
      function GetFechaIngreso: TDate;
      function GetFechaRecepcion: TDate;
      function GetFechaVencIngreso: TDate;
      function GetFechaVencRecepcion: TDate;
      function GetFechaLibros: TDate;
      function GetFechaContable: TDate;
      function GetFechaCobranza: TDate;
      function GetNumeroCuota: Integer;
      function GetTotalCuotas: Integer;
      function GetOrdenCobranza: Integer;
      function GetMontoNeto: Double;
      function GetImpuesto: Double;
      function GetTotal: Double;
      function GetPIva: Double;
      function GetIdCobranza: Integer;
      function GetIdContadoCobranza: Integer;
      function EliminarIdCobranza(varIdCobranza: Integer): Boolean;
      function BuscarIdCobranza: Boolean;
      function VerificarOrdenCobranza: Boolean;
  end;

implementation

{ TCobranzaCobrada }

procedure TCobranzaCobrada.Ajustar;
begin

end;

procedure TCobranzaCobrada.Mover(Ds: TClientDataSet);
begin
  Desde := Ds.FieldValues['desde'];
  NumeroDocumento := Ds.FieldValues['numero_documento'];
  CodigoMoneda := Ds.FieldValues['codigo_moneda'];
  CodigoCliente := Ds.FieldValues['codigo_cliente'];
  CodigoVendedor := Ds.FieldValues['codigo_vendedor'];
  CodigoZona := Ds.FieldValues['codigo_zona'];
  FechaIngreso := Ds.FieldByName('fecha_ingreso').AsDateTime;
  FechaRecepcion := Ds.FieldByName('fecha_recepcion').AsDateTime;
  FechaVencIngreso := Ds.FieldByName('fecha_venc_ingreso').AsDateTime;
  FechaVencRecepcion := Ds.FieldByName('fecha_venc_recepcion').AsDateTime;
  FechaLibros := Ds.FieldByName('fecha_libros').AsDateTime;
  FechaContable := Ds.FieldByName('fecha_contable').AsDateTime;
  FechaCobranza:= Ds.FieldByName('fecha_cobranza').AsDateTime;
  NumeroCuota := Ds.FieldValues['numero_cuota'];
  TotalCuotas := Ds.FieldValues['total_cuotas'];
  OrdenCobranza := Ds.FieldValues['orden_cobranza'];
  MontoNeto := Ds.FieldValues['monto_neto'];
  Impuesto := Ds.FieldValues['impuesto'];
  Total := Ds.FieldValues['total'];
  PIva := Ds.FieldValues['p_iva'];
  IdCobranza := Ds.FieldValues['id_cobranza'];
  IdContadoCobranza := Ds.FieldValues['id_contado_cobranza'];
end;

function TCobranzaCobrada.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,22);
  Params[0] := Desde;
  Params[1] := NumeroDocumento;
  Params[2] := CodigoMoneda;
  Params[3] := CodigoCliente;
  Params[4] := CodigoVendedor;
  Params[5] := CodigoZona;
  Params[6] := VarFromDateTime(FechaIngreso);
  Params[7] := VarFromDateTime(FechaRecepcion);
  Params[8] := VarFromDateTime(FechaVencIngreso);
  Params[9] := VarFromDateTime(FechaVencRecepcion);
  Params[10] := VarFromDateTime(FechaLibros);
  Params[11] := VarFromDateTime(FechaContable);
  Params[12] := VarFromDateTime(FechaCobranza);
  Params[13] := NumeroCuota;
  Params[14] := TotalCuotas;
  Params[15] := OrdenCobranza;
  Params[16] := MontoNeto;
  Params[17] := Impuesto;
  Params[18] := Total;
  Params[19] := PIva;
  Params[20] := IdCobranza;
  Params[21] := IdContadoCobranza;
  Result := Params;
end;

function TCobranzaCobrada.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,6);
  Campos[0] := 'desde';
  Campos[1] := 'numero_documento';
  Campos[2] := 'codigo_moneda';
  Campos[3] := 'codigo_cliente';
  Campos[4] := 'codigo_vendedor';
  Campos[5] := 'codigo_zona';
  Result := Campos;
end;

function TCobranzaCobrada.GetCondicion: string;
begin
  Result := '"id_cobranza" = ' + IntToStr(IdCobranza) + ' AND "numero_cuota" =' + IntToStr(NumeroCuota) + ' AND "orden_cobranza" =' + IntToStr(OrdenCobranza);
end;

procedure TCobranzaCobrada.AntesInsertar;
begin

end;

procedure TCobranzaCobrada.AntesModificar;
begin

end;

procedure TCobranzaCobrada.AntesEliminar;
begin

end;

procedure TCobranzaCobrada.DespuesEliminar;
begin

end;

procedure TCobranzaCobrada.DespuesInsertar;
begin

end;

procedure TCobranzaCobrada.DespuesModificar;
begin

end;

constructor TCobranzaCobrada.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_cobranzas_cobradas');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(22)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "desde"=$1,"numero_documento"=$2,"codigo_moneda"=$3,"codigo_cliente"=$4,"codigo_vendedor"=$5,"codigo_zona"=$6,"fecha_ingreso"=$7,"fecha_recepcion"=$8,"fecha_venc_ingreso"=$9, ' +
                   '"fecha_venc_recepcion"=$10,"fecha_libros"=$11,"fecha_contable"=$12,"fecha_cobranza"=$13,"total_cuotas"=$15,"monto_neto"=$17,"impuesto"=$18,"total"=$19,"p_iva"=$20, "id_contado_cobranza"=$22, ' +
                   ' WHERE ("id_cobranza"=$21) AND ("numero_cuota"=$14) AND ("orden_cobranza"=$16)');
  SetStrSQLDelete ('DELETE FROM ' + GetTabla + ' WHERE ("id_cobranza"=$21) AND ("numero_cuota"=$14) AND ("orden_cobranza"=$16)');
  SetLength(ClavesPrimarias,3);
  ClavesPrimarias[0] := '"id_cobranza"';
  ClavesPrimarias[1] := '"numero_cuota"';
  ClavesPrimarias[2] := '"orden_cobranza"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TCobranzaCobrada.SetCodigoCliente(varCodigoCliente: string);
begin
  CodigoCliente := varCodigoCliente;
end;

procedure TCobranzaCobrada.SetCodigoMoneda(varCodigoMoneda: string);
begin
  CodigoMoneda := varCodigoMoneda;
end;

procedure TCobranzaCobrada.SetCodigoVendedor(varCodigoVendedor: string);
begin
  CodigoVendedor := varCodigoVendedor;
end;

procedure TCobranzaCobrada.SetCodigoZona(varCodigoZona: string);
begin
  CodigoZona := varCodigoZona;
end;

procedure TCobranzaCobrada.SetDesde(varDesde: string);
begin
  Desde := varDesde;
end;

procedure TCobranzaCobrada.SetFechaContable(varFechaContable: TDate);
begin
 FechaContable :=varFechaContable;
end;

procedure TCobranzaCobrada.SetFechaCobranza(varFechaCobranza: TDate);
begin
 FechaCobranza :=varFechaCobranza;
end;

procedure TCobranzaCobrada.SetFechaIngreso(varFechaIngreso: TDate);
begin
  FechaIngreso := varFechaIngreso;
end;

procedure TCobranzaCobrada.SetFechaLibros(varFechaLibros: TDate);
begin
  FechaLibros := varFechaLibros;
end;

procedure TCobranzaCobrada.SetFechaRecepcion(varFechaRecepcion: TDate);
begin
  FechaRecepcion := varFechaRecepcion;
end;

procedure TCobranzaCobrada.SetFechaVencIngreso(varFechaVencIngreso: TDate);
begin
 FechaVencIngreso := varFechaVencIngreso;
end;

procedure TCobranzaCobrada.SetFechaVencRecepcion(varFechaVencRecepcion: TDate);
begin
   FechaVencRecepcion := varFechaVencRecepcion;
end;

procedure TCobranzaCobrada.SetIdCobranza(varIdCobranza: Integer);
begin
  IdCobranza := varIdCobranza;
end;

procedure TCobranzaCobrada.SetIdContadoCobranza(varIdContadoCobranza: Integer);
begin
   IdContadoCobranza := varIdContadoCobranza;
end;

procedure TCobranzaCobrada.SetImpuesto(varImpuesto: Double);
begin
  Impuesto := varImpuesto;
end;

procedure TCobranzaCobrada.SetMontoNeto(varMontoNeto: Double);
begin
  MontoNeto := varMontoNeto;
end;

procedure TCobranzaCobrada.SetNumeroDocumento(varNumeroDocumento: string);
begin
  NumeroDocumento := varNumeroDocumento;
end;

procedure TCobranzaCobrada.SetNumeroCuota(varNumeroCuota: Integer);
begin
  NumeroCuota := varNumeroCuota;
end;

procedure TCobranzaCobrada.SetPIva(varPIva: Double);
begin
  PIva := varPIva;
end;

procedure TCobranzaCobrada.SetTotal(varTotal: Double);
begin
  Total := varTotal;
end;

procedure TCobranzaCobrada.SetTotalCuotas(varTotalCuota: Integer);
begin
  TotalCuotas := varTotalCuota;
end;

procedure TCobranzaCobrada.SetOrdenCobranza(varOrdenCobranza: Integer);
begin
  OrdenCobranza := varOrdenCobranza;
end;

function TCobranzaCobrada.GetPIva: Double;
begin
  Result := PIva;
end;

function TCobranzaCobrada.GetTotal: Double;
begin
  Result := Total;
end;

function TCobranzaCobrada.GetTotalCuotas: Integer;
begin
  Result := TotalCuotas;
end;

function TCobranzaCobrada.GetOrdenCobranza: Integer;
begin
  Result := OrdenCobranza;
end;

function TCobranzaCobrada.GetCodigoCliente: string;
begin
  Result := CodigoCliente;
end;

function TCobranzaCobrada.GetCodigoMoneda: string;
begin
  Result := CodigoMoneda;
end;

function TCobranzaCobrada.GetCodigoVendedor: string;
begin
  Result := CodigoVendedor;
end;

function TCobranzaCobrada.GetCodigoZona: string;
begin
  Result := CodigoZona;
end;

function TCobranzaCobrada.GetDesde: string;
begin
  Result := Desde;
end;

function TCobranzaCobrada.GetFechaContable: TDate;
begin
  Result := FechaContable;
end;

function TCobranzaCobrada.GetFechaCobranza: TDate;
begin
  Result := FechaCobranza;
end;

function TCobranzaCobrada.GetFechaIngreso: TDate;
begin
  Result := FechaIngreso;
end;

function TCobranzaCobrada.GetFechaLibros: TDate;
begin
  Result := FechaLibros;
end;

function TCobranzaCobrada.GetFechaRecepcion: TDate;
begin
  Result := FechaRecepcion;
end;

function TCobranzaCobrada.GetFechaVencIngreso: TDate;
begin
  Result :=FechaVencIngreso;
end;

function TCobranzaCobrada.GetFechaVencRecepcion: TDate;
begin
  Result := FechaVencRecepcion
end;

function TCobranzaCobrada.GetIdCobranza: Integer;
begin
  Result := IdCobranza;
end;

function TCobranzaCobrada.GetIdContadoCobranza: Integer;
begin
  Result := IdContadoCobranza;
end;

function TCobranzaCobrada.GetImpuesto: Double;
begin
  Result := Impuesto;
end;

function TCobranzaCobrada.GetMontoNeto: Double;
begin
  Result := MontoNeto;
end;

function TCobranzaCobrada.GetNumeroDocumento: string;
begin
  Result := NumeroDocumento;
end;

function TCobranzaCobrada.GetNumeroCuota: Integer;
begin
  Result := NumeroCuota;
end;

function TCobranzaCobrada.EliminarIdCobranza(varIdCobranza: Integer): Boolean;
begin
  Result := EjecutarSQL('DELETE FROM ' + GetTabla + ' WHERE "id_cobranza" = ' + IntToStr(varIdCobranza));
end;

function TCobranzaCobrada.BuscarIdCobranza: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("id_cobranza" = ' + IntToStr(GetIdCobranza) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

function TCobranzaCobrada.VerificarOrdenCobranza: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("id_cobranza" = ' + IntToStr(GetIdCobranza) + ') AND ("numero_cuota" = ' + IntToStr(GetNumeroCuota) + ') AND ("orden_cobranza" > ' + IntToStr(GetOrdenCobranza) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
  end;
end;

end.
