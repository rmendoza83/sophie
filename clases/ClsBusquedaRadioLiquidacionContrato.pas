unit ClsBusquedaRadioLiquidacionContrato;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TBusquedaRadioLiquidacionContrato }

  TBusquedaRadioLiquidacionContrato = class(TGenericoBD)
    private
      Numero: string;
      CodigoCliente: string;
      Rif: string;
      Nit: string;
      RazonSocial: string;
      Contacto: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      function GetNumero: string;
      function GetCodigoCliente: string;
      function GetRif: string;
      function GetNit: string;
      function GetRazonSocial: string;
      function GetContacto: string;
  end;

implementation

{ TBusquedaRadioLiquidacionContrato }

procedure TBusquedaRadioLiquidacionContrato.Ajustar;
begin

end;

procedure TBusquedaRadioLiquidacionContrato.Mover(Ds: TClientDataSet);
begin
  Numero := Ds.FieldValues['numero'];
  CodigoCliente := Ds.FieldValues['codigo_cliente'];
  Rif := Ds.FieldValues['rif'];
  Nit := Ds.FieldValues['nit'];
  RazonSocial := Ds.FieldValues['razon_social'];
  Contacto := Ds.FieldValues['contacto'];
end;

function TBusquedaRadioLiquidacionContrato.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,6);
  Params[0] := Numero;
  Params[1] := CodigoCliente;
  Params[2] := Rif;
  Params[3] := Nit;
  Params[4] := RazonSocial;
  Params[5] := Contacto;
  Result := Params;
end;

function TBusquedaRadioLiquidacionContrato.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,6);
  Campos[0] := 'numero';
  Campos[1] := 'codigo_cliente';
  Campos[2] := 'rif';
  Campos[3] := 'nit';
  Campos[4] := 'razon_social';
  Campos[5] := 'contacto';
  Result := Campos;
end;

function TBusquedaRadioLiquidacionContrato.GetCondicion: string;
begin
  Result:= '';
end;

procedure TBusquedaRadioLiquidacionContrato.AntesInsertar;
begin

end;

procedure TBusquedaRadioLiquidacionContrato.AntesModificar;
begin

end;

procedure TBusquedaRadioLiquidacionContrato.AntesEliminar;
begin

end;

procedure TBusquedaRadioLiquidacionContrato.DespuesInsertar;
begin

end;

procedure TBusquedaRadioLiquidacionContrato.DespuesModificar;
begin

end;

procedure TBusquedaRadioLiquidacionContrato.DespuesEliminar;
begin

end;

constructor TBusquedaRadioLiquidacionContrato.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('v_busqueda_radio_liquidacion_contratos');
  SetLength(ClavesPrimarias,0);
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

function TBusquedaRadioLiquidacionContrato.GetCodigoCliente: string;
begin
  Result := CodigoCliente;
end;

function TBusquedaRadioLiquidacionContrato.GetContacto: string;
begin
  Result := Contacto;
end;

function TBusquedaRadioLiquidacionContrato.GetNit: string;
begin
  Result := Nit;
end;

function TBusquedaRadioLiquidacionContrato.GetNumero: string;
begin
  Result := Numero;
end;

function TBusquedaRadioLiquidacionContrato.GetRazonSocial: String;
begin
  Result := RazonSocial;
end;

function TBusquedaRadioLiquidacionContrato.GetRif: String;
begin
  Result := Rif;
end;

end.
