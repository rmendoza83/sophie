unit ClsOperacionBancaria;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type
  TOperacionBancaria = class(TGenericoBD)
    private
      CodigoCuenta: string;
      CodigoTipoOperacion: string;
      Numero: string;
      Fecha: TDate;
      FechaContable: TDate;
      Monto: Double;
      Concepto: string;
      Observaciones: string;
      Diferido: Boolean;
      Conciliado: Boolean;
      FechaConciliacion: TDate;
      Beneficiario: string;
      LoginUsuario: string;
      IdOperacionBancaria: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigoCuenta(varCodigoCuenta: string);
      procedure SetCodigoTipoOperacion(varCodigoTipoOperacion: string);
      procedure SetNumero(varNumero: string);
      procedure SetFecha(varFecha: TDate);
      procedure SetFechaContable(varFechaContable: TDate);
      procedure SetMonto(varMonto: Double);
      procedure SetConcepto(varConcepto: string);
      procedure SetObservaciones(varObservaciones: string);
      procedure SetDiferido(varDiferido: Boolean);
      procedure SetConciliado(varConciliado: Boolean);
      procedure SetFechaConciliacion(varFechaConciliacio: TDate);
      procedure SetBeneficiario(varBeneficiario: string);
      procedure SetLoginUsuario(varLoginUsuario: string);
      procedure SetIdOperacionBancaria(varIdOperacionBancaria: Integer);
      function GetCodigoCuenta: string;
      function GetCodigoTipoOperacion: string;
      function GetNumero: string;
      function GetFecha: TDate;
      function GetFechaContable: TDate;
      function GetMonto: Double;
      function GetConcepto: string;
      function GetObservaciones: string;
      function GetDiferido: Boolean;
      function GetConciliado: Boolean;
      function GetFechaConciliacion: TDate;
      function GetBeneficiario: string;
      function GetLoginUsuario: string;
      function GetIdOperacionBancaria: Integer;
      function BuscarIdOperacionBancaria: Boolean;
  end;

implementation

uses
  DB,
  ClsConsecutivo;

{ TOperacionBancaria }

procedure TOperacionBancaria.Ajustar;
begin

end;

procedure TOperacionBancaria.Mover(Ds: TClientDataSet);
begin
  CodigoCuenta := Ds.FieldValues['codigo_cuenta'];
  CodigoTipoOperacion := Ds.FieldValues['codigo_tipo_operacion_bancaria'];
  Numero := Ds.FieldValues['numero'];
  Fecha := Ds.FieldByName('fecha').AsDateTime;
  FechaContable := Ds.FieldByName('fecha_contable').AsDateTime;
  Monto := Ds.FieldValues['monto'];
  Concepto := Ds.FieldValues['concepto'];
  Observaciones := Ds.FieldValues['observaciones'];
  Diferido := Ds.FieldValues['diferido'];
  Conciliado := Ds.FieldValues['conciliado'];
  FechaConciliacion := ds.FieldByName('fecha_conciliacion').AsDateTime;
  Beneficiario := Ds.FieldValues['beneficiario'];
  LoginUsuario := Ds.FieldValues['login_usuario'];
  IdOperacionBancaria := Ds.FieldValues['id_operacion_bancaria'];
end;

function TOperacionBancaria.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,14);
  Params[0] := CodigoCuenta;
  Params[1] := CodigoTipoOperacion;
  Params[2] := Numero;
  Params[3] := VarFromDateTime(Fecha) ;
  Params[4] := VarFromDateTime(FechaContable);
  Params[5] := Monto;
  Params[6] := Concepto;
  Params[7] := Observaciones;
  Params[8] := Diferido;
  Params[9] := Conciliado;
  Params[10] := VarFromDateTime(FechaConciliacion);
  Params[11] := Beneficiario;
  Params[12] := LoginUsuario;
  Params[13] := IdOperacionBancaria;
  Result := Params;
end;

function TOperacionBancaria.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,7);
  Campos[0] := 'codigo_cuenta';
  Campos[1] := 'codigo_tipo_operacion_bancaria';
  Campos[2] := 'numero';
  Campos[3] := 'concepto';
  Campos[4] := 'observaciones';
  Campos[5] := 'beneficiario';
  Campos[6] := 'login_usuario';
  Result := Campos;
end;

function TOperacionBancaria.GetCondicion: string;
begin
  Result := '"codigo_cuenta" = ' + QuotedStr(CodigoCuenta) + 'AND "codigo_tipo_operacion_bancaria" =' + QuotedStr(CodigoTipoOperacion) + ' AND "numero" =' + QuotedStr(Numero);
end;

procedure TOperacionBancaria.AntesInsertar;
var
  Consecutivo: TConsecutivo;
begin
  Consecutivo := TConsecutivo.Create;
  IdOperacionBancaria := Consecutivo.ObtenerConsecutivo('id_operacion_bancaria');
end;

procedure TOperacionBancaria.AntesModificar;
begin

end;

procedure TOperacionBancaria.AntesEliminar;
begin

end;

procedure TOperacionBancaria.DespuesInsertar;
begin

end;

procedure TOperacionBancaria.DespuesEliminar;
begin

end;

procedure TOperacionBancaria.DespuesModificar;
begin

end;

constructor TOperacionBancaria.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_operaciones_bancarias');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(14)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "fecha"=$4,"fecha_contable"=$5, "monto"=$6, "concepto"=$7, "observaciones"=$8, "diferido"=$9, "conciliado"=$10, ' +
                  '"fecha_conciliacion"=$11, "beneficiario"=$12, "login_usuario"=$13, "id_operacion_bancaria"=$14 WHERE ("codigo_cuenta"=$1) AND ("codigo_tipo_operacion_bancaria"=$2) AND ("numero" =$3)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("codigo_cuenta"=$1) AND ("codigo_tipo_operacion_bancaria"=$2) AND ("numero"=$3)');
  SetLength(ClavesPrimarias,3);
  ClavesPrimarias[0] := '"codigo_cuenta"';
  ClavesPrimarias[1] := '"codigo_tipo_operacion_bancaria"';
  ClavesPrimarias[2] := '"numero"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TOperacionBancaria.SetBeneficiario(varBeneficiario: string);
begin
  Beneficiario := varBeneficiario;
end;

procedure TOperacionBancaria.SetCodigoCuenta(varCodigoCuenta: string);
begin
  CodigoCuenta := varCodigoCuenta;
end;

procedure TOperacionBancaria.SetCodigoTipoOperacion(varCodigoTipoOperacion: string);
begin
  CodigoTipoOperacion := varCodigoTipoOperacion;
end;

procedure TOperacionBancaria.SetConcepto(varConcepto: string);
begin
  Concepto := varConcepto;
end;

procedure TOperacionBancaria.SetConciliado(varConciliado: Boolean);
begin
  Conciliado := varConciliado;
end;

procedure TOperacionBancaria.SetDiferido(varDiferido: Boolean);
begin
  Diferido :=  varDiferido;
end;

procedure TOperacionBancaria.SetFecha(varFecha: TDate);
begin
  Fecha := varFecha;
end;

procedure TOperacionBancaria.SetFechaConciliacion(varFechaConciliacio: TDate);
begin
  FechaConciliacion := varFechaConciliacio;
end;

procedure TOperacionBancaria.SetFechaContable(varFechaContable: TDate);
begin
  FechaContable := varFechaContable;
end;

procedure TOperacionBancaria.SetIdOperacionBancaria(
  varIdOperacionBancaria: Integer);
begin
  IdOperacionBancaria := varIdOperacionBancaria;
end;

procedure TOperacionBancaria.SetLoginUsuario(varLoginUsuario: string);
begin
  LoginUsuario := varLoginUsuario;
end;

procedure TOperacionBancaria.SetMonto(varMonto: Double);
begin
  Monto := varMonto;
end;

procedure TOperacionBancaria.SetNumero(varNumero: string);
begin
  Numero := varNumero;
end;

procedure TOperacionBancaria.SetObservaciones(varObservaciones: string);
begin
  Observaciones := varObservaciones;
end;

function TOperacionBancaria.GetBeneficiario: string;
begin
  Result := Beneficiario;
end;

function TOperacionBancaria.GetCodigoCuenta: string;
begin
  Result := CodigoCuenta;
end;

function TOperacionBancaria.GetCodigoTipoOperacion: string;
begin
  Result := CodigoTipoOperacion;
end;

function TOperacionBancaria.GetConcepto: string;
begin
  Result := Concepto;
end;

function TOperacionBancaria.GetConciliado: Boolean;
begin
  Result := Conciliado;
end;

function TOperacionBancaria.GetDiferido: Boolean;
begin
  Result := Diferido;
end;

function TOperacionBancaria.GetFecha: TDate;
begin
  Result := Fecha;
end;

function TOperacionBancaria.GetFechaConciliacion: TDate;
begin
  Result := FechaConciliacion;
end;

function TOperacionBancaria.GetFechaContable: TDate;
begin
  Result := FechaContable;
end;

function TOperacionBancaria.GetIdOperacionBancaria: Integer;
begin
  Result := IdOperacionBancaria;
end;

function TOperacionBancaria.GetLoginUsuario: string;
begin
  Result := LoginUsuario;
end;

function TOperacionBancaria.GetMonto: Double;
begin
  Result := Monto;
end;

function TOperacionBancaria.GetNumero: string;
begin
  Result := Numero;
end;

function TOperacionBancaria.GetObservaciones: string;
begin
  Result := Observaciones;
end;

function TOperacionBancaria.BuscarIdOperacionBancaria: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("id_operacion_bancaria" = ' + IntToStr(GetIdOperacionBancaria) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

end.
