unit ClsCondominioRecibo;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TCondominioRecibo }

  TCondominioRecibo = class(TGenericoBD)
    private
      CodigoConjuntoAdministracion: string;
      PeriodoA: Integer;
      PeriodoM: Integer;
      FechaIngreso: TDate;
      FechaVencimiento: TDate;
      MontoBase: Double;
      PReserva: Double;
      MontoReserva: Double;
      MontoTotal: Double;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigoConjuntoAdministracion(varCodigoConjuntoAdministracion: string);
      procedure SetPeriodoA(varPeriodoA: Integer);
      procedure SetPeriodoM(varPeriodoM: Integer);
      procedure SetFechaIngreso(varFechaIngreso: TDate);
      procedure SetFechaVencimiento(varFechaVencimiento: TDate);
      procedure SetMontoBase(varMontoBase: Double);
      procedure SetPReserva(varPReserva: Double);
      procedure SetMontoReserva(varMontoReserva: Double);
      procedure SetMontoTotal(varMontoTotal: Double);
      function GetCodigoConjuntoAdministracion: string;
      function GetPeriodoA: Integer;
      function GetPeriodoM: Integer;
      function GetFechaIngreso: TDate;
      function GetFechaVencimiento: TDate;
      function GetMontoBase: Double;
      function GetPReserva: Double;
      function GetMontoReserva: Double;
      function GetMontoTotal: Double;
  end;

var
  Clase: TCondominioRecibo;

implementation

{ TCondominioRecibo }

procedure TCondominioRecibo.Ajustar;
begin

end;

procedure TCondominioRecibo.Mover(Ds: TClientDataSet);
begin
  CodigoConjuntoAdministracion := Ds.FieldValues['codigo_conjunto_administracion'];
  PeriodoA := Ds.FieldValues['periodo_a'];
  PeriodoM := Ds.FieldValues['periodo_m'];
  FechaIngreso := Ds.FieldValues['fecha_ingreso'];
  FechaVencimiento := Ds.FieldValues['fecha_vencimiento'];
  MontoBase := Ds.FieldValues['monto_base'];
  PReserva := Ds.FieldValues['p_reserva'];
  MontoReserva := Ds.FieldValues['monto_reserva'];
  MontoTotal := Ds.FieldValues['monto_total'];
end;

function TCondominioRecibo.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,9);
  Params[0] := CodigoConjuntoAdministracion;
  Params[1] := PeriodoA;
  Params[2] := PeriodoM;
  Params[3] := VarFromDateTime(FechaIngreso);
  Params[4] := VarFromDateTime(FechaVencimiento);
  Params[5] := MontoBase;
  Params[6] := PReserva;
  Params[7] := MontoReserva;
  Params[8] := MontoTotal;
  Result := Params;
end;

function TCondominioRecibo.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,3);
  Campos[0] := 'codigo_conjunto_administracion';
  Campos[1] := 'periodo_a';
  Campos[2] := 'periodo_m';
  Result := Campos;
end;

function TCondominioRecibo.GetCondicion: string;
begin
  Result := '"codigo_conjunto_administracion" = ' + QuotedStr(CodigoConjuntoAdministracion) + ' AND ' +
            '"periodo_a" = ' + IntToStr(PeriodoA) + ' AND ' +
            '"periodo_m" = ' + IntToStr(PeriodoM);
end;

procedure TCondominioRecibo.AntesEliminar;
begin

end;

procedure TCondominioRecibo.AntesInsertar;
begin

end;

procedure TCondominioRecibo.AntesModificar;
begin

end;

procedure TCondominioRecibo.DespuesEliminar;
begin

end;

procedure TCondominioRecibo.DespuesInsertar;
begin

end;

procedure TCondominioRecibo.DespuesModificar;
begin

end;

constructor TCondominioRecibo.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_condominio_recibos');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(9)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "fecha_ingreso"=$4, "fecha_vencimiento"=$5, "monto_base"=$6, "p_reserva"=$7, "monto_reserva"=$8, "monto_total"=$9 ' +
                  'WHERE ("codigo_conjunto_administracion"=$1 AND "periodo_a"=$2 AND "periodo_m"=$3)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("codigo_conjunto_administracion"=$1 AND "periodo_a"=$2 AND "periodo_m"=$3)');
  SetLength(ClavesPrimarias,3);
  ClavesPrimarias[0] := '"codigo_conjunto_administracion"';
  ClavesPrimarias[1] := '"periodo_a"';
  ClavesPrimarias[2] := '"periodo_m"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TCondominioRecibo.SetCodigoConjuntoAdministracion(varCodigoConjuntoAdministracion: string);
begin
  CodigoConjuntoAdministracion := varCodigoConjuntoAdministracion;
end;

procedure TCondominioRecibo.SetPeriodoA(varPeriodoA: Integer);
begin
  PeriodoA := varPeriodoA;
end;

procedure TCondominioRecibo.SetPeriodoM(varPeriodoM: Integer);
begin
  PeriodoM := varPeriodoM;
end;

procedure TCondominioRecibo.SetFechaIngreso(varFechaIngreso: TDate);
begin
  FechaIngreso := varFechaIngreso;
end;

procedure TCondominioRecibo.SetFechaVencimiento(varFechaVencimiento: TDate);
begin
  FechaVencimiento := varFechaVencimiento;
end;

procedure TCondominioRecibo.SetMontoBase(varMontoBase: Double);
begin
  MontoBase := varMontoBase;
end;

procedure TCondominioRecibo.SetPReserva(varPReserva: Double);
begin
  PReserva := varPReserva;
end;

procedure TCondominioRecibo.SetMontoReserva(varMontoReserva: Double);
begin
  MontoReserva := varMontoReserva;
end;

procedure TCondominioRecibo.SetMontoTotal(varMontoTotal: Double);
begin
  MontoTotal := varMontoTotal;
end;

function TCondominioRecibo.GetCodigoConjuntoAdministracion: string;
begin
  Result := CodigoConjuntoAdministracion;
end;

function TCondominioRecibo.GetPeriodoA: Integer;
begin
  Result := PeriodoA;
end;

function TCondominioRecibo.GetPeriodoM: Integer;
begin
  Result := PeriodoM;
end;

function TCondominioRecibo.GetFechaIngreso: TDate;
begin
  Result := FechaIngreso;
end;

function TCondominioRecibo.GetFechaVencimiento: TDate;
begin
  Result := FechaVencimiento;
end;

function TCondominioRecibo.GetMontoBase: Double;
begin
  Result := MontoBase;
end;

function TCondominioRecibo.GetPReserva: Double;
begin
  Result := PReserva;
end;

function TCondominioRecibo.GetMontoReserva: Double;
begin
  Result := MontoReserva;
end;

function TCondominioRecibo.GetMontoTotal: Double;
begin
  Result := MontoTotal;
end;

end.
