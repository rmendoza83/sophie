unit ClsAnticipoCliente;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TAnticipoCliente }

  TAnticipoCliente = class(TGenericoBD)
    private
      Numero: string;
      CodigoMoneda: string;
      CodigoCliente: string;
      CodigoZona: string;
      FechaIngreso: TDate;
      FechaRecepcion: TDate;
      FechaContable: TDate;
      FechaLibros: TDate;
      Concepto: string;
      Observaciones: string;
      MontoNeto: Double;
      LoginUsuario: string;
      TiempoIngreso: TDateTime;
      CodigoFormaPago: string;
      NumeroFormaPago: string;
      CodigoCuentaBancaria: string;
      NumeroDocumentoBancario: string;
      CodigoBancoCobranza: string;
      FechaOperacionBancaria: TDate;
      IdAnticipoCliente: Integer;
      IdCobranza: Integer;
      IdContadoCobranza: Integer;
      IdOperacionBancaria: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArraystring; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetNumero(varNumero: string);
      procedure SetCodigoMoneda(varCodigoMoneda: string);
      procedure SetCodigoCliente(varCodigoCliente: string);
      procedure SetCodigoZona(varCodigoZona: string);
      procedure SetFechaIngreso(varFechaIngreso: TDate);
      procedure SetFechaRecepcion(varFechaRecepcion: TDate);
      procedure SetFechaContable(varFechaContable: TDate);
      procedure SetFechaLibros(varFechalibros: TDate);
      procedure SetConcepto(varConcepto: string);
      procedure SetObservaciones(varObservaciones: string);
      procedure SetMontoNeto(varMontoNeto: Double);
      procedure SetLoginUsuario(varLoginUsuario: string);
      procedure SetTiempoIngreso(varTiempodeIngreso: TDateTime);
      procedure SetCodigoFormaPago(varCodigoFormaPago: string);
      procedure SetNumeroFormaPago(varNumeroFormaPago: string);
      procedure SetCodigoCuentaBancaria(varCodigoCuentaBancaria: string);
      procedure SetNumeroDocumentoBancario(varNumeroDocumentoBancario: string);
      procedure SetCodigoBancoCobranza(varCodigoBancoCobranza: string);
      procedure SetFechaOperacionBancaria(varFechaOperacionBancaria: TDate);
      procedure SetIdAnticipoCliente(varIdAnticipoCliente: Integer);
      procedure SetIdCobranza(varIdCobranza: Integer);
      procedure SetIdContadoCobranza(varIdContadoCobranza: Integer);
      procedure SetIdOperacionBancaria(varIdOperacionBancaria: Integer);
      function GetNumero: string;
      function GetCodigoMoneda: string;
      function GetCodigoCliente: string;
      function GetCodigoZona: string;
      function GetFechaIngreso: TDate;
      function GetFechaRecepcion: TDate;
      function GetFechaContable: TDate;
      function GetFechaLibros: TDate;
      function GetConcepto: string;
      function GetObservaciones: string;
      function GetMontoNeto: Double;
      function GetLoginUsuario: string;
      function GetTiempoIngreso: TDateTime;
      function GetCodigoFormaPago: string;
      function GetNumeroFormaPago: string;
      function GetCodigoCuentaBancaria: string;
      function GetNumeroDocumentoBancario: string;
      function GetCodigoBancoCobranza: string;
      function GetFechaOperacionBancaria: TDate;
      function GetIdAnticipoCliente: Integer;
      function GetIdCobranza: Integer;
      function GetIdContadoCobranza: Integer;
      function GetIdOperacionBancaria: Integer;
      function BuscarNumero: Boolean;
      function BuscarCodigoCliente: Boolean;
  end;

implementation

uses
  DB,
  ClsConsecutivo;

{ TAnticipoCliente }

procedure TAnticipoCliente.Ajustar;
begin

end;

procedure TAnticipoCliente.Mover(Ds: TClientDataSet);
begin
  Numero := Ds.FieldValues['numero'];
  CodigoMoneda := Ds.FieldValues['codigo_moneda'];
  CodigoCliente := Ds.FieldValues['codigo_cliente'];
  CodigoZona := Ds.FieldValues['codigo_zona'];
  FechaIngreso := Ds.FieldByName('fecha_ingreso').AsDateTime;
  FechaRecepcion := Ds.FieldByName('fecha_recepcion').AsDateTime;
  FechaContable := Ds.FieldByName('fecha_contable').AsDateTime;
  FechaLibros :=  Ds.FieldByName('fecha_libros').AsDateTime;
  Concepto := Ds.FieldValues['concepto'];
  Observaciones := Ds.FieldValues['observaciones'];
  MontoNeto := Ds.FieldValues['monto_neto'];
  LoginUsuario := Ds.FieldValues['login_usuario'];
  CodigoFormaPago := Ds.FieldValues['codigo_forma_pago'];
  NumeroFormaPago := Ds.FieldValues['numero_forma_pago'];
  CodigoCuentaBancaria := Ds.FieldValues['codigo_cuenta_bancaria'];
  NumeroDocumentoBancario := Ds.FieldValues['numero_documento_bancario'];
  CodigoBancoCobranza := Ds.FieldValues['codigo_banco_cobranza'];
  FechaOperacionBancaria := Ds.FieldValues['fecha_operacion_bancaria'];
  TiempoIngreso := Ds.FieldByName('tiempo_ingreso').AsDateTime;
  IdAnticipoCliente := Ds.FieldValues['id_anticipo_cliente'];
  IdCobranza := Ds.FieldValues['id_cobranza'];
  IdContadoCobranza := Ds.FieldValues['id_contado_cobranza'];
  IdOperacionBancaria := Ds.FieldValues['id_operacion_bancaria'];
end;

function TAnticipoCliente.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,23);
  Params[0] := Numero;
  Params[1] := CodigoMoneda;
  Params[2] := CodigoCliente;
  Params[3] := CodigoZona;
  Params[4] := VarFromDateTime(FechaIngreso);
  Params[5] := VarFromDateTime(FechaRecepcion);
  Params[6] := VarFromDateTime(FechaContable);
  Params[7] := VarFromDateTime(FechaLibros);
  Params[8] := Concepto;
  Params[9] := Observaciones;
  Params[10] := MontoNeto;
  Params[11] := LoginUsuario;
  Params[12] := VarFromDateTime(TiempoIngreso);
  params[13] := CodigoFormaPago;
  params[14] := NumeroFormaPago;
  params[15] := CodigoCuentaBancaria;
  params[16] := NumeroDocumentoBancario;
  params[17] := CodigoBancoCobranza;
  params[18] := VarFromDateTime(FechaOperacionBancaria);
  Params[19] := IdAnticipoCliente;
  Params[20] := IdCobranza;
  Params[21] := IdContadoCobranza;
  Params[22] := IdOperacionBancaria;
  Result := Params;
end;

function TAnticipoCliente.GetCampos: TArraystring;
var
  Campos: TArraystring;
begin
  SetLength(Campos,12);
  Campos[0] := 'numero';
  Campos[1] := 'codigo_moneda';
  Campos[2] := 'codigo_cliente';
  Campos[3] := 'codigo_zona';
  Campos[4] := 'concepto';
  Campos[5] := 'observaciones';
  Campos[6] := 'login_usuario';
  Campos[7] := 'codigo_forma_pago';
  Campos[8] := 'numero_forma_pago';
  Campos[9] := 'codigo_cuenta_bancaria';
  Campos[10] := 'numero_documento_bancario';
  Campos[11] := 'codigo_banco_cobranza';
  Result := Campos;
end;

function TAnticipoCliente.GetCondicion: string;
begin
  Result:= '"id_anticipo_cliente" = ' + IntToStr(IdAnticipoCliente);
end;

procedure TAnticipoCliente.AntesInsertar;
var
  Consecutivo: TConsecutivo;
begin
  Consecutivo := TConsecutivo.Create;
  Numero := IntToStr(Consecutivo.ObtenerConsecutivo('numero_anticipo_cliente'));
  IdAnticipoCliente := Consecutivo.ObtenerConsecutivo('id_anticipo_cliente');
  IdCobranza := Consecutivo.ObtenerConsecutivo('id_cobranza');
  IdContadoCobranza := Consecutivo.ObtenerConsecutivo('id_contado_cobranza');
end;

procedure TAnticipoCliente.AntesModificar;
begin

end;

procedure TAnticipoCliente.AntesEliminar;
begin

end;

procedure TAnticipoCliente.DespuesInsertar;
begin

end;

procedure TAnticipoCliente.DespuesModificar;
begin

end;

procedure TAnticipoCliente.DespuesEliminar;
begin

end;

constructor TAnticipoCliente.Create;
var
  ClavesPrimarias: TArraystring;
begin
  inherited Create;
  SetTabla ('tbl_anticipo_clientes');
  SetStrSQLInsert ('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(23)) + ')');
  SetStrSQLUpdate ('UPDATE ' + GetTabla + ' SET "numero"=$1, "codigo_moneda"=$2, "codigo_cliente"=$3, "codigo_zona"=$4, "fecha_ingreso"=$5, ' +
                   '"fecha_recepcion"=$6, "fecha_contable"=$7, "fecha_libros"=$8, "concepto"=$9, "observaciones"=$10, "monto_neto"=$11,  ' +
                   '"login_usuario"=$12, "tiempo_ingreso"=$13, "codigo_forma_pago"=$14, "numero_forma_pago"=$15, "codigo_cuenta_bancaria"=$16, ' +
                   '"numero_documento_bancario"=$17, "codigo_banco_cobranza"=$18, "fecha_operacion_bancaria"=$19, "id_cobranza"=$21, "id_contado_cobranza"=$22, ' +
                   '"id_operacion_bancaria"=$23 ' +
                   ' WHERE ("id_anticipo_cliente"=$20)');
  SetStrSQLDelete ('DELETE FROM ' + GetTabla + ' WHERE ("id_anticipo_cliente"=$20)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"id_anticipo_cliente"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TAnticipoCliente.SetCodigoBancoCobranza(varCodigoBancoCobranza: string);
begin
  CodigoBancoCobranza := varCodigoBancoCobranza;
end;

procedure TAnticipoCliente.SetCodigoCliente(varCodigoCliente: string);
begin
  CodigoCliente := varCodigoCliente;
end;

procedure TAnticipoCliente.SetCodigoCuentaBancaria(varCodigoCuentaBancaria: string);
begin
  CodigoCuentaBancaria := varCodigoCuentaBancaria;
end;

procedure TAnticipoCliente.SetCodigoFormaPago(varCodigoFormaPago: string);
begin
  CodigoFormaPago := varCodigoFormaPago;
end;

procedure TAnticipoCliente.SetCodigoMoneda(varCodigoMoneda: string);
begin
  CodigoMoneda := varCodigoMoneda;
end;

procedure TAnticipoCliente.SetCodigoZona(varCodigoZona: string);
begin
  CodigoZona := varCodigoZona;
end;

procedure TAnticipoCliente.SetConcepto(varConcepto: string);
begin
  Concepto := varConcepto;
end;

procedure TAnticipoCliente.SetFechaContable(varFechaContable: TDate);
begin
  FechaContable := varFechaContable;
end;

procedure TAnticipoCliente.SetFechaIngreso(varFechaIngreso: TDate);
begin
  FechaIngreso := varFechaIngreso;
end;

procedure TAnticipoCliente.SetFechaLibros(varFechalibros: TDate);
begin
  FechaLibros := varFechalibros;
end;

procedure TAnticipoCliente.SetFechaOperacionBancaria(varFechaOperacionBancaria: TDate);
begin
  FechaOperacionBancaria := varFechaOperacionBancaria;
end;

procedure TAnticipoCliente.SetFechaRecepcion(varFechaRecepcion: TDate);
begin
  FechaRecepcion := varFechaRecepcion;
end;

procedure TAnticipoCliente.SetIdAnticipoCliente(varIdAnticipoCliente: Integer);
begin
  IdAnticipoCliente := varIdAnticipoCliente;
end;

procedure TAnticipoCliente.SetIdCobranza(varIdCobranza: Integer);
begin
  IdCobranza := varIdCobranza;
end;

procedure TAnticipoCliente.SetIdContadoCobranza(varIdContadoCobranza: Integer);
begin
  IdContadoCobranza := varIdContadoCobranza;
end;

procedure TAnticipoCliente.SetIdOperacionBancaria(varIdOperacionBancaria: Integer);
begin
  IdOperacionBancaria := varIdOperacionBancaria;
end;

procedure TAnticipoCliente.SetLoginUsuario(varLoginUsuario: string);
begin
  LoginUsuario := varLoginUsuario;
end;

procedure TAnticipoCliente.SetMontoNeto(varMontoNeto: Double);
begin
  MontoNeto := varMontoNeto;
end;

procedure TAnticipoCliente.SetNumero(varNumero: string);
begin
  Numero := varNumero;
end;

procedure TAnticipoCliente.SetNumeroDocumentoBancario(varNumeroDocumentoBancario: string);
begin
  NumeroDocumentoBancario := varNumeroDocumentoBancario;
end;

procedure TAnticipoCliente.SetNumeroFormaPago(varNumeroFormaPago: string);
begin
  NumeroFormaPago := varNumeroFormaPago;
end;

procedure TAnticipoCliente.SetObservaciones(varObservaciones: string);
begin
  Observaciones :=  varObservaciones;
end;

procedure TAnticipoCliente.SetTiempoIngreso(varTiempodeIngreso: TDateTime);
begin
  TiempoIngreso := varTiempodeIngreso;
end;

function TAnticipoCliente.GetCodigoBancoCobranza: string;
begin
  Result := CodigoBancoCobranza;
end;

function TAnticipoCliente.GetCodigoCliente: string;
begin
  Result := CodigoCliente;
end;

function TAnticipoCliente.GetCodigoCuentaBancaria: string;
begin
  Result := CodigoCuentaBancaria;
end;

function TAnticipoCliente.GetCodigoFormaPago: string;
begin
  Result := CodigoFormaPago;
end;

function TAnticipoCliente.GetCodigoMoneda: string;
begin
  Result := CodigoMoneda;
end;

function TAnticipoCliente.GetCodigoZona: string;
begin
  Result := CodigoZona;
end;

function TAnticipoCliente.GetConcepto: string;
begin
  Result := Concepto;
end;

function TAnticipoCliente.GetFechaContable: TDate;
begin
  Result := FechaContable;
end;

function TAnticipoCliente.GetFechaIngreso: TDate;
begin
  Result := FechaIngreso;
end;

function TAnticipoCliente.GetFechaLibros: TDate;
begin
  Result := FechaLibros;
end;

function TAnticipoCliente.GetFechaOperacionBancaria: TDate;
begin
  Result := FechaOperacionBancaria;
end;

function TAnticipoCliente.GetFechaRecepcion: TDate;
begin
  Result := FechaRecepcion;
end;

function TAnticipoCliente.GetIdAnticipoCliente: Integer;
begin
  Result := IdAnticipoCliente;
end;

function TAnticipoCliente.GetIdCobranza: Integer;
begin
  Result := IdCobranza;
end;

function TAnticipoCliente.GetIdContadoCobranza: Integer;
begin
  Result := IdContadoCobranza;
end;

function TAnticipoCliente.GetIdOperacionBancaria: Integer;
begin
  Result := IdOperacionBancaria;
end;

function TAnticipoCliente.GetLoginUsuario: string;
begin
  Result := LoginUsuario;
end;

function TAnticipoCliente.GetMontoNeto: Double;
begin
  Result := MontoNeto;
end;

function TAnticipoCliente.GetNumero: string;
begin
  Result := Numero;
end;

function TAnticipoCliente.GetNumeroDocumentoBancario: string;
begin
  Result := NumeroDocumentoBancario;
end;

function TAnticipoCliente.GetNumeroFormaPago: string;
begin
  Result := NumeroFormaPago;
end;

function TAnticipoCliente.GetObservaciones: string;
begin
  Result := Observaciones;
end;

function TAnticipoCliente.GetTiempoIngreso: TDateTime;
begin
  Result := TiempoIngreso;
end;

function TAnticipoCliente.BuscarNumero: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("numero" = ' + QuotedStr(GetNumero) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

function TAnticipoCliente.BuscarCodigoCliente: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("codigo_cliente" = ' + QuotedStr(GetCodigoCliente) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

end.
