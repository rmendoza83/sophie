unit ClsBusquedaLiquidacionCobranza;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TBusquedaLiquidacionCobranza }

  TBusquedaLiquidacionCobranza = class(TGenericoBD)
    private
      Numero: string;
      CodigoCliente: string;
      Rif: string;
      Nit: string;
      RazonSocial: string;
      Contacto: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      function GetNumero: string;
      function GetCodigoCliente: string;
      function GetRif: string;
      function GetNit: string;
      function GetRazonSocial: string;
      function GetContacto: string;
  end;

implementation

{ TBusquedaLiquidacionCobranza }

procedure TBusquedaLiquidacionCobranza.Ajustar;
begin

end;

procedure TBusquedaLiquidacionCobranza.Mover(Ds: TClientDataSet);
begin
  Numero := Ds.FieldValues['numero'];
  CodigoCliente := Ds.FieldValues['codigo_cliente'];
  Rif := Ds.FieldValues['rif'];
  Nit := Ds.FieldValues['nit'];
  RazonSocial := Ds.FieldValues['razon_social'];
  Contacto := Ds.FieldValues['contacto'];
end;

function TBusquedaLiquidacionCobranza.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,6);
  Params[0] := Numero;
  Params[1] := CodigoCliente;
  Params[2] := Rif;
  Params[3] := Nit;
  Params[4] := RazonSocial;
  Params[5] := Contacto;
  Result := Params;
end;

function TBusquedaLiquidacionCobranza.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,6);
  Campos[0] := 'numero';
  Campos[1] := 'codigo_cliente';
  Campos[2] := 'rif';
  Campos[3] := 'nit';
  Campos[4] := 'razon_social';
  Campos[5] := 'contacto';
  Result := Campos;
end;

function TBusquedaLiquidacionCobranza.GetCondicion: string;
begin
  Result:= '';
end;

procedure TBusquedaLiquidacionCobranza.AntesInsertar;
begin

end;

procedure TBusquedaLiquidacionCobranza.AntesModificar;
begin

end;

procedure TBusquedaLiquidacionCobranza.AntesEliminar;
begin

end;

procedure TBusquedaLiquidacionCobranza.DespuesInsertar;
begin

end;

procedure TBusquedaLiquidacionCobranza.DespuesModificar;
begin

end;

procedure TBusquedaLiquidacionCobranza.DespuesEliminar;
begin

end;

constructor TBusquedaLiquidacionCobranza.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('v_busqueda_liquidacion_cobranzas');
  SetLength(ClavesPrimarias,0);
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

function TBusquedaLiquidacionCobranza.GetCodigoCliente: string;
begin
  Result := CodigoCliente;
end;

function TBusquedaLiquidacionCobranza.GetContacto: string;
begin
  Result := Contacto;
end;

function TBusquedaLiquidacionCobranza.GetNit: string;
begin
  Result := Nit;
end;

function TBusquedaLiquidacionCobranza.GetNumero: string;
begin
  Result := Numero;
end;

function TBusquedaLiquidacionCobranza.GetRazonSocial: String;
begin
  Result := RazonSocial;
end;

function TBusquedaLiquidacionCobranza.GetRif: String;
begin
  Result := Rif;
end;

end.
