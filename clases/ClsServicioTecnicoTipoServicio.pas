unit ClsServicioTecnicoTipoServicio;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TServicioTecnicoTipoServicio }

  TServicioTecnicoTipoServicio = class(TGenericoBD)
    private
      Codigo: string;
      Descripcion: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      function ObtenerCombo: TClientDataSet;
      procedure SetCodigo(varCodigo: string);
      procedure SetDescripcion(varDescripcion: string);
      function GetCodigo: string;
      function GetDescripcion: string;
  end;

implementation

{ TServicioTecnicoTipoServicio }

procedure TServicioTecnicoTipoServicio.Ajustar;
begin

end;

procedure TServicioTecnicoTipoServicio.Mover(Ds: TClientDataSet);
begin
  Codigo := Ds.FieldValues['codigo'];
  Descripcion := Ds.FieldValues['descripcion'];
end;

function TServicioTecnicoTipoServicio.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,2);
  Params[0] := Codigo;
  Params[1] := Descripcion;
  Result := Params;
end;

function TServicioTecnicoTipoServicio.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,2);
  Campos[0] := 'codigo';
  Campos[1] := 'descripcion';
  Result := Campos;
end;

function TServicioTecnicoTipoServicio.GetCondicion: string;
begin
  Result := '"codigo" = ' + QuotedStr(Codigo);
end;

procedure TServicioTecnicoTipoServicio.AntesInsertar;
begin

end;

procedure TServicioTecnicoTipoServicio.AntesModificar;
begin

end;

procedure TServicioTecnicoTipoServicio.AntesEliminar;
begin

end;

procedure TServicioTecnicoTipoServicio.DespuesInsertar;
begin

end;

procedure TServicioTecnicoTipoServicio.DespuesModificar;
begin

end;

procedure TServicioTecnicoTipoServicio.DespuesEliminar;
begin

end;

constructor TServicioTecnicoTipoServicio.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_servicio_tecnico_tipos_servicio');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(2)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "descripcion"=$2 WHERE ("codigo"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("codigo"=$1)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"codigo"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

function TServicioTecnicoTipoServicio.ObtenerCombo: TClientDataSet;
begin
  Result := BD.EjecutarQueryCliDs('SELECT "codigo", "descripcion" FROM ' + GetTabla);
end;

procedure TServicioTecnicoTipoServicio.SetCodigo(varCodigo: string);
begin
  Codigo := varCodigo;
end;

procedure TServicioTecnicoTipoServicio.SetDescripcion(varDescripcion: string);
begin
  Descripcion := varDescripcion;
end;

function TServicioTecnicoTipoServicio.GetCodigo: string;
begin
  Result := Codigo;
end;

function TServicioTecnicoTipoServicio.GetDescripcion: string;
begin
  Result := Descripcion;
end;

end.
