unit ClsCondominioBusquedaRecibo;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TCondominioBusquedaRecibo }

  TCondominioBusquedaRecibo = class(TGenericoBD)
    private
      Codigo: string;
      CodigoConjuntoAdministracion: string;
      Descripcion: string;
      PeriodoA: Integer;
      PeriodoM: Integer;
      FechaIngreso: TDate;
      FechaVencimiento: TDate;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      function GetCodigo: string;
      function GetCodigoConjuntoAdministracion: string;
      function GetDescripcion: string;
      function GetPeriodoA: Integer;
      function GetPeriodoM: Integer;
      function GetFechaIngreso: TDate;
      function GetFechaVencimiento: TDate;
  end;

implementation

uses DB;

{ TCondominioBusquedaRecibo }

procedure TCondominioBusquedaRecibo.Ajustar;
begin

end;

procedure TCondominioBusquedaRecibo.Mover(Ds: TClientDataSet);
begin
  Codigo := Ds.FieldValues['codigo'];
  CodigoConjuntoAdministracion := Ds.FieldValues['codigo_conjunto_administracion'];
  Descripcion := Ds.FieldValues['descripcion'];
  PeriodoA := Ds.FieldValues['periodo_a'];
  PeriodoM := Ds.FieldValues['periodo_m'];
  FechaIngreso := Ds.FieldValues['fecha_ingreso'];
  FechaVencimiento := Ds.FieldValues['fecha_vencimiento'];
end;

function TCondominioBusquedaRecibo.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,7);
  Params[0] := Codigo;
  Params[1] := CodigoConjuntoAdministracion;
  Params[2] := Descripcion;
  Params[3] := PeriodoA;
  Params[4] := PeriodoM;
  Params[5] := FechaIngreso;
  Params[6] := FechaVencimiento;
  Result := Params;
end;

function TCondominioBusquedaRecibo.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,3);
  Campos[0] := 'codigo';
  Campos[1] := 'codigo_conjunto_administracion';
  Campos[2] := 'descripcion';
  Result := Campos;
end;

function TCondominioBusquedaRecibo.GetCondicion: string;
begin
  Result:= '';
end;

procedure TCondominioBusquedaRecibo.AntesInsertar;
begin

end;

procedure TCondominioBusquedaRecibo.AntesModificar;
begin

end;

procedure TCondominioBusquedaRecibo.AntesEliminar;
begin

end;

procedure TCondominioBusquedaRecibo.DespuesInsertar;
begin

end;

procedure TCondominioBusquedaRecibo.DespuesModificar;
begin

end;

procedure TCondominioBusquedaRecibo.DespuesEliminar;
begin

end;

constructor TCondominioBusquedaRecibo.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('v_condominio_busqueda_recibo');
  SetLength(ClavesPrimarias,0);
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

function TCondominioBusquedaRecibo.GetCodigo;
begin
  Result := Codigo;
end;

function TCondominioBusquedaRecibo.GetCodigoConjuntoAdministracion;
begin
  Result := CodigoConjuntoAdministracion;
end;

function TCondominioBusquedaRecibo.GetDescripcion;
begin
  Result := Descripcion;
end;

function TCondominioBusquedaRecibo.GetPeriodoA;
begin
  Result := PeriodoA;
end;

function TCondominioBusquedaRecibo.GetPeriodoM;
begin
  Result := PeriodoM;
end;

function TCondominioBusquedaRecibo.GetFechaIngreso;
begin
  Result := FechaIngreso;
end;

function TCondominioBusquedaRecibo.GetFechaVencimiento;
begin
  Result := FechaVencimiento;
end;

end.
