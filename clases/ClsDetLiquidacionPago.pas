unit ClsDetLiquidacionPago;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TDetLiquidacionPago }

  TDetLiquidacionPago = class(TGenericoBD)
    private
      IdLiquidacionPago: Integer;
      IdPago: Integer;
      NumeroCuota: Integer;
      OrdenPago: Integer;
      Abonado: Double;
      CodigoRetencionIva: string;
      MontoRetencionIva: Double;
      CodigoRetencion1: string;
      MontoRetencion1: Double;
      CodigoRetencion2: string;
      MontoRetencion2: Double;
      CodigoRetencion3: string;
      MontoRetencion3: Double;
      CodigoRetencion4: string;
      MontoRetencion4: Double;
      CodigoRetencion5: string;
      MontoRetencion5: Double;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetIdLiquidacionPago(varIdLiquidacionPago: Integer);
      procedure SetIdPago(varIdPago: Integer);
      procedure SetNumeroCuota(varNumeroCuota: Integer);
      procedure SetOrdenPago(varOrdenPago: Integer);
      procedure SetAbonado(varAbonado: Double);
      procedure SetCodigoRetencionIva(varCodigoRetencionIva: string);
      procedure SetMontoRetencionIva(varMontoRetencionIva: Double);
      procedure SetCodigoRetencion1(varCodigoRetencion1: string);
      procedure SetMontoRetencion1(varMontoRetencion1: Double);
      procedure SetCodigoRetencion2(varCodigoRetencion2: string);
      procedure SetMontoRetencion2(varMontoRetencion2: Double);
      procedure SetCodigoRetencion3(varCodigoRetencion3: string);
      procedure SetMontoRetencion3(varMontoRetencion3: Double);
      procedure SetCodigoRetencion4(varCodigoRetencion4: string);
      procedure SetMontoRetencion4(varMontoRetencion4: Double);
      procedure SetCodigoRetencion5(varCodigoRetencion5: string);
      procedure SetMontoRetencion5(varMontoRetencion5: Double);
      function GetIdLiquidacionPago: Integer;
      function GetIdPago: Integer;
      function GetNumeroCuota: Integer;
      function GetOrdenPago: Integer;
      function GetAbonado: Double;
      function GetCodigoRetencionIva: string;
      function GetMontoRetencionIva: Double;
      function GetCodigoRetencion1: string;
      function GetMontoRetencion1: Double;
      function GetCodigoRetencion2: string;
      function GetMontoRetencion2: Double;
      function GetCodigoRetencion3: string;
      function GetMontoRetencion3: Double;
      function GetCodigoRetencion4: string;
      function GetMontoRetencion4: Double;
      function GetCodigoRetencion5: string;
      function GetMontoRetencion5: Double;
      function ObtenerIdLiquidacionPago(varIdLiquidacionPago: Integer): TClientDataSet;
      function EliminarIdLiquidacionPago(varIdLiquidacionPago: Integer): Boolean;
      function VerificarRetencionesISLR(varIdPago: Integer): Boolean;
      function VerificarRetencionesIVA(varIdPago: Integer): Boolean;
  end;

implementation

uses DB;

{ TDetLiquidacionPago }

procedure TDetLiquidacionPago.Ajustar;
begin

end;

procedure TDetLiquidacionPago.Mover(Ds: TClientDataSet);
begin
  IdLiquidacionPago := Ds.FieldValues['id_liquidacion_pago'];
  IdPago := Ds.FieldValues['id_pago'];
  OrdenPago := Ds.FieldValues['orden_pago'];
  NumeroCuota := Ds.FieldValues['numero_cuota'];
  Abonado := Ds.FieldValues['abonado'];
  CodigoRetencionIva := Ds.FieldValues['codigo_retencion_iva'];
  MontoRetencionIva := Ds.FieldValues['monto_retencion_iva'];
  CodigoRetencion1 := Ds.FieldValues['codigo_retencion_1'];
  MontoRetencion1 := Ds.FieldValues['monto_retencion_1'];
  CodigoRetencion2 := Ds.FieldValues['codigo_retencion_2'];
  MontoRetencion2 := Ds.FieldValues['monto_retencion_2'];
  CodigoRetencion3 := Ds.FieldValues['codigo_retencion_3'];
  MontoRetencion3 := Ds.FieldValues['monto_retencion_3'];
  CodigoRetencion4 := Ds.FieldValues['codigo_retencion_4'];
  MontoRetencion4 := Ds.FieldValues['monto_retencion_4'];
  CodigoRetencion5 := Ds.FieldValues['codigo_retencion_5'];
  MontoRetencion5 := Ds.FieldValues['monto_retencion_5'];
end;

function TDetLiquidacionPago.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,17);
  Params[0] := IdLiquidacionPago;
  Params[1] := IdPago;
  Params[2] := NumeroCuota;
  Params[3] := OrdenPago;
  Params[4] := Abonado;
  Params[5] := CodigoRetencionIva;
  Params[6] := MontoRetencionIva;
  Params[7] := CodigoRetencion1;
  Params[8] := MontoRetencion1;
  Params[9] := CodigoRetencion2;
  Params[10] := MontoRetencion2;
  Params[11] := CodigoRetencion3;
  Params[12] := MontoRetencion3;
  Params[13] := CodigoRetencion4;
  Params[14] := MontoRetencion4;
  Params[15] := CodigoRetencion5;
  Params[16] := MontoRetencion5;
  Result := Params;
end;

function TDetLiquidacionPago.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,6);
  Campos[0] := 'codigo_retencion_iva';
  Campos[1] := 'codigo_retencion_1';
  Campos[2] := 'codigo_retencion_2';
  Campos[3] := 'codigo_retencion_3';
  Campos[4] := 'codigo_retencion_4';
  Campos[5] := 'codigo_retencion_5';
  Result := Campos;
end;

function TDetLiquidacionPago.GetCondicion: string;
begin
  Result := '("id_liquidacion_pago" = ' + IntToStr(IdLiquidacionPago) + ') AND ' +
            '("id_pago" = ' + IntToStr(IdPago) + ') AND ' +
						'("numero_cuota" = ' + IntToStr(NumeroCuota) + ') AND ' +
            '("orden_pago" = ' + IntToStr(OrdenPago) + ')';
end;

procedure TDetLiquidacionPago.AntesInsertar;
begin

end;

procedure TDetLiquidacionPago.AntesModificar;
begin

end;

procedure TDetLiquidacionPago.AntesEliminar;
begin

end;

procedure TDetLiquidacionPago.DespuesInsertar;
begin

end;

procedure TDetLiquidacionPago.DespuesEliminar;
begin

end;

procedure TDetLiquidacionPago.DespuesModificar;
begin

end;

constructor TDetLiquidacionPago.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_det_liquidacion_pagos');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(17)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "abonado"=$5, "codigo_retencion_iva"=$6, "monto_retencion_iva"=$7, "codigo_retencion_1"=$8, "monto_retencion_1"=$9, "codigo_retencion_2"=$10, ' +
									'"monto_retencion_2"=$11, "codigo_retencion_3"=$12, "monto_retencion_3"=$13, "codigo_retencion_4"=$14, "monto_retencion_4"=$15, "codigo_retencion_5"=$16, "monto_retencion_5"=$17 ' + 
									'WHERE ("id_liquidacion_pago"=$1) AND ("id_pago"=$2) AND ("numero_cuota"=$3) AND ("orden_pago"=$4)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("id_liquidacion_pago"=$1) AND ("id_pago"=$2) AND ("numero_cuota"=$3) AND ("orden_pago"=$4)');
  SetLength(ClavesPrimarias,4);
  ClavesPrimarias[0] := '"id_liquidacion_pago"';
	ClavesPrimarias[1] := '"id_pago"';
	ClavesPrimarias[2] := '"numero_cuota"';
	ClavesPrimarias[3] := '"orden_pago"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TDetLiquidacionPago.SetIdLiquidacionPago(varIdLiquidacionPago: Integer);
begin
  IdLiquidacionPago := varIdLiquidacionPago;
end;

procedure TDetLiquidacionPago.SetIdPago(varIdPago: Integer);
begin
  IdPago := varIdPago;
end;

procedure TDetLiquidacionPago.SetNumeroCuota(varNumeroCuota: Integer);
begin
  NumeroCuota := varNumeroCuota;
end;

procedure TDetLiquidacionPago.SetOrdenPago(varOrdenPago: Integer);
begin
  OrdenPago := varOrdenPago;
end;

procedure TDetLiquidacionPago.SetAbonado(varAbonado: Double);
begin
  Abonado := varAbonado;
end;

procedure TDetLiquidacionPago.SetCodigoRetencionIva(varCodigoRetencionIva: string);
begin
  CodigoRetencionIva := varCodigoRetencionIva;
end;

procedure TDetLiquidacionPago.SetMontoRetencionIva(varMontoRetencionIva: Double);
begin
  MontoRetencionIva := varMontoRetencionIva;
end;

procedure TDetLiquidacionPago.SetCodigoRetencion1(varCodigoRetencion1: string);
begin
  CodigoRetencion1 := varCodigoRetencion1;
end;

procedure TDetLiquidacionPago.SetMontoRetencion1(varMontoRetencion1: Double);
begin
  MontoRetencion1 := varMontoRetencion1;
end;

procedure TDetLiquidacionPago.SetCodigoRetencion2(varCodigoRetencion2: string);
begin
  CodigoRetencion2 := varCodigoRetencion2;
end;

procedure TDetLiquidacionPago.SetMontoRetencion2(varMontoRetencion2: Double);
begin
  MontoRetencion2 := varMontoRetencion2;
end;

procedure TDetLiquidacionPago.SetCodigoRetencion3(varCodigoRetencion3: string);
begin
  CodigoRetencion3 := varCodigoRetencion3;
end;

procedure TDetLiquidacionPago.SetMontoRetencion3(varMontoRetencion3: Double);
begin
  MontoRetencion3 := varMontoRetencion3;
end;

procedure TDetLiquidacionPago.SetCodigoRetencion4(varCodigoRetencion4: string);
begin
  CodigoRetencion4 := varCodigoRetencion4;
end;

procedure TDetLiquidacionPago.SetMontoRetencion4(varMontoRetencion4: Double);
begin
  MontoRetencion4 := varMontoRetencion4;
end;

procedure TDetLiquidacionPago.SetCodigoRetencion5(varCodigoRetencion5: string);
begin
  CodigoRetencion5 := varCodigoRetencion5;
end;

procedure TDetLiquidacionPago.SetMontoRetencion5(varMontoRetencion5: Double);
begin
  MontoRetencion5 := varMontoRetencion5;
end;

function TDetLiquidacionPago.GetIdLiquidacionPago: Integer;
begin
  Result := IdLiquidacionPago;
end;

function TDetLiquidacionPago.GetIdPago: Integer;
begin
  Result := IdPago;
end;

function TDetLiquidacionPago.GetNumeroCuota: Integer;
begin
  Result := NumeroCuota;
end;

function TDetLiquidacionPago.GetOrdenPago: Integer;
begin
  Result := OrdenPago;
end;

function TDetLiquidacionPago.GetAbonado: Double;
begin
  Result := Abonado;
end;

function TDetLiquidacionPago.GetCodigoRetencionIva: string;
begin
  Result := CodigoRetencionIva;
end;

function TDetLiquidacionPago.GetMontoRetencionIva: Double;
begin
  Result := MontoRetencionIva;
end;

function TDetLiquidacionPago.GetCodigoRetencion1: string;
begin
  Result := CodigoRetencion1;
end;

function TDetLiquidacionPago.GetMontoRetencion1: Double;
begin
  Result := MontoRetencion1;
end;

function TDetLiquidacionPago.GetCodigoRetencion2: string;
begin
  Result := CodigoRetencion2;
end;

function TDetLiquidacionPago.GetMontoRetencion2: Double;
begin
  Result := MontoRetencion2;
end;

function TDetLiquidacionPago.GetCodigoRetencion3: string;
begin
  Result := CodigoRetencion3;
end;

function TDetLiquidacionPago.GetMontoRetencion3: Double;
begin
  Result := MontoRetencion3;
end;

function TDetLiquidacionPago.GetCodigoRetencion4: string;
begin
  Result := CodigoRetencion4;
end;

function TDetLiquidacionPago.GetMontoRetencion4: Double;
begin
  Result := MontoRetencion4;
end;

function TDetLiquidacionPago.GetCodigoRetencion5: string;
begin
  Result := CodigoRetencion5;
end;

function TDetLiquidacionPago.GetMontoRetencion5: Double;
begin
  Result := MontoRetencion5;
end;

function TDetLiquidacionPago.ObtenerIdLiquidacionPago(varIdLiquidacionPago: Integer): TClientDataSet;
begin
  Result := ObtenerListaCondicionSQL(' WHERE "id_liquidacion_pago" = ' + IntToStr(varIdLiquidacionPago));
end;

function TDetLiquidacionPago.EliminarIdLiquidacionPago(varIdLiquidacionPago: Integer): Boolean;
begin
  Result := EjecutarSQL('DELETE FROM ' + GetTabla + ' WHERE "id_liquidacion_pago" = ' + IntToStr(varIdLiquidacionPago));
end;

function TDetLiquidacionPago.VerificarRetencionesISLR(
  varIdPago: Integer): Boolean;
var
  Ds: TClientDataSet;
  sw: Boolean;
  Total: Double;
begin
  Ds := ObtenerListaCondicionSQL('WHERE (id_pago = ' + IntToStr(varIdPago) + ')');
  sw := False;
  if (Ds.RecordCount > 0) then
  begin
    Ds.First;
    while (not Ds.Eof) do
    begin
      Total := Ds.FieldValues['monto_retencion_1'] +
               Ds.FieldValues['monto_retencion_2'] +
               Ds.FieldValues['monto_retencion_3'] +
               Ds.FieldValues['monto_retencion_4'] +
               Ds.FieldValues['monto_retencion_5'];
      if (Total > 0) then
      begin
        sw := True;
        Break;
      end;
      Ds.Next;
    end;
  end;
  Result := sw;
end;

function TDetLiquidacionPago.VerificarRetencionesIVA(
  varIdPago: Integer): Boolean;
var
  Ds: TClientDataSet;
  sw: Boolean;
  Total: Double;
begin
  Ds := ObtenerListaCondicionSQL('WHERE (id_pago = ' + IntToStr(varIdPago) + ')');
  sw := False;
  if (Ds.RecordCount > 0) then
  begin
    Ds.First;
    while (not Ds.Eof) do
    begin
      Total := Ds.FieldValues['monto_retencion_iva'];
      if (Total > 0) then
      begin
        sw := True;
        Break;
      end;
      Ds.Next;
    end;
  end;
  Result := sw;
end;

end.
