unit ClsDetMovimientoInventario;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TDetMovimientoInventario }

  TDetMovimientoInventario = class(TGenericoBD)
    private
      Desde: string;
      CodigoProducto: string;
      Fecha: TDate;
      Cantidad: Double;
      EstacionSalida: string;
      EstacionEntrada: string;
      CodigoZona: string;
      Precio: Double;
      PDescuento: Double;
      MontoDescuento: Double;
      Impuesto: Double;
      PIva: Double;
      Total:Double;
      Costo: Double;
      UsCosto: Double;
      CostoFob: Double;
      CodigoCliente: string;
      CodigoProveedor: string;
      CodigoVendedor: string;
      NumeroDocumento: string;
      NumeroPedido: string;
      NumeroFacturaAsociada: string;
      IdMovimientoInventario: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArraystring; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetDesde(varDesde: string);
      procedure SetNumeroDocumento(varNumeroDocumento: string);
      procedure SetCodigoProducto(varCodigoProducto: string);
      procedure SetCantidad(varCantidad: Double);
      procedure SetEstacionSalida(varEstacionSalida: string);
      procedure SetEstacionEntrada(varEstacionEntrada: string);
      procedure SetCodigoZona(varCodigoZona: string);
      procedure SetPrecio(varPrecio: Double);
      procedure SetPDescuento(varPDescuento: Double);
      procedure SetMontoDescuento(varMontoDecuento: Double);
      procedure SetImpuesto(varImpuesto: Double);
      procedure SetPIva(varPIva: Double);
      procedure SetTotal(varTotal:Double);
      procedure SetCosto(varCosto: Double);
      procedure SetUsCosto(varUsCosto: Double);
      procedure SetCostoFob(varCostoFob: Double);
      procedure SetFecha(varFecha: TDate);
      procedure SetCodigoCliente(varCodigoCliente: string);
      procedure SetCodigoProveedor(varCodigoProveedor: string);
      procedure SetIdMovimientoInventario(varIdMovimientoInventario: Integer);
      function GetDesde: string;
      function GetNumeroDocumento: string;
      function GetCodigoProducto: string;
      function GetCantidad: Double;
      function GetEstacionSalida: string;
      function GetEstacionEntrada: string;
      function GetCodigoZona: string;
      function GetPrecio: Double;
      function GetPDescuento: Double;
      function GetMontoDescuento: Double;
      function GetImpuesto: Double;
      function GetPIva: Double;
      function GetTotal:Double;
      function GetCosto: Double;
      function GetUsCosto: Double;
      function GetCostoFob: Double;
      function GetFecha: TDate;
      function GetCodigoCliente: string;
      function GetCodigoProveedor: string;
      function GetIdMovimientoInventario: Integer;
      function ObtenerIdMovimientoInventario(varIdMovimientoInventario: Integer): TClientDataSet;
      function EliminarIdMovimientoInventario(varIdMovimientoInventario: Integer): Boolean;
  end;

implementation

uses DB;

{ TDetMovimientoInventario }

procedure TDetMovimientoInventario.Ajustar;
begin

end;

procedure TDetMovimientoInventario.Mover(Ds: TClientDataSet);
begin
  Desde := Ds.FieldValues['desde'];
  CodigoProducto := Ds.FieldValues['codigo_producto'];
  Fecha := Ds.FieldByName('fecha').AsDateTime;
  Cantidad := Ds.FieldValues['cantidad'];
  EstacionSalida := Ds.FieldValues['estacion_salida'];
  EstacionEntrada := Ds.FieldValues['estacion_entrada'];
  CodigoZona := Ds.FieldValues['codigo_zona'];
  Precio := Ds.FieldValues['precio'];
  PDescuento := Ds.FieldValues['p_descuento'];
  MontoDescuento := Ds.FieldValues['monto_descuento'];
  Impuesto := Ds.FieldValues['impuesto'];
  PIva := Ds.FieldValues['p_iva'];
  Total := Ds.FieldValues['total'];
  Costo := Ds.FieldValues['costo'];
  UsCosto := Ds.FieldValues['us_costo'];
  CostoFob := Ds.FieldValues['costo_fob'];
  CodigoCliente := Ds.FieldValues['codigo_cliente'];
  CodigoProveedor := Ds.FieldValues['codigo_proveedor'];
  CodigoVendedor := Ds.FieldValues['codigo_vendedor'];
  NumeroDocumento := Ds.FieldValues['numero_documento'];
  NumeroPedido := Ds.FieldValues['numero_pedido'];
  NumeroFacturaAsociada := Ds.FieldValues['numero_factura_asociada'];
  IdMovimientoInventario := Ds.FieldValues['id_movimiento_inventario'];
end;

function TDetMovimientoInventario.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,20);
  Params[0] := Desde;
  Params[1] := NumeroDocumento;
  Params[2] := CodigoProducto;
  Params[3] := Cantidad;
  Params[4] := EstacionSalida;
  Params[5] := EstacionEntrada;
  Params[6] := CodigoZona;
  Params[7] := Precio;
  Params[8] := PDescuento;
  Params[9] := MontoDescuento;
  Params[10] := Impuesto;
  Params[11] := PIva;
  Params[12] := Total;
  Params[13] := Costo;
  Params[14] := UsCosto;
  Params[15] := CostoFob;
  Params[16] := VarFromDateTime(Fecha);
  Params[17] := CodigoCliente;
  Params[18] := CodigoProveedor;
  Params[19] := IdMovimientoInventario;
  Result := Params;
end;

function TDetMovimientoInventario.GetCampos: TArraystring;
var
  Campos: TArraystring;
begin
  SetLength(Campos,8);
  Campos[0] := 'desde';
  Campos[1] := 'numero_documento';
  Campos[2] := 'codigo_producto';
  Campos[3] := 'estacion_salida';
  Campos[4] := 'estacion_entrada';
  Campos[5] := 'codigo_zona';
  Campos[6] := 'codigo_cliente';
  Campos[7] := 'codigo_proveedor';
  Result := Campos;
end;

function TDetMovimientoInventario.GetCondicion: string;
begin
  Result:= '"codigo_zona" = ' + QuotedStr(CodigoZona) + ' AND "id_movimiento_inventario" = ' + IntToStr(IdMovimientoInventario) + ' AND "codigo_producto" = ' + QuotedStr(CodigoProducto);
end;

procedure TDetMovimientoInventario.AntesInsertar;
begin

end;

procedure TDetMovimientoInventario.AntesModificar;
begin

end;

procedure TDetMovimientoInventario.AntesEliminar;
begin

end;

procedure TDetMovimientoInventario.DespuesInsertar;
begin

end;

procedure TDetMovimientoInventario.DespuesModificar;
begin

end;

procedure TDetMovimientoInventario.DespuesEliminar;
begin

end;

constructor TDetMovimientoInventario.Create;
var
  ClavesPrimarias: TArraystring;
begin
  inherited Create;
  SetTabla('tbl_det_movimientos_inventario');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(20)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "desde"=$1,"numero_documento"=$2,"cantidad"=$4,"estacion_salida"=$5,"estacion_entrada"=$6,"precio"=$8,"p_descuento"=$9, ' +
                   '"monto_descuento"=$10,"impuesto"=$11,"p_iva"=$12,"total"=$13,"costo"=$14,"us_costo"=$15,"costo_fob"=$16,"fecha"=$17,"codigo_cliente"=$18,"codigo_proveedor"=$19 ' +
                   ' WHERE ("codigo_zona"=$7) AND ("id_movimiento_inventario"=$20) AND ("codigo_producto"=$3)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("codigo_zona"=$7) AND ("id_movimiento_inventario"=$20) AND ("codigo_producto"=$3)');
  SetLength(ClavesPrimarias,3);
  ClavesPrimarias[0] := '"codigo_zona"';
  ClavesPrimarias[1] := '"id_movimiento_inventario"';
  ClavesPrimarias[2] := '"codigo_producto"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TDetMovimientoInventario.SetCosto(varCosto: Double);
begin
  Costo := varCosto
end;

procedure TDetMovimientoInventario.SetTotal(varTotal: Double);
begin
  Total := varTotal;
end;

procedure TDetMovimientoInventario.SetCantidad(varCantidad: Double);
begin
  Cantidad := varCantidad;
end;

procedure TDetMovimientoInventario.SetCodigoCliente(varCodigoCliente: string);
begin
  CodigoCliente := varCodigoCliente;
end;

procedure TDetMovimientoInventario.SetCodigoProducto(varCodigoProducto: string);
begin
  CodigoProducto := varCodigoProducto;
end;

procedure TDetMovimientoInventario.SetCodigoProveedor(varCodigoProveedor: string);
begin
  CodigoProveedor := varCodigoProveedor;
end;

procedure TDetMovimientoInventario.SetCodigoZona(varCodigoZona: string);
begin
  CodigoZona := varCodigoZona;
end;

procedure TDetMovimientoInventario.SetCostoFob(varCostoFob: Double);
begin
  CostoFob := varCostoFob;
end;

procedure TDetMovimientoInventario.SetDesde(varDesde: string);
begin
  Desde := varDesde;
end;

procedure TDetMovimientoInventario.SetEstacionEntrada(varEstacionEntrada: string);
begin
  EstacionEntrada := varEstacionEntrada;
end;

procedure TDetMovimientoInventario.SetEstacionSalida(varEstacionSalida: string);
begin
  EstacionSalida := varEstacionSalida;
end;

procedure TDetMovimientoInventario.SetFecha(varFecha: TDate);
begin
  Fecha := varFecha;
end;

procedure TDetMovimientoInventario.SetIdMovimientoInventario(varIdMovimientoInventario: Integer);
begin
  IdMovimientoInventario := varIdMovimientoInventario;
end;

procedure TDetMovimientoInventario.SetImpuesto(varImpuesto: Double);
begin
  Impuesto := varImpuesto;
end;

procedure TDetMovimientoInventario.SetMontoDescuento(varMontoDecuento: Double);
begin
  MontoDescuento := varMontoDecuento;
end;

procedure TDetMovimientoInventario.SetNumeroDocumento(varNumeroDocumento: string);
begin
  NumeroDocumento := varNumeroDocumento;
end;

procedure TDetMovimientoInventario.SetPDescuento(varPDescuento: Double);
begin
  PDescuento := varPDescuento;
end;

procedure TDetMovimientoInventario.SetPIva(varPIva: Double);
begin
  PIva := varPIva;
end;

procedure TDetMovimientoInventario.SetPrecio(varPrecio: Double);
begin
  Precio := varPrecio;
end;

procedure TDetMovimientoInventario.SetUsCosto(varUsCosto: Double);
begin
  UsCosto := varUsCosto;
end;

function TDetMovimientoInventario.GetCantidad: Double;
begin
  Result := Cantidad;
end;

function TDetMovimientoInventario.GetCodigoCliente: string;
begin
  Result := CodigoCliente;
end;

function TDetMovimientoInventario.GetCodigoProducto: string;
begin
  Result := CodigoProducto;
end;

function TDetMovimientoInventario.GetCodigoProveedor: string;
begin
  Result := CodigoProveedor;
end;

function TDetMovimientoInventario.GetCodigoZona: string;
begin
  Result := CodigoZona;
end;

function TDetMovimientoInventario.GetCosto: Double;
begin
  Result := Costo;
end;

function TDetMovimientoInventario.GetCostoFob: Double;
begin
  Result := CostoFob;
end;

function TDetMovimientoInventario.GetDesde: string;
begin
  Result := Desde;
end;

function TDetMovimientoInventario.GetEstacionEntrada: string;
begin
  Result := EstacionEntrada;
end;

function TDetMovimientoInventario.GetEstacionSalida: string;
begin
  Result := EstacionSalida;
end;

function TDetMovimientoInventario.GetFecha: TDate;
begin
  Result := Fecha;
end;

function TDetMovimientoInventario.GetIdMovimientoInventario: Integer;
begin
  Result := IdMovimientoInventario;
end;

function TDetMovimientoInventario.GetImpuesto: Double;
begin
  Result := Impuesto;
end;

function TDetMovimientoInventario.GetMontoDescuento: Double;
begin
  Result := MontoDescuento;
end;

function TDetMovimientoInventario.GetNumeroDocumento: string;
begin
  Result := NumeroDocumento;
end;

function TDetMovimientoInventario.GetPDescuento: Double;
begin
  Result := PDescuento;
end;

function TDetMovimientoInventario.GetPIva: Double;
begin
  Result := PIva;
end;

function TDetMovimientoInventario.GetPrecio: Double;
begin
  Result := Precio;
end;

function TDetMovimientoInventario.GetTotal: Double;
begin
  Result := Total;
end;

function TDetMovimientoInventario.GetUsCosto: Double;
begin
  Result := UsCosto;
end;

function TDetMovimientoInventario.ObtenerIdMovimientoInventario(
  varIdMovimientoInventario: Integer): TClientDataSet;
begin
  Result := ObtenerListaCondicionSQL(' WHERE "id_movimiento_inventario" = ' + IntToStr(varIdMovimientoInventario));
end;

function TDetMovimientoInventario.EliminarIdMovimientoInventario(
  varIdMovimientoInventario: Integer): Boolean;
begin
  Result := EjecutarSQL('DELETE FROM ' + GetTabla + ' WHERE "id_movimiento_inventario" = ' + IntToStr(varIdMovimientoInventario));
end;

end.
