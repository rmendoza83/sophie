unit ClsContableCuentaSaldo;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TContableCuentaSaldo }

  TContableCuentaSaldo = class(TGenericoBD)
    private
      Anno: Integer;
      Mes: Integer;
      CodigoCuenta: string;
      SaldoDebe: Double;
      SaldoHaber: Double;
      Saldo: Double;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetAnno(varAnno: Integer);
      procedure SetMes(varMes: Integer);
      procedure SetCodigoCuenta(varCodigoCuenta: string);
      procedure SetSaldoDebe(varSaldoDebe: Double);
      procedure SetSaldoHaber(varSaldoHaber: Double);
      procedure SetSaldo(varSaldo: Double);
      function GetAnno: Integer;
      function GetMes: Integer;
      function GetCodigoCuenta: string;
      function GetSaldoDebe: Double;
      function GetSaldoHaber: Double;
      function GetSaldo: Double;
  end;

var
  ContableCuentaSaldo: TContableCuentaSaldo;

implementation

uses DB;

{ TContableCuentaSaldo }

procedure TContableCuentaSaldo.Ajustar;
begin

end;

procedure TContableCuentaSaldo.Mover(Ds: TClientDataSet);
begin
  Anno := Ds.FieldValues['anno'];
  Mes := Ds.FieldValues['mes'];
  CodigoCuenta := Ds.FieldValues['codigo_cuenta'];
  SaldoDebe := Ds.FieldValues['saldo_debe'];
  SaldoHaber := Ds.FieldValues['saldo_haber'];
  Saldo := Ds.FieldValues['saldo'];
end;

function TContableCuentaSaldo.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,6);
  Params[0] := Anno;
  Params[1] := Mes;
  Params[2] := CodigoCuenta;
  Params[3] := SaldoDebe;
  Params[4] := SaldoHaber;
  Params[5] := Saldo;
  Result := Params;
end;

function TContableCuentaSaldo.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,1);
  Campos[0] := 'codigo_cuenta';
  Result := Campos;
end;

function TContableCuentaSaldo.GetCondicion: string;
begin
  Result := '"anno" = ' + IntToStr(Anno) + ' AND ' +
            '"mes" = ' + IntToStr(Mes) + ' AND ' +
            '"codigo_cuenta" = ' + QuotedStr(CodigoCuenta);
end;

procedure TContableCuentaSaldo.AntesEliminar;
begin

end;

procedure TContableCuentaSaldo.AntesInsertar;
begin

end;

procedure TContableCuentaSaldo.AntesModificar;
begin

end;

procedure TContableCuentaSaldo.DespuesEliminar;
begin

end;

procedure TContableCuentaSaldo.DespuesInsertar;
begin

end;

procedure TContableCuentaSaldo.DespuesModificar;
begin

end;

constructor TContableCuentaSaldo.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_contable_cuenta_saldos');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(6)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "saldo_debe"=$4, "saldo_haber"=$5, "saldo"=$6 WHERE ("anno"=$1) AND ("mes"=$2) AND ("codigo_cuenta=$3)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("anno"=$1) AND ("mes"=$2) AND ("codigo_cuenta=$3)');
  SetLength(ClavesPrimarias,3);
  ClavesPrimarias[0] := '"anno"';
  ClavesPrimarias[1] := '"mes"';
  ClavesPrimarias[2] := '"codigo_cuenta"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TContableCuentaSaldo.SetAnno(varAnno: Integer);
begin
  Anno := varAnno;
end;

procedure TContableCuentaSaldo.SetMes(varMes: Integer);
begin
  Mes := varMes;
end;

procedure TContableCuentaSaldo.SetCodigoCuenta(varCodigoCuenta: string);
begin
  CodigoCuenta := varCodigoCuenta;
end;

procedure TContableCuentaSaldo.SetSaldoDebe(varSaldoDebe: Double);
begin
  SaldoDebe := varSaldoDebe;
end;

procedure TContableCuentaSaldo.SetSaldoHaber(varSaldoHaber: Double);
begin
  SaldoHaber := varSaldoHaber;
end;

procedure TContableCuentaSaldo.SetSaldo(varSaldo: Double);
begin
  Saldo := varSaldo;
end;

function TContableCuentaSaldo.GetAnno: Integer;
begin
  Result := Anno;
end;

function TContableCuentaSaldo.GetMes: Integer;
begin
  Result := Mes;
end;

function TContableCuentaSaldo.GetCodigoCuenta: string;
begin
  Result := CodigoCuenta;
end;

function TContableCuentaSaldo.GetSaldoDebe: Double;
begin
  Result := SaldoDebe;
end;

function TContableCuentaSaldo.GetSaldoHaber: Double;
begin
  Result := SaldoHaber;
end;

function TContableCuentaSaldo.GetSaldo: Double;
begin
  Result := Saldo;
end;

end. 

