unit ClsCondominioReciboGastoOrdinario;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TCondominioReciboGastoOrdinario }

  TCondominioReciboGastoOrdinario = class(TGenericoBD)
    private
      CodigoConjuntoAdministracion: string;
      PeriodoA: Integer;
      PeriodoM: Integer;
      CodigoGasto: string;
      Monto: Double;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigoConjuntoAdministracion(varCodigoConjuntoAdministracion: string);
      procedure SetPeriodoA(varPeriodoA: Integer);
      procedure SetPeriodoM(varPeriodoM: Integer);
      procedure SetCodigoGasto(varCodigoGasto: string);
      procedure SetMonto(varMonto: Double);
      function GetCodigoConjuntoAdministracion: string;
      function GetPeriodoA: Integer;
      function GetPeriodoM: Integer;
      function GetCodigoGasto: string;
      function GetMonto: Double;
      function ObtenerGastosOrdinarios(varCodigoConjuntoAdministracion: string; varPeriodoA: Integer; varPeriodoM: Integer): TClientDataSet;
      function EliminarGastosOrdinarios(varCodigoConjuntoAdministracion: string; varPeriodoA: Integer; varPeriodoM: Integer): Boolean;
  end;

var
  Clase: TCondominioReciboGastoOrdinario;

implementation

{ TCondominioReciboGastoOrdinario }

procedure TCondominioReciboGastoOrdinario.Ajustar;
begin

end;

procedure TCondominioReciboGastoOrdinario.Mover(Ds: TClientDataSet);
begin
  CodigoConjuntoAdministracion := Ds.FieldValues['codigo_conjunto_administracion'];
  PeriodoA := Ds.FieldValues['periodo_a'];
  PeriodoM := Ds.FieldValues['periodo_m'];
  CodigoGasto := Ds.FieldValues['codigo_gasto'];
  Monto := Ds.FieldValues['monto'];
end;

function TCondominioReciboGastoOrdinario.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,5);
  Params[0] := CodigoConjuntoAdministracion;
  Params[1] := PeriodoA;
  Params[2] := PeriodoM;
  Params[3] := CodigoGasto;
  Params[4] := Monto;
  Result := Params;
end;

function TCondominioReciboGastoOrdinario.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,5);
  Campos[0] := 'codigo_conjunto_administracion';
  Campos[1] := 'periodo_a';
  Campos[2] := 'periodo_m';
  Campos[3] := 'codigo_inmueble';
  Campos[4] := 'codigo_gasto';
  Result := Campos;
end;

function TCondominioReciboGastoOrdinario.GetCondicion: string;
begin
  Result := '"codigo_conjunto_administracion" = ' + QuotedStr(CodigoConjuntoAdministracion) + ' AND ' +
            '"periodo_a" = ' + IntToStr(PeriodoA) + ' AND ' +
            '"periodo_m" = ' + IntToStr(PeriodoM) + ' AND ' +
            '"codigo_gasto" = ' + QuotedStr(CodigoGasto);
end;

procedure TCondominioReciboGastoOrdinario.AntesEliminar;
begin

end;

procedure TCondominioReciboGastoOrdinario.AntesInsertar;
begin

end;

procedure TCondominioReciboGastoOrdinario.AntesModificar;
begin

end;

procedure TCondominioReciboGastoOrdinario.DespuesEliminar;
begin

end;

procedure TCondominioReciboGastoOrdinario.DespuesInsertar;
begin

end;

procedure TCondominioReciboGastoOrdinario.DespuesModificar;
begin

end;

constructor TCondominioReciboGastoOrdinario.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_condominio_recibos_inmuebles_gastos');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(6)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "monto"=$6 ' +
                  'WHERE ("codigo_conjunto_administracion"=$1 AND "periodo_a"=$2 AND "periodo_m"=$3 AND "codigo_inmueble"=$4 AND "codigo_gasto"=$5)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("codigo_conjunto_administracion"=$1 AND "periodo_a"=$2 AND "periodo_m"=$3 AND "codigo_inmueble"=$4 AND "codigo_gasto"=$5)');
  SetLength(ClavesPrimarias,5);
  ClavesPrimarias[0] := '"codigo_conjunto_administracion"';
  ClavesPrimarias[1] := '"periodo_a"';
  ClavesPrimarias[2] := '"periodo_m"';
  ClavesPrimarias[3] := '"codigo_inmueble"';
  ClavesPrimarias[4] := '"codigo_gasto"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TCondominioReciboGastoOrdinario.SetCodigoConjuntoAdministracion(varCodigoConjuntoAdministracion: string);
begin
  CodigoConjuntoAdministracion := varCodigoConjuntoAdministracion;
end;

procedure TCondominioReciboGastoOrdinario.SetPeriodoA(varPeriodoA: Integer);
begin
  PeriodoA := varPeriodoA;
end;

procedure TCondominioReciboGastoOrdinario.SetPeriodoM(varPeriodoM: Integer);
begin
  PeriodoM := varPeriodoM;
end;

procedure TCondominioReciboGastoOrdinario.SetCodigoGasto(varCodigoGasto: string);
begin
  CodigoGasto := varCodigoGasto;
end;

procedure TCondominioReciboGastoOrdinario.SetMonto(varMonto: Double);
begin
  Monto := varMonto;
end;

function TCondominioReciboGastoOrdinario.GetCodigoConjuntoAdministracion: string;
begin
  Result := CodigoConjuntoAdministracion;
end;

function TCondominioReciboGastoOrdinario.GetPeriodoA: Integer;
begin
  Result := PeriodoA;
end;

function TCondominioReciboGastoOrdinario.GetPeriodoM: Integer;
begin
  Result := PeriodoM;
end;

function TCondominioReciboGastoOrdinario.GetCodigoGasto: string;
begin
  Result := CodigoGasto;
end;

function TCondominioReciboGastoOrdinario.GetMonto: Double;
begin
  Result := Monto;
end;

function TCondominioReciboGastoOrdinario.ObtenerGastosOrdinarios(
  varCodigoConjuntoAdministracion: string; varPeriodoA,
  varPeriodoM: Integer): TClientDataSet;
begin
  Result := ObtenerListaCondicionSQL(' WHERE ("codigo_conjunto_administracion" = ' + QuotedStr(varCodigoConjuntoAdministracion) + ') AND ("periodo_a" = ' + IntToStr(varPeriodoA) + ') AND ("periodo_m" = ' + IntToStr(varPeriodoM) + ')');
end;

function TCondominioReciboGastoOrdinario.EliminarGastosOrdinarios(
  varCodigoConjuntoAdministracion: string; varPeriodoA,
  varPeriodoM: Integer): Boolean;
begin
  Result := EjecutarSQL('DELETE FROM ' + GetTabla + ' WHERE ("codigo_conjunto_administracion" = ' + QuotedStr(varCodigoConjuntoAdministracion) + ') AND ("periodo_a" = ' + IntToStr(varPeriodoA) + ') AND ("periodo_m" = ' + IntToStr(varPeriodoM) + ')');
end;

end.
