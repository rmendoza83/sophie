unit ClsVendedor;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TVendedor }

  TVendedor = class(TGenericoBD)
    private
      Codigo: string;
      Descripcion: string;
      CodigoZona: string;
      Activo: Boolean;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigo(varCodigo: string);
      procedure SetDescripcion(varDescripcion: string);
      procedure SetCodigoZona(varCodigoZona: string);
      procedure SetActivo(varActivo: Boolean);
      function GetCodigo: string;
      function GetDescripcion: string;
      function GetCodigoZona: string;
      function GetActivo: boolean;
      function ObtenerCombo: TClientDataSet;
  end;

implementation

{ TVendedor }

procedure TVendedor.Ajustar;
begin

end;

procedure TVendedor.Mover(Ds: TClientDataSet);
begin
  Codigo := Ds.FieldValues['codigo'];
  Descripcion := Ds.FieldValues['descripcion'];
  CodigoZona := Ds.FieldValues['codigo_zona'];
  Activo := Ds.FieldValues['activo'];
end;

function TVendedor.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,4);
  Params[0] := Codigo;
  Params[1] := Descripcion;
  Params[2] := CodigoZona;
  Params[3] := Activo;
  Result := Params;
end;

function TVendedor.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,3);
  Campos[0] := 'codigo';
  Campos[1] := 'descripcion';
  Campos[2] := 'codigo_zona';
  Result := Campos;
end;

function TVendedor.GetCondicion: string;
begin
  Result := '"codigo" = ' + QuotedStr(Codigo);
end;

procedure TVendedor.AntesInsertar;
begin

end;

procedure TVendedor.AntesModificar;
begin

end;

procedure TVendedor.AntesEliminar;
begin

end;

procedure TVendedor.DespuesInsertar;
begin

end;

procedure TVendedor.DespuesModificar;
begin

end;

procedure TVendedor.DespuesEliminar;
begin

end;

constructor TVendedor.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_vendedores');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(4)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "descripcion"=$2, "codigo_zona"=$3,"activo"=$4 WHERE ("codigo"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("codigo"=$1)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"codigo"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TVendedor.SetCodigo(varCodigo: string);
begin
  Codigo := varCodigo;
end;

procedure TVendedor.SetCodigoZona(varCodigoZona: string);
begin
  CodigoZona := varCodigoZona;
end;

procedure TVendedor.SetDescripcion(varDescripcion: string);
begin
  Descripcion := varDescripcion;
end;

procedure TVendedor.SetActivo(varActivo: Boolean);
begin
  Activo:= varActivo;
end;

function TVendedor.GetActivo: Boolean;
begin
  Result := Activo;
end;

function TVendedor.GetDescripcion: string;
begin
  Result := Descripcion;
end;

function TVendedor.GetCodigo: string;
begin
  Result := Codigo;
end;

function TVendedor.GetCodigoZona: string;
begin
  Result := CodigoZona;
end;

function TVendedor.ObtenerCombo: TClientDataSet;
begin
  Result := BD.EjecutarQueryCliDs('SELECT "codigo", "descripcion", "codigo_zona" FROM ' + GetTabla);
end;

end.

