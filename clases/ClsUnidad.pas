unit ClsUnidad;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TUnidad }

  TUnidad = class(TGenericoBD)
    private
      Codigo: string;
      Descripcion: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigo(varCodigo: string);
      procedure SetDescripcion(varDescripcion: string);
      function GetCodigo: string;
      function GetDescripcion: string;
  end;

implementation

{ TUnidad }

procedure TUnidad.Ajustar;
begin

end;

procedure TUnidad.Mover(Ds: TClientDataSet);
begin
  Codigo := Ds.FieldValues['codigo'];
  Descripcion := Ds.FieldValues['descripcion'];
end;

function TUnidad.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,2);
  Params[0] := Codigo;
  Params[1] := Descripcion;
  Result := Params;
end;

function TUnidad.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,2);
  Campos[0] := 'codigo';
  Campos[1] := 'descripcion';
  Result := Campos;
end;

function TUnidad.GetCondicion: string;
begin
  Result := '"codigo" = ' + QuotedStr(Codigo);
end;

procedure TUnidad.AntesInsertar;
begin

end;

procedure TUnidad.AntesModificar;
begin

end;

procedure TUnidad.AntesEliminar;
begin

end;

procedure TUnidad.DespuesInsertar;
begin

end;

procedure TUnidad.DespuesModificar;
begin

end;

procedure TUnidad.DespuesEliminar;
begin

end;

constructor TUnidad.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_unidades');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(2)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "descripcion"=$2 WHERE ("codigo"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("codigo"=$1)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"codigo"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TUnidad.SetCodigo(varCodigo: string);
begin
  Codigo := varCodigo;
end;

procedure TUnidad.SetDescripcion(varDescripcion: string);
begin
  Descripcion := varDescripcion;
end;

function TUnidad.GetCodigo: string;
begin
  Result := Codigo;
end;

function TUnidad.GetDescripcion: string;
begin
  Result := Descripcion;
end;

end.
