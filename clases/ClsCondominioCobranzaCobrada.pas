unit ClsCondominioCobranzaCobrada;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TCondominioCobranzaCobrada }

  TCondominioCobranzaCobrada = class(TGenericoBD)
    private
      CodigoConjuntoAdministracion: string;
      PeriodoA: Integer;
      PeriodoM: Integer;
      CodigoInmueble: string;
      OrdenCobranza: Integer;
      FechaIngreso: TDate;
      FechaVencimiento: TDate;
      FechaCobranza: TDate;
      MontoNeto: Double;
      PReserva: Double;
      MontoReserva: Double;
      Total: Double;
      IdCondominioCobranza: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigoConjuntoAdministracion(varCodigoConjuntoAdministracion: string);
      procedure SetPeriodoA(varPeriodoA: Integer);
      procedure SetPeriodoM(varPeriodoM: Integer);
      procedure SetCodigoInmueble(varCodigoInmueble: string);
      procedure SetOrdenCobranza(varOrdenCobranza: Integer);
      procedure SetFechaIngreso(varFechaIngreso: TDate);
      procedure SetFechaVencimiento(varFechaVencimiento: TDate);
      procedure SetFechaCobranza(varFechaCobranza: TDate);
      procedure SetMontoNeto(varMontoNeto: Double);
      procedure SetPReserva(varPReserva: Double);
      procedure SetMontoReserva(varMontoReserva: Double);
      procedure SetTotal(varTotal: Double);
      procedure SetIdCondominioCobranza(varIdCondominioCobranza: Integer);
      function GetCodigoConjuntoAdministracion: string;
      function GetPeriodoA: Integer;
      function GetPeriodoM: Integer;
      function GetCodigoInmueble: string;
      function GetOrdenCobranza: Integer;
      function GetFechaIngreso: TDate;
      function GetFechaVencimiento: TDate;
      function GetFechaCobranza: TDate;
      function GetMontoNeto: Double;
      function GetPReserva: Double;
      function GetMontoReserva: Double;
      function GetTotal: Double;
      function GetIdCondominioCobranza: Integer;
  end;

var
  Clase: TCondominioCobranzaCobrada;

implementation

{ TCondominioCobranzaCobrada }

procedure TCondominioCobranzaCobrada.Ajustar;
begin

end;

procedure TCondominioCobranzaCobrada.Mover(Ds: TClientDataSet);
begin
  CodigoConjuntoAdministracion := Ds.FieldValues['codigo_conjunto_administracion'];
  PeriodoA := Ds.FieldValues['periodo_a'];
  PeriodoM := Ds.FieldValues['periodo_m'];
  CodigoInmueble := Ds.FieldValues['codigo_inmueble'];
  OrdenCobranza := Ds.FieldValues['orden_cobranza'];
  FechaIngreso := Ds.FieldValues['fecha_ingreso'];
  FechaVencimiento := Ds.FieldValues['fecha_vencimiento'];
  FechaCobranza := Ds.FieldValues['fecha_cobranza'];
  MontoNeto := Ds.FieldValues['monto_neto'];
  PReserva := Ds.FieldValues['p_reserva'];
  MontoReserva := Ds.FieldValues['monto_reserva'];
  Total := Ds.FieldValues['total'];
  IdCondominioCobranza := Ds.FieldValues['id_condominio_cobranza'];
end;

function TCondominioCobranzaCobrada.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,13);
  Params[0] := CodigoConjuntoAdministracion;
  Params[1] := PeriodoA;
  Params[2] := PeriodoM;
  Params[3] := CodigoInmueble;
  Params[4] := OrdenCobranza;
  Params[5] := VarFromDateTime(FechaIngreso);
  Params[6] := VarFromDateTime(FechaVencimiento);
  Params[7] := VarFromDateTime(FechaCobranza);
  Params[8] := MontoNeto;
  Params[9] := PReserva;
  Params[10] := MontoReserva;
  Params[11] := Total;
  Params[12] := IdCondominioCobranza;
  Result := Params;
end;

function TCondominioCobranzaCobrada.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,4);
  Campos[0] := 'codigo_conjunto_administracion';
  Campos[1] := 'periodo_a';
  Campos[2] := 'periodo_m';
  Campos[3] := 'codigo_inmueble';
  Result := Campos;
end;

function TCondominioCobranzaCobrada.GetCondicion: string;
begin
  Result := '"codigo_conjunto_administracion" = ' + QuotedStr(CodigoConjuntoAdministracion) + ' AND ' +
            '"periodo_a" = ' + IntToStr(PeriodoA) + ' AND ' +
            '"periodo_m" = ' + IntToStr(PeriodoM) + ' AND ' +
            '"codigo_inmueble" = ' + QuotedStr(CodigoInmueble) + ' AND ' +
            '"orden_cobranza" = ' + IntToStr(OrdenCobranza);
end;

procedure TCondominioCobranzaCobrada.AntesEliminar;
begin

end;

procedure TCondominioCobranzaCobrada.AntesInsertar;
begin

end;

procedure TCondominioCobranzaCobrada.AntesModificar;
begin

end;

procedure TCondominioCobranzaCobrada.DespuesEliminar;
begin

end;

procedure TCondominioCobranzaCobrada.DespuesInsertar;
begin

end;

procedure TCondominioCobranzaCobrada.DespuesModificar;
begin

end;

constructor TCondominioCobranzaCobrada.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_condominio_cobranzas_cobradas');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(13)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "fecha_ingreso"=$6, "fecha_vencimiento"=$7, "fecha_cobranza"=$8, "monto_neto"=$9, "p_reserva"=$10, "monto_reserva"=$11, "total"=$12, "id_condominio_cobranza"=$13 ' +
                  'WHERE ("codigo_conjunto_administracion"=$1 AND "periodo_a"=$2 AND "periodo_m"=$3 AND "codigo_inmueble"=$4 AND "orden_cobranza"=$5)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("codigo_conjunto_administracion"=$1 AND "periodo_a"=$2 AND "periodo_m"=$3 AND "codigo_inmueble"=$4 AND "orden_cobranza"=$5)');
  SetLength(ClavesPrimarias,5);
  ClavesPrimarias[0] := '"codigo_conjunto_administracion"';
  ClavesPrimarias[1] := '"periodo_a"';
  ClavesPrimarias[2] := '"periodo_m"';
  ClavesPrimarias[3] := '"codigo_inmueble"';
  ClavesPrimarias[4] := '"orden_cobranza"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TCondominioCobranzaCobrada.SetCodigoConjuntoAdministracion(varCodigoConjuntoAdministracion: string);
begin
  CodigoConjuntoAdministracion := varCodigoConjuntoAdministracion;
end;

procedure TCondominioCobranzaCobrada.SetPeriodoA(varPeriodoA: Integer);
begin
  PeriodoA := varPeriodoA;
end;

procedure TCondominioCobranzaCobrada.SetPeriodoM(varPeriodoM: Integer);
begin
  PeriodoM := varPeriodoM;
end;

procedure TCondominioCobranzaCobrada.SetCodigoInmueble(varCodigoInmueble: string);
begin
  CodigoInmueble := varCodigoInmueble;
end;

procedure TCondominioCobranzaCobrada.SetOrdenCobranza(varOrdenCobranza: Integer);
begin
  OrdenCobranza := varOrdenCobranza;
end;

procedure TCondominioCobranzaCobrada.SetFechaIngreso(varFechaIngreso: TDate);
begin
  FechaIngreso := varFechaIngreso;
end;

procedure TCondominioCobranzaCobrada.SetFechaVencimiento(varFechaVencimiento: TDate);
begin
  FechaVencimiento := varFechaVencimiento;
end;

procedure TCondominioCobranzaCobrada.SetFechaCobranza(varFechaCobranza: TDate);
begin
  FechaCobranza := varFechaCobranza;
end;

procedure TCondominioCobranzaCobrada.SetMontoNeto(varMontoNeto: Double);
begin
  MontoNeto := varMontoNeto;
end;

procedure TCondominioCobranzaCobrada.SetPReserva(varPReserva: Double);
begin
  PReserva := varPReserva;
end;

procedure TCondominioCobranzaCobrada.SetMontoReserva(varMontoReserva: Double);
begin
  MontoReserva := varMontoReserva;
end;

procedure TCondominioCobranzaCobrada.SetTotal(varTotal: Double);
begin
  Total := varTotal;
end;

procedure TCondominioCobranzaCobrada.SetIdCondominioCobranza(varIdCondominioCobranza: Integer);
begin
  IdCondominioCobranza := varIdCondominioCobranza;
end;

function TCondominioCobranzaCobrada.GetCodigoConjuntoAdministracion: string;
begin
  Result := CodigoConjuntoAdministracion;
end;

function TCondominioCobranzaCobrada.GetPeriodoA: Integer;
begin
  Result := PeriodoA;
end;

function TCondominioCobranzaCobrada.GetPeriodoM: Integer;
begin
  Result := PeriodoM;
end;

function TCondominioCobranzaCobrada.GetCodigoInmueble: string;
begin
  Result := CodigoInmueble;
end;

function TCondominioCobranzaCobrada.GetOrdenCobranza: Integer;
begin
  Result := OrdenCobranza;
end;

function TCondominioCobranzaCobrada.GetFechaIngreso: TDate;
begin
  Result := FechaIngreso;
end;

function TCondominioCobranzaCobrada.GetFechaVencimiento: TDate;
begin
  Result := FechaVencimiento;
end;

function TCondominioCobranzaCobrada.GetFechaCobranza: TDate;
begin
  Result := FechaCobranza;
end;

function TCondominioCobranzaCobrada.GetMontoNeto: Double;
begin
  Result := MontoNeto;
end;

function TCondominioCobranzaCobrada.GetPReserva: Double;
begin
  Result := PReserva;
end;

function TCondominioCobranzaCobrada.GetMontoReserva: Double;
begin
  Result := MontoReserva;
end;

function TCondominioCobranzaCobrada.GetTotal: Double;
begin
  Result := Total;
end;

function TCondominioCobranzaCobrada.GetIdCondominioCobranza: Integer;
begin
  Result := IdCondominioCobranza;
end;

end.
