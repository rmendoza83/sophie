unit ClsCondominioPropietario;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TCondominioPropietario }

  TCondominioPropietario = class(TGenericoBD)
    private
      Codigo: string;
      PrefijoRif: string;
      Rif: string;
      Nit: string;
      RazonSocial: string;
      Sexo: Char;
      FechaNacimiento: TDate;
      DireccionFiscal1: string;
      DireccionFiscal2: string;
      DireccionDomicilio1: string;
      DireccionDomicilio2: string;
      Telefonos: string;
      Fax: string;
      Email: string;
      Website: string;
      FechaIngreso: TDate;
      Contacto: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigo(varCodigo: string);
      procedure SetPrefijoRif(varPrefijoRif: string);
      procedure SetRif(varRif: string);
      procedure SetNit(varNit: string);
      procedure SetRazonSocial(varRazonSocial: string);
      procedure SetSexo(varSexo: Char);
      procedure SetFechaNacimiento(varFechaNacimiento: TDate);
      procedure SetDireccionFiscal1(varDireccionFiscal1: string);
      procedure SetDireccionFiscal2(varDireccionFiscal2: string);
      procedure SetDireccionDomicilio1(varDireccionDomicilio1: string);
      procedure SetDireccionDomicilio2(varDireccionDomicilio2: string);
      procedure SetTelefonos(varTelefonos: string);
      procedure SetFax(varFax: string);
      procedure SetEmail(varEmail: string);
      procedure SetWebsite(varWebsite: string);
      procedure SetFechaIngreso(varFechaIngreso: TDate);
      procedure SetContacto(varContacto: string);
      function GetCodigo: string;
      function GetPrefijoRif: string;
      function GetRif: string;
      function GetNit: string;
      function GetRazonSocial: string;
      function GetSexo: Char;
      function GetFechaNacimiento: TDate;
      function GetDireccionFiscal1: string;
      function GetDireccionFiscal2: string;
      function GetDireccionDomicilio1: string;
      function GetDireccionDomicilio2: string;
      function GetTelefonos: string;
      function GetFax: string;
      function GetEmail: string;
      function GetWebsite: string;
      function GetFechaIngreso: TDate;
      function GetContacto: string;
      function ObtenerCombo: TClientDataSet;
      function BuscarCodigo: Boolean;
  end;

implementation

{ TCondominioPropietario }

procedure TCondominioPropietario.Ajustar;
begin

end;

procedure TCondominioPropietario.Mover(Ds: TClientDataSet);
begin
  Codigo := Ds.FieldValues['codigo'];
  PrefijoRif := Ds.FieldValues['prefijo_rif'];
  Rif := Ds.FieldValues['rif'];
  Nit := Ds.FieldValues['nit'];
  RazonSocial := Ds.FieldValues['razon_social'];
  Sexo := Ds.FieldByName('sexo').AsString[1];
  FechaNacimiento := Ds.FieldByName('fecha_nacimiento').AsDateTime;
  DireccionFiscal1 := Ds.FieldValues['direccion_fiscal_1'];
  DireccionFiscal2 := Ds.FieldValues['direccion_fiscal_2'];
  DireccionDomicilio1 := Ds.FieldValues['direccion_domicilio_1'];
  DireccionDomicilio2 := Ds.FieldValues['direccion_domicilio_2'];
  Telefonos := Ds.FieldValues['telefonos'];
  Fax := Ds.FieldValues['fax'];
  Email := Ds.FieldValues['email'];
  Website := Ds.FieldValues['website'];
  FechaIngreso := Ds.FieldByName('fecha_ingreso').AsDateTime;
  Contacto := Ds.FieldValues['contacto'];
end;

function TCondominioPropietario.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,17);
  Params[0] := Codigo;
  Params[1] := PrefijoRif;
  Params[2] := Rif;
  Params[3] := Nit;
  Params[4] := RazonSocial;
  Params[5] := Sexo;
  Params[6] := VarFromDateTime(FechaNacimiento);
  Params[7] := DireccionFiscal1;
  Params[8] := DireccionFiscal2;
  Params[9] := DireccionDomicilio1;
  Params[10] := DireccionDomicilio2;
  Params[11] := Telefonos;
  Params[12] := Fax;
  Params[13] := Email;
  Params[14] := Website;
  Params[15] := VarFromDateTime(FechaIngreso);
  Params[16] := Contacto;
  Result := Params;
end;

function TCondominioPropietario.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,13);
  Campos[0] := 'codigo';
  Campos[1] := 'rif';
  Campos[2] := 'nit';
  Campos[3] := 'razon_social';
  Campos[4] := 'direccion_fiscal_1';
  Campos[5] := 'direccion_fiscal_2';
  Campos[6] := 'direccion_domicilio_1';
  Campos[7] := 'direccion_domicilio_2';
  Campos[8] := 'telefonos';
  Campos[9] := 'fax';
  Campos[10] := 'email';
  Campos[11] := 'website';
  Campos[12] := 'contacto';
  Result := Campos;
end;

function TCondominioPropietario.GetCondicion: string;
begin
  Result := '"codigo" = ' + QuotedStr(Codigo);
end;

procedure TCondominioPropietario.AntesInsertar;
begin
end;

procedure TCondominioPropietario.AntesModificar;
begin

end;

procedure TCondominioPropietario.AntesEliminar;
begin

end;

procedure TCondominioPropietario.DespuesInsertar;
begin

end;

procedure TCondominioPropietario.DespuesModificar;
begin

end;

procedure TCondominioPropietario.DespuesEliminar;
begin

end;

constructor TCondominioPropietario.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_condominio_propietarios');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(17)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "prefijo_rif"=$2, "rif"=$3, "nit"=$4, "razon_social"=$5, "sexo"=$6, "fecha_nacimiento"=$7, "direccion_fiscal_1"=$8, "direccion_fiscal_2"=$9, "direccion_domicilio_1"=$10, ' +
                  '"direccion_domicilio_2"=$11, "telefonos"=$12, "fax"=$13, "email"=$14, "website"=$15, "fecha_ingreso"=$16, "contacto"=$17 WHERE ("codigo"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("codigo"=$1)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"codigo"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TCondominioPropietario.SetCodigo(varCodigo: string);
begin
  Codigo := varCodigo;
end;

procedure TCondominioPropietario.SetPrefijoRif(varPrefijoRif: string);
begin
  PrefijoRif := varPrefijoRif;
end;

procedure TCondominioPropietario.SetRif(varRif: string);
begin
  Rif := varRif;
end;

procedure TCondominioPropietario.SetNit(varNit: string);
begin
  Nit := varNit;
end;

procedure TCondominioPropietario.SetRazonSocial(varRazonSocial: string);
begin
  RazonSocial := varRazonSocial;
end;

procedure TCondominioPropietario.SetSexo(varSexo: Char);
begin
  Sexo := varSexo;
end;

procedure TCondominioPropietario.SetFechaNacimiento(varFechaNacimiento: TDate);
begin
  FechaNacimiento := varFechaNacimiento;
end;

procedure TCondominioPropietario.SetDireccionFiscal1(varDireccionFiscal1: string);
begin
  DireccionFiscal1 := varDireccionFiscal1;
end;

procedure TCondominioPropietario.SetDireccionFiscal2(varDireccionFiscal2: string);
begin
  DireccionFiscal2 := varDireccionFiscal2;
end;

procedure TCondominioPropietario.SetDireccionDomicilio1(varDireccionDomicilio1: string);
begin
  DireccionDomicilio1 := varDireccionDomicilio1;
end;

procedure TCondominioPropietario.SetDireccionDomicilio2(varDireccionDomicilio2: string);
begin
  DireccionDomicilio2 := varDireccionDomicilio2;
end;

procedure TCondominioPropietario.SetTelefonos(varTelefonos: string);
begin
  Telefonos := varTelefonos;
end;

procedure TCondominioPropietario.SetFax(varFax: string);
begin
  Fax := varFax;
end;

procedure TCondominioPropietario.SetEmail(varEmail: string);
begin
  Email := varEmail;
end;

procedure TCondominioPropietario.SetWebsite(varWebsite: string);
begin
  Website := varWebsite;
end;

procedure TCondominioPropietario.SetFechaIngreso(varFechaIngreso: TDate);
begin
  FechaIngreso := varFechaIngreso;
end;

procedure TCondominioPropietario.SetContacto(varContacto: string);
begin
  Contacto := varContacto;
end;

function TCondominioPropietario.GetCodigo: string;
begin
  Result := Codigo;
end;

function TCondominioPropietario.GetPrefijoRif: string;
begin
  Result := PrefijoRif;
end;

function TCondominioPropietario.GetRif: string;
begin
  Result := Rif;
end;

function TCondominioPropietario.GetNit: string;
begin
  Result := Nit;
end;

function TCondominioPropietario.GetRazonSocial: string;
begin
  Result := RazonSocial;
end;

function TCondominioPropietario.GetSexo: Char;
begin
  Result := Sexo;
end;

function TCondominioPropietario.GetFechaNacimiento: TDate;
begin
  Result := FechaNacimiento;
end;

function TCondominioPropietario.GetDireccionFiscal1: string;
begin
  Result := DireccionFiscal1;
end;

function TCondominioPropietario.GetDireccionFiscal2: string;
begin
  Result := DireccionFiscal2;
end;

function TCondominioPropietario.GetDireccionDomicilio1: string;
begin
  Result := DireccionDomicilio1;
end;

function TCondominioPropietario.GetDireccionDomicilio2: string;
begin
  Result := DireccionDomicilio2;
end;

function TCondominioPropietario.GetTelefonos: string;
begin
  Result := Telefonos;
end;

function TCondominioPropietario.GetFax: string;
begin
  Result := Fax;
end;

function TCondominioPropietario.GetEmail: string;
begin
  Result := Email;
end;

function TCondominioPropietario.GetWebsite: string;
begin
  Result := Website;
end;

function TCondominioPropietario.GetFechaIngreso: TDate;
begin
  Result := FechaIngreso;
end;

function TCondominioPropietario.GetContacto: string;
begin
  Result := Contacto;
end;

function TCondominioPropietario.ObtenerCombo: TClientDataSet;
begin
  Result := BD.EjecutarQueryCliDs('SELECT "codigo", "razon_social" FROM ' + GetTabla + ' ORDER BY "codigo"');
end;

function TCondominioPropietario.BuscarCodigo: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("codigo" = ' + QuotedStr(GetCodigo) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

end.

