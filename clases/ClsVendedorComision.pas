unit ClsVendedorComision;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TVendedorComision }

  TVendedorComision = class(TGenericoBD)
    private
      CodigoVendedor: string;
      CodigoTipoComision: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigoVendedor(varCodigoVendedor: string);
      procedure SetCodigoTipoComision(varCodigoTipoComision: string);
      function GetCodigoVendedor: string;
      function GetCodigoTipoComision: string;
      function ObtenerCodigoVendedor(varCodigoVendedor: string): TClientDataSet;
      function EliminarCodigoVendedor(varCodigoVendedor: string): Boolean;
  end;

implementation

{ TVendedorComision }

procedure TVendedorComision.Ajustar;
begin

end;

procedure TVendedorComision.Mover(Ds: TClientDataSet);
begin
  CodigoVendedor := Ds.FieldValues['codigo_vendedor'];
  CodigoTipoComision := Ds.FieldValues['codigo_tipo_comision'];
end;

function TVendedorComision.ObtenerCodigoVendedor(varCodigoVendedor: string): TClientDataSet;
begin
  Result := ObtenerListaCondicionSQL(' WHERE "codigo_vendedor" = ' + QuotedStr(varCodigoVendedor));
end;

function TVendedorComision.EliminarCodigoVendedor(varCodigoVendedor: string): Boolean;
begin
  Result := EjecutarSQL('DELETE FROM ' + GetTabla + ' WHERE "codigo_vendedor" = ' + QuotedStr(varCodigoVendedor));
end;

function TVendedorComision.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,2);
  Params[0] := CodigoVendedor;
  Params[1] := CodigoTipoComision;
  Result := Params;
end;

function TVendedorComision.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,2);
  Campos[0] := 'codigo_vendedor';
  Campos[1] := 'codigo_tipo_comision';
  Result := Campos;
end;

function TVendedorComision.GetCondicion: string;
begin
  Result := '"codigo_vendedor" =' + QuotedStr(CodigoVendedor) + 'AND "codigo_tipo_comision" =' + QuotedStr(CodigoTipoComision);
end;

procedure TVendedorComision.AntesInsertar;
begin

end;

procedure TVendedorComision.AntesModificar;
begin

end;

procedure TVendedorComision.AntesEliminar;
begin

end;

procedure TVendedorComision.DespuesInsertar;
begin

end;

procedure TVendedorComision.DespuesModificar;
begin

end;

procedure TVendedorComision.DespuesEliminar;
begin

end;

constructor TVendedorComision.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_vendedores_comisiones');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(2)) + ')');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + 'WHERE ("codigo_vendedor"=$1) AND ("codigo_tipo_comision"=$2)');
  SetLength(ClavesPrimarias,2);
  ClavesPrimarias[0] := '"codigo_vendedor"';
  ClavesPrimarias[1] := '"codigo_tipo_comision"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TVendedorComision.SetCodigoTipoComision(varCodigoTipoComision: string);
begin
  CodigoTipoComision := varCodigoTipoComision;
end;

procedure TVendedorComision.SetCodigoVendedor(varCodigoVendedor: string);
begin
  CodigoVendedor := varCodigoVendedor;
end;

function TVendedorComision.GetCodigoTipoComision: string;
begin
  Result := CodigoTipoComision;
end;

function TVendedorComision.GetCodigoVendedor: string;
begin
  Result := CodigoVendedor;
end;

end.
