unit ClsBusquedaNotaCreditoCliente;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TBusquedaNotaCreditoCliente }

  TBusquedaNotaCreditoCliente = class(TGenericoBD)
    private
      Numero: string;
      CodigoCliente: string;
      Rif: string;
      Nit: string;
      RazonSocial: string;
      Contacto: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      Function GetNumero: string;
      Function GetCodigoCliente: string;
      Function GetRif: string;
      Function GetNit: string;
      Function GetRazonSocial: string;
      Function GetContacto: string;
  end;

implementation

{ TBusquedaNotaCreditoCliente }

procedure TBusquedaNotaCreditoCliente.Ajustar;
begin

end;

procedure TBusquedaNotaCreditoCliente.Mover(Ds: TClientDataSet);
begin
  Numero := Ds.FieldValues['numero'];
  CodigoCliente := Ds.FieldValues['codigo_cliente'];
  Rif := Ds.FieldValues['rif'];
  Nit := Ds.FieldValues['nit'];
  RazonSocial := Ds.FieldValues['razon_social'];
  Contacto := Ds.FieldValues['contacto'];
end;

function TBusquedaNotaCreditoCliente.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,6);
  Params[0] := Numero;
  Params[1] := CodigoCliente;
  Params[2] := Rif;
  Params[3] := Nit;
  Params[4] := RazonSocial;
  Params[5] := Contacto;
  Result := Params;
end;

function TBusquedaNotaCreditoCliente.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,6);
  Campos[0] := 'numero';
  Campos[1] := 'codigo_cliente';
  Campos[2] := 'rif';
  Campos[3] := 'nit';
  Campos[4] := 'razon_social';
  Campos[5] := 'contacto';
  Result := Campos;
end;

function TBusquedaNotaCreditoCliente.GetCondicion: string;
begin
  Result:= '';
end;

procedure TBusquedaNotaCreditoCliente.AntesInsertar;
begin

end;

procedure TBusquedaNotaCreditoCliente.AntesModificar;
begin

end;

procedure TBusquedaNotaCreditoCliente.AntesEliminar;
begin

end;

procedure TBusquedaNotaCreditoCliente.DespuesInsertar;
begin

end;

procedure TBusquedaNotaCreditoCliente.DespuesModificar;
begin

end;

procedure TBusquedaNotaCreditoCliente.DespuesEliminar;
begin

end;

constructor TBusquedaNotaCreditoCliente.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('v_busqueda_nota_credito_clientes');
  SetLength(ClavesPrimarias,0);
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

function TBusquedaNotaCreditoCliente.GetCodigoCliente: string;
begin
  Result := CodigoCliente;
end;

function TBusquedaNotaCreditoCliente.GetContacto: string;
begin
  Result := Contacto;
end;

function TBusquedaNotaCreditoCliente.GetNit: string;
begin
  Result := Nit;
end;

function TBusquedaNotaCreditoCliente.GetNumero: string;
begin
  Result := Numero;
end;

function TBusquedaNotaCreditoCliente.GetRazonSocial: string;
begin
  Result := RazonSocial;
end;

function TBusquedaNotaCreditoCliente.GetRif: string;
begin
  Result := Rif;
end;

end.
