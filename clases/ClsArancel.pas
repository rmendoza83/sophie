unit ClsArancel;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TArancel }

  TArancel = class(TGenericoBD)
    private
      Codigo: string;
      Descripcion: string;
      Arancel: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigo(varCodigo: string);
      procedure SetDescripcion(varDescripcion: string);
      procedure SetArancel(varArancel: Integer);
      function GetCodigo: string;
      function GetDescripcion: string;
      function GetArancel: Integer;
  end;

var
  Arancel: TArancel;

implementation

{ TArancel }

procedure TArancel.Ajustar;
begin

end;

procedure TArancel.Mover(Ds: TClientDataSet);
begin
  Codigo := Ds.FieldValues['codigo'];
  Descripcion := Ds.FieldValues['descripcion'];
  Arancel:= Ds.FieldValues['arancel'];
end;

function TArancel.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,3);
  Params[0] := Codigo;
  Params[1] := Descripcion;
  Params[2] := Arancel;
  Result := Params;
end;

function TArancel.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,2);
  Campos[0] := 'codigo';
  Campos[1] := 'descripcion';
  Result := Campos;
end;

function TArancel.GetCondicion: string;
begin
  Result := '"codigo" = ' + QuotedStr(Codigo);
end;

procedure TArancel.AntesEliminar;
begin

end;

procedure TArancel.AntesInsertar;
begin

end;

procedure TArancel.AntesModificar;
begin

end;

procedure TArancel.DespuesEliminar;
begin

end;

procedure TArancel.DespuesInsertar;
begin

end;

procedure TArancel.DespuesModificar;
begin

end;

constructor TArancel.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_aranceles');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(3)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "descripcion"=$2,"arancel"=$3 WHERE ("codigo"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("codigo"=$1)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"codigo"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TArancel.SetCodigo(varCodigo: string);
begin
  Codigo := varCodigo;
end;

procedure TArancel.SetDescripcion(varDescripcion: string);
begin
  Descripcion := varDescripcion;
end;

procedure TArancel.SetArancel(varArancel: Integer);
begin
  Arancel:= varArancel;
end;

function TArancel.GetCodigo: string;
begin
  Result := Codigo;
end;

function TArancel.GetDescripcion: string;
begin
  Result := Descripcion;
end;

function TArancel.GetArancel: Integer;
begin
  Result := Arancel;
end;

end.
