unit ClsBusquedaFacturaRadioContrato;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TBusquedaFacturaRadioContrato }

  TBusquedaFacturaRadioContrato = class(TGenericoBD)
    private
      NumeroContrato: string;
      CodigoCliente: string;
      Rif: string;
      Nit: string;
      RazonSocial: string;
      Contacto: string;
      TotalRadioContrato: Double;
      IdRadioContrato: Integer;
      Item: Integer;
      TotalItems: Integer;
      FechaIngreso: TDate;
      Total: Double;
      FechaInicial: TDate;
      FechaFinal: TDate;
      Observaciones: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetIdRadioContrato(varIdRadioContrato: Integer);
      procedure SetItem(varItem: Integer);
      function GetNumeroContrato: string;
      function GetCodigoCliente: string;
      function GetRif: string;
      function GetNit: string;
      function GetRazonSocial: string;
      function GetContacto: string;
      function GetTotalRadioContrato: Double;
      function GetIdRadioContrato: Integer;
      function GetItem: Integer;
      function GetTotalItems: Integer;
      function GetFechaIngreso: TDate;
      function GetTotal: Double;
      function GetFechaInicial: TDate;
      function GetFechaFinal: TDate;
      function GetObservaciones: string;
  end;

implementation

{ TBusquedaFacturaRadioContrato }

procedure TBusquedaFacturaRadioContrato.Ajustar;
begin

end;

procedure TBusquedaFacturaRadioContrato.Mover(Ds: TClientDataSet);
begin
  NumeroContrato := Ds.FieldValues['numero_contrato'];
  CodigoCliente := Ds.FieldValues['codigo_cliente'];
  Rif := Ds.FieldValues['rif'];
  Nit := Ds.FieldValues['nit'];
  RazonSocial := Ds.FieldValues['razon_social'];
  Contacto := Ds.FieldValues['contacto'];
  TotalRadioContrato := Ds.FieldValues['total_radio_contrato'];
  IdRadioContrato := Ds.FieldValues['id_radio_contrato'];
  Item := Ds.FieldValues['item'];
  TotalItems := Ds.FieldValues['total_items'];
  FechaIngreso := Ds.FieldValues['fecha_ingreso'];
  Total := Ds.FieldValues['total'];
  FechaInicial := Ds.FieldValues['fecha_inicial'];
  FechaFinal := Ds.FieldValues['fecha_final'];
  Observaciones := Ds.FieldValues['observaciones'];
end;

function TBusquedaFacturaRadioContrato.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,15);
  Params[0] := NumeroContrato;
  Params[1] := CodigoCliente;
  Params[2] := Rif;
  Params[3] := Nit;
  Params[4] := RazonSocial;
  Params[5] := Contacto;
  Params[6] := TotalRadioContrato;
  Params[7] := IdRadioContrato;
  Params[8] := Item;
  Params[9] := TotalItems;
  Params[10] := VarFromDateTime(FechaIngreso);
  Params[11] := Total;
  Params[12] := VarFromDateTime(FechaInicial);
  Params[13] := VarFromDateTime(FechaFinal);
  Params[14] := Observaciones;
  Result := Params;
end;

function TBusquedaFacturaRadioContrato.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,9);
  Campos[0] := 'numero_contrato';
  Campos[1] := 'item';
  Campos[2] := 'total_items';
  Campos[3] := 'codigo_cliente';
  Campos[4] := 'razon_social';
  Campos[5] := 'rif';
  Campos[6] := 'nit';
  Campos[7] := 'contacto';
  Campos[8] := 'observaciones';
  Result := Campos;
end;

function TBusquedaFacturaRadioContrato.GetCondicion: string;
begin
  Result := '("id_radio_contrato" = ' + IntToStr(IdRadioContrato) + ') AND ' +
            '("item" = ' + IntToStr(Item) + ')';
end;

procedure TBusquedaFacturaRadioContrato.AntesInsertar;
begin

end;

procedure TBusquedaFacturaRadioContrato.AntesModificar;
begin

end;

procedure TBusquedaFacturaRadioContrato.AntesEliminar;
begin

end;

procedure TBusquedaFacturaRadioContrato.DespuesInsertar;
begin

end;

procedure TBusquedaFacturaRadioContrato.DespuesModificar;
begin

end;

procedure TBusquedaFacturaRadioContrato.DespuesEliminar;
begin

end;

constructor TBusquedaFacturaRadioContrato.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('v_busqueda_facturas_radio_contratos');
  SetLength(ClavesPrimarias,0);
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TBusquedaFacturaRadioContrato.SetIdRadioContrato(varIdRadioContrato: Integer);
begin
  IdRadioContrato := varIdRadioContrato;
end;

procedure TBusquedaFacturaRadioContrato.SetItem(varItem: Integer);
begin
  Item := varItem;
end;

function TBusquedaFacturaRadioContrato.GetCodigoCliente: string;
begin
  Result := CodigoCliente;
end;

function TBusquedaFacturaRadioContrato.GetContacto: string;
begin
  Result := Contacto;
end;

function TBusquedaFacturaRadioContrato.GetFechaFinal: TDate;
begin
  Result := FechaFinal;
end;

function TBusquedaFacturaRadioContrato.GetFechaIngreso: TDate;
begin
  Result := FechaIngreso;
end;

function TBusquedaFacturaRadioContrato.GetFechaInicial: TDate;
begin
  Result := FechaInicial;
end;

function TBusquedaFacturaRadioContrato.GetIdRadioContrato: Integer;
begin
  Result := IdRadioContrato;
end;

function TBusquedaFacturaRadioContrato.GetItem: Integer;
begin
  Result := Item;
end;

function TBusquedaFacturaRadioContrato.GetNit: string;
begin
  Result := Nit;
end;

function TBusquedaFacturaRadioContrato.GetNumeroContrato: string;
begin
  Result := NumeroContrato;
end;

function TBusquedaFacturaRadioContrato.GetObservaciones: string;
begin
  Result := Observaciones;
end;

function TBusquedaFacturaRadioContrato.GetRazonSocial: string;
begin
  Result := RazonSocial;
end;

function TBusquedaFacturaRadioContrato.GetRif: string;
begin
  Result := Rif;
end;

function TBusquedaFacturaRadioContrato.GetTotal: Double;
begin
  Result := Total;
end;

function TBusquedaFacturaRadioContrato.GetTotalItems: Integer;
begin
  Result := TotalItems;
end;

function TBusquedaFacturaRadioContrato.GetTotalRadioContrato: Double;
begin
  Result := TotalRadioContrato;
end;

end.
