unit ClsCondominioDetalleServicioPago;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TCondominioDetalleServicioPago }

  TCondominioDetalleServicioPago = class(TGenericoBD)
    private
      IdCondominioServicio: Integer;
      CodigoFormaPago: string;
      Monto: Double;
      CodigoBanco: string;
      NumeroDocumento: string;
      NumeroAuxiliar: string;
      Referencia: string;
      Fecha: TDate;
      LoginUsuario: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetIdCondominioServicio(varIdCondominioServicio: Integer);
      procedure SetCodigoFormaPago(varCodigoFormaPago: string);
      procedure SetMonto(varMonto: Double);
      procedure SetCodigoBanco(varCodigoBanco: string);
      procedure SetNumeroDocumento(varNumeroDocumento: string);
      procedure SetNumeroAuxiliar(varNumeroAuxiliar: string);
      procedure SetReferencia(varReferencia: string);
      procedure SetFecha(varFecha: TDate);
      procedure SetLoginUsuario(varLoginUsuario: string);
      function GetIdCondominioServicio: Integer;
      function GetCodigoFormaPago: string;
      function GetMonto: Double;
      function GetCodigoBanco: string;
      function GetNumeroDocumento: string;
      function GetNumeroAuxiliar: string;
      function GetReferencia: string;
      function GetFecha: TDate;
      function GetLoginUsuario: string;
      function EliminarIdCondominioServicio(varIdCondominioServicio: Integer): Boolean;
  end;

implementation

uses DB;

{ TCondominioDetalleServicioPago }

procedure TCondominioDetalleServicioPago.Ajustar;
begin

end;

procedure TCondominioDetalleServicioPago.Mover(Ds: TClientDataSet);
begin
  IdCondominioServicio := Ds.FieldValues['id_condominio_servicio'];
  CodigoFormaPago := Ds.FieldValues['codigo_forma_pago'];
  Monto := Ds.FieldValues['monto'];
  CodigoBanco := Ds.FieldValues['codigo_banco'];
  NumeroDocumento := Ds.FieldValues['numero_documento'];
  NumeroAuxiliar := Ds.FieldValues['numero_auxiliar'];
  Referencia := Ds.FieldValues['referencia'];
  Fecha := Ds.FieldByName('fecha').AsDateTime ;
  LoginUsuario := Ds.FieldValues['login_usuario'];
end;

function TCondominioDetalleServicioPago.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,9);
  Params[0] := IdCondominioServicio;
  Params[1] := CodigoFormaPago;
  Params[2] := Monto;
  Params[3] := CodigoBanco;
  Params[4] := NumeroDocumento;
  Params[5] := NumeroAuxiliar;
  Params[6] := Referencia;
  Params[7] := VarToDateTime(Fecha);
  Params[8] := LoginUsuario;

  result := Params;
end;

function TCondominioDetalleServicioPago.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,6);
  Campos[0] := 'codigo_forma_pago';
  Campos[1] := 'codigo_banco';
  Campos[2] := 'numero_documento';
  Campos[3] := 'numero_auxiliar';
  Campos[4] := 'referencia';
  Campos[5] := 'login_usuario';
  Result := Campos;
end;

function TCondominioDetalleServicioPago.GetCondicion: string;
begin
  Result := '"id_condominio_servicio" = ' + IntToStr(IdCondominioServicio) + ' AND ' +
            '"codigo_forma_pago" =' + QuotedStr(CodigoFormaPago);
end;

procedure TCondominioDetalleServicioPago.AntesInsertar;
begin

end;

procedure TCondominioDetalleServicioPago.AntesModificar;
begin

end;

procedure TCondominioDetalleServicioPago.AntesEliminar;
begin

end;

procedure TCondominioDetalleServicioPago.DespuesInsertar;
begin

end;

procedure TCondominioDetalleServicioPago.DespuesModificar;
begin

end;

procedure TCondominioDetalleServicioPago.DespuesEliminar;
begin

end;

constructor TCondominioDetalleServicioPago.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_condominio_det_servicios_pagos');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(9)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "monto"=$3,"codigo_banco"=$4,"numero_documento"=$5,"numero_auxiliar"=$6,"referencia"=$7,"fecha"=$8,"login_usuario"=$9 ' +
                  'WHERE ("id_condominio_servicio"=$1) AND ("codigo_forma_pago"=$2)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("id_condominio_servicio"=$1) AND ("codigo_forma_pago"=$2)');
  SetLength(ClavesPrimarias,2);
  ClavesPrimarias[0]:='"id_contado_pago"';
  ClavesPrimarias[1]:='"codigo_forma_pago"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TCondominioDetalleServicioPago.SetIdCondominioServicio(varIdCondominioServicio: Integer);
begin
  IdCondominioServicio := varIdCondominioServicio;
end;

procedure TCondominioDetalleServicioPago.SetCodigoBanco(varCodigoBanco: string);
begin
  CodigoBanco := varCodigoBanco;
end;

procedure TCondominioDetalleServicioPago.SetCodigoFormapago(varCodigoFormaPago: string);
begin
  CodigoFormaPago := varCodigoFormaPago;
end;

procedure TCondominioDetalleServicioPago.SetFecha(varFecha: TDate);
begin
  Fecha := varFecha;
end;

procedure TCondominioDetalleServicioPago.SetMonto(varMonto: Double);
begin
  Monto := varMonto;
end;

procedure TCondominioDetalleServicioPago.SetNumeroAuxiliar(varNumeroAuxiliar: string);
begin
  NumeroAuxiliar := varNumeroAuxiliar;
end;

procedure TCondominioDetalleServicioPago.SetNumeroDocumento(varNumeroDocumento: string);
begin
  NumeroDocumento := varNumeroDocumento;
end;

procedure TCondominioDetalleServicioPago.SetReferencia(varReferencia: string);
begin
  Referencia := varReferencia;
end;

procedure TCondominioDetalleServicioPago.SetLoginUsuario(varLoginUsuario: string);
begin
  LoginUsuario := varLoginUsuario;
end;

function TCondominioDetalleServicioPago.GetCodigoBanco: string;
begin
  Result := CodigoBanco;
end;

function TCondominioDetalleServicioPago.GetCodigoFormaPago: string;
begin
  Result := CodigoFormaPago;
end;

function TCondominioDetalleServicioPago.GetFecha: TDate;
begin
  Result := Fecha;
end;

function TCondominioDetalleServicioPago.GetIdCondominioServicio: Integer;
begin
  Result := IdCondominioServicio;
end;

function TCondominioDetalleServicioPago.GetMonto: Double;
begin
  Result := Monto;
end;

function TCondominioDetalleServicioPago.GetNumeroAuxiliar: string;
begin
  Result := NumeroAuxiliar;
end;

function TCondominioDetalleServicioPago.GetNumeroDocumento: string;
begin
  Result := NumeroDocumento;
end;

function TCondominioDetalleServicioPago.GetReferencia: string;
begin
  Result := Referencia;
end;

function TCondominioDetalleServicioPago.GetLoginUsuario: string;
begin
  Result := LoginUsuario;
end;

function TCondominioDetalleServicioPago.EliminarIdCondominioServicio(
  varIdCondominioServicio: Integer): Boolean;
begin
  Result := EjecutarSQL('DELETE FROM ' + GetTabla + ' WHERE "id_condominio_servicio" = ' + IntToStr(varIdCondominioServicio));
end;

end.
