unit ClsEsquemaPago;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TEsquemaPago }

  TEsquemaPago = class(TGenericoBD)
    private
      Codigo: string;
      Descripcion: string;
      Credito: Boolean;
      Inicial: Boolean;
      PInicial: Double;
      Cuotas : Boolean;
      DiasCuota: Integer;
      CantidadCuotas: Integer;
      Dias : Boolean;
      CantidadDias : Integer;
      Interes : Boolean;
      TasaInteres : Currency;
      TasaOrdinal: Boolean;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      function ObtenerCombo: TClientDataSet;
      procedure SetCodigo(varCodigo: string);
      procedure SetDescripcion(varDescripcion: string);
      procedure SetCredito(varCredito: Boolean);
      procedure SetInicial(varInicial: Boolean);
      procedure SetPInicial(varPinicial: Double);
      procedure SetCuotas(varCoutas: Boolean);
      procedure SetDiasCuota(varDiasCuota: Integer);
      procedure SetCantidadCuotas(varCantidadCuotas: Integer);
      procedure SetDias(varDias: Boolean);
      procedure SetCantidadDias(varCantidadDias: Integer);
      procedure SetInteres(varInteres: Boolean);
      procedure SetTasaInteres(varTasaInteres: Currency);
      procedure SetTasaOrdinal(varTasaOrdinal: Boolean);
      function GetCodigo: string;
      function GetDescripcion: string;
      function GetCredito: Boolean;
      function GetInicial: Boolean;
      function GetPInicial: Double;
      function GetCuotas: Boolean;
      function GetDiasCuota: Integer;
      function GetCantidadCuotas: Integer;
      function GetDias: Boolean;
      function GetCantidadDias: Integer;
      function GetInteres: Boolean;
      function GetTasaInteres: Currency;
      function GetTasaOrdinal: Boolean;
  end;

implementation

{ TEsquemaPago }

procedure TEsquemaPago.Ajustar;
begin

end;

procedure TEsquemaPago.Mover(Ds: TClientDataSet);
begin
  Codigo := Ds.FieldValues['codigo'];
  Descripcion := Ds.FieldValues['descripcion'];
  Credito := Ds.FieldValues['credito'];
  Inicial := Ds.FieldValues['inicial'];
  PInicial := Ds.FieldValues['p_inicial'];
  Cuotas := Ds.FieldValues['cuotas'];
  DiasCuota := Ds.FieldValues['dias_cuota'];
  CantidadCuotas := Ds.FieldValues['cantidad_cuotas'];
  Dias := Ds.FieldValues['dias'];
  CantidadDias := Ds.FieldValues['cantidad_dias'];
  Interes := Ds.FieldValues['interes'];
  TasaInteres := Ds.FieldValues['tasa_interes'];
  TasaOrdinal := Ds.FieldValues['tasa_ordinal'];
end;

function TEsquemaPago.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,13);
  Params[0] := Codigo;
  Params[1] := Descripcion;
  Params[2] := Credito;
  Params[3] := Inicial;
  Params[4] := PInicial;
  Params[5] := Cuotas;
  Params[6] := DiasCuota;
  Params[7] := CantidadCuotas;
  Params[8] := Dias;
  Params[9] := CantidadDias;
  Params[10] := Interes;
  Params[11] := TasaInteres;
  Params[12] := TasaOrdinal;
  Result := Params;
end;

function TEsquemaPago.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,2);
  Campos[0] := 'codigo';
  Campos[1] := 'descripcion';
  Result := Campos;
end;

function TEsquemaPago.GetCondicion: string;
begin
  Result := '"codigo" = ' + QuotedStr(Codigo);
end;

procedure TEsquemaPago.AntesInsertar;
begin

end;

procedure TEsquemaPago.AntesModificar;
begin

end;

procedure TEsquemaPago.AntesEliminar;
begin

end;

procedure TEsquemaPago.DespuesInsertar;
begin

end;

procedure TEsquemaPago.DespuesModificar;
begin

end;


procedure TEsquemaPago.DespuesEliminar;
begin

end;

constructor TEsquemaPago.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_esquemas_pago');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(13)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "descripcion"=$2, "credito"=$3,"inicial"=$4,"p_inicial"=$5,"cuotas"=$6,"dias_cuota"=$7,"cantidad_cuotas"=$8,"dias"=$9,"cantidad_dias"=$10,"interes"=$11,"tasa_interes"=$12,"tasa_ordinal"=$13 WHERE ("codigo"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("codigo"=$1)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"codigo"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

function TEsquemaPago.ObtenerCombo: TClientDataSet;
begin
  Result := BD.EjecutarQueryCliDs('SELECT "codigo", "descripcion" FROM ' + GetTabla);
end;

procedure TEsquemaPago.SetCodigo(varCodigo: string);
begin
  Codigo := varCodigo;
end;

procedure TEsquemaPago.SetDescripcion(varDescripcion: string);
begin
  Descripcion := varDescripcion;
end;

procedure TEsquemaPago.SetCredito(varCredito: Boolean);
begin
  Credito := varCredito;
end;

procedure TEsquemaPago.SetInicial(varInicial: Boolean);
begin
  Inicial := varInicial;
end;

procedure TEsquemaPago.SetPInicial(varPInicial: Double);
begin
  PInicial := varPInicial;
end;

procedure TEsquemaPago.SetCuotas(varCoutas: Boolean);
begin
  Cuotas := varCoutas;
end;

procedure TEsquemaPago.SetDiasCuota(varDiasCuota: Integer);
begin
  DiasCuota := varDiasCuota;
end;

procedure TEsquemaPago.SetCantidadCuotas(varCantidadCuotas: Integer);
begin
  CantidadCuotas := varCantidadCuotas;
end;

procedure TEsquemaPago.SetDias(varDias: Boolean);
begin
  Dias := varDias;
end;

procedure TEsquemaPago.SetCantidadDias(varCantidadDias: Integer);
begin
  CantidadDias := varCantidadDias;
end;

procedure TEsquemaPago.SetInteres(varInteres: Boolean);
begin
  Interes := varInteres;
end;

procedure TEsquemaPago.SetTasaInteres(varTasaInteres: Currency);
begin
 TasaInteres := varTasaInteres;
end;

procedure TEsquemaPago.SetTasaOrdinal(varTasaOrdinal: Boolean);
begin
  TasaOrdinal := varTasaOrdinal;
end;

function TEsquemaPago.GetCodigo: string;
begin
  Result := Codigo;
end;

function TEsquemaPago.GetDescripcion: String;
begin
  Result := Descripcion;
end;

function TEsquemaPago.GetCredito: Boolean;
begin
  Result := Credito;
end;

function TEsquemaPago.GetInicial: Boolean;
begin
 Result := Inicial;
end;

function TEsquemaPago.GetPInicial: Double;
begin
 Result := PInicial;
end;

function TEsquemaPago.GetCuotas: Boolean;
begin
 Result := Cuotas;
end;

function TEsquemaPago.GetDiasCuota: Integer;
begin
 Result := DiasCuota;
end;

function TEsquemaPago.GetCantidadCuotas: Integer;
begin
 Result := CantidadCuotas;
end;

function TEsquemaPago.GetDias: Boolean;
begin
 Result := Dias;
end;

function TEsquemaPago.GetCantidadDias: Integer;
begin
 Result := CantidadDias;
end;

function TEsquemaPago.GetInteres: Boolean;
begin
 Result := Interes;
end;

function TEsquemaPago.GetTasaInteres: Currency;
begin
 Result := TasaInteres;
end;

function TEsquemaPago.GetTasaOrdinal: Boolean;
begin
 Result := TasaOrdinal;
end;

end.
