unit ClsCondominioServicio;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TCondominioServicio }

  TCondominioServicio = class(TGenericoBD)
    private
      Numero: string;
      CodigoConjuntoAdministracion: string;
      CodigoMoneda: string;
      CodigoInmueble: string;
      FechaIngreso: TDate;
      FechaServicio: TDate;
      Concepto: string;
      Observaciones: string;
      MontoNeto: Double;
      MontoDescuento: Double;
      SubTotal: Double;
      Impuesto: Double;
      Total: Double;
      PDescuento: Double;
      PIva: Double;
      LoginUsuario: string;
      TiempoIngreso: TDateTime;
      IdCondominioServicio: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArraystring; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetNumero(varNumero: string);
      procedure SetCodigoConjuntoAdministracion(varCodigoConjuntoAdministracion: string);
      procedure SetCodigoMoneda(varCodigoMoneda: string);
      procedure SetCodigoInmueble(varCodigoInmueble: string);
      procedure SetFechaIngreso(varFechaIngreso: TDate);
      procedure SetFechaServicio(varFechaServicio: TDate);
      procedure SetConcepto(varConcepto: string);
      procedure SetObservaciones(varObservaciones: string);
      procedure SetMontoNeto(varMontoNeto: Double);
      procedure SetMontoDescuento(varMontoDescuento: Double);
      procedure SetSubTotal(varSubTotal: Double);
      procedure SetImpuesto(varImpuesto: Double);
      procedure SetTotal(varTotal: Double);
      procedure SetPDescuento(varPDescuento: Double);
      procedure SetPIva(varPIva: Double);
      procedure SetLoginUsuario(varLoginUsuario: string);
      procedure SetTiempoIngreso(varTiempodeIngreso: TDateTime);
      procedure SetIdCondominioServicio(varIdCondominioServicio: Integer);
      function GetNumero: string;
      function GetCodigoConjuntoAdministracion: string;
      function GetCodigoMoneda: string;
      function GetCodigoInmueble: string;
      function GetFechaIngreso: TDate;
      function GetFechaServicio: TDate;
      function GetConcepto: string;
      function GetObservaciones: string;
      function GetMontoNeto: Double;
      function GetMontoDescuento: Double;
      function GetSubTotal: Double;
      function GetImpuesto: Double;
      function GetTotal: Double;
      function GetPDescuento: Double;
      function GetPIva: Double;
      function GetLoginUsuario: string;
      function GetTiempoIngreso: TDateTime;
      function GetIdCondominioServicio: Integer;
      function BuscarNumero: Boolean;
      function BuscarCodigoInmueble: Boolean;
  end;

implementation

uses
  DB,
  ClsConsecutivo;

{ TCondominioServicio }

procedure TCondominioServicio.Ajustar;
begin

end;

procedure TCondominioServicio.Mover(Ds: TClientDataSet);
begin
  Numero := Ds.FieldValues['numero'];
  CodigoConjuntoAdministracion := Ds.FieldValues['codigo_conjunto_administracion'];
  CodigoMoneda := Ds.FieldValues['codigo_moneda'];
  CodigoInmueble := Ds.FieldValues['codigo_inmueble'];
  FechaIngreso := Ds.FieldByName('fecha_ingreso').AsDateTime;
  FechaServicio := Ds.FieldByName('fecha_servicio').AsDateTime;
  Concepto := Ds.FieldValues['concepto'];
  Observaciones := Ds.FieldValues['observaciones'];
  MontoNeto := Ds.FieldValues['monto_neto'];
  MontoDescuento := Ds.FieldValues['monto_descuento'];
  SubTotal := Ds.FieldValues['sub_total'];
  Impuesto := Ds.FieldValues['impuesto'];
  Total := Ds.FieldValues['total'];
  PDescuento := Ds.FieldValues['p_descuento'];
  PIva := Ds.FieldValues['p_iva'];
  LoginUsuario := Ds.FieldValues['login_usuario'];
  TiempoIngreso := Ds.FieldByName('tiempo_ingreso').AsDateTime;
  IdCondominioServicio := Ds.FieldValues['id_condominio_servicio'];
end;

function TCondominioServicio.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,18);
  Params[0] := Numero;
  Params[1] := CodigoConjuntoAdministracion;
  Params[2] := CodigoMoneda;
  Params[3] := CodigoInmueble;
  Params[4] := VarFromDateTime(FechaIngreso);
  Params[5] := VarFromDateTime(FechaServicio);
  Params[6] := Concepto;
  Params[7] := Observaciones;
  Params[8] := MontoNeto;
  Params[9] := MontoDescuento;
  Params[10] := SubTotal;
  Params[11] := Impuesto;
  Params[12] := Total;
  Params[13] := PDescuento;
  Params[14] := PIva;
  Params[15] := LoginUsuario;
  Params[16] := VarFromDateTime(TiempoIngreso);
  Params[17] := IdCondominioServicio;
  Result := Params;
end;

function TCondominioServicio.GetCampos: TArraystring;
var
  Campos: TArraystring;
begin
  SetLength(Campos,7);
  Campos[0] := 'numero';
  Campos[1] := 'codigo_conjunto_administracion';
  Campos[2] := 'codigo_moneda';
  Campos[3] := 'codigo_inmueble';
  Campos[4] := 'concepto';
  Campos[5] := 'observaciones';
  Campos[6] := 'login_usuario';
  Result := Campos;
end;

function TCondominioServicio.GetCondicion: string;
begin
  Result:= '"id_condominio_servicio" =' + IntToStr(IdCondominioServicio);
end;

procedure TCondominioServicio.AntesInsertar;
var
  Consecutivo: TConsecutivo;
begin
  Consecutivo := TConsecutivo.Create;
  Numero := IntToStr(Consecutivo.ObtenerConsecutivo('numero_condominio_servicio'));
  IdCondominioServicio := Consecutivo.ObtenerConsecutivo('id_condominio_servicio');
end;

procedure TCondominioServicio.AntesModificar;
begin

end;

procedure TCondominioServicio.AntesEliminar;
begin

end;

procedure TCondominioServicio.DespuesInsertar;
begin

end;

procedure TCondominioServicio.DespuesModificar;
begin

end;

procedure TCondominioServicio.DespuesEliminar;
begin

end;

constructor TCondominioServicio.Create;
var
  ClavesPrimarias: TArraystring;
begin
  inherited Create;
  SetTabla('tbl_condominio_servicio');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(18)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "numero"=$1, "codigo_conjunto_administracion"=$2, "codigo_moneda"=$3, "codigo_inmueble"=$4, "fecha_ingreso"=$5, "fecha_servicio"=$6, "concepto"=$7, "observaciones"=$8, "monto_neto"=$9, "monto_descuento"=$10, ' +
                  '"sub_total"=$11, "impuesto"=$12, "total"=$13, "p_descuento"=$14, "p_iva"=$15, "login_usuario"=$16, "tiempo_ingreso"=$17 WHERE ("id_condominio_servicio"=$18)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("id_condominio_servicio"=$18)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"id_condominio_servicio"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TCondominioServicio.SetNumero(varNumero: string);
begin
  Numero := varNumero;
end;

procedure TCondominioServicio.SetCodigoConjuntoAdministracion(varCodigoConjuntoAdministracion: string);
begin
  CodigoConjuntoAdministracion := varCodigoConjuntoAdministracion;
end;

procedure TCondominioServicio.SetCodigoMoneda(varCodigoMoneda: string);
begin
  CodigoMoneda := varCodigoMoneda;
end;

procedure TCondominioServicio.SetCodigoInmueble(varCodigoInmueble: string);
begin
  CodigoInmueble := varCodigoInmueble;
end;

procedure TCondominioServicio.SetFechaIngreso(varFechaIngreso: TDate);
begin
  FechaIngreso := varFechaIngreso;
end;

procedure TCondominioServicio.SetFechaServicio(varFechaServicio: TDate);
begin
  FechaServicio := varFechaServicio;
end;

procedure TCondominioServicio.SetConcepto(varConcepto: string);
begin
  Concepto := varConcepto;
end;

procedure TCondominioServicio.SetObservaciones(varObservaciones: string);
begin
  Observaciones := varObservaciones;
end;

procedure TCondominioServicio.SetMontoNeto(varMontoNeto: Double);
begin
  MontoNeto := varMontoNeto;
end;

procedure TCondominioServicio.SetMontoDescuento(varMontoDescuento: Double);
begin
  MontoDescuento := varMontoDescuento;
end;

procedure TCondominioServicio.SetSubTotal(varSubTotal: Double);
begin
  SubTotal := varSubTotal;
end;

procedure TCondominioServicio.SetImpuesto(varImpuesto: Double);
begin
  Impuesto := varImpuesto;
end;

procedure TCondominioServicio.SetTotal(varTotal: Double);
begin
  Total := varTotal;
end;

procedure TCondominioServicio.SetPDescuento(varPDescuento: Double);
begin
  PDescuento := varPDescuento;
end;

procedure TCondominioServicio.SetPIva(varPIva: Double);
begin
  PIva := varPIva;
end;

procedure TCondominioServicio.SetLoginUsuario(varLoginUsuario: string);
begin
  LoginUsuario := varLoginUsuario;
end;

procedure TCondominioServicio.SetTiempoIngreso(varTiempodeIngreso: TDateTime);
begin
  TiempoIngreso := varTiempodeIngreso;
end;

procedure TCondominioServicio.SetIdCondominioServicio(varIdCondominioServicio: Integer);
begin
  IdCondominioServicio := varIdCondominioServicio;
end;

function TCondominioServicio.GetNumero: string;
begin
  Result := Numero;
end;

function TCondominioServicio.GetCodigoConjuntoAdministracion: string;
begin
  Result := CodigoConjuntoAdministracion;
end;

function TCondominioServicio.GetCodigoMoneda: string;
begin
  Result := CodigoMoneda;
end;

function TCondominioServicio.GetCodigoInmueble: string;
begin
  Result := CodigoInmueble;
end;

function TCondominioServicio.GetFechaIngreso: TDate;
begin
  Result := FechaIngreso;
end;

function TCondominioServicio.GetFechaServicio: TDate;
begin
  Result := FechaServicio;
end;

function TCondominioServicio.GetConcepto: string;
begin
  Result := Concepto;
end;

function TCondominioServicio.GetObservaciones: string;
begin
  Result := Observaciones;
end;

function TCondominioServicio.GetMontoNeto: Double;
begin
  Result := MontoNeto;
end;

function TCondominioServicio.GetMontoDescuento: Double;
begin
  Result := MontoDescuento;
end;

function TCondominioServicio.GetSubTotal: Double;
begin
  Result := SubTotal;
end;

function TCondominioServicio.GetImpuesto: Double;
begin
  Result := Impuesto;
end;

function TCondominioServicio.GetTotal: Double;
begin
  Result := Total;
end;

function TCondominioServicio.GetPDescuento: Double;
begin
  Result := PDescuento;
end;

function TCondominioServicio.GetPIva: Double;
begin
  Result := PIva;
end;

function TCondominioServicio.GetLoginUsuario: string;
begin
  Result := LoginUsuario;
end;

function TCondominioServicio.GetTiempoIngreso: TDateTime;
begin
  Result := TiempoIngreso;
end;

function TCondominioServicio.GetIdCondominioServicio: Integer;
begin
  Result := IdCondominioServicio;
end;

function TCondominioServicio.BuscarNumero: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("numero" = ' + QuotedStr(GetNumero) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

function TCondominioServicio.BuscarCodigoInmueble: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("codigo_inmueble" = ' + QuotedStr(GetCodigoInmueble) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

end.
