unit ClsContablePeriodoEstado;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TContablePeriodoEstado }

  TContablePeriodoEstado = class(TGenericoBD)
    private
      Anno: Integer;
      Mes: Integer;
      FechaHoraEstado: TDateTime;
      LoginUsuarioEstado: string;
      Observaciones: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetAnno(varAnno: Integer);
      procedure SetMes(varMes: Integer);
      procedure SetFechaHoraEstado(varFechaHoraEstado: TDateTime);
      procedure SetLoginUsuarioEstado(varLoginUsuarioEstado: string);
      procedure SetObservaciones(varObservaciones: string);
      function GetAnno: Integer;
      function GetMes: Integer;
      function GetFechaHoraEstado: TDateTime;
      function GetLoginUsuarioEstado: string;
      function GetObservaciones: string;
  end;

var
  ContablePeriodoEstado: TContablePeriodoEstado;

implementation

uses DB;

{ TContablePeriodoEstado }

procedure TContablePeriodoEstado.Ajustar;
begin

end;

procedure TContablePeriodoEstado.Mover(Ds: TClientDataSet);
begin
  Anno := Ds.FieldValues['anno'];
  Mes := Ds.FieldValues['mes'];
  FechaHoraEstado := Ds.FieldByName('fecha_hora_estado').AsDateTime;
  LoginUsuarioEstado := Ds.FieldValues['login_usuario_estado'];
  Observaciones := Ds.FieldValues['observaciones'];
end;

function TContablePeriodoEstado.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,5);
  Params[0] := Anno;
  Params[1] := Mes;
  Params[2] := VarFromDateTime(FechaHoraEstado);
  Params[3] := LoginUsuarioEstado;
  Params[4] := Observaciones;
  Result := Params;
end;

function TContablePeriodoEstado.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,2);
  Campos[0] := 'login_usuario_estado';
  Campos[1] := 'observaciones';
  Result := Campos;
end;

function TContablePeriodoEstado.GetCondicion: string;
begin
  Result := '"anno" = ' + IntToStr(Anno) + ' AND ' +
            '"mes" = ' + IntToStr(Mes) + ' AND ' +
            '"fecha_hora_estado" = ' + QuotedStr(FormatDateTime('yyyy-mm-dd hh:nn:ss',FechaHoraEstado));
end;

procedure TContablePeriodoEstado.AntesEliminar;
begin

end;

procedure TContablePeriodoEstado.AntesInsertar;
begin

end;

procedure TContablePeriodoEstado.AntesModificar;
begin

end;

procedure TContablePeriodoEstado.DespuesEliminar;
begin

end;

procedure TContablePeriodoEstado.DespuesInsertar;
begin

end;

procedure TContablePeriodoEstado.DespuesModificar;
begin

end;

constructor TContablePeriodoEstado.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_contable_periodo_estado');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(5)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "login_usuario_estado"=$4, "observaciones"=$5 WHERE ("anno"=$1) AND ("mes"=$2) AND ("fecha_hora_estado=$3)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("anno"=$1) AND ("mes"=$2) AND ("fecha_hora_estado=$3)');
  SetLength(ClavesPrimarias,3);
  ClavesPrimarias[0] := '"anno"';
  ClavesPrimarias[1] := '"mes"';
  ClavesPrimarias[2] := '"fecha_hora_estado"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TContablePeriodoEstado.SetAnno(varAnno: Integer);
begin
  Anno := varAnno;
end;

procedure TContablePeriodoEstado.SetMes(varMes: Integer);
begin
  Mes := varMes;
end;

procedure TContablePeriodoEstado.SetFechaHoraEstado(varFechaHoraEstado: TDateTime);
begin
  FechaHoraEstado := varFechaHoraEstado;
end;

procedure TContablePeriodoEstado.SetLoginUsuarioEstado(varLoginUsuarioEstado: string);
begin
  LoginUsuarioEstado := varLoginUsuarioEstado;
end;

procedure TContablePeriodoEstado.SetObservaciones(varObservaciones: string);
begin
  Observaciones := varObservaciones;
end;

function TContablePeriodoEstado.GetAnno: Integer;
begin
  Result := Anno;
end;

function TContablePeriodoEstado.GetMes: Integer;
begin
  Result := Mes;
end;

function TContablePeriodoEstado.GetFechaHoraEstado: TDateTime;
begin
  Result := FechaHoraEstado;
end;

function TContablePeriodoEstado.GetLoginUsuarioEstado: string;
begin
  Result := LoginUsuarioEstado;
end;

function TContablePeriodoEstado.GetObservaciones: string;
begin
  Result := Observaciones;
end;


end.

