unit ClsBusquedaDocumentoCuentaxCobrar;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TBusquedaDocumentoCuentaxCobrar }

  TBusquedaDocumentoCuentaxCobrar = class(TGenericoBD)
    private
      Desde: string;
      NumeroDocumento: string;
      CodigoCliente: string;
      Rif: string;
      Nit: string;
      RazonSocial: string;
      Contacto: string;
      FechaIngreso: TDate;
      NumeroCuota: Integer;
      TotalCuotas: Integer;
      MontoNeto: Double;
      Impuesto: Double;
      Total: Double;
      MontoNetoDocumento: Double;
      ImpuestoDocumento: Double;
      TotalDocumento: Double;
      IdCobranza: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create; overload;
      procedure SetDesde(varDesde: string);
      procedure SetNumeroDocumento(varNumeroDocumento: string);
      procedure SetCodigoCliente(varCodigoCliente: string);
      procedure SetNumeroCuota(varNumeroCuota: Integer);
      function GetDesde: string;
      function GetNumeroDocumento: string;
      function GetCodigoCliente: string;
      function GetRif: string;
      function GetNit: string;
      function GetRazonSocial: string;
      function GetContacto: string;
      function GetFechaIngreso: TDate;
      function GetNumeroCuota: Integer;
      function GetTotalCuotas: Integer;
      function GetMontoNeto: Double;
      function GetImpuesto: Double;
      function GetTotal: Double;
      function GetMontoNetoDocumento: Double;
      function GetImpuestoDocumento: Double;
      function GetTotalDocumento: Double;
      function GetIdCobranza: Integer;
  end;

implementation

{ TBusquedaDocumentoCuentaxCobrar }

procedure TBusquedaDocumentoCuentaxCobrar.Ajustar;
begin

end;

procedure TBusquedaDocumentoCuentaxCobrar.Mover(Ds: TClientDataSet);
begin
  Desde := Ds.FieldValues['desde'];
  NumeroDocumento := Ds.FieldValues['numero_documento'];
  CodigoCliente := Ds.FieldValues['codigo_cliente'];
  Rif := Ds.FieldValues['rif'];
  Nit := Ds.FieldValues['nit'];
  RazonSocial := Ds.FieldValues['razon_social'];
  Contacto := Ds.FieldValues['contacto'];
  FechaIngreso := Ds.FieldValues['fecha_ingreso'];
  NumeroCuota := Ds.FieldValues['numero_cuota'];
  TotalCuotas := Ds.FieldValues['total_cuotas'];
  MontoNeto := Ds.FieldValues['monto_neto'];
  Impuesto := Ds.FieldValues['impuesto'];
  Total := Ds.FieldValues['total'];
  MontoNetoDocumento := Ds.FieldValues['monto_neto_documento'];
  ImpuestoDocumento := Ds.FieldValues['impuesto_documento'];
  TotalDocumento := Ds.FieldValues['total_documento'];
  IdCobranza := Ds.FieldValues['id_cobranza'];
end;

function TBusquedaDocumentoCuentaxCobrar.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,17);
  Params[0] := Desde;
  Params[1] := NumeroDocumento;
  Params[2] := CodigoCliente;
  Params[3] := Rif;
  Params[4] := Nit;
  Params[5] := RazonSocial;
  Params[6] := Contacto;
  Params[7] := FechaIngreso;
  Params[8] := NumeroCuota;
  Params[9] := TotalCuotas;
  Params[10] := MontoNeto;
  Params[11] := Impuesto;
  Params[12] := Total;
  Params[13] := MontoNetoDocumento;
  Params[14] := ImpuestoDocumento;
  Params[15] := TotalDocumento;
  Params[16] := IdCobranza;
  Result := Params;
end;

function TBusquedaDocumentoCuentaxCobrar.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,8);
  Campos[0] := 'desde';
  Campos[1] := 'numero_documento';
  Campos[2] := 'numero_cuota';
  Campos[3] := 'codigo_cliente';
  Campos[4] := 'rif';
  Campos[5] := 'nit';
  Campos[6] := 'razon_social';
  Campos[7] := 'contacto';
  Result := Campos;
end;

function TBusquedaDocumentoCuentaxCobrar.GetCondicion: string;
begin
  Result:= '("desde" = ' + QuotedStr(Desde) + ') AND ' +
           '("numero_documento" = ' + QuotedStr(NumeroDocumento) + ') AND ' +
           '("codigo_cliente" = ' + QuotedStr(CodigoCliente) + ') AND ' +
           '("numero_cuota" = ' + IntToStr(NumeroCuota) + ')';
end;

procedure TBusquedaDocumentoCuentaxCobrar.AntesInsertar;
begin

end;

procedure TBusquedaDocumentoCuentaxCobrar.AntesModificar;
begin

end;

procedure TBusquedaDocumentoCuentaxCobrar.AntesEliminar;
begin

end;

procedure TBusquedaDocumentoCuentaxCobrar.DespuesInsertar;
begin

end;

procedure TBusquedaDocumentoCuentaxCobrar.DespuesModificar;
begin

end;

procedure TBusquedaDocumentoCuentaxCobrar.DespuesEliminar;
begin

end;

constructor TBusquedaDocumentoCuentaxCobrar.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('v_busqueda_documentos_cuentas_x_cobrar');
  SetLength(ClavesPrimarias,0);
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TBusquedaDocumentoCuentaxCobrar.SetCodigoCliente(
  varCodigoCliente: string);
begin
  CodigoCliente := varCodigoCliente;
end;

procedure TBusquedaDocumentoCuentaxCobrar.SetDesde(varDesde: string);
begin
  Desde := varDesde;
end;

procedure TBusquedaDocumentoCuentaxCobrar.SetNumeroDocumento(varNumeroDocumento: string);
begin
  NumeroDocumento := varNumeroDocumento;
end;

procedure TBusquedaDocumentoCuentaxCobrar.SetNumeroCuota(
  varNumeroCuota: Integer);
begin
  NumeroCuota := varNumeroCuota;
end;

function TBusquedaDocumentoCuentaxCobrar.GetCodigoCliente: string;
begin
  Result := CodigoCliente;
end;

function TBusquedaDocumentoCuentaxCobrar.GetContacto: string;
begin
  Result := Contacto;
end;

function TBusquedaDocumentoCuentaxCobrar.GetDesde: string;
begin
  Result := Desde;
end;

function TBusquedaDocumentoCuentaxCobrar.GetFechaIngreso: TDate;
begin
  Result := FechaIngreso;
end;

function TBusquedaDocumentoCuentaxCobrar.GetImpuesto: Double;
begin
  Result := Impuesto;
end;

function TBusquedaDocumentoCuentaxCobrar.GetImpuestoDocumento: Double;
begin
  Result := ImpuestoDocumento; 
end;

function TBusquedaDocumentoCuentaxCobrar.GetMontoNeto: Double;
begin
  Result := MontoNeto;
end;

function TBusquedaDocumentoCuentaxCobrar.GetMontoNetoDocumento: Double;
begin
  Result := MontoNetoDocumento
end;

function TBusquedaDocumentoCuentaxCobrar.GetNit: string;
begin
  Result := Nit;
end;

function TBusquedaDocumentoCuentaxCobrar.GetNumeroCuota: Integer;
begin
  Result := NumeroCuota;
end;

function TBusquedaDocumentoCuentaxCobrar.GetNumeroDocumento: string;
begin
  Result := NumeroDocumento;
end;

function TBusquedaDocumentoCuentaxCobrar.GetRazonSocial: String;
begin
  Result := RazonSocial;
end;

function TBusquedaDocumentoCuentaxCobrar.GetRif: String;
begin
  Result := Rif;
end;

function TBusquedaDocumentoCuentaxCobrar.GetTotal: Double;
begin
  Result := Total;
end;

function TBusquedaDocumentoCuentaxCobrar.GetTotalCuotas: Integer;
begin
  Result := TotalCuotas;
end;

function TBusquedaDocumentoCuentaxCobrar.GetTotalDocumento: Double;
begin
  Result := TotalDocumento;
end;

function TBusquedaDocumentoCuentaxCobrar.GetIdCobranza: Integer;
begin
  Result := IdCobranza;
end;

end.

