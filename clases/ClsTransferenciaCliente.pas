unit ClsTransferenciaCliente;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TTransferenciaCliente }

  TTransferenciaCliente = class(TGenericoBD)
    private
      Numero: string;
      CodigoMoneda: string;
      CodigoCliente: string;
      CodigoZona: string;
      FechaIngreso: TDate;
      FechaContable: TDate;
      Concepto: string;
      Observaciones: string;
      Total: Double;
      LoginUsuario: string;
      TiempoIngreso: TDateTime;
      IdTransferenciaCliente: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArraystring; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetNumero(varNumero: string);
      procedure SetCodigoMoneda(varCodigoMoneda: string);
      procedure SetCodigoCliente(varCodigoCliente: string);
      procedure SetCodigoZona(varCodigoZona: string);
      procedure SetFechaIngreso(varFechaIngreso: TDate);
      procedure SetFechaContable(varFechaContable: TDate);
      procedure SetConcepto(varConcepto: string);
      procedure SetObservaciones(varObservaciones: string);
      procedure SetTotal(varTotal: Double);
      procedure SetLoginUsuario(varLoginUsuario: string);
      procedure SetTiempoIngreso(varTiempodeIngreso: TDateTime);
      procedure SetIdTransferenciaCliente(varIdTransferenciaCliente: Integer);
      function GetNumero: string;
      function GetCodigoMoneda: string;
      function GetCodigoCliente: string;
      function GetCodigoZona: string;
      function GetFechaIngreso: TDate;
      function GetFechaContable: TDate;
      function GetConcepto: string;
      function GetObservaciones: string;
      function GetTotal: Double;
      function GetLoginUsuario: string;
      function GetTiempoIngreso: TDateTime;
      function GetIdTransferenciaCliente: Integer;
      function BuscarNumero: Boolean;
  end;

implementation

uses
  DB,
  ClsConsecutivo;

{ TTransferenciaCliente }

procedure TTransferenciaCliente.Ajustar;
begin

end;

procedure TTransferenciaCliente.Mover(Ds: TClientDataSet);
begin
  Numero := Ds.FieldValues['numero'];
  CodigoMoneda := Ds.FieldValues['codigo_moneda'];
  CodigoCliente := Ds.FieldValues['codigo_cliente'];
  CodigoZona := Ds.FieldValues['codigo_zona'];
  FechaIngreso := Ds.FieldByName('fecha_ingreso').AsDateTime;
  FechaContable := Ds.FieldByName('fecha_contable').AsDateTime;
  Concepto := Ds.FieldValues['concepto'];
  Observaciones := Ds.FieldValues['observaciones'];
  Total := Ds.FieldValues['total'];
  LoginUsuario := Ds.FieldValues['login_usuario'];
  TiempoIngreso := Ds.FieldByName('tiempo_ingreso').AsDateTime;
  IdTransferenciaCliente := Ds.FieldValues['id_transferencia_cliente'];
end;

function TTransferenciaCliente.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,12);
  Params[0] := Numero;
  Params[1] := CodigoMoneda;
  Params[2] := CodigoCliente;
  Params[3] := CodigoZona;
  Params[4] := VarFromDateTime(FechaIngreso);
  Params[5] := VarFromDateTime(FechaContable);
  Params[6] := Concepto;
  Params[7] := Observaciones;
  Params[8] := Total;
  Params[9] := LoginUsuario;
  Params[10] := VarFromDateTime(TiempoIngreso);
  Params[11] := IdTransferenciaCliente;
  Result := Params;
end;

function TTransferenciaCliente.GetCampos: TArraystring;
var
  Campos: TArraystring;
begin
  SetLength(Campos,7);
  Campos[0] := 'numero';
  Campos[1] := 'codigo_moneda';
  Campos[2] := 'codigo_cliente';
  Campos[3] := 'codigo_zona';
  Campos[4] := 'concepto';
  Campos[5] := 'observaciones';
  Campos[6] := 'login_usuario';
  Result := Campos;
end;

function TTransferenciaCliente.GetCondicion: string;
begin
  Result:= '"id_transferencia_cliente" =' + IntToStr(IdTransferenciaCliente);
end;

procedure TTransferenciaCliente.AntesInsertar;
var
  Consecutivo: TConsecutivo;
begin
  Consecutivo := TConsecutivo.Create;
  Numero := IntToStr(Consecutivo.ObtenerConsecutivo('numero_transferencia_cliente'));
  IdTransferenciaCliente := Consecutivo.ObtenerConsecutivo('id_transferencia_cliente');
end;

procedure TTransferenciaCliente.AntesModificar;
begin

end;

procedure TTransferenciaCliente.AntesEliminar;
begin

end;

procedure TTransferenciaCliente.DespuesInsertar;
begin

end;

procedure TTransferenciaCliente.DespuesModificar;
begin

end;

procedure TTransferenciaCliente.DespuesEliminar;
begin

end;

constructor TTransferenciaCliente.Create;
var
  ClavesPrimarias: TArraystring;
begin
  inherited Create;
  SetTabla('tbl_transferencias_clientes');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(12)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "numero"=$1, "codigo_moneda"=$2, "codigo_cliente"=$3, "codigo_zona"=$4, "fecha_ingreso"=$5, "fecha_contable"=$6, "concepto"=$7, "observaciones"=$8, "total"=$9, "login_usuario"=$10, ' +
                  '"tiempo_ingreso"=$11' +
                  ' WHERE ("id_transferencia_cliente"=$12)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("id_transferencia_cliente"=$12)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"id_transferencia_cliente"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TTransferenciaCliente.SetNumero(varNumero: string);
begin
  Numero := varNumero;
end;

procedure TTransferenciaCliente.SetCodigoMoneda(varCodigoMoneda: string);
begin
  CodigoMoneda := varCodigoMoneda;
end;

procedure TTransferenciaCliente.SetCodigoCliente(varCodigoCliente: string);
begin
 CodigoCliente := varCodigoCliente;
end;

procedure TTransferenciaCliente.SetCodigoZona(varCodigoZona: string);
begin
  CodigoZona := varCodigoZona;
end;

procedure TTransferenciaCliente.SetFechaIngreso(varFechaIngreso: TDate);
begin
  FechaIngreso := varFechaIngreso;
end;

procedure TTransferenciaCliente.SetFechaContable(varFechaContable: TDate);
begin
  FechaContable := varFechaContable;
end;

procedure TTransferenciaCliente.SetConcepto(varConcepto: string);
begin
  Concepto := varConcepto;
end;

procedure TTransferenciaCliente.SetObservaciones(varObservaciones: string);
begin
  Observaciones := varObservaciones;
end;

procedure TTransferenciaCliente.SetTotal(varTotal: Double);
begin
  Total := varTotal;
end;

procedure TTransferenciaCliente.SetLoginUsuario(varLoginUsuario: string);
begin
  LoginUsuario := varLoginUsuario;
end;

procedure TTransferenciaCliente.SetTiempoIngreso(varTiempodeIngreso: TDateTime);
begin
  TiempoIngreso := varTiempodeIngreso;
end;

procedure TTransferenciaCliente.SetIdTransferenciaCliente(varIdTransferenciaCliente: Integer);
begin
  IdTransferenciaCliente := varIdTransferenciaCliente;
end;

function TTransferenciaCliente.GetNumero: string;
begin
  Result := Numero;
end;

function TTransferenciaCliente.GetCodigoMoneda: string;
begin
  Result := CodigoMoneda;
end;

function TTransferenciaCliente.GetCodigoCliente: string;
begin
  Result := CodigoCliente
end;

function TTransferenciaCliente.GetCodigoZona: string;
begin
  Result := CodigoZona;
end;

function TTransferenciaCliente.GetFechaIngreso: TDate;
begin
  Result := FechaIngreso;
end;

function TTransferenciaCliente.GetFechaContable: TDate;
begin
  Result := FechaContable;
end;

function TTransferenciaCliente.GetConcepto: string;
begin
  Result := Concepto;
end;

function TTransferenciaCliente.GetObservaciones: string;
begin
  Result := Observaciones;
end;

function TTransferenciaCliente.GetTotal: Double;
begin
  Result := Total;
end;

function TTransferenciaCliente.GetLoginUsuario: string;
begin
  Result := LoginUsuario;
end;

function TTransferenciaCliente.GetTiempoIngreso: TDateTime;
begin
  Result := TiempoIngreso;
end;

function TTransferenciaCliente.GetIdTransferenciaCliente: Integer;
begin
  Result := IdTransferenciaCliente;
end;

function TTransferenciaCliente.BuscarNumero: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("numero" = ' + QuotedStr(GetNumero) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

end.
