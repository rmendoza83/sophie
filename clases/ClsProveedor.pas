unit ClsProveedor;
interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TProveedor }

  TProveedor = class(TGenericoBD)
    private
      Codigo: string;
      PrefijoRif: string;
      Rif: string;
      Nit: string;
      RazonSocial: string;
      DireccionFiscal1: string;
      DireccionFiscal2: string;
      DireccionDomicilio1: string;
      DireccionDomicilio2: string;
      Telefonos: string;
      Fax: string;
      Email: string;
      Website: string;
      FechaIngreso: TDate;
      Contacto: string;
      Observaciones: string;
      CodigoTipoProveedor: string;
      CodigoZona: string;
      CodigoEsquemaPago: string;
      CodigoTipoRetencionIva: string;
      UniqueId: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArraystring; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigo(varCodigo: string);
      procedure SetPrefijoRif(varPrefijoRif: string);
      procedure SetRif(varRif: string);
      procedure SetNit(varNit: string);
      procedure SetRazonSocial(varRazonSocial: string);
      procedure SetDireccionFiscal1(varDireccionFiscal1: string);
      procedure SetDireccionFiscal2(varDireccionFiscal2: string);
      procedure SetDireccionDomicilio1(varDireccionDomicilio1: string);
      procedure SetDireccionDomicilio2(varDireccionDomicilio2: string);
      procedure SetTelefonos(varTelefonos: string);
      procedure SetFax(varFax: string);
      procedure SetEmail(varEmail: string);
      procedure SetWebsite(varWebsite: string);
      procedure SetFechaIngreso(varFechaIngreso: TDate);
      procedure SetContacto(varContacto: string);
      procedure SetObservaciones(varObservaciones: string);
      procedure SetCodigoTipoProveedor(varCodigoTipoProveedor: string);
      procedure SetCodigoZona(varCodigoZona: string);
      procedure SetCodigoEsquemaPago(varCodigoEsquemaPago: string);
      procedure SetCodigoTipoRetencionIva(varCodigoTipoRetencionIva: string);
      procedure SetUniqueId(varUniqueId: Integer);
      function GetCodigo: string;
      function GetPrefijoRif: string;
      function GetRif: string;
      function GetNit: string;
      function GetRazonSocial: string;
      function GetDireccionFiscal1: string;
      function GetDireccionFiscal2: string;
      function GetDireccionDomicilio1: string;
      function GetDireccionDomicilio2: string;
      function GetTelefonos: string;
      function GetFax: string;
      function GetEmail: string;
      function GetWebsite: string;
      function GetFechaIngreso: TDate;
      function GetContacto: string;
      function GetObservaciones: string;
      function GetCodigoTipoProveedor: string;
      function GetCodigoZona: string;
      function GetCodigoEsquemaPago: string;
      function GetCodigoTipoRetencionIva: string;
      function GetUniqueId: Integer;
      function ObtenerCombo: TClientDataSet;
      function BuscarCodigo: Boolean;
  end;

implementation

uses
  DB, ClsConsecutivo;

{ TProveedor }

procedure TProveedor.Ajustar;
begin

end;

procedure TProveedor.Mover(Ds: TClientDataSet);
begin
  Codigo := Ds.FieldValues['codigo'];
  PrefijoRif := Ds.FieldValues['prefijo_rif'];
  Rif := Ds.FieldValues['rif'];
  Nit := Ds.FieldValues['nit'];
  RazonSocial := Ds.FieldValues['razon_social'];
  DireccionFiscal1 := Ds.FieldValues['direccion_fiscal_1'];
  DireccionFiscal2 := Ds.FieldValues['direccion_fiscal_2'];
  DireccionDomicilio1 := Ds.FieldValues['direccion_domicilio_1'];
  DireccionDomicilio2 := Ds.FieldValues['direccion_domicilio_2'];
  Telefonos := Ds.FieldValues['telefonos'];
  Fax := Ds.FieldValues['fax'];
  Email := Ds.FieldValues['email'];
  Website := Ds.FieldValues['website'];
  FechaIngreso := Ds.FieldByName('fecha_ingreso').AsDateTime;
  Contacto := Ds.FieldValues['contacto'];
  Observaciones := Ds.FieldValues['observaciones'];
  CodigoTipoProveedor:= Ds.FieldValues['codigo_tipo_proveedor'];
  CodigoZona := Ds.FieldValues['codigo_zona'];
  CodigoEsquemaPago := Ds.FieldValues['codigo_esquema_pago'];
  CodigoTipoRetencionIva := Ds.FieldValues['codigo_tipo_retencion_iva'];
  UniqueId := Ds.FieldValues['unique_id'];
end;

function TProveedor.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,21);
  Params[0] := Codigo;
  Params[1] := PrefijoRif;
  Params[2] := Rif;
  Params[3] := Nit;
  Params[4] := RazonSocial;
  Params[5] := DireccionFiscal1;
  Params[6] := DireccionFiscal2;
  Params[7] := DireccionDomicilio1;
  Params[8] := DireccionDomicilio2;
  Params[9] := Telefonos;
  Params[10] := Fax;
  Params[11] := Email;
  Params[12] := Website;
  Params[13] := VarToDateTime(FechaIngreso);
  Params[14] := Contacto;
  Params[15] := Observaciones;
  Params[16] := CodigoTipoProveedor;
  Params[17] := CodigoZona;
  Params[18] := CodigoEsquemaPago;
  Params[19] := CodigoTipoRetencionIva;
  Params[20] := UniqueId;
  Result := Params;
end;

function TProveedor.GetCampos: TArraystring;
var
  Campos: TArraystring;
begin
  SetLength(Campos,18);
  Campos[0] := 'codigo';
  Campos[1] := 'prefijo_rif';
  Campos[2] := 'rif';
  Campos[3] := 'nit';
  Campos[4] := 'razon_social';
  Campos[5] := 'direccion_fiscal_1';
  Campos[6] := 'direccion_fiscal_2';
  Campos[7] := 'direccion_domicilio_1';
  Campos[8] := 'direccion_domicilio_2';
  Campos[9] := 'telefonos';
  Campos[10] := 'fax';
  Campos[11] := 'email';
  Campos[12] := 'website';
  Campos[13] := 'contacto';
  Campos[14] := 'observaciones';
  Campos[15] := 'codigo_tipo_proveedor';
  Campos[16] := 'codigo_zona';
  Campos[17] := 'codigo_esquema_pago';
  Campos[17] := 'codigo_tipo_retencion_iva';
  Result := Campos;
end;

function TProveedor.GetCondicion: string;
begin
  Result:= '"unique_id" = ' + IntToStr(UniqueId);
end;

procedure TProveedor.AntesInsertar;
var
  Consecutivo: TConsecutivo;
begin
  Consecutivo := TConsecutivo.Create;
  UniqueId := Consecutivo.ObtenerConsecutivo('id_proveedores');
end;

procedure TProveedor.AntesModificar;
begin

end;

procedure TProveedor.AntesEliminar;
begin

end;

procedure TProveedor.DespuesInsertar;
begin

end;

procedure TProveedor.DespuesModificar;
begin

end;

procedure TProveedor.DespuesEliminar;
begin

end;

constructor TProveedor.Create;
var
  ClavesPrimarias: TArraystring;
begin
  inherited Create;
  SetTabla('tbl_proveedores');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(21)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "codigo"=$1, "prefijo_rif"=$2, "rif"=$3, "nit"=$4, "razon_social"=$5, "direccion_fiscal_1"=$6, "direccion_fiscal_2"=$7, ' +
                  '"direccion_domicilio_1"=$8, "direccion_domicilio_2"=$9, "telefonos"=$10, "fax"=$11, "email"=$12, "website"=$13, "fecha_ingreso"=$14, "contacto"=$15, "observaciones"=$16, ' +
                  '"codigo_tipo_proveedor"=$17, "codigo_zona"=$18, "codigo_esquema_pago"=$19, "codigo_tipo_retencion_iva"=$20 ' +
                  'WHERE ("unique_id"=$21)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("unique_id"=$21)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"unique_id"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TProveedor.SetCodigo(varCodigo: string);
begin
  Codigo := varCodigo;
end;

procedure TProveedor.SetCodigoEsquemaPago(varCodigoEsquemaPago: string);
begin
  CodigoEsquemaPago := varCodigoEsquemaPago;
end;

procedure TProveedor.SetCodigoTipoProveedor(varCodigoTipoProveedor: string);
begin
  CodigoTipoProveedor := varCodigoTipoProveedor;
end;

procedure TProveedor.SetCodigoZona(varCodigoZona: string);
begin
  CodigoZona := varCodigoZona;
end;

procedure TProveedor.SetContacto(varContacto: string);
begin
  Contacto := varContacto;
end;

procedure TProveedor.SetDireccionDomicilio1(varDireccionDomicilio1: string);
begin
  DireccionDomicilio1 := varDireccionDomicilio1;
end;

procedure TProveedor.SetDireccionDomicilio2(varDireccionDomicilio2: string);
begin
  DireccionDomicilio2 := varDireccionDomicilio2;
end;

procedure TProveedor.SetDireccionFiscal1(varDireccionFiscal1: string);
begin
  DireccionFiscal1 := varDireccionFiscal1;
end;

procedure TProveedor.SetDireccionFiscal2(varDireccionFiscal2: string);
begin
  DireccionFiscal2 := varDireccionFiscal2;
end;

procedure TProveedor.SetEmail(varEmail: string);
begin
  Email := varEmail;
end;

procedure TProveedor.SetFax(varFax: string);
begin
  Fax := varFax;
end;

procedure TProveedor.SetFechaIngreso(varFechaIngreso: TDate);
begin
  FechaIngreso := varFechaIngreso;
end;

procedure TProveedor.SetNit(varNit: string);
begin
  Nit := varNit;
end;

procedure TProveedor.SetObservaciones(varObservaciones: string);
begin
  Observaciones := varObservaciones;
end;

procedure TProveedor.SetCodigoTipoRetencionIva(varCodigoTipoRetencionIva: string);
begin
  CodigoTipoRetencionIva := varCodigoTipoRetencionIva;
end;

procedure TProveedor.SetPrefijoRif(varPrefijoRif: string);
begin
  PrefijoRif := varPrefijoRif;
end;

procedure TProveedor.SetRazonSocial(varRazonSocial: string);
begin
  RazonSocial := varRazonSocial;
end;

procedure TProveedor.SetRif(varRif: string);
begin
  Rif := varRif;
end;

procedure TProveedor.SetTelefonos(varTelefonos: string);
begin
  Telefonos := varTelefonos;
end;

procedure TProveedor.SetUniqueId(varUniqueId: Integer);
begin
  UniqueId := varUniqueId;
end;

procedure TProveedor.SetWebsite(varWebsite: string);
begin
  Website := varWebsite;
end;

function TProveedor.GetCodigo: string;
begin
  Result := Codigo;
end;

function TProveedor.GetPrefijoRif: string;
begin
  Result := PrefijoRif;
end;

function TProveedor.GetRif: string;
begin
  Result := Rif
end;

function TProveedor.GetNit: string;
begin
  Result := Nit;
end;

function TProveedor.GetRazonSocial: string;
begin
  Result := RazonSocial;
end;

function TProveedor.GetDireccionFiscal1: string;
begin
  Result := DireccionFiscal1;
end;

function TProveedor.GetDireccionFiscal2: string;
begin
  Result := DireccionFiscal2;
end;

function TProveedor.GetDireccionDomicilio1: string;
begin
  Result := DireccionDomicilio1;
end;

function TProveedor.GetDireccionDomicilio2: string;
begin
  Result := DireccionDomicilio2;
end;

function TProveedor.GetTelefonos: string;
begin
  Result := Telefonos;
end;

function TProveedor.GetFax: string;
begin
  Result := Fax;
end;

function TProveedor.GetEmail: string;
begin
  Result := Email;
end;

function TProveedor.GetWebsite: string;
begin
  Result := Website;
end;

function TProveedor.GetFechaIngreso: TDate;
begin
  Result := FechaIngreso;
end;

function TProveedor.GetContacto: string;
begin
  Result := Contacto;
end;

function TProveedor.GetObservaciones: string;
begin
  Result := Observaciones;
end;

function TProveedor.GetCodigoTipoProveedor: string;
begin
  Result := CodigoTipoProveedor;
end;

function TProveedor.GetCodigoZona: string;
begin
  Result := CodigoZona;
end;

function TProveedor.GetCodigoEsquemaPago: string;
begin
  Result := CodigoEsquemaPago;
end;

function TProveedor.GetCodigoTipoRetencionIva: string;
begin
  Result := CodigoTipoRetencionIva;
end;

function TProveedor.GetUniqueId: Integer;
begin
  Result := UniqueId;
end;

function TProveedor.ObtenerCombo: TClientDataSet;
begin
  Result := BD.EjecutarQueryCliDs('SELECT "codigo", "razon_social" FROM ' + GetTabla + ' ORDER BY "codigo"');
end;

function TProveedor.BuscarCodigo: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("codigo" = ' + QuotedStr(GetCodigo) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

end.
