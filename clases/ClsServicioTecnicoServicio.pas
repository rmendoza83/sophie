unit ClsServicioTecnicoServicio;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TServicioTecnicoServicio }

  TServicioTecnicoServicio = class(TGenericoBD)
    private
      Codigo: string;
      Descripcion: string;
      CodigoTipoServicio: string;
      FechaIngreso: Tdate;
      Precio: Double;
      MontoDescuento: Double;
      Comision: Boolean;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArraystring; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigo(varCodigo: string);
      procedure SetDescripcion(varDescricion:string);
      procedure SetCodigoTipoServicio(varCodigoTipoServicio: string);
      procedure SetFechaIngreso(varFechaIngreso: Tdate);
      procedure SetPrecio(varPrecio: Double);
      procedure SetMontoDescuento(varMontoDescuento: Double);
      procedure SetComision(varComision: Boolean);
      function GetCodigo: string;
      function GetDescripcion:string;
      function GetCodigoTipoServicio: string;
      function GetFechaIngreso: Tdate;
      function GetPrecio: Double;
      function GetMontoDescuento: Double;
      function GetComision: Boolean;
      function ObtenerCombo: TClientDataSet;
  end;

implementation

uses
  DB, ClsConsecutivo;

{ TServicioTecnicoServicio }

procedure TServicioTecnicoServicio.Ajustar;
begin
  inherited;

end;


procedure TServicioTecnicoServicio.Mover(Ds: TClientDataSet);
begin
  Codigo := Ds.FieldValues['codigo'];
  Descripcion := Ds.FieldValues['descripcion'];
  CodigoTipoServicio := Ds.FieldValues['codigo_tipo_servicio'];
  FechaIngreso := Ds.FieldByName('fecha_ingreso').AsDateTime;
  Precio := Ds.FieldValues['precio'];
  MontoDescuento := Ds.FieldValues['monto_descuento'];
  Comision := Ds.FieldValues['comision'];
end;

function TServicioTecnicoServicio.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,7);
  Params[0] := Codigo;
  Params[1] := Descripcion;
  Params[2] := CodigoTipoServicio;
  Params[3] := VarFromDateTime(FechaIngreso);
  Params[4] := Precio;
  Params[5] := MontoDescuento;
  Params[6] := Comision;
  Result := Params;
end;

function TServicioTecnicoServicio.GetCampos: TArraystring;
var
  Campos: TArraystring;
begin
  SetLength(Campos,11);
  Campos[0] := 'codigo';
  Campos[1] := 'referencia';
  Campos[2] := 'descripcion';
  Campos[3] := 'codigo_division';
  Campos[4] := 'codigo_linea';
  Campos[5] := 'codigo_familia';
  Campos[6] := 'codigo_clase';
  Campos[7] := 'codigo_manufactura';
  Campos[8] := 'codigo_arancel';
  Campos[9] := 'codigo_unidad';
  Campos[10] :='ubicacion';
  Result := Campos;
end;

function TServicioTecnicoServicio.GetCondicion: string;
begin
  Result:= '"codigo" = ' + QuotedStr(Codigo); 
end;

procedure TServicioTecnicoServicio.AntesInsertar;
begin
  FechaIngreso := Date;
end;

procedure TServicioTecnicoServicio.AntesModificar;
begin

end;

procedure TServicioTecnicoServicio.AntesEliminar;
begin

end;

procedure TServicioTecnicoServicio.DespuesInsertar;
begin

end;

procedure TServicioTecnicoServicio.DespuesModificar;
begin

end;

procedure TServicioTecnicoServicio.DespuesEliminar;
begin

end;

constructor TServicioTecnicoServicio.Create;
var
  ClavesPrimarias: TArraystring;
begin
  inherited Create;
  SetTabla('tbl_servicio_tecnico_servicios');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(7)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "descripcion"=$2, "codigo_tipo_servicio"=$3, "fecha_ingreso"=$4, "precio"=$5, "monto_descuento"=$6, "comision"=$7 WHERE ("codigo" = $1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("codigo" = $1)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"codigo"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TServicioTecnicoServicio.SetCodigo(varCodigo: string);
begin
  Codigo := varCodigo;
end;

procedure TServicioTecnicoServicio.SetDescripcion(varDescricion: string);
begin
  Descripcion := varDescricion;
end;

procedure TServicioTecnicoServicio.SetCodigoTipoServicio(varCodigoTipoServicio: string);
begin
  CodigoTipoServicio := varCodigoTipoServicio;
end;

procedure TServicioTecnicoServicio.SetFechaIngreso(varFechaIngreso: Tdate);
begin
  FechaIngreso := varFechaIngreso;
end;

procedure TServicioTecnicoServicio.SetPrecio(varPrecio: Double);
begin
  Precio := varPrecio;
end;

procedure TServicioTecnicoServicio.SetMontoDescuento(varMontoDescuento: Double);
begin
  MontoDescuento := varMontoDescuento;
end;

procedure TServicioTecnicoServicio.SetComision(varComision: Boolean);
begin
  Comision := varComision;
end;

function TServicioTecnicoServicio.GetCodigo: string;
begin
  Result := Codigo;
end;

function TServicioTecnicoServicio.GetDescripcion: string;
begin
  Result := Descripcion;
end;

function TServicioTecnicoServicio.GetCodigoTipoServicio: string;
begin
  Result := CodigoTipoServicio;
end;

function TServicioTecnicoServicio.GetFechaIngreso: Tdate;
begin
  Result := FechaIngreso;
end;

function TServicioTecnicoServicio.GetPrecio: Double;
begin
  Result := Precio;
end;

function TServicioTecnicoServicio.GetMontoDescuento: Double;
begin
  Result := MontoDescuento;
end;

function TServicioTecnicoServicio.GetComision: Boolean;
begin
  Result := Comision;
end;

function TServicioTecnicoServicio.ObtenerCombo: TClientDataSet;
begin
  Result := BD.EjecutarQueryCliDs('SELECT "codigo", "descripcion" FROM ' + GetTabla + ' ORDER BY "codigo"');
end;

end.
