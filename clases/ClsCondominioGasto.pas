unit ClsCondominioGasto;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TCondominioGasto }

  TCondominioGasto = class(TGenericoBD)
    private
      CodigoConjuntoAdministracion: string;
      Codigo: string;
      Descripcion: string;
      DescripcionLarga: string;
      CodigoTipoGasto: string;
      UsarAlicuotaInmueble: Boolean;
      UsarAlicuotaParticular: Boolean;
      PAlicuota: Double;
      MontoFijo: Boolean;
      Monto: Double;
      Extraordinario: Boolean;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigoConjuntoAdministracion(varCodigoConjuntoAdministracion: string);
      procedure SetCodigo(varCodigo: string);
      procedure SetDescripcion(varDescripcion: string);
      procedure SetDescripcionLarga(varDescripcionLarga: string);
      procedure SetCodigoTipoGasto(varCodigoTipoGasto: string);
      procedure SetUsarAlicuotaInmueble(varUsarAlicuotaInmueble: Boolean);
      procedure SetUsarAlicuotaParticular(varUsarAlicuotaParticular: Boolean);
      procedure SetPAlicuota(varPAlicuota: Double);
      procedure SetMontoFijo(varMontoFijo: Boolean);
      procedure SetMonto(varMonto: Double);
      procedure SetExtraordinario(varExtraordinario: Boolean);
      function GetCodigoConjuntoAdministracion: string;
      function GetCodigo: string;
      function GetDescripcion: string;
      function GetDescripcionLarga: string;
      function GetCodigoTipoGasto: string;
      function GetUsarAlicuotaInmueble: Boolean;
      function GetUsarAlicuotaParticular: Boolean;
      function GetPAlicuota: Double;
      function GetMontoFijo: Boolean;
      function GetMonto: Double;
      function GetExtraordinario: Boolean;
  end;

var
  Clase: TCondominioGasto;

implementation

uses DB;

{ TCondominioGasto }

procedure TCondominioGasto.Ajustar;
begin

end;

procedure TCondominioGasto.Mover(Ds: TClientDataSet);
begin
  CodigoConjuntoAdministracion := Ds.FieldValues['codigo_conjunto_administracion'];
  Codigo := Ds.FieldValues['codigo'];
  Descripcion := Ds.FieldValues['descripcion'];
  DescripcionLarga := Ds.FieldValues['descripcion_larga'];
  CodigoTipoGasto := Ds.FieldValues['codigo_tipo_gasto'];
  UsarAlicuotaInmueble := Ds.FieldValues['usar_alicuota_inmueble'];
  UsarAlicuotaParticular := Ds.FieldValues['usar_alicuota_particular'];
  PAlicuota := Ds.FieldValues['p_alicuota'];
  MontoFijo := Ds.FieldValues['monto_fijo'];
  Monto := Ds.FieldValues['monto'];
  Extraordinario := Ds.FieldValues['extraordinario'];
end;

function TCondominioGasto.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,11);
  Params[0] := CodigoConjuntoAdministracion;
  Params[1] := Codigo;
  Params[2] := Descripcion;
  Params[3] := DescripcionLarga;
  Params[4] := CodigoTipoGasto;
  Params[5] := UsarAlicuotaInmueble;
  Params[6] := UsarAlicuotaParticular;
  Params[7] := PAlicuota;
  Params[8] := MontoFijo;
  Params[9] := Monto;
  Params[10] := Extraordinario;
  Result := Params;
end;

function TCondominioGasto.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,5);
  Campos[0] := 'codigo_conjunto_administracion';
  Campos[1] := 'codigo';
  Campos[2] := 'descripcion';
  Campos[3] := 'descripcion_larga';
  Campos[4] := 'codigo_tipo_gasto';
  Result := Campos;
end;

function TCondominioGasto.GetCondicion: string;
begin
  Result := '"codigo_conjunto_administracion" = ' + QuotedStr(CodigoConjuntoAdministracion) + ' AND ' +
            '"codigo" = ' + QuotedStr(Codigo);
end;

procedure TCondominioGasto.AntesEliminar;
begin

end;

procedure TCondominioGasto.AntesInsertar;
begin

end;

procedure TCondominioGasto.AntesModificar;
begin

end;

procedure TCondominioGasto.DespuesEliminar;
begin

end;

procedure TCondominioGasto.DespuesInsertar;
begin

end;

procedure TCondominioGasto.DespuesModificar;
begin

end;

constructor TCondominioGasto.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_condominio_gastos');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(11)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "descripcion"=$3, "descripcion_larga"=$4, "codigo_tipo_gasto"=$5, "usar_alicuota_inmueble"=$6, "usar_alicuota_particular"=$7, "p_alicuota"=$8, "monto_fijo"=$9, "monto"=$10, "extraordinario"=$11 ' +
                  'WHERE ("codigo_conjunto_administracion"=$1 AND "codigo"=$2)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("codigo_conjunto_administracion"=$1 AND "codigo"=$2)');
  SetLength(ClavesPrimarias,2);
  ClavesPrimarias[0] := '"codigo_conjunto_administracion"';
  ClavesPrimarias[1] := '"codigo"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TCondominioGasto.SetCodigoConjuntoAdministracion(varCodigoConjuntoAdministracion: string);
begin
  CodigoConjuntoAdministracion := varCodigoConjuntoAdministracion;
end;

procedure TCondominioGasto.SetCodigo(varCodigo: string);
begin
  Codigo := varCodigo;
end;

procedure TCondominioGasto.SetDescripcion(varDescripcion: string);
begin
  Descripcion := varDescripcion;
end;

procedure TCondominioGasto.SetDescripcionLarga(varDescripcionLarga: string);
begin
  DescripcionLarga := varDescripcionLarga;
end;

procedure TCondominioGasto.SetCodigoTipoGasto(varCodigoTipoGasto: string);
begin
  CodigoTipoGasto := varCodigoTipoGasto;
end;

procedure TCondominioGasto.SetUsarAlicuotaInmueble(varUsarAlicuotaInmueble: Boolean);
begin
  UsarAlicuotaInmueble := varUsarAlicuotaInmueble;
end;

procedure TCondominioGasto.SetUsarAlicuotaParticular(varUsarAlicuotaParticular: Boolean);
begin
  UsarAlicuotaParticular := varUsarAlicuotaParticular;
end;

procedure TCondominioGasto.SetPAlicuota(varPAlicuota: Double);
begin
  PAlicuota := varPAlicuota;
end;

procedure TCondominioGasto.SetMontoFijo(varMontoFijo: Boolean);
begin
  MontoFijo := varMontoFijo;
end;

procedure TCondominioGasto.SetMonto(varMonto: Double);
begin
  Monto := varMonto;
end;

procedure TCondominioGasto.SetExtraordinario(varExtraordinario: Boolean);
begin
  Extraordinario := varExtraordinario;
end;

function TCondominioGasto.GetCodigoConjuntoAdministracion: string;
begin
  Result := CodigoConjuntoAdministracion;
end;

function TCondominioGasto.GetCodigo: string;
begin
  Result := Codigo;
end;

function TCondominioGasto.GetDescripcion: string;
begin
  Result := Descripcion;
end;

function TCondominioGasto.GetDescripcionLarga: string;
begin
  Result := DescripcionLarga;
end;

function TCondominioGasto.GetCodigoTipoGasto: string;
begin
  Result := CodigoTipoGasto;
end;

function TCondominioGasto.GetUsarAlicuotaInmueble: Boolean;
begin
  Result := UsarAlicuotaInmueble;
end;

function TCondominioGasto.GetUsarAlicuotaParticular: Boolean;
begin
  Result := UsarAlicuotaParticular;
end;

function TCondominioGasto.GetPAlicuota: Double;
begin
  Result := PAlicuota;
end;

function TCondominioGasto.GetMontoFijo: Boolean;
begin
  Result := MontoFijo;
end;

function TCondominioGasto.GetMonto: Double;
begin
  Result := Monto;
end;

function TCondominioGasto.GetExtraordinario: Boolean;
begin
  Result := Extraordinario;
end;

end.
