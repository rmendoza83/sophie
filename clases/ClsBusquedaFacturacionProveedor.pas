unit ClsBusquedaFacturacionProveedor;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TBusquedaFacturacionProveedor }

  TBusquedaFacturacionProveedor = class(TGenericoBD)
    private
      Numero: string;
      NumeroPedido: string;
      CodigoProveedor: string;
      Rif: string;
      Nit: string;
      RazonSocial: string;
      Contacto: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      Function GetNumero: string;
      Function GetNumeroPedido: string;
      Function GetCodigoProveedor: string;
      Function GetRif: string;
      Function GetNit: string;
      Function GetRazonSocial: string;
      Function GetContacto: string;
  end;

implementation

{ TBusquedaFacturacionProveedor }

procedure TBusquedaFacturacionProveedor.Ajustar;
begin

end;

procedure TBusquedaFacturacionProveedor.Mover(Ds: TClientDataSet);
begin
  Numero := Ds.FieldValues['numero'];
  NumeroPedido:= Ds.FieldValues['numero_pedido'];
  CodigoProveedor := Ds.FieldValues['codigo_proveedor'];
  Rif := Ds.FieldValues['rif'];
  Nit := Ds.FieldValues['nit'];
  RazonSocial := Ds.FieldValues['razon_social'];
  Contacto := Ds.FieldValues['contacto'];
end;

function TBusquedaFacturacionProveedor.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,7);
  Params[0] := Numero;
  Params[1] := NumeroPedido;
  Params[2] := CodigoProveedor;
  Params[3] := Rif;
  Params[4] := Nit;
  Params[5] := RazonSocial;
  Params[6] := Contacto;
  Result := Params;
end;

function TBusquedaFacturacionProveedor.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,7);
  Campos[0] := 'numero';
  Campos[1] := 'numero_pedido';
  Campos[2] := 'codigo_proveedor';
  Campos[3] := 'rif';
  Campos[4] := 'nit';
  Campos[5] := 'razon_social';
  Campos[6] := 'contacto';
  Result := Campos;
end;

function TBusquedaFacturacionProveedor.GetCondicion: string;
begin
  Result:= '';
end;

procedure TBusquedaFacturacionProveedor.AntesInsertar;
begin

end;

procedure TBusquedaFacturacionProveedor.AntesModificar;
begin

end;

procedure TBusquedaFacturacionProveedor.AntesEliminar;
begin

end;

procedure TBusquedaFacturacionProveedor.DespuesInsertar;
begin

end;

procedure TBusquedaFacturacionProveedor.DespuesModificar;
begin

end;

procedure TBusquedaFacturacionProveedor.DespuesEliminar;
begin

end;

constructor TBusquedaFacturacionProveedor.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('v_busqueda_facturacion_proveedores');
  SetLength(ClavesPrimarias,0);
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

function TBusquedaFacturacionProveedor.GetCodigoProveedor: string;
begin
  Result := CodigoProveedor;
end;

function TBusquedaFacturacionProveedor.GetContacto: string;
begin
  Result := Contacto;
end;

function TBusquedaFacturacionProveedor.GetNit: string;
begin
  Result := Nit;
end;

function TBusquedaFacturacionProveedor.GetNumero: string;
begin
  Result := Numero;
end;

function TBusquedaFacturacionProveedor.GetNumeroPedido: string;
begin
  Result := NumeroPedido;
end;

function TBusquedaFacturacionProveedor.GetRazonSocial: string;
begin
  Result := RazonSocial;
end;

function TBusquedaFacturacionProveedor.GetRif: string;
begin
  Result := Rif;
end;

end.
