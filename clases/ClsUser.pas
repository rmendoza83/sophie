unit ClsUser;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils;

type

  { TUser }

  TUser = class(TGenericoBD)
    private
      LoginUsuario: string;
      Nombres: string;
      Apellidos: string;
      PasswordUsuario: string;
      SuperUsuario: Boolean;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: String; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetLoginUsuario(varLoginUsuario: string);
      procedure SetNombres(varNombres: string);
      procedure SetApellidos(varApellidos: string);
      procedure SetPasswordUsuario(varPasswordUsuario: string);
      procedure SetSuperUsuario(varSuperUsuario: Boolean);
      function GetLoginUsuario: string;
      function GetNombres: string;
      function GetApellidos: string;
      function GetPasswordUsuario: string;
      function GetSuperUsuario: Boolean;
  end;

implementation

{ TUser }

procedure TUser.Ajustar;
begin

end;

procedure TUser.Mover(Ds: TClientDataSet);
begin
  LoginUsuario := Ds.FieldValues['login_usuario'];
  Nombres := Ds.FieldValues['nombres'];
  Apellidos := Ds.FieldValues['apellidos'];
  PasswordUsuario := Ds.FieldValues['password_usuario'];
  SuperUsuario := Ds.FieldValues['super_usuario'];
end;

function TUser.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,5);
  Params[0] := LoginUsuario;
  Params[1] := Nombres;
  Params[2] := Apellidos;
  Params[3] := PasswordUsuario;
  Params[4] := SuperUsuario;
  Result := Params;
end;

function TUser.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,3);
  Campos[0] := 'login_usuario';
  Campos[1] := 'nombres';
  Campos[2] := 'apellidos';
  Result := Campos;
end;

function TUser.GetCondicion: String;
begin
  Result := '"login_usuario" = ' + QuotedStr(LoginUsuario);
end;

procedure TUser.AntesInsertar;
begin
  SetPasswordUsuario(GetLoginUsuario);
end;

procedure TUser.AntesModificar;
begin

end;

procedure TUser.AntesEliminar;
begin

end;

procedure TUser.DespuesInsertar;
begin

end;

procedure TUser.DespuesModificar;
begin

end;

procedure TUser.DespuesEliminar;
begin

end;

constructor TUser.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('sys_usuarios');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',', CreateDollarsParams(5)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "nombres"=$2, "apellidos"=$3, "password_usuario"=$4, "super_usuario"=$5 WHERE ("login_usuario"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("login_usuario"=$1)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"login_usuario"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TUser.SetLoginUsuario(varLoginUsuario: string);
begin
  LoginUsuario := varLoginUsuario;
end;

procedure TUser.SetNombres(varNombres: string);
begin
  Nombres := varNombres;
end;

procedure TUser.SetApellidos(varApellidos: string);
begin
  Apellidos := varApellidos;
end;

procedure TUser.SetPasswordUsuario(varPasswordUsuario: string);
begin
  PasswordUsuario := MD5String(varPasswordUsuario);
end;

procedure TUser.SetSuperUsuario(varSuperUsuario: Boolean);
begin
  SuperUsuario := varSuperUsuario;
end;

function TUser.GetLoginUsuario: string;
begin
  Result := LoginUsuario;
end;

function TUser.GetNombres: string;
begin
  Result := Nombres;
end;

function TUser.GetApellidos: string;
begin
  Result := Apellidos;
end;

function TUser.GetPasswordUsuario: string;
begin
  Result := PasswordUsuario;
end;

function TUser.GetSuperUsuario: Boolean;
begin
  Result := SuperUsuario;
end;

end.

