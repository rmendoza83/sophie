unit ClsRadioContratoFacturaPendiente;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TRadioContratoFacturaPendiente}

  TRadioContratoFacturaPendiente = class(TGenericoBD)
    private
      IdRadioContrato: Integer;
      Item: Integer;
      TotalItems: Integer;
      FechaIngreso: TDate;
      Total: Double;
      FechaInicial: TDate;
      FechaFinal: TDate;
      Observaciones: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetIdRadioContrato(varIdRadioContrato: Integer);
      procedure SetItem(varItem: Integer);
      procedure SetTotalItems(varTotalItems: Integer);
      procedure SetFechaIngreso(varFechaIngreso: TDate);
      procedure SetTotal(varTotal: Double);
      procedure SetFechaInicial(varFechaInicial: TDate);
      procedure SetFechaFinal(varFechaFinal: TDate);
      procedure SetObservaciones(varObservaciones: string);
      function GetIdRadioContrato: Integer;
      function GetItem: Integer;
      function GetTotalItems: Integer;
      function GetFechaIngreso: TDate;
      function GetTotal: Double;
      function GetFechaInicial: TDate;
      function GetFechaFinal: TDate;
      function GetObservaciones: string;
      function ObtenerIdRadioContrato(varIdRadioContrato: Integer): TClientDataSet;
      function EliminarIdRadioContrato(varIdRadioContrato: Integer): Boolean;
  end;

implementation

{ TRadioContratoFacturaPendiente }

procedure TRadioContratoFacturaPendiente.Ajustar;
begin

end;

procedure TRadioContratoFacturaPendiente.Mover(Ds: TClientDataSet);
begin
  IdRadioContrato := Ds.FieldValues['id_radio_contrato'];
  Item := Ds.FieldValues['item'];
  TotalItems := Ds.FieldValues['total_items'];
  FechaIngreso := Ds.FieldValues['fecha_ingreso'];
  Total := Ds.FieldValues['total'];
  FechaInicial := Ds.FieldValues['fecha_inicial'];
  FechaFinal := Ds.FieldValues['fecha_final'];
  Observaciones := Ds.FieldValues['observaciones'];
end;

function TRadioContratoFacturaPendiente.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,8);
  Params[0] := IdRadioContrato;
  Params[1] := Item;
  Params[2] := TotalItems;
  Params[3] := VarFromDateTime(FechaIngreso);
  Params[4] := Total;
  Params[5] := VarFromDateTime(FechaInicial);
  Params[6] := VarFromDateTime(FechaFinal);
  Params[7] := Observaciones;
  Result := Params;
end;

function TRadioContratoFacturaPendiente.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,1);
  Campos[0] := 'observaciones';
  Result := Campos;
end;

function TRadioContratoFacturaPendiente.GetCondicion: string;
begin
  Result := '("id_radio_contrato" = ' + IntToStr(IdRadioContrato) + ') AND ' +
            '("item" = ' + IntToStr(Item) + ')';
end;

procedure TRadioContratoFacturaPendiente.AntesInsertar;
begin

end;

procedure TRadioContratoFacturaPendiente.AntesModificar;
begin

end;

procedure TRadioContratoFacturaPendiente.AntesEliminar;
begin

end;

procedure TRadioContratoFacturaPendiente.DespuesInsertar;
begin

end;

procedure TRadioContratoFacturaPendiente.DespuesModificar;
begin

end;

procedure TRadioContratoFacturaPendiente.DespuesEliminar;
begin

end;

constructor TRadioContratoFacturaPendiente.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_radio_contratos_facturas_pendientes');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(8)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "total_items"=$3, "fecha_ingreso"=$4, "total"=$5, "fecha_inicial"=$6, "fecha_final"=$7, "observaciones"=$8 WHERE ("id_radio_contrato"=$1) AND ("item"=$2)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("id_radio_contrato"=$1) AND ("item"=$2)');
  SetLength(ClavesPrimarias,2);
  ClavesPrimarias[0] := '"id_radio_contrato"';
  ClavesPrimarias[1] := '"item"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TRadioContratoFacturaPendiente.SetIdRadioContrato(varIdRadioContrato: Integer);
begin
  IdRadioContrato := varIdRadioContrato;
end;

procedure TRadioContratoFacturaPendiente.SetItem(varItem: Integer);
begin
  Item := varItem;
end;

procedure TRadioContratoFacturaPendiente.SetTotalItems(varTotalItems: Integer);
begin
  TotalItems := varTotalItems;
end;

procedure TRadioContratoFacturaPendiente.SetFechaIngreso(varFechaIngreso: TDate);
begin
  FechaIngreso := varFechaIngreso;
end;

procedure TRadioContratoFacturaPendiente.SetTotal(varTotal: Double);
begin
  Total := varTotal;
end;

procedure TRadioContratoFacturaPendiente.SetFechaInicial(varFechaInicial: TDate);
begin
  FechaInicial := varFechaInicial;
end;

procedure TRadioContratoFacturaPendiente.SetFechaFinal(varFechaFinal: TDate);
begin
  FechaFinal := varFechaFinal;
end;

procedure TRadioContratoFacturaPendiente.SetObservaciones(varObservaciones: string);
begin
  Observaciones := varObservaciones;
end;

function TRadioContratoFacturaPendiente.GetIdRadioContrato: Integer;
begin
  Result := IdRadioContrato;
end;

function TRadioContratoFacturaPendiente.GetItem: Integer;
begin
  Result := Item;
end;

function TRadioContratoFacturaPendiente.GetTotalItems: Integer;
begin
  Result := TotalItems;
end;

function TRadioContratoFacturaPendiente.GetFechaIngreso: TDate;
begin
  Result := FechaIngreso;
end;

function TRadioContratoFacturaPendiente.GetTotal: Double;
begin
  Result := Total;
end;

function TRadioContratoFacturaPendiente.GetFechaInicial: TDate;
begin
  Result := FechaInicial;
end;

function TRadioContratoFacturaPendiente.GetFechaFinal: TDate;
begin
  Result := FechaFinal;
end;

function TRadioContratoFacturaPendiente.GetObservaciones: string;
begin
  Result := Observaciones;
end;

function TRadioContratoFacturaPendiente.ObtenerIdRadioContrato(varIdRadioContrato: Integer): TClientDataSet;
begin
  Result := ObtenerListaCondicionSQL(' WHERE "id_radio_contrato" = ' + IntToStr(varIdRadioContrato));
end;

function TRadioContratoFacturaPendiente.EliminarIdRadioContrato(varIdRadioContrato: Integer): Boolean;
begin
  Result := EjecutarSQL('DELETE FROM ' + GetTabla + ' WHERE "id_radio_contrato" = ' + IntToStr(varIdRadioContrato));
end;

end.
