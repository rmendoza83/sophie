unit ClsCondominioLiquidacionCobranza;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TCondominioLiquidacionCobranza }

  TCondominioLiquidacionCobranza = class(TGenericoBD)
    private
      Numero: string;
      CodigoConjuntoAdministracion: string;
      CodigoMoneda: string;
      CodigoInmueble: string;
      FechaIngreso: TDate;
      Observaciones: string;
      Total: Double;
      CodigoFormaPago: string;
      NumeroFormaPago: string;
      CodigoCuentaBancaria: string;
      NumeroDocumentoBancario: string;
      CodigoBancoCobranza: string;
      FechaOperacionBancaria: TDate;
      IdCondominioLiquidacionCobranza: Integer;
      IdOperacionBancaria: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetNumero(varNumero: string);
      procedure SetCodigoConjuntoAdministracion(varCodigoConjuntoAdministracion: string);
      procedure SetCodigoMoneda(varCodigoMoneda: string);
      procedure SetCodigoInmueble(varCodigoInmueble: string);
      procedure SetFechaIngreso(varFechaIngreso: TDate);
      procedure SetObservaciones(varObservaciones: string);
      procedure SetTotal(varTotal: Double);
      procedure SetCodigoFormaPago(varCodigoFormaPago: string);
      procedure SetNumeroFormaPago(varNumeroFormaPago: string);
      procedure SetCodigoCuentaBancaria(varCodigoCuentaBancaria: string);
      procedure SetNumeroDocumentoBancario(varNumeroDocumentoBancario: string);
      procedure SetCodigoBancoCobranza(varCodigoBancoCobranza: string);
      procedure SetFechaOperacionBancaria(varFechaOperacionBancaria: TDate);
      procedure SetIdCondominioLiquidacionCobranza(varIdCondominioLiquidacionCobranza: Integer);
      procedure SetIdOperacionBancaria(varIdOperacionBancaria: Integer);
      function GetNumero: string;
      function GetCodigoConjuntoAdministracion: string;
      function GetCodigoMoneda: string;
      function GetCodigoInmueble: string;
      function GetFechaIngreso: TDate;
      function GetObservaciones: string;
      function GetTotal: Double;
      function GetCodigoFormaPago: string;
      function GetNumeroFormaPago: string;
      function GetCodigoCuentaBancaria: string;
      function GetNumeroDocumentoBancario: string;
      function GetCodigoBancoCobranza: string;
      function GetFechaOperacionBancaria: TDate;
      function GetIdCondominioLiquidacionCobranza: Integer;
      function GetIdOperacionBancaria: Integer;
      function BuscarNumero: Boolean;
  end;

implementation

uses
  DB,
  ClsConsecutivo;

{ TCondominioLiquidacionCobranza }

procedure TCondominioLiquidacionCobranza.Ajustar;
begin

end;

procedure TCondominioLiquidacionCobranza.Mover(Ds: TClientDataSet);
begin
  Numero := Ds.FieldValues['numero'];
  CodigoConjuntoAdministracion := Ds.FieldValues['codigo_conjunto_administracion'];
  CodigoMoneda := Ds.FieldValues['codigo_moneda'];
  CodigoInmueble := Ds.FieldValues['codigo_inmueble'];
  FechaIngreso := Ds.FieldByName('fecha_ingreso').AsDateTime;
  Observaciones := Ds.FieldValues['observaciones'];
  Total := Ds.FieldValues['total'];
  CodigoFormaPago := Ds.FieldValues['codigo_forma_pago'];
  NumeroFormaPago := Ds.FieldValues['numero_forma_pago'];
  CodigoCuentaBancaria := Ds.FieldValues['codigo_cuenta_bancaria'];
  NumeroDocumentoBancario := Ds.FieldValues['numero_documento_bancario'];
  CodigoBancoCobranza := Ds.FieldValues['codigo_banco_cobranza'];
  FechaOperacionBancaria := Ds.FieldByName('fecha_operacion_bancaria').AsDateTime;
  IdCondominioLiquidacionCobranza := Ds.FieldValues['id_condominio_liquidacion_cobranza'];
  IdOperacionBancaria := Ds.FieldValues['id_operacion_bancaria'];
end;

function TCondominioLiquidacionCobranza.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,15);
  Params[0] := Numero;
  Params[1] := CodigoConjuntoAdministracion;
  Params[2] := CodigoMoneda;
  Params[3] := CodigoInmueble;
  Params[4] := VarFromDateTime(FechaIngreso);
  Params[5] := Observaciones;
  Params[6] := Total;
  Params[7] := CodigoFormaPago;
  Params[8] := NumeroFormaPago;
  Params[9] := CodigoCuentaBancaria;
  Params[10] := NumeroDocumentoBancario;
  Params[11] := CodigoBancoCobranza;
  Params[12] := VarFromDateTime(FechaOperacionBancaria);
  Params[13] := IdCondominioLiquidacionCobranza;
  Params[14] := IdOperacionBancaria;
  Result := Params;
end;

function TCondominioLiquidacionCobranza.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,6);
  Campos[0] := 'numero';
  Campos[1] := 'codigo_moneda';
  Campos[2] := 'codigo_cliente';
  Campos[3] := 'codigo_zona';
  Campos[4] := 'observaciones';
  Campos[5] := 'numero_documento_bancario';
  Result := Campos;
end;

function TCondominioLiquidacionCobranza.GetCondicion: string;
begin
  Result := '"id_condominio_liquidacion_cobranza" = ' + IntToStr(IdCondominioLiquidacionCobranza);
end;

procedure TCondominioLiquidacionCobranza.AntesInsertar;
var
  Consecutivo: TConsecutivo;
begin
  Consecutivo := TConsecutivo.Create;
  Numero := IntToStr(Consecutivo.ObtenerConsecutivo('numero_condominio_liquidacion_cobranza'));
  IdCondominioLiquidacionCobranza := Consecutivo.ObtenerConsecutivo('id_condominio_liquidacion_cobranza');
end;

procedure TCondominioLiquidacionCobranza.AntesModificar;
begin

end;

procedure TCondominioLiquidacionCobranza.AntesEliminar;
begin

end;

procedure TCondominioLiquidacionCobranza.DespuesInsertar;
begin

end;

procedure TCondominioLiquidacionCobranza.DespuesEliminar;
begin

end;

procedure TCondominioLiquidacionCobranza.DespuesModificar;
begin

end;

constructor TCondominioLiquidacionCobranza.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_condominio_liquidacion_cobranzas');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(15)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "numero"=$1, "codigo_conjunto_administracion"=$2, "codigo_moneda"=$3, "codigo_inmueble"=$4, "fecha_ingreso"=$5, "observaciones"=$6, "total"=$7, "codigo_forma_pago"=$8, "numero_forma_pago"=$9, "codigo_cuenta_bancaria"=$10, ' +
                  '"numero_documento_bancario"=$11, "codigo_banco_cobranza"=$12, "fecha_operacion_bancaria"=$13, "id_operacion_bancaria"=$15 WHERE ("id_condominio_liquidacion_cobranza"=$14)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("id_liquidacion_cobranza"=$17)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"id_condominio_liquidacion_cobranza"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TCondominioLiquidacionCobranza.SetNumero(varNumero: string);
begin
  Numero := varNumero;
end;

procedure TCondominioLiquidacionCobranza.SetCodigoConjuntoAdministracion(varCodigoConjuntoAdministracion: string);
begin
  CodigoConjuntoAdministracion := varCodigoConjuntoAdministracion;
end;

procedure TCondominioLiquidacionCobranza.SetCodigoMoneda(varCodigoMoneda: string);
begin
  CodigoMoneda := varCodigoMoneda;
end;

procedure TCondominioLiquidacionCobranza.SetCodigoInmueble(varCodigoInmueble: string);
begin
  CodigoInmueble := varCodigoInmueble;
end;

procedure TCondominioLiquidacionCobranza.SetFechaIngreso(varFechaIngreso: TDate);
begin
  FechaIngreso := varFechaIngreso;
end;

procedure TCondominioLiquidacionCobranza.SetObservaciones(varObservaciones: string);
begin
  Observaciones := varObservaciones;
end;

procedure TCondominioLiquidacionCobranza.SetTotal(varTotal: Double);
begin
  Total := varTotal;
end;

procedure TCondominioLiquidacionCobranza.SetCodigoFormaPago(varCodigoFormaPago: string);
begin
  CodigoFormaPago := varCodigoFormaPago;
end;

procedure TCondominioLiquidacionCobranza.SetNumeroFormaPago(varNumeroFormaPago: string);
begin
  NumeroFormaPago := varNumeroFormaPago;
end;

procedure TCondominioLiquidacionCobranza.SetCodigoCuentaBancaria(varCodigoCuentaBancaria: string);
begin
  CodigoCuentaBancaria := varCodigoCuentaBancaria;
end;

procedure TCondominioLiquidacionCobranza.SetNumeroDocumentoBancario(varNumeroDocumentoBancario: string);
begin
  NumeroDocumentoBancario := varNumeroDocumentoBancario;
end;

procedure TCondominioLiquidacionCobranza.SetCodigoBancoCobranza(varCodigoBancoCobranza: string);
begin
  CodigoBancoCobranza := varCodigoBancoCobranza;
end;

procedure TCondominioLiquidacionCobranza.SetFechaOperacionBancaria(varFechaOperacionBancaria: TDate);
begin
  FechaOperacionBancaria := varFechaOperacionBancaria;
end;

procedure TCondominioLiquidacionCobranza.SetIdCondominioLiquidacionCobranza(varIdCondominioLiquidacionCobranza: Integer);
begin
  IdCondominioLiquidacionCobranza := varIdCondominioLiquidacionCobranza;
end;

procedure TCondominioLiquidacionCobranza.SetIdOperacionBancaria(varIdOperacionBancaria: Integer);
begin
  IdOperacionBancaria := varIdOperacionBancaria;
end;

function TCondominioLiquidacionCobranza.GetNumero: string;
begin
  Result := Numero;
end;

function TCondominioLiquidacionCobranza.GetCodigoConjuntoAdministracion: string;
begin
  Result := CodigoConjuntoAdministracion;
end;

function TCondominioLiquidacionCobranza.GetCodigoMoneda: string;
begin
  Result := CodigoMoneda;
end;

function TCondominioLiquidacionCobranza.GetCodigoInmueble: string;
begin
  Result := CodigoInmueble;
end;

function TCondominioLiquidacionCobranza.GetFechaIngreso: TDate;
begin
  Result := FechaIngreso;
end;

function TCondominioLiquidacionCobranza.GetObservaciones: string;
begin
  Result := Observaciones;
end;

function TCondominioLiquidacionCobranza.GetTotal: Double;
begin
  Result := Total;
end;

function TCondominioLiquidacionCobranza.GetCodigoFormaPago: string;
begin
  Result := CodigoFormaPago;
end;

function TCondominioLiquidacionCobranza.GetNumeroFormaPago: string;
begin
  Result := NumeroFormaPago;
end;

function TCondominioLiquidacionCobranza.GetCodigoCuentaBancaria: string;
begin
  Result := CodigoCuentaBancaria;
end;

function TCondominioLiquidacionCobranza.GetNumeroDocumentoBancario: string;
begin
  Result := NumeroDocumentoBancario;
end;

function TCondominioLiquidacionCobranza.GetCodigoBancoCobranza: string;
begin
  Result := CodigoBancoCobranza;
end;

function TCondominioLiquidacionCobranza.GetFechaOperacionBancaria: TDate;
begin
  Result := FechaOperacionBancaria;
end;

function TCondominioLiquidacionCobranza.GetIdCondominioLiquidacionCobranza: Integer;
begin
  Result := IdCondominioLiquidacionCobranza;
end;

function TCondominioLiquidacionCobranza.GetIdOperacionBancaria: Integer;
begin
  Result := IdOperacionBancaria;
end;

function TCondominioLiquidacionCobranza.BuscarNumero: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("numero" = ' + QuotedStr(GetNumero) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

end.
