unit ClsLiquidacionPago;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TLiquidacionPago }

  TLiquidacionPago = class(TGenericoBD)
    private
      Numero: string;
      CodigoMoneda: string;
      CodigoProveedor: string;
      CodigoZona: string;
      FechaIngreso: TDate;
      FechaContable: TDate;
      Observaciones: string;
      MontoNeto: Double;
      TotalRetencion: Double;
      Total: Double;
      CodigoFormaPago: string;
      NumeroFormaPago: string;
      CodigoCuentaBancaria: string;
      NumeroDocumentoBancario: string;
      FechaOperacionBancaria: TDate;
      IdLiquidacionPago: Integer;
      IdOperacionBancaria: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetNumero(varNumero: string);
      procedure SetCodigoMoneda(varCodigoMoneda: string);
      procedure SetCodigoProveedor(varCodigoProveedor: string);
      procedure SetCodigoZona(varCodigoZona: string);
      procedure SetFechaIngreso(varFechaIngreso: TDate);
      procedure SetFechaContable(varFechaContable: TDate);
      procedure SetObservaciones(varObservaciones: string);
      procedure SetMontoNeto(varMontoNeto: Double);
      procedure SetTotalRetencion(varTotalRetencion: Double);
      procedure SetTotal(varTotal: Double);
      procedure SetCodigoFormaPago(varCodigoFormaPago: string);
      procedure SetNumeroFormaPago(varNumeroFormaPago: string);
      procedure SetCodigoCuentaBancaria(varCodigoCuentaBancaria: string);
      procedure SetNumeroDocumentoBancario(varNumeroDocumentoBancario: string);
      procedure SetFechaOperacionBancaria(varFechaOperacionBancaria: TDate);
      procedure SetIdLiquidacionPago(varIdLiquidacionPago: Integer);
      procedure SetIdOperacionBancaria(varIdOperacionBancaria: Integer);
      function GetNumero: string;
      function GetCodigoMoneda: string;
      function GetCodigoProveedor: string;
      function GetCodigoZona: string;
      function GetFechaIngreso: TDate;
      function GetFechaContable: TDate;
      function GetObservaciones: string;
      function GetMontoNeto: Double;
      function GetTotalRetencion: Double;
      function GetTotal: Double;
      function GetCodigoFormaPago: string;
      function GetNumeroFormaPago: string;
      function GetCodigoCuentaBancaria: string;
      function GetNumeroDocumentoBancario: string;
      function GetFechaOperacionBancaria: TDate;
      function GetIdLiquidacionPago: Integer;
      function GetIdOperacionBancaria: Integer;
      function BuscarNumero: Boolean;
  end;

implementation

uses
  DB,
  ClsConsecutivo;

{ TLiquidacionPago }

procedure TLiquidacionPago.Ajustar;
begin

end;

procedure TLiquidacionPago.Mover(Ds: TClientDataSet);
begin
  Numero := Ds.FieldValues['numero'];
  CodigoMoneda := Ds.FieldValues['codigo_moneda'];
  CodigoProveedor := Ds.FieldValues['codigo_proveedor'];
  CodigoZona := Ds.FieldValues['codigo_zona'];
  FechaIngreso := Ds.FieldByName('fecha_ingreso').AsDateTime;
  FechaContable := Ds.FieldByName('fecha_contable').AsDateTime;
  Observaciones := Ds.FieldValues['observaciones'];
  MontoNeto := Ds.FieldValues['monto_neto'];
  TotalRetencion := Ds.FieldValues['total_retencion'];
  Total := Ds.FieldValues['total'];
  CodigoFormaPago := Ds.FieldValues['codigo_forma_pago'];
  NumeroFormaPago := Ds.FieldValues['numero_forma_pago'];
  CodigoCuentaBancaria := Ds.FieldValues['codigo_cuenta_bancaria'];
  NumeroDocumentoBancario := Ds.FieldValues['numero_documento_bancario'];
  FechaOperacionBancaria := Ds.FieldByName('fecha_operacion_bancaria').AsDateTime;
  IdLiquidacionPago := Ds.FieldValues['id_liquidacion_pago'];
  IdOperacionBancaria := Ds.FieldValues['id_operacion_bancaria'];
end;

function TLiquidacionPago.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,17);
  Params[0] := Numero;
  Params[1] := CodigoMoneda;
  Params[2] := CodigoProveedor;
  Params[3] := CodigoZona;
  Params[4] := VarFromDateTime(FechaIngreso);
  Params[5] := VarFromDateTime(FechaContable);
  Params[6] := Observaciones;
  Params[7] := MontoNeto;
  Params[8] := TotalRetencion;
  Params[9] := Total;
  Params[10] := CodigoFormaPago;
  Params[11] := NumeroFormaPago;
  Params[12] := CodigoCuentaBancaria;
  Params[13] := NumeroDocumentoBancario;
  Params[14] := VarFromDateTime(FechaOperacionBancaria);
  Params[15] := IdLiquidacionPago;
  Params[16] := IdOperacionBancaria;
  Result := Params;
end;

function TLiquidacionPago.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,6);
  Campos[0] := 'numero';
  Campos[1] := 'codigo_moneda';
  Campos[2] := 'codigo_proveedor';
  Campos[3] := 'codigo_zona';
  Campos[4] := 'observaciones';
  Campos[5] := 'numero_documento_bancario';
  Result := Campos;
end;

function TLiquidacionPago.GetCondicion: string;
begin
  Result := '"id_liquidacion_pago" = ' + IntToStr(IdLiquidacionPago);
end;

procedure TLiquidacionPago.AntesInsertar;
var
  Consecutivo: TConsecutivo;
begin
  Consecutivo := TConsecutivo.Create;
  Numero := IntToStr(Consecutivo.ObtenerConsecutivo('numero_liquidacion_pago'));
  IdLiquidacionPago := Consecutivo.ObtenerConsecutivo('id_liquidacion_pago');
end;

procedure TLiquidacionPago.AntesModificar;
begin

end;

procedure TLiquidacionPago.AntesEliminar;
begin

end;

procedure TLiquidacionPago.DespuesInsertar;
begin

end;

procedure TLiquidacionPago.DespuesEliminar;
begin

end;

procedure TLiquidacionPago.DespuesModificar;
begin

end;

constructor TLiquidacionPago.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_liquidacion_pagos');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(17)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "numero"=$1,"codigo_moneda"=$2, "codigo_proveedor"=$3, "codigo_zona"=$4, "fecha_ingreso"=$5, "fecha_contable"=$6, "observaciones"=$7, "monto_neto"=$8, "total_retencion"=$9, "total"=$10, ' +
                  '"codigo_forma_pago"=$11, "numero_forma_pago"=$12, "codigo_cuenta_bancaria"=$13, "numero_documento_bancario"=$14, "fecha_operacion_bancaria"=$15, "id_operacion_bancaria"=$17 WHERE ("id_liquidacion_pago"=$16)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("id_liquidacion_pago"=$16)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"id_liquidacion_cobranza"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TLiquidacionPago.SetNumero(varNumero: string);
begin
  Numero := varNumero;
end;

procedure TLiquidacionPago.SetCodigoMoneda(varCodigoMoneda: string);
begin
  CodigoMoneda := varCodigoMoneda;
end;

procedure TLiquidacionPago.SetCodigoProveedor(varCodigoProveedor: string);
begin
  CodigoProveedor := varCodigoProveedor;
end;

procedure TLiquidacionPago.SetCodigoZona(varCodigoZona: string);
begin
  CodigoZona := varCodigoZona;
end;

procedure TLiquidacionPago.SetFechaIngreso(varFechaIngreso: TDate);
begin
  FechaIngreso := varFechaIngreso;
end;

procedure TLiquidacionPago.SetFechaContable(varFechaContable: TDate);
begin
  FechaContable := varFechaContable;
end;

procedure TLiquidacionPago.SetObservaciones(varObservaciones: string);
begin
  Observaciones := varObservaciones;
end;

procedure TLiquidacionPago.SetMontoNeto(varMontoNeto: Double);
begin
  MontoNeto := varMontoNeto;
end;

procedure TLiquidacionPago.SetTotalRetencion(varTotalRetencion: Double);
begin
  TotalRetencion := varTotalRetencion;
end;

procedure TLiquidacionPago.SetTotal(varTotal: Double);
begin
  Total := varTotal;
end;

procedure TLiquidacionPago.SetCodigoFormaPago(varCodigoFormaPago: string);
begin
  CodigoFormaPago := varCodigoFormaPago;
end;

procedure TLiquidacionPago.SetNumeroFormaPago(varNumeroFormaPago: string);
begin
  NumeroFormaPago := varNumeroFormaPago; 
end;

procedure TLiquidacionPago.SetCodigoCuentaBancaria(varCodigoCuentaBancaria: string);
begin
  CodigoCuentaBancaria := varCodigoCuentaBancaria;
end;

procedure TLiquidacionPago.SetNumeroDocumentoBancario(varNumeroDocumentoBancario: string);
begin
  NumeroDocumentoBancario := varNumeroDocumentoBancario;
end;

procedure TLiquidacionPago.SetFechaOperacionBancaria(varFechaOperacionBancaria: TDate);
begin
  FechaOperacionBancaria := varFechaOperacionBancaria;
end;

procedure TLiquidacionPago.SetIdLiquidacionPago(varIdLiquidacionPago: Integer);
begin
  IdLiquidacionPago := varIdLiquidacionPago;
end;

procedure TLiquidacionPago.SetIdOperacionBancaria(varIdOperacionBancaria: Integer);
begin
  IdOperacionBancaria := varIdOperacionBancaria;
end;

function TLiquidacionPago.GetNumero: string;
begin
  Result := Numero;
end;

function TLiquidacionPago.GetCodigoMoneda: string;
begin
  Result := CodigoMoneda;
end;

function TLiquidacionPago.GetCodigoProveedor: string;
begin
  Result := CodigoProveedor;
end;

function TLiquidacionPago.GetCodigoZona: string;
begin
  Result := CodigoZona;
end;

function TLiquidacionPago.GetFechaIngreso: TDate;
begin
  Result := FechaIngreso;
end;

function TLiquidacionPago.GetFechaContable: TDate;
begin
  Result := FechaContable;
end;

function TLiquidacionPago.GetObservaciones: string;
begin
  Result := Observaciones;
end;

function TLiquidacionPago.GetMontoNeto: Double;
begin
  Result := MontoNeto;
end;

function TLiquidacionPago.GetTotalRetencion: Double;
begin
  Result := TotalRetencion;
end;

function TLiquidacionPago.GetTotal: Double;
begin
  Result := Total;
end;

function TLiquidacionPago.GetCodigoFormaPago: string;
begin
  Result := CodigoFormaPago;
end;

function TLiquidacionPago.GetNumeroFormaPago: string;
begin
  Result := NumeroFormaPago;
end;

function TLiquidacionPago.GetCodigoCuentaBancaria: string;
begin
  Result := CodigoCuentaBancaria;
end;

function TLiquidacionPago.GetNumeroDocumentoBancario: string;
begin
  Result := NumeroDocumentoBancario;
end;

function TLiquidacionPago.GetFechaOperacionBancaria: TDate;
begin
  Result := FechaOperacionBancaria;
end;

function TLiquidacionPago.GetIdLiquidacionPago: Integer;
begin
  Result := IdLiquidacionPago;
end;

function TLiquidacionPago.GetIdOperacionBancaria: Integer;
begin
  Result := IdOperacionBancaria;
end;

function TLiquidacionPago.BuscarNumero: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("numero" = ' + QuotedStr(GetNumero) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

end.
