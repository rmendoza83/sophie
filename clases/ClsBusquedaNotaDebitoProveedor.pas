unit ClsBusquedaNotaDebitoProveedor;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TBusquedaFacturacionProveedor }

  TBusquedaNotaDebitoProveedor = class(TGenericoBD)
    private
      Numero: string;
      CodigoProveedor: string;
      Rif: string;
      Nit: string;
      RazonSocial: string;
      Contacto: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      Function GetNumero: string;
      Function GetCodigoProveedor: string;
      Function GetRif: string;
      Function GetNit: string;
      Function GetRazonSocial: string;
      Function GetContacto: string;
  end;

implementation

{ TBusquedaNotaDebitoProveedor }

procedure TBusquedaNotaDebitoProveedor.Ajustar;
begin

end;

procedure TBusquedaNotaDebitoProveedor.Mover(Ds: TClientDataSet);
begin
  Numero := Ds.FieldValues['numero'];
  CodigoProveedor := Ds.FieldValues['codigo_proveedor'];
  Rif := Ds.FieldValues['rif'];
  Nit := Ds.FieldValues['nit'];
  RazonSocial := Ds.FieldValues['razon_social'];
  Contacto := Ds.FieldValues['contacto'];
end;

function TBusquedaNotaDebitoProveedor.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,6);
  Params[0] := Numero;
  Params[1] := CodigoProveedor;
  Params[2] := Rif;
  Params[3] := Nit;
  Params[4] := RazonSocial;
  Params[5] := Contacto;
  Result := Params;
end;

function TBusquedaNotaDebitoProveedor.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,6);
  Campos[0] := 'numero';
  Campos[1] := 'codigo_proveedor';
  Campos[2] := 'rif';
  Campos[3] := 'nit';
  Campos[4] := 'razon_social';
  Campos[5] := 'contacto';
  Result := Campos;
end;

function TBusquedaNotaDebitoProveedor.GetCondicion: string;
begin
  Result:= '';
end;

procedure TBusquedaNotaDebitoProveedor.AntesInsertar;
begin

end;

procedure TBusquedaNotaDebitoProveedor.AntesModificar;
begin

end;

procedure TBusquedaNotaDebitoProveedor.AntesEliminar;
begin

end;

procedure TBusquedaNotaDebitoProveedor.DespuesInsertar;
begin

end;

procedure TBusquedaNotaDebitoProveedor.DespuesModificar;
begin

end;

procedure TBusquedaNotaDebitoProveedor.DespuesEliminar;
begin

end;

constructor TBusquedaNotaDebitoProveedor.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('v_busqueda_nota_debito_proveedores');
  SetLength(ClavesPrimarias,0);
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

function TBusquedaNotaDebitoProveedor.GetCodigoProveedor: string;
begin
  Result := CodigoProveedor;
end;

function TBusquedaNotaDebitoProveedor.GetContacto: string;
begin
  Result := Contacto;
end;

function TBusquedaNotaDebitoProveedor.GetNit: string;
begin
  Result := Nit;
end;

function TBusquedaNotaDebitoProveedor.GetNumero: string;
begin
  Result := Numero;
end;

function TBusquedaNotaDebitoProveedor.GetRazonSocial: string;
begin
  Result := RazonSocial;
end;

function TBusquedaNotaDebitoProveedor.GetRif: string;
begin
  Result := Rif;
end;

end.
