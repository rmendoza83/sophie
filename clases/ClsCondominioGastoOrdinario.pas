unit ClsCondominioGastoOrdinario;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TCondominioGastoOrdinario }

  TCondominioGastoOrdinario = class(TGenericoBD)
    private
      CodigoConjuntoAdministracion: string;
      CodigoGasto: string;
      Monto: Double;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigoConjuntoAdministracion(varCodigoConjuntoAdministracion: string);
      procedure SetCodigoGasto(varCodigoGasto: string);
      procedure SetMonto(varMonto: Double);
      function GetCodigoConjuntoAdministracion: string;
      function GetCodigoGasto: string;
      function GetMonto: Double;
      function ObtenerCodigoConjuntoAdministracion(varCodigoConjuntoAdministracion: string): TClientDataSet;
      function EliminarCodigoConjuntoAdministracion(varCodigoConjuntoAdministracion: string): Boolean;
  end;

var
  Clase: TCondominioGastoOrdinario;

implementation

{ TCondominioGastoOrdinario }

procedure TCondominioGastoOrdinario.Ajustar;
begin

end;

procedure TCondominioGastoOrdinario.Mover(Ds: TClientDataSet);
begin
  CodigoConjuntoAdministracion := Ds.FieldValues['codigo_conjunto_administracion'];
  CodigoGasto := Ds.FieldValues['codigo_gasto'];
  Monto := Ds.FieldValues['monto'];
end;

function TCondominioGastoOrdinario.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,3);
  Params[0] := CodigoConjuntoAdministracion;
  Params[1] := CodigoGasto;
  Params[2] := Monto;
  Result := Params;
end;

function TCondominioGastoOrdinario.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,2);
  Campos[0] := 'codigo_conjunto_administracion';
  Campos[1] := 'codigo_gasto';
  Result := Campos;
end;

function TCondominioGastoOrdinario.GetCondicion: string;
begin
  Result := '"codigo_conjunto_administracion" = ' + QuotedStr(CodigoConjuntoAdministracion) + ' AND ' +
            '"codigo_gasto" = ' + QuotedStr(CodigoGasto);
end;

procedure TCondominioGastoOrdinario.AntesEliminar;
begin

end;

procedure TCondominioGastoOrdinario.AntesInsertar;
begin

end;

procedure TCondominioGastoOrdinario.AntesModificar;
begin

end;

procedure TCondominioGastoOrdinario.DespuesEliminar;
begin

end;

procedure TCondominioGastoOrdinario.DespuesInsertar;
begin

end;

procedure TCondominioGastoOrdinario.DespuesModificar;
begin

end;

constructor TCondominioGastoOrdinario.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_condominio_gastos_ordinarios');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(3)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "monto"=$3 WHERE ("codigo_conjunto_administracion"=$1 AND "codigo_gasto"=$2)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("codigo_conjunto_administracion"=$1 AND "codigo_gasto"=$2)');
  SetLength(ClavesPrimarias,2);
  ClavesPrimarias[0] := '"codigo_conjunto_administracion"';
  ClavesPrimarias[0] := '"codigo_gasto"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TCondominioGastoOrdinario.SetCodigoConjuntoAdministracion(varCodigoConjuntoAdministracion: string);
begin
  CodigoConjuntoAdministracion := varCodigoConjuntoAdministracion;
end;

procedure TCondominioGastoOrdinario.SetCodigoGasto(varCodigoGasto: string);
begin
  CodigoGasto := varCodigoGasto;
end;

procedure TCondominioGastoOrdinario.SetMonto(varMonto: Double);
begin
  Monto := varMonto;
end;

function TCondominioGastoOrdinario.GetCodigoConjuntoAdministracion: string;
begin
  Result := CodigoConjuntoAdministracion;
end;

function TCondominioGastoOrdinario.GetCodigoGasto: string;
begin
  Result := CodigoGasto;
end;

function TCondominioGastoOrdinario.GetMonto: Double;
begin
  Result := Monto;
end;

function TCondominioGastoOrdinario.ObtenerCodigoConjuntoAdministracion(
  varCodigoConjuntoAdministracion: string): TClientDataSet;
begin
  Result := ObtenerListaCondicionSQL(' WHERE "codigo_conjunto_administracion" = ' + QuotedStr(varCodigoConjuntoAdministracion));
end;

function TCondominioGastoOrdinario.EliminarCodigoConjuntoAdministracion(
  varCodigoConjuntoAdministracion: string): Boolean;
begin
  Result := EjecutarSQL('DELETE FROM ' + GetTabla + ' WHERE "codigo_conjunto_administracion" = ' + QuotedStr(varCodigoConjuntoAdministracion));
end;

end.
