unit ClsAnticipoProveedor;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TAnticipoProveedor }

  TAnticipoProveedor = class(TGenericoBD)
    private
      Numero: string;
      CodigoMoneda: string;
      CodigoProveedor: string;
      CodigoZona: string;
      FechaIngreso: TDate;
      FechaRecepcion: TDate;
      FechaContable: TDate;
      FechaLibros: TDate;
      Concepto: string;
      Observaciones: string;
      MontoNeto: Double;
      LoginUsuario: string;
      TiempoIngreso: TDateTime;
      CodigoFormaPago: string;
      NumeroFormaPago: string;
      CodigoCuentaBancaria: string;
      NumeroDocumentoBancario: string;
      FechaOperacionBancaria: TDate;
      IdAnticipoProveedor: Integer;
      IdPago: Integer;
      IdContadoPago: Integer;
      IdOperacionBancaria: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArraystring; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetNumero(varNumero: string);
      procedure SetCodigoMoneda(varCodigoMoneda: string);
      procedure SetCodigoProveedor(varCodigoProveedor: string);
      procedure SetCodigoZona(varCodigoZona: string);
      procedure SetFechaIngreso(varFechaIngreso: TDate);
      procedure SetFechaRecepcion(varFechaRecepcion: TDate);
      procedure SetFechaContable(varFechaContable: TDate);
      procedure SetFechaLibros(varFechalibros: TDate);
      procedure SetConcepto(varConcepto: string);
      procedure SetObservaciones(varObservaciones: string);
      procedure SetMontoNeto(varMontoNeto: Double);
      procedure SetLoginUsuario(varLoginUsuario: string);
      procedure SetTiempoIngreso(varTiempodeIngreso: TDateTime);
      procedure SetCodigoFormaPago(varCodigoFormaPago: string);
      procedure SetNumeroFormaPago(varNumeroFormaPago: string);
      procedure SetCodigoCuentaBancaria(varCodigoCuentaBancaria: string);
      procedure SetNumeroDocumentoBancario(varNumeroDocumentoBancario: string);
      procedure SetFechaOperacionBancaria(varFechaOperacionBancaria: TDate);
      procedure SetIdAnticipoProveedor(varIdAnticipoProveedor: Integer);
      procedure SetIdPago(varIdPago: Integer);
      procedure SetIdContadoPago(varIdContadoPago: Integer);
      procedure SetIdOperacionBancaria(varIdOperacionBancaria: Integer);
      function GetNumero: string;
      function GetCodigoMoneda: string;
      function GetCodigoProveedor: string;
      function GetCodigoZona: string;
      function GetFechaIngreso: TDate;
      function GetFechaRecepcion: TDate;
      function GetFechaContable: TDate;
      function GetFechaLibros: TDate;
      function GetConcepto: string;
      function GetObservaciones: string;
      function GetMontoNeto: Double;
      function GetLoginUsuario: string;
      function GetTiempoIngreso: TDateTime;
      function GetCodigoFormaPago: string;
      function GetNumeroFormaPago: string;
      function GetCodigoCuentaBancaria: string;
      function GetNumeroDocumentoBancario: string;
      function GetFechaOperacionBancaria: TDate;
      function GetIdAnticipoProveedor: Integer;
      function GetIdPago: Integer;
      function GetIdContadoPago: Integer;
      function GetIdOperacionBancaria: Integer;
      function BuscarNumero: Boolean;
      function BuscarCodigoProveedor: Boolean;
  end;

implementation

uses
  DB,
  ClsConsecutivo;

{ TAnticipoProveedor }

procedure TAnticipoProveedor.Ajustar;
begin

end;

procedure TAnticipoProveedor.Mover(Ds: TClientDataSet);
begin
  Numero := Ds.FieldValues['numero'];
  CodigoMoneda := Ds.FieldValues['codigo_moneda'];
  CodigoProveedor := Ds.FieldValues['codigo_proveedor'];
  CodigoZona := Ds.FieldValues['codigo_zona'];
  FechaIngreso := Ds.FieldByName('fecha_ingreso').AsDateTime;
  FechaRecepcion := Ds.FieldByName('fecha_recepcion').AsDateTime;
  FechaContable := Ds.FieldByName('fecha_contable').AsDateTime;
  FechaLibros :=  Ds.FieldByName('fecha_libros').AsDateTime;
  Concepto := Ds.FieldValues['concepto'];
  Observaciones := Ds.FieldValues['observaciones'];
  MontoNeto := Ds.FieldValues['monto_neto'];
  LoginUsuario := Ds.FieldValues['login_usuario'];
  TiempoIngreso := Ds.FieldByName('tiempo_ingreso').AsDateTime;
  CodigoFormaPago := Ds.FieldValues['codigo_forma_pago'];
  NumeroFormaPago := Ds.FieldValues['numero_forma_pago'];
  CodigoCuentaBancaria := Ds.FieldValues['codigo_cuenta_bancaria'];
  NumeroDocumentoBancario := Ds.FieldValues['numero_documento_bancario'];
  FechaOperacionBancaria := Ds.FieldByName('fecha_operacion_bancaria').AsDateTime;
  IdAnticipoProveedor := Ds.FieldValues['id_anticipo_proveedor'];
  IdPago := Ds.FieldValues['id_pago'];
  IdContadoPago := Ds.FieldValues['id_contado_pago'];
  IdOperacionBancaria := Ds.FieldValues['id_operacion_bancaria'];
end;

function TAnticipoProveedor.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,22);
  Params[0] := Numero;
  Params[1] := CodigoMoneda;
  Params[2] := CodigoProveedor;
  Params[3] := CodigoZona;
  Params[4] := VarFromDateTime(FechaIngreso);
  Params[5] := VarFromDateTime(FechaRecepcion);
  Params[6] := VarFromDateTime(FechaContable);
  Params[7] := VarFromDateTime(FechaLibros);
  Params[8] := Concepto;
  Params[9] := Observaciones;
  Params[10] := MontoNeto;
  Params[11] := LoginUsuario;
  Params[12] := VarFromDateTime(TiempoIngreso);
  Params[13] := CodigoFormaPago;
  Params[14] := NumeroFormaPago;
  Params[15] := CodigoCuentaBancaria;
  Params[16] := NumeroDocumentoBancario;
  Params[17] := VarFromDateTime(FechaOperacionBancaria);
  Params[18] := IdAnticipoProveedor;
  Params[19] := IdPago;
  Params[20] := IdContadoPago;
  Params[21] := IdOperacionBancaria;
  Result := Params;
end;

function TAnticipoProveedor.GetCampos: TArraystring;
var
  Campos: TArraystring;
begin
  SetLength(Campos,11);
  Campos[0] := 'numero';
  Campos[1] := 'codigo_moneda';
  Campos[2] := 'codigo_proveedor';
  Campos[3] := 'codigo_zona';
  Campos[4] := 'concepto';
  Campos[5] := 'observaciones';
  Campos[6] := 'login_usuario';
  Campos[7] := 'codigo_forma_pago';
  Campos[8] := 'numero_forma_pago';
  Campos[9] := 'codigo_cuenta_bancaria';
  Campos[10] := 'numero_documento_bancario';
  Result := Campos;
end;

function TAnticipoProveedor.GetCondicion: string;
begin
  Result:= '"id_anticipo_proveedor" =' + IntToStr(IdAnticipoProveedor);
end;

procedure TAnticipoProveedor.AntesInsertar;
var
  Consecutivo: TConsecutivo;
begin
  Consecutivo := TConsecutivo.Create;
  Numero := IntToStr(Consecutivo.ObtenerConsecutivo('numero_anticipo_proveedor'));
  IdAnticipoProveedor := Consecutivo.ObtenerConsecutivo('id_anticipo_proveedor');
  IdPago := Consecutivo.ObtenerConsecutivo('id_pago');
  IdContadoPago := Consecutivo.ObtenerConsecutivo('id_contado_pago');
end;

procedure TAnticipoProveedor.AntesModificar;
begin

end;

procedure TAnticipoProveedor.AntesEliminar;
begin

end;

procedure TAnticipoProveedor.DespuesInsertar;
begin

end;

procedure TAnticipoProveedor.DespuesModificar;
begin

end;

procedure TAnticipoProveedor.DespuesEliminar;
begin

end;

constructor TAnticipoProveedor.Create;
var
  ClavesPrimarias: TArraystring;
begin
  inherited Create;
  SetTabla ('tbl_anticipo_proveedores');
  SetStrSQLInsert ('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(22)) + ')');
  SetStrSQLUpdate ('UPDATE ' + GetTabla + ' SET "numero"=$1, "codigo_moneda"=$2, "codigo_proveedor"=$3, "codigo_zona"=$4, "fecha_ingreso"=$5, "fecha_recepcion"=$6, "fecha_contable"=$7, "fecha_libros"=$8, "concepto"=$9, "observaciones"=$10, ' +
                   '"monto_neto"=$11, "login_usuario"=$12, "tiempo_ingreso"=$13, "codigo_forma_pago"=$14, "numero_forma_pago"=$15, "codigo_cuenta_bancaria"=$16, "numero_documento_bancario"=$17, "fecha_operacion_bancaria"=$18, "id_pago"=$20, ' +
                   '"id_contado_pago"=$21, "id_operacion_bancaria"=$22 WHERE ("id_anticipo_proveedor"=$19)');
  SetStrSQLDelete ('DELETE FROM ' + GetTabla + ' WHERE ("id_anticipo_proveedor"=$19)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"id_anticipo_proveedor"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TAnticipoProveedor.SetCodigoProveedor(varCodigoProveedor: string);
begin
  CodigoProveedor := varCodigoProveedor;
end;

procedure TAnticipoProveedor.SetCodigoCuentaBancaria(
  varCodigoCuentaBancaria: string);
begin
  CodigoCuentaBancaria := varCodigoCuentaBancaria;
end;

procedure TAnticipoProveedor.SetCodigoFormaPago(varCodigoFormaPago: string);
begin
  CodigoFormaPago := varCodigoFormaPago;
end;

procedure TAnticipoProveedor.SetCodigoMoneda(varCodigoMoneda: string);
begin
  CodigoMoneda := varCodigoMoneda;
end;

procedure TAnticipoProveedor.SetCodigoZona(varCodigoZona: string);
begin
  CodigoZona := varCodigoZona;
end;

procedure TAnticipoProveedor.SetConcepto(varConcepto: string);
begin
  Concepto := varConcepto;
end;

procedure TAnticipoProveedor.SetFechaContable(varFechaContable: TDate);
begin
  FechaContable := varFechaContable;
end;

procedure TAnticipoProveedor.SetFechaIngreso(varFechaIngreso: TDate);
begin
  FechaIngreso := varFechaIngreso;
end;

procedure TAnticipoProveedor.SetFechaLibros(varFechalibros: TDate);
begin
  FechaLibros := varFechalibros;
end;

procedure TAnticipoProveedor.SetFechaOperacionBancaria(
  varFechaOperacionBancaria: TDate);
begin
  FechaOperacionBancaria := varFechaOperacionBancaria;
end;

procedure TAnticipoProveedor.SetFechaRecepcion(varFechaRecepcion: TDate);
begin
  FechaRecepcion := varFechaRecepcion;
end;

procedure TAnticipoProveedor.SetIdAnticipoProveedor(varIdAnticipoProveedor: Integer);
begin
  IdAnticipoProveedor := varIdAnticipoProveedor;
end;

procedure TAnticipoProveedor.SetIdPago(varIdPago: Integer);
begin
  IdPago := varIdPago;
end;

procedure TAnticipoProveedor.SetIdContadoPago(varIdContadoPago: Integer);
begin
  IdContadoPago := varIdContadoPago;
end;

procedure TAnticipoProveedor.SetIdOperacionBancaria(
  varIdOperacionBancaria: Integer);
begin
  IdOperacionBancaria := varIdOperacionBancaria;
end;

procedure TAnticipoProveedor.SetLoginUsuario(varLoginUsuario: string);
begin
  LoginUsuario := varLoginUsuario;
end;

procedure TAnticipoProveedor.SetMontoNeto(varMontoNeto: Double);
begin
  MontoNeto := varMontoNeto;
end;

procedure TAnticipoProveedor.SetNumero(varNumero: string);
begin
  Numero := varNumero;
end;

procedure TAnticipoProveedor.SetNumeroDocumentoBancario(
  varNumeroDocumentoBancario: string);
begin
  NumeroDocumentoBancario := varNumeroDocumentoBancario;
end;

procedure TAnticipoProveedor.SetNumeroFormaPago(varNumeroFormaPago: string);
begin
  NumeroFormaPago := varNumeroFormaPago;
end;

procedure TAnticipoProveedor.SetObservaciones(varObservaciones: string);
begin
  Observaciones :=  varObservaciones;
end;

procedure TAnticipoProveedor.SetTiempoIngreso(varTiempodeIngreso: TDateTime);
begin
  TiempoIngreso := varTiempodeIngreso;
end;

function TAnticipoProveedor.GetCodigoProveedor: string;
begin
  Result := CodigoProveedor;
end;

function TAnticipoProveedor.GetCodigoCuentaBancaria: string;
begin
  Result := CodigoCuentaBancaria;
end;

function TAnticipoProveedor.GetCodigoFormaPago: string;
begin
  Result := CodigoFormaPago;
end;

function TAnticipoProveedor.GetCodigoMoneda: string;
begin
  Result := CodigoMoneda;
end;

function TAnticipoProveedor.GetCodigoZona: string;
begin
  Result := CodigoZona;
end;

function TAnticipoProveedor.GetConcepto: string;
begin
  Result := Concepto;
end;

function TAnticipoProveedor.GetFechaContable: TDate;
begin
  Result := FechaContable;
end;

function TAnticipoProveedor.GetFechaIngreso: TDate;
begin
  Result := FechaIngreso;
end;

function TAnticipoProveedor.GetFechaLibros: TDate;
begin
  Result := FechaLibros;
end;

function TAnticipoProveedor.GetFechaOperacionBancaria: TDate;
begin
  Result := FechaOperacionBancaria;
end;

function TAnticipoProveedor.GetFechaRecepcion: TDate;
begin
  Result := FechaRecepcion;
end;

function TAnticipoProveedor.GetIdAnticipoProveedor: Integer;
begin
  Result := IdAnticipoProveedor;
end;

function TAnticipoProveedor.GetIdPago: Integer;
begin
  Result := IdPago;
end;

function TAnticipoProveedor.GetIdContadoPago: Integer;
begin
  Result := IdContadoPago;
end;

function TAnticipoProveedor.GetIdOperacionBancaria: Integer;
begin
  Result := IdOperacionBancaria;
end;

function TAnticipoProveedor.GetLoginUsuario: string;
begin
  Result := LoginUsuario;
end;

function TAnticipoProveedor.GetMontoNeto: Double;
begin
  Result := MontoNeto;
end;

function TAnticipoProveedor.GetNumero: string;
begin
  Result := Numero;
end;

function TAnticipoProveedor.GetNumeroDocumentoBancario: string;
begin
  Result := NumeroDocumentoBancario;
end;

function TAnticipoProveedor.GetNumeroFormaPago: string;
begin
  Result := NumeroFormaPago;
end;

function TAnticipoProveedor.GetObservaciones: string;
begin
  Result := Observaciones;
end;

function TAnticipoProveedor.GetTiempoIngreso: TDateTime;
begin
  Result := TiempoIngreso;
end;

function TAnticipoProveedor.BuscarNumero: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("numero" = ' + QuotedStr(GetNumero) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

function TAnticipoProveedor.BuscarCodigoProveedor: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("codigo_proveedor" = ' + QuotedStr(GetCodigoProveedor) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

end.

