unit ClsDetConteoInventario;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TDetConteoInventario }

  TDetConteoInventario = class(TGenericoBD)
    private
      NumeroConteo: Integer;
      CodigoProducto: string;
      Referencia: string;
      Descripcion: string;
      Cantidad: Double;
      IdConteoInventario: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetNumeroConteo(varNumeroConteo: Integer);
      procedure SetCodigoProducto(varCodigoProducto: string);
      procedure SetReferencia(varReferencia: string);
      procedure SetDescripcion(varDescripcion: string);
      procedure SetCantidad(varCantidad: Double);
      procedure SetIdConteoInventario(varIdConteoInventario: Integer);
      function GetNumeroConteo: Integer;
      function GetCodigoProducto: string;
      function GetReferencia: string;
      function GetDescripcion: string;
      function GetCantidad: Double;
      function GetIdConteoInventario: Integer;
      function ObtenerIdConteoInventario(varIdConteoInventario: Integer): TClientDataSet;
      function EliminarIdConteoInventario(varIdConteoInventario: Integer): Boolean;
      function ObtenerIdConteoInventarioNumeroConteo(varIdConteoInventario: Integer; varNumeroConteo: Integer): TClientDataSet;
  end;

implementation

uses DB;

{ TDetConteoInventario }

procedure TDetConteoInventario.Ajustar;
begin
  inherited;

end;

procedure TDetConteoInventario.Mover(Ds: TClientDataSet);
begin
  NumeroConteo := Ds.FieldValues['numero_conteo'];
  CodigoProducto := Ds.FieldValues['codigo_producto'];
  Referencia := Ds.FieldValues['referencia'];
  Descripcion := Ds.FieldValues['descripcion'];
  Cantidad := Ds.FieldValues['cantidad'];
  IdConteoInventario := Ds.FieldValues['id_conteo_inventario'];
end;

function TDetConteoInventario.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,6);
  Params[0] := NumeroConteo;
  Params[1] := CodigoProducto;
  Params[2] := Referencia;
  Params[3] := Descripcion;
  Params[4] := Cantidad;
  Params[5] := IdConteoInventario;
  Result := Params;
end;

function TDetConteoInventario.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,3);
  Campos[0] := 'codigo_producto';
  Campos[1] := 'referencia';
  Campos[2] := 'descripcion';
  Result := Campos;
end;

function TDetConteoInventario.GetCondicion: string;
begin
  Result := '"id_conteo_inventario" = ' + IntToStr(IdConteoInventario) + ' AND ' +
            '"numero_conteo" = ' + IntToStr(NumeroConteo) + ' AND ' +
            '"codigo_producto" =' + QuotedStr(CodigoProducto);
end;

procedure TDetConteoInventario.AntesEliminar;
begin

end;

procedure TDetConteoInventario.AntesInsertar;
begin

end;

procedure TDetConteoInventario.AntesModificar;
begin

end;

procedure TDetConteoInventario.DespuesEliminar;
begin

end;

procedure TDetConteoInventario.DespuesInsertar;
begin

end;

procedure TDetConteoInventario.DespuesModificar;
begin

end;

constructor TDetConteoInventario.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_det_conteos_inventario');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(6)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "referencia"=$3, "descripcion"=$4, "cantidad"=$5 WHERE ("id_conteo_inventario"=$6) AND ("numero_conteo"=$1) AND ("codigo_producto"=$2)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("id_conteo_inventario"=$6) AND ("numero_conteo"=$1) AND ("codigo_producto"=$2)');
  SetLength(ClavesPrimarias,3);
  ClavesPrimarias[0] := '"id_conteo_inventario"';
  ClavesPrimarias[1] := '"numero_conteo"';
  ClavesPrimarias[2] := '"codigo_producto"';
  SetArrayClavesPrimarias(ClavesPrimarias);
  end;

procedure TDetConteoInventario.SetNumeroConteo(varNumeroConteo: Integer);
begin
  NumeroConteo := varNumeroConteo;
end;

procedure TDetConteoInventario.SetCodigoProducto(varCodigoProducto: string);
begin
  CodigoProducto := varCodigoProducto;
end;

procedure TDetConteoInventario.SetReferencia(varReferencia: string);
begin
  Referencia := varReferencia;
end;

procedure TDetConteoInventario.SetDescripcion(varDescripcion: string);
begin
  Descripcion := varDescripcion;
end;

procedure TDetConteoInventario.SetCantidad(varCantidad: Double);
begin
  Cantidad := varCantidad;
end;

procedure TDetConteoInventario.SetIdConteoInventario(varIdConteoInventario: Integer);
begin
  IdConteoInventario := varIdConteoInventario;
end;

function TDetConteoInventario.GetNumeroConteo: Integer;
begin
  Result := NumeroConteo;
end;

function TDetConteoInventario.GetCodigoProducto: string;
begin
  Result := CodigoProducto;
end;

function TDetConteoInventario.GetReferencia: string;
begin
  Result := Referencia;
end;

function TDetConteoInventario.GetDescripcion: string;
begin
  Result := Descripcion;
end;

function TDetConteoInventario.GetCantidad: Double;
begin
  Result := Cantidad;
end;

function TDetConteoInventario.GetIdConteoInventario: Integer;
begin
  Result := IdConteoInventario;
end;

function TDetConteoInventario.ObtenerIdConteoInventario(
  varIdConteoInventario: Integer): TClientDataSet;
begin
  Result := ObtenerListaCondicionSQL(' WHERE "id_conteo_inventario" = ' + IntToStr(varIdConteoInventario));
end;

function TDetConteoInventario.EliminarIdConteoInventario(
  varIdConteoInventario: Integer): Boolean;
begin
  Result := EjecutarSQL('DELETE FROM ' + GetTabla + ' WHERE "id_conteo_inventario" = ' + IntToStr(varIdConteoInventario));
end;

function TDetConteoInventario.ObtenerIdConteoInventarioNumeroConteo(
  varIdConteoInventario: Integer; varNumeroConteo: Integer): TClientDataSet;
begin
  Result := ObtenerListaCondicionSQL(' WHERE ("id_conteo_inventario" = ' + IntToStr(varIdConteoInventario) + ') AND ("numero_conteo" = ' + IntToStr(varNumeroConteo) + ')');
end;

end.
