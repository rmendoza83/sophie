unit ClsTipoComision;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TTipoComision }

  TTipoComision = class(TGenericoBD)
    private
      Codigo: string;
      Descripcion: string;
      Porcentaje: Double;
      CodigoDivision: string;
      CodigoLinea: string;
      CodigoFamilia: string;
      CodigoClase: string;
      CodigoManufactura: string;
      ComisionCantidad: Boolean;
      MontoCantidad: Double;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigo(varCodigo: string);
      procedure SetDescripcion(varDescricion: string);
      procedure SetPorcentaje(varPorcentaje: Double);
      procedure SetCodigoDivision(varCodigoDivision: string);
      procedure SetCodigoLinea(varCodigoLinea: string);
      procedure SetCodigoFamilia(varCodigoFamilia: string);
      procedure SetCodigoClase(varCodigoClase: string);
      procedure SetCodigoManufactura(varCodigoManufactura: string);
      procedure SetComisionCantidad(varComisionCantidad: Boolean);
      procedure SetMontoCantidad(varMontoCantidad: Double);
      function GetCodigo: string;
      function GetDescripcion: string;
      function GetPorcentaje: Double;
      function GetCodigoDivision: string;
      function GetCodigoLinea: string;
      function GetCodigoFamilia: string;
      function GetCodigoClase: string;
      function GetCodigoManufactura: string;
      function GetComisionCantidad: Boolean;
      function GetMontoCantidad: Double;
  end;

implementation

uses DB;

{ TTipoComision }

procedure TTipoComision.Ajustar;
begin

end;

procedure TTipoComision.Mover(Ds: TClientDataSet);
begin
  Codigo := Ds.FieldValues['codigo'];
  Descripcion := Ds.FieldValues['descripcion'];
  Porcentaje:= Ds.FieldValues['porcentaje'];
  CodigoDivision:= Ds.FieldValues['codigo_division'];
  CodigoLinea:= Ds.FieldValues['codigo_linea'];
  CodigoFamilia:= Ds.FieldValues['codigo_familia'];
  CodigoClase:= Ds.FieldValues['codigo_clase'];
  CodigoManufactura:= Ds.FieldValues['codigo_manufactura'];
  ComisionCantidad := Ds.FieldValues['comision_cantidad'];
  MontoCantidad := Ds.FieldValues['monto_cantidad'];
end;

function TTipoComision.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,10);
  Params[0] := Codigo;
  Params[1] := Descripcion;
  Params[2] := Porcentaje;
  Params[3] := CodigoDivision;
  Params[4] := CodigoLinea;
  Params[5] := CodigoFamilia;
  Params[6] := CodigoClase;
  Params[7] := CodigoManufactura;
  Params[8] := ComisionCantidad;
  Params[9] := MontoCantidad;
  Result := Params;
end;

function TTipoComision.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,7);
  Campos[0] := 'codigo';
  Campos[1] := 'descripcion';
  Campos[2] := 'codigo_division';
  Campos[3] := 'codigo_linea';
  Campos[4] := 'codigo_familia';
  Campos[5] := 'codigo_clase';
  Campos[6] := 'codigo_manufactura';
  Result := Campos;
end;

function TTipoComision.GetCondicion: string;
begin
  Result := '"codigo" = ' + QuotedStr(Codigo) ;

end;

procedure TTipoComision.AntesInsertar;
begin

end;

procedure TTipoComision.AntesModificar;
begin

end;

procedure TTipoComision.AntesEliminar;
begin

end;

procedure TTipoComision.DespuesInsertar;
begin

end;

procedure TTipoComision.DespuesModificar;
begin

end;

procedure TTipoComision.DespuesEliminar;
begin

end;

constructor TTipoComision.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_tipos_comisiones');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(10)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "descripcion"=$2, "porcentaje"=$3, "codigo_division"=$4, "codigo_linea"=$5, "codigo_familia"=$6, "codigo_clase"=$7, "codigo_manufactura"=$8, "comision_cantidad"=$9, "monto_cantidad"=$10 WHERE ("codigo"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + 'WHERE ("codigo"=$1)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"codigo"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TTipoComision.SetCodigo(varCodigo: string);
begin
  Codigo := varCodigo;
end;

procedure TTipoComision.SetDescripcion(varDescricion: string);
begin
  Descripcion := varDescricion;
end;

procedure TTipoComision.SetPorcentaje(varPorcentaje: Double);
begin
  Porcentaje := varPorcentaje;
end;

procedure TTipoComision.SetCodigoDivision(varCodigoDivision: string);
begin
  CodigoDivision := varCodigoDivision;
end;

procedure TTipoComision.SetCodigoLinea(varCodigoLinea: string);
begin
  CodigoLinea := varCodigoLinea;
end;

procedure TTipoComision.SetCodigoFamilia(varCodigoFamilia: string);
begin
  CodigoFamilia := varCodigoFamilia;
end;

procedure TTipoComision.SetCodigoClase(varCodigoClase: string);
begin
  CodigoClase := varCodigoClase;
end;

procedure TTipoComision.SetCodigoManufactura(varCodigoManufactura: string);
begin
  CodigoManufactura := varCodigoManufactura;
end;

procedure TTipoComision.SetComisionCantidad(varComisionCantidad: Boolean);
begin
  ComisionCantidad := varComisionCantidad;
end;

procedure TTipoComision.SetMontoCantidad(varMontoCantidad: Double);
begin
  MontoCantidad := varMontoCantidad;
end;

function TTipoComision.GetCodigo: string;
begin
  Result := Codigo;
end;

function TTipoComision.GetDescripcion: string;
begin
  Result := Descripcion;
end;

function TTipoComision.GetPorcentaje: Double;
begin
  Result := Porcentaje;
end;

function TTipoComision.GetCodigoDivision: string;
begin
  Result := CodigoDivision;
end;

function TTipoComision.GetCodigoLinea: string;
begin
  Result := CodigoLinea;
end;

function TTipoComision.GetCodigoFamilia: string;
begin
  Result := CodigoFamilia;
end;

function TTipoComision.GetCodigoClase: string;
begin
  Result := CodigoClase;
end;

function TTipoComision.GetCodigoManufactura: string;
begin
  Result := CodigoManufactura;
end;

function TTipoComision.GetComisionCantidad: Boolean;
begin
  Result := ComisionCantidad;
end;

function TTipoComision.GetMontoCantidad: Double;
begin
  Result := MontoCantidad;
end;

end.
