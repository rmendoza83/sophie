unit ClsEmpresa;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TEmpresa }

  TEmpresa = class(TGenericoBD)
    private
      Codigo: String;
      Rif: String;
      Nit: String;
      RazonSocial: String;
      DireccionFiscal: String;
      Telefonos: String;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigo(varCodigo: string);
      procedure SetRif(varRif: string);
      procedure SetNit(varNit: string);
      procedure SetRazonSocial(varRazonSocial: string);
      procedure SetDireccionFiscal(varDireccionFiscal: string);
      procedure SetTelefonos(varTelefonos: string);
      function GetCodigo: string;
      function GetRif: string;
      function GetNit: string;
      function GetRaZonSocial: string;
      function GetDireccionFsical: string;
      function GetTelefonos: string;
  end;

implementation

{ TEmpresa }

procedure TEmpresa.Ajustar;
begin

end;

procedure TEmpresa.Mover(Ds: TClientDataSet);
begin
  Codigo := Ds.FieldValues['codigo'];
  Rif := Ds.FieldValues['rif'];
  Nit := Ds.FieldValues['nit'];
  RazonSocial := Ds.FieldValues['razon_social'];
  DireccionFiscal := Ds.FieldValues['direccion_fiscal'];
  Telefonos := Ds.FieldValues['telefonos'];
end;

function TEmpresa.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,6);
  Params[0]:= Codigo;
  Params[1]:= Rif;
  Params[2]:= Nit;
  Params[3]:= RazonSocial;
  Params[4]:= DireccionFiscal;
  Params[5]:= Telefonos;
  Result:= Params;
end;

function TEmpresa.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,6);
  Campos[0] := 'codigo';
  Campos[1] := 'rif';
  Campos[2] := 'nit';
  Campos[3] := 'razon_social';
  Campos[4] := 'direccion_fiscal';
  Campos[5] := 'telefonos';
  Result:= Campos;
end;

function TEmpresa.GetCondicion: string;
begin

end;

procedure TEmpresa.AntesInsertar;
begin

end;

procedure TEmpresa.AntesModificar;
begin

end;

procedure TEmpresa.AntesEliminar;
begin

end;

procedure TEmpresa.DespuesInsertar;
begin

end;

procedure TEmpresa.DespuesModificar;
begin

end;

procedure TEmpresa.DespuesEliminar;
begin

end;

constructor TEmpresa.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_empresas');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(6)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "rif"=$2,"nit"=$3,"razonsocial"=$4,"direccionfiscal"=$5,"telefonos"=$6, WHERE ("codigo"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("codigo"=$1)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"codigo"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TEmpresa.SetCodigo(varCodigo: string);
begin
  Codigo := varCodigo;
end;

procedure TEmpresa.SetDireccionFiscal(varDireccionFiscal: string);
begin
  DireccionFiscal := varDireccionFiscal;
end;

procedure TEmpresa.SetNit(varNit: string);
begin
  Nit := varNit;
end;

procedure TEmpresa.SetRazonSocial(varRazonSocial: string);
begin
  RazonSocial := varRazonSocial;
end;

procedure TEmpresa.SetRif(varRif: string);
begin
  Rif := varRif;
end;

procedure TEmpresa.SetTelefonos(varTelefonos: string);
begin
  Telefonos := varTelefonos;
end;

function TEmpresa.GetCodigo: string;
begin
  Result := Codigo;
end;

function TEmpresa.GetDireccionFsical: string;
begin
  Result := DireccionFiscal;
end;

function TEmpresa.GetNit: string;
begin
  Result := Nit;
end;

function TEmpresa.GetRaZonSocial: string;
begin
  Result := RazonSocial;
end;

function TEmpresa.GetRif: string;
begin
  Result := Rif;
end;

function TEmpresa.GetTelefonos: string;
begin
  Result := Telefonos;
end;

end.
