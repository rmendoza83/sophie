unit ClsLiquidacionCobranza;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TLiquidacionCobranza }

  TLiquidacionCobranza = class(TGenericoBD)
    private
      Numero: string;
      CodigoMoneda: string;
      CodigoCliente: string;
      CodigoZona: string;
      FechaIngreso: TDate;
      FechaContable: TDate;
      Observaciones: string;
      MontoNeto: Double;
      TotalRetencion: Double;
      Total: Double;
      CodigoFormaPago: string;
      NumeroFormaPago: string;
      CodigoCuentaBancaria: string;
      NumeroDocumentoBancario: string;
      CodigoBancoCobranza: string;
      FechaOperacionBancaria: TDate;
      IdLiquidacionCobranza: Integer;
      IdOperacionBancaria: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetNumero(varNumero: string);
      procedure SetCodigoMoneda(varCodigoMoneda: string);
      procedure SetCodigoCliente(varCodigoCliente: string);
      procedure SetCodigoZona(varCodigoZona: string);
      procedure SetFechaIngreso(varFechaIngreso: TDate);
      procedure SetFechaContable(varFechaContable: TDate);
      procedure SetObservaciones(varObservaciones: string);
      procedure SetMontoNeto(varMontoNeto: Double);
      procedure SetTotalRetencion(varTotalRetencion: Double);
      procedure SetTotal(varTotal: Double);
      procedure SetCodigoFormaPago(varCodigoFormaPago: string);
      procedure SetNumeroFormaPago(varNumeroFormaPago: string);
      procedure SetCodigoCuentaBancaria(varCodigoCuentaBancaria: string);
      procedure SetNumeroDocumentoBancario(varNumeroDocumentoBancario: string);
      procedure SetCodigoBancoCobranza(varCodigoBancoCobranza: string);
      procedure SetFechaOperacionBancaria(varFechaOperacionBancaria: TDate);
      procedure SetIdLiquidacionCobranza(varIdLiquidacionCobranza: Integer);
      procedure SetIdOperacionBancaria(varIdOperacionBancaria: Integer);
      function GetNumero: string;
      function GetCodigoMoneda: string;
      function GetCodigoCliente: string;
      function GetCodigoZona: string;
      function GetFechaIngreso: TDate;
      function GetFechaContable: TDate;
      function GetObservaciones: string;
      function GetMontoNeto: Double;
      function GetTotalRetencion: Double;
      function GetTotal: Double;
      function GetCodigoFormaPago: string;
      function GetNumeroFormaPago: string;
      function GetCodigoCuentaBancaria: string;
      function GetNumeroDocumentoBancario: string;
      function GetCodigoBancoCobranza: string;
      function GetFechaOperacionBancaria: TDate;
      function GetIdLiquidacionCobranza: Integer;
      function GetIdOperacionBancaria: Integer;
      function BuscarNumero: Boolean;
  end;

implementation

uses
  DB,
  ClsConsecutivo;

{ TLiquidacionCobranza }

procedure TLiquidacionCobranza.Ajustar;
begin

end;

procedure TLiquidacionCobranza.Mover(Ds: TClientDataSet);
begin
  Numero := Ds.FieldValues['numero'];
  CodigoMoneda := Ds.FieldValues['codigo_moneda'];
  CodigoCliente := Ds.FieldValues['codigo_cliente'];
  CodigoZona := Ds.FieldValues['codigo_zona'];
  FechaIngreso := Ds.FieldByName('fecha_ingreso').AsDateTime;
  FechaContable := Ds.FieldByName('fecha_contable').AsDateTime;
  Observaciones := Ds.FieldValues['observaciones'];
  MontoNeto := Ds.FieldValues['monto_neto'];
  TotalRetencion := Ds.FieldValues['total_retencion'];
  Total := Ds.FieldValues['total'];
  CodigoFormaPago := Ds.FieldValues['codigo_forma_pago'];
  NumeroFormaPago := Ds.FieldValues['numero_forma_pago'];
  CodigoCuentaBancaria := Ds.FieldValues['codigo_cuenta_bancaria'];
  NumeroDocumentoBancario := Ds.FieldValues['numero_documento_bancario'];
  CodigoBancoCobranza := Ds.FieldValues['codigo_banco_cobranza'];
  FechaOperacionBancaria := Ds.FieldByName('fecha_operacion_bancaria').AsDateTime;
  IdLiquidacionCobranza := Ds.FieldValues['id_liquidacion_cobranza'];
  IdOperacionBancaria := Ds.FieldValues['id_operacion_bancaria'];
end;

function TLiquidacionCobranza.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,18);
  Params[0] := Numero;
  Params[1] := CodigoMoneda;
  Params[2] := CodigoCliente;
  Params[3] := CodigoZona;
  Params[4] := VarFromDateTime(FechaIngreso);
  Params[5] := VarFromDateTime(FechaContable);
  Params[6] := Observaciones;
  Params[7] := MontoNeto;
  Params[8] := TotalRetencion;
  Params[9] := Total;
  Params[10] := CodigoFormaPago;
  Params[11] := NumeroFormaPago;
  Params[12] := CodigoCuentaBancaria;
  Params[13] := NumeroDocumentoBancario;
  Params[14] := CodigoBancoCobranza;
  Params[15] := VarFromDateTime(FechaOperacionBancaria);
  Params[16] := IdLiquidacionCobranza;
  Params[17] := IdOperacionBancaria;
  Result := Params;
end;

function TLiquidacionCobranza.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,6);
  Campos[0] := 'numero';
  Campos[1] := 'codigo_moneda';
  Campos[2] := 'codigo_cliente';
  Campos[3] := 'codigo_zona';
  Campos[4] := 'observaciones';
  Campos[5] := 'numero_documento_bancario';
  Result := Campos;
end;

function TLiquidacionCobranza.GetCondicion: string;
begin
  Result := '"id_liquidacion_cobranza" = ' + IntToStr(IdLiquidacionCobranza);
end;

procedure TLiquidacionCobranza.AntesInsertar;
var
  Consecutivo: TConsecutivo;
begin
  Consecutivo := TConsecutivo.Create;
  Numero := IntToStr(Consecutivo.ObtenerConsecutivo('numero_liquidacion_cobranza'));
  IdLiquidacionCobranza := Consecutivo.ObtenerConsecutivo('id_liquidacion_cobranza');
end;

procedure TLiquidacionCobranza.AntesModificar;
begin

end;

procedure TLiquidacionCobranza.AntesEliminar;
begin

end;

procedure TLiquidacionCobranza.DespuesInsertar;
begin

end;

procedure TLiquidacionCobranza.DespuesEliminar;
begin

end;

procedure TLiquidacionCobranza.DespuesModificar;
begin

end;

constructor TLiquidacionCobranza.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_liquidacion_cobranzas');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(18)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "numero"=$1,"codigo_moneda"=$2, "codigo_cliente"=$3, "codigo_zona"=$4, "fecha_ingreso"=$5, "fecha_contable"=$6, "observaciones"=$7, "monto_neto"=$8, "total_retencion"=$9, "total"=$10, ' +
                  '"codigo_forma_pago"=$11, "numero_forma_pago"=$12, "codigo_cuenta_bancaria"=$13, "numero_documento_bancario"=$14, "codigo_banco_cobranza"=$15, "fecha_operacion_bancaria"=$16, "id_operacion_bancaria"=$18 WHERE ("id_liquidacion_cobranza"=$17)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("id_liquidacion_cobranza"=$17)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"id_liquidacion_cobranza"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TLiquidacionCobranza.SetNumero(varNumero: string);
begin
  Numero := varNumero;
end;

procedure TLiquidacionCobranza.SetCodigoMoneda(varCodigoMoneda: string);
begin
  CodigoMoneda := varCodigoMoneda;
end;

procedure TLiquidacionCobranza.SetCodigoCliente(varCodigoCliente: string);
begin
  CodigoCliente := varCodigoCliente;
end;

procedure TLiquidacionCobranza.SetCodigoZona(varCodigoZona: string);
begin
  CodigoZona := varCodigoZona;
end;

procedure TLiquidacionCobranza.SetFechaIngreso(varFechaIngreso: TDate);
begin
  FechaIngreso := varFechaIngreso;
end;

procedure TLiquidacionCobranza.SetFechaContable(varFechaContable: TDate);
begin
  FechaContable := varFechaContable;
end;

procedure TLiquidacionCobranza.SetObservaciones(varObservaciones: string);
begin
  Observaciones := varObservaciones;
end;

procedure TLiquidacionCobranza.SetMontoNeto(varMontoNeto: Double);
begin
  MontoNeto := varMontoNeto;
end;

procedure TLiquidacionCobranza.SetTotalRetencion(varTotalRetencion: Double);
begin
  TotalRetencion := varTotalRetencion;
end;

procedure TLiquidacionCobranza.SetTotal(varTotal: Double);
begin
  Total := varTotal;
end;

procedure TLiquidacionCobranza.SetCodigoFormaPago(varCodigoFormaPago: string);
begin
  CodigoFormaPago := varCodigoFormaPago;
end;

procedure TLiquidacionCobranza.SetNumeroFormaPago(varNumeroFormaPago: string);
begin
  NumeroFormaPago := varNumeroFormaPago; 
end;

procedure TLiquidacionCobranza.SetCodigoCuentaBancaria(varCodigoCuentaBancaria: string);
begin
  CodigoCuentaBancaria := varCodigoCuentaBancaria;
end;

procedure TLiquidacionCobranza.SetNumeroDocumentoBancario(varNumeroDocumentoBancario: string);
begin
  NumeroDocumentoBancario := varNumeroDocumentoBancario;
end;

procedure TLiquidacionCobranza.SetCodigoBancoCobranza(varCodigoBancoCobranza: string);
begin
  CodigoBancoCobranza := varCodigoBancoCobranza;
end;

procedure TLiquidacionCobranza.SetFechaOperacionBancaria(varFechaOperacionBancaria: TDate);
begin
  FechaOperacionBancaria := varFechaOperacionBancaria;
end;

procedure TLiquidacionCobranza.SetIdLiquidacionCobranza(varIdLiquidacionCobranza: Integer);
begin
  IdLiquidacionCobranza := varIdLiquidacionCobranza;
end;

procedure TLiquidacionCobranza.SetIdOperacionBancaria(varIdOperacionBancaria: Integer);
begin
  IdOperacionBancaria := varIdOperacionBancaria;
end;

function TLiquidacionCobranza.GetNumero: string;
begin
  Result := Numero;
end;

function TLiquidacionCobranza.GetCodigoMoneda: string;
begin
  Result := CodigoMoneda;
end;

function TLiquidacionCobranza.GetCodigoCliente: string;
begin
  Result := CodigoCliente;
end;

function TLiquidacionCobranza.GetCodigoZona: string;
begin
  Result := CodigoZona;
end;

function TLiquidacionCobranza.GetFechaIngreso: TDate;
begin
  Result := FechaIngreso;
end;

function TLiquidacionCobranza.GetFechaContable: TDate;
begin
  Result := FechaContable;
end;

function TLiquidacionCobranza.GetObservaciones: string;
begin
  Result := Observaciones;
end;

function TLiquidacionCobranza.GetMontoNeto: Double;
begin
  Result := MontoNeto;
end;

function TLiquidacionCobranza.GetTotalRetencion: Double;
begin
  Result := TotalRetencion;
end;

function TLiquidacionCobranza.GetTotal: Double;
begin
  Result := Total;
end;

function TLiquidacionCobranza.GetCodigoFormaPago: string;
begin
  Result := CodigoFormaPago;
end;

function TLiquidacionCobranza.GetNumeroFormaPago: string;
begin
  Result := NumeroFormaPago;
end;

function TLiquidacionCobranza.GetCodigoCuentaBancaria: string;
begin
  Result := CodigoCuentaBancaria;
end;

function TLiquidacionCobranza.GetNumeroDocumentoBancario: string;
begin
  Result := NumeroDocumentoBancario;
end;

function TLiquidacionCobranza.GetCodigoBancoCobranza: string;
begin
  Result := CodigoBancoCobranza;
end;

function TLiquidacionCobranza.GetFechaOperacionBancaria: TDate;
begin
  Result := FechaOperacionBancaria;
end;

function TLiquidacionCobranza.GetIdLiquidacionCobranza: Integer;
begin
  Result := IdLiquidacionCobranza;
end;

function TLiquidacionCobranza.GetIdOperacionBancaria: Integer;
begin
  Result := IdOperacionBancaria;
end;

function TLiquidacionCobranza.BuscarNumero: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("numero" = ' + QuotedStr(GetNumero) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

end.
