unit ClsTallerServicio;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TTallerServicio }

  TTallerServicio = class(TGenericoBD)
    private
      Numero: string;
      CodigoCliente: string;
      PlacaVehiculoCliente: string;
      CodigoVendedor: string;
      CodigoZona: string;
      FechaIngreso: TDate;
      Kilometraje: Double;
      Concepto: string;
      Observaciones: string;
      MontoNeto: Double;
      MontoDescuento: Double;
      SubTotal: Double;
      Impuesto: Double;
      Total: Double;
      PDescuento: Double;
      PIva: Double;
      FormaLibre: Boolean;
      LoginUsuario: string;
      TiempoIngreso: TDateTime;
      IdTallerServicio: Integer;
      IdFacturacionCliente: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArraystring; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetNumero(varNumero: string);
      procedure SetCodigoCliente(varCodigoCliente: string);
      procedure SetPlacaVehiculoCliente(varPlacaVehiculoCliente: string);
      procedure SetCodigoVendedor(varCodigoVendedor: string);
      procedure SetCodigoZona(varCodigoZona: string);
      procedure SetFechaIngreso(varFechaIngreso: TDate);
      procedure SetKilometraje(varKilometraje: Double);
      procedure SetConcepto(varConcepto: string);
      procedure SetObservaciones(varObservaciones: string);
      procedure SetMontoNeto(varMontoNeto: Double);
      procedure SetMontoDescuento(varMontoDescuento: Double);
      procedure SetSubTotal(varSubTotal: Double);
      procedure SetImpuesto(varImpuesto: Double);
      procedure SetTotal(varTotal: Double);
      procedure SetPDescuento(varPDescuento: Double);
      procedure SetPIva(varPIva: Double);
      procedure SetFormaLibre(varFormaLibre: Boolean);
      procedure SetLoginUsuario(varLoginUsuario: string);
      procedure SetTiempoIngreso(varTiempodeIngreso: TDateTime);
      procedure SetIdTallerServicio(varIdTallerServicio: Integer);
      procedure SetIdFacturacionCliente(varIdFacturacionCliente: Integer);
      function GetNumero: string;
      function GetCodigoCliente: string;
      function GetPlacaVehiculoCliente: string;
      function GetCodigoVendedor: string;
      function GetCodigoZona: string;
      function GetFechaIngreso: TDate;
      function GetKilometraje: Double;
      function GetConcepto: string;
      function GetObservaciones: string;
      function GetMontoNeto: Double;
      function GetMontoDescuento: Double;
      function GetSubTotal: Double;
      function GetImpuesto: Double;
      function GetTotal: Double;
      function GetPDescuento: Double;
      function GetPIva: Double;
      function GetFormaLibre: Boolean;
      function GetLoginUsuario: string;
      function GetTiempoIngreso: TDateTime;
      function GetIdTallerServicio: Integer;
      function GetIdFacturacionCliente: Integer;
      function BuscarNumero: Boolean;
      function BuscarCodigoCliente: Boolean;
      function BuscarCodigoVendedor: Boolean;
      function BuscarIdFacturacionCliente: Boolean;
  end;

implementation

uses
  DB,
  ClsConsecutivo;

{ TFacturacionCliente }

procedure TTallerServicio.Ajustar;
begin

end;

procedure TTallerServicio.Mover(Ds: TClientDataSet);
begin
  Numero := Ds.FieldValues['numero'];
  CodigoCliente := Ds.FieldValues['codigo_cliente'];
  PlacaVehiculoCliente := Ds.FieldValues['placa_vehiculo_cliente'];
  CodigoVendedor := Ds.FieldValues['codigo_vendedor'];
  CodigoZona := Ds.FieldValues['codigo_zona'];
  FechaIngreso := Ds.FieldByName('fecha_ingreso').AsDateTime;
  Kilometraje := Ds.FieldValues['kilometraje'];
  Concepto := Ds.FieldValues['concepto'];
  Observaciones := Ds.FieldValues['observaciones'];
  MontoNeto := Ds.FieldValues['monto_neto'];
  MontoDescuento := Ds.FieldValues['monto_descuento'];
  SubTotal := Ds.FieldValues['sub_total'];
  Impuesto := Ds.FieldValues['impuesto'];
  Total := Ds.FieldValues['total'];
  PDescuento := Ds.FieldValues['p_descuento'];
  PIva := Ds.FieldValues['p_iva'];
  FormaLibre := Ds.FieldValues['forma_libre'];
  LoginUsuario := Ds.FieldValues['login_usuario'];
  TiempoIngreso := Ds.FieldByName('tiempo_ingreso').AsDateTime;
  IdTallerServicio := Ds.FieldValues['id_taller_servicio'];
  IdFacturacionCliente := Ds.FieldValues['id_facturacion_cliente'];
end;

function TTallerServicio.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,21);
  Params[0] := Numero;
  Params[1] := CodigoCliente;
  Params[2] := PlacaVehiculoCliente;
  Params[3] := CodigoVendedor;
  Params[4] := CodigoZona;
  Params[5] := VarFromDateTime(FechaIngreso);
  Params[6] := Kilometraje;
  Params[7] := Concepto;
  Params[8] := Observaciones;
  Params[9] := MontoNeto;
  Params[10] := MontoDescuento;
  Params[11] := SubTotal;
  Params[12] := Impuesto;
  Params[13] := Total;
  Params[14] := PDescuento;
  Params[15] := PIva;
  Params[16] := FormaLibre;
  Params[17] := LoginUsuario;
  Params[18] := VarFromDateTime(TiempoIngreso);
  Params[19] := IdTallerServicio;
  Params[20] := IdFacturacionCliente;
  Result := Params;
end;

function TTallerServicio.GetCampos: TArraystring;
var
  Campos: TArraystring;
begin
  SetLength(Campos,7);
  Campos[0] := 'numero';
  Campos[1] := 'codigo_cliente';
  Campos[2] := 'codigo_vendedor';
  Campos[3] := 'codigo_zona';
  Campos[4] := 'concepto';
  Campos[5] := 'observaciones';
  Campos[6] := 'login_usuario';
  Result := Campos;
end;

function TTallerServicio.GetCondicion: string;
begin
  Result:= '"id_taller_servicio" =' + IntToStr(IdTallerServicio);
end;

procedure TTallerServicio.AntesInsertar;
var
  Consecutivo: TConsecutivo;
begin
  Consecutivo := TConsecutivo.Create;
  Numero := IntToStr(Consecutivo.ObtenerConsecutivo('numero_taller_servicio'));
  IdTallerServicio := Consecutivo.ObtenerConsecutivo('id_taller_servicio');
end;

procedure TTallerServicio.AntesModificar;
begin

end;

procedure TTallerServicio.AntesEliminar;
begin

end;

procedure TTallerServicio.DespuesInsertar;
begin

end;

procedure TTallerServicio.DespuesModificar;
begin

end;

procedure TTallerServicio.DespuesEliminar;
begin

end;

constructor TTallerServicio.Create;
var
  ClavesPrimarias: TArraystring;
begin
  inherited Create;
  SetTabla('tbl_taller_servicio');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(21)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "numero"=$1, "codigo_cliente"=$2, "placa_vehiculo_cliente"=$3, "codigo_vendedor"=$4, "codigo_zona"=$5, "fecha_ingreso"=$6, "kilometraje"=$7, "concepto"=$8, "observaciones"=$9, "monto_neto"=$10, ' +
                  '"monto_descuento"=$11, "sub_total"=$12, "impuesto"=$13, "total"=$14, "p_descuento"=$15, "p_iva"=$16, "forma_libre"=$17, "login_usuario"=$18, "tiempo_ingreso"=$19, "id_facturacion_cliente"=$21' +
                  ' WHERE ("id_taller_servicio"=$20)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("id_taller_servicio"=$20)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"id_taller_servicio"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TTallerServicio.SetCodigoCliente(varCodigoCliente: string);
begin
 CodigoCliente := varCodigoCliente;
end;


procedure TTallerServicio.SetCodigoVendedor(varCodigoVendedor: string);
begin
  CodigoVendedor := varCodigoVendedor;
end;

procedure TTallerServicio.SetCodigoZona(varCodigoZona: string);
begin
  CodigoZona := varCodigoZona;
end;

procedure TTallerServicio.SetConcepto(varConcepto: string);
begin
  Concepto := varConcepto;
end;

procedure TTallerServicio.SetFechaIngreso(varFechaIngreso: TDate);
begin
  FechaIngreso := varFechaIngreso;
end;

procedure TTallerServicio.SetFormaLibre(varFormaLibre: Boolean);
begin
  FormaLibre := varFormaLibre;
end;

procedure TTallerServicio.SetIdFacturacionCliente(varIdFacturacionCliente: Integer);
begin
  IdFacturacionCliente := varIdFacturacionCliente;
end;

procedure TTallerServicio.SetIdTallerServicio(varIdTallerServicio: Integer);
begin
  IdTallerServicio := varIdTallerServicio;
end;

procedure TTallerServicio.SetImpuesto(varImpuesto: Double);
begin
  Impuesto := varImpuesto;
end;

procedure TTallerServicio.SetKilometraje(varKilometraje: Double);
begin
  Kilometraje := varKilometraje;
end;

procedure TTallerServicio.SetLoginUsuario(varLoginUsuario: string);
begin
  LoginUsuario := varLoginUsuario;
end;

procedure TTallerServicio.SetMontoDescuento(varMontoDescuento: Double);
begin
  MontoDescuento := varMontoDescuento;
end;

procedure TTallerServicio.SetMontoNeto(varMontoNeto: Double);
begin
  MontoNeto := varMontoNeto;
end;

procedure TTallerServicio.SetNumero(varNumero: string);
begin
  Numero := varNumero;
end;

procedure TTallerServicio.SetObservaciones(varObservaciones: string);
begin
  Observaciones := varObservaciones;
end;

procedure TTallerServicio.SetPDescuento(varPDescuento: Double);
begin
  PDescuento := varPDescuento;
end;

procedure TTallerServicio.SetPIva(varPIva: Double);
begin
  PIva := varPIva;
end;

procedure TTallerServicio.SetPlacaVehiculoCliente(
  varPlacaVehiculoCliente: string);
begin
  PlacaVehiculoCliente := varPlacaVehiculoCliente;
end;

procedure TTallerServicio.SetSubTotal(varSubTotal: Double);
begin
  SubTotal := varSubTotal;
end;

procedure TTallerServicio.SetTiempoIngreso(varTiempodeIngreso: TDateTime);
begin
  TiempoIngreso := varTiempodeIngreso;
end;

procedure TTallerServicio.SetTotal(varTotal: Double);
begin
  Total := varTotal;
end;

function TTallerServicio.GetCodigoCliente: string;
begin
  Result := CodigoCliente
end;

function TTallerServicio.GetCodigoVendedor: string;
begin
  Result := CodigoVendedor;
end;

function TTallerServicio.GetCodigoZona: string;
begin
  Result := CodigoZona;
end;

function TTallerServicio.GetConcepto: string;
begin
  Result := Concepto;
end;

function TTallerServicio.GetFechaIngreso: TDate;
begin
  Result := FechaIngreso;
end;

function TTallerServicio.GetFormaLibre: Boolean;
begin
  Result := FormaLibre;
end;

function TTallerServicio.GetIdFacturacionCliente: Integer;
begin
  Result := IdFacturacionCliente;
end;

function TTallerServicio.GetIdTallerServicio: Integer;
begin
  Result := IdTallerServicio;
end;

function TTallerServicio.GetImpuesto: Double;
begin
  Result := Impuesto;
end;

function TTallerServicio.GetKilometraje: Double;
begin
  Result := Kilometraje;
end;

function TTallerServicio.GetLoginUsuario: string;
begin
  Result := LoginUsuario;
end;

function TTallerServicio.GetMontoDescuento: Double;
begin
  Result := MontoDescuento;
end;

function TTallerServicio.GetMontoNeto: Double;
begin
  Result := MontoNeto;
end;

function TTallerServicio.GetNumero: string;
begin
  Result := Numero;
end;

function TTallerServicio.GetObservaciones: string;
begin
  Result := Observaciones;
end;

function TTallerServicio.GetPDescuento: Double;
begin
  Result := PDescuento;
end;

function TTallerServicio.GetPIva: Double;
begin
  Result := PIva;
end;

function TTallerServicio.GetPlacaVehiculoCliente: string;
begin
  Result := PlacaVehiculoCliente;
end;

function TTallerServicio.GetSubTotal: Double;
begin
  Result := SubTotal;
end;

function TTallerServicio.GetTiempoIngreso: TDateTime;
begin
  Result := TiempoIngreso;
end;

function TTallerServicio.GetTotal: Double;
begin
  Result := Total;
end;

function TTallerServicio.BuscarNumero: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("numero" = ' + QuotedStr(GetNumero) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

function TTallerServicio.BuscarCodigoCliente: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("codigo_cliente" = ' + QuotedStr(GetCodigoCliente) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

function TTallerServicio.BuscarCodigoVendedor: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("codigo_vendedor" = ' + QuotedStr(GetCodigoVendedor) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

function TTallerServicio.BuscarIdFacturacionCliente: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("id_facturacion_cliente" = ' + IntToStr(GetIdFacturacionCliente) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

end.
