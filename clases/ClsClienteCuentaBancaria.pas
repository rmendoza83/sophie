unit ClsClienteCuentaBancaria;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TClienteCuentaBancaria }

  TClienteCuentaBancaria = class(TGenericoBD)
    private
      CodigoCliente : string;
      NumeroCuenta: string;
      Entidad: string;
      Tipo: string;
      Email: string;
      Observaciones: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigoCliente(varCodigoCliente: string);
      procedure SetNumeroCuenta(varNumeroCuenta: string);
      procedure SetEntidad(varEntidad: string);
      procedure SetTipo(varTipo: string);
      procedure SetEmail(varEmail: string);
      procedure SetObservaciones(varObservaciones: string);
      function GetCodigoCliente: string;
      function GetNumeroCuenta: string;
      function GetEntidad: string;
      function GetTipo: string;
      function GetEmail: string;
      function GetObservaciones: string;
      function ObtenerCodigoCliente(varCodigoCliente: string): TClientDataSet;
      function EliminarCodigoCliente(varCodigoCliente: string): Boolean;
      function ObtenerComboCodigoCliente(varCodigoCliente: string): TClientDataSet;
  end;

implementation

uses
  ClsConsecutivo, DB;

{ TClienteCuentaBancaria }

procedure TClienteCuentaBancaria.Ajustar;
begin

end;

procedure TClienteCuentaBancaria.Mover(Ds: TClientDataSet);
begin
  CodigoCliente := Ds.FieldValues['codigo_cliente'];
  NumeroCuenta := Ds.FieldValues['numero_cuenta'];
  Entidad := Ds.FieldValues['entidad'];
  Tipo := Ds.FieldValues['tipo'];
  Email := Ds.FieldValues['email'];
  Observaciones := Ds.FieldValues['observaciones'];
end;

function TClienteCuentaBancaria.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,6);
  Params[0] := CodigoCliente;
  Params[1] := NumeroCuenta;
  Params[2] := Entidad;
  Params[3] := Tipo;
  Params[4] := Email;
  Params[5] := Observaciones;
  Result := Params;
end;

function TClienteCuentaBancaria.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,6);
  Campos[0] := 'codigo_cliente';
  Campos[1] := 'numero_cuenta';
  Campos[2] := 'entidad';
  Campos[3] := 'tipo';
  Campos[4] := 'email';
  Campos[5] := 'observaciones';
  Result := Campos;
end;

function TClienteCuentaBancaria.GetCondicion: string;
begin
  Result := '"codigo_cliente" = ' + QuotedStr(CodigoCliente) + ' AND ' +
            '"numero_cuenta" = ' + QuotedStr(NumeroCuenta);
end;

procedure TClienteCuentaBancaria.AntesInsertar;
begin

end;

procedure TClienteCuentaBancaria.AntesModificar;
begin

end;

procedure TClienteCuentaBancaria.AntesEliminar;
begin

end;

procedure TClienteCuentaBancaria.DespuesInsertar;
begin

end;

procedure TClienteCuentaBancaria.DespuesModificar;
begin

end;

procedure TClienteCuentaBancaria.DespuesEliminar;
begin

end;

constructor TClienteCuentaBancaria.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_clientes_cuentas_bancarias');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(6)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "entidad"=$3, "tipo"=$4, "email"=$5, "observaciones"=$6  WHERE ("codigo_cliente"=$1) AND ("numero_cuenta"=$2)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("codigo_cliente"=$1) AND ("numero_cuenta"=$2)');
  SetLength(ClavesPrimarias,2);
  ClavesPrimarias[0] := '"codigo_cliente"';
  ClavesPrimarias[1] := '"numero_cuenta"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TClienteCuentaBancaria.SetCodigoCliente(varCodigoCliente: string);
begin
  CodigoCliente := varCodigoCliente;
end;

procedure TClienteCuentaBancaria.SetNumeroCuenta(varNumeroCuenta: string);
begin
  NumeroCuenta := varNumeroCuenta;
end;

procedure TClienteCuentaBancaria.SetEntidad(varEntidad: string);
begin
  Entidad := varEntidad;
end;

procedure TClienteCuentaBancaria.SetTipo(varTipo: string);
begin
  Tipo := varTipo;
end;

procedure TClienteCuentaBancaria.SetEmail(varEmail: string);
begin
  Email := varEmail;
end;

procedure TClienteCuentaBancaria.SetObservaciones(varObservaciones: string);
begin
  Observaciones := varObservaciones;
end;

function TClienteCuentaBancaria.GetCodigoCliente: string;
begin
  Result := CodigoCliente;
end;

function TClienteCuentaBancaria.GetNumeroCuenta: string;
begin
  Result := NumeroCuenta;
end;

function TClienteCuentaBancaria.GetEntidad: string;
begin
  Result := Entidad;
end;

function TClienteCuentaBancaria.GetTipo: string;
begin
  Result := Tipo;
end;

function TClienteCuentaBancaria.GetEmail: string;
begin
  Result := Email;
end;

function TClienteCuentaBancaria.GetObservaciones: string;
begin
  Result := Observaciones;
end;

function TClienteCuentaBancaria.ObtenerCodigoCliente(varCodigoCliente : string): TClientDataSet;
begin
  Result := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("codigo_cliente" = ' + QuotedStr(varCodigoCliente) + ')');
end;

function TClienteCuentaBancaria.EliminarCodigoCliente(
  varCodigoCliente: string): Boolean;
begin
  Result := EjecutarSQL('DELETE FROM ' + GetTabla + ' WHERE ("codigo_cliente" = ' + QuotedStr(varCodigoCliente) + ')');
end;

function TClienteCuentaBancaria.ObtenerComboCodigoCliente(
  varCodigoCliente: string): TClientDataSet;
begin
  Result := BD.EjecutarQueryCliDs('SELECT "numero_cuenta", "entidad", "tipo" FROM ' + GetTabla + ' WHERE ("codigo_cliente" = ' + QuotedStr(varCodigoCliente) + ')');
end;

end.

