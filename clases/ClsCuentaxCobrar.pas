unit ClsCuentaxCobrar;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TCuentaxCobrar }

  TCuentaxCobrar = class(TGenericoBD)
    private
      Desde: string;
      NumeroDocumento: string;
      CodigoMoneda: string;
      CodigoCliente: string;
      CodigoVendedor: string;
      CodigoZona: string;
      FechaIngreso: TDate;
      FechaRecepcion: TDate;
      FechaVencIngreso: TDate;
      FechaVencRecepcion: TDate;
      FechaLibros: TDate;
      FechaContable: TDate;
      FechaCobranza: TDate;
      NumeroCuota: Integer;
      TotalCuotas: Integer;
      OrdenCobranza: Integer;
      MontoNeto: Double;
      Impuesto: Double;
      Total: Double;
      PIva: Double;
      IdCobranza: Integer;
      IdContadoCobranza: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetDesde(varDesde: string);
      procedure SetNumeroDocumento(varNumeroDocumento: string);
      procedure SetCodigoMoneda(varCodigoMoneda: string);
      procedure SetCodigoCliente(varCodigoCliente: string);
      procedure SetCodigoVendedor(varCodigoVendedor: string);
      procedure SetCodigoZona(varCodigoZona: string);
      procedure SetFechaIngreso(varFechaIngreso: TDate);
      procedure SetFechaRecepcion(varFechaRecepcion: TDate);
      procedure SetFechaVencIngreso(varFechaVencIngreso: TDate);
      procedure SetFechaVencRecepcion(varFechaVencRecepcion: TDate);
      procedure SetFechaLibros(varFechaLibros: TDate);
      procedure SetFechaContable(varFechaContable: TDate);
      procedure SetFechaCobranza(varFechaCobranza: TDate);
      procedure SetNumeroCuota(varNumeroCuota: Integer);
      procedure SetTotalCuotas(varTotalCuota: Integer);
      procedure SetOrdenCobranza(varOrdenCobranza: Integer);
      procedure SetMontoNeto(varMontoNeto: Double);
      procedure SetImpuesto(varImpuesto: Double);
      procedure SetTotal(varTotal: Double);
      procedure SetPIva(varPIva: Double);
      procedure SetIdCobranza(varIdCobranza: Integer);
      procedure SetIdContadoCobranza(varIdContadoCobranza: Integer);
      function GetDesde: string;
      function GetNumeroDocumento: string;
      function GetCodigoMoneda: string;
      function GetCodigoCliente: string;
      function GetCodigoVendedor: string;
      function GetCodigoZona: string;
      function GetFechaIngreso: TDate;
      function GetFechaRecepcion: TDate;
      function GetFechaVencIngreso: TDate;
      function GetFechaVencRecepcion: TDate;
      function GetFechaLibros: TDate;
      function GetFechaContable: TDate;
      function GetFechaCobranza: TDate;
      function GetNumeroCuota: Integer;
      function GetTotalCuotas: Integer;
      function GetOrdenCobranza: Integer;
      function GetMontoNeto: Double;
      function GetImpuesto: Double;
      function GetTotal: Double;
      function GetPIva: Double;
      function GetIdCobranza: Integer;
      function GetIdContadoCobranza: Integer;
      function EliminarIdCobranza(varIdCobranza: Integer): Boolean;
      function BuscarCobranza: Boolean;

  end;

implementation

{ TCobranzaCobrada }

procedure TCuentaxCobrar.Ajustar;
begin

end;

procedure TCuentaxCobrar.Mover(Ds: TClientDataSet);
begin
  Desde := Ds.FieldValues['desde'];
  NumeroDocumento := Ds.FieldValues['numero_documento'];
  CodigoMoneda := Ds.FieldValues['codigo_moneda'];
  CodigoCliente := Ds.FieldValues['codigo_cliente'];
  CodigoVendedor := Ds.FieldValues['codigo_vendedor'];
  CodigoZona := Ds.FieldValues['codigo_zona'];
  FechaIngreso := Ds.FieldByName('fecha_ingreso').AsDateTime;
  FechaRecepcion := Ds.FieldByName('fecha_recepcion').AsDateTime;
  FechaVencIngreso := Ds.FieldByName('fecha_venc_ingreso').AsDateTime;
  FechaVencRecepcion := Ds.FieldByName('fecha_venc_recepcion').AsDateTime;
  FechaLibros := Ds.FieldByName('fecha_libros').AsDateTime;
  FechaContable := Ds.FieldByName('fecha_contable').AsDateTime;
  FechaCobranza := Ds.FieldByName('fecha_cobranza').AsDateTime;
  NumeroCuota := Ds.FieldValues['numero_cuota'];
  TotalCuotas := Ds.FieldValues['total_cuotas'];
  OrdenCobranza := Ds.FieldValues['orden_cobranza'];
  MontoNeto := Ds.FieldValues['monto_neto'];
  Impuesto := Ds.FieldValues['impuesto'];
  Total := Ds.FieldValues['total'];
  PIva := Ds.FieldValues['p_iva'];
  IdCobranza := Ds.FieldValues['id_cobranza'];
  IdContadoCobranza := Ds.FieldValues['id_contado_cobranza'];
end;

function TCuentaxCobrar.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,22);
  Params[0] := Desde;
  Params[1] := NumeroDocumento;
  Params[2] := CodigoMoneda;
  Params[3] := CodigoCliente;
  Params[4] := CodigoVendedor;
  Params[5] := CodigoZona;
  Params[6] := VarFromDateTime(FechaIngreso);
  Params[7] := VarFromDateTime(FechaRecepcion);
  Params[8] := VarFromDateTime(FechaVencIngreso);
  Params[9] := VarFromDateTime(FechaVencRecepcion);
  Params[10] := VarFromDateTime(FechaLibros);
  Params[11] := VarFromDateTime(FechaContable);
  Params[12] := VarFromDateTime(FechaCobranza);
  Params[13] := NumeroCuota;
  Params[14] := TotalCuotas;
  Params[15] := OrdenCobranza;
  Params[16] := MontoNeto;
  Params[17] := Impuesto;
  Params[18] := Total;
  Params[19] := PIva;
  Params[20] := IdCobranza;
  Params[21] := IdContadoCobranza;
  Result := Params;
end;

function TCuentaxCobrar.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,6);
  Campos[0] := 'desde';
  Campos[1] := 'numero_documento';
  Campos[2] := 'codigo_moneda';
  Campos[3] := 'codigo_cliente';
  Campos[4] := 'codigo_vendedor';
  Campos[5] := 'codigo_zona';
  Result := Campos;
end;

function TCuentaxCobrar.GetCondicion: string;
begin
  Result := '"id_cobranza" = ' + IntToStr(IdCobranza) + ' AND "numero_cuota" =' + IntToStr(NumeroCuota) + ' AND "orden_cobranza" =' + IntToStr(OrdenCobranza);
end;

procedure TCuentaxCobrar.AntesInsertar;
begin

end;

procedure TCuentaxCobrar.AntesModificar;
begin

end;

procedure TCuentaxCobrar.AntesEliminar;
begin

end;

procedure TCuentaxCobrar.DespuesEliminar;
begin

end;

procedure TCuentaxCobrar.DespuesInsertar;
begin

end;

procedure TCuentaxCobrar.DespuesModificar;
begin

end;

constructor TCuentaxCobrar.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_cuentas_x_cobrar');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(22)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "desde"=$1,"numero_documento"=$2,"codigo_moneda"=$3,"codigo_cliente"=$4,"codigo_vendedor"=$5,"codigo_zona"=$6,"fecha_ingreso"=$7,"fecha_recepcion"=$8,"fecha_venc_ingreso"=$9, ' +
                   '"fecha_venc_recepcion"=$10,"fecha_libros"=$11,"fecha_contable"=$12,"fecha_cobranza"=$13,"total_cuotas"=$15,"monto_neto"=$17,"impuesto"=$18,"total"=$19,"p_iva"=$20, "id_contado_cobranza"=$22 ' +
                   ' WHERE ("id_cobranza"=$21) AND ("numero_cuota"=$14) AND ("orden_cobranza"=16)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("id_cobranza"=$21) AND ("numero_cuota"=$14) AND ("orden_cobranza"=$16)');
  SetLength(ClavesPrimarias,3);
  ClavesPrimarias[0] := '"id_cobranza"';
  ClavesPrimarias[1] := '"numero_cuota"';
  ClavesPrimarias[2] := '"orden_cobranza"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TCuentaxCobrar.SetCodigoCliente(varCodigoCliente: string);
begin
  CodigoCliente := varCodigoCliente;
end;

procedure TCuentaxCobrar.SetCodigoMoneda(varCodigoMoneda: string);
begin
  CodigoMoneda := varCodigoMoneda;
end;

procedure TCuentaxCobrar.SetCodigoVendedor(varCodigoVendedor: string);
begin
  CodigoVendedor := varCodigoVendedor;
end;

procedure TCuentaxCobrar.SetCodigoZona(varCodigoZona: string);
begin
  CodigoZona := varCodigoZona;
end;

procedure TCuentaxCobrar.SetDesde(varDesde: string);
begin
  Desde := varDesde;
end;

procedure TCuentaxCobrar.SetFechaCobranza(varFechaCobranza: TDate);
begin
  FechaCobranza := varFechaCobranza;
end;

procedure TCuentaxCobrar.SetFechaContable(varFechaContable: TDate);
begin
 FechaContable := varFechaContable;
end;

procedure TCuentaxCobrar.SetFechaIngreso(varFechaIngreso: TDate);
begin
  FechaIngreso := varFechaIngreso;
end;

procedure TCuentaxCobrar.SetFechaLibros(varFechaLibros: TDate);
begin
  FechaLibros := varFechaLibros;
end;

procedure TCuentaxCobrar.SetFechaRecepcion(varFechaRecepcion: TDate);
begin
  FechaRecepcion := varFechaRecepcion;
end;

procedure TCuentaxCobrar.SetFechaVencIngreso(varFechaVencIngreso: TDate);
begin
 FechaVencIngreso := varFechaVencIngreso;
end;

procedure TCuentaxCobrar.SetFechaVencRecepcion(varFechaVencRecepcion: TDate);
begin
   FechaVencRecepcion := varFechaVencRecepcion;
end;

procedure TCuentaxCobrar.SetIdCobranza(varIdCobranza: Integer);
begin
  IdCobranza := varIdCobranza;
end;

procedure TCuentaxCobrar.SetIdContadoCobranza(varIdContadoCobranza: Integer);
begin
  IdContadoCobranza := varIdContadoCobranza;
end;

procedure TCuentaxCobrar.SetImpuesto(varImpuesto: Double);
begin
  Impuesto := varImpuesto;
end;

procedure TCuentaxCobrar.SetMontoNeto(varMontoNeto: Double);
begin
  MontoNeto := varMontoNeto;
end;

procedure TCuentaxCobrar.SetNumeroDocumento(varNumeroDocumento: string);
begin
  NumeroDocumento := varNumeroDocumento;
end;

procedure TCuentaxCobrar.SetOrdenCobranza(varOrdenCobranza: Integer);
begin
  OrdenCobranza := varOrdenCobranza;
end;

procedure TCuentaxCobrar.SetNumeroCuota(varNumeroCuota: Integer);
begin
  NumeroCuota := varNumeroCuota;
end;

procedure TCuentaxCobrar.SetPIva(varPIva: Double);
begin
  PIva := varPIva;
end;

procedure TCuentaxCobrar.SetTotal(varTotal: Double);
begin
  Total := varTotal;
end;

procedure TCuentaxCobrar.SetTotalCuotas(varTotalCuota: Integer);
begin
  TotalCuotas := varTotalCuota;
end;

function TCuentaxCobrar.GetPIva: Double;
begin
  Result := PIva;
end;

function TCuentaxCobrar.GetTotal: Double;
begin
  Result := Total;
end;

function TCuentaxCobrar.GetTotalCuotas: Integer;
begin
  Result := TotalCuotas;
end;

function TCuentaxCobrar.GetCodigoCliente: string;
begin
  Result := CodigoCliente;
end;

function TCuentaxCobrar.GetCodigoMoneda: string;
begin
  Result := CodigoMoneda;
end;

function TCuentaxCobrar.GetCodigoVendedor: string;
begin
  Result := CodigoVendedor;
end;

function TCuentaxCobrar.GetCodigoZona: string;
begin
  Result := CodigoZona;
end;

function TCuentaxCobrar.GetDesde: string;
begin
  Result := Desde;
end;

function TCuentaxCobrar.GetFechaCobranza: TDate;
begin
  Result := FechaCobranza;
end;

function TCuentaxCobrar.GetFechaContable: TDate;
begin
  Result := FechaContable;
end;

function TCuentaxCobrar.GetFechaIngreso: TDate;
begin
  Result := FechaIngreso;
end;

function TCuentaxCobrar.GetFechaLibros: TDate;
begin
  Result := FechaLibros;
end;

function TCuentaxCobrar.GetFechaRecepcion: TDate;
begin
  Result := FechaRecepcion;
end;

function TCuentaxCobrar.GetFechaVencIngreso: TDate;
begin
  Result :=FechaVencIngreso;
end;

function TCuentaxCobrar.GetFechaVencRecepcion: TDate;
begin
  Result := FechaVencRecepcion
end;

function TCuentaxCobrar.GetIdCobranza: Integer;
begin
  Result := IdCobranza;
end;

function TCuentaxCobrar.GetImpuesto: Double;
begin
  Result := Impuesto;
end;

function TCuentaxCobrar.GetMontoNeto: Double;
begin
  Result := MontoNeto;
end;

function TCuentaxCobrar.GetNumeroDocumento: string;
begin
  Result := NumeroDocumento;
end;

function TCuentaxCobrar.GetOrdenCobranza: Integer;
begin
  Result := OrdenCobranza;
end;

function TCuentaxCobrar.GetNumeroCuota: Integer;
begin
  Result := NumeroCuota;
end;

function TCuentaxCobrar.GetIdContadoCobranza: Integer;
begin
  Result := IdContadoCobranza;
end;

function TCuentaxCobrar.EliminarIdCobranza(varIdCobranza: Integer): Boolean;
begin
  Result := EjecutarSQL('DELETE FROM ' + GetTabla + ' WHERE "id_cobranza" = ' + IntToStr(varIdCobranza));
end;

function TCuentaxCobrar.BuscarCobranza: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("desde" = ' + QuotedStr(GetDesde) + ') AND ("numero_documento" = ' + QuotedStr(GetNumeroDocumento) + ') AND ("numero_cuota" = ' + IntToStr(GetNumeroCuota) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

end.
