unit ClsServicioTecnicoComisionTecnicoTipoServicio;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TServicioTecnicoComisionTecnicoTipoServicio }

  TServicioTecnicoComisionTecnicoTipoServicio = class(TGenericoBD)
    private
      CodigoTecnico: string;
      CodigoTipoServicio: string;
      PComision: Double;
      Observaciones: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigoTecnico(varCodigoTecnico: string);
      procedure SetCodigoTipoServicio(varCodigoTipoServicio: string);
      procedure SetPComision(varPComision: Double);
      procedure SetObservaciones(varObservaciones: string);
      function GetCodigoTecnico: string;
      function GetCodigoTipoServicio: string;
      function GetPComision: Double;
      function GetObservaciones: string;
      function ObtenerCodigoTecnico(varCodigoTecnico: string): TClientDataSet;
      function EliminarCodigoTecnico(varCodigoTecnico: string): Boolean;
  end;

implementation

{ TServicioTecnicoComisionTecnicoTipoServicio }

procedure TServicioTecnicoComisionTecnicoTipoServicio.Ajustar;
begin

end;

procedure TServicioTecnicoComisionTecnicoTipoServicio.Mover(Ds: TClientDataSet);
begin
  CodigoTecnico := Ds.FieldValues['codigo_tecnico'];
  CodigoTipoServicio := Ds.FieldValues['codigo_tipo_servicio'];
  PComision := Ds.FieldValues['p_comision'];
  Observaciones := Ds.FieldValues['observaciones'];
end;

function TServicioTecnicoComisionTecnicoTipoServicio.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,4);
  Params[0] := CodigoTecnico;
  Params[1] := CodigoTipoServicio;
  Params[2] := PComision;
  Params[3] := Observaciones;
  Result := Params;
end;

function TServicioTecnicoComisionTecnicoTipoServicio.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,3);
  Campos[0] := 'codigo_tecnico';
  Campos[1] := 'codigo_tipo_servicio';
  Campos[2] := 'observaciones';
  Result := Campos;
end;

function TServicioTecnicoComisionTecnicoTipoServicio.GetCondicion: string;
begin
  Result := '"codigo_tecnico" = ' + QuotedStr(CodigoTecnico) + ' AND ' +
            '"codigo_tipo_servicio" = ' + QuotedStr(CodigoTipoServicio);
end;

procedure TServicioTecnicoComisionTecnicoTipoServicio.AntesInsertar;
begin

end;

procedure TServicioTecnicoComisionTecnicoTipoServicio.AntesModificar;
begin

end;

procedure TServicioTecnicoComisionTecnicoTipoServicio.AntesEliminar;
begin

end;

procedure TServicioTecnicoComisionTecnicoTipoServicio.DespuesInsertar;
begin

end;

procedure TServicioTecnicoComisionTecnicoTipoServicio.DespuesModificar;
begin

end;

procedure TServicioTecnicoComisionTecnicoTipoServicio.DespuesEliminar;
begin

end;

constructor TServicioTecnicoComisionTecnicoTipoServicio.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_servicio_tecnico_comisiones_tecnicos_tipos_servicio');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(4)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "p_comision"=$3, "observaciones"=$4 WHERE ("codigo_tecnico"=$1 AND "codigo_tipo_servicio"=$2)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("codigo_tecnico"=$1 AND "codigo_tipo_servicio"=$2)');
  SetLength(ClavesPrimarias,2);
  ClavesPrimarias[0] := '"codigo_tecnico"';
  ClavesPrimarias[1] := '"codigo_tipo_servicio"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TServicioTecnicoComisionTecnicoTipoServicio.SetCodigoTecnico(varCodigoTecnico: string);
begin
  CodigoTecnico := varCodigoTecnico;
end;

procedure TServicioTecnicoComisionTecnicoTipoServicio.SetCodigoTipoServicio(varCodigoTipoServicio: string);
begin
  CodigoTipoServicio := varCodigoTipoServicio;
end;

procedure TServicioTecnicoComisionTecnicoTipoServicio.SetPComision(varPComision: Double);
begin
  PComision := varPComision;
end;

procedure TServicioTecnicoComisionTecnicoTipoServicio.SetObservaciones(varObservaciones: string);
begin
  Observaciones := varObservaciones;
end;

function TServicioTecnicoComisionTecnicoTipoServicio.GetCodigoTecnico: string;
begin
  Result := CodigoTecnico;
end;

function TServicioTecnicoComisionTecnicoTipoServicio.GetCodigoTipoServicio: string;
begin
  Result := CodigoTipoServicio;
end;

function TServicioTecnicoComisionTecnicoTipoServicio.GetPComision: Double;
begin
  Result := PComision;
end;

function TServicioTecnicoComisionTecnicoTipoServicio.GetObservaciones: string;
begin
  Result := Observaciones;
end;

function TServicioTecnicoComisionTecnicoTipoServicio.ObtenerCodigoTecnico(varCodigoTecnico: string): TClientDataSet;
begin
  Result := ObtenerListaCondicionSQL(' WHERE "codigo_tecnico" = ' + QuotedStr(varCodigoTecnico));
end;

function TServicioTecnicoComisionTecnicoTipoServicio.EliminarCodigoTecnico(varCodigoTecnico: string): Boolean;
begin
  Result := EjecutarSQL('DELETE FROM ' + GetTabla + ' WHERE "codigo_tecnico" = ' + QuotedStr(varCodigoTecnico));
end;

end.
