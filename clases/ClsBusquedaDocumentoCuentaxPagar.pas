unit ClsBusquedaDocumentoCuentaxPagar;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TBusquedaDocumentoCuentaxPagar }

  TBusquedaDocumentoCuentaxPagar = class(TGenericoBD)
    private
      Desde: string;
      NumeroDocumento: string;
      CodigoProveedor: string;
      Rif: string;
      Nit: string;
      RazonSocial: string;
      Contacto: string;
      FechaIngreso: TDate;
      NumeroCuota: Integer;
      TotalCuotas: Integer;
      MontoNeto: Double;
      Impuesto: Double;
      Total: Double;
      MontoNetoDocumento: Double;
      ImpuestoDocumento: Double;
      TotalDocumento: Double;
      IdPago: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create; overload;
      procedure SetDesde(varDesde: string);
      procedure SetNumeroDocumento(varNumeroDocumento: string);
      procedure SetCodigoProveedor(varCodigoProveedor: string);
      procedure SetNumeroCuota(varNumeroCuota: Integer);
      function GetDesde: string;
      function GetNumeroDocumento: string;
      function GetCodigoProveedor: string;
      function GetRif: string;
      function GetNit: string;
      function GetRazonSocial: string;
      function GetContacto: string;
      function GetFechaIngreso: TDate;
      function GetNumeroCuota: Integer;
      function GetTotalCuotas: Integer;
      function GetMontoNeto: Double;
      function GetImpuesto: Double;
      function GetTotal: Double;
      function GetMontoNetoDocumento: Double;
      function GetImpuestoDocumento: Double;
      function GetTotalDocumento: Double;
      function GetIdPago: Integer;
  end;

implementation

{ TBusquedaDocumentoCuentaxPagar }

procedure TBusquedaDocumentoCuentaxPagar.Ajustar;
begin

end;

procedure TBusquedaDocumentoCuentaxPagar.Mover(Ds: TClientDataSet);
begin
  Desde := Ds.FieldValues['desde'];
  NumeroDocumento := Ds.FieldValues['numero_documento'];
  CodigoProveedor := Ds.FieldValues['codigo_proveedor'];
  Rif := Ds.FieldValues['rif'];
  Nit := Ds.FieldValues['nit'];
  RazonSocial := Ds.FieldValues['razon_social'];
  Contacto := Ds.FieldValues['contacto'];
  FechaIngreso := Ds.FieldValues['fecha_ingreso'];
  NumeroCuota := Ds.FieldValues['numero_cuota'];
  TotalCuotas := Ds.FieldValues['total_cuotas'];
  MontoNeto := Ds.FieldValues['monto_neto'];
  Impuesto := Ds.FieldValues['impuesto'];
  Total := Ds.FieldValues['total'];
  MontoNetoDocumento := Ds.FieldValues['monto_neto_documento'];
  ImpuestoDocumento := Ds.FieldValues['impuesto_documento'];
  TotalDocumento := Ds.FieldValues['total_documento'];
  IdPago := Ds.FieldValues['id_pago'];
end;

function TBusquedaDocumentoCuentaxPagar.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,17);
  Params[0] := Desde;
  Params[1] := NumeroDocumento;
  Params[2] := CodigoProveedor;
  Params[3] := Rif;
  Params[4] := Nit;
  Params[5] := RazonSocial;
  Params[6] := Contacto;
  Params[7] := FechaIngreso;
  Params[8] := NumeroCuota;
  Params[9] := TotalCuotas;
  Params[10] := MontoNeto;
  Params[11] := Impuesto;
  Params[12] := Total;
  Params[13] := MontoNetoDocumento;
  Params[14] := ImpuestoDocumento;
  Params[15] := TotalDocumento;
  Params[16] := IdPago;
  Result := Params;
end;

function TBusquedaDocumentoCuentaxPagar.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,8);
  Campos[0] := 'desde';
  Campos[1] := 'numero_documento';
  Campos[2] := 'numero_cuota';
  Campos[3] := 'codigo_proveedor';
  Campos[4] := 'rif';
  Campos[5] := 'nit';
  Campos[6] := 'razon_social';
  Campos[7] := 'contacto';
  Result := Campos;
end;

function TBusquedaDocumentoCuentaxPagar.GetCondicion: string;
begin
  Result:= '("desde" = ' + QuotedStr(Desde) + ') AND ' +
           '("numero_documento" = ' + QuotedStr(NumeroDocumento) + ') AND ' +
           '("codigo_proveedor" = ' + QuotedStr(CodigoProveedor) + ') AND ' +
           '("numero_cuota" = ' + IntToStr(NumeroCuota) + ')';
end;

procedure TBusquedaDocumentoCuentaxPagar.AntesInsertar;
begin

end;

procedure TBusquedaDocumentoCuentaxPagar.AntesModificar;
begin

end;

procedure TBusquedaDocumentoCuentaxPagar.AntesEliminar;
begin

end;

procedure TBusquedaDocumentoCuentaxPagar.DespuesInsertar;
begin

end;

procedure TBusquedaDocumentoCuentaxPagar.DespuesModificar;
begin

end;

procedure TBusquedaDocumentoCuentaxPagar.DespuesEliminar;
begin

end;

constructor TBusquedaDocumentoCuentaxPagar.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('v_busqueda_documentos_cuentas_x_pagar');
  SetLength(ClavesPrimarias,0);
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TBusquedaDocumentoCuentaxPagar.SetCodigoProveedor(
  varCodigoProveedor: string);
begin
  CodigoProveedor := varCodigoProveedor;
end;

procedure TBusquedaDocumentoCuentaxPagar.SetDesde(varDesde: string);
begin
  Desde := varDesde;
end;

procedure TBusquedaDocumentoCuentaxPagar.SetNumeroDocumento(varNumeroDocumento: string);
begin
  NumeroDocumento := varNumeroDocumento;
end;

procedure TBusquedaDocumentoCuentaxPagar.SetNumeroCuota(
  varNumeroCuota: Integer);
begin
  NumeroCuota := varNumeroCuota;
end;

function TBusquedaDocumentoCuentaxPagar.GetCodigoProveedor: string;
begin
  Result := CodigoProveedor;
end;

function TBusquedaDocumentoCuentaxPagar.GetContacto: string;
begin
  Result := Contacto;
end;

function TBusquedaDocumentoCuentaxPagar.GetDesde: string;
begin
  Result := Desde;
end;

function TBusquedaDocumentoCuentaxPagar.GetFechaIngreso: TDate;
begin
  Result := FechaIngreso;
end;

function TBusquedaDocumentoCuentaxPagar.GetImpuesto: Double;
begin
  Result := Impuesto;
end;

function TBusquedaDocumentoCuentaxPagar.GetImpuestoDocumento: Double;
begin
  Result := ImpuestoDocumento; 
end;

function TBusquedaDocumentoCuentaxPagar.GetMontoNeto: Double;
begin
  Result := MontoNeto;
end;

function TBusquedaDocumentoCuentaxPagar.GetMontoNetoDocumento: Double;
begin
  Result := MontoNetoDocumento
end;

function TBusquedaDocumentoCuentaxPagar.GetNit: string;
begin
  Result := Nit;
end;

function TBusquedaDocumentoCuentaxPagar.GetNumeroCuota: Integer;
begin
  Result := NumeroCuota;
end;

function TBusquedaDocumentoCuentaxPagar.GetNumeroDocumento: string;
begin
  Result := NumeroDocumento;
end;

function TBusquedaDocumentoCuentaxPagar.GetRazonSocial: String;
begin
  Result := RazonSocial;
end;

function TBusquedaDocumentoCuentaxPagar.GetRif: String;
begin
  Result := Rif;
end;

function TBusquedaDocumentoCuentaxPagar.GetTotal: Double;
begin
  Result := Total;
end;

function TBusquedaDocumentoCuentaxPagar.GetTotalCuotas: Integer;
begin
  Result := TotalCuotas;
end;

function TBusquedaDocumentoCuentaxPagar.GetTotalDocumento: Double;
begin
  Result := TotalDocumento;
end;

function TBusquedaDocumentoCuentaxPagar.GetIdPago: Integer;
begin
  Result := IdPago;
end;

end.

