unit ClsRadioLiquidacionContrato;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TRadioLiquidacionContrato}

  TRadioLiquidacionContrato = class(TGenericoBD)
    private
      Numero: string;
      CodigoCliente: string;
      CodigoZona: string;
      FechaIngreso: TDate;
      FechaContable: TDate;
      FechaFactura: TDate;
      Observaciones: string;
      Total: Double;
      IdRadioLiquidacionContrato: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetNumero(varNumero: string);
      procedure SetCodigoCliente(varCodigoCliente: string);
      procedure SetCodigoZona(varCodigoZona: string);
      procedure SetFechaIngreso(varFechaIngreso: TDate);
      procedure SetFechaContable(varFechaContable: TDate);
      procedure SetFechaFactura(varFechaFactura: TDate);
      procedure SetObservaciones(varObservaciones: string);
      procedure SetTotal(varTotal: Double);
      procedure SetIdRadioLiquidacionContrato(varIdRadioLiquidacionContrato: Integer);
      function GetNumero: string;
      function GetCodigoCliente: string;
      function GetCodigoZona: string;
      function GetFechaIngreso: TDate;
      function GetFechaContable: TDate;
      function GetFechaFactura: TDate;
      function GetObservaciones: string;
      function GetTotal: Double;
      function GetIdRadioLiquidacionContrato: Integer;
      function BuscarNumero: Boolean;
  end;

implementation

uses
  DB,
  ClsConsecutivo;
  
{ TRadioLiquidacionContrato }

procedure TRadioLiquidacionContrato.Ajustar;
begin

end;

procedure TRadioLiquidacionContrato.Mover(Ds: TClientDataSet);
begin
  Numero := Ds.FieldValues['numero'];
  CodigoCliente := Ds.FieldValues['codigo_cliente'];
  CodigoZona := Ds.FieldValues['codigo_zona'];
  FechaIngreso := Ds.FieldValues['fecha_ingreso'];
  FechaContable := Ds.FieldValues['fecha_contable'];
  FechaFactura := Ds.FieldValues['fecha_factura'];
  Observaciones := Ds.FieldValues['observaciones'];
  Total := Ds.FieldValues['total'];
  IdRadioLiquidacionContrato := Ds.FieldValues['id_radio_liquidacion_contrato'];
end;

function TRadioLiquidacionContrato.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,9);
  Params[0] := Numero;
  Params[1] := CodigoCliente;
  Params[2] := CodigoZona;
  Params[3] := VarFromDateTime(FechaIngreso);
  Params[4] := VarFromDateTime(FechaContable);
  Params[5] := VarFromDateTime(FechaFactura);
  Params[6] := Observaciones;
  Params[7] := Total;
  Params[8] := IdRadioLiquidacionContrato;
  Result := Params;
end;

function TRadioLiquidacionContrato.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,4);
  Campos[0] := 'numero';
  Campos[1] := 'codigo_cliente';
  Campos[2] := 'codigo_zona';
  Campos[3] := 'observaciones';
  Result := Campos;
end;

function TRadioLiquidacionContrato.GetCondicion: string;
begin
  Result := '"id_radio_liquidacion_contrato" = ' + IntToStr(IdRadioLiquidacionContrato);
end;

procedure TRadioLiquidacionContrato.AntesInsertar;
var
  Consecutivo: TConsecutivo;
begin
  Consecutivo := TConsecutivo.Create;
  Numero := IntToStr(Consecutivo.ObtenerConsecutivo('numero_radio_liquidacion_contrato'));
  IdRadioLiquidacionContrato := Consecutivo.ObtenerConsecutivo('id_radio_liquidacion_contrato');
end;

procedure TRadioLiquidacionContrato.AntesModificar;
begin

end;

procedure TRadioLiquidacionContrato.AntesEliminar;
begin

end;

procedure TRadioLiquidacionContrato.DespuesInsertar;
begin

end;

procedure TRadioLiquidacionContrato.DespuesModificar;
begin

end;

procedure TRadioLiquidacionContrato.DespuesEliminar;
begin

end;

constructor TRadioLiquidacionContrato.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_radio_liquidacion_contratos');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(9)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "numero"=$1, "codigo_cliente"=$2, "codigo_zona"=$3, "fecha_ingreso"=$4, "fecha_contable"=$5, "fecha_factura"=$6, "observaciones"=$7, "total"=$8 WHERE ("id_radio_liquidacion_contrato"=$9)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("id_radio_liquidacion_contrato"=$9)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"id_radio_liquidacion_contrato"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TRadioLiquidacionContrato.SetNumero(varNumero: string);
begin
  Numero := varNumero;
end;

procedure TRadioLiquidacionContrato.SetCodigoCliente(varCodigoCliente: string);
begin
  CodigoCliente := varCodigoCliente;
end;

procedure TRadioLiquidacionContrato.SetCodigoZona(varCodigoZona: string);
begin
  CodigoZona := varCodigoZona;
end;

procedure TRadioLiquidacionContrato.SetFechaIngreso(varFechaIngreso: TDate);
begin
  FechaIngreso := varFechaIngreso;
end;

procedure TRadioLiquidacionContrato.SetFechaContable(varFechaContable: TDate);
begin
  FechaContable := varFechaContable;
end;

procedure TRadioLiquidacionContrato.SetFechaFactura(varFechaFactura: TDate);
begin
  FechaFactura := varFechaFactura;
end;

procedure TRadioLiquidacionContrato.SetObservaciones(varObservaciones: string);
begin
  Observaciones := varObservaciones;
end;

procedure TRadioLiquidacionContrato.SetTotal(varTotal: Double);
begin
  Total := varTotal;
end;

procedure TRadioLiquidacionContrato.SetIdRadioLiquidacionContrato(varIdRadioLiquidacionContrato: Integer);
begin
  IdRadioLiquidacionContrato := varIdRadioLiquidacionContrato;
end;

function TRadioLiquidacionContrato.GetNumero: string;
begin
  Result := Numero;
end;

function TRadioLiquidacionContrato.GetCodigoCliente: string;
begin
  Result := CodigoCliente;
end;

function TRadioLiquidacionContrato.GetCodigoZona: string;
begin
  Result := CodigoZona;
end;

function TRadioLiquidacionContrato.GetFechaIngreso: TDate;
begin
  Result := FechaIngreso;
end;

function TRadioLiquidacionContrato.GetFechaContable: TDate;
begin
  Result := FechaContable;
end;

function TRadioLiquidacionContrato.GetFechaFactura: TDate;
begin
  Result := FechaFactura;
end;

function TRadioLiquidacionContrato.GetObservaciones: string;
begin
  Result := Observaciones;
end;

function TRadioLiquidacionContrato.GetTotal: Double;
begin
  Result := Total;
end;

function TRadioLiquidacionContrato.GetIdRadioLiquidacionContrato: Integer;
begin
  Result := IdRadioLiquidacionContrato;
end;

function TRadioLiquidacionContrato.BuscarNumero: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("numero" = ' + QuotedStr(GetNumero) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

end.
