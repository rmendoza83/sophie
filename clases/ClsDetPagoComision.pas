unit ClsDetPagoComision;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TDetPagoComision }

  TDetPagoComision = class(TGenericoBD)
    private
      Numero: string;
      CodigoTipoComision: string;
      Desde: string;
      NumeroDocumento: string;
      BaseCalculo: Double;
      PorcentajeComision: Double;
      Cantidad: Double;
      MontoComision: Double;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetNumero(varNumero: string);
      procedure SetCodigoTipoComision(varCodigoTipoComision: string);
      procedure SetDesde(varDesde: string);
      procedure SetNumeroDocumento(varNumeroDocumento: string);
      procedure SetBaseCalculo(varBaseCalculo: Double);
      procedure SetPorcentajeComision(varPorcentajeComision: Double);
      procedure SetCantidad(varCantidad: Double);
      procedure SetMontoComision(varMontoComision: Double);
      function GetNumero: string;
      function GetCodigoTipoComision: string;
      function GetDesde: string;
      function GetNumeroDocumento: string;
      function GetBaseCalculo: Double;
      function GetPorcentajeComision: Double;
      function GetCantidad: Double;
      function GetMontoComision: Double;
      function EliminarNumero(varNumero: string): Boolean;
      function ObtenerNumero(varNumero: string): TClientDataSet;
  end;

var
  DetPagoComision: TDetPagoComision;

implementation

uses DB;

{ TDetPagoComision }

procedure TDetPagoComision.Ajustar;
begin

end;

procedure TDetPagoComision.Mover(Ds: TClientDataSet);
begin
  Numero := Ds.FieldValues['numero'];
  CodigoTipoComision := Ds.FieldValues['codigo_tipo_comision'];
  Desde:= Ds.FieldValues['desde'];
  NumeroDocumento := Ds.FieldValues['numero_documento'];
  BaseCalculo := Ds.FieldValues['base_calculo'];
  PorcentajeComision := Ds.FieldValues['porcentaje_comision'];
  Cantidad := Ds.FieldValues['cantidad'];
  MontoComision:= Ds.FieldValues['monto_comision'];
end;

function TDetPagoComision.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,8);
  Params[0] := Numero;
  Params[1] := CodigoTipoComision;
  Params[2] := Desde;
  Params[3] := NumeroDocumento;
  Params[4] := BaseCalculo;
  Params[5] := PorcentajeComision;
  Params[6] := Cantidad;
  Params[7] := MontoComision;
  Result := Params;
end;

function TDetPagoComision.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,4);
  Campos[0] := 'numero';
  Campos[1] := 'codigo_tipo_comision';
  Campos[2] := 'desde';
  Campos[3] := 'numero_documento';
  Result := Campos;
end;

function TDetPagoComision.GetCondicion: string;
begin
  Result := '"numero" =' + QuotedStr(Numero) + 'AND "codigo_tipo_comision" =' + QuotedStr(CodigoTipoComision) + 'AND "desde" =' + QuotedStr(Desde) + '"numero_documento" =' + QuotedStr(NumeroDocumento);
end;

procedure TDetPagoComision.AntesInsertar;
begin

end;

procedure TDetPagoComision.AntesModificar;
begin

end;

procedure TDetPagoComision.AntesEliminar;
begin

end;

procedure TDetPagoComision.DespuesInsertar;
begin

end;

procedure TDetPagoComision.DespuesModificar;
begin

end;

procedure TDetPagoComision.DespuesEliminar;
begin

end;

constructor TDetPagoComision.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_det_pagos_comisiones');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(8)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "base_calculo"=$5,"porcentaje_comision"=$6, "cantidad"=$7, "monto_comision"=$8 WHERE ("numero"=$1) AND ("codigo_tipo_comision"=$2) AND ("desde"=$3) AND ("numero_documento"=$4)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + 'WHERE ("numero"=$1) AND ("codigo_tipo_comision"=$2) AND ("desde"=$3) AND ("numero_documento"=$4) ');
  SetLength(ClavesPrimarias,4);
  ClavesPrimarias[0] := '"numero"';
  ClavesPrimarias[1] := '"codigo_comision"';
  ClavesPrimarias[2] := '"desde"';
  ClavesPrimarias[3] := '"numero_documento"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TDetPagoComision.SetNumero(varNumero: string);
begin
  Numero := varNumero;
end;

procedure TDetPagoComision.SetCodigoTipoComision(varCodigoTipoComision: string);
begin
  CodigoTipoComision := varCodigoTipoComision;
end;

procedure TDetPagoComision.SetDesde(varDesde: string);
begin
  Desde := varDesde;
end;

procedure TDetPagoComision.SetNumeroDocumento(varNumeroDocumento: string);
begin
  NumeroDocumento := varNumeroDocumento;
end;

procedure TDetPagoComision.SetBaseCalculo(varBaseCalculo: Double);
begin
  BaseCalculo := varBaseCalculo;
end;

procedure TDetPagoComision.SetPorcentajeComision(varPorcentajeComision: Double);
begin
  PorcentajeComision := varPorcentajeComision;
end;

procedure TDetPagoComision.SetCantidad(varCantidad: Double);
begin
  Cantidad := varCantidad;
end;

procedure TDetPagoComision.SetMontoComision(varMontoComision: Double);
begin
  MontoComision := varMontoComision;
end;

function TDetPagoComision.GetNumero: string;
begin
  Result := Numero;
end;

function TDetPagoComision.GetCodigoTipoComision: string;
begin
  Result := CodigoTipoComision;
end;

function TDetPagoComision.GetDesde: string;
begin
  Result := Desde;
end;

function TDetPagoComision.GetNumeroDocumento: string;
begin
  Result := NumeroDocumento;
end;

function TDetPagoComision.GetBaseCalculo: Double;
begin
  Result := BaseCalculo;
end;

function TDetPagoComision.GetPorcentajeComision: Double;
begin
  Result := PorcentajeComision;
end;

function TDetPagoComision.GetCantidad: Double;
begin
  Result := Cantidad;
end;

function TDetPagoComision.GetMontoComision: Double;
begin
  Result := MontoComision;
end;

function TDetPagoComision.EliminarNumero(varNumero: string): Boolean;
begin
  Result := EjecutarSQL('DELETE FROM ' + GetTabla + ' WHERE "numero" = ' + QuotedStr(varNumero));
end;

function TDetPagoComision.ObtenerNumero(varNumero: string): TClientDataSet;
begin
  Result := ObtenerListaCondicionSQL(' WHERE "numero" = ' + QuotedStr(varNumero));
end;

end.
