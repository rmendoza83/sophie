unit ClsBusquedaDocumentoRetencionIva;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TBusquedaDocumentoRetencionIva }

  TBusquedaDocumentoRetencionIva = class(TGenericoBD)
    private
      Desde: string;
      Numero: string;
      CodigoProveedor: string;
      Rif: string;
      Nit: string;
      RazonSocial: string;
      Contacto: string;
      MontoNeto: Double;
      Impuesto: Double;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create; overload;
      procedure SetDesde(varDesde: string);
      procedure SetNumero(varNumero: string);
      procedure SetCodigoProveedor(varCodigoProveedor: string);
      function GetDesde: string;
      function GetNumero: string;
      function GetCodigoProveedor: string;
      function GetRif: string;
      function GetNit: string;
      function GetRazonSocial: string;
      function GetContacto: string;
      function GetMontoNeto: Double;
      function GetImpuesto: Double;
  end;

implementation

{ TBusquedaDocumentoRetencionIva }

procedure TBusquedaDocumentoRetencionIva.Ajustar;
begin

end;

procedure TBusquedaDocumentoRetencionIva.Mover(Ds: TClientDataSet);
begin
  Desde := Ds.FieldValues['desde'];
  Numero := Ds.FieldValues['numero'];
  CodigoProveedor := Ds.FieldValues['codigo_proveedor'];
  Rif := Ds.FieldValues['rif'];
  Nit := Ds.FieldValues['nit'];
  RazonSocial := Ds.FieldValues['razon_social'];
  Contacto := Ds.FieldValues['contacto'];
  MontoNeto := Ds.FieldValues['monto_neto'];
  Impuesto := Ds.FieldValues['impuesto'];
end;

function TBusquedaDocumentoRetencionIva.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,9);
  Params[0] := Desde;
  Params[1] := Numero;
  Params[2] := CodigoProveedor;
  Params[3] := Rif;
  Params[4] := Nit;
  Params[5] := RazonSocial;
  Params[6] := Contacto;
  Params[7] := MontoNeto;
  Params[8] := Impuesto;
  Result := Params;
end;

function TBusquedaDocumentoRetencionIva.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,7);
  Campos[0] := 'desde';
  Campos[1] := 'numero';
  Campos[2] := 'codigo_proveedor';
  Campos[3] := 'rif';
  Campos[4] := 'nit';
  Campos[5] := 'razon_social';
  Campos[6] := 'contacto';
  Result := Campos;
end;

function TBusquedaDocumentoRetencionIva.GetCondicion: string;
begin
  Result:= '("desde" = ' + QuotedStr(Desde) + ') AND ("numero" = ' + QuotedStr(Numero) + ') AND ("codigo_proveedor" = ' + QuotedStr(CodigoProveedor) + ')';
end;

procedure TBusquedaDocumentoRetencionIva.AntesInsertar;
begin

end;

procedure TBusquedaDocumentoRetencionIva.AntesModificar;
begin

end;

procedure TBusquedaDocumentoRetencionIva.AntesEliminar;
begin

end;

procedure TBusquedaDocumentoRetencionIva.DespuesInsertar;
begin

end;

procedure TBusquedaDocumentoRetencionIva.DespuesModificar;
begin

end;

procedure TBusquedaDocumentoRetencionIva.DespuesEliminar;
begin

end;

constructor TBusquedaDocumentoRetencionIva.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('v_busqueda_documentos_retencion_iva');
  SetLength(ClavesPrimarias,0);
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TBusquedaDocumentoRetencionIva.SetCodigoProveedor(
  varCodigoProveedor: string);
begin
  CodigoProveedor := varCodigoProveedor;
end;

procedure TBusquedaDocumentoRetencionIva.SetDesde(varDesde: string);
begin
  Desde := varDesde;
end;

procedure TBusquedaDocumentoRetencionIva.SetNumero(varNumero: string);
begin
  Numero := varNumero;
end;

function TBusquedaDocumentoRetencionIva.GetCodigoProveedor: string;
begin
  Result := CodigoProveedor;
end;

function TBusquedaDocumentoRetencionIva.GetContacto: string;
begin
  Result := Contacto;
end;

function TBusquedaDocumentoRetencionIva.GetDesde: string;
begin
  Result := Desde;
end;

function TBusquedaDocumentoRetencionIva.GetImpuesto: Double;
begin
  Result := Impuesto;
end;

function TBusquedaDocumentoRetencionIva.GetMontoNeto: Double;
begin
  Result := MontoNeto;
end;

function TBusquedaDocumentoRetencionIva.GetNit: string;
begin
  Result := Nit;
end;

function TBusquedaDocumentoRetencionIva.GetNumero: string;
begin
  Result := Numero;
end;

function TBusquedaDocumentoRetencionIva.GetRazonSocial: String;
begin
  Result := RazonSocial;
end;

function TBusquedaDocumentoRetencionIva.GetRif: String;
begin
  Result := Rif;
end;

end.
