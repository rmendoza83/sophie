unit ClsBusquedaNotaDebitoCliente;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TBusquedaNotaDebitoCliente }

  TBusquedaNotaDebitoCliente = class(TGenericoBD)
    private
      Numero: string;
      CodigoCliente: string;
      Rif: string;
      Nit: string;
      RazonSocial: string;
      Contacto: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      Function GetNumero: string;
      Function GetCodigoCliente: string;
      Function GetRif: string;
      Function GetNit: string;
      Function GetRazonSocial: string;
      Function GetContacto: string;
  end;

implementation

{ TBusquedaNotaDebitoCliente }

procedure TBusquedaNotaDebitoCliente.Ajustar;
begin

end;

procedure TBusquedaNotaDebitoCliente.Mover(Ds: TClientDataSet);
begin
  Numero := Ds.FieldValues['numero'];
  CodigoCliente := Ds.FieldValues['codigo_cliente'];
  Rif := Ds.FieldValues['rif'];
  Nit := Ds.FieldValues['nit'];
  RazonSocial := Ds.FieldValues['razon_social'];
  Contacto := Ds.FieldValues['contacto'];
end;

function TBusquedaNotaDebitoCliente.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,6);
  Params[0] := Numero;
  Params[1] := CodigoCliente;
  Params[2] := Rif;
  Params[3] := Nit;
  Params[4] := RazonSocial;
  Params[5] := Contacto;
  Result := Params;
end;

function TBusquedaNotaDebitoCliente.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,6);
  Campos[0] := 'numero';
  Campos[1] := 'codigo_cliente';
  Campos[2] := 'rif';
  Campos[3] := 'nit';
  Campos[4] := 'razon_social';
  Campos[5] := 'contacto';
  Result := Campos;
end;

function TBusquedaNotaDebitoCliente.GetCondicion: string;
begin
  Result:= '';
end;

procedure TBusquedaNotaDebitoCliente.AntesInsertar;
begin

end;

procedure TBusquedaNotaDebitoCliente.AntesModificar;
begin

end;

procedure TBusquedaNotaDebitoCliente.AntesEliminar;
begin

end;

procedure TBusquedaNotaDebitoCliente.DespuesInsertar;
begin

end;

procedure TBusquedaNotaDebitoCliente.DespuesModificar;
begin

end;

procedure TBusquedaNotaDebitoCliente.DespuesEliminar;
begin

end;

constructor TBusquedaNotaDebitoCliente.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('v_busqueda_nota_debito_clientes');
  SetLength(ClavesPrimarias,0);
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

function TBusquedaNotaDebitoCliente.GetCodigoCliente: string;
begin
  Result := CodigoCliente;
end;

function TBusquedaNotaDebitoCliente.GetContacto: string;
begin
  Result := Contacto;
end;

function TBusquedaNotaDebitoCliente.GetNit: string;
begin
  Result := Nit;
end;

function TBusquedaNotaDebitoCliente.GetNumero: string;
begin
  Result := Numero;
end;

function TBusquedaNotaDebitoCliente.GetRazonSocial: string;
begin
  Result := RazonSocial;
end;

function TBusquedaNotaDebitoCliente.GetRif: string;
begin
  Result := Rif;
end;

end.
