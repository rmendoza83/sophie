unit ClsBusquedaFacturacionCliente;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TBusquedaFacturacionCliente }

  TBusquedaFacturacionCliente = class(TGenericoBD)
    private
      Numero: string;
      NumeroPedido: string;
      CodigoCliente: string;
      Rif: string;
      Nit: string;
      RazonSocial: string;
      Contacto: string;
      CodigoVendedor: string;
      NombreVendedor: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      function GetNumero: string;
      function GetNumeroPedido: string;
      function GetCodigoCliente: string;
      function GetRif: string;
      function GetNit: string;
      function GetRazonSocial: string;
      function GetContacto: string;
      function GetCodigoVendedor: string;
      function GetNombreVendedor: string;
  end;

implementation

{ TBusquedaFacturacionCliente }

procedure TBusquedaFacturacionCliente.Ajustar;
begin

end;

procedure TBusquedaFacturacionCliente.Mover(Ds: TClientDataSet);
begin
  Numero := Ds.FieldValues['numero'];
  NumeroPedido:= Ds.FieldValues['numero_pedido'];
  CodigoCliente := Ds.FieldValues['codigo_cliente'];
  Rif := Ds.FieldValues['rif'];
  Nit := Ds.FieldValues['nit'];
  RazonSocial := Ds.FieldValues['razon_social'];
  Contacto := Ds.FieldValues['contacto'];
  CodigoVendedor := Ds.FieldValues['codigo_vendedor'];
  NombreVendedor := Ds.FieldValues['nombre_vendedor'];
end;

function TBusquedaFacturacionCliente.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,9);
  Params[0] := Numero;
  Params[1] := NumeroPedido;
  Params[2] := CodigoCliente;
  Params[3] := Rif;
  Params[4] := Nit;
  Params[5] := RazonSocial;
  Params[6] := Contacto;
  Params[7] := CodigoVendedor;
  Params[8] := NombreVendedor;
  Result := Params;
end;

function TBusquedaFacturacionCliente.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,9);
  Campos[0] := 'numero';
  Campos[1] := 'numero_pedido';
  Campos[2] := 'codigo_cliente';
  Campos[3] := 'rif';
  Campos[4] := 'nit';
  Campos[5] := 'razon_social';
  Campos[6] := 'contacto';
  Campos[7] := 'codigo_vendedor';
  Campos[8] := 'nombre_vendedor';
  Result := Campos;
end;

function TBusquedaFacturacionCliente.GetCondicion: string;
begin
  Result:= '';
end;

procedure TBusquedaFacturacionCliente.AntesInsertar;
begin

end;

procedure TBusquedaFacturacionCliente.AntesModificar;
begin

end;

procedure TBusquedaFacturacionCliente.AntesEliminar;
begin

end;

procedure TBusquedaFacturacionCliente.DespuesInsertar;
begin

end;

procedure TBusquedaFacturacionCliente.DespuesModificar;
begin

end;

procedure TBusquedaFacturacionCliente.DespuesEliminar;
begin

end;

constructor TBusquedaFacturacionCliente.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('v_busqueda_facturacion_clientes');
  SetLength(ClavesPrimarias,0);
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

function TBusquedaFacturacionCliente.GetCodigoCliente: string;
begin
  Result := CodigoCliente;
end;

function TBusquedaFacturacionCliente.GetCodigoVendedor: string;
begin
  Result := CodigoVendedor;
end;

function TBusquedaFacturacionCliente.GetContacto: string;
begin
  Result := Contacto;
end;

function TBusquedaFacturacionCliente.GetNit: string;
begin
  Result := Nit;
end;

function TBusquedaFacturacionCliente.GetNombreVendedor: string;
begin
  Result := NombreVendedor;
end;

function TBusquedaFacturacionCliente.GetNumero: string;
begin
  Result := Numero;
end;

function TBusquedaFacturacionCliente.GetNumeroPedido: string;
begin
  Result := NumeroPedido;
end;

function TBusquedaFacturacionCliente.GetRazonSocial: string;
begin
  Result := RazonSocial;
end;

function TBusquedaFacturacionCliente.GetRif: string;
begin
  Result := Rif;
end;

end.
