unit ClsPresupuesto;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TPresupuesto }

  TPresupuesto = class(TGenericoBD)
    private
      Numero: string;
      CodigoMoneda: string;
      CodigoCliente: string;
      CodigoVendedor: string;
      CodigoZona: string;
      CodigoEsquemaPago: string;
      FechaIngreso: TDate;
      FechaVencimiento: TDate;
      Concepto: string;
      Observaciones: string;
      MontoNeto: Double;
      MontoDescuento: Double;
      SubTotal: Double;
      Impuesto: Double;
      Total: Double;
      PDescuento: Double;
      PIva: Double;
      TasaDolar: Double;
      FormaLibre: Boolean;
      LoginUsuario: string;
      TiempoIngreso: TDate;
      IdPresupuesto: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetNumero(varNumero: string);
      procedure SetCodigoMoneda(varCodigoMoneda: string);
      procedure SetCodigoCliente(varCodigoCliente: string);
      procedure SetCodigoVendedor(varCodigoVendedor: string);
      procedure SetCodigoZona(varCodigoZona: string);
      procedure SetCodigoEsquemaPago(varCodigoEsquemaPago: string);
      procedure SetFechaIngreso(varFechaIngreso: TDate);
      procedure SetFechaVencimiento(varFechaVencimiento: TDate);
      procedure SetConcepto(varConcepto: string);
      procedure SetObservaciones(varObservaciones: string);
      procedure SetMontoNeto(varMontoNeto: Double);
      procedure SetMontoDescuento(varMontoDescuento: Double);
      procedure SetSubTotal(varSubTotal: Double);
      procedure SetImpuesto(varImpuesto: Double);
      procedure SetTotal(varTotal: Double);
      procedure SetPDescuento(varPDescuento: Double);
      procedure SetPIva(varPIva: Double);
      procedure SetTasaDolar(varTasaDolar: Double);
      procedure SetFormaLibre(varFormaLibre: Boolean);
      procedure SetLoginUsuario(varLoginUsuario: string);
      procedure SetTiempoIngreso(varTiempoIngreso: TDate);
      procedure SetIdPresupuesto(varIdPresupuesto: Integer);
      function GetNumero: string;
      function GetCodigoMoneda: string;
      function GetCodigoCliente: string;
      function GetCodigoVendedor: string;
      function GetCodigoZona: string;
      function GetCodigoEsquemaPago: string;
      function GetFechaIngreso: TDate;
      function GetFechaVencimiento: TDate;
      function GetConcepto: string;
      function GetObservaciones: string;
      function GetMontoNeto: Double;
      function GetMontoDescuento: Double;
      function GetSubTotal: Double;
      function GetImpuesto: Double;
      function GetTotal: Double;
      function GetPDescuento: Double;
      function GetPIva: Double;
      function GetTasaDolar: Double;
      function GetFormaLibre: Boolean;
      function GetLoginUsuario: string;
      function GetTiempoIngreso: TDate;
      function GetIdPresupuesto: Integer;
      function BuscarNumero: Boolean;
  end;

implementation

uses
  ClsConsecutivo;

{ TPresupuesto }

procedure TPresupuesto.Ajustar;
begin

end;

procedure TPresupuesto.Mover(Ds: TClientDataSet);
begin
  Numero := Ds.FieldValues['numero'];
  CodigoMoneda := Ds.FieldValues['codigo_moneda'];
  CodigoCliente := Ds.FieldValues['codigo_cliente'];
  CodigoVendedor := Ds.FieldValues['codigo_vendedor'];
  CodigoZona := Ds.FieldValues['codigo_zona'];
  CodigoEsquemaPago := Ds.FieldValues['codigo_esquema_pago'];
  FechaIngreso := Ds.FieldByName('fecha_ingreso').AsDateTime;
  FechaVencimiento := Ds.FieldByName('fecha_vencimiento').AsDateTime;
  Concepto := Ds.FieldValues['concepto'];
  Observaciones := Ds.FieldValues['observaciones'];
  MontoNeto := Ds.FieldValues['monto_neto'];
  MontoDescuento := Ds.FieldValues['monto_descuento'];
  SubTotal := Ds.FieldValues['sub_total'];
  Impuesto := Ds.FieldValues['impuesto'];
  Total := Ds.FieldValues['total'];
  PDescuento := Ds.FieldValues['p_descuento'];
  PIva := Ds.FieldValues['p_iva'];
  TasaDolar := Ds.FieldValues['tasa_dolar'];
  FormaLibre := Ds.FieldValues['forma_libre'];
  LoginUsuario := Ds.FieldValues['login_usuario'];
  TiempoIngreso := Ds.FieldValues['tiempo_ingreso'];
  IdPresupuesto := Ds.FieldValues['id_presupuesto'];
end;

function TPresupuesto.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,22);
  Params[0] := Numero;
  Params[1] := CodigoMoneda;
  Params[2] := CodigoCliente;
  Params[3] := CodigoVendedor;
  Params[4] := CodigoZona;
  Params[5] := CodigoEsquemaPago;
  Params[6] := VarFromDateTime(FechaIngreso);
  Params[7] := VarFromDateTime(FechaVencimiento);
  Params[8] := Concepto;
  Params[9] := Observaciones;
  Params[10] := MontoNeto;
  Params[11] := MontoDescuento;
  Params[12] := SubTotal;
  Params[13] := Impuesto;
  Params[14] := Total;
  Params[15] := PDescuento;
  Params[16] := PIva;
  Params[17] := TasaDolar;
  Params[18] := FormaLibre;
  Params[19] := LoginUsuario;
  Params[20] := VarFromDateTime(TiempoIngreso);
  Params[21] := IdPresupuesto;
  Result := Params;
end;

function TPresupuesto.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,9);
  Campos[0] := 'numero';
  Campos[1] := 'codigo_moneda';
  Campos[2] := 'codigo_cliente';
  Campos[3] := 'codigo_vendedor';
  Campos[4] := 'codigo_zona';
  Campos[5] := 'codigo_esquema_pago';
  Campos[6] := 'concepto';
  Campos[7] := 'observaciones';
  Campos[8] := 'login_usuario';
  Result := Campos;
end;

function TPresupuesto.GetCondicion: string;
begin
  Result := '"id_presupuesto" = ' + IntToStr(IdPresupuesto);
end;

procedure TPresupuesto.AntesInsertar;
var
  Consecutivo: TConsecutivo;
begin
  Consecutivo := TConsecutivo.Create;
  Numero := IntToStr(Consecutivo.ObtenerConsecutivo('numero_presupuesto'));
  IdPresupuesto := Consecutivo.ObtenerConsecutivo('id_presupuesto');
end;

procedure TPresupuesto.AntesModificar;
begin

end;

function TPresupuesto.BuscarNumero: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("numero" = ' + QuotedStr(GetNumero) + ')');
  if (Ds.RecordCount > 0) then
    begin
      Result := True;
      Mover(Ds);
    end;
end;

procedure TPresupuesto.AntesEliminar;
begin

end;

procedure TPresupuesto.DespuesInsertar;
begin

end;

procedure TPresupuesto.DespuesModificar;
begin

end;

procedure TPresupuesto.DespuesEliminar;
begin

end;

constructor TPresupuesto.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_presupuestos');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(22)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "numero"=$1,"codigo_moneda"=$2, "codigo_cliente"=$3, "codigo_vendedor"=$4, "codigo_zona"=$5, "codigo_esquema_pago"=$6, "fecha_ingreso"=$7, "fecha_vencimiento"=$8, "concepto"=$9, "observaciones"=$10, ' +
                  '"monto_neto"=$11, "monto_descuento"=$12, "sub_total"=$13, "impuesto"=$14, "total"=$15, "p_descuento"=$16, "p_iva"=$17, "tasa_dolar"=$18, "forma_libre"=$19, "login_usuario"=$20, "tiempo_ingreso"=$21 WHERE ("id_presupuesto"=$22)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("id_presupuesto"=$22)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"id_presupuesto"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TPresupuesto.SetCodigoCliente(varCodigoCliente: string);
begin
  CodigoCliente := varCodigoCliente;
end;

procedure TPresupuesto.SetCodigoEsquemaPago(varCodigoEsquemaPago: string);
begin
  CodigoEsquemaPago := varCodigoEsquemaPago;
end;

procedure TPresupuesto.SetCodigoMoneda(varCodigoMoneda: string);
begin
  CodigoMoneda := varCodigoMoneda;
end;

procedure TPresupuesto.SetCodigoVendedor(varCodigoVendedor: string);
begin
  CodigoVendedor := varCodigoVendedor;
end;

procedure TPresupuesto.SetCodigoZona(varCodigoZona: string);
begin
  CodigoZona := varCodigoZona;
end;

procedure TPresupuesto.SetConcepto(varConcepto: string);
begin
  Concepto := varConcepto;
end;

procedure TPresupuesto.SetFechaIngreso(varFechaIngreso: TDate);
begin
  FechaIngreso := varFechaIngreso;
end;

procedure TPresupuesto.SetFechaVencimiento(varFechaVencimiento: TDate);
begin
  FechaVencimiento := varFechaVencimiento;
end;

procedure TPresupuesto.SetFormaLibre(varFormaLibre: Boolean);
begin
  FormaLibre := varFormaLibre;
end;

procedure TPresupuesto.SetIdPresupuesto(varIdPresupuesto: Integer);
begin
  IdPresupuesto := varIdPresupuesto;
end;

procedure TPresupuesto.SetImpuesto(varImpuesto: Double);
begin
  Impuesto := varImpuesto;
end;

procedure TPresupuesto.SetLoginUsuario(varLoginUsuario: string);
begin
  LoginUsuario := varLoginUsuario;
end;

procedure TPresupuesto.SetMontoDescuento(varMontoDescuento: Double);
begin
  MontoDescuento := varMontoDescuento;
end;

procedure TPresupuesto.SetMontoNeto(varMontoNeto: Double);
begin
  MontoNeto := varMontoNeto;
end;

procedure TPresupuesto.SetNumero(varNumero: string);
begin
  Numero := varNumero;
end;

procedure TPresupuesto.SetObservaciones(varObservaciones: string);
begin
  Observaciones := varObservaciones;
end;

procedure TPresupuesto.SetPDescuento(varPDescuento: Double);
begin
  PDescuento := varPDescuento;
end;

procedure TPresupuesto.SetPIva(varPIva: Double);
begin
  PIva := varPIva;
end;

procedure TPresupuesto.SetSubTotal(varSubTotal: Double);
begin
  SubTotal := varSubTotal;
end;

procedure TPresupuesto.SetTasaDolar(varTasaDolar: Double);
begin
  TasaDolar := varTasaDolar;
end;

procedure TPresupuesto.SetTiempoIngreso(varTiempoIngreso: TDate);
begin
  TiempoIngreso := varTiempoIngreso;
end;

procedure TPresupuesto.SetTotal(varTotal: Double);
begin
  Total := varTotal;
end;

function TPresupuesto.GetCodigoCliente: string;
begin
  Result := CodigoCliente;
end;

function TPresupuesto.GetCodigoEsquemaPago: string;
begin
  Result := CodigoEsquemaPago;
end;

function TPresupuesto.GetCodigoMoneda: string;
begin
  Result := CodigoMoneda;
end;

function TPresupuesto.GetCodigoVendedor: string;
begin
  Result := CodigoVendedor;
end;

function TPresupuesto.GetCodigoZona: string;
begin
  Result := CodigoZona;
end;

function TPresupuesto.GetConcepto: string;
begin
  Result := Concepto;
end;

function TPresupuesto.GetFechaIngreso: TDate;
begin
  Result := FechaIngreso;
end;

function TPresupuesto.GetFechaVencimiento: TDate;
begin
  Result := FechaVencimiento;
end;

function TPresupuesto.GetFormaLibre: Boolean;
begin
  Result := FormaLibre;
end;

function TPresupuesto.GetIdPresupuesto: Integer;
begin
  Result := IdPresupuesto;
end;

function TPresupuesto.GetImpuesto: Double;
begin
  Result := Impuesto;
end;

function TPresupuesto.GetLoginUsuario: string;
begin
  Result := LoginUsuario;
end;

function TPresupuesto.GetMontoDescuento: Double;
begin
  Result := MontoDescuento;
end;

function TPresupuesto.GetMontoNeto: Double;
begin
  Result := MontoNeto;
end;

function TPresupuesto.GetNumero: string;
begin
  Result := Numero;
end;

function TPresupuesto.GetObservaciones: string;
begin
  Result := Observaciones;
end;

function TPresupuesto.GetPDescuento: Double;
begin
  Result := PDescuento;
end;

function TPresupuesto.GetPIva: Double;
begin
  Result := PIva;
end;

function TPresupuesto.GetSubTotal: Double;
begin
  Result := SubTotal;
end;

function TPresupuesto.GetTasaDolar: Double;
begin
  Result := TasaDolar;
end;

function TPresupuesto.GetTiempoIngreso: TDate;
begin
  Result := TiempoIngreso;
end;

function TPresupuesto.GetTotal: Double;
begin
  Result := Total
end;

end.
