 unit ClsDocumentoAnulado;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TDocumentoAnulado }

  TDocumentoAnulado = class(TGenericoBD)
    private
      Desde: string;
      NumeroDocumento: string;
      Serie: string;
      NumeroControl: string;
      FechaDocumento : TDate;
      CodigoCliente : string;
      CodigoProveedor : string;
      CodigoZona : string;
      SubTotal : Double;
      Impuesto : Double;
      Total : Double;
      PIva : Double;
      LoginUsuario: string;
      Fecha: TDate;
      Hora: TTime;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArraystring; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetDesde(varDesde: string);
      procedure SetNumeroDocumento(varNumeroDocumento: string);
      procedure SetSerie(varSerie: string);
      procedure SetNumeroControl(varNumeroControl: string);
      procedure SetFechaDocumento(varFechaDocumento: TDate);
      procedure SetCodigoCliente(varCodigoCliente: string);
      procedure SetCodigoProveedor(varCodigoProveedor: string);
      procedure SetCodigoZona(varCodigoZona: string);
      procedure SetSubTotal(varSubTotal: Double);
      procedure SetImpuesto(varImpuesto: Double);
      procedure SetTotal(varTotal: Double);
      procedure SetPIva(varPIva: Double);
      procedure SetLoginUsuario(varLoginUsuario: string);
      procedure SetFecha(varFecha: TDate);
      procedure SetHora(varHora: TTime);
      function GetDesde: string;
      function GetNumeroDocumento: string;
      function GetSerie: string;
      function GetNumeroControl: string;
      function GetFechaDocumento : TDate;
      function GetCodigoCliente : string;
      function GetCodigoProveedor : string;
      function GetCodigoZona : string;
      function GetSubTotal : Double;
      function GetImpuesto : Double;
      function GetTotal : Double;
      function GetPIva : Double;
      function GetLoginUsuario: string;
      function GetFecha: TDate;
      function GetHora: TTime;
  end;

implementation

uses DB;

{ TDocumentoAnulado }

procedure TDocumentoAnulado.Ajustar;
begin

end;

procedure TDocumentoAnulado.Mover(Ds: TClientDataSet);
begin
  Desde := Ds.FieldValues['desde'];
  NumeroDocumento := Ds.FieldValues['numero_documento'];
  Serie := Ds.FieldValues['serie'];
  NumeroControl := Ds.FieldValues['numero_control'];
  FechaDocumento := Ds.FieldValues['fecha_documento'];
  CodigoCliente := Ds.FieldValues['codigo_cliente'];
  CodigoProveedor := Ds.FieldValues['codigo_proveedor'];
  CodigoZona := Ds.FieldValues['codigo_zona'];
  SubTotal := Ds.FieldValues['sub_total'];
  Impuesto := Ds.FieldValues['impuesto'];
  Total := Ds.FieldValues['total'];
  PIva := Ds.FieldValues['p_iva'];
  LoginUsuario := Ds.FieldValues['login_usuario'];
  Fecha := Ds.FieldValues['fecha'];
  Hora := Ds.FieldValues['hora'];
end;

function TDocumentoAnulado.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,15);
  Params[0] := Desde;;
  Params[1] := NumeroDocumento;
  Params[2] := Serie;
  Params[3] := NumeroControl;
  Params[4] := VarFromDateTime(FechaDocumento);
  Params[5] := CodigoCliente;
  Params[6] := CodigoProveedor;
  Params[7] := CodigoZona;
  Params[8] := SubTotal;
  Params[9] := Impuesto;
  Params[10] := Total;
  Params[11] := PIva;
  Params[12] := LoginUsuario;
  Params[13] := VarFromDateTime(Fecha);
  Params[14] := VarFromDateTime(Hora);
  Result := Params;
end;

function TDocumentoAnulado.GetCampos: TArraystring;
var
  Campos: TArraystring;
begin
  SetLength(Campos,8);
  Campos[0] := 'desde';
  Campos[1] := 'numero_documento';
  Campos[2] := 'codigo_cliente';
  Campos[3] := 'codigo_proveedor';
  Campos[4] := 'codigo_zona';
  Campos[5] := 'serie';
  Campos[6] := 'numero_control';
  Campos[7] := 'login_usuario';
  Result := Campos;
end;

function TDocumentoAnulado.GetCondicion: string;
begin
  Result:= '("desde" = ' + QuotedStr(Desde) + ') AND' + '("numero_documento" = ' + QuotedStr(NumeroDocumento) + ') AND ' + '("numero_documento" = ' + QuotedStr(Serie) + ') AND ' + '("numero_control" = ' + QuotedStr(NumeroControl) + ')';
end;

procedure TDocumentoAnulado.AntesInsertar;
begin

end;

procedure TDocumentoAnulado.AntesModificar;
begin

end;

procedure TDocumentoAnulado.AntesEliminar;
begin

end;

procedure TDocumentoAnulado.DespuesInsertar;
begin

end;

procedure TDocumentoAnulado.DespuesModificar;
begin

end;

procedure TDocumentoAnulado.DespuesEliminar;
begin

end;

constructor TDocumentoAnulado.Create;
var
  ClavesPrimarias: TArraystring;
begin
  inherited Create;
  SetTabla('tbl_documentos_anulados');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(15)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "fecha_documento"=$5, "codigo_cliente"=$6, "codigo_proveedor"=$7, "codigo_zona"=$8, "sub_total"=$9, "impuesto"=$10, ' +
                  '"total"=$11, "p_iva"=$12, "login_usuario"=$13, "fecha"=$14, "hora"=$15 ' +
                  ' WHERE ("desde"=$1) AND ("numero_documento"=$2) AND ("serie"=$3) AND ("numero_control"=$4)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("desde"=$1) AND ("numero_documento"=$2) AND ("serie"=$3) AND ("numero_control"=$4)');
  SetLength(ClavesPrimarias,4);
  ClavesPrimarias[0] := '"desde"';
  ClavesPrimarias[1] := '"numero_documento"';
  ClavesPrimarias[2] := '"serie"';
  ClavesPrimarias[3] := '"numero_control"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TDocumentoAnulado.SetDesde(varDesde: string);
begin
  Desde := varDesde;
end;

procedure TDocumentoAnulado.SetFecha(varFecha: TDate);
begin
  Fecha := varFecha;
end;

procedure TDocumentoAnulado.SetHora(varHora: TTime);
begin
  Hora := varHora;
end;

procedure TDocumentoAnulado.SetLoginUsuario(varLoginUsuario: string);
begin
  LoginUsuario := varLoginUsuario;
end;

procedure TDocumentoAnulado.SetNumeroControl(varNumeroControl: string);
begin
  NumeroControl := varNumeroControl;
end;

procedure TDocumentoAnulado.SetNumeroDocumento(varNumeroDocumento: string);
begin
  NumeroDocumento := varNumeroDocumento;
end;

procedure TDocumentoAnulado.SetSerie(varSerie: string);
begin
  Serie := varSerie;
end;

procedure TDocumentoAnulado.SetFechaDocumento(varFechaDocumento: TDate);
begin
  FechaDocumento := varFechaDocumento;
end;

procedure TDocumentoAnulado.SetCodigoCliente(varCodigoCliente: string);
begin
  CodigoCliente := varCodigoCliente;
end;

procedure TDocumentoAnulado.SetCodigoProveedor(varCodigoProveedor: string);
begin
  CodigoProveedor := varCodigoProveedor;
end;

procedure TDocumentoAnulado.SetCodigoZona(varCodigoZona: string);
begin
  CodigoZona := varCodigoZona;
end;

procedure TDocumentoAnulado.SetSubTotal(varSubTotal: Double);
begin
  SubTotal := varSubTotal;
end;

procedure TDocumentoAnulado.SetImpuesto(varImpuesto: Double);
begin
  Impuesto := varImpuesto;
end;

procedure TDocumentoAnulado.SetTotal(varTotal: Double);
begin
  Total := varTotal;
end;

procedure TDocumentoAnulado.SetPIva(varPIva: Double);
begin
  PIva := varPIva;
end;

function TDocumentoAnulado.GetDesde: string;
begin
  Result := Desde;
end;

function TDocumentoAnulado.GetFecha: TDate;
begin
  Result := Fecha;
end;

function TDocumentoAnulado.GetHora: TTime;
begin
  Result := Hora;
end;

function TDocumentoAnulado.GetLoginUsuario: string;
begin
  Result := LoginUsuario;
end;

function TDocumentoAnulado.GetNumeroControl: string;
begin
  Result := NumeroControl;
end;

function TDocumentoAnulado.GetNumeroDocumento: string;
begin
  Result := NumeroDocumento;
end;

function TDocumentoAnulado.GetSerie: string;
begin
  Result := Serie;
end;

function TDocumentoAnulado.GetFechaDocumento : TDate;
begin
  Result := FechaDocumento;
end;

function TDocumentoAnulado.GetCodigoCliente : string;
begin
  Result := CodigoCliente;
end;

function TDocumentoAnulado.GetCodigoProveedor : string;
begin
  Result := CodigoProveedor;
end;

function TDocumentoAnulado.GetCodigoZona : string;
begin
  Result := CodigoZona;
end;

function TDocumentoAnulado.GetSubTotal : Double;
begin
  Result := SubTotal;
end;

function TDocumentoAnulado.GetImpuesto : Double;
begin
  Result := Impuesto;
end;

function TDocumentoAnulado.GetTotal : Double;
begin
  Result := Total;
end;

function TDocumentoAnulado.GetPIva : Double;
begin
  Result := PIva;
end;

end.
