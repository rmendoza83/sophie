unit ClsDetPresupuesto;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TDetPresupuesto }

  TDetPresupuesto = class(TGenericoBD)
    private
      CodigoProducto: string;
      Referencia: string;
      Descripcion: string;
      Cantidad: Double;
      Precio: Double;
      PDescuento: Double;
      MontoDescuento: Double;
      Impuesto: Double;
      PIva: Double;
      Total: Double;
      IdPresupuesto: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigoProducto(varCodigoProducto: string);
      procedure SetReferencia(varReferencia: string);
      procedure SetDescripcion(varDescripcion: string);
      procedure SetCantidad(varCantidad: Double);
      procedure SetPrecio(varPrecio: Double);
      procedure SetPDescuento(varPDescuento: Double);
      procedure SetMontoDescuento(varMontoDescuento: Double);
      procedure SetImpuesto(varImpuesto: Double);
      procedure SetPIva(varPIva: Double);
      procedure SetTotal(varTotal: Double);
      procedure SetIdPresupuesto(varIdPresupuesto: Integer);
      function GetCodigoProducto: string;
      function GetReferencia: string;
      function GetDescripcion: string;
      function GetCantidad: Double;
      function GetPrecio: Double;
      function GetPDescuento: Double;
      function GetMontoDescuento: Double;
      function GetImpuesto: Double;
      function GetPIva: Double;
      function GetTotal: Double;
      function GetIdPresupuesto: Integer;
      function EliminarIdPresupuesto(varIdPresupuesto: Integer): Boolean;
      function ObtenerIdPresupuesto(varIdPresupuesto: Integer): TClientDataSet;
  end;

implementation

{ TDetPresupuesto }

procedure TDetPresupuesto.Ajustar;
begin

end;

procedure TDetPresupuesto.Mover(Ds: TClientDataSet);
begin
  CodigoProducto := Ds.FieldValues['codigo_producto'];
  Referencia := Ds.FieldValues['referencia'];
  Descripcion := Ds.FieldValues['descripcion'];
  Cantidad := Ds.FieldValues['cantidad'];
  Precio := Ds.FieldValues['precio'];
  PDescuento := Ds.FieldValues['p_descuento'];
  MontoDescuento := Ds.FieldValues['monto_descuento'];
  Impuesto := Ds.FieldValues['impuesto'];
  PIva := Ds.FieldValues['p_iva'];
  Total := Ds.FieldValues['total'];
  IdPresupuesto := Ds.FieldValues['id_presupuesto'];
end;

function TDetPresupuesto.ObtenerIdPresupuesto(varIdPresupuesto: Integer): TClientDataSet;
begin
  Result := ObtenerListaCondicionSQL(' WHERE "id_presupuesto" = ' + IntToStr(varIdPresupuesto));
end;

function TDetPresupuesto.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,11);
  Params[0] := CodigoProducto;
  Params[1] := Referencia;
  Params[2] := Descripcion;
  Params[3] := Cantidad;
  Params[4] := Precio;
  Params[5] := PDescuento;
  Params[6] := MontoDescuento;
  Params[7] := Impuesto;
  Params[8] := PIva;
  Params[9] := Total;
  Params[10] := IdPresupuesto;
  Result:= Params;
end;

function TDetPresupuesto.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,3);
  Campos[0] := 'codigo_producto';
  Campos[1] := 'referencia';
  Campos[2] := 'descripcion';
end;

function TDetPresupuesto.GetCondicion: string;
begin
  Result := '"id_presupuesto" = ' + QuotedStr(IntToStr(IdPresupuesto)) + ' AND "codigo_producto" =' + QuotedStr(CodigoProducto);
end;

procedure TDetPresupuesto.AntesInsertar;
begin

end;

procedure TDetPresupuesto.AntesModificar;
begin

end;

procedure TDetPresupuesto.AntesEliminar;
begin

end;

procedure TDetPresupuesto.DespuesInsertar;
begin

end;

procedure TDetPresupuesto.DespuesModificar;
begin

end;

function TDetPresupuesto.EliminarIdPresupuesto(varIdPresupuesto: Integer): Boolean;
begin
  Result := EjecutarSQL('DELETE FROM ' + GetTabla + ' WHERE "id_presupuesto" = ' + IntToStr(varIdPresupuesto));
end;

procedure TDetPresupuesto.DespuesEliminar;
begin

end;

constructor TDetPresupuesto.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_det_presupuestos');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(11)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "referencia"=$2, "descripcion"=$3, "cantidad"=$4, "precio"=$5, "p_descuento"=$6, "monto_descuento"=$7, "impuesto"=8, "p_iva"=$9, "total"=$10 ' +
                  'WHERE ("id_presupuesto"=$11) AND ("codigo_producto"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("id_presupuesto"=$11) AND ("codigo_producto"=$1)');
  SetLength(ClavesPrimarias,2);
  ClavesPrimarias[0] := '"id_presupuesto"';
  ClavesPrimarias[1] := '"codigo_producto"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TDetPresupuesto.SetCantidad(varCantidad: Double);
begin
  Cantidad := varCantidad;
end;

procedure TDetPresupuesto.SetCodigoProducto(varCodigoProducto: string);
begin
  CodigoProducto := varCodigoProducto;
end;

procedure TDetPresupuesto.SetDescripcion(varDescripcion: string);
begin
  Descripcion := varDescripcion;
end;

procedure TDetPresupuesto.SetIdPresupuesto(varIdPresupuesto: Integer);
begin
  IdPresupuesto := varIdPresupuesto;
end;

procedure TDetPresupuesto.SetImpuesto(varImpuesto: Double);
begin
  Impuesto := varImpuesto;
end;

procedure TDetPresupuesto.SetMontoDescuento(varMontoDescuento: Double);
begin
  MontoDescuento := varMontoDescuento;
end;

procedure TDetPresupuesto.SetPDescuento(varPDescuento: Double);
begin
  PDescuento := varPDescuento;
end;

procedure TDetPresupuesto.SetPIva(varPIva: Double);
begin
  PIva := varPIva;
end;

procedure TDetPresupuesto.SetPrecio(varPrecio: Double);
begin
  Precio := varPrecio;
end;

procedure TDetPresupuesto.SetReferencia(varReferencia: string);
begin
  Referencia := varReferencia;
end;

procedure TDetPresupuesto.SetTotal(varTotal: Double);
begin
  Total := varTotal;
end;

function TDetPresupuesto.GetCantidad: Double;
begin
  Result := Cantidad;
end;

function TDetPresupuesto.GetCodigoProducto: string;
begin
  Result := CodigoProducto;
end;

function TDetPresupuesto.GetDescripcion: string;
begin
  Result :=  Descripcion;
end;

function TDetPresupuesto.GetIdPresupuesto: Integer;
begin
  Result := IdPresupuesto;
end;

function TDetPresupuesto.GetImpuesto: Double;
begin
  Result := Impuesto;
end;

function TDetPresupuesto.GetMontoDescuento: Double;
begin
  Result := MontoDescuento;
end;

function TDetPresupuesto.GetPDescuento: Double;
begin
  Result := PDescuento;
end;

function TDetPresupuesto.GetPIva: Double;
begin
  Result := PIva;
end;

function TDetPresupuesto.GetPrecio: Double;
begin
  Result := Precio;
end;

function TDetPresupuesto.GetReferencia: string;
begin
  Result := Referencia;
end;

function TDetPresupuesto.GetTotal: Double;
begin
  Result := Total;
end;

end.
