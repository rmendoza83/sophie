unit ClsDetalleCalculoPagoComision;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TDetalleCalculoPagoComision }

  TDetalleCalculoPagoComision = class(TGenericoBD)
    private
      Desde: string;
      Numero: string;
      CodigoVendedor: string;
      FechaIngreso: TDate;
      CodigoProducto: string;
      Referencia: string;
      Descripcion: string;
      Cantidad: Double;
      Precio: Double;
      PDescuento: Double;
      MontoDescuento: Double;
      Impuesto: Double;
      PIva: Double;
      Total: Double;
      IdFacturacion: Integer;
      CodigoDivision: string;
      CodigoLinea: string;
      CodigoFamilia: string;
      CodigoClase: string;
      CodigoManufactura: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      function GetDesde: string;
      function GetNumero: string;
      function GetCodigoVendedor: string;
      function GetFechaIngreso: TDate;
      function GetCodigoProducto: string;
      function GetReferencia: string;
      function GetDescripcion: string;
      function GetCantidad: Double;
      function GetPrecio: Double;
      function GetPDescuento: Double;
      function GetMontoDescuento: Double;
      function GetImpuesto: Double;
      function GetPIva: Double;
      function GetTotal: Double;
      function GetIdFacturacion: Integer;
      function GetCodigoDivision: string;
      function GetCodigoLinea: string;
      function GetCodigoFamilia: string;
      function GetCodigoClase: string;
      function GetCodigoManufactura: string;
      function ObtenerVendedor(varCodigoVendedor: string; varFechaInicial: TDate; varFechaFinal: TDate): TClientDataSet;
  end;

implementation

{ TDetalleCalculoPagoComision }

procedure TDetalleCalculoPagoComision.Ajustar;
begin

end;

procedure TDetalleCalculoPagoComision.Mover(Ds: TClientDataSet);
begin
  Desde := Ds.FieldValues['desde'];
  Numero := Ds.FieldValues['numero'];
  CodigoVendedor := Ds.FieldValues['codigo_vendedor'];
  FechaIngreso := Ds.FieldByName('fecha_ingreso').AsDateTime;
  CodigoProducto := Ds.FieldValues['codigo_producto'];
  Referencia := Ds.FieldValues['referencia'];
  Descripcion := Ds.FieldValues['descripcion'];
  Cantidad := Ds.FieldValues['cantidad'];
  Precio := Ds.FieldValues['precio'];
  PDescuento := Ds.FieldValues['p_descuento'];
  MontoDescuento := Ds.FieldValues['monto_descuento'];
  Impuesto := Ds.FieldValues['impuesto'];
  PIva := Ds.FieldValues['p_iva'];
  Total := Ds.FieldValues['total'];
  IdFacturacion := Ds.FieldValues['id_facturacion'];
  CodigoDivision := Ds.FieldValues['codigo_division'];
  CodigoLinea := Ds.FieldValues['codigo_linea'];
  CodigoFamilia := Ds.FieldValues['codigo_familia'];
  CodigoClase := Ds.FieldValues['codigo_clase'];
  CodigoManufactura := Ds.FieldValues['codigo_manufactura'];
end;

function TDetalleCalculoPagoComision.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,20);
  Params[0] := Desde;
  Params[1] := Numero;
  Params[2] := CodigoVendedor;
  Params[3] := VarFromDateTime(FechaIngreso);
  Params[4] := CodigoProducto;
  Params[5] := Referencia;
  Params[6] := Descripcion;
  Params[7] := Cantidad;
  Params[8] := Precio;
  Params[9] := PDescuento;
  Params[10] := MontoDescuento;
  Params[11] := Impuesto;
  Params[12] := PIva;
  Params[13] := Total;
  Params[14] := IdFacturacion;
  Params[15] := CodigoDivision;
  Params[16] := CodigoLinea;
  Params[17] := CodigoFamilia;
  Params[18] := CodigoClase;
  Params[19] := CodigoManufactura;
  Result := Params;
end;

function TDetalleCalculoPagoComision.GetCampos: TArrayString;
var
  Campos: TArraystring;
begin
  SetLength(Campos,11);
  Campos[0] := 'desde';
  Campos[1] := 'numero';
  Campos[2] := 'codigo_vendedor';
  Campos[3] := 'codigo_producto';
  Campos[4] := 'referencia';
  Campos[5] := 'descripcion';
  Campos[6] := 'codigo_division';
  Campos[7] := 'codigo_linea';
  Campos[8] := 'codigo_familia';
  Campos[9] := 'codigo_clase';
  Campos[10] := 'codigo_manufactura';
  Result := Campos;
end;

function TDetalleCalculoPagoComision.GetCondicion: string;
begin
  Result:= '';
end;

procedure TDetalleCalculoPagoComision.AntesInsertar;
begin

end;

procedure TDetalleCalculoPagoComision.AntesModificar;
begin

end;

procedure TDetalleCalculoPagoComision.AntesEliminar;
begin

end;

procedure TDetalleCalculoPagoComision.DespuesInsertar;
begin

end;

procedure TDetalleCalculoPagoComision.DespuesModificar;
begin

end;

procedure TDetalleCalculoPagoComision.DespuesEliminar;
begin

end;

constructor TDetalleCalculoPagoComision.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('v_detalle_calculo_pago_comision');
  SetLength(ClavesPrimarias,0);
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

function TDetalleCalculoPagoComision.GetCantidad: Double;
begin
  Result := Cantidad;
end;

function TDetalleCalculoPagoComision.GetCodigoClase: string;
begin
  Result := CodigoClase;
end;

function TDetalleCalculoPagoComision.GetCodigoDivision: string;
begin
  Result := CodigoDivision;
end;

function TDetalleCalculoPagoComision.GetCodigoFamilia: string;
begin
  Result := CodigoFamilia;
end;

function TDetalleCalculoPagoComision.GetCodigoLinea: string;
begin
  Result := CodigoLinea;
end;

function TDetalleCalculoPagoComision.GetCodigoManufactura: string;
begin
  Result := CodigoManufactura;
end;

function TDetalleCalculoPagoComision.GetCodigoProducto: string;
begin
  Result := CodigoProducto;
end;

function TDetalleCalculoPagoComision.GetCodigoVendedor: string;
begin
  Result := CodigoVendedor;
end;

function TDetalleCalculoPagoComision.GetDescripcion: string;
begin
  Result := Descripcion;
end;

function TDetalleCalculoPagoComision.GetDesde: string;
begin
  Result := Desde;
end;

function TDetalleCalculoPagoComision.GetFechaIngreso: TDate;
begin
  Result := FechaIngreso;
end;

function TDetalleCalculoPagoComision.GetIdFacturacion: Integer;
begin
  Result := IdFacturacion;
end;

function TDetalleCalculoPagoComision.GetImpuesto: Double;
begin
  Result := Impuesto;
end;

function TDetalleCalculoPagoComision.GetMontoDescuento: Double;
begin
  Result := MontoDescuento;
end;

function TDetalleCalculoPagoComision.GetNumero: string;
begin
  Result := Numero;
end;

function TDetalleCalculoPagoComision.GetPDescuento: Double;
begin
  Result := PDescuento;
end;

function TDetalleCalculoPagoComision.GetPIva: Double;
begin
  Result := PIva;
end;

function TDetalleCalculoPagoComision.GetPrecio: Double;
begin
  Result := Precio;
end;

function TDetalleCalculoPagoComision.GetReferencia: string;
begin
  Result := Referencia;
end;

function TDetalleCalculoPagoComision.GetTotal: Double;
begin
  Result := Total;
end;

function TDetalleCalculoPagoComision.ObtenerVendedor(varCodigoVendedor: string;
  varFechaInicial, varFechaFinal: TDate): TClientDataSet;
begin
  Result := ObtenerListaCondicionSQL(' WHERE ("codigo_vendedor" = ' + QuotedStr(varCodigoVendedor) + ') AND "fecha_ingreso" BETWEEN ' + QuotedStr(FormatDateTime('YYYY-mm-dd',varFechaInicial)) + ' AND (' + QuotedStr(FormatDateTime('YYYY-mm-dd',varFechaFinal)) + ') ORDER BY id_facturacion_cliente');
end;

end.
