unit ClsContableActivoFijo;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TContableActivoFijo }

  TContableActivoFijo = class(TGenericoBD)
    private
      Anno: Integer;
      Mes: Integer;
      Codigo: string;
      Descripcion: string;
      CodigoFisico: string;
      FechaAdquisicion: TDate;
      CodigoCentroCosto: string;
      MesesVidaUtilProbable: Integer;
      CodigoDepartamento: string;
      CostoReferencial: Double;
      CodigoMoneda: string;
      TasaReferencial: Double;
      Depreciable: Boolean;
      FechaTopeDepreciacion: TDate;
      CostoReal: Double;
      ValorDepreciacionInicial: Double;
      ValorRecuperacion: Double;
      DepreciacionAcumulada: Double;
      ValorLibros: Double;
      CodigoCuentaDebe: string;
      CodigoCuentaHaber: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetAnno(varAnno: Integer);
      procedure SetMes(varMes: Integer);
      procedure SetCodigo(varCodigo: string);
      procedure SetDescripcion(varDescripcion: string);
      procedure SetCodigoFisico(varCodigoFisico: string);
      procedure SetFechaAdquisicion(varFechaAdquisicion: TDate);
      procedure SetCodigoCentroCosto(varCodigoCentroCosto: string);
      procedure SetMesesVidaUtilProbable(varMesesVidaUtilProbable: Integer);
      procedure SetCodigoDepartamento(varCodigoDepartamento: string);
      procedure SetCostoReferencial(varCostoReferencial: Double);
      procedure SetCodigoMoneda(varCodigoMoneda: string);
      procedure SetTasaReferencial(varTasaReferencial: Double);
      procedure SetDepreciable(varDepreciable: Boolean);
      procedure SetFechaTopeDepreciacion(varFechaTopeDepreciacion: TDate);
      procedure SetCostoReal(varCostoReal: Double);
      procedure SetValorDepreciacionInicial(varValorDepreciacionInicial: Double);
      procedure SetValorRecuperacion(varValorRecuperacion: Double);
      procedure SetDepreciacionAcumulada(varDepreciacionAcumulada: Double);
      procedure SetValorLibros(varValorLibros: Double);
      procedure SetCodigoCuentaDebe(varCodigoCuentaDebe: string);
      procedure SetCodigoCuentaHaber(varCodigoCuentaHaber: string);
      function GetAnno: Integer;
      function GetMes: Integer;
      function GetCodigo: string;
      function GetDescripcion: string;
      function GetCodigoFisico: string;
      function GetFechaAdquisicion: TDate;
      function GetCodigoCentroCosto: string;
      function GetMesesVidaUtilProbable: Integer;
      function GetCodigoDepartamento: string;
      function GetCostoReferencial: Double;
      function GetCodigoMoneda: string;
      function GetTasaReferencial: Double;
      function GetDepreciable: Boolean;
      function GetFechaTopeDepreciacion: TDate;
      function GetCostoReal: Double;
      function GetValorDepreciacionInicial: Double;
      function GetValorRecuperacion: Double;
      function GetDepreciacionAcumulada: Double;
      function GetValorLibros: Double;
      function GetCodigoCuentaDebe: string;
      function GetCodigoCuentaHaber: string;
  end;

var
  ContableActivoFijo: TContableActivoFijo;

implementation

uses DB;

{ TContableActivoFijo }

procedure TContableActivoFijo.Ajustar;
begin

end;

procedure TContableActivoFijo.Mover(Ds: TClientDataSet);
begin
  Anno := Ds.FieldValues['anno'];
  Mes := Ds.FieldValues['mes'];
  Codigo := Ds.FieldValues['codigo'];
  Descripcion := Ds.FieldValues['descripcion'];
  CodigoFisico := Ds.FieldValues['codigo_fisico'];
  FechaAdquisicion := Ds.FieldByName('fecha_adquisicion').AsDateTime;
  CodigoCentroCosto := Ds.FieldValues['codigo_centro_costo'];
  MesesVidaUtilProbable := Ds.FieldValues['meses_vida_util_probable'];
  CodigoDepartamento := Ds.FieldValues['codigo_departamento'];
  CostoReferencial := Ds.FieldValues['costo_referencial'];
  CodigoMoneda := Ds.FieldValues['codigo_moneda'];
  TasaReferencial := Ds.FieldValues['tasa_referencial'];
  Depreciable := Ds.FieldValues['depreciable'];
  FechaTopeDepreciacion := Ds.FieldByName('fecha_tope_depreciacion').AsDateTime;
  CostoReal := Ds.FieldValues['costo_real'];
  ValorDepreciacionInicial := Ds.FieldValues['valor_depreciacion_inicial']; 
  ValorRecuperacion := Ds.FieldValues['valor_recuperacion'];
  DepreciacionAcumulada := Ds.FieldValues['depreciacion_acumulada'];
  ValorLibros := Ds.FieldValues['valor_libros'];
  CodigoCuentaDebe := Ds.FieldValues['codigo_cuenta_debe'];
  CodigoCuentaHaber := Ds.FieldValues['codigo_cuenta_haber'];
end;

function TContableActivoFijo.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,21);
  Params[0] := Anno;
  Params[1] := Mes;
  Params[2] := Codigo;
  Params[3] := Descripcion;
  Params[4] := CodigoFisico;
  Params[5] := VarFromDateTime(FechaAdquisicion);
  Params[6] := CodigoCentroCosto;
  Params[7] := MesesVidaUtilProbable;
  Params[8] := CodigoDepartamento;
  Params[9] := CostoReferencial;
  Params[10] := CodigoMoneda;
  Params[11] := TasaReferencial;
  Params[12] := Depreciable;
  Params[13] := VarFromDateTime(FechaTopeDepreciacion);
  Params[14] := CostoReal;
  Params[15] := ValorDepreciacionInicial;
  Params[16] := ValorRecuperacion;
  Params[17] := DepreciacionAcumulada;
  Params[18] := ValorLibros;
  Params[19] := CodigoCuentaDebe;
  Params[20] := CodigoCuentaHaber;
  Result := Params;
end;

function TContableActivoFijo.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,8);
  Campos[0] := 'codigo';
  Campos[1] := 'descripcion';
  Campos[2] := 'codigo_fisico';
  Campos[3] := 'codigo_centro_costo';
  Campos[4] := 'codigo_departamento';
  Campos[5] := 'codigo_moneda';
  Campos[6] := 'codigo_cuenta_debe';
  Campos[7] := 'codigo_cuenta_haber';
  Result := Campos;
end;

function TContableActivoFijo.GetCondicion: string;
begin
  Result := '"anno" = ' + IntToStr(Anno) + ' AND ' +
            '"mes" = ' + IntToStr(Mes) + ' AND ' +
            '"codigo" = ' + QuotedStr(Codigo);
end;

procedure TContableActivoFijo.AntesEliminar;
begin

end;

procedure TContableActivoFijo.AntesInsertar;
begin

end;

procedure TContableActivoFijo.AntesModificar;
begin

end;

procedure TContableActivoFijo.DespuesEliminar;
begin

end;

procedure TContableActivoFijo.DespuesInsertar;
begin

end;

procedure TContableActivoFijo.DespuesModificar;
begin

end;

constructor TContableActivoFijo.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_contable_activo_fijo');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(21)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "descripcion"=$4, "codigo_fisico"=$5, "fecha_adquisicion"=$6, "codigo_centro_costo"=$7, "meses_vida_util_probable"=$8, "codigo_departamento"=$9,"costo_referencial"=$10, ' +
                  '"codigo_moneda"=$11, "tasa_referencial"=$12, "depreciable"=$13, "fecha_tope_depreciacion"=$14, "costo_real"=$15, "valor_depreciacion_inicial"=$16, "valor_recuperacion"=$17, "depreciacion_acumulada"=$18, "valor_libros"=$19, "codigo_cuenta_debe"=$20, ' +
                  '"codigo_cuenta_haber"=$21 WHERE ("anno"=$1) AND ("mes"=$2) AND ("codigo"=$3)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("anno"=$1) AND ("mes"=$2) AND ("codigo"=$3)');
  SetLength(ClavesPrimarias,3);
  ClavesPrimarias[0] := '"anno"';
  ClavesPrimarias[1] := '"mes"';
  ClavesPrimarias[2] := '"codigo"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TContableActivoFijo.SetAnno(varAnno: Integer);
begin
  Anno := varAnno;
end;

procedure TContableActivoFijo.SetMes(varMes: Integer);
begin
  Mes := varMes;
end;

procedure TContableActivoFijo.SetCodigo(varCodigo: string);
begin
  Codigo := varCodigo;
end;

procedure TContableActivoFijo.SetDescripcion(varDescripcion: string);
begin
  Descripcion := varDescripcion;
end;

procedure TContableActivoFijo.SetCodigoFisico(varCodigoFisico: string);
begin
  CodigoFisico := varCodigoFisico;
end;

procedure TContableActivoFijo.SetFechaAdquisicion(varFechaAdquisicion: TDate);
begin
  FechaAdquisicion := varFechaAdquisicion;
end;

procedure TContableActivoFijo.SetCodigoCentroCosto(varCodigoCentroCosto: string);
begin
  CodigoCentroCosto := varCodigoCentroCosto;
end;

procedure TContableActivoFijo.SetMesesVidaUtilProbable(varMesesVidaUtilProbable: Integer);
begin
  MesesVidaUtilProbable := varMesesVidaUtilProbable;
end;

procedure TContableActivoFijo.SetCodigoDepartamento(varCodigoDepartamento: string);
begin
  CodigoDepartamento := varCodigoDepartamento;
end;

procedure TContableActivoFijo.SetCostoReferencial(varCostoReferencial: Double);
begin
  CostoReferencial := varCostoReferencial;
end;

procedure TContableActivoFijo.SetCodigoMoneda(varCodigoMoneda: string);
begin
  CodigoMoneda := varCodigoMoneda;
end;

procedure TContableActivoFijo.SetTasaReferencial(varTasaReferencial: Double);
begin
  TasaReferencial := varTasaReferencial;
end;

procedure TContableActivoFijo.SetDepreciable(varDepreciable: Boolean);
begin
  Depreciable := varDepreciable;
end;

procedure TContableActivoFijo.SetFechaTopeDepreciacion(varFechaTopeDepreciacion: TDate);
begin
  FechaTopeDepreciacion := varFechaTopeDepreciacion;
end;

procedure TContableActivoFijo.SetCostoReal(varCostoReal: Double);
begin
  CostoReal := varCostoReal;
end;

procedure TContableActivoFijo.SetValorDepreciacionInicial(varValorDepreciacionInicial: Double);
begin
  ValorDepreciacionInicial := varValorDepreciacionInicial;
end;

procedure TContableActivoFijo.SetValorRecuperacion(varValorRecuperacion: Double);
begin
  ValorRecuperacion := varValorRecuperacion;
end;

procedure TContableActivoFijo.SetDepreciacionAcumulada(varDepreciacionAcumulada: Double);
begin
  DepreciacionAcumulada := varDepreciacionAcumulada;
end;

procedure TContableActivoFijo.SetValorLibros(varValorLibros: Double);
begin
  ValorLibros := varValorLibros;
end;

procedure TContableActivoFijo.SetCodigoCuentaDebe(varCodigoCuentaDebe: string);
begin
  CodigoCuentaDebe := varCodigoCuentaDebe;
end;

procedure TContableActivoFijo.SetCodigoCuentaHaber(varCodigoCuentaHaber: string);
begin
  CodigoCuentaHaber := varCodigoCuentaHaber;
end;

function TContableActivoFijo.GetAnno: Integer;
begin
  Result := Anno;
end;

function TContableActivoFijo.GetMes: Integer;
begin
  Result := Mes;
end;

function TContableActivoFijo.GetCodigo: string;
begin
  Result := Codigo;
end;

function TContableActivoFijo.GetDescripcion: string;
begin
  Result := Descripcion;
end;

function TContableActivoFijo.GetCodigoFisico: string;
begin
  Result := CodigoFisico;
end;

function TContableActivoFijo.GetFechaAdquisicion: TDate;
begin
  Result := FechaAdquisicion;
end;

function TContableActivoFijo.GetCodigoCentroCosto: string;
begin
  Result := CodigoCentroCosto;
end;

function TContableActivoFijo.GetMesesVidaUtilProbable: Integer;
begin
  Result := MesesVidaUtilProbable;
end;

function TContableActivoFijo.GetCodigoDepartamento: string;
begin
  Result := CodigoDepartamento;
end;

function TContableActivoFijo.GetCostoReferencial: Double;
begin
  Result := CostoReferencial;
end;

function TContableActivoFijo.GetCodigoMoneda: string;
begin
  Result := CodigoMoneda;
end;

function TContableActivoFijo.GetTasaReferencial: Double;
begin
  Result := TasaReferencial;
end;

function TContableActivoFijo.GetDepreciable: Boolean;
begin
  Result := Depreciable;
end;

function TContableActivoFijo.GetFechaTopeDepreciacion: TDate;
begin
  Result := FechaTopeDepreciacion;
end;

function TContableActivoFijo.GetCostoReal: Double;
begin
  Result := CostoReal;
end;

function TContableActivoFijo.GetValorDepreciacionInicial: Double;
begin
  Result := ValorDepreciacionInicial;
end;

function TContableActivoFijo.GetValorRecuperacion: Double;
begin
  Result := ValorRecuperacion;
end;

function TContableActivoFijo.GetDepreciacionAcumulada: Double;
begin
  Result := DepreciacionAcumulada;
end;

function TContableActivoFijo.GetValorLibros: Double;
begin
  Result := ValorLibros;
end;

function TContableActivoFijo.GetCodigoCuentaDebe: string;
begin
  Result := CodigoCuentaDebe;
end;

function TContableActivoFijo.GetCodigoCuentaHaber: string;
begin
  Result := CodigoCuentaHaber;
end;

end.
