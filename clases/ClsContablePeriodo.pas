unit ClsContablePeriodo;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TContablePeriodo }

  TContablePeriodo = class(TGenericoBD)
    private
      Anno: Integer;
      FechaInicio: TDate;
      FechaFin: TDate;
      FechaCierre: TDate;
      LoginUsuarioCierre: string;
      Activo: Boolean;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetAnno(varAnno: Integer);
      procedure SetFechaInicio(varFechaInicio: TDate);
      procedure SetFechaFin(varFechaFin: TDate);
      procedure SetFechaCierre(varFechaCierre: TDate);
      procedure SetLoginUsuarioCierre(varLoginUsuarioCierre: string);
      procedure SetActivo(varActivo: Boolean);
      function GetAnno: Integer;
      function GetFechaInicio: TDate;
      function GetFechaFin: TDate;
      function GetFechaCierre: TDate;
      function GetLoginUsuarioCierre: string;
      function GetActivo: Boolean;
  end;

implementation

uses DB;

{ TContablePeriodo }

procedure TContablePeriodo.Ajustar;
begin

end;

procedure TContablePeriodo.Mover(Ds: TClientDataSet);
begin
  Anno := Ds.FieldValues['anno'];
  FechaInicio := Ds.FieldByName('fecha_inicio').AsDateTime;
  FechaFin := Ds.FieldByName('fecha_fin').AsDateTime;
  FechaCierre := Ds.FieldByName('fecha_cierre').AsDateTime;
  LoginUsuarioCierre := Ds.FieldValues['login_usuario_cierre'];
  Activo := Ds.FieldValues['activo'];
end;

function TContablePeriodo.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,6);
  Params[0] := Anno;
  Params[1] := VarFromDateTime(FechaInicio);
  Params[2] := VarFromDateTime(FechaFin);
  Params[3] := VarFromDateTime(FechaCierre);
  Params[4] := LoginUsuarioCierre;
  Params[5] := Activo;
  Result := Params;
end;

function TContablePeriodo.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,1);
  Campos[0] := 'login_usuario_cierre';
  Result := Campos;
end;

function TContablePeriodo.GetCondicion: string;
begin
  Result := '"anno" = ' + IntToStr(Anno);
end;

procedure TContablePeriodo.AntesEliminar;
begin

end;

procedure TContablePeriodo.AntesInsertar;
begin

end;

procedure TContablePeriodo.AntesModificar;
begin

end;

procedure TContablePeriodo.DespuesEliminar;
begin

end;

procedure TContablePeriodo.DespuesInsertar;
begin

end;

procedure TContablePeriodo.DespuesModificar;
begin

end;

constructor TContablePeriodo.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_contable_periodo');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(6)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "fecha_inicio"=$2, "fecha_fin"=$3, "fecha_cierre"=$4, "login_usuario_cierre"=$5, "activo"=$6 WHERE ("anno"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("anno"=$1)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"anno"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TContablePeriodo.SetAnno(varAnno: Integer);
begin
  Anno := varAnno;
end;

procedure TContablePeriodo.SetFechaInicio(varFechaInicio: TDate);
begin
  FechaInicio := varFechaInicio
end;

procedure TContablePeriodo.SetFechaFin(varFechaFin: TDate);
begin
  FechaFin := varFechaFin;
end;

procedure TContablePeriodo.SetFechaCierre(varFechaCierre: TDate);
begin
  FechaCierre := varFechaCierre;
end;

procedure TContablePeriodo.SetLoginUsuarioCierre(varLoginUsuarioCierre: string);
begin
  LoginUsuarioCierre := varLoginUsuarioCierre;
end;

procedure TContablePeriodo.SetActivo(varActivo: Boolean);
begin
  Activo := varActivo;
end;

function TContablePeriodo.GetAnno: Integer;
begin
  Result := Anno;
end;

function TContablePeriodo.GetFechaInicio: TDate;
begin
  Result := FechaInicio;
end;

function TContablePeriodo.GetFechaFin: TDate;
begin
  Result := FechaFin;
end;

function TContablePeriodo.GetFechaCierre: TDate;
begin
  Result := FechaCierre;
end;

function TContablePeriodo.GetLoginUsuarioCierre: string;
begin
  Result := LoginUsuarioCierre;
end;

function TContablePeriodo.GetActivo: Boolean;
begin
  Result := Activo;
end;

end.
