unit ClsContableComprobante;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TContableComprobante }

  TContableComprobante = class(TGenericoBD)
    private
      Anno: Integer;
      Mes: Integer;
      Numero: string;
      CodigoTipoComprobante: string;
      ConsecutivoComprobante: Integer;
      Automatico: Boolean;
      CodigoMoneda: string;
      FechaRegistro: TDate;
      FechaContable: TDate;
      FechaAutorizacion: TDate;
      LoginUsuarioRegistro: string;
      LoginUsuarioAutorizacion: string;
      TotalDebe: Double;
      TotalHaber: Double;
      Diferencia: Double;
      Observaciones: string;
      IdComprobante: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetAnno(varAnno: Integer);
      procedure SetMes(varMes: Integer);
      procedure SetNumero(varNumero: string);
      procedure SetCodigoTipoComprobante(varCodigoTipoComprobante: string);
      procedure SetConsecutivoComprobante(varConsecutivoComprobante: Integer);
      procedure SetAutomatico(varAutomatico: Boolean);
      procedure SetCodigoMoneda(varCodigoMoneda: string);
      procedure SetFechaRegistro(varFechaRegistro: TDate);
      procedure SetFechaContable(varFechaContable: TDate);
      procedure SetFechaAutorizacion(varFechaAutorizacion: TDate);
      procedure SetLoginUsuarioRegistro(varLoginUsuarioRegistro: string);
      procedure SetLoginUsuarioAutorizacion(varLoginUsuarioAutorizacion: string);
      procedure SetTotalDebe(varTotalDebe: Double);
      procedure SetTotalHaber(varTotalHaber: Double);
      procedure SetDiferencia(varDiferencia: Double);
      procedure SetObservaciones(varObservaciones: string);
      procedure SetIdComprobante(varIdComprobante: Integer);
      function GetAnno: Integer;
      function GetMes: Integer;
      function GetNumero: string;
      function GetCodigoTipoComprobante: string;
      function GetConsecutivoComprobante: Integer;
      function GetAutomatico: Boolean;
      function GetCodigoMoneda: string;
      function GetFechaRegistro: TDate;
      function GetFechaContable: TDate;
      function GetFechaAutorizacion: TDate;
      function GetLoginUsuarioRegistro: string;
      function GetLoginUsuarioAutorizacion: string;
      function GetTotalDebe: Double;
      function GetTotalHaber: Double;
      function GetDiferencia: Double;
      function GetObservaciones: string;
      function GetIdComprobante: Integer;
  end;

var
  ContableComprobante: TContableComprobante;

implementation

uses DB;

{ TContableComprobante }

procedure TContableComprobante.Ajustar;
begin

end;

procedure TContableComprobante.Mover(Ds: TClientDataSet);
begin
  Anno := Ds.FieldValues['anno'];
  Mes := Ds.FieldValues['mes'];
  Numero := Ds.FieldValues['numero'];
  CodigoTipoComprobante := Ds.FieldValues['codigo_tipo_comprobante'];
  ConsecutivoComprobante := Ds.FieldValues['consecutivo_comprobante'];
  Automatico := Ds.FieldValues['automatico'];
  CodigoMoneda := Ds.FieldValues['codigo_moneda'];
  FechaRegistro := Ds.FieldByName('fecha_registro').AsDateTime;
  FechaContable := Ds.FieldByName('fecha_contable').AsDateTime;
  FechaAutorizacion := Ds.FieldByName('fecha_autorizacion').AsDateTime;
  LoginUsuarioRegistro := Ds.FieldValues['login_usuario_registro'];
  LoginUsuarioAutorizacion := Ds.FieldValues['login_usuario_autorizacion'];
  TotalDebe := Ds.FieldValues['total_debe'];
  TotalHaber := Ds.FieldValues['total_haber'];
  Diferencia := Ds.FieldValues['diferencia'];
  Observaciones := Ds.FieldValues['observaciones'];
  IdComprobante := Ds.FieldValues['id_comprobante'];
end;

function TContableComprobante.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,17);
  Params[0] := Anno;
  Params[1] := Mes;
  Params[2] := Numero;
  Params[3] := CodigoTipoComprobante;
  Params[4] := ConsecutivoComprobante;
  Params[5] := Automatico;
  Params[6] := CodigoMoneda;
  Params[7] := VarFromDateTime(FechaRegistro);
  Params[8] := VarFromDateTime(FechaContable);
  Params[9] := VarFromDateTime(FechaAutorizacion);
  Params[10] := LoginUsuarioRegistro;
  Params[11] := LoginUsuarioAutorizacion;
  Params[12] := TotalDebe;
  Params[13] := TotalHaber;
  Params[14] := Diferencia;
  Params[15] := Observaciones;
  Params[16] := IdComprobante;
  Result := Params;
end;

function TContableComprobante.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,6);
  Campos[0] := 'numero';
  Campos[1] := 'codigo_tipo_comprobante';
  Campos[2] := 'codigo_moneda';
  Campos[3] := 'login_usuario_registro';
  Campos[4] := 'login_usuario_autorizacion';
  Campos[5] := 'observaciones';
  Result := Campos;
end;

function TContableComprobante.GetCondicion: string;
begin
  Result := '"anno" = ' + IntToStr(Anno) + ' AND ' +
            '"mes" = ' + IntToStr(Mes) + ' AND ' +
            '"id_comprobante" = ' + IntToStr(IdComprobante);
end;

procedure TContableComprobante.AntesEliminar;
begin

end;

procedure TContableComprobante.AntesInsertar;
begin

end;

procedure TContableComprobante.AntesModificar;
begin

end;

procedure TContableComprobante.DespuesEliminar;
begin

end;

procedure TContableComprobante.DespuesInsertar;
begin

end;

procedure TContableComprobante.DespuesModificar;
begin

end;

constructor TContableComprobante.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_contable_comprobantes');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(17)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "numero"=$3, "codigo_tipo_comprobante"=$4, "consecutivo_comprobante"=$5, "automatico"=$6, "codigo_moneda"=$7, "fecha_registro"=$8, "fecha_contable"=$9, "fecha_autorizacion"=$10, ' +
                  '"login_usuario_registro"=$11, "login_usuario_autorizacion"=$12, "total_debe"=$13, "total_haber"=$14, "diferencia"=$15, "observaciones"=$16 WHERE ("anno"=$1) AND ("mes"=$2) AND ("id_comprobante=$17)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("anno"=$1) AND ("mes"=$2) AND ("id_comprobante=$17)');
  SetLength(ClavesPrimarias,3);
  ClavesPrimarias[0] := '"anno"';
  ClavesPrimarias[1] := '"mes"';
  ClavesPrimarias[2] := '"id_comprobante"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TContableComprobante.SetAnno(varAnno: Integer);
begin
  Anno := varAnno;
end;

procedure TContableComprobante.SetMes(varMes: Integer);
begin
  Mes := varMes;
end;

procedure TContableComprobante.SetNumero(varNumero: string);
begin
  Numero := varNumero;
end;

procedure TContableComprobante.SetCodigoTipoComprobante(varCodigoTipoComprobante: string);
begin
  CodigoTipoComprobante := varCodigoTipoComprobante;
end;

procedure TContableComprobante.SetConsecutivoComprobante(varConsecutivoComprobante: Integer);
begin
  ConsecutivoComprobante := varConsecutivoComprobante;
end;

procedure TContableComprobante.SetAutomatico(varAutomatico: Boolean);
begin
  Automatico := varAutomatico;
end;

procedure TContableComprobante.SetCodigoMoneda(varCodigoMoneda: string);
begin
  CodigoMoneda := varCodigoMoneda;
end;

procedure TContableComprobante.SetFechaRegistro(varFechaRegistro: TDate);
begin
  FechaRegistro := varFechaRegistro;
end;

procedure TContableComprobante.SetFechaContable(varFechaContable: TDate);
begin
  FechaContable := varFechaContable;
end;

procedure TContableComprobante.SetFechaAutorizacion(varFechaAutorizacion: TDate);
begin
  FechaAutorizacion := varFechaAutorizacion;
end;

procedure TContableComprobante.SetLoginUsuarioRegistro(varLoginUsuarioRegistro: string);
begin
  LoginUsuarioRegistro := varLoginUsuarioRegistro;
end;

procedure TContableComprobante.SetLoginUsuarioAutorizacion(varLoginUsuarioAutorizacion: string);
begin
  LoginUsuarioAutorizacion := varLoginUsuarioAutorizacion;
end;

procedure TContableComprobante.SetTotalDebe(varTotalDebe: Double);
begin
  TotalDebe := varTotalDebe;
end;

procedure TContableComprobante.SetTotalHaber(varTotalHaber: Double);
begin
  TotalHaber := varTotalHaber;
end;

procedure TContableComprobante.SetDiferencia(varDiferencia: Double);
begin
  Diferencia := varDiferencia;
end;

procedure TContableComprobante.SetObservaciones(varObservaciones: string);
begin
  Observaciones := varObservaciones;
end;

procedure TContableComprobante.SetIdComprobante(varIdComprobante: Integer);
begin
  IdComprobante := varIdComprobante;
end;

function TContableComprobante.GetAnno: Integer;
begin
  Result := Anno;
end;

function TContableComprobante.GetMes: Integer;
begin
  Result := Mes;
end;

function TContableComprobante.GetNumero: string;
begin
  Result := Numero;
end;

function TContableComprobante.GetCodigoTipoComprobante: string;
begin
  Result := CodigoTipoComprobante;
end;

function TContableComprobante.GetConsecutivoComprobante: Integer;
begin
  Result := ConsecutivoComprobante;
end;

function TContableComprobante.GetAutomatico: Boolean;
begin
  Result := Automatico;
end;

function TContableComprobante.GetCodigoMoneda: string;
begin
  Result := CodigoMoneda;
end;

function TContableComprobante.GetFechaRegistro: TDate;
begin
  Result := FechaRegistro;
end;

function TContableComprobante.GetFechaContable: TDate;
begin
  Result := FechaContable;
end;

function TContableComprobante.GetFechaAutorizacion: TDate;
begin
  Result := FechaAutorizacion;
end;

function TContableComprobante.GetLoginUsuarioRegistro: string;
begin
  Result := LoginUsuarioRegistro;
end;

function TContableComprobante.GetLoginUsuarioAutorizacion: string;
begin
  Result := LoginUsuarioAutorizacion;
end;

function TContableComprobante.GetTotalDebe: Double;
begin
  Result := TotalDebe;
end;

function TContableComprobante.GetTotalHaber: Double;
begin
  Result := TotalHaber;
end;

function TContableComprobante.GetDiferencia: Double;
begin
  Result := Diferencia;
end;

function TContableComprobante.GetObservaciones: string;
begin
  Result := Observaciones;
end;

function TContableComprobante.GetIdComprobante: Integer;
begin
  Result := IdComprobante;
end;

end. 
