unit ClsDetRetencionIslr;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TDetRetencionIslr }

  TDetRetencionIslr = class(TGenericoBD)
    private
      Numero: string;
      Desde: string;
      NumeroDocumento: string;
      CodigoRetencion1: string;
      MontoRetencion1: Double;
      CodigoRetencion2: string;
      MontoRetencion2: Double;
      CodigoRetencion3: string;
      MontoRetencion3: Double;
      CodigoRetencion4: string;
      MontoRetencion4: Double;
      CodigoRetencion5: string;
      MontoRetencion5: Double;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArraystring; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetNumero(varNumero: string);
      procedure SetDesde(varDesde: string);
      procedure SetNumeroDocumento(varNumeroDocumento: string);
      procedure SetCodigoRetencion1(varCodigoRetencion1: string);
      procedure SetMontoRetencion1(varMontoRetencion1: Double);
      procedure SetCodigoRetencion2(varCodigoRetencion2: string);
      procedure SetMontoRetencion2(varMontoRetencion2: Double);
      procedure SetCodigoRetencion3(varCodigoRetencion3: string);
      procedure SetMontoRetencion3(varMontoRetencion3: Double);
      procedure SetCodigoRetencion4(varCodigoRetencion4: string);
      procedure SetMontoRetencion4(varMontoRetencion4: Double);
      procedure SetCodigoRetencion5(varCodigoRetencion5: string);
      procedure SetMontoRetencion5(varMontoRetencion5: Double);
      function GetNumero: string;
      function GetDesde: string;
      function GetNumeroDocumento: string;
      function GetCodigoRetencion1: string;
      function GetMontoRetencion1: Double;
      function GetCodigoRetencion2: string;
      function GetMontoRetencion2: Double;
      function GetCodigoRetencion3: string;
      function GetMontoRetencion3: Double;
      function GetCodigoRetencion4: string;
      function GetMontoRetencion4: Double;
      function GetCodigoRetencion5: string;
      function GetMontoRetencion5: Double;
      function ObtenerNumero(varNumero: string): TClientDataSet;
      function EliminarNumero(varNumero: string): Boolean;
      function BuscarRetencionDocumento: Boolean;
  end;

implementation

uses
  DB,
  ClsConsecutivo;

{ TDetRetencionIslr }

procedure TDetRetencionIslr.Ajustar;
begin

end;

procedure TDetRetencionIslr.Mover(Ds: TClientDataSet);
begin
  Numero := Ds.FieldValues['numero'];
  Desde := Ds.FieldValues['desde'];
  NumeroDocumento := Ds.FieldValues['numero_documento'];
  CodigoRetencion1 := Ds.FieldValues['codigo_retencion_1'];
  MontoRetencion1 := Ds.FieldValues['monto_retencion_1'];
  CodigoRetencion2 := Ds.FieldValues['codigo_retencion_2'];
  MontoRetencion2 := Ds.FieldValues['monto_retencion_2'];
  CodigoRetencion3 := Ds.FieldValues['codigo_retencion_3'];
  MontoRetencion3 := Ds.FieldValues['monto_retencion_3'];
  CodigoRetencion4 := Ds.FieldValues['codigo_retencion_4'];
  MontoRetencion4 := Ds.FieldValues['monto_retencion_4'];
  CodigoRetencion5 := Ds.FieldValues['codigo_retencion_5'];
  MontoRetencion5 := Ds.FieldValues['monto_retencion_5'];
end;

function TDetRetencionIslr.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,13);
  Params[0] := Numero;
  Params[1] := Desde;
  Params[2] := NumeroDocumento;
  Params[3] := CodigoRetencion1;
  Params[4] := MontoRetencion1;
  Params[5] := CodigoRetencion2;
  Params[6] := MontoRetencion2;
  Params[7] := CodigoRetencion3;
  Params[8] := MontoRetencion3;
  Params[9] := CodigoRetencion4;
  Params[10] := MontoRetencion4;
  Params[11] := CodigoRetencion5;
  Params[12] := MontoRetencion5;
  Result := Params;
end;

function TDetRetencionIslr.GetCampos: TArraystring;
var
  Campos: TArraystring;
begin
  SetLength(Campos,3);
  Campos[0] := 'numero';
  Campos[1] := 'desde';
  Campos[2] := 'numero_documento';
  Result := Campos;
end;

function TDetRetencionIslr.GetCondicion: string;
begin
  Result:= '("numero"=' + QuotedStr(Numero) + ') AND ' +
           '("desde"=' + QuotedStr(Desde) + ') AND ' +
           '("numero_documento"=' + QuotedStr(NumeroDocumento) + ')';
end;

procedure TDetRetencionIslr.AntesInsertar;
begin

end;

procedure TDetRetencionIslr.AntesModificar;
begin

end;

procedure TDetRetencionIslr.AntesEliminar;
begin

end;

procedure TDetRetencionIslr.DespuesInsertar;
begin

end;

procedure TDetRetencionIslr.DespuesModificar;
begin

end;

procedure TDetRetencionIslr.DespuesEliminar;
begin

end;

constructor TDetRetencionIslr.Create;
var
  ClavesPrimarias: TArraystring;
begin
  inherited Create;
  SetTabla('tbl_det_retenciones_islr');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(13)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "codigo_retencion_1"=$4, "monto_retencion_1"=$5, "codigo_retencion_2"=$6, "monto_retencion_2"=$7, "codigo_retencion_3"=$8, "monto_retencion_3"=$9,' +
                  '"codigo_retencion_4"=$10, "monto_retencion_4"=$11, "codigo_retencion_5"=$12, monto_retencion_5"=$13 WHERE (("numero"=$1) AND ("desde"=$2) AND ("numero_documento"=$3))');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE (("numero"=$1) AND ("desde"=$2) AND ("numero_documento"=$3))');
  SetLength(ClavesPrimarias,3);
  ClavesPrimarias[0] := '"numero"';
  ClavesPrimarias[1] := '"desde"';
  ClavesPrimarias[2] := '"numero_documento"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TDetRetencionIslr.SetNumero(varNumero: string);
begin
  Numero := varNumero;
end;

procedure TDetRetencionIslr.SetDesde(varDesde: string);
begin
  Desde := varDesde
end;

procedure TDetRetencionIslr.SetNumeroDocumento(varNumeroDocumento: string);
begin
  NumeroDocumento := varNumeroDocumento;
end;

procedure TDetRetencionIslr.SetCodigoRetencion1(varCodigoRetencion1: string);
begin
  CodigoRetencion1 := varCodigoRetencion1;
end;

procedure TDetRetencionIslr.SetMontoRetencion1(varMontoRetencion1: Double);
begin
  MontoRetencion1 := varMontoRetencion1;
end;

procedure TDetRetencionIslr.SetCodigoRetencion2(varCodigoRetencion2: string);
begin
  CodigoRetencion2 := varCodigoRetencion2;
end;

procedure TDetRetencionIslr.SetMontoRetencion2(varMontoRetencion2: Double);
begin
  MontoRetencion2 := varMontoRetencion2;
end;

procedure TDetRetencionIslr.SetCodigoRetencion3(varCodigoRetencion3: string);
begin
  CodigoRetencion3 := varCodigoRetencion3;
end;

procedure TDetRetencionIslr.SetMontoRetencion3(varMontoRetencion3: Double);
begin
  MontoRetencion3 := varMontoRetencion3;
end;

procedure TDetRetencionIslr.SetCodigoRetencion4(varCodigoRetencion4: string);
begin
   CodigoRetencion4 := varCodigoRetencion4;
end;

procedure TDetRetencionIslr.SetMontoRetencion4(varMontoRetencion4: Double);
begin
   MontoRetencion4 := varMontoRetencion4;
end;

procedure TDetRetencionIslr.SetCodigoRetencion5(varCodigoRetencion5: string);
begin
  CodigoRetencion5 := varCodigoRetencion5;
end;

procedure TDetRetencionIslr.SetMontoRetencion5(varMontoRetencion5: Double);
begin
  MontoRetencion5 := varMontoRetencion5;
end;

function TDetRetencionIslr.GetNumero: string;
begin
  Result := Numero;
end;

function TDetRetencionIslr.GetDesde: string;
begin
  Result := Desde;
end;

function TDetRetencionIslr.GetNumeroDocumento: string;
begin
  Result := NumeroDocumento;
end;

function TDetRetencionIslr.GetCodigoRetencion1: string;
begin
  Result := CodigoRetencion1;
end;

function TDetRetencionIslr.GetMontoRetencion1: Double;
begin
  Result := MontoRetencion1;
end;

function TDetRetencionIslr.GetCodigoRetencion2: string;
begin
  Result := CodigoRetencion2;
end;

function TDetRetencionIslr.GetMontoRetencion2: Double;
begin
  Result := MontoRetencion2;
end;

function TDetRetencionIslr.GetCodigoRetencion3: string;
begin
  Result := CodigoRetencion3;
end;

function TDetRetencionIslr.GetMontoRetencion3: Double;
begin
  Result := MontoRetencion3;
end;

function TDetRetencionIslr.GetCodigoRetencion4: string;
begin
  Result := CodigoRetencion4;
end;

function TDetRetencionIslr.GetMontoRetencion4: Double;
begin
  Result := MontoRetencion4;
end;

function TDetRetencionIslr.GetCodigoRetencion5: string;
begin
  Result := CodigoRetencion5;
end;

function TDetRetencionIslr.GetMontoRetencion5: Double;
begin
  Result := MontoRetencion5;
end;

function TDetRetencionIslr.ObtenerNumero(varNumero: string): TClientDataSet;
begin
  Result := ObtenerListaCondicionSQL(' WHERE "numero" = ' + QuotedStr(varNumero));
end;

function TDetRetencionIslr.EliminarNumero(varNumero: string): Boolean;
begin
  Result := EjecutarSQL('DELETE FROM ' + GetTabla + ' WHERE "numero" = ' + QuotedStr(varNumero));
end;

function TDetRetencionIslr.BuscarRetencionDocumento: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("desde" = ' + QuotedStr(GetDesde) + ') AND ("numero_documento" = ' + QuotedStr(GetNumeroDocumento) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

end.
