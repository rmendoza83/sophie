unit ClsContableDetalleComprobante;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TContableDetalleComprobante }

  TContableDetalleComprobante = class(TGenericoBD)
    private
      Anno: Integer;
      Mes: Integer;
      Item: Integer;
      CodigoCuenta: string;
      MontoDebe: Double;
      MontoHaber: Double;
      NumeroDocumento: string;
      CodigoCentroCosto: string;
      Concepto: string;
      DetalleConcepto: string;
      IdComprobante: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetAnno(varAnno: Integer);
      procedure SetMes(varMes: Integer);
      procedure SetItem(varItem: Integer);
      procedure SetCodigoCuenta(varCodigoCuenta: string);
      procedure SetMontoDebe(varMontoDebe: Double);
      procedure SetMontoHaber(varMontoHaber: Double);
      procedure SetNumeroDocumento(varNumeroDocumento: string);
      procedure SetCodigoCentroCosto(varCodigoCentroCosto: string);
      procedure SetConcepto(varConcepto: string);
      procedure SetDetalleConcepto(varDetalleConcepto: string);
      procedure SetIdComprobante(varIdComprobante: Integer);
      function GetAnno: Integer;
      function GetMes: Integer;
      function GetItem: Integer;
      function GetCodigoCuenta: string;
      function GetMontoDebe: Double;
      function GetMontoHaber: Double;
      function GetNumeroDocumento: string;
      function GetCodigoCentroCosto: string;
      function GetConcepto: string;
      function GetDetalleConcepto: string;
      function GetIdComprobante: Integer;
  end;

var
  ContableDetalleComprobante: TContableDetalleComprobante;

implementation

uses DB;

{ TContableDetalleComprobante }

procedure TContableDetalleComprobante.Ajustar;
begin

end;

procedure TContableDetalleComprobante.Mover(Ds: TClientDataSet);
begin
  Anno := Ds.FieldValues['anno'];
  Mes := Ds.FieldValues['mes'];
  Item := Ds.FieldValues['item'];
  CodigoCuenta := Ds.FieldValues['codigo_cuenta'];
  MontoDebe := Ds.FieldValues['monto_debe'];
  MontoHaber := Ds.FieldValues['monto_haber'];
  NumeroDocumento := Ds.FieldValues['numero_documento'];
  CodigoCentroCosto := Ds.FieldValues['codigo_centro_costo'];
  Concepto := Ds.FieldValues['concepto'];
  DetalleConcepto := Ds.FieldValues['detalle_concepto'];
  IdComprobante := Ds.FieldValues['id_comprobante'];
end;

function TContableDetalleComprobante.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,11);
  Params[0] := Anno;
  Params[1] := Mes;
  Params[2] := Item;
  Params[3] := CodigoCuenta;
  Params[4] := MontoDebe;
  Params[5] := MontoHaber;
  Params[6] := NumeroDocumento;
  Params[7] := CodigoCentroCosto;
  Params[8] := Concepto;
  Params[9] := DetalleConcepto;
  Params[10] := IdComprobante;
  Result := Params;
end;

function TContableDetalleComprobante.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,5);
  Campos[0] := 'codigo_cuenta';
  Campos[1] := 'numero_documento';
  Campos[2] := 'codigo_centro_costo';
  Campos[3] := 'concepto';
  Campos[4] := 'detalle_concepto';
  Result := Campos;
end;

function TContableDetalleComprobante.GetCondicion: string;
begin
  Result := '"anno" = ' + IntToStr(Anno) + ' AND ' +
            '"mes" = ' + IntToStr(Mes) + ' AND ' +
            '"id_comprobante" = ' + IntToStr(IdComprobante) + ' AND ' +
            '"item" = ' + IntToStr(Item);
end;

procedure TContableDetalleComprobante.AntesEliminar;
begin

end;

procedure TContableDetalleComprobante.AntesInsertar;
begin

end;

procedure TContableDetalleComprobante.AntesModificar;
begin

end;

procedure TContableDetalleComprobante.DespuesEliminar;
begin

end;

procedure TContableDetalleComprobante.DespuesInsertar;
begin

end;

procedure TContableDetalleComprobante.DespuesModificar;
begin

end;

constructor TContableDetalleComprobante.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_contable_detalle_comprobante');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(11)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "codigo_cuenta"=$4, "monto_debe"=$5, "monto_haber"=$6, "numero_documento"=$7, "codigo_centro_costo"=$8, "concepto"=$9, "detalle_concepto"=$10 ' +
                  'WHERE ("anno"=$1) AND ("mes"=$2) AND ("id_comprobante=$11) AND ("item"=$3)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("anno"=$1) AND ("mes"=$2) AND ("id_comprobante=$11) AND ("item"=$3)');
  SetLength(ClavesPrimarias,4);
  ClavesPrimarias[0] := '"anno"';
  ClavesPrimarias[1] := '"mes"';
  ClavesPrimarias[2] := '"id_comprobante"';
  ClavesPrimarias[3] := '"item"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TContableDetalleComprobante.SetAnno(varAnno: Integer);
begin
  Anno := varAnno;
end;

procedure TContableDetalleComprobante.SetMes(varMes: Integer);
begin
  Mes := varMes;
end;

procedure TContableDetalleComprobante.SetItem(varItem: Integer);
begin
  Item := varItem;
end;

procedure TContableDetalleComprobante.SetCodigoCuenta(varCodigoCuenta: string);
begin
  CodigoCuenta := varCodigoCuenta;
end;

procedure TContableDetalleComprobante.SetMontoDebe(varMontoDebe: Double);
begin
  MontoDebe := varMontoDebe;
end;

procedure TContableDetalleComprobante.SetMontoHaber(varMontoHaber: Double);
begin
  MontoHaber := varMontoHaber;
end;

procedure TContableDetalleComprobante.SetNumeroDocumento(varNumeroDocumento: string);
begin
  NumeroDocumento := varNumeroDocumento;
end;

procedure TContableDetalleComprobante.SetCodigoCentroCosto(varCodigoCentroCosto: string);
begin
  CodigoCentroCosto := varCodigoCentroCosto;
end;

procedure TContableDetalleComprobante.SetConcepto(varConcepto: string);
begin
  Concepto := varConcepto;
end;

procedure TContableDetalleComprobante.SetDetalleConcepto(varDetalleConcepto: string);
begin
  DetalleConcepto := varDetalleConcepto;
end;

procedure TContableDetalleComprobante.SetIdComprobante(varIdComprobante: Integer);
begin
  IdComprobante := varIdComprobante;
end;

function TContableDetalleComprobante.GetAnno: Integer;
begin
  Result := Anno;
end;

function TContableDetalleComprobante.GetMes: Integer;
begin
  Result := Mes;
end;

function TContableDetalleComprobante.GetItem: Integer;
begin
  Result := Item;
end;

function TContableDetalleComprobante.GetCodigoCuenta: string;
begin
  Result := CodigoCuenta;
end;

function TContableDetalleComprobante.GetMontoDebe: Double;
begin
  Result := MontoDebe;
end;

function TContableDetalleComprobante.GetMontoHaber: Double;
begin
  Result := MontoHaber;
end;

function TContableDetalleComprobante.GetNumeroDocumento: string;
begin
  Result := NumeroDocumento;
end;

function TContableDetalleComprobante.GetCodigoCentroCosto: string;
begin
  Result := CodigoCentroCosto;
end;

function TContableDetalleComprobante.GetConcepto: string;
begin
  Result := Concepto;
end;

function TContableDetalleComprobante.GetDetalleConcepto: string;
begin
  Result := DetalleConcepto;
end;

function TContableDetalleComprobante.GetIdComprobante: Integer;
begin
  Result := IdComprobante;
end;

end. 

