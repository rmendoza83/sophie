unit ClsBusquedaConteoInventario;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TBusquedaConteoInventario }

  TBusquedaConteoInventario = class(TGenericoBD)
    private
      Numero: string;
      CodigoZona: string;
      DescripcionZona: string;
      CodigoEstacion: string;
      DescripcionEstacion: string;
      Observaciones: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      function GetNumero: string;
      function GetCodigoZona: string;
      function GetDescripcionZona: string;
      function GetCodigoEstacion: string;
      function GetDescripcionEstacion: string;
      function GetObservaciones: string;
  end;

implementation

{ TBusquedaConteoInventario }

procedure TBusquedaConteoInventario.Ajustar;
begin

end;

procedure TBusquedaConteoInventario.Mover(Ds: TClientDataSet);
begin
  Numero := Ds.FieldValues['numero'];
  CodigoZona := Ds.FieldValues['codigo_zona'];
  DescripcionZona := Ds.FieldValues['descripcion_zona'];
  CodigoEstacion := Ds.FieldValues['codigo_estacion'];
  DescripcionEstacion := Ds.FieldValues['descripcion_estacion'];
  Observaciones := Ds.FieldValues['observaciones'];
end;

function TBusquedaConteoInventario.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,6);
  Params[0] := Numero;
  Params[1] := CodigoZona;
  Params[2] := DescripcionZona;
  Params[3] := CodigoEstacion;
  Params[4] := DescripcionEstacion;
  Params[5] := Observaciones;
  Result := Params;
end;

function TBusquedaConteoInventario.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,6);
  Campos[0] := 'numero';
  Campos[1] := 'codigo_zona';
  Campos[2] := 'descripcion_zona';
  Campos[3] := 'codigo_estacion';
  Campos[4] := 'descripcion_estacion';
  Campos[5] := 'observaciones';
  Result := Campos;
end;

function TBusquedaConteoInventario.GetCondicion: string;
begin
  Result:= '';
end;

procedure TBusquedaConteoInventario.AntesInsertar;
begin

end;

procedure TBusquedaConteoInventario.AntesModificar;
begin

end;

procedure TBusquedaConteoInventario.AntesEliminar;
begin

end;

procedure TBusquedaConteoInventario.DespuesInsertar;
begin

end;

procedure TBusquedaConteoInventario.DespuesModificar;
begin

end;

procedure TBusquedaConteoInventario.DespuesEliminar;
begin

end;

constructor TBusquedaConteoInventario.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('v_busqueda_conteos_inventario');
  SetLength(ClavesPrimarias,0);
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

function TBusquedaConteoInventario.GetNumero: string;
begin

end;

function TBusquedaConteoInventario.GetCodigoZona: string;
begin

end;

function TBusquedaConteoInventario.GetDescripcionZona: string;
begin

end;

function TBusquedaConteoInventario.GetCodigoEstacion: string;
begin

end;

function TBusquedaConteoInventario.GetDescripcionEstacion: string;
begin

end;

function TBusquedaConteoInventario.GetObservaciones: string;
begin

end;

end.
