unit ClsServicioTecnicoTecnico;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TServicioTecnicoTecnico }

  TServicioTecnicoTecnico = class(TGenericoBD)
    private
      Codigo: String; 
      PrefijoRif: String;
      Rif: String;
      Nit: String;
      RazonSocial: String;
      DireccionFiscal1: String;
      DireccionFiscal2: String;
      Telefonos: String;
      FechaIngreso: TDate;
      CodigoZona: String;
      PComisionGlobal: Double;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigo(varCodigo: String);
      procedure SetPrefijoRif(varPrefijoRif: String);
      procedure SetRif(varRif: String);
      procedure SetNit(varNit: String);
      procedure SetRazonSocial(varRazonSocial: String);
      procedure SetDireccionFiscal1(varDireccionFiscal1: String);
      procedure SetDireccionFiscal2(varDireccionFiscal2: String);
      procedure SetTelefonos(varTelefonos: String);
      procedure SetFechaIngreso(varFechaIngreso: TDate);
      procedure SetCodigoZona(varCodigoZona: String);
      procedure SetPComisionGlobal(varPComisionGlobal: Double);
      function GetCodigo: String;
      function GetPrefijoRif: String;
      function GetRif: String;
      function GetNit: String;
      function GetRazonSocial: String;
      function GetDireccionFiscal1: String;
      function GetDireccionFiscal2: String;
      function GetTelefonos: String;
      function GetFechaIngreso: TDate;
      function GetCodigoZona: String;
      function GetPComisionGlobal: Double;
      function ObtenerCombo: TClientDataSet;
  end;

implementation

uses
  ClsConsecutivo;
{ TServicioTecnicoTecnico }

procedure TServicioTecnicoTecnico.Ajustar;
begin

end;

procedure TServicioTecnicoTecnico.Mover(Ds: TClientDataSet);
begin
  Codigo := Ds.FieldValues['codigo'];
  PrefijoRif := Ds.FieldValues['prefijo_rif'];
  Rif := Ds.FieldValues['rif'];
  Nit := Ds.FieldValues['nit'];
  RazonSocial := Ds.FieldValues['razon_social'];
  DireccionFiscal1 := Ds.FieldValues['direccion_fiscal_1'];
  DireccionFiscal2 := Ds.FieldValues['direccion_fiscal_2'];
  Telefonos := Ds.FieldValues['telefonos'];
  FechaIngreso := Ds.FieldByName('fecha_ingreso').AsDateTime;
  CodigoZona := Ds.FieldValues['codigo_zona'];
  PComisionGlobal := Ds.FieldValues['p_comision_global'];
end;

function TServicioTecnicoTecnico.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,11);
  Params[0] := Codigo;
  Params[1] := PrefijoRif;
  Params[2] := Rif;
  Params[3] := Nit;
  Params[4] := RazonSocial;
  Params[5] := DireccionFiscal1;
  Params[6] := DireccionFiscal2;
  Params[7] := Telefonos;
  Params[8] := VarFromDateTime(FechaIngreso);
  Params[9] := CodigoZona;
  Params[10] := PComisionGlobal;
  Result := Params;
end;

function TServicioTecnicoTecnico.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,16);
  Campos[0] := 'codigo';
  Campos[1] := 'rif';
  Campos[2] := 'nit';
  Campos[3] := 'razon_social';
  Campos[4] := 'direccion_fiscal_1';
  Campos[5] := 'direccion_fiscal_2';
  Campos[6] := 'direccion_domicilio_1';
  Campos[7] := 'direccion_domicilio_2';
  Campos[8] := 'telefonos';
  Campos[9] := 'fax';
  Campos[10] := 'email';
  Campos[11] := 'website';
  Campos[12] := 'contacto';
  Campos[13] := 'codigo_vendedor';
  Campos[14] := 'codigo_esquema_pago';
  Campos[15] := 'codigo_zona';
  Result := Campos;
end;

function TServicioTecnicoTecnico.GetCondicion: string;
begin
  Result := '"codigo" = ' + QuotedStr(Codigo);
end;

procedure TServicioTecnicoTecnico.AntesInsertar;
begin

end;

procedure TServicioTecnicoTecnico.AntesModificar;
begin

end;

procedure TServicioTecnicoTecnico.AntesEliminar;
begin

end;

procedure TServicioTecnicoTecnico.DespuesInsertar;
begin

end;

procedure TServicioTecnicoTecnico.DespuesModificar;
begin

end;

procedure TServicioTecnicoTecnico.DespuesEliminar;
begin

end;

constructor TServicioTecnicoTecnico.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_servicio_tecnico_tecnicos');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(11)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "prefijo_rif"=$2, "rif"=$3, "nit"=$4, "razon_social"=$5, "direccion_fiscal_1"=$6, "direccion_fiscal_2"=$7, "telefonos"=$8, "fecha_ingreso"=$9, "codigo_zona"=$10, "p_comision_global"=$11 WHERE ("codigo" = $1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("codigo" = $1)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"codigo"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TServicioTecnicoTecnico.SetCodigo(varCodigo: String);
begin
  Codigo := varCodigo;
end;

procedure TServicioTecnicoTecnico.SetPrefijoRif(varPrefijoRif: String);
begin
  PrefijoRif := varPrefijoRif;
end;

procedure TServicioTecnicoTecnico.SetRif(varRif: String);
begin
  Rif := varRif;
end;

procedure TServicioTecnicoTecnico.SetNit(varNit: String);
begin
  Nit := varNit;
end;

procedure TServicioTecnicoTecnico.SetRazonSocial(varRazonSocial: String);
begin
  RazonSocial := varRazonSocial;
end;

procedure TServicioTecnicoTecnico.SetDireccionFiscal1(varDireccionFiscal1: String);
begin
  DireccionFiscal1 := varDireccionFiscal1;
end;

procedure TServicioTecnicoTecnico.SetDireccionFiscal2(varDireccionFiscal2: String);
begin
  DireccionFiscal2 := varDireccionFiscal2;
end;

procedure TServicioTecnicoTecnico.SetTelefonos(varTelefonos: String);
begin
  Telefonos := varTelefonos;
end;

procedure TServicioTecnicoTecnico.SetFechaIngreso(varFechaIngreso: TDate);
begin
  FechaIngreso := varFechaIngreso;
end;

procedure TServicioTecnicoTecnico.SetCodigoZona(varCodigoZona: String);
begin
  CodigoZona := varCodigoZona;
end;

procedure TServicioTecnicoTecnico.SetPComisionGlobal(varPComisionGlobal: Double);
begin
  PComisionGlobal := varPComisionGlobal;
end;

function TServicioTecnicoTecnico.GetCodigo: String;
begin
  Result := Codigo;
end;

function TServicioTecnicoTecnico.GetPrefijoRif: String;
begin
  Result := PrefijoRif;
end;

function TServicioTecnicoTecnico.GetRif: String;
begin
  Result := Rif;
end;

function TServicioTecnicoTecnico.GetNit: String;
begin
  Result := Nit;
end;

function TServicioTecnicoTecnico.GetRazonSocial: String;
begin
  Result := RazonSocial;
end;

function TServicioTecnicoTecnico.GetDireccionFiscal1: String;
begin
  Result := DireccionFiscal1;
end;

function TServicioTecnicoTecnico.GetDireccionFiscal2: String;
begin
  Result := DireccionFiscal2;
end;

function TServicioTecnicoTecnico.GetTelefonos: String;
begin
  Result := Telefonos;
end;

function TServicioTecnicoTecnico.GetFechaIngreso: TDate;
begin
  Result := FechaIngreso;
end;

function TServicioTecnicoTecnico.GetCodigoZona: String;
begin
  Result := CodigoZona;
end;

function TServicioTecnicoTecnico.GetPComisionGlobal: Double;
begin
  Result := PComisionGlobal;
end;

function TServicioTecnicoTecnico.ObtenerCombo: TClientDataSet;
begin
  Result := BD.EjecutarQueryCliDs('SELECT "codigo", "razon_social" FROM ' + GetTabla + ' ORDER BY "codigo"');
end;

end.

