unit ClsMoneda;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TMoneda }

  TMoneda = class(TGenericoBD)
    private
      Codigo: string;
      Descripcion: string;
      TasaDolar: Double;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigo(varCodigo: string);
      procedure SetDescripcion(varDescripcion: string);
      procedure SetTasaDolar(varTasaDolar: Double);
      function GetCodigo: string;
      function GetDescripcion: string;
      function GetTasaDolar: Double;
      function ObtenerCombo: TClientDataSet;
  end;

implementation

{ TMoneda }

procedure TMoneda.Ajustar;
begin

end;

procedure TMoneda.Mover(Ds: TClientDataSet);
begin
  Codigo := Ds.FieldValues['codigo'];
  Descripcion := Ds.FieldValues['descripcion'];
  TasaDolar := Ds.FieldValues['tasa_dolar'];
end;

function TMoneda.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,3);
  Params[0] := Codigo;
  Params[1] := Descripcion;
  Params[2] := TasaDolar;
  Result := Params;
end;

function TMoneda.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,2);
  Campos[0] := 'codigo';
  Campos[1] := 'descripcion';
  Result := Campos;
end;

function TMoneda.GetCondicion: string;
begin
  Result := '"codigo" = ' + QuotedStr(Codigo);
end;

procedure TMoneda.AntesInsertar;
begin

end;

procedure TMoneda.AntesModificar;
begin

end;

procedure TMoneda.AntesEliminar;
begin

end;

procedure TMoneda.DespuesInsertar;
begin

end;

procedure TMoneda.DespuesModificar;
begin

end;

procedure TMoneda.DespuesEliminar;
begin

end;

constructor TMoneda.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_monedas');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(3)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "descripcion"=$2,"tasa_dolar"=$3 WHERE ("codigo"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("codigo"=$1)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"codigo"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TMoneda.SetCodigo(varCodigo: string);
begin
  Codigo := varCodigo;
end;

procedure TMoneda.SetDescripcion(varDescripcion: string);
begin
  Descripcion := varDescripcion;
end;

procedure TMoneda.SetTasaDolar(varTasaDolar: Double);
begin
  TasaDolar:= varTasaDolar;
end;

function TMoneda.GetCodigo: string;
begin
  Result := Codigo;
end;

function TMoneda.GetDescripcion: string;
begin
  Result := Descripcion;
end;

function TMoneda.GetTasaDolar: Double;
begin
  Result := TasaDolar;
end;

function TMoneda.ObtenerCombo: TClientDataSet;
begin
  Result := BD.EjecutarQueryCliDs('SELECT "codigo", "descripcion" FROM ' + GetTabla);
end;

end.

