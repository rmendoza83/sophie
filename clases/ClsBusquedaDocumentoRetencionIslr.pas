unit ClsBusquedaDocumentoRetencionIslr;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TBusquedaDocumentoRetencionIslr }

  TBusquedaDocumentoRetencionIslr = class(TGenericoBD)
    private
      Desde: string;
      Numero: string;
      CodigoProveedor: string;
      Rif: string;
      Nit: string;
      RazonSocial: string;
      Contacto: string;
      MontoNeto: Double;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create; overload;
      procedure SetDesde(varDesde: string);
      procedure SetNumero(varNumero: string);
      procedure SetCodigoProveedor(varCodigoProveedor: string);
      function GetDesde: string;
      function GetNumero: string;
      function GetCodigoProveedor: string;
      function GetRif: string;
      function GetNit: string;
      function GetRazonSocial: string;
      function GetContacto: string;
      function GetMontoNeto: Double;
  end;

implementation

{ TBusquedaDocumentoRetencionIslr }

procedure TBusquedaDocumentoRetencionIslr.Ajustar;
begin

end;

procedure TBusquedaDocumentoRetencionIslr.Mover(Ds: TClientDataSet);
begin
  Desde := Ds.FieldValues['desde'];
  Numero := Ds.FieldValues['numero'];
  CodigoProveedor := Ds.FieldValues['codigo_proveedor'];
  Rif := Ds.FieldValues['rif'];
  Nit := Ds.FieldValues['nit'];
  RazonSocial := Ds.FieldValues['razon_social'];
  Contacto := Ds.FieldValues['contacto'];
  MontoNeto := Ds.FieldValues['monto_neto'];
end;

function TBusquedaDocumentoRetencionIslr.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,8);
  Params[0] := Desde;
  Params[1] := Numero;
  Params[2] := CodigoProveedor;
  Params[3] := Rif;
  Params[4] := Nit;
  Params[5] := RazonSocial;
  Params[6] := Contacto;
  Params[7] := MontoNeto;
  Result := Params;
end;

function TBusquedaDocumentoRetencionIslr.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,7);
  Campos[0] := 'desde';
  Campos[1] := 'numero';
  Campos[2] := 'codigo_proveedor';
  Campos[3] := 'rif';
  Campos[4] := 'nit';
  Campos[5] := 'razon_social';
  Campos[6] := 'contacto';
  Result := Campos;
end;

function TBusquedaDocumentoRetencionIslr.GetCondicion: string;
begin
  Result:= '("desde" = ' + QuotedStr(Desde) + ') AND ("numero" = ' + QuotedStr(Numero) + ') AND ("codigo_proveedor" = ' + QuotedStr(CodigoProveedor) + ')';
end;

procedure TBusquedaDocumentoRetencionIslr.AntesInsertar;
begin

end;

procedure TBusquedaDocumentoRetencionIslr.AntesModificar;
begin

end;

procedure TBusquedaDocumentoRetencionIslr.AntesEliminar;
begin

end;

procedure TBusquedaDocumentoRetencionIslr.DespuesInsertar;
begin

end;

procedure TBusquedaDocumentoRetencionIslr.DespuesModificar;
begin

end;

procedure TBusquedaDocumentoRetencionIslr.DespuesEliminar;
begin

end;

constructor TBusquedaDocumentoRetencionIslr.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('v_busqueda_documentos_retencion_islr');
  SetLength(ClavesPrimarias,0);
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TBusquedaDocumentoRetencionIslr.SetCodigoProveedor(
  varCodigoProveedor: string);
begin
  CodigoProveedor := varCodigoProveedor;
end;

procedure TBusquedaDocumentoRetencionIslr.SetDesde(varDesde: string);
begin
  Desde := varDesde;
end;

procedure TBusquedaDocumentoRetencionIslr.SetNumero(varNumero: string);
begin
  Numero := varNumero;
end;

function TBusquedaDocumentoRetencionIslr.GetCodigoProveedor: string;
begin
  Result := CodigoProveedor;
end;

function TBusquedaDocumentoRetencionIslr.GetContacto: string;
begin
  Result := Contacto;
end;

function TBusquedaDocumentoRetencionIslr.GetDesde: string;
begin
  Result := Desde;
end;

function TBusquedaDocumentoRetencionIslr.GetMontoNeto: Double;
begin
  Result := MontoNeto;
end;

function TBusquedaDocumentoRetencionIslr.GetNit: string;
begin
  Result := Nit;
end;

function TBusquedaDocumentoRetencionIslr.GetNumero: string;
begin
  Result := Numero;
end;

function TBusquedaDocumentoRetencionIslr.GetRazonSocial: String;
begin
  Result := RazonSocial;
end;

function TBusquedaDocumentoRetencionIslr.GetRif: String;
begin
  Result := Rif;
end;

end.
