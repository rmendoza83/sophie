unit ClsServicioTecnicoDetPagoComision;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TServicioTecnicoDetPagoComision }

  TServicioTecnicoDetPagoComision = class(TGenericoBD)
    private
      Codigo: string;
      Descripcion: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      function ObtenerCombo: TClientDataSet;
      procedure SetCodigo(varCodigo: string);
      procedure SetDescripcion(varDescripcion: string);
      function GetCodigo: string;
      function GetDescripcion: string;
  end;

implementation

{ TServicioTecnicoDetPagoComision }

procedure TServicioTecnicoDetPagoComision.Ajustar;
begin

end;

procedure TServicioTecnicoDetPagoComision.Mover(Ds: TClientDataSet);
begin
  Codigo := Ds.FieldValues['codigo'];
  Descripcion := Ds.FieldValues['descripcion'];
end;

function TServicioTecnicoDetPagoComision.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,2);
  Params[0] := Codigo;
  Params[1] := Descripcion;
  Result := Params;
end;

function TServicioTecnicoDetPagoComision.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,2);
  Campos[0] := 'codigo';
  Campos[1] := 'descripcion';
  Result := Campos;
end;

function TServicioTecnicoDetPagoComision.GetCondicion: string;
begin
  Result := '"codigo" = ' + QuotedStr(Codigo);
end;

procedure TServicioTecnicoDetPagoComision.AntesInsertar;
begin

end;

procedure TServicioTecnicoDetPagoComision.AntesModificar;
begin

end;

procedure TServicioTecnicoDetPagoComision.AntesEliminar;
begin

end;

procedure TServicioTecnicoDetPagoComision.DespuesInsertar;
begin

end;

procedure TServicioTecnicoDetPagoComision.DespuesModificar;
begin

end;

procedure TServicioTecnicoDetPagoComision.DespuesEliminar;
begin

end;

constructor TServicioTecnicoDetPagoComision.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_servicio_tecnico_tipos_servicio');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(2)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "descripcion"=$2 WHERE ("codigo"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("codigo"=$1)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"codigo"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

function TServicioTecnicoDetPagoComision.ObtenerCombo: TClientDataSet;
begin
  Result := BD.EjecutarQueryCliDs('SELECT "codigo", "descripcion" FROM ' + GetTabla);
end;

procedure TServicioTecnicoDetPagoComision.SetCodigo(varCodigo: string);
begin
  Codigo := varCodigo;
end;

procedure TServicioTecnicoDetPagoComision.SetDescripcion(varDescripcion: string);
begin
  Descripcion := varDescripcion;
end;

function TServicioTecnicoDetPagoComision.GetCodigo: string;
begin
  Result := Codigo;
end;

function TServicioTecnicoDetPagoComision.GetDescripcion: string;
begin
  Result := Descripcion;
end;

end.
