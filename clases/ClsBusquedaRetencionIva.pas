unit ClsBusquedaRetencionIva;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TBusquedaRetencionIva }

  TBusquedaRetencionIva = class(TGenericoBD)
    private
      Numero: string;
      CodigoProveedor: string;
      Rif: string;
      Nit: string;
      RazonSocial: string;
      Contacto: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      function GetNumero: string;
      function GetCodigoProveedor: string;
      function GetRif: string;
      function GetNit: string;
      function GetRazonSocial: string;
      function GetContacto: string;
  end;

implementation

{ TBusquedaRetencionIva }

procedure TBusquedaRetencionIva.Ajustar;
begin

end;

procedure TBusquedaRetencionIva.Mover(Ds: TClientDataSet);
begin
  Numero := Ds.FieldValues['numero'];
  CodigoProveedor := Ds.FieldValues['codigo_proveedor'];
  Rif := Ds.FieldValues['rif'];
  Nit := Ds.FieldValues['nit'];
  RazonSocial := Ds.FieldValues['razon_social'];
  Contacto := Ds.FieldValues['contacto'];
end;

function TBusquedaRetencionIva.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,6);
  Params[0] := Numero;
  Params[1] := CodigoProveedor;
  Params[2] := Rif;
  Params[3] := Nit;
  Params[4] := RazonSocial;
  Params[5] := Contacto;
  Result := Params;
end;

function TBusquedaRetencionIva.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,6);
  Campos[0] := 'numero';
  Campos[1] := 'codigo_proveedor';
  Campos[2] := 'rif';
  Campos[3] := 'nit';
  Campos[4] := 'razon_social';
  Campos[5] := 'contacto';
  Result := Campos;
end;

function TBusquedaRetencionIva.GetCondicion: string;
begin
  Result:= '';
end;

procedure TBusquedaRetencionIva.AntesInsertar;
begin

end;

procedure TBusquedaRetencionIva.AntesModificar;
begin

end;

procedure TBusquedaRetencionIva.AntesEliminar;
begin

end;

procedure TBusquedaRetencionIva.DespuesInsertar;
begin

end;

procedure TBusquedaRetencionIva.DespuesModificar;
begin

end;

procedure TBusquedaRetencionIva.DespuesEliminar;
begin

end;

constructor TBusquedaRetencionIva.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('v_busqueda_retencion_iva');
  SetLength(ClavesPrimarias,0);
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

function TBusquedaRetencionIva.GetCodigoProveedor: string;
begin
  Result := CodigoProveedor;
end;

function TBusquedaRetencionIva.GetContacto: string;
begin
  Result := Contacto;
end;

function TBusquedaRetencionIva.GetNit: string;
begin
  Result := Nit;
end;

function TBusquedaRetencionIva.GetNumero: string;
begin
  Result := Numero;
end;

function TBusquedaRetencionIva.GetRazonSocial: String;
begin
  Result := RazonSocial;
end;

function TBusquedaRetencionIva.GetRif: String;
begin
  Result := Rif;
end;

end.
