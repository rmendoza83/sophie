unit ClsDetFacturacionCliente;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TDetFacturacionCliente }

  TDetFacturacionCliente = class(TGenericoBD)
    private
      CodigoProducto: string;
      Referencia: string;
      Descripcion: string;
      Cantidad: Double;
      Precio: Double;
      PDescuento: Double;
      MontoDescuento: Double;
      Impuesto: Double;
      PIva: Double;
      Total: Double;
      IdFacturacionCliente: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigoProducto(varCodigoProducto: string);
      procedure SetReferencia(varReferencia: string);
      procedure SetDescripcion(varDescripcion: string);
      procedure SetCantidad(varCantidad: Double);
      procedure SetPrecio(varPrecio: Double);
      procedure SetPDescuento(varPDescuento: Double);
      procedure SetMontoDescuento(varMontoDescuento: Double);
      procedure SetImpuesto(varImpuesto: Double);
      procedure SetPIva(varPIva: Double);
      procedure SetTotal(varTotal: Double);
      procedure SetIdFacturacionCliente(varIdFacturacionCliente: Integer);
      function GetCodigoProducto: string;
      function GetReferencia: string;
      function GetDescripcion: string;
      function GetCantidad: Double;
      function GetPrecio: Double;
      function GetPDescuento: Double;
      function GetMontoDescuento: Double;
      function GetImpuesto: Double;
      function GetPIva: Double;
      function GetTotal: Double;
      function GetIdFacturacionCliente: Integer;
      function ObtenerIdFacturacionCliente(varIdFacturacionCliente: Integer): TClientDataSet;
      function EliminarIdFacturacionCliente(varIdFacturacionCliente: Integer): Boolean;
  end;

implementation

uses DB;

{ TDetFacturacionCliente }

procedure TDetFacturacionCliente.Ajustar;
begin
  inherited;

end;

procedure TDetFacturacionCliente.Mover(Ds: TClientDataSet);
begin
  CodigoProducto := Ds.FieldValues['codigo_producto'];
  Referencia := Ds.FieldValues['referencia'];
  Descripcion := Ds.FieldValues['descripcion'];
  Cantidad := Ds.FieldValues['cantidad'];
  Precio := Ds.FieldValues['precio'];
  PDescuento := Ds.FieldValues['p_descuento'];
  MontoDescuento := Ds.FieldValues['monto_descuento'];
  Impuesto := Ds.FieldValues['impuesto'];
  PIva := Ds.FieldValues['p_iva'];
  Total := Ds.FieldValues['total'];
  IdFacturacionCliente := Ds.FieldValues['id_facturacion_cliente'];
end;

function TDetFacturacionCliente.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,11);
  Params[0] := CodigoProducto;
  Params[1] := Referencia;
  Params[2] := Descripcion;
  Params[3] := Cantidad;
  Params[4] := Precio;
  Params[5] := PDescuento;
  Params[6] := MontoDescuento;
  Params[7] := Impuesto;
  Params[8] := PIva;
  Params[9] := Total;
  Params[10] := IdFacturacionCliente;
  Result:= Params;
end;

function TDetFacturacionCliente.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,3);
  Campos[0] := 'codigo_producto';
  Campos[1] := 'referencia';
  Campos[2] := 'descripcion';
  Result:= Campos;
end;

function TDetFacturacionCliente.GetCondicion: string;
begin
  Result:= '"id_facturacion_cliente" =' + IntToStr(IdFacturacionCliente) + ' AND "codigo_producto" =' + QuotedStr(CodigoProducto)
end;

procedure TDetFacturacionCliente.AntesEliminar;
begin

end;

procedure TDetFacturacionCliente.AntesInsertar;
begin

end;

procedure TDetFacturacionCliente.AntesModificar;
begin

end;

procedure TDetFacturacionCliente.DespuesEliminar;
begin

end;

procedure TDetFacturacionCliente.DespuesInsertar;
begin

end;

procedure TDetFacturacionCliente.DespuesModificar;
begin

end;

constructor TDetFacturacionCliente.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_det_facturacion_clientes');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(11)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "referencia"=$2,"descripcion"=$3,"cantidad"=$4,"precio"=$5,"p_descuento"=$6,"monto_descuento"=$7,"impuesto"=$8,"p_iva"=$9,"total"=$10 WHERE ("id_facturacion_cliente"=$11) AND ("codigo_producto"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("id_facturacion_cliente"=$11) AND ("codigo_producto"=$1)');
  SetLength(ClavesPrimarias,2);
  ClavesPrimarias[0] := '"id_facturacion_cliente"';
  ClavesPrimarias[1] := '"codigo_producto"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TDetFacturacionCliente.SetCantidad(varCantidad: Double);
begin
  Cantidad := varCantidad;
end;

procedure TDetFacturacionCliente.SetCodigoProducto(varCodigoProducto: string);
begin
  CodigoProducto := varCodigoProducto;
end;

procedure TDetFacturacionCliente.SetDescripcion(varDescripcion: string);
begin
  Descripcion := varDescripcion;
end;

procedure TDetFacturacionCliente.SetIdFacturacionCliente(varIdFacturacionCliente: Integer);
begin
  IdFacturacionCliente := varIdFacturacionCliente;
end;

procedure TDetFacturacionCliente.SetImpuesto(varImpuesto: Double);
begin
  Impuesto := varImpuesto;
end;

procedure TDetFacturacionCliente.SetMontoDescuento(varMontoDescuento: Double);
begin
  MontoDescuento := varMontoDescuento;
end;

procedure TDetFacturacionCliente.SetPDescuento(varPDescuento: Double);
begin
  PDescuento := varPDescuento;
end;

procedure TDetFacturacionCliente.SetPIva(varPIva: Double);
begin
  PIva := varPIva;
end;

procedure TDetFacturacionCliente.SetPrecio(varPrecio: Double);
begin
  Precio := varPrecio
end;

procedure TDetFacturacionCliente.SetReferencia(varReferencia: string);
begin
  Referencia := varReferencia;
end;

procedure TDetFacturacionCliente.SetTotal(varTotal: Double);
begin
  Total := varTotal;
end;

function TDetFacturacionCliente.GetCantidad: Double;
begin
  Result := Cantidad;
end;

function TDetFacturacionCliente.GetCodigoProducto: string;
begin
  Result := CodigoProducto;
end;

function TDetFacturacionCliente.GetDescripcion: string;
begin
  Result := Descripcion;
end;

function TDetFacturacionCliente.GetIdFacturacionCliente: Integer;
begin
  Result := IdFacturacionCliente;
end;

function TDetFacturacionCliente.GetImpuesto: Double;
begin
  Result := Impuesto;
end;

function TDetFacturacionCliente.GetMontoDescuento: Double;
begin
  Result := MontoDescuento;
end;

function TDetFacturacionCliente.GetPDescuento: Double;
begin
  Result := PDescuento;
end;

function TDetFacturacionCliente.GetPIva: Double;
begin
  Result := PIva;
end;

function TDetFacturacionCliente.GetPrecio: Double;
begin
  Result := Precio;
end;

function TDetFacturacionCliente.GetReferencia: string;
begin
  Result := Referencia;
end;

function TDetFacturacionCliente.GetTotal: Double;
begin
  Result := Total;
end;

function TDetFacturacionCliente.ObtenerIdFacturacionCliente(
  varIdFacturacionCliente: Integer): TClientDataSet;
begin
  Result := ObtenerListaCondicionSQL(' WHERE "id_facturacion_cliente" = ' + IntToStr(varIdFacturacionCliente));
end;

function TDetFacturacionCliente.EliminarIdFacturacionCliente(
  varIdFacturacionCliente: Integer): Boolean;
begin
  Result := EjecutarSQL('DELETE FROM ' + GetTabla + ' WHERE "id_facturacion_cliente" = ' + IntToStr(varIdFacturacionCliente));
end;

end.
