unit ClsCondominioConjuntoAdministracion;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TCondominioConjuntoAdministracion }

  TCondominioConjuntoAdministracion = class(TGenericoBD)
    private
      Codigo: string;
      Descripcion: string;
      DireccionUbicacion: string;
      PersonasContacto: string;
      Observaciones: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigo(varCodigo: string);
      procedure SetDescripcion(varDescripcion: string);
      procedure SetDireccionUbicacion(varDireccionUbicacion: string);
      procedure SetPersonasContacto(varPersonasContacto: string);
      procedure SetObservaciones(varObservaciones: string);
      function GetCodigo: string;
      function GetDescripcion: string;
      function GetDireccionUbicacion: string;
      function GetPersonasContacto: string;
      function GetObservaciones: string;
  end;

var
  Clase: TCondominioConjuntoAdministracion;

implementation

{ TCondominioConjuntoAdministracion }

procedure TCondominioConjuntoAdministracion.Ajustar;
begin

end;

procedure TCondominioConjuntoAdministracion.Mover(Ds: TClientDataSet);
begin
  Codigo := Ds.FieldValues['codigo'];
  Descripcion := Ds.FieldValues['descripcion'];
  DireccionUbicacion := Ds.FieldValues['direccion_ubicacion'];
  PersonasContacto := Ds.FieldValues['personas_contacto'];
  Observaciones := Ds.FieldValues['observaciones'];
end;

function TCondominioConjuntoAdministracion.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,5);
  Params[0] := Codigo;
  Params[1] := Descripcion;
  Params[2] := DireccionUbicacion;
  Params[3] := PersonasContacto;
  Params[4] := Observaciones;
  Result := Params;
end;

function TCondominioConjuntoAdministracion.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,5);
  Campos[0] := 'codigo';
  Campos[1] := 'descripcion';
  Campos[2] := 'direccion_ubicacion';
  Campos[3] := 'personas_contacto';
  Campos[4] := 'observaciones';
  Result := Campos;
end;

function TCondominioConjuntoAdministracion.GetCondicion: string;
begin
  Result := '"codigo" = ' + QuotedStr(Codigo);
end;

procedure TCondominioConjuntoAdministracion.AntesEliminar;
begin

end;

procedure TCondominioConjuntoAdministracion.AntesInsertar;
begin

end;

procedure TCondominioConjuntoAdministracion.AntesModificar;
begin

end;

procedure TCondominioConjuntoAdministracion.DespuesEliminar;
begin

end;

procedure TCondominioConjuntoAdministracion.DespuesInsertar;
begin

end;

procedure TCondominioConjuntoAdministracion.DespuesModificar;
begin

end;

constructor TCondominioConjuntoAdministracion.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_condominio_conjuntos_administracion');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(5)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "descripcion"=$2, "direccion_ubicacion"=$3, "personas_contacto"=$4, "observaciones"=$5 WHERE ("codigo"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("codigo"=$1)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"codigo"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TCondominioConjuntoAdministracion.SetCodigo(varCodigo: string);
begin
  Codigo := varCodigo;
end;

procedure TCondominioConjuntoAdministracion.SetDescripcion(varDescripcion: string);
begin
  Descripcion := varDescripcion;
end;

procedure TCondominioConjuntoAdministracion.SetDireccionUbicacion(varDireccionUbicacion: string);
begin
  DireccionUbicacion := varDireccionUbicacion;
end;

procedure TCondominioConjuntoAdministracion.SetPersonasContacto(varPersonasContacto: string);
begin
  PersonasContacto := varPersonasContacto;
end;

procedure TCondominioConjuntoAdministracion.SetObservaciones(varObservaciones: string);
begin
  Observaciones := varObservaciones;
end;

function TCondominioConjuntoAdministracion.GetCodigo: string;
begin
  Result := Codigo;
end;

function TCondominioConjuntoAdministracion.GetDescripcion: string;
begin
  Result := Descripcion;
end;

function TCondominioConjuntoAdministracion.GetDireccionUbicacion: string;
begin
  Result := DireccionUbicacion;
end;

function TCondominioConjuntoAdministracion.GetPersonasContacto: string;
begin
  Result := PersonasContacto;
end;

function TCondominioConjuntoAdministracion.GetObservaciones: string;
begin
  Result := Observaciones;
end;

end.
