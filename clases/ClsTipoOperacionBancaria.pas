unit ClsTipoOperacionBancaria;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TTipoOperacionBancaria }

  TTipoOperacionBancaria = class(TGenericoBD)
    private
      Codigo: string;
      Descripcion: string;
      Egreso: Boolean;
      EmiteCheque: Boolean;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigo(varCodigo: string);
      procedure SetDescripcion(varDescripcion: string);
      procedure SetEgreso(VarEgreso: Boolean);
      procedure SetEmiteCheque(VarEmiteCheque: Boolean);
      function GetCodigo: string;
      function GetDescripcion: string;
      function GetEgreso: Boolean;
      function GetEmiteCheque: Boolean;
      function ObtenerCombo: TClientDataSet;
  end;

implementation

{ TTipoOperacionBancaria }

procedure TTipoOperacionBancaria.Ajustar;
begin

end;

procedure TTipoOperacionBancaria.Mover(Ds: TClientDataSet);
begin
  Codigo := Ds.FieldValues['codigo'];
  Descripcion := Ds.FieldValues['descripcion'];
  Egreso := Ds.FieldValues['egreso'];
  EmiteCheque := ds.FieldValues['emite_cheque'];
end;

function TTipoOperacionBancaria.GetParametros: TArrayVariant;
var
 Params : TArrayVariant;
begin
  SetLength(Params,4);
  Params[0] := Codigo;
  Params[1] := Descripcion;
  Params[2] := Egreso;
  Params[3] := EmiteCheque;
  Result := Params;
end;

function TTipoOperacionBancaria.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,2);
  Campos[0] := 'codigo';
  Campos[1] := 'descripcion';
  Result := Campos;
end;

function TTipoOperacionBancaria.GetCondicion: string;
begin
  Result := '"codigo" = ' + QuotedStr(Codigo);
end;

procedure TTipoOperacionBancaria.AntesInsertar;
begin

end;

procedure TTipoOperacionBancaria.AntesModificar;
begin

end;

procedure TTipoOperacionBancaria.AntesEliminar;
begin

end;

procedure TTipoOperacionBancaria.DespuesInsertar;
begin

end;

procedure TTipoOperacionBancaria.DespuesModificar;
begin

end;

procedure TTipoOperacionBancaria.DespuesEliminar;
begin

end;

constructor TTipoOperacionBancaria.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_tipos_operaciones_bancarias');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(4)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "descripcion"=$2,"egreso"=$3, "emite_cheque"=$4 WHERE ("codigo"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("codigo"=$1)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"codigo"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TTipoOperacionBancaria.SetCodigo(varCodigo: string);
begin
  Codigo := varCodigo;
end;

procedure TTipoOperacionBancaria.SetDescripcion(varDescripcion: string);
begin
  Descripcion := varDescripcion;
end;

procedure TTipoOperacionBancaria.SetEgreso(VarEgreso: Boolean);
begin
  Egreso := VarEgreso;
end;

procedure TTipoOperacionBancaria.SetEmiteCheque(VarEmiteCheque: Boolean);
begin
  EmiteCheque := VarEmiteCheque;
end;

function TTipoOperacionBancaria.GetCodigo: string;
begin
  Result := Codigo;
end;

function TTipoOperacionBancaria.GetDescripcion: string;
begin
  Result := Descripcion;
end;

function TTipoOperacionBancaria.GetEgreso: Boolean;
begin
  Result := Egreso;
end;

function TTipoOperacionBancaria.GetEmiteCheque: Boolean;
begin
  Result := EmiteCheque;
end;

function TTipoOperacionBancaria.ObtenerCombo: TClientDataSet;
begin
  Result := BD.EjecutarQueryCliDs('SELECT "codigo", "descripcion" FROM ' + GetTabla);
end;

end.
