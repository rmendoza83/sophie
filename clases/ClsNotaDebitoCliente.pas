unit ClsNotaDebitoCliente;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TNotaDebitoCliente }

  TNotaDebitoCliente = class(TGenericoBD)
    private
      Numero: string;
      CodigoMoneda: string;
      CodigoCliente: string;
      CodigoVendedor: string;
      CodigoZona: string;
      CodigoEsquemaPago: string;
      FechaIngreso: TDate;
      FechaRecepcion: TDate;
      FechaContable: TDate;
      FechaLibros: TDate;
      Concepto: string;
      Observaciones: string;
      MontoNeto: Double;
      MontoDescuento: Double;
      SubTotal: Double;
      Impuesto: Double;
      Total: Double;
      PDescuento: Double;
      PIva: Double;
      TasaDolar: Double;
      LoginUsuario: string;
      TiempoIngreso: TDateTime;
      IdNotaDebitoCliente: Integer;
      IdCobranza: Integer;
      IdContadoCobranza: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArraystring; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetNumero(varNumero: string);
      procedure SetCodigoMoneda(varCodigoMoneda: string);
      procedure SetCodigoCliente(varCodigoCliente: string);
      procedure SetCodigoVendedor(varCodigoVendedor: string);
      procedure SetCodigoZona(varCodigoZona: string);
      procedure SetCodigoEsquemaPago(varCodigoEsquemaPago: string);
      procedure SetFechaIngreso(varFechaIngreso: TDate);
      procedure SetFechaRecepcion(varFechaRecepcion: TDate);
      procedure SetFechaContable(varFechaContable: TDate);
      procedure SetFechaLibros(varFechalibros: TDate);
      procedure SetConcepto(varConcepto: string);
      procedure SetObservaciones(varObservaciones: string);
      procedure SetMontoNeto(varMontoNeto: Double);
      procedure SetMontoDescuento(varMontoDescuento: Double);
      procedure SetSubTotal(varSubTotal: Double);
      procedure SetImpuesto(varImpuesto: Double);
      procedure SetTotal(varTotal: Double);
      procedure SetPDescuento(varPDescuento: Double);
      procedure SetPIva(varPIva: Double);
      procedure SetTasaDolar(varTasaDolar: Double);
      procedure SetLoginUsuario(varLoginUsuario: string);
      procedure SetTiempoIngreso(varTiempodeIngreso: TDateTime);
      procedure SetIdNotaDebitoCliente(varIdNotaDebitoCliente: Integer);
      procedure SetIdCobranza(varIdCobranza: Integer);
      procedure SetIdContadoCobranza(varIdContadoCobranza: Integer);
      function GetNumero: string;
      function GetCodigoMoneda: string;
      function GetCodigoCliente: string;
      function GetCodigoVendedor: string;
      function GetCodigoZona: string;
      function GetCodigoEsquemaPago: string;
      function GetFechaIngreso: TDate;
      function GetFechaRecepcion: TDate;
      function GetFechaContable: TDate;
      function GetFechaLibros: TDate;
      function GetConcepto: string;
      function GetObservaciones: string;
      function GetMontoNeto: Double;
      function GetMontoDescuento: Double;
      function GetSubTotal: Double;
      function GetImpuesto: Double;
      function GetTotal: Double;
      function GetPDescuento: Double;
      function GetPIva: Double;
      function GetTasaDolar: Double;
      function GetLoginUsuario: string;
      function GetTiempoIngreso: TDateTime;
      function GetIdNotaDebitoCliente: Integer;
      function GetIdCobranza: Integer;
      function GetIdContadoCobranza: Integer;
      function BuscarNumero: Boolean;
      function BuscarCodigoCliente: Boolean;
      function BuscarCodigoVendedor: Boolean;
  end;

implementation

uses
  DB,
  ClsConsecutivo;

{ TNotaDebitoCliente }

procedure TNotaDebitoCliente.Ajustar;
begin

end;

procedure TNotaDebitoCliente.Mover(Ds: TClientDataSet);
begin
  Numero:= Ds.FieldValues['numero'];
  CodigoMoneda:= Ds.FieldValues['codigo_moneda'];
  CodigoCliente:= Ds.FieldValues['codigo_cliente'];
  CodigoVendedor:= Ds.FieldValues['codigo_vendedor'];
  CodigoZona:= Ds.FieldValues['codigo_zona'];
  CodigoEsquemaPago:= Ds.FieldValues['codigo_esquema_pago'];
  FechaIngreso:= Ds.FieldByName('fecha_ingreso').AsDateTime;
  FechaRecepcion:= Ds.FieldByName('fecha_recepcion').AsDateTime;
  FechaContable:= Ds.FieldByName('fecha_contable').AsDateTime;
  FechaLibros:=  Ds.FieldByName('fecha_libros').AsDateTime;
  Concepto:= Ds.FieldValues['concepto'];
  Observaciones:= Ds.FieldValues['observaciones'];
  MontoNeto:= Ds.FieldValues['monto_neto'];
  MontoDescuento:= Ds.FieldValues['monto_descuento'];
  SubTotal:= Ds.FieldValues['sub_total'];
  Impuesto:= Ds.FieldValues['impuesto'];
  Total:= Ds.FieldValues['total'];
  PDescuento:= Ds.FieldValues['p_descuento'];
  PIva:= Ds.FieldValues['p_iva'];
  TasaDolar:= Ds.FieldValues['tasa_dolar'];
  LoginUsuario:= Ds.FieldValues['login_usuario'];
  TiempoIngreso:= Ds.FieldByName('tiempo_ingreso').AsDateTime;
  IdNotaDebitoCliente:= Ds.FieldValues['id_nota_debito_cliente'];
  IdCobranza:= Ds.FieldValues['id_cobranza'];
  IdContadoCobranza:= Ds.FieldValues['id_contado_cobranza'];
end;

function TNotaDebitoCliente.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,25);
  Params[0] := Numero;
  Params[1] := CodigoMoneda;
  Params[2] := CodigoCliente;
  Params[3] := CodigoVendedor;
  Params[4] := CodigoZona;
  Params[5] := CodigoEsquemaPago;
  Params[6] := VarFromDateTime(FechaIngreso);
  Params[7] := VarFromDateTime(FechaRecepcion);
  Params[8] := VarFromDateTime(FechaContable);
  Params[9] := VarFromDateTime(FechaLibros);
  Params[10] := Concepto;
  Params[11] := Observaciones;
  Params[12] := MontoNeto;
  Params[13] := MontoDescuento;
  Params[14] := SubTotal;
  Params[15] := Impuesto;
  Params[16] := Total;
  Params[17] := PDescuento;
  Params[18] := PIva;
  Params[19] := TasaDolar;
  Params[20] := LoginUsuario;
  Params[21] := VarFromDateTime(TiempoIngreso);
  Params[22] := IdNotaDebitoCliente;
  Params[23] := IdCobranza;
  Params[24] := IdContadoCobranza;
  Result := Params;
end;

function TNotaDebitoCliente.GetCampos: TArraystring;
var
  Campos: TArraystring;
begin
  SetLength(Campos,9);
  Campos[0] := 'numero';
  Campos[2] := 'codigo_moneda';
  Campos[3] := 'codigo_cliente';
  Campos[4] := 'codigo_vendedor';
  Campos[5] := 'codigo_zona';
  Campos[6] := 'codigo_esquema_pago';
  Campos[7] := 'concepto';
  Campos[8] := 'observaciones';
  Campos[9] := 'login_usuario';
  Result := Campos;
end;

function TNotaDebitoCliente.GetCondicion: string;
begin
  Result:= '"id_nota_debito_cliente" =' + IntToStr(IdNotaDebitoCliente);
end;

procedure TNotaDebitoCliente.AntesInsertar;
var
  Consecutivo: TConsecutivo;
begin
  Consecutivo := TConsecutivo.Create;
  Numero := IntToStr(Consecutivo.ObtenerConsecutivo('numero_nota_debito_cliente'));
  IdNotaDebitoCliente := Consecutivo.ObtenerConsecutivo('id_nota_debito_cliente');
  IdCobranza := Consecutivo.ObtenerConsecutivo('id_cobranza');
end;

procedure TNotaDebitoCliente.AntesModificar;
begin

end;

procedure TNotaDebitoCliente.AntesEliminar;
begin

end;

procedure TNotaDebitoCliente.DespuesInsertar;
begin

end;

procedure TNotaDebitoCliente.DespuesModificar;
begin

end;

procedure TNotaDebitoCliente.DespuesEliminar;
begin

end;

constructor TNotaDebitoCliente.Create;
var
  ClavesPrimarias: TArraystring;
begin
  inherited Create;
  SetTabla('tbl_nota_debito_clientes');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(25)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "numero"=$1, "codigo_moneda"=$2, "codigo_cliente"=$3, "codigo_vendedor"=$4, "codigo_zona"=$5, "codigo_esquema_pago"=$6, "fecha_ingreso"=$7, ' +
                  '"fecha_recepcion"=$8, "fecha_contable"=$9, "fecha_libros"=$10, "concepto"=$11, "observaciones"=$12, "monto_neto"=$13, "monto_descuento"=$14, "sub_total"=$15, "impuesto"=$16, "total"=$17, "p_descuento"=$18, ' +
                  '"p_iva"=$19, "tasa_dolar"=$20,"login_usuario"=$21, "tiempo_ingreso"=$22, "id_cobranza"=$24, "id_contado_cobranza"=$25 ' +
                  ' WHERE ("id_nota_debito_cliente"=$23)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("id_nota_debito_cliente"=$23)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"id_nota_debito_cliente"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;


procedure TNotaDebitoCliente.SetCodigoCliente(varCodigoCliente: string);
begin
  CodigoCliente := varCodigoCliente;
end;

procedure TNotaDebitoCliente.SetCodigoEsquemaPago(varCodigoEsquemaPago: string);
begin
  CodigoEsquemaPago := varCodigoEsquemaPago;
end;

procedure TNotaDebitoCliente.SetCodigoMoneda(varCodigoMoneda: string);
begin
  CodigoMoneda := varCodigoMoneda;
end;

procedure TNotaDebitoCliente.SetCodigoVendedor(varCodigoVendedor: string);
begin
  CodigoVendedor := varCodigoVendedor;
end;

procedure TNotaDebitoCliente.SetCodigoZona(varCodigoZona: string);
begin
  CodigoZona := varCodigoZona;
end;

procedure TNotaDebitoCliente.SetConcepto(varConcepto: string);
begin
  Concepto := varConcepto;
end;

procedure TNotaDebitoCliente.SetFechaContable(varFechaContable: TDate);
begin
  FechaContable := varFechaContable;
end;

procedure TNotaDebitoCliente.SetFechaIngreso(varFechaIngreso: TDate);
begin
  FechaIngreso := varFechaIngreso;
end;

procedure TNotaDebitoCliente.SetFechaLibros(varFechalibros: TDate);
begin
  FechaLibros := varFechalibros;
end;

procedure TNotaDebitoCliente.SetFechaRecepcion(varFechaRecepcion: TDate);
begin
  FechaRecepcion := varFechaRecepcion;
end;

procedure TNotaDebitoCliente.SetIdCobranza(varIdCobranza: Integer);
begin
  IdCobranza := varIdCobranza;
end;

procedure TNotaDebitoCliente.SetIdContadoCobranza(varIdContadoCobranza: Integer);
begin
  IdContadoCobranza := varIdContadoCobranza;
end;

procedure TNotaDebitoCliente.SetIdNotaDebitoCliente(varIdNotaDebitoCliente: Integer);
begin
  IdNotaDebitoCliente := varIdNotaDebitoCliente;
end;

procedure TNotaDebitoCliente.SetImpuesto(varImpuesto: Double);
begin
  Impuesto := varImpuesto;
end;

procedure TNotaDebitoCliente.SetLoginUsuario(varLoginUsuario: string);
begin
  LoginUsuario := varLoginUsuario;
end;

procedure TNotaDebitoCliente.SetMontoDescuento(varMontoDescuento: Double);
begin
  MontoDescuento := varMontoDescuento;
end;

procedure TNotaDebitoCliente.SetMontoNeto(varMontoNeto: Double);
begin
  MontoNeto := varMontoNeto;
end;

procedure TNotaDebitoCliente.SetNumero(varNumero: string);
begin
  Numero := varNumero;
end;

procedure TNotaDebitoCliente.SetObservaciones(varObservaciones: string);
begin
  Observaciones := varObservaciones;
end;

procedure TNotaDebitoCliente.SetPDescuento(varPDescuento: Double);
begin
  PDescuento := varPDescuento;
end;

procedure TNotaDebitoCliente.SetPIva(varPIva: Double);
begin
  PIva := varPIva;
end;

procedure TNotaDebitoCliente.SetSubTotal(varSubTotal: Double);
begin
  SubTotal := varSubTotal;
end;

procedure TNotaDebitoCliente.SetTasaDolar(varTasaDolar: Double);
begin
  TasaDolar := varTasaDolar;
end;

procedure TNotaDebitoCliente.SetTiempoIngreso(varTiempodeIngreso: TDateTime);
begin
  TiempoIngreso := varTiempodeIngreso;
end;

procedure TNotaDebitoCliente.SetTotal(varTotal: Double);
begin
  Total := varTotal;
end;

function TNotaDebitoCliente.GetCodigoCliente: string;
begin
  Result := CodigoCliente;
end;

function TNotaDebitoCliente.GetCodigoEsquemaPago: string;
begin
  Result := CodigoEsquemaPago;
end;

function TNotaDebitoCliente.GetCodigoMoneda: string;
begin
  Result := CodigoMoneda;
end;

function TNotaDebitoCliente.GetCodigoVendedor: string;
begin
  Result := CodigoVendedor;
end;

function TNotaDebitoCliente.GetCodigoZona: string;
begin
  Result := CodigoZona;
end;

function TNotaDebitoCliente.GetConcepto: string;
begin
  Result := Concepto;
end;

function TNotaDebitoCliente.GetFechaContable: TDate;
begin
  Result := FechaContable;
end;

function TNotaDebitoCliente.GetFechaIngreso: TDate;
begin
  Result := FechaIngreso;
end;

function TNotaDebitoCliente.GetFechaLibros: TDate;
begin
  Result := FechaLibros;
end;

function TNotaDebitoCliente.GetFechaRecepcion: TDate;
begin
  Result := FechaRecepcion;
end;

function TNotaDebitoCliente.GetIdCobranza: Integer;
begin
  Result := IdCobranza;
end;

function TNotaDebitoCliente.GetIdContadoCobranza: Integer;
begin
  Result := IdContadoCobranza;
end;

function TNotaDebitoCliente.GetIdNotaDebitoCliente: Integer;
begin
  Result := IdNotaDebitoCliente;
end;

function TNotaDebitoCliente.GetImpuesto: Double;
begin
  Result := Impuesto;
end;

function TNotaDebitoCliente.GetLoginUsuario: string;
begin
  Result := LoginUsuario;
end;

function TNotaDebitoCliente.GetMontoDescuento: Double;
begin
  Result := MontoDescuento;
end;

function TNotaDebitoCliente.GetMontoNeto: Double;
begin
  Result := MontoNeto;
end;

function TNotaDebitoCliente.GetNumero: string;
begin
  Result := Numero;
end;

function TNotaDebitoCliente.GetObservaciones: string;
begin
  Result := Observaciones;
end;

function TNotaDebitoCliente.GetPDescuento: Double;
begin
  Result := PDescuento;
end;

function TNotaDebitoCliente.GetPIva: Double;
begin
  Result := PIva;
end;

function TNotaDebitoCliente.GetSubTotal: Double;
begin
  Result := SubTotal;
end;

function TNotaDebitoCliente.GetTasaDolar: Double;
begin
  Result := TasaDolar;
end;

function TNotaDebitoCliente.GetTiempoIngreso: TDateTime;
begin
  Result := TiempoIngreso;
end;

function TNotaDebitoCliente.GetTotal: Double;
begin
  Result := Total;
end;

function TNotaDebitoCliente.BuscarNumero: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("numero" = ' + QuotedStr(GetNumero) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

function TNotaDebitoCliente.BuscarCodigoCliente: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("codigo_cliente" = ' + QuotedStr(GetCodigoCliente) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

function TNotaDebitoCliente.BuscarCodigoVendedor: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("codigo_vendedor" = ' + QuotedStr(GetCodigoVendedor) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

end.
