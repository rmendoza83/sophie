unit ClsFacturacionProveedor;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TFacturacionProveedor }

  TFacturacionProveedor = class(TGenericoBD)
    private
      Numero: string;
      NumeroPedido: string;
      CodigoMoneda: string;
      CodigoProveedor: string;
      CodigoZona: string;
      CodigoEstacion: string;
      CodigoEsquemaPago: string;
      FechaIngreso: TDate;
      FechaRecepcion: TDate;
      FechaContable: TDate;
      FechaLibros: TDate;
      Concepto: string;
      Observaciones: string;
      MontoNeto: Double;
      MontoDescuento: Double;
      SubTotal: Double;
      Impuesto: Double;
      Total: Double;
      PDescuento: Double;
      PIva: Double;
      TasaDolar: Double;
      FormaLibre: Boolean;
      LoginUsuario: string;
      TiempoIngreso: TDateTime;
      IdFacturacionProveedor: Integer;
      IdPago: Integer;
      IdMovimientoInventario: Integer;
      IdContadoPago: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArraystring; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetNumero(varNumero: string);
      procedure SetNumeroPedido(varNumeroPedido: string);
      procedure SetCodigoMoneda(varCodigoMoneda: string);
      procedure SetCodigoProveedor(varCodigoProveedor: string);
      procedure SetCodigoZona(varCodigoZona: string);
      procedure SetCodigoEstacion(varCodigoEstacion: string);
      procedure SetCodigoEsquemaPago(varCodigoEsquemaPago: string);
      procedure SetFechaIngreso(varFechaIngreso: TDate);
      procedure SetFechaRecepcion(varFechaRecepcion: TDate);
      procedure SetFechaContable(varFechaContable: TDate);
      procedure SetFechaLibros(varFechalibros: TDate);
      procedure SetConcepto(varConcepto: string);
      procedure SetObservaciones(varObservaciones: string);
      procedure SetMontoNeto(varMontoNeto: Double);
      procedure SetMontoDescuento(varMontoDescuento: Double);
      procedure SetSubTotal(varSubTotal: Double);
      procedure SetImpuesto(varImpuesto: Double);
      procedure SetTotal(varTotal: Double);
      procedure SetPDescuento(varPDescuento: Double);
      procedure SetPIva(varPIva: Double);
      procedure SetTasaDolar(varTasaDolar: Double);
      procedure SetFormaLibre(varFormaLibre: Boolean);
      procedure SetLoginUsuario(varLoginUsuario: string);
      procedure SetTiempoIngreso(varTiempodeIngreso: TDateTime);
      procedure SetIdFacturacionProveedor(varIdFacturacionProveedor: Integer);
      procedure SetIdPago(varIdPago: Integer);
      procedure SetIdMovimientoInventario(varIdMovimientoInventario: Integer);
      procedure SetIdContadoPago(varIdContadoPago: Integer);
      function GetNumero: string;
      function GetNumeroPedido: string;
      function GetCodigoMoneda: string;
      function GetCodigoProveedor: string;
      function GetCodigoZona: string;
      function GetCodigoEstacion: string;
      function GetCodigoEsquemaPago: string;
      function GetFechaIngreso: TDate;
      function GetFechaRecepcion: TDate;
      function GetFechaContable: TDate;
      function GetFechaLibros: TDate;
      function GetConcepto: string;
      function GetObservaciones: string;
      function GetMontoNeto: Double;
      function GetMontoDescuento: Double;
      function GetSubTotal: Double;
      function GetImpuesto: Double;
      function GetTotal: Double;
      function GetPDescuento: Double;
      function GetPIva: Double;
      function GetTasaDolar: Double;
      function GetFormaLibre: Boolean;
      function GetLoginUsuario: string;
      function GetTiempoIngreso: TDateTime;
      function GetIdFacturacionProveedor: Integer;
      function GetIdPago: Integer;
      function GetIdMovimientoInventario: Integer;
      function GetIdContadoPago: Integer;
      function BuscarNumero: Boolean;
      function BuscarFacturaProveedor: Boolean;
      function BuscarCodigoProveedor: Boolean;
  end;

implementation

uses
  DB,
  ClsConsecutivo,
  ClsEsquemaPago;

{ TFacturacionProveedor }

procedure TFacturacionProveedor.Ajustar;
begin

end;

procedure TFacturacionProveedor.Mover(Ds: TClientDataSet);
begin
  Numero:= Ds.FieldValues['numero'];
  NumeroPedido:= Ds.FieldValues['numero_pedido'];
  CodigoMoneda:= Ds.FieldValues['codigo_moneda'];
  CodigoProveedor:= Ds.FieldValues['codigo_proveedor'];
  CodigoZona:= Ds.FieldValues['codigo_zona'];
  CodigoEstacion:= Ds.FieldValues['codigo_estacion'];
  CodigoEsquemaPago:= Ds.FieldValues['codigo_esquema_pago'];
  FechaIngreso:= Ds.FieldByName('fecha_ingreso').AsDateTime;
  FechaRecepcion:= Ds.FieldByName('fecha_recepcion').AsDateTime;
  FechaContable:= Ds.FieldByName('fecha_contable').AsDateTime;
  FechaLibros:=  Ds.FieldByName('fecha_libros').AsDateTime;
  Concepto:= Ds.FieldValues['concepto'];
  Observaciones:= Ds.FieldValues['observaciones'];
  MontoNeto:= Ds.FieldValues['monto_neto'];
  MontoDescuento:= Ds.FieldValues['monto_descuento'];
  SubTotal:= Ds.FieldValues['sub_total'];
  Impuesto:= Ds.FieldValues['impuesto'];
  Total:= Ds.FieldValues['total'];
  PDescuento:= Ds.FieldValues['p_descuento'];
  PIva:= Ds.FieldValues['p_iva'];
  TasaDolar:= Ds.FieldValues['tasa_dolar'];
  FormaLibre:= Ds.FieldValues['forma_libre'];
  LoginUsuario:= Ds.FieldValues['login_usuario'];
  TiempoIngreso:= Ds.FieldByName('tiempo_ingreso').AsDateTime;
  IdFacturacionProveedor:= Ds.FieldValues['id_facturacion_proveedor'];
  IdPago:= Ds.FieldValues['id_pago'];
  IdMovimientoInventario:= Ds.FieldValues['id_movimiento_inventario'];
  IdContadoPago := Ds.FieldValues['id_contado_pago'];
end;

function TFacturacionProveedor.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,28);
  Params[0] := Numero;
  Params[1] := NumeroPedido;
  Params[2] := CodigoMoneda;
  Params[3] := CodigoProveedor;
  Params[4] := CodigoZona;
  Params[5] := CodigoEstacion;
  Params[6] := CodigoEsquemaPago;
  Params[7] := VarFromDateTime(FechaIngreso);
  Params[8] := VarFromDateTime(FechaRecepcion);
  Params[9] := VarFromDateTime(FechaContable);
  Params[10] := VarFromDateTime(FechaLibros);
  Params[11] := Concepto;
  Params[12] := Observaciones;
  Params[13] := MontoNeto;
  Params[14] := MontoDescuento;
  Params[15] := SubTotal;
  Params[16] := Impuesto;
  Params[17] := Total;
  Params[18] := PDescuento;
  Params[19] := PIva;
  Params[20] := TasaDolar;
  Params[21] := FormaLibre;
  Params[22] := LoginUsuario;
  Params[23] := VarFromDateTime(TiempoIngreso);
  Params[24] := IdFacturacionProveedor;
  Params[25] := IdPago;
  Params[26] := IdMovimientoInventario;
  Params[27] := IdContadoPago;
  Result := Params;
end;

function TFacturacionProveedor.GetCampos: TArraystring;
var
  Campos: TArraystring;
begin
  SetLength(Campos,10);
  Campos[0] := 'numero';
  Campos[1] := 'numero_pedido';
  Campos[2] := 'codigo_moneda';
  Campos[3] := 'codigo_proveedor';
  Campos[4] := 'codigo_zona';
  Campos[5] := 'codigo_estacion';
  Campos[6] := 'codigo_esquema_pago';
  Campos[7] := 'concepto';
  Campos[8] := 'observaciones';
  Campos[9] := 'login_usuario';
  Result := Campos;
end;

function TFacturacionProveedor.GetCondicion: string;
begin
  Result:= '"id_facturacion_proveedor" =' + IntToStr(IdFacturacionProveedor);
end;

procedure TFacturacionProveedor.AntesInsertar;
var
  Consecutivo: TConsecutivo;
begin
  Consecutivo := TConsecutivo.Create;
  IdFacturacionProveedor := Consecutivo.ObtenerConsecutivo('id_facturacion_proveedor');
  if (not FormaLibre) then
  begin
    IdMovimientoInventario := Consecutivo.ObtenerConsecutivo('id_movimiento_inventario');
  end;
  IdPago := Consecutivo.ObtenerConsecutivo('id_pago');
end;

procedure TFacturacionProveedor.AntesModificar;
begin

end;

procedure TFacturacionProveedor.AntesEliminar;
begin

end;

procedure TFacturacionProveedor.DespuesInsertar;
begin

end;

procedure TFacturacionProveedor.DespuesModificar;
begin

end;

procedure TFacturacionProveedor.DespuesEliminar;
begin

end;

constructor TFacturacionProveedor.Create;
var
  ClavesPrimarias: TArraystring;
begin
  inherited Create;
  SetTabla('tbl_facturacion_proveedores');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(28)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "numero"=$1, "numero_pedido"=$2, "codigo_moneda"=$3, "codigo_proveedor"=$4, "codigo_zona"=$5, "codigo_estacion"=$6, "codigo_esquema_pago"=$7, "fecha_ingreso"=$8, "fecha_recepcion"=$9, ' +
                  '"fecha_contable"=$10, "fecha_libros"=$11, "concepto"=$12, "observaciones"=$13, "monto_neto"=$14, "monto_descuento"=$15, "sub_total"=$16, "impuesto"=$17, "total"=$18, "p_descuento"=$19, "p_iva"=$20, ' +
                  '"tasa_dolar"=$21, "forma_libre"=$22, "login_usuario"=$23, "tiempo_ingreso"=$24, "id_pago"=$26, "id_movimiento_inventario"=$27, "id_contado_pago"=$28 ' +
                  'WHERE ("id_facturacion_proveedor"=$25)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("id_facturacion_proveedor"=$25)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"id_facturacion_proveedor"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TFacturacionProveedor.SetCodigoProveedor(varCodigoProveedor: string);
begin
 CodigoProveedor := varCodigoProveedor;
end;

procedure TFacturacionProveedor.SetCodigoEsquemaPago(varCodigoEsquemaPago: string);
begin
  CodigoEsquemaPago := varCodigoEsquemaPago;
end;

procedure TFacturacionProveedor.SetCodigoEstacion(varCodigoEstacion: string);
begin
  CodigoEstacion := varCodigoEstacion;
end;

procedure TFacturacionProveedor.SetCodigoMoneda(varCodigoMoneda: string);
begin
  CodigoMoneda := varCodigoMoneda;
end;

procedure TFacturacionProveedor.SetCodigoZona(varCodigoZona: string);
begin
  CodigoZona := varCodigoZona;
end;

procedure TFacturacionProveedor.SetConcepto(varConcepto: string);
begin
  Concepto := varConcepto;
end;

procedure TFacturacionProveedor.SetFechaContable(varFechaContable: TDate);
begin
  FechaContable := varFechaContable;
end;

procedure TFacturacionProveedor.SetFechaIngreso(varFechaIngreso: TDate);
begin
  FechaIngreso := varFechaIngreso;
end;

procedure TFacturacionProveedor.SetFechaLibros(varFechalibros: TDate);
begin
  FechaLibros := varFechalibros;
end;

procedure TFacturacionProveedor.SetFechaRecepcion(varFechaRecepcion: TDate);
begin
  FechaRecepcion := varFechaRecepcion;
end;

procedure TFacturacionProveedor.SetFormaLibre(varFormaLibre: Boolean);
begin
  FormaLibre := varFormaLibre;
end;

procedure TFacturacionProveedor.SetIdFacturacionProveedor(varIdFacturacionProveedor: Integer);
begin
  IdFacturacionProveedor := varIdFacturacionProveedor;
end;

procedure TFacturacionProveedor.SetIdPago(varIdPago: Integer);
begin
  IdPago := varIdPago;
end;

procedure TFacturacionProveedor.SetIdMovimientoInventario(varIdMovimientoInventario: Integer);
begin
  IdMovimientoInventario := varIdMovimientoInventario;
end;

procedure TFacturacionProveedor.SetIdContadoPago(varIdContadoPago: Integer);
begin
  IdContadoPago := varIdContadoPago;
end;

procedure TFacturacionProveedor.SetImpuesto(varImpuesto: Double);
begin
  Impuesto := varImpuesto;
end;

procedure TFacturacionProveedor.SetLoginUsuario(varLoginUsuario: string);
begin
  LoginUsuario := varLoginUsuario;
end;

procedure TFacturacionProveedor.SetMontoDescuento(varMontoDescuento: Double);
begin
  MontoDescuento := varMontoDescuento;
end;

procedure TFacturacionProveedor.SetMontoNeto(varMontoNeto: Double);
begin
  MontoNeto := varMontoNeto;
end;

procedure TFacturacionProveedor.SetNumero(varNumero: string);
begin
  Numero := varNumero;
end;

procedure TFacturacionProveedor.SetNumeroPedido(varNumeroPedido: string);
begin
  NumeroPedido := varNumeroPedido;
end;

procedure TFacturacionProveedor.SetObservaciones(varObservaciones: string);
begin
  Observaciones := varObservaciones;
end;

procedure TFacturacionProveedor.SetPDescuento(varPDescuento: Double);
begin
  PDescuento := varPDescuento;
end;

procedure TFacturacionProveedor.SetPIva(varPIva: Double);
begin
  PIva := varPIva;
end;

procedure TFacturacionProveedor.SetSubTotal(varSubTotal: Double);
begin
  SubTotal := varSubTotal;
end;

procedure TFacturacionProveedor.SetTasaDolar(varTasaDolar: Double);
begin
  TasaDolar := varTasaDolar;
end;

procedure TFacturacionProveedor.SetTiempoIngreso(varTiempodeIngreso: TDateTime);
begin
  TiempoIngreso := varTiempodeIngreso;
end;

procedure TFacturacionProveedor.SetTotal(varTotal: Double);
begin
  Total := varTotal;
end;

function TFacturacionProveedor.GetCodigoProveedor: string;
begin
  Result := CodigoProveedor
end;

function TFacturacionProveedor.GetCodigoEsquemaPago: string;
begin
  Result := CodigoEsquemaPago;
end;

function TFacturacionProveedor.GetCodigoEstacion: string;
begin
  Result := CodigoEstacion;
end;

function TFacturacionProveedor.GetCodigoMoneda: string;
begin
  Result := CodigoMoneda;
end;

function TFacturacionProveedor.GetCodigoZona: string;
begin
  Result := CodigoZona;
end;

function TFacturacionProveedor.GetConcepto: string;
begin
  Result := Concepto;
end;

function TFacturacionProveedor.GetFechaContable: TDate;
begin
  Result := FechaContable;
end;

function TFacturacionProveedor.GetFechaIngreso: TDate;
begin
  Result := FechaIngreso;
end;

function TFacturacionProveedor.GetFechaLibros: TDate;
begin
  Result := FechaLibros;
end;

function TFacturacionProveedor.GetFechaRecepcion: TDate;
begin
  Result := FechaRecepcion;
end;

function TFacturacionProveedor.GetFormaLibre: Boolean;
begin
  Result := FormaLibre;
end;

function TFacturacionProveedor.GetIdPago: Integer;
begin
  Result := IdPago;
end;

function TFacturacionProveedor.GetIdContadoPago: Integer;
begin
  Result := IdContadoPago;
end;

function TFacturacionProveedor.GetIdFacturacionProveedor: Integer;
begin
  Result := IdFacturacionProveedor;
end;

function TFacturacionProveedor.GetIdMovimientoInventario: Integer;
begin
  Result := IdMovimientoInventario;
end;

function TFacturacionProveedor.GetImpuesto: Double;
begin
  Result := Impuesto;
end;

function TFacturacionProveedor.GetLoginUsuario: string;
begin
  Result := LoginUsuario;
end;

function TFacturacionProveedor.GetMontoDescuento: Double;
begin
  Result := MontoDescuento;
end;

function TFacturacionProveedor.GetMontoNeto: Double;
begin
  Result := MontoNeto;
end;

function TFacturacionProveedor.GetNumero: string;
begin
  Result := Numero;
end;

function TFacturacionProveedor.GetNumeroPedido: string;
begin
  Result := NumeroPedido;
end;

function TFacturacionProveedor.GetObservaciones: string;
begin
  Result := Observaciones;
end;

function TFacturacionProveedor.GetPDescuento: Double;
begin
  Result := PDescuento;
end;

function TFacturacionProveedor.GetPIva: Double;
begin
  Result := PIva;
end;

function TFacturacionProveedor.GetSubTotal: Double;
begin
  Result := SubTotal;
end;

function TFacturacionProveedor.GetTasaDolar: Double;
begin
  Result := TasaDolar;
end;

function TFacturacionProveedor.GetTiempoIngreso: TDateTime;
begin
  Result := TiempoIngreso;
end;

function TFacturacionProveedor.GetTotal: Double;
begin
  Result := Total;
end;

function TFacturacionProveedor.BuscarNumero: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("numero" = ' + QuotedStr(GetNumero) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

function TFacturacionProveedor.BuscarFacturaProveedor: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("numero" = ' + QuotedStr(GetNumero) + ') AND ("codigo_proveedor" = ' + QuotedStr(GetCodigoProveedor) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

function TFacturacionProveedor.BuscarCodigoProveedor: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("codigo_proveedor" = ' + QuotedStr(GetCodigoProveedor) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

end.
