unit ClsNotaDebitoProveedor;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TNotaDebitoProveedor }

  TNotaDebitoProveedor = class(TGenericoBD)
    private
      Numero: string;
      CodigoMoneda: string;
      CodigoProveedor: string;
      CodigoZona: string;
      CodigoEsquemaPago: string;
      FechaIngreso: TDate;
      FechaRecepcion: TDate;
      FechaContable: TDate;
      FechaLibros: TDate;
      Concepto: string;
      Observaciones: string;
      MontoNeto: Double;
      MontoDescuento: Double;
      SubTotal: Double;
      Impuesto: Double;
      Total: Double;
      PDescuento: Double;
      PIva: Double;
      TasaDolar: Double;
      LoginUsuario: string;
      TiempoIngreso: TDateTime;
      IdNotaDebitoProveedor: Integer;
      IdPago: Integer;
      IdContadoPago: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArraystring; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetNumero(varNumero: string);
      procedure SetCodigoMoneda(varCodigoMoneda: string);
      procedure SetCodigoProveedor(varCodigoProveedor: string);
      procedure SetCodigoZona(varCodigoZona: string);
      procedure SetCodigoEsquemaPago(varCodigoEsquemaPago: string);
      procedure SetFechaIngreso(varFechaIngreso: TDate);
      procedure SetFechaRecepcion(varFechaRecepcion: TDate);
      procedure SetFechaContable(varFechaContable: TDate);
      procedure SetFechaLibros(varFechalibros: TDate);
      procedure SetConcepto(varConcepto: string);
      procedure SetObservaciones(varObservaciones: string);
      procedure SetMontoNeto(varMontoNeto: Double);
      procedure SetMontoDescuento(varMontoDescuento: Double);
      procedure SetSubTotal(varSubTotal: Double);
      procedure SetImpuesto(varImpuesto: Double);
      procedure SetTotal(varTotal: Double);
      procedure SetPDescuento(varPDescuento: Double);
      procedure SetPIva(varPIva: Double);
      procedure SetTasaDolar(varTasaDolar: Double);
      procedure SetLoginUsuario(varLoginUsuario: string);
      procedure SetTiempoIngreso(varTiempodeIngreso: TDateTime);
      procedure SetIdNotaDebitoProveedor(varIdNotaDebitoProveedor: Integer);
      procedure SetIdPago(varIdPago: Integer);
      procedure SetIdContadoPago(varIdContadoPago: Integer);
      function GetNumero: string;
      function GetCodigoMoneda: string;
      function GetCodigoProveedor: string;
      function GetCodigoZona: string;
      function GetCodigoEsquemaPago: string;
      function GetFechaIngreso: TDate;
      function GetFechaRecepcion: TDate;
      function GetFechaContable: TDate;
      function GetFechaLibros: TDate;
      function GetConcepto: string;
      function GetObservaciones: string;
      function GetMontoNeto: Double;
      function GetMontoDescuento: Double;
      function GetSubTotal: Double;
      function GetImpuesto: Double;
      function GetTotal: Double;
      function GetPDescuento: Double;
      function GetPIva: Double;
      function GetTasaDolar: Double;
      function GetLoginUsuario: string;
      function GetTiempoIngreso: TDateTime;
      function GetIdNotaDebitoProveedor: Integer;
      function GetIdPago: Integer;
      function GetIdContadoPagos: Integer;
      function BuscarNumero: Boolean;
      function BuscarCodigoProveedor: Boolean;
  end;

implementation

uses
  DB,
  ClsConsecutivo,
  ClsEsquemaPago;

{ TNotaDebitoProveedor }

procedure TNotaDebitoProveedor.Ajustar;
begin

end;

procedure TNotaDebitoProveedor.Mover(Ds: TClientDataSet);
begin
  Numero:= Ds.FieldValues['numero'];
  CodigoMoneda:= Ds.FieldValues['codigo_moneda'];
  CodigoProveedor:= Ds.FieldValues['codigo_proveedor'];
  CodigoZona:= Ds.FieldValues['codigo_zona'];
  CodigoEsquemaPago:= Ds.FieldValues['codigo_esquema_pago'];
  FechaIngreso:= Ds.FieldByName('fecha_ingreso').AsDateTime;
  FechaRecepcion:= Ds.FieldByName('fecha_recepcion').AsDateTime;
  FechaContable:= Ds.FieldByName('fecha_contable').AsDateTime;
  FechaLibros:=  Ds.FieldByName('fecha_libros').AsDateTime;
  Concepto:= Ds.FieldValues['concepto'];
  Observaciones:= Ds.FieldValues['observaciones'];
  MontoNeto:= Ds.FieldValues['monto_neto'];
  MontoDescuento:= Ds.FieldValues['monto_descuento'];
  SubTotal:= Ds.FieldValues['sub_total'];
  Impuesto:= Ds.FieldValues['impuesto'];
  Total:= Ds.FieldValues['total'];
  PDescuento:= Ds.FieldValues['p_descuento'];
  PIva:= Ds.FieldValues['p_iva'];
  TasaDolar:= Ds.FieldValues['tasa_dolar'];
  LoginUsuario:= Ds.FieldValues['login_usuario'];
  TiempoIngreso:= Ds.FieldByName('tiempo_ingreso').AsDateTime;
  IdNotaDebitoProveedor:= Ds.FieldValues['id_nota_debito_proveedor'];
  IdPago:= Ds.FieldValues['id_pago'];
  IdContadoPago:= Ds.FieldValues['id_contado_pago'];
end;

function TNotaDebitoProveedor.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,24);
  Params[0] := Numero;
  Params[1] := CodigoMoneda;
  Params[2] := CodigoProveedor;
  Params[3] := CodigoZona;
  Params[4] := CodigoEsquemaPago;
  Params[5] := VarFromDateTime(FechaIngreso);
  Params[6] := VarFromDateTime(FechaRecepcion);
  Params[7] := VarFromDateTime(FechaContable);
  Params[8] := VarFromDateTime(FechaLibros);
  Params[9] := Concepto;
  Params[10] := Observaciones;
  Params[11] := MontoNeto;
  Params[12] := MontoDescuento;
  Params[13] := SubTotal;
  Params[14] := Impuesto;
  Params[15] := Total;
  Params[16] := PDescuento;
  Params[17] := PIva;
  Params[18] := TasaDolar;
  Params[19] := LoginUsuario;
  Params[20] := VarFromDateTime(TiempoIngreso);
  Params[21] := IdNotaDebitoProveedor;
  Params[22] := IdPago;
  Params[23] := IdContadoPago;
  Result := Params;
end;

function TNotaDebitoProveedor.GetCampos: TArraystring;
var
  Campos: TArraystring;
begin
  SetLength(Campos,8);
  Campos[0] := 'numero';
  Campos[1] := 'codigo_moneda';
  Campos[2] := 'codigo_proveedor';
  Campos[3] := 'codigo_zona';
  Campos[4] := 'codigo_esquema_pago';
  Campos[5] := 'concepto';
  Campos[6] := 'observaciones';
  Campos[7] := 'login_usuario';
  Result := Campos;
end;

function TNotaDebitoProveedor.GetCondicion: string;
begin
  Result:= '"id_nota_debito_proveedor" =' + IntToStr(IdNotaDebitoProveedor);
end;

procedure TNotaDebitoProveedor.AntesInsertar;
var
  Consecutivo: TConsecutivo;
begin
  Consecutivo := TConsecutivo.Create;
  //Numero := IntToStr(Consecutivo.ObtenerConsecutivo('numero_nota_debito_proveedor'));
  IdNotaDebitoProveedor := Consecutivo.ObtenerConsecutivo('id_nota_debito_proveedor');
  //IdPago := Consecutivo.ObtenerConsecutivo('id_pago');
end;

procedure TNotaDebitoProveedor.AntesModificar;
begin

end;

procedure TNotaDebitoProveedor.AntesEliminar;
begin

end;

procedure TNotaDebitoProveedor.DespuesInsertar;
begin

end;

procedure TNotaDebitoProveedor.DespuesModificar;
begin

end;

procedure TNotaDebitoProveedor.DespuesEliminar;
begin

end;

constructor TNotaDebitoProveedor.Create;
var
  ClavesPrimarias: TArraystring;
begin
  inherited Create;
  SetTabla('tbl_nota_debito_proveedores');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(24)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "numero"=$1, "codigo_moneda"=$2, "codigo_proveedor"=$3, "codigo_zona"=$4, "codigo_esquema_pago"=$5, "fecha_ingreso"=$6, "fecha_recepcion"=$7, ' +
                  '"fecha_contable"=$8, "fecha_libros"=$9, "concepto"=$10, "observaciones"=$11, "monto_neto"=$12, "monto_descuento"=$13, "sub_total"=$14, "impuesto"=$15, "total"=$16, "p_descuento"=$17, "p_iva"=$18, ' +
                  '"tasa_dolar"=$19, "login_usuario"=$20, "tiempo_ingreso"=$21, "id_pago"=$23, "id_contado_pago"=$24 ' +
                  'WHERE ("id_nota_debito_proveedor"=$22)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("id_nota_debito_proveedor"=$22)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"id_nota_debito_proveedor"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TNotaDebitoProveedor.SetCodigoEsquemaPago(varCodigoEsquemaPago: string);
begin
  CodigoEsquemaPago := varCodigoEsquemaPago;
end;

procedure TNotaDebitoProveedor.SetCodigoMoneda(varCodigoMoneda: string);
begin
  CodigoMoneda := varCodigoMoneda;
end;

procedure TNotaDebitoProveedor.SetCodigoProveedor(varCodigoProveedor: string);
begin
  CodigoProveedor := varCodigoProveedor;
end;

procedure TNotaDebitoProveedor.SetCodigoZona(varCodigoZona: string);
begin
  CodigoZona := varCodigoZona;
end;

procedure TNotaDebitoProveedor.SetConcepto(varConcepto: string);
begin
  Concepto := varConcepto;
end;

procedure TNotaDebitoProveedor.SetFechaContable(varFechaContable: TDate);
begin
  FechaContable := varFechaContable;
end;

procedure TNotaDebitoProveedor.SetFechaIngreso(varFechaIngreso: TDate);
begin
  FechaIngreso := varFechaIngreso;
end;

procedure TNotaDebitoProveedor.SetFechaLibros(varFechalibros: TDate);
begin
  FechaLibros := varFechalibros;
end;

procedure TNotaDebitoProveedor.SetFechaRecepcion(varFechaRecepcion: TDate);
begin
  FechaRecepcion := varFechaRecepcion;
end;

procedure TNotaDebitoProveedor.SetIdContadoPago(varIdContadoPago: Integer);
begin
  IdContadoPago := varIdContadoPago;
end;

procedure TNotaDebitoProveedor.SetIdNotaDebitoProveedor(varIdNotaDebitoProveedor: Integer);
begin
  IdNotaDebitoProveedor := varIdNotaDebitoProveedor;
end;

procedure TNotaDebitoProveedor.SetIdPago(varIdPago: Integer);
begin
  IdPago := varIdPago;
end;

procedure TNotaDebitoProveedor.SetImpuesto(varImpuesto: Double);
begin
  Impuesto := varImpuesto;
end;

procedure TNotaDebitoProveedor.SetLoginUsuario(varLoginUsuario: string);
begin
  LoginUsuario := varLoginUsuario;
end;

procedure TNotaDebitoProveedor.SetMontoDescuento(varMontoDescuento: Double);
begin
  MontoDescuento := varMontoDescuento;
end;

procedure TNotaDebitoProveedor.SetMontoNeto(varMontoNeto: Double);
begin
  MontoNeto := varMontoNeto;
end;

procedure TNotaDebitoProveedor.SetNumero(varNumero: string);
begin
  Numero := varNumero;
end;

procedure TNotaDebitoProveedor.SetObservaciones(varObservaciones: string);
begin
  Observaciones := varObservaciones;
end;

procedure TNotaDebitoProveedor.SetPDescuento(varPDescuento: Double);
begin
  PDescuento := varPDescuento;
end;

procedure TNotaDebitoProveedor.SetPIva(varPIva: Double);
begin
  PIva := varPIva;
end;

procedure TNotaDebitoProveedor.SetSubTotal(varSubTotal: Double);
begin
  SubTotal := varSubTotal;
end;

procedure TNotaDebitoProveedor.SetTasaDolar(varTasaDolar: Double);
begin
  TasaDolar := varTasaDolar;
end;

procedure TNotaDebitoProveedor.SetTiempoIngreso(varTiempodeIngreso: TDateTime);
begin
  TiempoIngreso := varTiempodeIngreso;
end;

procedure TNotaDebitoProveedor.SetTotal(varTotal: Double);
begin
  Total := varTotal;
end;

function TNotaDebitoProveedor.GetCodigoEsquemaPago: string;
begin
  Result := CodigoEsquemaPago;
end;

function TNotaDebitoProveedor.GetCodigoMoneda: string;
begin
  Result := CodigoMoneda;
end;

function TNotaDebitoProveedor.GetCodigoProveedor: string;
begin
  Result := CodigoProveedor;
end;

function TNotaDebitoProveedor.GetCodigoZona: string;
begin
  Result := CodigoZona;
end;

function TNotaDebitoProveedor.GetConcepto: string;
begin
  Result := Concepto;
end;

function TNotaDebitoProveedor.GetFechaContable: TDate;
begin
  Result := FechaContable;
end;

function TNotaDebitoProveedor.GetFechaIngreso: TDate;
begin
  Result := FechaIngreso;
end;

function TNotaDebitoProveedor.GetFechaLibros: TDate;
begin
  Result := FechaLibros;
end;

function TNotaDebitoProveedor.GetFechaRecepcion: TDate;
begin
  Result := FechaRecepcion;
end;

function TNotaDebitoProveedor.GetIdContadoPagos: Integer;
begin
  Result := IdContadoPago;
end;

function TNotaDebitoProveedor.GetIdNotaDebitoProveedor: Integer;
begin
  Result := IdNotaDebitoProveedor;
end;

function TNotaDebitoProveedor.GetIdPago: Integer;
begin
  Result := IdPago;
end;

function TNotaDebitoProveedor.GetImpuesto: Double;
begin
  Result := Impuesto;
end;

function TNotaDebitoProveedor.GetLoginUsuario: string;
begin
  Result := LoginUsuario;
end;

function TNotaDebitoProveedor.GetMontoDescuento: Double;
begin
  Result := MontoDescuento;
end;

function TNotaDebitoProveedor.GetMontoNeto: Double;
begin
  Result := MontoNeto;
end;

function TNotaDebitoProveedor.GetNumero: string;
begin
  Result := Numero;
end;

function TNotaDebitoProveedor.GetObservaciones: string;
begin
  Result := Observaciones;
end;

function TNotaDebitoProveedor.GetPDescuento: Double;
begin
  Result := PDescuento;
end;

function TNotaDebitoProveedor.GetPIva: Double;
begin
  Result := PIva;
end;

function TNotaDebitoProveedor.GetSubTotal: Double;
begin
  Result := SubTotal;
end;

function TNotaDebitoProveedor.GetTasaDolar: Double;
begin
  Result := TasaDolar;
end;

function TNotaDebitoProveedor.GetTiempoIngreso: TDateTime;
begin
  Result := TiempoIngreso;
end;

function TNotaDebitoProveedor.GetTotal: Double;
begin
  Result := Total;
end;

function TNotaDebitoProveedor.BuscarNumero: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("numero" = ' + QuotedStr(GetNumero) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

function TNotaDebitoProveedor.BuscarCodigoProveedor: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("codigo_proveedor" = ' + QuotedStr(GetCodigoProveedor) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

end.
