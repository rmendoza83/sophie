unit ClsFacturacionCliente;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TFacturacionCliente }

  TFacturacionCliente = class(TGenericoBD)
    private
      Numero: string;
      NumeroPedido: string;
      CodigoMoneda: string;
      CodigoCliente: string;
      CodigoVendedor: string;
      CodigoZona: string;
      CodigoEstacion: string;
      CodigoEsquemaPago: string;
      FechaIngreso: TDate;
      FechaRecepcion: TDate;
      FechaContable: TDate;
      FechaLibros: TDate;
      Concepto: string;
      Observaciones: string;
      MontoNeto: Double;
      MontoDescuento: Double;
      SubTotal: Double;
      Impuesto: Double;
      Total: Double;
      PDescuento: Double;
      PIva: Double;
      TasaDolar: Double;
      FormaLibre: Boolean;
      LoginUsuario: string;
      TiempoIngreso: TDateTime;
      IdFacturacionCliente: Integer;
      IdCobranza: Integer;
      IdMovimientoInventario: Integer;
      IdContadoCobranza: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArraystring; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetNumero(varNumero: string);
      procedure SetNumeroPedido(varNumeroPedido: string);
      procedure SetCodigoMoneda(varCodigoMoneda: string);
      procedure SetCodigoCliente(varCodigoCliente: string);
      procedure SetCodigoVendedor(varCodigoVendedor: string);
      procedure SetCodigoZona(varCodigoZona: string);
      procedure SetCodigoEstacion(varCodigoEstacion: string);
      procedure SetCodigoEsquemaPago(varCodigoEsquemaPago: string);
      procedure SetFechaIngreso(varFechaIngreso: TDate);
      procedure SetFechaRecepcion(varFechaRecepcion: TDate);
      procedure SetFechaContable(varFechaContable: TDate);
      procedure SetFechaLibros(varFechalibros: TDate);
      procedure SetConcepto(varConcepto: string);
      procedure SetObservaciones(varObservaciones: string);
      procedure SetMontoNeto(varMontoNeto: Double);
      procedure SetMontoDescuento(varMontoDescuento: Double);
      procedure SetSubTotal(varSubTotal: Double);
      procedure SetImpuesto(varImpuesto: Double);
      procedure SetTotal(varTotal: Double);
      procedure SetPDescuento(varPDescuento: Double);
      procedure SetPIva(varPIva: Double);
      procedure SetTasaDolar(varTasaDolar: Double);
      procedure SetFormaLibre(varFormaLibre: Boolean);
      procedure SetLoginUsuario(varLoginUsuario: string);
      procedure SetTiempoIngreso(varTiempodeIngreso: TDateTime);
      procedure SetIdFacturacionCliente(varIdFacturacionCliente: Integer);
      procedure SetIdCobranza(varIdCobranza: Integer);
      procedure SetIdMovimientoInventario(varIdMovimientoInventario: Integer);
      procedure SetIdContadoCobranza(varIdContadoCobranza: Integer);
      function GetNumero: string;
      function GetNumeroPedido: string;
      function GetCodigoMoneda: string;
      function GetCodigoCliente: string;
      function GetCodigoVendedor: string;
      function GetCodigoZona: string;
      function GetCodigoEstacion: string;
      function GetCodigoEsquemaPago: string;
      function GetFechaIngreso: TDate;
      function GetFechaRecepcion: TDate;
      function GetFechaContable: TDate;
      function GetFechaLibros: TDate;
      function GetConcepto: string;
      function GetObservaciones: string;
      function GetMontoNeto: Double;
      function GetMontoDescuento: Double;
      function GetSubTotal: Double;
      function GetImpuesto: Double;
      function GetTotal: Double;
      function GetPDescuento: Double;
      function GetPIva: Double;
      function GetTasaDolar: Double;
      function GetFormaLibre: Boolean;
      function GetLoginUsuario: string;
      function GetTiempoIngreso: TDateTime;
      function GetIdFacturacionCliente: Integer;
      function GetIdCobranza: Integer;
      function GetIdMovimientoInventario: Integer;
      function GetIdContadoCobranza: Integer;
      function BuscarNumero: Boolean;
      function BuscarCodigoCliente: Boolean;
      function BuscarCodigoVendedor: Boolean;
  end;

implementation

uses
  DB,
  ClsConsecutivo;

{ TFacturacionCliente }

procedure TFacturacionCliente.Ajustar;
begin

end;

procedure TFacturacionCliente.Mover(Ds: TClientDataSet);
begin
  Numero:= Ds.FieldValues['numero'];
  NumeroPedido:= Ds.FieldValues['numero_pedido'];
  CodigoMoneda:= Ds.FieldValues['codigo_moneda'];
  CodigoCliente:= Ds.FieldValues['codigo_cliente'];
  CodigoVendedor:= Ds.FieldValues['codigo_vendedor'];
  CodigoZona:= Ds.FieldValues['codigo_zona'];
  CodigoEstacion:= Ds.FieldValues['codigo_estacion'];
  CodigoEsquemaPago:= Ds.FieldValues['codigo_esquema_pago'];
  FechaIngreso:= Ds.FieldByName('fecha_ingreso').AsDateTime;
  FechaRecepcion:= Ds.FieldByName('fecha_recepcion').AsDateTime;
  FechaContable:= Ds.FieldByName('fecha_contable').AsDateTime;
  FechaLibros:=  Ds.FieldByName('fecha_libros').AsDateTime;
  Concepto:= Ds.FieldValues['concepto'];
  Observaciones:= Ds.FieldValues['observaciones'];
  MontoNeto:= Ds.FieldValues['monto_neto'];
  MontoDescuento:= Ds.FieldValues['monto_descuento'];
  SubTotal:= Ds.FieldValues['sub_total'];
  Impuesto:= Ds.FieldValues['impuesto'];
  Total:= Ds.FieldValues['total'];
  PDescuento:= Ds.FieldValues['p_descuento'];
  PIva:= Ds.FieldValues['p_iva'];
  TasaDolar:= Ds.FieldValues['tasa_dolar'];
  FormaLibre:= Ds.FieldValues['forma_libre'];
  LoginUsuario:= Ds.FieldValues['login_usuario'];
  TiempoIngreso:= Ds.FieldByName('tiempo_ingreso').AsDateTime;
  IdFacturacionCliente:= Ds.FieldValues['id_facturacion_cliente'];
  IdCobranza:= Ds.FieldValues['id_cobranza'];
  IdMovimientoInventario:= Ds.FieldValues['id_movimiento_inventario'];
  IdContadoCobranza := Ds.FieldValues['id_contado_cobranza'];
end;

function TFacturacionCliente.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,29);
  Params[0] := Numero;
  Params[1] := NumeroPedido;
  Params[2] := CodigoMoneda;
  Params[3] := CodigoCliente;
  Params[4] := CodigoVendedor;
  Params[5] := CodigoZona;
  Params[6] := CodigoEstacion;
  Params[7] := CodigoEsquemaPago;
  Params[8] := VarFromDateTime(FechaIngreso);
  Params[9] := VarFromDateTime(FechaRecepcion);
  Params[10] := VarFromDateTime(FechaContable);
  Params[11] := VarFromDateTime(FechaLibros);
  Params[12] := Concepto;
  Params[13] := Observaciones;
  Params[14] := MontoNeto;
  Params[15] := MontoDescuento;
  Params[16] := SubTotal;
  Params[17] := Impuesto;
  Params[18] := Total;
  Params[19] := PDescuento;
  Params[20] := PIva;
  Params[21] := TasaDolar;
  Params[22] := FormaLibre;
  Params[23] := LoginUsuario;
  Params[24] := VarFromDateTime(TiempoIngreso);
  Params[25] := IdFacturacionCliente;
  Params[26] := IdCobranza;
  Params[27] := IdMovimientoInventario;
  Params[28] := IdContadoCobranza;
  Result := Params;
end;

function TFacturacionCliente.GetCampos: TArraystring;
var
  Campos: TArraystring;
begin
  SetLength(Campos,11);
  Campos[0] := 'numero';
  Campos[1] := 'numero_pedido';
  Campos[2] := 'codigo_moneda';
  Campos[3] := 'codigo_cliente';
  Campos[4] := 'codigo_vendedor';
  Campos[5] := 'codigo_zona';
  Campos[6] := 'codigo_estacion';
  Campos[7] := 'codigo_esquema_pago';
  Campos[8] := 'concepto';
  Campos[9] := 'observaciones';
  Campos[10] := 'login_usuario';
  Result := Campos;
end;

function TFacturacionCliente.GetCondicion: string;
begin
  Result:= '"id_facturacion_cliente" =' + IntToStr(IdFacturacionCliente);
end;

procedure TFacturacionCliente.AntesInsertar;
var
  Consecutivo: TConsecutivo;
begin
  Consecutivo := TConsecutivo.Create;
  Numero := IntToStr(Consecutivo.ObtenerConsecutivo('numero_facturacion_cliente'));
  IdFacturacionCliente := Consecutivo.ObtenerConsecutivo('id_facturacion_cliente');
  if (not FormaLibre) then
  begin
    IdMovimientoInventario := Consecutivo.ObtenerConsecutivo('id_movimiento_inventario');
  end;
  IdCobranza := Consecutivo.ObtenerConsecutivo('id_cobranza');
end;

procedure TFacturacionCliente.AntesModificar;
begin

end;

procedure TFacturacionCliente.AntesEliminar;
begin

end;

procedure TFacturacionCliente.DespuesInsertar;
begin

end;

procedure TFacturacionCliente.DespuesModificar;
begin

end;

procedure TFacturacionCliente.DespuesEliminar;
begin

end;

constructor TFacturacionCliente.Create;
var
  ClavesPrimarias: TArraystring;
begin
  inherited Create;
  SetTabla('tbl_facturacion_clientes');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(29)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "numero"=$1, "numero_pedido"=$2, "codigo_moneda"=$3, "codigo_cliente"=$4, "codigo_vendedor"=$5, "codigo_zona"=$6, "codigo_estacion"=$7, "codigo_esquema_pago"=$8, "fecha_ingreso"=$9, ' +
                  '"fecha_recepcion"=$10, "fecha_contable"=$11, "fecha_libros"=$12, "concepto"=$13, "observaciones"=$14, "monto_neto"=$15, "monto_descuento"=$16, "sub_total"=$17, "impuesto"=$18, "total"=$19, "p_descuento"=$20, ' +
                  '"p_iva"=$21, "tasa_dolar"=$22, "forma_libre"=$23, "login_usuario"=$24, "tiempo_ingreso"=$25, "id_cobranza"=$27, "id_movimiento_inventario"=$28, "id_contado_cobranza"=$29' +
                  ' WHERE ("id_facturacion_cliente"=$26)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("id_facturacion_cliente"=$26)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"id_facturacion_cliente"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TFacturacionCliente.SetCodigoCliente(varCodigoCliente: string);
begin
 CodigoCliente := varCodigoCliente;
end;

procedure TFacturacionCliente.SetCodigoEsquemaPago(varCodigoEsquemaPago: string);
begin
  CodigoEsquemaPago := varCodigoEsquemaPago;
end;

procedure TFacturacionCliente.SetCodigoEstacion(varCodigoEstacion: string);
begin
  CodigoEstacion := varCodigoEstacion;
end;

procedure TFacturacionCliente.SetCodigoMoneda(varCodigoMoneda: string);
begin
  CodigoMoneda := varCodigoMoneda;
end;

procedure TFacturacionCliente.SetCodigoVendedor(varCodigoVendedor: string);
begin
  CodigoVendedor := varCodigoVendedor;
end;

procedure TFacturacionCliente.SetCodigoZona(varCodigoZona: string);
begin
  CodigoZona := varCodigoZona;
end;

procedure TFacturacionCliente.SetConcepto(varConcepto: string);
begin
  Concepto := varConcepto;
end;

procedure TFacturacionCliente.SetFechaContable(varFechaContable: TDate);
begin
  FechaContable := varFechaContable;
end;

procedure TFacturacionCliente.SetFechaIngreso(varFechaIngreso: TDate);
begin
  FechaIngreso := varFechaIngreso;
end;

procedure TFacturacionCliente.SetFechaLibros(varFechalibros: TDate);
begin
  FechaLibros := varFechalibros;
end;

procedure TFacturacionCliente.SetFechaRecepcion(varFechaRecepcion: TDate);
begin
  FechaRecepcion := varFechaRecepcion;
end;

procedure TFacturacionCliente.SetFormaLibre(varFormaLibre: Boolean);
begin
  FormaLibre := varFormaLibre;
end;

procedure TFacturacionCliente.SetIdCobranza(varIdCobranza: Integer);
begin
  IdCobranza := varIdCobranza;
end;

procedure TFacturacionCliente.SetIdContadoCobranza(varIdContadoCobranza: Integer);
begin
  IdContadoCobranza := varIdContadoCobranza;
end;

procedure TFacturacionCliente.SetIdFacturacionCliente(varIdFacturacionCliente: Integer);
begin
  IdFacturacionCliente := varIdFacturacionCliente;
end;

procedure TFacturacionCliente.SetIdMovimientoInventario(varIdMovimientoInventario: Integer);
begin
  IdMovimientoInventario := varIdMovimientoInventario;
end;

procedure TFacturacionCliente.SetImpuesto(varImpuesto: Double);
begin
  Impuesto := varImpuesto;
end;

procedure TFacturacionCliente.SetLoginUsuario(varLoginUsuario: string);
begin
  LoginUsuario := varLoginUsuario;
end;

procedure TFacturacionCliente.SetMontoDescuento(varMontoDescuento: Double);
begin
  MontoDescuento := varMontoDescuento;
end;

procedure TFacturacionCliente.SetMontoNeto(varMontoNeto: Double);
begin
  MontoNeto := varMontoNeto;
end;

procedure TFacturacionCliente.SetNumero(varNumero: string);
begin
  Numero := varNumero;
end;

procedure TFacturacionCliente.SetNumeroPedido(varNumeroPedido: string);
begin
  NumeroPedido := varNumeroPedido;
end;

procedure TFacturacionCliente.SetObservaciones(varObservaciones: string);
begin
  Observaciones := varObservaciones;
end;

procedure TFacturacionCliente.SetPDescuento(varPDescuento: Double);
begin
  PDescuento := varPDescuento;
end;

procedure TFacturacionCliente.SetPIva(varPIva: Double);
begin
  PIva := varPIva;
end;

procedure TFacturacionCliente.SetSubTotal(varSubTotal: Double);
begin
  SubTotal := varSubTotal;
end;

procedure TFacturacionCliente.SetTasaDolar(varTasaDolar: Double);
begin
  TasaDolar := varTasaDolar;
end;

procedure TFacturacionCliente.SetTiempoIngreso(varTiempodeIngreso: TDateTime);
begin
  TiempoIngreso := varTiempodeIngreso;
end;

procedure TFacturacionCliente.SetTotal(varTotal: Double);
begin
  Total := varTotal;
end;

function TFacturacionCliente.GetCodigoCliente: string;
begin
  Result := CodigoCliente
end;

function TFacturacionCliente.GetCodigoEsquemaPago: string;
begin
  Result := CodigoEsquemaPago;
end;

function TFacturacionCliente.GetCodigoEstacion: string;
begin
  Result := CodigoEstacion;
end;

function TFacturacionCliente.GetCodigoMoneda: string;
begin
  Result := CodigoMoneda;
end;

function TFacturacionCliente.GetCodigoVendedor: string;
begin
  Result := CodigoVendedor;
end;

function TFacturacionCliente.GetCodigoZona: string;
begin
  Result := CodigoZona;
end;

function TFacturacionCliente.GetConcepto: string;
begin
  Result := Concepto;
end;

function TFacturacionCliente.GetFechaContable: TDate;
begin
  Result := FechaContable;
end;

function TFacturacionCliente.GetFechaIngreso: TDate;
begin
  Result := FechaIngreso;
end;

function TFacturacionCliente.GetFechaLibros: TDate;
begin
  Result := FechaLibros;
end;

function TFacturacionCliente.GetFechaRecepcion: TDate;
begin
  Result := FechaRecepcion;
end;

function TFacturacionCliente.GetFormaLibre: Boolean;
begin
  Result := FormaLibre;
end;

function TFacturacionCliente.GetIdCobranza: Integer;
begin
  Result := IdCobranza;
end;

function TFacturacionCliente.GetIdContadoCobranza: Integer;
begin
  Result := IdContadoCobranza;
end;

function TFacturacionCliente.GetIdFacturacionCliente: Integer;
begin
  Result := IdFacturacionCliente;
end;

function TFacturacionCliente.GetIdMovimientoInventario: Integer;
begin
  Result := IdMovimientoInventario;
end;

function TFacturacionCliente.GetImpuesto: Double;
begin
  Result := Impuesto;
end;

function TFacturacionCliente.GetLoginUsuario: string;
begin
  Result := LoginUsuario;
end;

function TFacturacionCliente.GetMontoDescuento: Double;
begin
  Result := MontoDescuento;
end;

function TFacturacionCliente.GetMontoNeto: Double;
begin
  Result := MontoNeto;
end;

function TFacturacionCliente.GetNumero: string;
begin
  Result := Numero;
end;

function TFacturacionCliente.GetNumeroPedido: string;
begin
  Result := NumeroPedido;
end;

function TFacturacionCliente.GetObservaciones: string;
begin
  Result := Observaciones;
end;

function TFacturacionCliente.GetPDescuento: Double;
begin
  Result := PDescuento;
end;

function TFacturacionCliente.GetPIva: Double;
begin
  Result := PIva;
end;

function TFacturacionCliente.GetSubTotal: Double;
begin
  Result := SubTotal;
end;

function TFacturacionCliente.GetTasaDolar: Double;
begin
  Result := TasaDolar;
end;

function TFacturacionCliente.GetTiempoIngreso: TDateTime;
begin
  Result := TiempoIngreso;
end;

function TFacturacionCliente.GetTotal: Double;
begin
  Result := Total;
end;

function TFacturacionCliente.BuscarNumero: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("numero" = ' + QuotedStr(GetNumero) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

function TFacturacionCliente.BuscarCodigoCliente: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("codigo_cliente" = ' + QuotedStr(GetCodigoCliente) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

function TFacturacionCliente.BuscarCodigoVendedor: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("codigo_vendedor" = ' + QuotedStr(GetCodigoVendedor) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

end.
