unit ClsDetFacturacionProveedor;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TDetFacturacionProveedor }

  TDetFacturacionProveedor = class(TGenericoBD)
    private
      CodigoProducto: string;
      Referencia: string;
      Descripcion: string;
      Cantidad: Double;
      CantidadPendiente: Double;
      Costo: Double;
      PDescuento: Double;
      MontoDescuento: Double;
      Impuesto: Double;
      PIva: Double;
      Total: Double;
      IdFacturacionProveedor: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigoProducto(varCodigoProducto: string);
      procedure SetReferencia(varReferencia: string);
      procedure SetDescripcion(varDescripcion: string);
      procedure SetCantidad(varCantidad: Double);
      procedure SetCantidadPendiente(varCantidadPendiente: Double);
      procedure SetCosto(varCosto: Double);
      procedure SetPDescuento(varPDescuento: Double);
      procedure SetMontoDescuento(varMontoDescuento: Double);
      procedure SetImpuesto(varImpuesto: Double);
      procedure SetPIva(varPIva: Double);
      procedure SetTotal(varTotal: Double);
      procedure SetIdFacturacionProveedor(varIdFacturacionProveedor: Integer);
      function GetCodigoProducto: string;
      function GetReferencia: string;
      function GetDescripcion: string;
      function GetCantidad: Double;
      function GetCantidadPendiente: Double;
      function GetCosto: Double;
      function GetPDescuento: Double;
      function GetMontoDescuento: Double;
      function GetImpuesto: Double;
      function GetPIva: Double;
      function GetTotal: Double;
      function GetIdFacturacionProveedor: Integer;
      function ObtenerIdFacturacionProveedor(varIdFacturacionProveedor: Integer): TClientDataSet;
      function EliminarIdFacturacionProveedor(varIdFacturacionProveedor: Integer): Boolean;
  end;

implementation

uses DB;

{ TDetFacturacionProveedor }

procedure TDetFacturacionProveedor.Ajustar;
begin
  inherited;

end;

procedure TDetFacturacionProveedor.Mover(Ds: TClientDataSet);
begin
  CodigoProducto := Ds.FieldValues['codigo_producto'];
  Referencia := Ds.FieldValues['referencia'];
  Descripcion := Ds.FieldValues['descripcion'];
  Cantidad := Ds.FieldValues['cantidad'];
  CantidadPendiente := Ds.FieldValues['cantidad_pendiente'];
  Costo := Ds.FieldValues['costo'];
  PDescuento := Ds.FieldValues['p_descuento'];
  MontoDescuento := Ds.FieldValues['monto_descuento'];
  Impuesto := Ds.FieldValues['impuesto'];
  PIva := Ds.FieldValues['p_iva'];
  Total := Ds.FieldValues['total'];
  IdFacturacionProveedor := Ds.FieldValues['id_facturacion_proveedor'];
end;

function TDetFacturacionProveedor.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,12);
  Params[0] := CodigoProducto;
  Params[1] := Referencia;
  Params[2] := Descripcion;
  Params[3] := Cantidad;
  Params[4] := CantidadPendiente;
  Params[5] := Costo;
  Params[6] := PDescuento;
  Params[7] := MontoDescuento;
  Params[8] := Impuesto;
  Params[9] := PIva;
  Params[10] := Total;
  Params[11] := IdFacturacionProveedor;
  Result:= Params;
end;

function TDetFacturacionProveedor.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,3);
  Campos[0] := 'codigo_producto';
  Campos[1] := 'referencia';
  Campos[2] := 'descripcion';
  Result:= Campos;
end;

function TDetFacturacionProveedor.GetCondicion: string;
begin
  Result:= '"id_facturacion_proveedor" =' + IntToStr(IdFacturacionProveedor) + ' AND "codigo_producto" =' + QuotedStr(CodigoProducto);
end;

procedure TDetFacturacionProveedor.AntesEliminar;
begin

end;

procedure TDetFacturacionProveedor.AntesInsertar;
begin

end;

procedure TDetFacturacionProveedor.AntesModificar;
begin

end;

procedure TDetFacturacionProveedor.DespuesEliminar;
begin

end;

procedure TDetFacturacionProveedor.DespuesInsertar;
begin

end;

procedure TDetFacturacionProveedor.DespuesModificar;
begin

end;

constructor TDetFacturacionProveedor.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_det_facturacion_proveedores');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(12)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "referencia"=$2,"descripcion"=$3,"cantidad"=$4,"cantidad_pendiente"=$5,"costo"=$6,"p_descuento"=$7,"monto_descuento"=$8,"impuesto"=$9,"p_iva"=$10,"total"=$11 WHERE ("id_facturacion_cliente"=$12) AND ("codigo_producto"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("id_facturacion_proveedor"=$12) AND ("codigo_producto"=$1)');
  SetLength(ClavesPrimarias,2);
  ClavesPrimarias[0] := '"id_facturacion_proveedor"';
  ClavesPrimarias[1] := '"codigo_producto"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TDetFacturacionProveedor.SetCantidad(varCantidad: Double);
begin
  Cantidad := varCantidad;
end;

procedure TDetFacturacionProveedor.SetCantidadPendiente(varCantidadPendiente: Double);
begin
  CantidadPendiente := varCantidadPendiente;
end;

procedure TDetFacturacionProveedor.SetCodigoProducto(varCodigoProducto: string);
begin
  CodigoProducto := varCodigoProducto;
end;

procedure TDetFacturacionProveedor.SetDescripcion(varDescripcion: string);
begin
  Descripcion := varDescripcion;
end;

procedure TDetFacturacionProveedor.SetIdFacturacionProveedor(varIdFacturacionProveedor: Integer);
begin
  IdFacturacionProveedor := varIdFacturacionProveedor;
end;

procedure TDetFacturacionProveedor.SetImpuesto(varImpuesto: Double);
begin
  Impuesto := varImpuesto;
end;

procedure TDetFacturacionProveedor.SetMontoDescuento(varMontoDescuento: Double);
begin
  MontoDescuento := varMontoDescuento;
end;

procedure TDetFacturacionProveedor.SetPDescuento(varPDescuento: Double);
begin
  PDescuento := varPDescuento;
end;

procedure TDetFacturacionProveedor.SetPIva(varPIva: Double);
begin
  PIva := varPIva;
end;

procedure TDetFacturacionProveedor.SetCosto(varCosto: Double);
begin
  Costo := varCosto;
end;

procedure TDetFacturacionProveedor.SetReferencia(varReferencia: string);
begin
  Referencia := varReferencia;
end;

procedure TDetFacturacionProveedor.SetTotal(varTotal: Double);
begin
  Total := varTotal;
end;

function TDetFacturacionProveedor.GetCantidad: Double;
begin
  Result := Cantidad;
end;

function TDetFacturacionProveedor.GetCantidadPendiente: Double;
begin
  Result := CantidadPendiente;
end;

function TDetFacturacionProveedor.GetCodigoProducto: string;
begin
  Result := CodigoProducto;
end;

function TDetFacturacionProveedor.GetDescripcion: string;
begin
  Result := Descripcion;
end;

function TDetFacturacionProveedor.GetIdFacturacionProveedor: Integer;
begin
  Result := IdFacturacionProveedor;
end;

function TDetFacturacionProveedor.GetImpuesto: Double;
begin
  Result := Impuesto;
end;

function TDetFacturacionProveedor.GetMontoDescuento: Double;
begin
  Result := MontoDescuento;
end;

function TDetFacturacionProveedor.GetPDescuento: Double;
begin
  Result := PDescuento;
end;

function TDetFacturacionProveedor.GetPIva: Double;
begin
  Result := PIva;
end;

function TDetFacturacionProveedor.GetCosto: Double;
begin
  Result := Costo;
end;

function TDetFacturacionProveedor.GetReferencia: string;
begin
  Result := Referencia;
end;

function TDetFacturacionProveedor.GetTotal: Double;
begin
  Result := Total;
end;

function TDetFacturacionProveedor.ObtenerIdFacturacionProveedor(
  varIdFacturacionProveedor: Integer): TClientDataSet;
begin
  Result := ObtenerListaCondicionSQL(' WHERE "id_facturacion_proveedor" = ' + IntToStr(varIdFacturacionProveedor));
end;

function TDetFacturacionProveedor.EliminarIdFacturacionProveedor(
  varIdFacturacionProveedor: Integer): Boolean;
begin
  Result := EjecutarSQL('DELETE FROM ' + GetTabla + ' WHERE "id_facturacion_proveedor" = ' + IntToStr(varIdFacturacionProveedor));
end;

end.
