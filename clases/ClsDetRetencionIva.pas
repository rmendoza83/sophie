unit ClsDetRetencionIva;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TDetRetencionIva }

  TDetRetencionIva = class(TGenericoBD)
    private
      Numero: string;
      Desde: string;
      NumeroDocumento: string;
      CodigoRetencion: string;
      MontoRetencion: Double;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArraystring; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetNumero(varNumero: string);
      procedure SetDesde(varDesde: string);
      procedure SetNumeroDocumento(varNumeroDocumento: string);
      procedure SetCodigoRetencion(varCodigoRetencion: string);
      procedure SetMontoRetencion(varMontoRetencion: Double);
      function GetNumero: string;
      function GetDesde: string;
      function GetNumeroDocumento: string;
      function GetCodigoRetencion: string;
      function GetMontoRetencion: Double;
      function ObtenerNumero(varNumero: string): TClientDataSet;
      function EliminarNumero(varNumero: string): Boolean;
      function BuscarRetencionDocumento: Boolean;
  end;

implementation

uses
  DB,
  ClsConsecutivo;

{ TDetRetencionIva }

procedure TDetRetencionIva.Ajustar;
begin

end;

procedure TDetRetencionIva.Mover(Ds: TClientDataSet);
begin
  Numero := Ds.FieldValues['numero'];
  Desde := Ds.FieldValues['desde'];
  NumeroDocumento := Ds.FieldValues['numero_documento'];
  CodigoRetencion := Ds.FieldValues['codigo_retencion'];
  MontoRetencion := Ds.FieldValues['monto_retencion'];
end;

function TDetRetencionIva.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,5);
  Params[0] := Numero;
  Params[1] := Desde;
  Params[2] := NumeroDocumento;
  Params[3] := CodigoRetencion;
  Params[4] := MontoRetencion;
  Result := Params;
end;

function TDetRetencionIva.GetCampos: TArraystring;
var
  Campos: TArraystring;
begin
  SetLength(Campos,3);
  Campos[0] := 'numero';
  Campos[1] := 'desde';
  Campos[2] := 'numero_documento';
  Result := Campos;
end;

function TDetRetencionIva.GetCondicion: string;
begin
  Result:= '("numero"=' + QuotedStr(Numero) + ') AND ' +
					 '("desde"=' + QuotedStr(Desde) + ') AND ' +
					 '("numero_documento"=' + QuotedStr(NumeroDocumento) + ')';
end;

procedure TDetRetencionIva.AntesInsertar;
begin

end;

procedure TDetRetencionIva.AntesModificar;
begin

end;

procedure TDetRetencionIva.AntesEliminar;
begin

end;

procedure TDetRetencionIva.DespuesInsertar;
begin

end;

procedure TDetRetencionIva.DespuesModificar;
begin

end;

procedure TDetRetencionIva.DespuesEliminar;
begin

end;

constructor TDetRetencionIva.Create;
var
  ClavesPrimarias: TArraystring;
begin
  inherited Create;
  SetTabla('tbl_det_retenciones_iva');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(5)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "codigo_retencion"=$4, "monto_retencion"=$5 ' +
                  'WHERE (("numero"=$1) AND ("desde"=$2) AND ("numero_documento"=$3))');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE (("numero"=$1) AND ("desde"=$2) AND ("numero_documento"=$3))');
  SetLength(ClavesPrimarias,3);
  ClavesPrimarias[0] := '"numero"';
  ClavesPrimarias[1] := '"desde"';
  ClavesPrimarias[2] := '"numero_documento"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TDetRetencionIva.SetNumero(varNumero: string);
begin
  Numero := varNumero;
end;

procedure TDetRetencionIva.SetDesde(varDesde: string);
begin
  Desde := varDesde
end;

procedure TDetRetencionIva.SetNumeroDocumento(varNumeroDocumento: string);
begin
  NumeroDocumento := varNumeroDocumento;
end;

procedure TDetRetencionIva.SetCodigoRetencion(varCodigoRetencion: string);
begin
  CodigoRetencion := varCodigoRetencion;
end;

procedure TDetRetencionIva.SetMontoRetencion(varMontoRetencion: Double);
begin
  MontoRetencion := varMontoRetencion;
end;

function TDetRetencionIva.GetNumero: string;
begin
  Result := Numero;
end;

function TDetRetencionIva.GetDesde: string;
begin
  Result := Desde;
end;

function TDetRetencionIva.GetNumeroDocumento: string;
begin
  Result := NumeroDocumento;
end;

function TDetRetencionIva.GetCodigoRetencion: string;
begin
  Result := CodigoRetencion;
end;

function TDetRetencionIva.GetMontoRetencion: Double;
begin
  Result := MontoRetencion;
end;

function TDetRetencionIva.ObtenerNumero(varNumero: string): TClientDataSet;
begin
  Result := ObtenerListaCondicionSQL(' WHERE "numero" = ' + QuotedStr(varNumero));
end;

function TDetRetencionIva.EliminarNumero(varNumero: string): Boolean;
begin
  Result := EjecutarSQL('DELETE FROM ' + GetTabla + ' WHERE "numero" = ' + QuotedStr(varNumero));
end;

function TDetRetencionIva.BuscarRetencionDocumento: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("desde" = ' + QuotedStr(GetDesde) + ') AND ("numero_documento" = ' + QuotedStr(GetNumeroDocumento) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

end.
