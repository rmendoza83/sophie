unit ClsDetTransferenciaCliente;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type
  TDetTransferenciaCliente = class(TGenericoBD)
    private
      CodigoCliente: string;
      NumeroCuentaCliente: string;
      CodigoCuenta: string;
      CodigoTipoOperacion: string;
      Numero: string;
      Fecha: TDate;
      Monto: Double;
      IdOperacionBancaria: Integer;
      IdTransferenciaCliente: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigoCliente(varCodigoCliente: string);
      procedure SetNumeroCuentaCliente(varNumeroCuentaCliente: string);
      procedure SetCodigoCuenta(varCodigoCuenta: string);
      procedure SetCodigoTipoOperacion(varCodigoTipoOperacion: string);
      procedure SetNumero(varNumero: string);
      procedure SetFecha(varFecha: TDate);
      procedure SetMonto(varMonto: Double);
      procedure SetIdOperacionBancaria(varIdOperacionBancaria: Integer);
      procedure SetIdTransferenciaCliente(varIdTransferenciaCliente: Integer);
      function GetCodigoCliente: string;
      function GetNumeroCuentaCliente: string;
      function GetCodigoCuenta: string;
      function GetCodigoTipoOperacion: string;
      function GetNumero: string;
      function GetFecha: TDate;
      function GetMonto: Double;
      function GetIdOperacionBancaria: Integer;
      function GetIdTransferenciaCliente: Integer;
      function ObtenerIdTransferenciaCliente(varIdTransferenciaCliente: Integer): TClientDataSet;
      function EliminarIdTransferenciaCliente(varIdTransferenciaCliente: Integer): Boolean;
  end;

implementation

uses
  DB,
  ClsConsecutivo;

{ TDetTransferenciaCliente }

procedure TDetTransferenciaCliente.Ajustar;
begin

end;

procedure TDetTransferenciaCliente.Mover(Ds: TClientDataSet);
begin
  CodigoCliente := Ds.FieldValues['codigo_cliente'];
  NumeroCuentaCliente := Ds.FieldValues['numero_cuenta_cliente'];
  CodigoCuenta := Ds.FieldValues['codigo_cuenta'];
  CodigoTipoOperacion := Ds.FieldValues['codigo_tipo_operacion_bancaria'];
  Numero := Ds.FieldValues['numero'];
  Fecha := Ds.FieldByName('fecha').AsDateTime;
  Monto := Ds.FieldValues['monto'];
  IdOperacionBancaria := Ds.FieldValues['id_operacion_bancaria'];
  IdTransferenciaCliente := Ds.FieldValues['id_transferencia_cliente'];
end;

function TDetTransferenciaCliente.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,9);
  Params[0] := CodigoCliente;
  Params[1] := NumeroCuentaCliente;
  Params[2] := CodigoCuenta;
  Params[3] := CodigoTipoOperacion;
  Params[4] := Numero;
  Params[5] := VarFromDateTime(Fecha) ;
  Params[6] := Monto;
  Params[7] := IdOperacionBancaria;
  Params[8] := IdTransferenciaCliente;
  Result := Params;
end;

function TDetTransferenciaCliente.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,5);
  Campos[0] := 'codigo_cliente';
  Campos[1] := 'numero_cuenta_cliente';
  Campos[2] := 'codigo_cuenta';
  Campos[3] := 'codigo_tipo_operacion_bancaria';
  Campos[4] := 'numero';
  Result := Campos;
end;

function TDetTransferenciaCliente.GetCondicion: string;
begin
  Result := '"codigo_cliente" = ' + QuotedStr(CodigoCliente) + 'AND ' +
            '"numero_cuenta_cliente" =' + QuotedStr(NumeroCuentaCliente) + ' AND ' +
            '"id_transferencia_cliente" =' + IntToStr(IdTransferenciaCliente);
end;

procedure TDetTransferenciaCliente.AntesInsertar;
begin

end;

procedure TDetTransferenciaCliente.AntesModificar;
begin

end;

procedure TDetTransferenciaCliente.AntesEliminar;
begin

end;

procedure TDetTransferenciaCliente.DespuesInsertar;
begin

end;

procedure TDetTransferenciaCliente.DespuesEliminar;
begin

end;

procedure TDetTransferenciaCliente.DespuesModificar;
begin

end;

constructor TDetTransferenciaCliente.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_det_transferencias_clientes');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(9)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "codigo_cuenta"=$3, "codigo_tipo_operacion_bancaria"=$4, "numero"=$5, "fecha"=$6, "monto"=$7, "id_operacion_bancaria"=$8 WHERE ("codigo_cliente"=$1) AND ("numero_cuenta_cliente"=$2) AND ("id_transferencia_cliente" =$9)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("codigo_cliente"=$1) AND ("numero_cuenta_cliente"=$2) AND ("id_transferencia_cliente" =$9)');
  SetLength(ClavesPrimarias,3);
  ClavesPrimarias[0] := '"codigo_cliente"';
  ClavesPrimarias[1] := '"numero_cuenta_cliente"';
  ClavesPrimarias[2] := '"id_transferencia_cliente"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TDetTransferenciaCliente.SetCodigoCliente(varCodigoCliente: string);
begin
  CodigoCliente := varCodigoCliente;
end;

procedure TDetTransferenciaCliente.SetNumeroCuentaCliente(varNumeroCuentaCliente: string);
begin
  NumeroCuentaCliente := varNumeroCuentaCliente;
end;

procedure TDetTransferenciaCliente.SetCodigoCuenta(varCodigoCuenta: string);
begin
  CodigoCuenta := varCodigoCuenta;
end;

procedure TDetTransferenciaCliente.SetCodigoTipoOperacion(varCodigoTipoOperacion: string);
begin
  CodigoTipoOperacion := varCodigoTipoOperacion;
end;

procedure TDetTransferenciaCliente.SetNumero(varNumero: string);
begin
  Numero := varNumero;
end;

procedure TDetTransferenciaCliente.SetFecha(varFecha: TDate);
begin
  Fecha := varFecha;
end;

procedure TDetTransferenciaCliente.SetMonto(varMonto: Double);
begin
  Monto := varMonto;
end;

procedure TDetTransferenciaCliente.SetIdOperacionBancaria(varIdOperacionBancaria: Integer);
begin
  IdOperacionBancaria := varIdOperacionBancaria;
end;

procedure TDetTransferenciaCliente.SetIdTransferenciaCliente(varIdTransferenciaCliente: Integer);
begin
  IdTransferenciaCliente := varIdTransferenciaCliente;
end;

function TDetTransferenciaCliente.GetCodigoCliente: string;
begin
  Result := CodigoCliente;
end;

function TDetTransferenciaCliente.GetNumeroCuentaCliente: string;
begin
  Result := NumeroCuentaCliente;
end;

function TDetTransferenciaCliente.GetCodigoCuenta: string;
begin
  Result := CodigoCuenta;
end;

function TDetTransferenciaCliente.GetCodigoTipoOperacion: string;
begin
  Result := CodigoTipoOperacion;
end;

function TDetTransferenciaCliente.GetNumero: string;
begin
  Result := Numero;
end;

function TDetTransferenciaCliente.GetFecha: TDate;
begin
  Result := Fecha;
end;

function TDetTransferenciaCliente.GetMonto: Double;
begin
  Result := Monto;
end;

function TDetTransferenciaCliente.GetIdOperacionBancaria: Integer;
begin
  Result := IdOperacionBancaria;
end;

function TDetTransferenciaCliente.GetIdTransferenciaCliente: Integer;
begin
  Result := IdTransferenciaCliente;
end;

function TDetTransferenciaCliente.ObtenerIdTransferenciaCliente(varIdTransferenciaCliente: Integer): TClientDataSet;
begin
  Result := ObtenerListaCondicionSQL(' WHERE "id_transferencia_cliente" = ' + IntToStr(varIdTransferenciaCliente));
end;

function TDetTransferenciaCliente.EliminarIdTransferenciaCliente(varIdTransferenciaCliente: Integer): Boolean;
begin
  Result := EjecutarSQL('DELETE FROM ' + GetTabla + ' WHERE "id_transferencia_cliente" = ' + IntToStr(varIdTransferenciaCliente));
end;

end.
