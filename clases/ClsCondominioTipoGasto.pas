unit ClsCondominioTipoGasto;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TCondominioTipoGasto }

  TCondominioTipoGasto = class(TGenericoBD)
    private
      Codigo: string;
      Descripcion: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigo(varCodigo: string);
      procedure SetDescripcion(varDescripcion: string);
      function GetCodigo: string;
      function GetDescripcion: string;
  end;

var
  Clase: TCondominioTipoGasto;

implementation

{ TCondominioTipoGasto }

procedure TCondominioTipoGasto.Ajustar;
begin

end;

procedure TCondominioTipoGasto.Mover(Ds: TClientDataSet);
begin
  Codigo := Ds.FieldValues['codigo'];
  Descripcion := Ds.FieldValues['descripcion'];
end;

function TCondominioTipoGasto.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,2);
  Params[0] := Codigo;
  Params[1] := Descripcion;
  Result := Params;
end;

function TCondominioTipoGasto.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,2);
  Campos[0] := 'codigo';
  Campos[1] := 'descripcion';
  Result := Campos;
end;

function TCondominioTipoGasto.GetCondicion: string;
begin
  Result := '"codigo" = ' + QuotedStr(Codigo);
end;

procedure TCondominioTipoGasto.AntesEliminar;
begin

end;

procedure TCondominioTipoGasto.AntesInsertar;
begin

end;

procedure TCondominioTipoGasto.AntesModificar;
begin

end;

procedure TCondominioTipoGasto.DespuesEliminar;
begin

end;

procedure TCondominioTipoGasto.DespuesInsertar;
begin

end;

procedure TCondominioTipoGasto.DespuesModificar;
begin

end;

constructor TCondominioTipoGasto.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('tbl_condominio_tipos_gastos');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(2)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "descripcion"=$2 WHERE ("codigo"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("codigo"=$1)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"codigo"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TCondominioTipoGasto.SetCodigo(varCodigo: string);
begin
  Codigo := varCodigo;
end;

procedure TCondominioTipoGasto.SetDescripcion(varDescripcion: string);
begin
  Descripcion := varDescripcion;
end;

function TCondominioTipoGasto.GetCodigo: string;
begin
  Result := Codigo;
end;

function TCondominioTipoGasto.GetDescripcion: string;
begin
  Result := Descripcion;
end;

end.
