unit ClsMovimientoInventario;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TMovimientoInventario }

  TMovimientoInventario = class(TGenericoBD)
    private
      Numero: string;
      CodigoZona: string;
      CodigoEstacionSalida: string;
      CodigoEstacionEntrada: string;
      FechaIngreso: TDate;
      FechaRecepcion: TDate;
      FechaContable: TDate;
      FechaLibros: TDate;
      Concepto: string;
      Observaciones: string;
      LoginUsuario: string;
      TiempoIngreso: TDateTime;
      IdMovimientoInventario: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArraystring; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetNumero(varNumero: string);
      procedure SetCodigoZona(varCodigoZona: string);
      procedure SetCodigoEstacionSalida(varCodigoEstacionSalida: string);
      procedure SetCodigoEstacionEntrada(varCodigoEstacionEntrada: string);
      procedure SetFechaIngreso(varFechaIngreso: TDate);
      procedure SetFechaRecepcion(varFechaRecepcion: TDate);
      procedure SetFechaContable(varFechaContable: TDate);
      procedure SetFechaLibros(varFechaLibros: TDate);
      procedure SetConcepto(varConcepto: string);
      procedure SetObservaciones(varObservaciones: string);
      procedure SetLoginUsuario(varLoginUsuario: string);
      procedure SetTiempoIngreso(varTiempoIngreso: TDateTime);
      procedure SetIdMovimientoInventario(varIdMovimientoInventario: Integer);
      function GetNumero: string;
      function GetCodigoZona: string;
      function GetCodigoEstacionSalida: string;
      function GetCodigoEstacionEntrada: string;
      function GetFechaIngreso: TDate;
      function GetFechaRecepcion: TDate;
      function GetFechaContable: TDate;
      function GetFechaLibros: TDate;
      function GetConcepto: string;
      function GetObservaciones: string;
      function GetLoginUsuario: string;
      function GetTiempoIngreso: TDateTime;
      function GetIdMovimientoInventario: Integer;
      function BuscarNumero: Boolean;
  end;

implementation

uses
  DB,
  ClsConsecutivo;

{ TMovimientoInventario }

procedure TMovimientoInventario.Ajustar;
begin

end;

procedure TMovimientoInventario.Mover(Ds: TClientDataSet);
begin
  Numero := Ds.FieldValues['numero'];
  CodigoZona := Ds.FieldValues['codigo_zona'];
  CodigoEstacionSalida := Ds.FieldValues['codigo_estacion_salida'];
  CodigoEstacionEntrada := Ds.FieldValues['codigo_estacion_entrada'];
  FechaIngreso := Ds.FieldValues['fecha_ingreso'];
  FechaRecepcion := Ds.FieldValues['fecha_recepcion'];
  FechaContable := Ds.FieldValues['fecha_contable'];
  FechaLibros := Ds.FieldValues['fecha_libros'];
  Concepto := Ds.FieldValues['concepto'];
  Observaciones := Ds.FieldValues['observaciones'];
  LoginUsuario := Ds.FieldValues['login_usuario'];
  TiempoIngreso := Ds.FieldValues['tiempo_ingreso'];
  IdMovimientoInventario := Ds.FieldValues['id_movimiento_inventario'];
end;

function TMovimientoInventario.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,13);
  Params[0] := Numero;
  Params[1] := CodigoZona;
  Params[2] := CodigoEstacionSalida;
  Params[3] := CodigoEstacionEntrada;
  Params[4] := VarFromDateTime(FechaIngreso);
  Params[5] := VarFromDateTime(FechaRecepcion);
  Params[6] := VarFromDateTime(FechaContable);
  Params[7] := VarFromDateTime(FechaLibros);
  Params[8] := Concepto;
  Params[9] := Observaciones;
  Params[10] := LoginUsuario;
  Params[11] := VarFromDateTime(TiempoIngreso);
  Params[12] := IdMovimientoInventario;
  Result := Params;
end;

function TMovimientoInventario.GetCampos: TArraystring;
var
  Campos: TArraystring;
begin
  SetLength(Campos,8);
  Campos[0] := 'numero';
  Campos[1] := 'codigo_zona';
  Campos[2] := 'codigo_producto';
  Campos[3] := 'codigo_estacion_salida';
  Campos[4] := 'codigo_estacion_entrada';
  Campos[5] := 'concepto';
  Campos[6] := 'observaciones';
  Campos[7] := 'login_usuario';
  Result := Campos;
end;

function TMovimientoInventario.GetCondicion: string;
begin
  Result:= '"id_movimiento_inventario" = ' + IntToStr(IdMovimientoInventario);
end;

procedure TMovimientoInventario.AntesInsertar;
var
  Consecutivo: TConsecutivo;
begin
  Consecutivo := TConsecutivo.Create;
  Numero := IntToStr(Consecutivo.ObtenerConsecutivo('numero_movimiento_inventario'));
  IdMovimientoInventario := Consecutivo.ObtenerConsecutivo('id_movimiento_inventario');
end;

procedure TMovimientoInventario.AntesModificar;
begin

end;

procedure TMovimientoInventario.AntesEliminar;
begin

end;

procedure TMovimientoInventario.DespuesInsertar;
begin

end;

procedure TMovimientoInventario.DespuesModificar;
begin

end;

procedure TMovimientoInventario.DespuesEliminar;
begin

end;

constructor TMovimientoInventario.Create;
var
  ClavesPrimarias: TArraystring;
begin
  inherited Create;
  SetTabla('tbl_movimientos_inventario');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(13)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "numero"=$1,"codigo_zona"=$2,"codigo_estacion_salida"=$3,"codigo_estacion_entrada"=$4,"fecha_ingreso"=$5,"fecha_recepcion"=$6,"fecha_contable"=$7, ' +
                   '"fecha_libros"=$8,"concepto"=$9,"observaciones"=$10,"login_usuario"=$11,"tiempo_ingreso"=$12 ' +
                   ' WHERE ("id_movimiento_inventario"=$13)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("id_movimiento_inventario"=$13)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"id_movimiento_inventario"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TMovimientoInventario.SetNumero(varNumero: string);
begin
  Numero := varNumero;
end;

procedure TMovimientoInventario.SetCodigoZona(varCodigoZona: string);
begin
  CodigoZona := varCodigoZona;
end;

procedure TMovimientoInventario.SetCodigoEstacionSalida(varCodigoEstacionSalida: string);
begin
  CodigoEstacionSalida := varCodigoEstacionSalida;
end;

procedure TMovimientoInventario.SetCodigoEstacionEntrada(varCodigoEstacionEntrada: string);
begin
  CodigoEstacionEntrada := varCodigoEstacionEntrada;
end;

procedure TMovimientoInventario.SetFechaIngreso(varFechaIngreso: TDate);
begin
  FechaIngreso := varFechaIngreso;
end;

procedure TMovimientoInventario.SetFechaRecepcion(varFechaRecepcion: TDate);
begin
  FechaRecepcion := varFechaRecepcion;
end;

procedure TMovimientoInventario.SetFechaContable(varFechaContable: TDate);
begin
  FechaContable := varFechaContable;
end;

procedure TMovimientoInventario.SetFechaLibros(varFechaLibros: TDate);
begin
  FechaLibros := varFechaLibros;
end;

procedure TMovimientoInventario.SetConcepto(varConcepto: string);
begin
  Concepto := varConcepto;
end;

procedure TMovimientoInventario.SetObservaciones(varObservaciones: string);
begin
  Observaciones := varObservaciones;
end;

procedure TMovimientoInventario.SetLoginUsuario(varLoginUsuario: string);
begin
  LoginUsuario := varLoginUsuario;
end;

procedure TMovimientoInventario.SetTiempoIngreso(varTiempoIngreso: TDateTime);
begin
  TiempoIngreso := varTiempoIngreso;
end;

procedure TMovimientoInventario.SetIdMovimientoInventario(varIdMovimientoInventario: Integer);
begin
  IdMovimientoInventario := varIdMovimientoInventario;
end;

function TMovimientoInventario.GetNumero: string;
begin
  Result := Numero;
end;

function TMovimientoInventario.GetCodigoZona: string;
begin
  Result := CodigoZona; 
end;

function TMovimientoInventario.GetCodigoEstacionSalida: string;
begin
  Result := CodigoEstacionSalida;
end;

function TMovimientoInventario.GetCodigoEstacionEntrada: string;
begin
  Result := CodigoEstacionEntrada;
end;

function TMovimientoInventario.GetFechaIngreso: TDate;
begin
  Result := FechaIngreso;
end;

function TMovimientoInventario.GetFechaRecepcion: TDate;
begin
  Result := FechaRecepcion;
end;

function TMovimientoInventario.GetFechaContable: TDate;
begin
  Result := FechaContable;
end;

function TMovimientoInventario.GetFechaLibros: TDate;
begin
  Result := FechaLibros;
end;

function TMovimientoInventario.GetConcepto: string;
begin
  Result := Concepto;
end;

function TMovimientoInventario.GetObservaciones: string;
begin
  Result := Observaciones;
end;

function TMovimientoInventario.GetLoginUsuario: string;
begin
  Result := LoginUsuario;
end;

function TMovimientoInventario.GetTiempoIngreso: TDateTime;
begin
  Result := TiempoIngreso;
end;

function TMovimientoInventario.GetIdMovimientoInventario: Integer;
begin
  Result := IdMovimientoInventario;
end;

function TMovimientoInventario.BuscarNumero: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("numero" = ' + QuotedStr(GetNumero) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

end.
