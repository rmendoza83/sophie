unit ClsProducto;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TProducto }

  TProducto = class(TGenericoBD)
    private
      Codigo: string;
      Referencia: string;
      Descripcion:string;
      CodigoDivision: string;
      CodigoLinea: string;
      CodigoFamilia: string;
      CodigoClase: string;
      CodigoManufactura: string;
      CodigoArancel: string;
      CodigoUnidad: string;
      FechaIngreso: Tdate;
      Peso: Double;
      Volumen: Double;
      PIvaVenta: Double;
      PIvaCompra: Double;
      Precio: Double;
      PDescuento: Double;
      CostoAnterior: Double;
      CostoActual: Double;
      FechaUCompra: Tdate;
      UsCostoAnterior: Double;
      UsCostoActual: Double;
      CostoFob: Double;
      Ubicacion: string;
      Inventario: Boolean;
      UniqueId: Integer;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArraystring; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigo(varCodigo: string);
      procedure SetReferencia(varReferencia: string);
      procedure SetDescripcion(varDescricion:string);
      procedure SetCodigoDivision(varCodigoDivision: string);
      procedure SetCodigoLinea(varCodigoLinea: string);
      procedure SetCodigoFamilia(varCodigoFamilia: string);
      procedure SetCodigoClase(varCodigoClase: string);
      procedure SetCodigoManufactura(varCodigoManufactura: string);
      procedure SetCodigoArancel(varCodigoArancel: string);
      procedure SetCodigoUnidad(varCodigoUnidad: string);
      procedure SetFechaIngreso(varFechaIngreso: Tdate);
      procedure SetPeso(varPeso: Double);
      procedure SetVolumen(varVolumen: Double);
      procedure SetPIvaVenta(varPIvaVenta: Double);
      procedure SetPIvaCompra(varPIvaCompra: Double);
      procedure SetPrecio(varPrecio: Double);
      procedure SetPDescuento(varPDescuento: Double);
      procedure SetCostoAnterior(varCostoAnterior: Double);
      procedure SetCostoActual(varCostoActual: Double);
      procedure SetFechaUCompra(varFechaUCompra: Tdate);
      procedure SetUsCostoAnterior(varUsCostoAnterior: Double);
      procedure SetUsCostoActual(varUsCostoActual: Double);
      procedure SetCostoFob(varCostoFob: Double);
      procedure SetUbicacion(varUbicacion: string);
      procedure SetInventario(varInventario: Boolean);
      procedure SetUniqueId(varUniqueId: Integer);
      function GetCodigo: string;
      function GetReferencia: string;
      function GetDescripcion:string;
      function GetCodigoDivision: string;
      function GetCodigoLinea: string;
      function GetCodigoFamilia: string;
      function GetCodigoClase: string;
      function GetCodigoManufactura: string;
      function GetCodigoArancel: string;
      function GetCodigoUnidad: string;
      function GetFechaIngreso: Tdate;
      function GetPeso: Double;
      function GetVolumen: Double;
      function GetPIvaVenta: Double;
      function GetPIvaCompra: Double;
      function GetPrecio: Double;
      function GetPDescuento: Double;
      function GetCostoAnterior: Double;
      function GetCostoActual: Double;
      function GetFechaUCompra: Tdate;
      function GetUsCostoAnterior: Double;
      function GetUsCostoActual: Double;
      function GetCostoFob: Double;
      function GetUbicacion: string;
      function GetInventario: Boolean;
      function GetUniqueId: Integer;
      function ObtenerCombo: TClientDataSet;
      function BuscarCodigo: Boolean;
  end;

implementation

uses
  DB, ClsConsecutivo;

{ TProducto }

procedure TProducto.Ajustar;
begin
  inherited;

end;


procedure TProducto.Mover(Ds: TClientDataSet);
begin
  Codigo := Ds.FieldValues['codigo'];
  Referencia := Ds.FieldValues['referencia'];
  Descripcion := Ds.FieldValues['descripcion'];
  CodigoDivision := Ds.FieldValues['codigo_division'];
  CodigoLinea := Ds.FieldValues['codigo_linea'];
  CodigoFamilia := Ds.FieldValues['codigo_familia'];
  CodigoClase := Ds.FieldValues['codigo_clase'];
  CodigoManufactura := Ds.FieldValues['codigo_manufactura'];
  CodigoArancel := Ds.FieldValues['codigo_arancel'];
  CodigoUnidad := Ds.FieldValues['codigo_unidad'];
  FechaIngreso := Ds.FieldByName('fecha_ingreso').AsDateTime;
  Peso := Ds.FieldValues['peso'];
  Volumen := Ds.FieldValues['volumen'];
  PIvaVenta := Ds.FieldValues['p_iva_venta'];
  PIvaCompra := Ds.FieldValues['p_iva_compra'];
  Precio := Ds.FieldValues['precio'];
  PDescuento := Ds.FieldValues['p_descuento'];
  CostoAnterior := Ds.FieldValues['costo_anterior'];
  CostoActual := Ds.FieldValues['costo_actual'];
  FechaUCompra := Ds.FieldByName('fecha_u_compra').AsDateTime;
  UsCostoAnterior := Ds.FieldValues['us_costo_anterior'];
  UsCostoActual := Ds.FieldValues['us_costo_actual'];
  CostoFob := Ds.FieldValues['costo_fob'];
  Ubicacion := Ds.FieldValues['ubicacion'];
  Inventario := Ds.FieldValues['inventario'];
  UniqueId := Ds.FieldValues['unique_id'];
end;

function TProducto.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,26);
  Params[0] := Codigo;
  Params[1] := Referencia;
  Params[2] := Descripcion;
  Params[3] := CodigoDivision;
  Params[4] := CodigoLinea;
  Params[5] := CodigoFamilia;
  Params[6] := CodigoClase;
  Params[7] := CodigoManufactura;
  Params[8] := CodigoArancel;
  Params[9] := CodigoUnidad;
  Params[10] := VarFromDateTime(FechaIngreso);
  Params[11] := Peso;
  Params[12] := Volumen;
  Params[13] := PIvaVenta;
  Params[14] := PIvaCompra;
  Params[15] := Precio;
  Params[16] := PDescuento;
  Params[17] := CostoAnterior;
  Params[18] := CostoActual;
  Params[19] := VarFromDateTime(FechaUCompra);
  Params[20] := UsCostoAnterior;
  Params[21] := UsCostoActual;
  Params[22] := CostoFob;
  Params[23] := Ubicacion;
  Params[24] := Inventario;
  Params[25] := UniqueId;
  Result := Params;
end;

function TProducto.GetCampos: TArraystring;
var
  Campos: TArraystring;
begin
  SetLength(Campos,11);
  Campos[0] := 'codigo';
  Campos[1] := 'referencia';
  Campos[2] := 'descripcion';
  Campos[3] := 'codigo_division';
  Campos[4] := 'codigo_linea';
  Campos[5] := 'codigo_familia';
  Campos[6] := 'codigo_clase';
  Campos[7] := 'codigo_manufactura';
  Campos[8] := 'codigo_arancel';
  Campos[9] := 'codigo_unidad';
  Campos[10] :='ubicacion';
  Result := Campos;
end;

function TProducto.GetCondicion: string;
begin
  Result:= '"unique_id" = ' + IntToStr(UniqueId); 
end;

procedure TProducto.AntesInsertar;
var
  Consecutivo: TConsecutivo;
begin
  Consecutivo := TConsecutivo.Create;
  UniqueId := Consecutivo.ObtenerConsecutivo('id_productos');
  FechaIngreso := Date;
  FechaUCompra := Date;
end;

procedure TProducto.AntesModificar;
begin

end;

procedure TProducto.AntesEliminar;
begin

end;

procedure TProducto.DespuesInsertar;
begin

end;

procedure TProducto.DespuesModificar;
begin

end;

procedure TProducto.DespuesEliminar;
begin

end;

constructor TProducto.Create;
var
  ClavesPrimarias: TArraystring;
begin
  inherited Create;
  SetTabla('tbl_productos');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(26)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "referencia"=$2,"descripcion"=$3,"codigo_division"=$4,"codigo_linea"=$5,"codigo_familia"=$6,"codigo_clase"=$7,"codigo_manufactura"=$8,"codigo_arancel"=$9,"codigo_unidad"=$10,' +
                  '"fecha_ingreso"=$11, "peso"=$12,"volumen"=$13,"p_iva_venta"=$14,"p_iva_compra"=$15,"precio"=$16,"p_descuento"=$17,"costo_anterior"=$18,"costo_actual"=$19,"fecha_u_compra"=$20,' +
                  '"us_costo_anterior"=$21,"us_costo_actual"=$22,"costo_fob"=$23,"ubicacion"=$24,"inventario"=$25 ' +
                  'WHERE ("unique_id"=$26)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("unique_id"=$26)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"unique_id"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TProducto.SetCodigo(varCodigo: string);
begin
  Codigo := varCodigo;
end;

procedure TProducto.SetCodigoArancel(varCodigoArancel: string);
begin
  CodigoArancel := varCodigoArancel;
end;

procedure TProducto.SetCodigoClase(varCodigoClase: string);
begin
  CodigoClase := varCodigoClase;
end;

procedure TProducto.SetCodigoDivision(varCodigoDivision: string);
begin
  CodigoDivision := varCodigoDivision;
end;

procedure TProducto.SetCodigoFamilia(varCodigoFamilia: string);
begin
  CodigoFamilia := varCodigoFamilia;
end;

procedure TProducto.SetCodigoLinea(varCodigoLinea: string);
begin
  CodigoLinea := varCodigoLinea;
end;

procedure TProducto.SetCodigoManufactura(varCodigoManufactura: string);
begin
  CodigoManufactura := varCodigoManufactura;
end;

procedure TProducto.SetCodigoUnidad(varCodigoUnidad: string);
begin
  CodigoUnidad := varCodigoUnidad;
end;

procedure TProducto.SetCostoActual(varCostoActual: Double);
begin
  CostoActual := varCostoActual;
end;

procedure TProducto.SetCostoAnterior(varCostoAnterior: Double);
begin
  CostoAnterior := varCostoAnterior;
end;

procedure TProducto.SetCostoFob(varCostoFob: Double);
begin
  CostoFob := varCostoFob;
end;

procedure TProducto.SetDescripcion(varDescricion: string);
begin
  Descripcion := varDescricion;
end;

procedure TProducto.SetFechaIngreso(varFechaIngreso: Tdate);
begin
  FechaIngreso := varFechaIngreso;
end;

procedure TProducto.SetFechaUCompra(varFechaUCompra: Tdate);
begin
  FechaUCompra := varFechaUCompra;
end;

procedure TProducto.SetInventario(varInventario: Boolean);
begin
  Inventario := varInventario;
end;

procedure TProducto.SetPDescuento(varPDescuento: Double);
begin
  PDescuento := varPDescuento;
end;

procedure TProducto.SetPeso(varPeso: Double);
begin
  Peso := varPeso;
end;

procedure TProducto.SetPIvaCompra(varPIvaCompra: Double);
begin
  PIvaCompra := varPIvaCompra;
end;

procedure TProducto.SetPIvaVenta(varPIvaVenta: Double);
begin
  PIvaVenta := varPIvaVenta;
end;

procedure TProducto.SetPrecio(varPrecio: Double);
begin
  Precio := varPrecio;
end;

procedure TProducto.SetReferencia(varReferencia: string);
begin
  Referencia := varReferencia;
end;

procedure TProducto.SetUbicacion(varUbicacion: string);
begin
  Ubicacion := varUbicacion;
end;

procedure TProducto.SetUniqueId(varUniqueId: Integer);
begin
  UniqueId := varUniqueId;
end;

procedure TProducto.SetUsCostoActual(varUsCostoActual: Double);
begin
  UsCostoActual := varUsCostoActual;
end;

procedure TProducto.SetUsCostoAnterior(varUsCostoAnterior: Double);
begin
  UsCostoAnterior := UsCostoAnterior;
end;

procedure TProducto.SetVolumen(varVolumen: Double);
begin
  Volumen := varVolumen;
end;

function TProducto.GetCodigo: string;
begin
  Result := Codigo;
end;

function TProducto.GetCodigoArancel: string;
begin
  Result := CodigoArancel;
end;

function TProducto.GetCodigoClase: string;
begin
  Result := CodigoClase;
end;

function TProducto.GetCodigoDivision: string;
begin
  Result := CodigoDivision;
end;

function TProducto.GetCodigoFamilia: string;
begin
  Result := CodigoFamilia;
end;

function TProducto.GetCodigoLinea: string;
begin
  Result := CodigoLinea;
end;

function TProducto.GetCodigoManufactura: string;
begin
  Result := CodigoManufactura;
end;

function TProducto.GetCodigoUnidad: string;
begin
  Result := CodigoUnidad;
end;

function TProducto.GetCostoActual: Double;
begin
  Result := CostoActual;
end;

function TProducto.GetCostoAnterior: Double;
begin
  Result := CostoAnterior;
end;

function TProducto.GetCostoFob: Double;
begin
  Result := CostoFob;
end;

function TProducto.GetDescripcion: string;
begin
  Result := Descripcion;
end;

function TProducto.GetFechaIngreso: Tdate;
begin
  Result := FechaIngreso;
end;

function TProducto.GetFechaUCompra: Tdate;
begin
  Result := FechaUCompra;
end;

function TProducto.GetInventario: Boolean;
begin
  Result := Inventario;
end;

function TProducto.GetPDescuento: Double;
begin
  Result := PDescuento;
end;

function TProducto.GetPeso: Double;
begin
  Result := Peso;
end;

function TProducto.GetPIvaCompra: Double;
begin
  Result := PIvaCompra;
end;

function TProducto.GetPIvaVenta: Double;
begin
  Result := PIvaVenta;
end;

function TProducto.GetPrecio: Double;
begin
  Result := Precio;
end;

function TProducto.GetReferencia: string;
begin
  Result := Referencia;
end;

function TProducto.GetUbicacion: string;
begin
  Result := Ubicacion;
end;

function TProducto.GetUniqueId: Integer;
begin
  Result := UniqueId;
end;

function TProducto.GetUsCostoActual: Double;
begin
  Result := UsCostoActual;
end;

function TProducto.GetUsCostoAnterior: Double;
begin
  Result := UsCostoAnterior;
end;

function TProducto.GetVolumen: Double;
begin
  Result := Volumen;
end;

function TProducto.ObtenerCombo: TClientDataSet;
begin
  Result := BD.EjecutarQueryCliDs('SELECT "codigo", "descripcion" FROM ' + GetTabla + ' ORDER BY "codigo"');
end;

function TProducto.BuscarCodigo: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("codigo" = ' + QuotedStr(GetCodigo) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

end.
