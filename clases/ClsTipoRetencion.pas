unit ClsTipoRetencion;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Controls, Variants;

type

  { TTipoRetencion }

  TTipoRetencion = class(TGenericoBD)
    private
      Codigo: string;
      Descripcion: string;
      RetencionIVA: Boolean;
      Base: Double;
      Tarifa: Double;
      Sustraendo: Double;
      Cobranza: Boolean;
      Pago: Boolean;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArraystring; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigo(varCodigo: string);
      procedure SetDescripcion(varDescripcion: string);
      procedure SetRetencionIVA(varRetencionIVA: Boolean);
      procedure SetBase(varBase: Double);
      procedure SetTarifa(varTarifa: Double);
      procedure SetSustraendo(varSustraendo: Double);
      procedure SetCobranza(varCobranza: Boolean);
      procedure SetPago(varPago: Boolean);
      function GetCodigo: string;
      function GetDescripcion: string;
      function GetRetencionIVA: Boolean;
      function GetBase: Double;
      function GetTarifa: Double;
      function GetSustraendo: Double;
      function GetCobranza: Boolean;
      function GetPago: Boolean;
      function ObtenerComboISLR: TClientDataSet;
      function ObtenerComboIVA: TClientDataSet;
  end;

implementation

uses
  DB,
  ClsConsecutivo;

{ TTipoRetencion }

procedure TTipoRetencion.Ajustar;
begin

end;

procedure TTipoRetencion.Mover(Ds: TClientDataSet);
begin
  Codigo := Ds.FieldValues['codigo'];
  Descripcion := Ds.FieldValues['descripcion'];
  RetencionIVA := Ds.FieldValues['retencion_iva'];
  Base := Ds.FieldValues['base'];
  Tarifa := Ds.FieldValues['tarifa'];
  Sustraendo := Ds.FieldValues['sustraendo'];
  Cobranza := Ds.FieldValues['cobranza'];
  Pago := Ds.FieldValues['pago'];
end;

function TTipoRetencion.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,8);
  Params[0] := Codigo;
  Params[1] := Descripcion;
  Params[2] := RetencionIVA;
  Params[3] := Base;
  Params[4] := Tarifa;
  Params[5] := Sustraendo;
  Params[6] := Cobranza;
  Params[7] := Pago;
  Result := Params;
end;

function TTipoRetencion.GetCampos: TArraystring;
var
  Campos: TArraystring;
begin
  SetLength(Campos,2);
  Campos[0] := 'codigo';
  Campos[1] := 'descripcion';
  Result := Campos;
end;

function TTipoRetencion.GetCondicion: string;
begin
  Result:= '"codigo" = ' + QuotedStr(Codigo);
end;

procedure TTipoRetencion.AntesInsertar;
begin

end;

procedure TTipoRetencion.AntesModificar;
begin

end;

procedure TTipoRetencion.AntesEliminar;
begin

end;

procedure TTipoRetencion.DespuesInsertar;
begin

end;

procedure TTipoRetencion.DespuesModificar;
begin

end;

procedure TTipoRetencion.DespuesEliminar;
begin

end;

constructor TTipoRetencion.Create;
var
  ClavesPrimarias: TArraystring;
begin
  inherited Create;
  SetTabla('tbl_tipos_retenciones');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(8)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "descripcion"=$2, "retencion_iva"=$3, "base"=$4, "tarifa"=$5, "sustraendo"=$6, "cobranza"=$7, "pago"=$8 ' +
                  ' WHERE ("codigo"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("codigo"=$1)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"codigo"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TTipoRetencion.SetCodigo(varCodigo: string);
begin
  Codigo := varCodigo;
end;

procedure TTipoRetencion.SetDescripcion(varDescripcion: string);
begin
  Descripcion := varDescripcion;
end;

procedure TTipoRetencion.SetRetencionIVA(varRetencionIVA: Boolean);
begin
  RetencionIVA := varRetencionIVA;
end;

procedure TTipoRetencion.SetBase(varBase: Double);
begin
  Base := varBase;
end;

procedure TTipoRetencion.SetTarifa(varTarifa: Double);
begin
  Tarifa := varTarifa;
end;

procedure TTipoRetencion.SetSustraendo(varSustraendo: Double);
begin
  Sustraendo := varSustraendo;
end;

procedure TTipoRetencion.SetCobranza(varCobranza: Boolean);
begin
  Cobranza := varCobranza;
end;

procedure TTipoRetencion.SetPago(varPago: Boolean);
begin
  Pago := varPago;
end;

function TTipoRetencion.GetCodigo: string;
begin
  Result := Codigo;
end;

function TTipoRetencion.GetDescripcion: string;
begin
  Result := Descripcion;
end;

function TTipoRetencion.GetRetencionIVA: Boolean;
begin
  Result := RetencionIVA;
end;

function TTipoRetencion.GetBase: Double;
begin
  Result := Base;
end;

function TTipoRetencion.GetTarifa: Double;
begin
  Result := Tarifa;
end;

function TTipoRetencion.GetSustraendo: Double;
begin
  Result := Sustraendo;
end;

function TTipoRetencion.GetCobranza: Boolean;
begin
  Result := Cobranza;
end;

function TTipoRetencion.GetPago: Boolean;
begin
  Result := Pago;
end;

function TTipoRetencion.ObtenerComboISLR: TClientDataSet;
begin
  Result := BD.EjecutarQueryCliDs('SELECT "codigo", "descripcion" FROM ' + GetTabla + ' WHERE (not "retencion_iva")');
end;

function TTipoRetencion.ObtenerComboIVA: TClientDataSet;
begin
  Result := BD.EjecutarQueryCliDs('SELECT "codigo", "descripcion" FROM ' + GetTabla + ' WHERE ("retencion_iva")');
end;

end.
