object DMRave: TDMRave
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 300
  Width = 191
  object RvProyecto: TRvProject
    Engine = RvMotor
    Left = 29
    Top = 8
  end
  object RvMotor: TRvSystem
    TitleSetup = 'Opciones de Impresi'#243'n'
    TitleStatus = 'Estado del Reporte'
    TitlePreview = 'Previsualizaci'#243'n del Reporte'
    SystemOptions = [soUseFiler, soShowStatus, soAllowPrintFromPreview, soAllowSaveFromPreview, soPreviewModal]
    SystemFiler.StatusFormat = 'Generando P'#225'gina %p'
    SystemFiler.StatusText.Strings = (
      'Por Favor Espere...')
    SystemFiler.StreamMode = smTempFile
    SystemPreview.FormWidth = 800
    SystemPreview.FormHeight = 600
    SystemPreview.FormState = wsMaximized
    SystemPreview.RulerType = rtBothCm
    SystemPreview.ZoomFactor = 100.000000000000000000
    SystemPreview.ZoomInc = 5
    SystemPrinter.ScaleX = 100.000000000000000000
    SystemPrinter.ScaleY = 100.000000000000000000
    SystemPrinter.StatusFormat = 'Imprimiendo P'#225'gina %p'
    SystemPrinter.StatusText.Strings = (
      'Por Favor Espere...')
    SystemPrinter.Title = 'Impresi'#243'n de Reporte desde Rave Reports BEX 7.0.5'
    SystemPrinter.Units = unMM
    SystemPrinter.UnitsFactor = 25.400000000000000000
    OverrideSetup = RvMotorOverrideSetup
    OverrideStatus = RvMotorOverrideStatus
    OverridePreview = RvMotorOverridePreview
    Left = 112
    Top = 8
  end
  object RvRenderPDF: TRvRenderPDF
    DisplayName = 'Archivo Adobe Acrobat (*.pdf)'
    FileExtension = '*.pdf'
    UseCompression = True
    EmbedFonts = False
    ImageQuality = 100
    MetafileDPI = 300
    FontEncoding = feWinAnsiEncoding
    DocInfo.Title = 'Reporte Exportado por S.A.E.'
    DocInfo.Author = 'S.A.E.'
    DocInfo.Creator = 'Ing. Reinaldo Mendoza'
    DocInfo.Producer = 'Ing. Eliud Duno'
    BufferDocument = True
    DisableHyperlinks = False
    Left = 29
    Top = 56
  end
  object RvRenderHTML: TRvRenderHTML
    DisplayName = 'P'#225'gina WEB (*.html,*.htm)'
    FileExtension = '*.html;*.htm'
    ServerMode = False
    UseBreakingSpaces = False
    Left = 29
    Top = 104
  end
  object RvRenderRTF: TRvRenderRTF
    DisplayName = 'Archivo de Texto Enriquecido (*.rtf)'
    FileExtension = '*.rtf'
    Left = 29
    Top = 152
  end
  object RvRenderTEXT: TRvRenderText
    DisplayName = 'Archivo de Texto (*.txt)'
    FileExtension = '*.txt'
    CPI = 10.000000000000000000
    LPI = 6.000000000000000000
    Left = 29
    Top = 200
  end
  object RvRenderBMP: TRvRenderBitmap
    DisplayName = 'Archivo de Mapas de Bits de Windows - OS/2 (*.bmp)'
    FileExtension = '*.bmp'
    ImageDPI = 72
    Left = 112
    Top = 56
  end
  object RvRenderWMF: TRvRenderMetafile
    DisplayName = 'Windows Metafile (WMF)'
    FileExtension = '*.wmf'
    ImageDPI = 72
    Enhanced = False
    Left = 112
    Top = 104
  end
  object RvRenderJPG: TRvRenderJpeg
    DisplayName = 'Archivo de Imagen JPG (*.jpg)'
    FileExtension = '*.jpg'
    ImageDPI = 72
    CompressionQuality = 100
    Left = 112
    Top = 200
  end
  object RvRenderEMF: TRvRenderMetafile
    DisplayName = 'Windows Enhanced Metafile (EMF)'
    FileExtension = '*.emf'
    ImageDPI = 72
    Enhanced = True
    Left = 112
    Top = 152
  end
end
