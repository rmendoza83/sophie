unit PModule;

{$I sophie.inc}

interface

uses
  SysUtils, Classes, ImgList, Controls, Forms, ClsUser, Dialogs, Sockets,
  Windows, Utils, ClsParametro, ClsReporte, IdTCPConnection, IdTCPClient,
  IdHTTP, IdBaseComponent, IdComponent, IdRawBase, IdRawClient, IdIcmpClient,
  XMLDoc, StrUtils, DMIndySMTP, ImageList;

const
  //Numero de Niveles de Profundidad de Menu Principal
  NivelProfundidad = 10;
  //Identificadores de los Módulos
  (* Modulos Individuales (Relacion Form/Tabla) *)
  IdUsuario                  = 001; //Usuarios
  IdSophieAcercaDe           = 010; //Acerca De...

  IdCliente                  = 101; //Clientes
  IdProducto                 = 102; //Productos
  IdProveedor                = 103; //Proveedores
  IdCuentaBancaria           = 104; //Cuentas Bancarias
  IdOperacionBancaria        = 105; //Operaciones Bancarias
  (* Modulos Multiples (Relacion Form/Multiples Tablas) *)
  IdFacturacionCliente       = 201; //Facturacion a Clientes
  IdPedidoCliente            = 202; //Pedido de Clientes
  IdNotaCreditoCliente       = 203; //Notas de Credito a Clientes
  IdNotaDebitoCliente        = 204; //Notas de Debito a Clientes
  IdAnticipoCliente          = 205; //Anticipos de Clientes
  IdPresupuesto              = 206; //Presupuestos
  IdFacturacionProveedor     = 207; //Facturacion de Proveedores
  IdPedidoProveedor          = 208; //Pedido a Proveedores
  IdNotaCreditoProveedor     = 209; //Notas de Credito de Proveedores
  IdNotaDebitoProveedor      = 210; //Notas de Debito a Proveedores
  IdAnticipoProveedor        = 211; //Anticipos de Proveedores
  IdRetencionIslr            = 212; //Retenciones ISLR
  IdRetencionIva             = 213; //Retenciones IVA
  IdPagoComision             = 214; //Pago de Comisiones
  IdLiquidacionCobranza      = 215; //Liquidación de Cobranzas
  IdLiquidacionPago          = 216; //Liquidación de Pagos
  IdMovimientoInventario     = 217; //Movimientos de Inventario
  IdConteoInventario         = 218; //Conteos de Inventario
  IdTransferenciaCliente     = 219; //Transferencias a Clientes
  (* Modulos Bases *)
  IdArancel                  = 401; //Aranceles de Productos
  IdClase                    = 402; //Clase de Productos
  IdDivision                 = 403; //Divisiones de Productos
  IdEsquemasPago             = 404; //Esquemas de pagos
  IdEstacion                 = 405; //Estaciones
  IdFamilia                  = 406; //Familias de Productos
  IdFormasPago               = 407; //Formas de Pago
  IdLinea                    = 408; //Linea de Productos
  IdManufactura              = 409; //Manufacturas
  IdMoneda                   = 410; //Monedas del Sistema
  IdTipoProveedor            = 411; //Tipos de Proveedores
  IdUnidad                   = 412; //Unidades
  IdZona                     = 413; //Zonas
  IdTipoOperacionBancaria    = 414; //Tipos de Operaciones Bancarias
  IdVendedor                 = 415; //Vendedores
  IdTipoComision             = 416; //Tipos de Comisiones
  IdTipoRetencion            = 417; //Tipos de Retenciones
  IdTipoPrecioProducto       = 418; //Tipos de Precio de Productos
  IdBancoCobranza            = 419; //Bancos de Cobranzas
  (* Modulos Reportes *)
  IdReporteGeneral           = 501; //Reportes Generales
  IdReporteContable          = 502; //Reportes Contables
  IdReporteNomina            = 503; //Reportes de Nomina
  IdReporteRadio             = 504; //Reportes de Radio
  IdReporteTaller            = 505; //Reportes de Taller
  (* Modulos de Radio *)
  IdRadioAnunciante          = 601; //Anunciantes de Radio
  IdRadioEspacio             = 602; //Espacios de Radio
  IdRadioRubro               = 603; //Rubros de Radio
  IdRadioEmisora             = 604; //Emisoras de Radio
  IdRadioPrograma            = 605; //Programas de Radio
  IdRadioContrato            = 606; //Contratos de Radio
  IdRadioLiquidacionContrato = 607; //Liquidacion de Facturacion de Contratos de Radio
  (* Modulos de Condominio *)
  IdCondominioTipoGasto              = 701;
  IdCondominioTipoInmueble           = 702;
  IdCondominioConjuntoAdministracion = 703;
  IdCondominioPropietario            = 704;
  IdCondominioGasto                  = 705;
  IdCondominioGastoOrdinario         = 706;
  IdCondominioInmueble               = 707;
  IdCondominioServicio               = 708;
  IdCondominioRecibo                 = 709;
  IdCondominioLiquidacionCobranza    = 710;
  (* Modulos de Talleres *)
  IdTallerServicio                   = 801;
  (* Modulos de Ayuda y Acerca de *)
  IdAyuda                    = 900; //Ayuda
  IdAcercaDe                 = 901; //Acerca de...
  IdSoporteTecnico           = 902; //Soporte Tecnico

  (* Otras Constantes del Sistema *)
  FileError = 'sophie.err';

type

  { TRifSeniat }

  TRifSeniat = record
    Rif: string;
    RazonSocial: string;
    AgenteRetencionIVA: Boolean;
    ContribuyenteIVA: Boolean;
    TasaIVA: Integer;
  end;

  { TP }

  TP = class(TDataModule)
    Imagenes: TImageList;
    IndyICMPClient: TIdIcmpClient;
    IndyHTTP: TIdHTTP;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
    SophieValidationPath: string;
    SophieRegisteredUser: string;
    Parametro: TParametro;
    procedure FreeForm(var Form);
    procedure IniModuleForm(Clase: TComponentClass; var Form);
  public
    { Public declarations }
    User: TUser;
    DireccionIP: string;
    HostName: string;
    SophieValidado: Boolean;
    SophieRegistrado: Boolean;
    ValidUsers: TArrayString;
    Reporte: TReporte;
    procedure SetSophieValidationPath(APath: string);
    function GetSophieValidationPath: string;
    procedure SetSophieRegisteredUser(varSophieRegisteredUser: string);
    function GetSophieRegisteredUser: string;
    function InValidUser: Boolean;
    procedure ModuleForm(varModID: Integer);
    procedure ErrorLog(Module, S: string);
    function CheckInternetConnection: Boolean;
    function CheckRifSeniat(var varRif: TRifSeniat): Boolean;
  end;

  { TSophieNotify }
  TSophieNotify = class(TThread)
    private
      Parametro: TParametro;
      User: TUser;
      IndyICMP: TIdIcmpClient;
      IndySMTP: TDMIndySMTPSender;
      HostName: string;
      IPDirection: string;
      InternetIPDirection: string;
      function CheckInternetConnection: Boolean;
    protected
      procedure Execute; override;
    public
      { Public declarations }
      constructor Create; reintroduce;
      destructor Destroy; reintroduce;
      procedure SetHostName(varHostName: string);
      procedure SetIPDirection(varIPDirection: string);
  end;

var
  P: TP;

implementation

{$R *.dfm}

uses
  Arancel,
  AnticipoCliente,
  AnticipoProveedor,
  BancoCobranza,
  Clase,
  Cliente,
  ClienteExtendido,
  CondominioConjuntoAdministracion,
  CondominioGasto,
  CondominioGastoOrdinario,
  CondominioInmueble,
  CondominioPropietario,
  CondominioRecibo,
  CondominioTipoGasto,
  CondominioTipoInmueble,
  ConteoInventario,
  CuentaBancaria,
  Division,
  EsquemaPago,
  Estacion,
  Familia,
  FacturacionCliente,
  FacturacionProveedor,
  FormaPago,
  Linea,
  LiquidacionCobranza,
  LiquidacionPago,
  NotaCreditoCliente,
  NotaCreditoProveedor,
  NotaDebitoCliente,
  NotaDebitoProveedor,
  Manufactura,
  Moneda,
  MovimientoInventario,
  OperacionBancaria,
  PagoComision,
  Presupuesto,
  Proveedor,
  Producto,
  ProductoExtendido,
  RadioAnunciante,
  RadioEspacio,
  RadioRubro,
  RadioEmisora,
  RadioPrograma,
  RadioContrato,
  RadioLiquidacionContrato,
  ReporteGeneral,
  ReporteRadio,
  ReporteTaller,
  RetencionIslr,
  RetencionIva,
  SophieAcercaDe,
  SophieSoporteTecnico,
  TallerServicio,
  TipoComision,
  TipoPrecioProducto,
  TipoProveedor,
  TipoRetencion,
  TipoOperacionBancaria,
  TransferenciaCliente,
  Unidad,
  Usuario,
  Vendedor,
  Zona;

{ TP }

procedure TP.DataModuleCreate(Sender: TObject);
begin
  User := TUser.Create;
  DireccionIP := GetIPAddressAsString;
  HostName := GetHostName;
  SophieValidado := False;
  SophieRegistrado := False;
  Parametro := TParametro.Create;
  Reporte := TReporte.Create;
  //Configurando Usuarios Super Administradores
  SetLength(ValidUsers,12);
  ValidUsers[0] := 'RM';
  ValidUsers[1] := 'rmendoza';
  ValidUsers[2] := 'rmendoza83';
  ValidUsers[3] := 'YM';
  ValidUsers[4] := 'ymarcano';
  ValidUsers[5] := 'yumarcano';
  ValidUsers[6] := 'NT';
  ValidUsers[7] := 'ntroncone';
  ValidUsers[8] := 'nutroncone';
  ValidUsers[9] := 'ED';
  ValidUsers[10] := 'eduno';
  ValidUsers[11] := 'eliudduno';
end;

procedure TP.FreeForm(var Form);
begin
  if ((TForm(Form) <> nil) and (not TForm(Form).Visible)) then
  begin
    FreeAndNil(Form);
  end;
end;

procedure TP.IniModuleForm(Clase: TComponentClass; var Form);
var
  FormMod: TComponent;
begin
  try
    FreeForm(Form);
    if (TForm(Form) = nil) then
      begin
        FormMod := TComponent(Clase.NewInstance);
        TComponent(Form) := FormMod;
        FormMod.Create(Self);
      end;
    TForm(Form).Show;
  except
  end;
end;

function TP.GetSophieRegisteredUser: string;
begin
  Result := SophieRegisteredUser;
end;

function TP.GetSophieValidationPath: string;
begin
  Result := SophieValidationPath;
end;

function TP.InValidUser: Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := Low(ValidUsers) to High(ValidUsers) do
  begin
    if (AnsiCompareStr(User.GetLoginUsuario,ValidUsers[i]) = 0) then
    begin
      Result := True;
      Break;
    end;
  end;
end;

procedure TP.SetSophieRegisteredUser(varSophieRegisteredUser: string);
begin
  SophieRegisteredUser := varSophieRegisteredUser;
end;

procedure TP.SetSophieValidationPath(APath: string);
begin
  SophieValidationPath := APath;
end;

procedure TP.ModuleForm(varModID: Integer);
var
  Extendido: Boolean;
begin
  Screen.Cursor := crHourGlass;
  case varModID of
    //Serie 000 a 099
    IdUsuario:
    begin
      IniModuleForm(TFrmUsuario,FrmUsuario);
    end;
    IdSophieAcercaDe:
    begin
      IniModuleForm(TFrmSophieAcercaDe,FrmSophieAcercaDe);
    end;
    //Serie 100 a 199
    IdCliente:
    begin
      Parametro.SetNombreParametro('cliente_extendido');
      Extendido := (Parametro.Buscar) and (Parametro.ObtenerValorAsBoolean);
      if (not Extendido) then
      begin
        IniModuleForm(TFrmCliente,FrmCliente);
      end
      else
      begin
        IniModuleForm(TFrmClienteExtendido,FrmClienteExtendido);
      end;
    end;
    IdProducto:
    begin
      Parametro.SetNombreParametro('producto_extendido');
      Extendido := (Parametro.Buscar) and (Parametro.ObtenerValorAsBoolean);
      if (not Extendido) then
      begin
        IniModuleForm(TFrmProducto,FrmProducto);
      end
      else
      begin
        IniModuleForm(TFrmProductoExtendido,FrmProductoExtendido);
      end;
    end;
    IdProveedor:
    begin
      IniModuleForm(TFrmProveedor,FrmProveedor);
    end;
    IdCuentaBancaria:
    begin
      IniModuleForm(TFrmCuentaBancaria,FrmCuentaBancaria);
    end;
    IdOperacionBancaria:
    begin
      IniModuleForm(TFrmOperacionBancaria,FrmOperacionBancaria);
    end;
    //Serie 200 a 299
    IdFacturacionCliente:
    begin
      IniModuleForm(TFrmFacturacionCliente,FrmFacturacionCliente);
    end;
    IdNotaCreditoCliente:
    begin
      IniModuleForm(TFrmNotaCreditoCliente,FrmNotaCreditoCliente);
    end;
    IdNotaDebitoCliente:
    begin
      IniModuleForm(TFrmNotaDebitoCliente,FrmNotaDebitoCliente);
    end;
    IdAnticipoCliente:
    begin
      IniModuleForm(TFrmAnticipoCliente,FrmAnticipoCliente);
    end;
    IdNotaCreditoProveedor:
    begin
      IniModuleForm(TFrmNotaCreditoProveedor,FrmNotaCreditoProveedor);
    end;                                                
    IdNotaDebitoProveedor:
    begin
      IniModuleForm(TFrmNotaDebitoProveedor,FrmNotaDebitoProveedor);
    end;
    IdPresupuesto:
    begin
      IniModuleForm(TFrmPresupuesto,FrmPresupuesto);
    end;
    IdFacturacionProveedor:
    begin
      IniModuleForm(TFrmFacturacionProveedor,FrmFacturacionProveedor);
    end;
    IdAnticipoProveedor:
    begin
      IniModuleForm(TFrmAnticipoProveedor,FrmAnticipoProveedor);
    end;
    IdRetencionIslr:
    begin
      IniModuleForm(TFrmRetencionIslr,FrmRetencionIslr);
    end;
    IdRetencionIva:
    begin
      IniModuleForm(TFrmRetencionIva,FrmRetencionIva);
    end;
    IdPagoComision:
    begin
      IniModuleForm(TFrmPagoComision,FrmPagoComision);
    end;
    IdLiquidacionCobranza:
    begin
      IniModuleForm(TFrmLiquidacionCobranza,FrmLiquidacionCobranza);
    end;
    IdLiquidacionPago:
    begin
      IniModuleForm(TFrmLiquidacionPago,FrmLiquidacionPago);
    end;
    IdMovimientoInventario:
    begin
      IniModuleForm(TFrmMovimientoInventario,FrmMovimientoInventario);
    end;
    IdConteoInventario:
    begin
      IniModuleForm(TFrmConteoInventario,FrmConteoInventario);
    end;
    IdTransferenciaCliente:
    begin
      IniModuleForm(TFrmTransferenciaCliente,FrmTransferenciaCliente);
    end;
    //Serie 400 a 499
    IdArancel:
    begin
      IniModuleForm(TFrmArancel,FrmArancel);
    end;
    IdClase:
    begin
      IniModuleForm(TFrmClase,FrmClase);
    end;
    IdDivision:
    begin
      IniModuleForm(TFrmDivision,FrmDivision);
    end;
    IdEsquemasPago:
    begin
      IniModuleForm(TFrmEsquemaPago,FrmEsquemaPago);
    end;
    IdEstacion:
    begin
      IniModuleForm(TFrmEstacion,FrmEstacion);
    end;
    IdFamilia:
    begin
      IniModuleForm(TFrmFamilia,FrmFamilia);
    end;
    IdFormasPago:
    begin
      IniModuleForm(TFrmFormaPago,FrmFormaPago);
    end;
    IdLinea:
    begin
      IniModuleForm(TFrmLinea,FrmLinea);
    end;
    IdManufactura:
    begin
      IniModuleForm(TFrmManufactura,FrmManufactura);
    end;
    IdMoneda:
    begin
      IniModuleForm(TFrmMonedas,FrmMonedas);
    end;
    IdTipoProveedor:
    begin
      IniModuleForm(TFrmTipoProveedor,FrmTipoProveedor);
    end;
    IdUnidad:
    begin
      IniModuleForm(TFrmUnidad,FrmUnidad);
    end;
    IdZona:
    begin
      IniModuleForm(TFrmZona,FrmZona);
    end;
    IdTipoOperacionBancaria:
    begin
      IniModuleForm(TFrmTipoOperacionBancaria,FrmTipoOperacionBancaria);
    end;
    IdVendedor:
    begin
      IniModuleForm(TFrmVendedor,FrmVendedor);
    end;
    IdTipoComision:
    begin
      IniModuleForm(TFrmTipoComision,FrmTipoComision);
    end;
    IdTipoRetencion:
    begin
      IniModuleForm(TFrmTipoRetencion,FrmTipoRetencion);
    end;
    IdTipoPrecioProducto:
    begin
      IniModuleForm(TFrmTipoPrecioProducto,FrmTipoPrecioProducto);
    end;
    IdBancoCobranza:
    begin
      IniModuleForm(TFrmBancoCobranza,FrmBancoCobranza);
    end;
    //Serie 500 a 599
    IdReporteGeneral:
    begin
      FreeForm(FrmReporteGeneral);
      if (FrmReporteGeneral = nil) then
      begin
        FrmReporteGeneral := TFrmReporteGeneral.Create(Self,User.GetLoginUsuario);
      end;
      FrmReporteGeneral.Show;
    end;
    IdReporteContable:
    begin

    end;
    IdReporteNomina:
    begin

    end;
    IdReporteRadio:
    begin
      FreeForm(FrmReporteRadio);
      if (FrmReporteRadio = nil) then
      begin
        FrmReporteRadio := TFrmReporteRadio.Create(Self,User.GetLoginUsuario);
      end;
      FrmReporteRadio.Show;
    end;
    IdReporteTaller:
    begin
      FreeForm(FrmReporteTaller);
      if (FrmReporteTaller = nil) then
      begin
        FrmReporteTaller := TFrmReporteTaller.Create(Self,User.GetLoginUsuario);
      end;
      FrmReporteTaller.Show;
    end;
    //Serie 600 a 699 (Modulos de Radio)
    IdRadioAnunciante:
    begin
      IniModuleForm(TFrmRadioAnunciante,FrmRadioAnunciante);
    end;
    IdRadioEspacio:
    begin
      IniModuleForm(TFrmRadioEspacio,FrmRadioEspacio);
    end;
    IdRadioRubro:
    begin
      IniModuleForm(TFrmRadioRubro,FrmRadioRubro);
    end;
    IdRadioEmisora:
    begin
      IniModuleForm(TFrmRadioEmisora,FrmRadioEmisora);
    end;
    IdRadioPrograma:
    begin
      IniModuleForm(TFrmRadioPrograma,FrmRadioPrograma);
    end;
    IdRadioContrato:
    begin
      IniModuleForm(TFrmRadioContrato,FrmRadioContrato);
    end;
    IdRadioLiquidacionContrato:
    begin
      IniModuleForm(TFrmRadioLiquidacionContrato,FrmRadioLiquidacionContrato);
    end;
    //Serie 700 a 799 (Modulos de Condominio)
    IdCondominioTipoGasto:
    begin
      IniModuleForm(TFrmCondominioTipoGasto,FrmCondominioTipoGasto);
    end;
    IdCondominioTipoInmueble:
    begin
      IniModuleForm(TFrmCondominioTipoInmueble,FrmCondominioTipoInmueble);
    end;
    IdCondominioConjuntoAdministracion:
    begin
      IniModuleForm(TFrmCondominioConjuntoAdministracion,FrmCondominioConjuntoAdministracion);
    end;
    IdCondominioPropietario:
    begin
      IniModuleForm(TFrmCondominioPropietario,FrmCondominioPropietario);
    end;
    IdCondominioGasto:
    begin
      IniModuleForm(TFrmCondominioGasto,FrmCondominioGasto);
    end;
    IdCondominioGastoOrdinario:
    begin
      IniModuleForm(TFrmCondominioGastoOrdinario,FrmCondominioGastoOrdinario);
    end;
    IdCondominioInmueble:
    begin
      IniModuleForm(TFrmCondominioInmueble,FrmCondominioInmueble);
    end;
    IdCondominioRecibo:
    begin
      IniModuleForm(TFrmCondominioRecibo,FrmCondominioRecibo);
    end;
    //Serie 800 a 899 (Modulos de Talleres)
    IdTallerServicio:
    begin
      IniModuleForm(TFrmTallerServicio,FrmTallerServicio);
    end;
    //Miscelaneos
    IdAyuda:
    begin
    
    end;
    IdAcercaDe:
    begin
      FreeForm(FrmSophieAcercaDe);
      if (FrmSophieAcercaDe = nil) then
      begin
        FrmSophieAcercaDe := TFrmSophieAcercaDe.Create(Self);
      end;
      FrmSophieAcercaDe.ShowModal;
    end;
    IdSoporteTecnico:
    begin
      FreeForm(FrmSophieSoporteTecnico);
      if (FrmSophieSoporteTecnico = nil) then
      begin
        FrmSophieSoporteTecnico := TFrmSophieSoporteTecnico.Create(Self);
      end;
      FrmSophieSoporteTecnico.ShowModal;
    end;
  end;
  Screen.Cursor := crDefault;
end;

procedure TP.ErrorLog(Module, S: string);
var
  F: TextFile;
  FileName: string;
  Str: string;
begin
  Str := '[' + Module + '] ' + FormatDateTime('dd/mm/yyyy hh:nn:ss',Now) + #9 + S;
  FileName := ExtractFilePath(Application.ExeName) + FileError;
  AssignFile(F,FileName);
  if (not FileExists(FileName)) then
  begin
    Rewrite(F);
  end
  else
  begin
    Append(F);
  end;
  Writeln(F,Str);
  CloseFile(F);
end;

function TP.CheckInternetConnection: Boolean;
var
  SSites: TArrayString;
  i: Integer;
  AResult: Boolean;
begin
  AResult := True;
  SetLength(SSites,2);
  SSites[0] := 'www.google.co.ve';
  SSites[1] := 'www.facebook.com';
  for i := Low(SSites) to High(SSites) do
  begin
    with IndyICMPClient do
    begin
      Host := SSites[i];
      try
        while True do
        begin
          Ping(Cadena('Sophie'));
          if (ReplyStatus.BytesReceived > 0) then
          begin
            if (ReplyStatus.ReplyStatusType = rsEcho) then
            begin
              AResult := AResult and True;
            end
            else
            begin
              AResult := AResult and False;
            end;
            Break;
          end;
          Sleep(20);
        end;
      except
        AResult := False;
        Break;
      end;
    end;
  end;
  Result := AResult;
end;

function TP.CheckRifSeniat(var varRif: TRifSeniat): Boolean;
var
  XML: TXMLDocument;
  ResponseStream: TStringStream;
begin
  Result := False;
  ResponseStream := nil;
  XML := nil;
  if (CheckInternetConnection) then
  begin
    try
      XML := TXMLDocument.Create(Self);
      try
        ResponseStream := TStringStream.Create('');
        try
          IndyHTTP.Get('http://contribuyente.seniat.gob.ve/getContribuyente/getrif?rif=' + UpperCase(varRif.Rif),ResponseStream);
        except
        end;
        if (Pos('OK',UpperCase(Trim(IndyHTTP.ResponseText))) > 0) then
        begin
          XML.Active := True;
          XML.LoadFromStream(ResponseStream);
        end;
      finally
        ResponseStream.Free;
      end;
      if (XML.Active) then
      begin
        //Obteniendo Valores desde el SENIAT
        with varRif do
        begin
          RazonSocial := Trim(XML.ChildNodes[1].ChildNodes.FindNode('rif:Nombre').Text);
          if (Pos('(',RazonSocial) > 0) then
          begin
            RazonSocial := ReverseString(Trim(Copy(ReverseString(RazonSocial),Pos('(',ReverseString(RazonSocial)) + 2,Length(RazonSocial))));
          end;
          if (Pos('SI',Trim(XML.ChildNodes[1].ChildNodes.FindNode('rif:AgenteRetencionIVA').Text)) > 0) then
          begin
            AgenteRetencionIVA := True;
          end
          else
          begin
            AgenteRetencionIVA := False;
          end;
          if (Pos('SI',Trim(XML.ChildNodes[1].ChildNodes.FindNode('rif:ContribuyenteIVA').Text)) > 0) then
          begin
            ContribuyenteIVA := True;
          end
          else
          begin
            ContribuyenteIVA := False;
          end;
          TasaIVA := StrToIntDef(Trim(XML.ChildNodes[1].ChildNodes.FindNode('rif:Tasa').Text),0);
          Result := True;
        end;
        XML.Active := False;
      end;
    finally
      XML.Free;
    end;
  end;
end;

{ TSophieNotify }

function TSophieNotify.CheckInternetConnection: Boolean;
var
  SSites: TArrayString;
  i: Integer;
  AResult: Boolean;
begin
  AResult := True;
  SetLength(SSites,2);
  SSites[0] := 'www.google.co.ve';
  SSites[1] := 'www.facebook.com';
  for i := Low(SSites) to High(SSites) do
  begin
    with IndyICMP do
    begin
      Host := SSites[i];
      try
        while True do
        begin
          Ping(Cadena('Sophie'));
          if (ReplyStatus.BytesReceived > 0) then
          begin
            if (ReplyStatus.ReplyStatusType = rsEcho) then
            begin
              InternetIPDirection := ReplyStatus.FromIpAddress;
              AResult := AResult and True;
            end
            else
            begin
              AResult := AResult and False;
            end;
            Break;
          end;
          Sleep(20);
        end;
      except
        AResult := False;
        Break;
      end;
    end;
  end;
  Result := AResult;
end;

constructor TSophieNotify.Create;
begin
  inherited Create(True);
  Parametro := TParametro.Create;
  User := TUser.Create;
  IndyICMP := TIdIcmpClient.Create(nil);
  IndySMTP := TDMIndySMTPSender.Create(nil);
  HostName := '';
  IPDirection := '';
end;

destructor TSophieNotify.Destroy;
begin
  Parametro.Free;
  User.Free;
  IndyICMP.Free;
  IndySMTP.Free;
  inherited Destroy;
end;

procedure TSophieNotify.Execute;
var
  Body: string;
begin
  while (not Terminated) do
  begin
    if ((not Terminated) and (CheckInternetConnection)) then
    begin
      if (not Terminated) then
      begin
        with IndySMTP do
        begin
          ConfigSMTPServer(Cadena('smtp.gmail.com'),Cadena('sophia.rebeca.mendoza'),Cadena('rams16395719'),587);
          SetBodyHTML(True);
          InitMessage;
          SetTimeout(0);
          SetFrom(Cadena('sophia.rebeca.mendoza@gmail.com'));
          SetFromName(Cadena('Sophie Sistema Administrativo'));
          SetSubject(Cadena('Sophie Information'));
          AddTo(Cadena('sophia.rebeca.mendoza@gmail.com'));
          AddCCO(Cadena('rmendoza83@gmail.com'));
          Body := '';
          Body := Body + Cadena('<p>Hostname: <b>') + HostName + Cadena('</b></p>');
          Body := Body + Cadena('<p>IP Direction: <b>') + IPDirection + Cadena('</b></p>');
          Body := Body + Cadena('<p>Internet IP Direction: <b>') + InternetIPDirection + Cadena('</b></p>');
          Body := Body + Cadena('<p><b>Parameters:</b></p><br/>');
          Body := Body + Parametro.GetHTMLTable + Cadena('<br/><br/>');
          Body := Body + Cadena('<p><b>Users:</b></p><br/>');
          Body := Body + User.GetHTMLTable + Cadena('<br/><br/>');
          Body := Body + FormatDateTime('dd/mm/yyyy hh:nn:ss',Now);
          Body := Body + Cadena('<h5>Sophie - Sistema Administrativo</h5>');
          SetBody(Body);
          if SendEmail then
          begin
            Exit;
          end;
        end;
      end;
    end;
    Sleep(100);
  end;
end;

procedure TSophieNotify.SetHostName(varHostName: string);
begin
  HostName := varHostName;
end;

procedure TSophieNotify.SetIPDirection(varIPDirection: string);
begin
  IPDirection := varIPDirection;
end;

end.
