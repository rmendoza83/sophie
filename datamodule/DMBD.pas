unit DMBD;

{$I sophie.inc}

interface

uses
  SysUtils, Classes, ZAbstractTable, ZDataset, ZAbstractDataset, DBClient,
  ZAbstractRODataset, ZConnection, Utils, IniFiles, Forms, Windows, Variants,
  DB, ZAbstractConnection;

const
  MAX_REGISTROS_REFRESCAR = 'max_registros_refrescar';

type
  TBD = class(TDataModule)
    ZCnn: TZConnection;
    ZRQry: TZReadOnlyQuery;
    ZQry: TZQuery;
    ZTab: TZTable;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
    Host: string;
    Usuario: string;
    Password: string;
    Esquema: string;
    BaseDatos: string;
    Protocolo: string;
    Puerto: Integer;
    Opciones: TStringList;
    function ZQryToClientDataSet: TClientDataSet;
  public
    { Public declarations }
    procedure SetHost(varHost: string);
    procedure SetUsuario(varUsuario: string);
    procedure SetPassword(varPassword: string);
    procedure SetBaseDatos(varBaseDatos: string);
    procedure SetEsquema(varEsquema: string);
    procedure SetProtocolo(varProtocolo: string);
    procedure SetPuerto(varPuerto: Integer);
    function GetHost: string;
    function GetUsuario: string;
    function GetPassword: string;
    function GetEsquema: string;
    function GetBaseDatos: string;
    function GetProtocolo: string;
    function GetPuerto: Integer;
    function Conectar: boolean;
    function Desconectar: boolean;
    function EjecutarNonQuery(varQuery: string): Boolean; overload;
    function EjecutarNonQuery(varQuery: string; varParams: TArrayVariant): Boolean; overload;
    function EjecutarQuery(varQuery: string): TDataSet;
    function EjecutarQueryCliDs(varQuery: string): TClientDataSet; 
  end;

var
  BD: TBD;

implementation

uses
  PModule;

{$R *.dfm}

{ TBD }

procedure TBD.DataModuleCreate(Sender: TObject);
var
  Ini: TIniFile;
  i: Integer;
  StrConf: TStringList;
begin
  StrConf := TStringList.Create;
  Ini := TIniFile.Create(ExtractFilePath(Application.ExeName) + '\sophie.ini');
  try
    ZCnn.Catalog := Ini.ReadString('bd','schema','public');
    ZCnn.Database := Ini.ReadString('bd','database','sophiebd');
    ZCnn.HostName := Ini.ReadString('bd','hostname','localhost');
    ZCnn.Password := Ini.ReadString('bd','password','postgres');
    ZCnn.Port := StrToIntDef(Ini.ReadString('bd','port','5432'),5432);
    ZCnn.Protocol := Ini.ReadString('bd','protocol','postgresql');
    ZCnn.User := Ini.ReadString('bd','user','postgres');
    //Asignando Opciones de Conexion segun la Seccion "options_bd" (Caracteres Especiales entre otros parametros)
    Ini.ReadSectionValues('options_bd',StrConf);
    for i := 0 to StrConf.Count - 1 do
    begin
      ZCnn.Properties.Values[StrConf.Names[i]] := StrConf.ValueFromIndex[i];
    end;
    ZCnn.Connect;
  except
    on E: Exception do
    begin
      P.ErrorLog('PModule',E.Message);
      MessageBox(0, 'El Sistema no puede ser Iniciado'+#13+#10+#13+#10+'Verifique los Parámetros de Configuración y vuelva a intentar iniciar el sistema.', 'SGS - Error', MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      Application.Terminate;
    end;
  end;
  ZCnn.Disconnect;
  Esquema := ZCnn.Catalog;
  BaseDatos := ZCnn.Database;
  Host := ZCnn.HostName;
  Password := ZCnn.Password;
  Puerto := ZCnn.Port;
  Protocolo := ZCnn.Protocol;
  Usuario := ZCnn.User;
  Opciones := TStringList.Create;
  Opciones := StrConf;
  Ini.Free;
  ZQry.Connection := ZCnn;
end;

function TBD.ZQryToClientDataSet: TClientDataSet;
var
  CliDs: TClientDataSet;
  i: Integer;
  DefCampo: TFieldDef;
begin
  //Configurando ClientDataSet
  CliDs := TClientDataSet.Create(nil);
  with CliDs do
  begin
    for i := 0 to ZQry.FieldCount - 1 do
    begin
      DefCampo := ZQry.FieldDefs[i];
      FieldDefs.Add(DefCampo.Name,DefCampo.DataType,DefCampo.Size);
    end;
    CreateDataset;
    Open;
    ZQry.First;
    while (not ZQry.EOF) do
    begin
      Append;
      for i := 0 to ZQry.FieldCount - 1 do
      begin
        FieldValues[Fields[i].FieldName] := ZQry.FieldValues[Fields[i].FieldName];
      end;
      Post;
      ZQry.Next;
    end;
  end;
  Result := CliDs;
end;

procedure TBD.SetHost(varHost: string);
begin
  ZCnn.HostName := varHost;
  Host := varHost;
end;

procedure TBD.SetUsuario(varUsuario: string);
begin
  ZCnn.User := varUsuario;
  Usuario := varUsuario;
end;

procedure TBD.SetPassword(varPassword: string);
begin
  ZCnn.Password := varPassword;
  Password := varPassword;
end;

procedure TBD.SetEsquema(varEsquema: string);
begin
  ZCnn.Catalog := varEsquema;
  Esquema := varEsquema;
end;

procedure TBD.SetBaseDatos(varBaseDatos: string);
begin
  ZCnn.Database := varBaseDatos;
  BaseDatos := varBaseDatos;
end;

procedure TBD.SetProtocolo(varProtocolo: string);
begin
  ZCnn.Protocol := varProtocolo;
  Protocolo := varProtocolo;
end;

procedure TBD.SetPuerto(varPuerto: Integer);
begin
  ZCnn.Port := varPuerto;
  Puerto := varPuerto;
end;

function TBD.GetHost: string;
begin
  Result := Host;
end;

function TBD.GetUsuario: string;
begin
  Result := Usuario;
end;

function TBD.GetPassword: string;
begin
  Result := Password;
end;

function TBD.GetEsquema: string;
begin
  Result := Esquema;
end;

function TBD.GetBaseDatos: string;
begin
  Result := BaseDatos;
end;

function TBD.GetProtocolo: string;
begin
  Result := Protocolo;
end;

function TBD.GetPuerto: Integer;
begin
  Result := Puerto;
end;

function TBD.Conectar: boolean;
var
  i: Integer;
begin
  Result := True;
  ZCnn.Protocol := Protocolo;
  ZCnn.HostName := Host;
  ZCnn.User := Usuario;
  ZCnn.Password := Password;
  ZCnn.Catalog := Esquema;
  ZCnn.Database := BaseDatos;
  ZCnn.Port := Puerto;
  //Asignando Opciones de Conexion segun la Seccion "options_bd" (Caracteres Especiales entre otros parametros)
  for i := 0 to Opciones.Count - 1 do
  begin
    ZCnn.Properties.Values[Opciones.Names[i]] := Opciones.ValueFromIndex[i];
  end;
  if (not ZCnn.Connected) then
  begin
    try
      ZCnn.Connect;
    except
      on E: Exception do
      begin
        P.ErrorLog('PModule',E.Message);
        Result := False;
      end;
    end;
  end;
end;

function TBD.Desconectar: boolean;
begin
  Result := True;
  if (ZCnn.Connected) then
  begin
    try
      ZCnn.Disconnect;
    except
      on E: Exception do
      begin
        P.ErrorLog('PModule',E.Message);
        Result := False;
      end;
    end;
  end;
end;

function TBD.EjecutarNonQuery(varQuery: string): Boolean;
begin
  Conectar;
  with ZRQry do
  begin
    Connection := ZCnn;
    SQL.Text := varQuery;
    ExecSQL;
    Result := (RowsAffected > 0);
  end;
  Desconectar;
end;

function TBD.EjecutarNonQuery(varQuery: string; varParams: TArrayVariant
 ): Boolean;
var
  i: Integer;
  Query, RS: string;
begin
  Query := varQuery;
  for i := High(varParams) downto Low(varParams) do
  begin
    //Encontrando el Tipo de Dato de la Variable Variant
    RS := varParams[i];
    case (VarType(varParams[i]) and VarTypeMask) of
      //Tipo String
      varString{$IFDEF DELPHI10}, varUString{$ENDIF}:
      begin
        RS := QuotedStr(varParams[i]);
      end;
      //Tipo DateTime
      varDate:
      begin
        //Verificando Tipo TDate
        if ((Int(VarToDateTime(varParams[i])) > 0) and (Frac(VarToDateTime(varParams[i])) > 0)) then //Hay Ambos (Tipo FechaHora o TDateTime)
        begin
          RS := QuotedStr(FormatDateTime('yyyy-mm-dd hh:nn:ss',VarToDateTime(varParams[i])));
        end
        else
        begin
          if (Frac(VarToDateTime(varParams[i])) > 0) then //Hay Hora
          begin
            RS := QuotedStr(FormatDateTime('hh:nn:ss',VarToDateTime(varParams[i])));
          end
          else //Hay Fecha
          begin
            RS := QuotedStr(FormatDateTime('yyyy-mm-dd',VarToDateTime(varParams[i])));
          end;
        end;
      end;
    end;
    Query := StringReplace(Query,'$' + IntToStr(i + 1),RS,[]);
  end;
  Result := EjecutarNonQuery(Query);
end;

function TBD.EjecutarQuery(varQuery: string): TDataSet;
begin
  Conectar;
  with ZQry do
  begin
    Connection := ZCnn;
    SQL.Text := varQuery;
    Open;
    Result := ZQry;
    Close;
  end;
  Desconectar;
end;

function TBD.EjecutarQueryCliDs(varQuery: string): TClientDataSet;
begin
  Conectar;
  with ZQry do
  begin
    Connection := ZCnn;
    SQL.Text := varQuery;
    Open;
    Result := ZQryToClientDataSet;
    Close;
  end;
  Desconectar;
end;

end.
