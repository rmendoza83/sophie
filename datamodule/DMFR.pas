unit DMFR;

interface

uses
  SysUtils, Classes, frxClass, ClsParametro, Printers, Windows,
  frxZEOSComponents, frxPrinter;

const
  ConnectionID = 'SophieBD';
  QueryID = '';
  FastReportSophieExtension = '.sfr';
  FastReportFolder = 'reportes';

type
  //Tipo Registro para las Modificaciones SQL Pre-Emision de Reportes
  TModDDVSQL = record
    Nombre: string;
    Valor: string;
  end;
  TArrayModDDVSQL = array of TModDDVSQL;
  //Tipo Registro para la coleccion de Parametros de Reportes en Fast Report
  TParametroFastReport = record
    Nombre: string;
    Valor: string;
  end;
  TArrayParametro = array of TParametroFastReport;

  TTipoReporte = (trGeneral, trDocumento);
  TDMFastReport = class(TDataModule)
    frxSophieReport: TfrxReport;
  private
    { Private declarations }
    ArrayModDDVSQL: TArrayModDDVSQL;
    Parametros: TArrayParametro;
    Parametro: TParametro;
    ReporteActivo: string;
    TipoReporte: TTipoReporte;
    ImprimirDirectamente: Boolean;
    ImpresoraPredeterminadaWindows: string;
    ImpresoraGeneral: string;
    ImpresoraDocumento: string;
    IndiceImpresoraPredeterminadaWindows: Integer;
    IndiceImpresoraGeneral: Integer;
    IndiceImpresoraDocumento: Integer;
    ConfigurarImpresionGeneral: Boolean;
    procedure ActualizaPathBDFastReport;
    procedure ActualizaDDVSQL;
    procedure AgregarParametrosProyecto;
    procedure AsignarParametro(NombreParametro, Valor: string);
    procedure EnviarParametrosFastReport;
    function VerificaRecordCountSql: Boolean;
    procedure BuscarDisplayFormatZEOSQuery(NumeroDecimales: Integer);
    procedure BuscarDisplayFormatFastReportExtendido(Component: TComponent; NumeroDecimales: Integer);
    procedure BuscarDisplayFormatFastReport(NumeroDecimales: Integer);
    procedure BuscarDisplayFormat(NumeroDecimales: Integer);
    function EmiteReporte(NombreReporte: string): Boolean; overload;
    function ImprimirReporte(NombreReporte: string): Boolean; overload;
    function ObtenerImpresoraPredeterminadaWindows: string;
    procedure ObtenerParametrosImpresion;
    function ObtenerPathReporte(NombreReporte: string): string;
  public
    { Public declarations }
    constructor Construir(AOwner: TComponent);
    function EmiteReporte(NombreReporte: String; varTipoReporte: TTipoReporte): Boolean; overload;
    function ImprimirReporte(NombreReporte: String; varTipoReporte: TTipoReporte): Boolean; overload;
    procedure AgregarModDDVSQL(Nombre: string; Valor: string);
    procedure AgregarParametro(varNombre: string; varValor: string);
  end;

var
  DMFastReport: TDMFastReport;

implementation

uses
  Forms,
  Controls,
  DMBD,
  PModule,
  Utils;

{$R *.dfm}

{ TDMFastReport }

procedure TDMFastReport.ActualizaPathBDFastReport;
var
  frxZEOSDatabase: TfrxZEOSDatabase;
begin
  frxZEOSDatabase := frxSophieReport.FindComponent(ConnectionID) as TfrxZEOSDatabase;
  if (Assigned(frxZEOSDatabase)) then
  begin
    with frxZEOSDatabase do
    begin
      Protocol := BD.GetProtocolo;
      HostName := BD.GetHost;
      Port := BD.GetPuerto;
      DatabaseName := BD.GetBaseDatos;
      User := BD.GetUsuario;
      Password := BD.GetPassword;
      Catalog := BD.GetEsquema;
    end;
  end;
end;

procedure TDMFastReport.ActualizaDDVSQL;
var
  i: Integer;
  frxZEOSQuery: TfrxZEOSQuery;
begin
  for i := Low(ArrayModDDVSQL) to High(ArrayModDDVSQL) do
  begin
    frxZEOSQuery := frxSophieReport.FindComponent(ArrayModDDVSQL[i].Nombre) as TfrxZEOSQuery;
    if Assigned(frxZEOSQuery) then
    begin
      frxZEOSQuery.SQL.Text := ArrayModDDVSQL[i].Valor;
    end;
  end;
end;

procedure TDMFastReport.AgregarParametrosProyecto;
var
  Parametro: TParametro;
begin
  Parametro := TParametro.Create;
  //Enviando Parametros Generales del Sistema al Reporte de Fast Report
  frxSophieReport.Variables.Clear;
  //Verificando si el Sistema Esta Registrado
  if (P.SophieRegistrado) then
  begin
    AsignarParametro(Cadena('NombreEmpresa'),P.GetSophieRegisteredUser);
  end
  else
  begin
    AsignarParametro(Cadena('NombreEmpresa'),Parametro.ObtenerValor(Cadena('nombre_empresa')));
  end;
  AsignarParametro(Cadena('RifEmpresa'),Parametro.ObtenerValor(Cadena('rif_empresa')));
  AsignarParametro(Cadena('NitEmpresa'),Parametro.ObtenerValor(Cadena('nit_empresa')));
  AsignarParametro(Cadena('DireccionEmpresa'),Parametro.ObtenerValor(Cadena('direccion_empresa')));
  AsignarParametro(Cadena('IPEmisor'),P.DireccionIP);
  AsignarParametro(Cadena('NombreHost'),P.HostName);
  AsignarParametro(Cadena('LoginUsuario'),P.User.GetLoginUsuario);
  AsignarParametro(Cadena('NombreUsuario'),P.User.GetApellidos + ', ' + P.User.GetNombres);
end;

procedure TDMFastReport.AsignarParametro(NombreParametro, Valor: String);
begin
  with frxSophieReport do
  begin
    Variables[NombreParametro] := QuotedStr(Valor);
  end;
end;

procedure TDMFastReport.EnviarParametrosFastReport;
var
  i: Integer;
begin
  AgregarParametrosProyecto;
  for i := Low(Parametros) to High(Parametros) do
  begin
    AsignarParametro(Parametros[i].Nombre,Parametros[i].Valor);
  end;
end;

function TDMFastReport.VerificaRecordCountSql: Boolean;
begin
  Result := True;
(*
var
  ReporteDataView: TReporteDataView;
  RaveDriverDataView: TRaveDriverDataView;
  Query: string;
  ParamName: string;
  ParamValue: string;
  i: Integer;
  Ds: TClientDataSet;
begin
  Result := True;
  Query := '';
  //Ubicando DataView del Reporte Activo
  ReporteDataView := TReporteDataView.Create;
  ReporteDataView.SetNombreReporte(ReporteActivo);
  if (ReporteDataView.BuscarNombreReporte) then
  begin
    //Obteniendo Objeto DataView
    EnviarParametrosRave;
    ActualizaPathBDRave;
    ActualizaDDVSQL;
    RaveDriverDataView := TRaveDriverDataView(RvProyecto.ProjMan.FindRaveComponent(ReporteDataView.GetNombreDataview,nil));
    if (Assigned(RaveDriverDataView)) then
    begin
      Query := RaveDriverDataView.Query;
      //Asignando Valores de los Parametros dentro del Query
      for i := 0 to RaveDriverDataView.QueryParams.Count - 1 do
      begin
        ParamName := Trim(RaveDriverDataView.QueryParams.Names[i]);
        ParamValue := Trim(RvProyecto.GetParam(ParamName));
        Query := StringReplace(Query,':' + ParamName,ParamValue,[rfReplaceAll]);
      end;
    end;
    Application.ProcessMessages;
    //Verificando que el Query posea registros
    Ds := BD.EjecutarQueryCliDs(Query);
    Result := (Ds.RecordCount > 0);
    FreeAndNil(Ds);
  end;
*)
end;

procedure TDMFastReport.BuscarDisplayFormat(NumeroDecimales: Integer);
begin
  //Realizando Cambios en los Componentes internos al DriverDataView
  BuscarDisplayFormatZEOSQuery(NumeroDecimales);
  //Realizando Cambios en los Componentes internos al RaveReport
  BuscarDisplayFormatFastReport(NumeroDecimales);
end;

function TDMFastReport.EmiteReporte(NombreReporte: String): Boolean;
begin
  ReporteActivo := NombreReporte;
  Result := False;
  if (FileExists(ObtenerPathReporte(NombreReporte))) then
  begin
    frxSophieReport.LoadFromFile(ObtenerPathReporte(NombreReporte));
    Result := VerificaRecordCountSql;
    if (Result) then
    begin
      with frxSophieReport do
      begin
        Screen.Cursor := crHourGlass;
        AgregarParametro('NombreReporte',NombreReporte);
        EnviarParametrosFastReport;
        ActualizaPathBDFastReport;
        ActualizaDDVSQL;
        Parametro.SetNombreParametro('numero_decimales');
        Parametro.Buscar;
        BuscarDisplayFormat(Parametro.ObtenerValorAsInteger);
        PrepareReport(True);
        PrintOptions.ShowDialog := False;
        if (TipoReporte = trGeneral) then
        begin
          frxPrinters.PrinterIndex := IndiceImpresoraGeneral;
          PrintOptions.Printer := frxPrinters[IndiceImpresoraGeneral].Name;
        end
        else
        begin
          frxPrinters.PrinterIndex := IndiceImpresoraDocumento;
          PrintOptions.Printer := frxPrinters[IndiceImpresoraDocumento].Name;
        end;
        SelectPrinter;
        ShowPreparedReport;
        SetLength(ArrayModDDVSQL,0);
        SetLength(Parametros,0);
      end;
    end;
  end
  else
  begin
    MessageBox(Application.Handle, Cadena('El Reporte Seleccionado No Existe!!! Consulte Con Servicio T�cnico'), PChar(MsgTituloInformacion), MB_ICONEXCLAMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end;
  Screen.Cursor := crDefault;
end;

function TDMFastReport.ImprimirReporte(NombreReporte: String): Boolean;
begin
  ReporteActivo := NombreReporte;
  Result := False;
  if (FileExists(ObtenerPathReporte(NombreReporte))) then
  begin
    frxSophieReport.LoadFromFile(ObtenerPathReporte(NombreReporte));
    Result := VerificaRecordCountSql;
    if (Result) then
    begin
      with frxSophieReport do
      begin
        Screen.Cursor := crHourGlass;
        AgregarParametro('NombreReporte',NombreReporte);
        EnviarParametrosFastReport;
        ActualizaPathBDFastReport;
        ActualizaDDVSQL;
        Parametro.SetNombreParametro('numero_decimales');
        Parametro.Buscar;
        BuscarDisplayFormat(Parametro.ObtenerValorAsInteger);
        PrepareReport(True);
        PrintOptions.ShowDialog := not ImprimirDirectamente;
        if (TipoReporte = trGeneral) then
        begin
          frxPrinters.PrinterIndex := IndiceImpresoraGeneral;
          PrintOptions.Printer := frxPrinters[IndiceImpresoraGeneral].Name;
        end
        else
        begin
          frxPrinters.PrinterIndex := IndiceImpresoraDocumento;
          PrintOptions.Printer := frxPrinters[IndiceImpresoraDocumento].Name;
        end;
        SelectPrinter;
        Print;
        SetLength(ArrayModDDVSQL,0);
        SetLength(Parametros,0);
      end;
    end;
  end
  else
  begin
    MessageBox(Application.Handle, Cadena('El Reporte Seleccionado No Existe!!! Consulte Con Servicio T�cnico'), PChar(MsgTituloInformacion), MB_ICONEXCLAMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end;
  Screen.Cursor := crDefault;
(*
  ReporteActivo := NombreReporte;
  Result := VerificaRecordCountSql;
  if (Result) then
  begin
    Screen.Cursor := crHourGlass;
    AgregarParametro('NombreReporte',NombreReporte);
    { Configurando Parametros de Salida del Reporte }
    with RvMotor do
    begin
      SystemSetups := SystemSetups - [ssAllowSetup, ssAllowPrinterSetup, ssAllowPreviewSetup];
      DefaultDest := rdPrinter;
    end;
    { Mostrando Reporte }
    with RvProyecto do
    begin
      EnviarParametrosRave;
      ActualizaPathBDRave;
      ActualizaDDVSQL;
      Parametro.SetNombreParametro('numero_decimales');
      Parametro.Buscar;
      BuscarDisplayFormat(Parametro.ObtenerValorAsInteger);
      if (SelectReport(NombreReporte,False)) then
      begin
        if (TipoReporte = trGeneral) then
        begin
          RPDev.DeviceIndex := IndiceImpresoraGeneral;
        end
        else
        begin
          RPDev.DeviceIndex := IndiceImpresoraDocumento;
        end;
        //Configurando Opciones de Impresion segun Configuracion del Sistema
        if (not ImprimirDirectamente) then
        begin
          RvMotor.SystemSetups := RvMotor.SystemSetups + [ssAllowSetup, ssAllowPreviewSetup];
        end;
        Execute;
        SetLength(ArrayModDDVSQL,0);
        SetLength(Parametros,0);
      end
      else
      begin
        MessageBox(Application.Handle, Cadena('El Reporte Seleccionado No Existe!!! Consulte Con Servicio T�cnico'), PChar(MsgTituloInformacion), MB_ICONEXCLAMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      end;
    end;
    Screen.Cursor := crDefault;
  end;
*)
end;

function TDMFastReport.ObtenerImpresoraPredeterminadaWindows: string;
var
  ResStr: array[0..255] of Char;
begin
  GetProfileString('Windows', 'device', '', ResStr, 255);
  Result := StrPas(ResStr);
  Result := Copy(Result,0,Pos(',',Result) - 1);
end;

procedure TDMFastReport.ObtenerParametrosImpresion;
var
  Printer: TPrinter;
begin
  //Configurando Impresoras a Utilizar
  Printer := Printers.Printer;
  ImpresoraPredeterminadaWindows := ObtenerImpresoraPredeterminadaWindows;
  IndiceImpresoraPredeterminadaWindows := Printer.Printers.IndexOf(ImpresoraPredeterminadaWindows);
  //Impresora General
  Parametro.SetNombreParametro('impresora_general');
  if (Parametro.Buscar) then
  begin
    if (Length(Trim(Parametro.ObtenerValorAsString)) > 0) then
    begin
      ImpresoraGeneral := Trim(Parametro.ObtenerValorAsString);
      if (Printer.Printers.IndexOf(ImpresoraGeneral) <> -1)  then
      begin
        IndiceImpresoraGeneral := Printer.Printers.IndexOf(ImpresoraGeneral);
      end
      else
      begin
        ImpresoraGeneral := ImpresoraPredeterminadaWindows;
        IndiceImpresoraGeneral := IndiceImpresoraPredeterminadaWindows;
      end;
    end
    else
    begin
      ImpresoraGeneral := ImpresoraPredeterminadaWindows;
      IndiceImpresoraGeneral := IndiceImpresoraPredeterminadaWindows;
    end;
  end
  else
  begin
    ImpresoraGeneral := ImpresoraPredeterminadaWindows;
    IndiceImpresoraGeneral := IndiceImpresoraPredeterminadaWindows;
  end;
  //Impresora Documento
  Parametro.SetNombreParametro('impresora_documento');
  if (Parametro.Buscar) then
  begin
    if (Length(Trim(Parametro.ObtenerValorAsString)) > 0) then
    begin
      //Validar si la impresora es correcta OJO!!!
      ImpresoraDocumento := Trim(Parametro.ObtenerValorAsString);
      if (Printer.Printers.IndexOf(ImpresoraDocumento) <> -1)  then
      begin
        IndiceImpresoraDocumento := Printer.Printers.IndexOf(ImpresoraDocumento);
      end
      else
      begin
        ImpresoraDocumento := ImpresoraPredeterminadaWindows;
        IndiceImpresoraDocumento := IndiceImpresoraPredeterminadaWindows;
      end;
    end
    else
    begin
      ImpresoraDocumento := ImpresoraPredeterminadaWindows;
      IndiceImpresoraDocumento := IndiceImpresoraPredeterminadaWindows;
    end;
  end
  else
  begin
    ImpresoraDocumento := ImpresoraPredeterminadaWindows;
    IndiceImpresoraDocumento := IndiceImpresoraPredeterminadaWindows;
  end;
  //Imprimir Directamente
  Parametro.SetNombreParametro('imprimir_directamente');
  if (Parametro.Buscar) then
  begin
    ImprimirDirectamente := Parametro.ObtenerValorAsBoolean;
  end
  else
  begin
    ImprimirDirectamente := False;
  end;
  //Configurar Impresion General
  Parametro.SetNombreParametro('configurar_impresion_general');
  if (Parametro.Buscar) then
  begin
    ConfigurarImpresionGeneral := Parametro.ObtenerValorAsBoolean;
  end
  else
  begin
    ConfigurarImpresionGeneral := False;
  end;
  TipoReporte := trGeneral;
end;

function TDMFastReport.ObtenerPathReporte(NombreReporte: string): string;
begin
  Result := ExtractFilePath(Application.ExeName) + FastReportFolder + '\' + NombreReporte + FastReportSophieExtension;
end;


procedure TDMFastReport.BuscarDisplayFormatZEOSQuery(NumeroDecimales: Integer);
begin

end;

procedure TDMFastReport.BuscarDisplayFormatFastReportExtendido(
  Component: TComponent; NumeroDecimales: Integer);
begin

end;

procedure TDMFastReport.BuscarDisplayFormatFastReport(NumeroDecimales: Integer);
begin

end;

constructor TDMFastReport.Construir(AOwner: TComponent);
begin
  inherited Create(AOwner);
  SetLength(ArrayModDDVSQL,0);
  SetLength(Parametros,0);
  Parametro := TParametro.Create;
  ObtenerParametrosImpresion;
end;

function TDMFastReport.EmiteReporte(NombreReporte: String;
  varTipoReporte: TTipoReporte): Boolean;
begin
  ObtenerParametrosImpresion;
  TipoReporte := varTipoReporte;
  Result := EmiteReporte(NombreReporte);
end;

function TDMFastReport.ImprimirReporte(NombreReporte: String;
  varTipoReporte: TTipoReporte): Boolean;
begin
  ObtenerParametrosImpresion;
  TipoReporte := varTipoReporte;
  Result := ImprimirReporte(NombreReporte);
end;

procedure TDMFastReport.AgregarModDDVSQL(Nombre, Valor: string);
begin
  SetLength(ArrayModDDVSQL,Length(ArrayModDDVSQL) + 1);
  ArrayModDDVSQL[Length(ArrayModDDVSQL) - 1].Nombre := Nombre;
  ArrayModDDVSQL[Length(ArrayModDDVSQL) - 1].Valor := Valor;
end;

procedure TDMFastReport.AgregarParametro(varNombre, varValor: string);
begin
  SetLength(Parametros,Length(Parametros) + 1);
  with Parametros[Length(Parametros) - 1] do
  begin
    Nombre := varNombre;
    Valor := varValor;
  end;
end;

end.
