unit DMRR;

{$I sophie.inc}

interface

{$IFDEF USE_RAVE_REPORTS}

uses
  SysUtils, Classes, Forms, Controls, RpRenderJPEG, RpRenderMetafile,
  RpRenderCanvas, RpRenderBitmap, RpRenderText, RpRenderRTF, RpRenderHTML,
  RpRender, RpRenderPDF, RpBase, RpSystem, RpRave, RpDefine, ClsParametro,
  Printers, Windows;

const
  DatabaseID = 'SOPHIEBD';

type
  //Tipo Registro para las Modificaciones SQL Pre-Emision de Reportes
  TModDDVSQL = record
    Nombre: string;
    Valor: string;
  end;
  TArrayModDDVSQL = array of TModDDVSQL;
  //Tipo Registro para la coleccion de Parametros de Reportes en Rave
  TParametroRave = record
    Nombre: string;
    Valor: string;
  end;
  TArrayParametro = array of TParametroRave;

  TTipoReporte = (trGeneral, trDocumento);

  TDMRave = class(TDataModule)
    RvProyecto: TRvProject;
    RvMotor: TRvSystem;
    RvRenderPDF: TRvRenderPDF;
    RvRenderHTML: TRvRenderHTML;
    RvRenderRTF: TRvRenderRTF;
    RvRenderTEXT: TRvRenderText;
    RvRenderBMP: TRvRenderBitmap;
    RvRenderWMF: TRvRenderMetafile;
    RvRenderJPG: TRvRenderJpeg;
    RvRenderEMF: TRvRenderMetafile;
    procedure RvMotorOverridePreview(ReportSystem: TRvSystem;
      OverrideMode: TOverrideMode; var OverrideForm: TForm);
    procedure RvMotorOverrideSetup(ReportSystem: TRvSystem;
      OverrideMode: TOverrideMode; var OverrideForm: TForm);
    procedure RvMotorOverrideStatus(ReportSystem: TRvSystem;
      OverrideMode: TOverrideMode; var OverrideForm: TForm);
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    ArrayModDDVSQL: TArrayModDDVSQL;
    Parametros: TArrayParametro;
    Parametro: TParametro;
    ReporteActivo: string;
    TipoReporte: TTipoReporte;
    ImprimirDirectamente: Boolean;
    ImpresoraPredeterminadaWindows: string;
    ImpresoraGeneral: string;
    ImpresoraDocumento: string;
    IndiceImpresoraPredeterminadaWindows: Integer;
    IndiceImpresoraGeneral: Integer;
    IndiceImpresoraDocumento: Integer;
    ConfigurarImpresionGeneral: Boolean;
    procedure ActualizaPathBDRave;
    procedure ActualizaDDVSQL;
    procedure AgregarParametrosProyecto;
    procedure AsignarParametro(NombreParametro, Valor: String);
    procedure EnviarParametrosRave;
    function VerificaRecordCountSql: Boolean;
    procedure BuscarDisplayFormatDriverDataView(NumeroDecimales: Integer);
    procedure BuscarDisplayFormatRaveReportExtendido(Component: TComponent; NumeroDecimales: Integer);
    procedure BuscarDisplayFormatRaveReport(NumeroDecimales: Integer);
    procedure BuscarDisplayFormat(NumeroDecimales: Integer);
    function EmiteReporte(NombreReporte: String): Boolean; overload;
    function ImprimirReporte(NombreReporte: String): Boolean; overload;
    function ObtenerImpresoraPredeterminadaWindows: string;
    procedure ObtenerParametrosImpresion;
  public
    { Public declarations }
    constructor Construir(AOwner: TComponent; ArchivoProyecto: String);
    function EmiteReporte(NombreReporte: String; varTipoReporte: TTipoReporte): Boolean; overload;
    function ImprimirReporte(NombreReporte: String; varTipoReporte: TTipoReporte): Boolean; overload;
    procedure AgregarModDDVSQL(Nombre: string; Valor: string);
    procedure AgregarParametro(varNombre: string; varValor: string);
  end;

var
  DMRave: TDMRave;

{$ENDIF}

implementation

{$IFDEF USE_RAVE_REPORTS}

uses
  RvLDCompiler, {Permite Compilar con Delphi Proyectos Realizados en RAVE 7}
  RvDataBase, {Permite la Generacion Dinamica de los Parametros de Conexion a la Base de Datos del Generador de Reportes RAVE 7}
  RvDataLink,
  RvDriverDataView,
  RvProj,
  RvClass,
  Dialogs,
  RavePreview,
  RaveStatus,
  RaveSetup,
  ClsReporteDataview,
  DMBD,
  DBClient,
  PModule,
  Utils,
  TypInfo,
  RPDevice;

{$R *.dfm}

{ TDM }

constructor TDMRave.Construir(AOwner: TComponent; ArchivoProyecto: String);
begin
  inherited Create(AOwner);
  RvProyecto.SetProjectFile(ArchivoProyecto);
  AddDynamicDataLinks('');
  SetLength(ArrayModDDVSQL,0);
  SetLength(Parametros,0);
  Parametro := TParametro.Create;
  ObtenerParametrosImpresion;
end;

procedure TDMRave.DataModuleCreate(Sender: TObject);
begin
  RvProyecto.Open;
end;

procedure TDMRave.DataModuleDestroy(Sender: TObject);
begin
  RvProyecto.Close;
end;

procedure TDMRave.ActualizaPathBDRave;
var
  RaveBD: TRaveDatabase;
begin
  RaveBD := TRaveDatabase(RvProyecto.ProjMan.FindRaveComponent(DatabaseID,nil));
  if (Assigned(RaveBD)) then
    begin
      RaveBD.AuthRun.Datasource := BD.GetBaseDatos;
      RaveBD.AuthRun.Username := BD.GetUsuario;
      RaveBD.AuthRun.Password := BD.GetPassword;
      RaveBD.AuthRun.Options := 'Protocolo=' + BD.GetProtocolo + ',NombreHost=' + BD.GetHost + ',Puerto=' + IntToStr(BD.GetPuerto);
    end;
end;

procedure TDMRave.ActualizaDDVSQL;
var
  i: Integer;
  RaveDriverDataView: TRaveDriverDataView;
begin
  for i := Low(ArrayModDDVSQL) to High(ArrayModDDVSQL) do
  begin
    RaveDriverDataView := TRaveDriverDataView(RvProyecto.ProjMan.FindRaveComponent(ArrayModDDVSQL[i].Nombre,nil));
    if (Assigned(RaveDriverDataView)) then
    begin
      RaveDriverDataView.Query := ArrayModDDVSQL[i].Valor;
    end;
  end;
end;

procedure TDMRave.AgregarParametrosProyecto;
var
  Parametro: TParametro;
begin
  Parametro := TParametro.Create;
  //Enviando Parametros Generales del Sistema a los Parametros Generales del
  //Proyecto de RAVE
  RvProyecto.ClearParams;
  //Verificando si el Sistema Esta Registrado
  if (P.SophieRegistrado) then
  begin
    AsignarParametro(Cadena('NombreEmpresa'),P.GetSophieRegisteredUser);
  end
  else
  begin
    AsignarParametro(Cadena('NombreEmpresa'),Parametro.ObtenerValor(Cadena('nombre_empresa')));
  end;
  AsignarParametro(Cadena('RifEmpresa'),Parametro.ObtenerValor(Cadena('rif_empresa')));
  AsignarParametro(Cadena('NitEmpresa'),Parametro.ObtenerValor(Cadena('nit_empresa')));
  AsignarParametro(Cadena('DireccionEmpresa'),Parametro.ObtenerValor(Cadena('direccion_empresa')));
  AsignarParametro(Cadena('IPEmisor'),P.DireccionIP);
  AsignarParametro(Cadena('NombreHost'),P.HostName);
  AsignarParametro(Cadena('LoginUsuario'),P.User.GetLoginUsuario);
  AsignarParametro(Cadena('NombreUsuario'),P.User.GetApellidos + ', ' + P.User.GetNombres);
end;

procedure TDMRave.AsignarParametro(NombreParametro, Valor: String);
begin
  if (RvProyecto.Active) then
  begin
    RvProyecto.SetParam(NombreParametro,Valor);
  end;
end;

function TDMRave.EmiteReporte(NombreReporte: String;
  varTipoReporte: TTipoReporte): Boolean;
begin
  ObtenerParametrosImpresion;
  TipoReporte := varTipoReporte;
  Result := EmiteReporte(NombreReporte);
end;

procedure TDMRave.EnviarParametrosRave;
var
  i: Integer;
begin
  AgregarParametrosProyecto;
  for i := Low(Parametros) to High(Parametros) do
  begin
    AsignarParametro(Parametros[i].Nombre,Parametros[i].Valor);
  end;
end;

function TDMRave.ImprimirReporte(NombreReporte: String;
  varTipoReporte: TTipoReporte): Boolean;
begin
  ObtenerParametrosImpresion;
  TipoReporte := varTipoReporte;
  Result := ImprimirReporte(NombreReporte);
end;

function TDMRave.ObtenerImpresoraPredeterminadaWindows: string;
var
  ResStr: array[0..255] of Char;
begin
  GetProfileString('Windows', 'device', '', ResStr, 255);
  Result := StrPas(ResStr);
  Result := Copy(Result,0,Pos(',',Result) - 1);
end;

procedure TDMRave.ObtenerParametrosImpresion;
var
  Printer: TPrinter;
begin
//Configurando Impresoras a Utilizar
  Printer := Printers.Printer;
  ImpresoraPredeterminadaWindows := ObtenerImpresoraPredeterminadaWindows;
  IndiceImpresoraPredeterminadaWindows := Printer.Printers.IndexOf(ImpresoraPredeterminadaWindows);
  //Impresora General
  Parametro.SetNombreParametro('impresora_general');
  if (Parametro.Buscar) then
  begin
    if (Length(Trim(Parametro.ObtenerValorAsString)) > 0) then
    begin
      ImpresoraGeneral := Trim(Parametro.ObtenerValorAsString);
      if (Printer.Printers.IndexOf(ImpresoraGeneral) <> -1)  then
      begin
        IndiceImpresoraGeneral := Printer.Printers.IndexOf(ImpresoraGeneral);
      end
      else
      begin
        ImpresoraGeneral := ImpresoraPredeterminadaWindows;
        IndiceImpresoraGeneral := IndiceImpresoraPredeterminadaWindows;
      end;
    end
    else
    begin
      ImpresoraGeneral := ImpresoraPredeterminadaWindows;
      IndiceImpresoraGeneral := IndiceImpresoraPredeterminadaWindows;
    end;
  end
  else
  begin
    ImpresoraGeneral := ImpresoraPredeterminadaWindows;
    IndiceImpresoraGeneral := IndiceImpresoraPredeterminadaWindows;
  end;
  //Impresora Documento
  Parametro.SetNombreParametro('impresora_documento');
  if (Parametro.Buscar) then
  begin
    if (Length(Trim(Parametro.ObtenerValorAsString)) > 0) then
    begin
      //Validar si la impresora es correcta OJO!!!
      ImpresoraDocumento := Trim(Parametro.ObtenerValorAsString);
      if (Printer.Printers.IndexOf(ImpresoraDocumento) <> -1)  then
      begin
        IndiceImpresoraDocumento := Printer.Printers.IndexOf(ImpresoraDocumento);
      end
      else
      begin
        ImpresoraDocumento := ImpresoraPredeterminadaWindows;
        IndiceImpresoraDocumento := IndiceImpresoraPredeterminadaWindows;
      end;
    end
    else
    begin
      ImpresoraDocumento := ImpresoraPredeterminadaWindows;
      IndiceImpresoraDocumento := IndiceImpresoraPredeterminadaWindows;
    end;
  end
  else
  begin
    ImpresoraDocumento := ImpresoraPredeterminadaWindows;
    IndiceImpresoraDocumento := IndiceImpresoraPredeterminadaWindows;
  end;
  //Imprimir Directamente
  Parametro.SetNombreParametro('imprimir_directamente');
  if (Parametro.Buscar) then
  begin
    ImprimirDirectamente := Parametro.ObtenerValorAsBoolean;
  end
  else
  begin
    ImprimirDirectamente := False;
  end;
  //Configurar Impresion General
  Parametro.SetNombreParametro('configurar_impresion_general');
  if (Parametro.Buscar) then
  begin
    ConfigurarImpresionGeneral := Parametro.ObtenerValorAsBoolean;
  end
  else
  begin
    ConfigurarImpresionGeneral := False;
  end;
  TipoReporte := trGeneral;
end;

function TDMRave.EmiteReporte(NombreReporte: String): Boolean;
begin
  ReporteActivo := NombreReporte;
  Result := VerificaRecordCountSql;
  if (Result) then
  begin
    Screen.Cursor := crHourGlass;
    AgregarParametro('NombreReporte',NombreReporte);
    { Configurando Parametros de Salida del Reporte }
    with RvMotor do
    begin
      SystemSetups := SystemSetups - [ssAllowSetup, ssAllowPrinterSetup, ssAllowPreviewSetup];
      DefaultDest := rdPreview;
    end;
    { Mostrando Reporte }
    with RvProyecto do
    begin
      EnviarParametrosRave;
      ActualizaPathBDRave;
      ActualizaDDVSQL;
      Parametro.SetNombreParametro('numero_decimales');
      Parametro.Buscar;
      BuscarDisplayFormat(Parametro.ObtenerValorAsInteger);
      if (SelectReport(NombreReporte,False)) then
      begin
        if (TipoReporte = trGeneral) then
        begin
          RPDev.DeviceIndex := IndiceImpresoraGeneral;
        end
        else
        begin
          RPDev.DeviceIndex := IndiceImpresoraDocumento;
        end;
        //Configurando Opciones de Impresion segun Configuracion del Sistema
        if (ConfigurarImpresionGeneral) then
        begin
          RvMotor.SystemSetups := RvMotor.SystemSetups + [ssAllowSetup, ssAllowPrinterSetup, ssAllowPreviewSetup];
        end;
        Execute;
        SetLength(ArrayModDDVSQL,0);
        SetLength(Parametros,0);
      end
      else
      begin
        MessageBox(Application.Handle, Cadena('El Reporte Seleccionado No Existe!!! Consulte Con Servicio T�cnico'), PChar(MsgTituloInformacion), MB_ICONEXCLAMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      end;
    end;
    Screen.Cursor := crDefault;
  end;
end;

function TDMRave.ImprimirReporte(NombreReporte: String): Boolean;
begin
  ReporteActivo := NombreReporte;
  Result := VerificaRecordCountSql;
  if (Result) then
  begin
    Screen.Cursor := crHourGlass;
    AgregarParametro('NombreReporte',NombreReporte);
    { Configurando Parametros de Salida del Reporte }
    with RvMotor do
    begin
      SystemSetups := SystemSetups - [ssAllowSetup, ssAllowPrinterSetup, ssAllowPreviewSetup];
      DefaultDest := rdPrinter;
    end;
    { Mostrando Reporte }
    with RvProyecto do
    begin
      EnviarParametrosRave;
      ActualizaPathBDRave;
      ActualizaDDVSQL;
      Parametro.SetNombreParametro('numero_decimales');
      Parametro.Buscar;
      BuscarDisplayFormat(Parametro.ObtenerValorAsInteger);
      if (SelectReport(NombreReporte,False)) then
      begin
        if (TipoReporte = trGeneral) then
        begin
          RPDev.DeviceIndex := IndiceImpresoraGeneral;
        end
        else
        begin
          RPDev.DeviceIndex := IndiceImpresoraDocumento;
        end;
        //Configurando Opciones de Impresion segun Configuracion del Sistema
        if (not ImprimirDirectamente) then
        begin
          RvMotor.SystemSetups := RvMotor.SystemSetups + [ssAllowSetup, ssAllowPreviewSetup];
        end;
        Execute;
        SetLength(ArrayModDDVSQL,0);
        SetLength(Parametros,0);
      end
      else
      begin
        MessageBox(Application.Handle, Cadena('El Reporte Seleccionado No Existe!!! Consulte Con Servicio T�cnico'), PChar(MsgTituloInformacion), MB_ICONEXCLAMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      end;
    end;
    Screen.Cursor := crDefault;
  end;
end;

procedure TDMRave.AgregarModDDVSQL(Nombre: string; Valor: string);
begin
  SetLength(ArrayModDDVSQL,Length(ArrayModDDVSQL) + 1);
  ArrayModDDVSQL[Length(ArrayModDDVSQL) - 1].Nombre := Nombre;
  ArrayModDDVSQL[Length(ArrayModDDVSQL) - 1].Valor := Valor;
end;

procedure TDMRave.AgregarParametro(varNombre: string; varValor: string);
begin
  SetLength(Parametros,Length(Parametros) + 1);
  with Parametros[Length(Parametros) - 1] do
  begin
    Nombre := varNombre;
    Valor := varValor;
  end;
end;

procedure TDMRave.RvMotorOverridePreview(ReportSystem: TRvSystem;
  OverrideMode: TOverrideMode; var OverrideForm: TForm);
begin
  case OverrideMode of
    omCreate:
      begin
        OverrideForm := TFormRavePreview.Create(Self);
        OverrideForm.Caption := ReportSystem.TitlePreview + ' [ ' + RvProyecto.ReportName + ' ]';
        OverrideForm.Width := ReportSystem.SystemPreview.FormWidth;
        OverrideForm.Height := ReportSystem.SystemPreview.FormHeight;
        OverrideForm.WindowState := ReportSystem.SystemPreview.FormState;
       (OverrideForm as TFormRavePreview).ReportSystem := ReportSystem;
      end;
    omShow:
      begin
        ReportSystem.SystemPreview.InitPreview((OverrideForm as TFormRavePreview).RvRenderPreview);
        if Assigned(ReportSystem.OnPreviewSetup) then
          begin
            ReportSystem.OnPreviewSetup((OverrideForm as TFormRavePreview).RvRenderPreview);
          end;
        (OverrideForm as TFormRavePreview).InputFileName := ReportSystem.SystemFiler.Filename;
        (OverrideForm as TFormRavePreview).InputStream := ReportSystem.SystemFiler.Stream;
        (OverrideForm as TFormRavePreview).InitFromRPSystem;
        if soPreviewModal In ReportSystem.SystemOptions then
          begin
            OverrideForm.ShowModal
          end
        else
          begin
            OverrideForm.Show;
          end;
      end;
    omWait:
      begin
        if not (soPreviewModal in ReportSystem.SystemOptions) then
          begin
            // Espera para cerrar
            repeat
              Sleep(350);
              Application.ProcessMessages;
            until (not OverrideForm.Visible);
          end;
      end;
    omFree:
      begin
        if (ReportSystem.SystemFiler.StreamMode In [smTempFile, smFile]) then
          begin
            (OverrideForm As TFormRavePreview).RvRenderPreview.NDRStream.Free;
            (OverrideForm As TFormRavePreview).RvRenderPreview.NDRStream := nil;
          end;
        FreeAndNil(OverrideForm);
      end;
  end;
end;

procedure TDMRave.RvMotorOverrideSetup(ReportSystem: TRvSystem;
  OverrideMode: TOverrideMode; var OverrideForm: TForm);
begin
  case OverrideMode of
    omCreate:
      begin
        OverrideForm := TFormRaveSetup.Create(Self);
        OverrideForm.Caption := RvMotor.TitleSetup;
        (OverrideForm as TFormRaveSetup).ReportSystem := ReportSystem;
      end;
    omShow:
      begin
        with OverrideForm as TFormRaveSetup, ReportSystem do
          begin
            PreviewSetup := False;
            Aborted := ShowModal = mrCancel;
          end;
      end;
    omWait:
      begin
        {No es necesario esperar a causa del showModal}
      end;
    omFree:
      begin
        FreeAndNil(OverrideForm);
      end;
  end;
end;

procedure TDMRave.RvMotorOverrideStatus(ReportSystem: TRvSystem;
  OverrideMode: TOverrideMode; var OverrideForm: TForm);
begin
  if (not (soShowStatus in ReportSystem.SystemOptions)) then
    begin
      Exit;
    end;
  case OverrideMode of
    omCreate:
      begin
        OverrideForm := TFormRaveStatus.Create(Self);
        OverrideForm.Caption := RvMotor.TitleStatus;
        (OverrideForm as TFormRaveStatus).ReportSystem := ReportSystem;
      end;
    omShow:
      begin
        with OverrideForm as TFormRaveStatus, ReportSystem do
          begin
            BaseReport.StatusLabel := StatusLabel;
            BtnCancelar.Caption := Trans('&Cancelar');
            BtnCancelar.ModalResult := mrCancel;
            Show;
          end;
      end;
    omWait:
      begin
        with OverrideForm as TFormRaveStatus, ReportSystem do
          begin
            if (soWaitForOK in SystemOptions) then
              begin
                BtnCancelar.Caption := Trans('&Aceptar');
                BtnCancelar.ModalResult := mrOk;
                FormClosed := False;
                repeat
                  Sleep(350);
                  Application.ProcessMessages;
                until FormClosed;
              end;
          end;
      end;
    omFree:
      begin
        FreeAndNil(OverrideForm);
      end;
  end;
end;

function TDMRave.VerificaRecordCountSql: Boolean;
var
  ReporteDataView: TReporteDataView;
  RaveDriverDataView: TRaveDriverDataView;
  Query: string;
  ParamName: string;
  ParamValue: string;
  i: Integer;
  Ds: TClientDataSet;
begin
  Result := True;
  Query := '';
  //Ubicando DataView del Reporte Activo
  ReporteDataView := TReporteDataView.Create;
  ReporteDataView.SetNombreReporte(ReporteActivo);
  if (ReporteDataView.BuscarNombreReporte) then
  begin
    //Obteniendo Objeto DataView
    EnviarParametrosRave;
    ActualizaPathBDRave;
    ActualizaDDVSQL;
    RaveDriverDataView := TRaveDriverDataView(RvProyecto.ProjMan.FindRaveComponent(ReporteDataView.GetNombreDataview,nil));
    if (Assigned(RaveDriverDataView)) then
    begin
      Query := RaveDriverDataView.Query;
      //Asignando Valores de los Parametros dentro del Query
      for i := 0 to RaveDriverDataView.QueryParams.Count - 1 do
      begin
        ParamName := Trim(RaveDriverDataView.QueryParams.Names[i]);
        ParamValue := Trim(RvProyecto.GetParam(ParamName));
        Query := StringReplace(Query,':' + ParamName,ParamValue,[rfReplaceAll]);
      end;
    end;
    Application.ProcessMessages;
    //Verificando que el Query posea registros
    Ds := BD.EjecutarQueryCliDs(Query);
    Result := (Ds.RecordCount > 0);
    FreeAndNil(Ds);
  end;
end;

procedure TDMRave.BuscarDisplayFormatDriverDataView(NumeroDecimales: Integer);
var
  ReporteDataView: TReporteDataView;
  RaveDriverDataView: TRaveDriverDataView;
  i: Integer;
begin
  ReporteDataView := TReporteDataView.Create;
  ReporteDataView.SetNombreReporte(ReporteActivo);
  if (ReporteDataView.BuscarNombreReporte) then
  begin
    //Ubicando DriverDataView del Reporte Activo
    RaveDriverDataView := TRaveDriverDataView(RvProyecto.ProjMan.FindRaveComponent(ReporteDataView.GetNombreDataview,nil));
    if (Assigned(RaveDriverDataView)) then
    begin
      for i := 0 to RaveDriverDataView.ComponentCount - 1 do
      begin
        if (IsPublishedProp(RaveDriverDataView.Components[i],'DisplayFormat')) then
        begin
          AsignarDisplayFormatRaveReports(RaveDriverDataView.Components[i],NumeroDecimales);
        end;
      end;
    end;
  end;
end;

procedure TDMRave.BuscarDisplayFormatRaveReportExtendido(Component: TComponent; NumeroDecimales: Integer);
var
  i, j: Integer;
  Str: string;
begin
  for i := 0 to Component.ComponentCount - 1 do
  begin
    Str := Component.Components[i].ClassName + ' - ' + Component.Components[i].Name + ' - ' + IntToStr(i);
    //Corrigiendo DisplayFormat del Nodo Parent
    if (IsPublishedProp(Component.Components[i],'DisplayFormat')) then
    begin
      AsignarDisplayFormatRaveReports(Component.Components[i],NumeroDecimales);
    end;
    if ((Component as TRaveComponent).ChildCount > 0) then
    begin
      //Revisando Componentes Hijos del Componente
      for j := 0 to (Component as TRaveComponent).ChildCount - 1 do
      begin
        BuscarDisplayFormatRaveReportExtendido((Component as TRaveComponent).Child[j],NumeroDecimales);
        if (IsPublishedProp(Component.Components[i],'DisplayFormat')) then
        begin
          AsignarDisplayFormatRaveReports((Component as TRaveComponent).Child[j],NumeroDecimales);
        end;
      end;
    end;
  end;
end;

procedure TDMRave.BuscarDisplayFormatRaveReport(NumeroDecimales: Integer);
var
  RaveReport: TRaveReport;
  i, j: Integer;
begin
  RaveReport := RvProyecto.ProjMan.FindReport(ReporteActivo,False);
  if (Assigned(RaveReport)) then
  begin
    //Recorriendo Paginas del Reporte
    for i := 0 to RaveReport.ComponentCount - 1 do
    begin
      //Corrigiendo DisplayFormat del Nodo Parent
      if (IsPublishedProp(RaveReport.Components[i],'DisplayFormat')) then
      begin
        AsignarDisplayFormatRaveReports(RaveReport.Components[i],NumeroDecimales);
      end;
      if (RaveReport.ChildCount > 0) then
      begin
        for j := 0 to RaveReport.ChildCount - 1 do
        begin
          BuscarDisplayFormatRaveReportExtendido(RaveReport.Child[j],NumeroDecimales);
        end;
      end;
    end;
  end;
end;

procedure TDMRave.BuscarDisplayFormat(NumeroDecimales: Integer);
begin
  //Realizando Cambios en los Componentes internos al DriverDataView
  BuscarDisplayFormatDriverDataView(NumeroDecimales);
  //Realizando Cambios en los Componentes internos al RaveReport
  BuscarDisplayFormatRaveReport(NumeroDecimales);
end;

{$ENDIF}

end.

