unit DMIndySMTP;

interface

uses
  SysUtils, Classes, IdMessage, IdIOHandler, IdIOHandlerSocket,
  IdIOHandlerStack, IdSSL, IdSSLOpenSSL, IdBaseComponent, IdComponent,
  IdTCPConnection, IdTCPClient, IdExplicitTLSClientServerBase, IdMessageClient,
  IdSMTPBase, IdSMTP;

type
  TIndySMTPEvent = procedure(Sender: TObject) of object;

  TDMIndySMTPSender = class(TDataModule)
    ISMTP: TIdSMTP;
    ISSLIOHandlerSocketOpenSSL: TIdSSLIOHandlerSocketOpenSSL;
    IMessage: TIdMessage;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    //SMTP Server
    Host: string;
    Port: Integer;
    UserName: string;
    Password: string;
    //Data Message
    From: string;
    FromName: string;
    Subject: string;
    ToList: TStringList;
    CCList: TStringList;
    CCOList: TStringList;
    AttachList: TStringList;
    Body: string;
    BodyHTML: Boolean;
    Error: Boolean;
    LastError: string;
    TraceError: string;
    Timeout: Integer;
  public
    { Public declarations }
    AfterSend: TIndySMTPEvent;
    BeforeSend: TIndySMTPEvent;
    procedure SetHost(varHost: string);
    procedure SetPort(varPort: Integer);
    procedure SetUserName(varUserName: string);
    procedure SetPassword(varPassword: string);
    procedure SetFrom(varFrom: string);
    procedure SetFromName(varFromName: string);
    procedure SetSubject(varSubject: string);
    procedure SetToList(varToList: TStringList);
    procedure SetCCList(varCCList: TStringList);
    procedure SetCCOList(varCCOList: TStringList);
    procedure SetAttachList(varAttachList: TStringList);
    procedure SetBody(varBody: string);
    procedure SetBodyHTML(varBodyHTML: Boolean);
    procedure SetTimeout(varTimeout: Integer);
    function GetHost: string;
    function GetPort: Integer;
    function GetUserName: string;
    function GetPassword: string;
    function GetFrom: string;
    function GetFromName: string;
    function GetSubject: string;
    function GetToList: TStringList;
    function GetCCList: TStringList;
    function GetCCOList: TStringList;
    function GetAttachList: TStringList;
    function GetBody: string;
    function GetBodyHTML: Boolean;
    function GetTimeout: Integer;
    function GetError: Boolean;
    function GetLastError: string;
    function GetTraceError: string;
    procedure ConfigSMTPServer(varHost, varUserName, varPassword: string;
      varPort: Integer);
    procedure InitMessage;
    procedure AddTo(varTo: string);
    procedure AddCC(varCC: string);
    procedure AddCCO(varCCO: string);
    procedure AddAttachment(varFileName: string);
    function SendEmail: Boolean;
  end;

var
  DMIndySMTPSender: TDMIndySMTPSender;

implementation

{$R *.dfm}

uses
  IdSSLOpenSSLHeaders,
  IdMessageBuilder;

{ TDMIndySMTP }

procedure TDMIndySMTPSender.SetHost(varHost: string);
begin
  Host := varHost;
end;

procedure TDMIndySMTPSender.SetPort(varPort: Integer);
begin
  Port := varPort;
end;

procedure TDMIndySMTPSender.SetUserName(varUserName: string);
begin
  UserName := varUserName;
end;

procedure TDMIndySMTPSender.SetPassword(varPassword: string);
begin
  Password := varPassword;
end;

procedure TDMIndySMTPSender.SetFrom(varFrom: string);
begin
  From := varFrom;
end;

procedure TDMIndySMTPSender.SetFromName(varFromName: string);
begin
  FromName := varFromName;
end;

procedure TDMIndySMTPSender.SetSubject(varSubject: string);
begin
  Subject := varSubject;
end;

procedure TDMIndySMTPSender.SetToList(varToList: TStringList);
begin
  ToList.Clear;
  ToList.Assign(varToList);
end;

procedure TDMIndySMTPSender.SetCCList(varCCList: TStringList);
begin
  CCList.Clear;
  CCList.Assign(varCCList);
end;

procedure TDMIndySMTPSender.SetCCOList(varCCOList: TStringList);
begin
  CCOList.Clear;
  CCOList.Assign(varCCOList);
end;

procedure TDMIndySMTPSender.SetAttachList(varAttachList: TStringList);
begin
  AttachList := varAttachList;
end;

procedure TDMIndySMTPSender.SetBody(varBody: string);
begin
  Body := varBody;
end;

procedure TDMIndySMTPSender.SetBodyHTML(varBodyHTML: Boolean);
begin
  BodyHTML := varBodyHTML;
end;

procedure TDMIndySMTPSender.SetTimeout(varTimeout: Integer);
begin
  Timeout := varTimeout * 1000;
end;

function TDMIndySMTPSender.GetHost: string;
begin
  Result := Host;
end;

function TDMIndySMTPSender.GetPort: Integer;
begin
  Result := Port;
end;

function TDMIndySMTPSender.GetUserName: string;
begin
  Result := UserName;
end;

function TDMIndySMTPSender.GetPassword: string;
begin
  Result := Password;
end;

function TDMIndySMTPSender.GetFrom: string;
begin
  Result := From;
end;

function TDMIndySMTPSender.GetFromName: string;
begin
  Result := FromName;
end;

function TDMIndySMTPSender.GetSubject: string;
begin
  Result := Subject;
end;

function TDMIndySMTPSender.GetToList: TStringList;
begin
  Result := ToList;
end;

function TDMIndySMTPSender.GetCCList: TStringList;
begin
  Result := CCList;
end;

function TDMIndySMTPSender.GetCCOList: TStringList;
begin
  Result := CCOList;
end;

function TDMIndySMTPSender.GetAttachList;
begin
  Result := AttachList;
end;

function TDMIndySMTPSender.GetBody: string;
begin
  Result := Body;
end;

function TDMIndySMTPSender.GetBodyHTML: Boolean;
begin
  Result := BodyHTML;
end;

function TDMIndySMTPSender.GetTimeout: Integer;
begin
  Result := Timeout;
end;

function TDMIndySMTPSender.GetError: Boolean;
begin
  Result := Error;
end;

function TDMIndySMTPSender.GetLastError: string;
begin
  Result := LastError;
end;

function TDMIndySMTPSender.GetTraceError: string;
begin
  Result := TraceError;
end;

procedure TDMIndySMTPSender.ConfigSMTPServer(varHost, varUserName, varPassword: string;
  varPort: Integer);
begin
  Host := varHost;
  UserName := varUserName;
  Password := varPassword;
  Port := varPort;
end;

procedure TDMIndySMTPSender.InitMessage;
begin
  From := '';
  FromName := '';
  Subject := '';
  ToList.Clear;
  CCList.Clear;
  CCOList.Clear;
  AttachList.Clear;
  LastError := '';
end;

procedure TDMIndySMTPSender.AddTo(varTo: string);
begin
  ToList.Add(varTo);
end;

procedure TDMIndySMTPSender.DataModuleCreate(Sender: TObject);
begin
  ToList := TStringList.Create;
  CCList := TStringList.Create;
  CCOList := TStringList.Create;
  AttachList := TStringList.Create;
  Timeout := 0;
  AfterSend := nil;
  BeforeSend := nil;
end;

procedure TDMIndySMTPSender.DataModuleDestroy(Sender: TObject);
begin
  ToList.Free;
  CCList.Free;
  CCOList.Free;
  AttachList.Free;
end;

procedure TDMIndySMTPSender.AddCC(varCC: string);
begin
  CCList.Add(varCC);
end;

procedure TDMIndySMTPSender.AddCCO(varCCO: string);
begin
  CCOList.Add(varCCO);
end;

procedure TDMIndySMTPSender.AddAttachment(varFileName: string);
begin
  if (FileExists(varFileName)) then
  begin
    AttachList.Add(varFileName);
  end;
end;

function TDMIndySMTPSender.SendEmail: Boolean;
var
  i: Integer;

  procedure AddAttachments(AttachBuilder: TIdMessageBuilderAttachments);
  var
    i: Integer;
  begin
    with AttachBuilder do
    begin
      if (AttachList.Count > 0) then
      begin
        for i := 0 to AttachList.Count - 1 do
        begin            
          Add(AttachList[i]);
        end;
      end;      
    end;
  end;
begin
  Result := True;
  Error := False;
  try
    with ISSLIOHandlerSocketOpenSSL do
    begin
      SSLOptions.Method := sslvTLSv1_2;
      SSLOptions.Mode := sslmClient;
      SSLOptions.VerifyMode := [];
      SSLOptions.VerifyDepth := 0;
    end;

    with ISMTP do
    begin
      IOHandler := ISSLIOHandlerSocketOpenSSL;
      Host := Self.Host;
      Port := Self.Port;
      Username := Self.UserName;
      Password := Self.Password;
      UseTLS := utUseExplicitTLS;
    end;

    if (Self.BodyHTML) then
    begin
      with TIdMessageBuilderHtml.Create do
      try
        Html.Text := Self.Body;
        //Adding Attachments
        AddAttachments(Attachments);
        FillMessage(IMessage);
      finally
        Free;
      end;
    end
    else
    begin
      with TIdMessageBuilderPlain.Create do
      try
        PlainText.Text := Body;
        //Adding Attachments
        AddAttachments(Attachments);
        FillMessage(IMessage);
      finally
        Free;      
      end;
    end;

    with IMessage do
    begin
      From.Name := Self.FromName;
      From.Address := Self.From;
      Subject := Self.Subject;
      //Adding ToList
      Recipients.Clear;
      for i := 0 to Self.ToList.Count - 1 do
      begin
        with Recipients.Add do
        begin
          Text := Self.ToList[i];
        end;
      end;
      //Adding CCList
      CCList.Clear;
      for i := 0 to Self.CCList.Count - 1 do
      begin
        with CCList.Add do
        begin
          Text := Self.CCList[i];
        end;
      end;
      //Adding CCOList
      BccList.Clear;
      for i := 0 to Self.CCOList.Count - 1 do
      begin
        with BccList.Add do
        begin
          Text := Self.CCOList[i];
        end;
      end;
    end;

    if (Assigned(BeforeSend)) then
    begin
      BeforeSend(Self);
    end;
    ISMTP.ConnectTimeout := Timeout;
    ISMTP.Connect;
    try
      ISMTP.Send(IMessage);
    finally
      if (ISMTP.Connected) then
      begin
        ISMTP.Disconnect;
      end;
    end;
  except
    on E: Exception do
      begin
        LastError := E.Message;
        TraceError := '';
        Error := True;
        Result := False;
      end;
  end;
  if (Assigned(AfterSend)) then
  begin
    AfterSend(Self)
  end;
end;


end.
