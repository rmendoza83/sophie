unit DMFR;

interface

uses
  SysUtils, Classes, frxBDEComponents, frxADOComponents,
  frxIBXComponents, frxDBXComponents,
  {$IF CompilerVersion >= 30.0} frxFDComponents,{$ENDIF} frxClass,
  frxZEOSComponents, frxExportText, frxExportCSV, frxExportRTF, frxExportHTML,
  frxExportPDF, frxExportHTMLDiv, frxExportXLSX, frxExportDOCX, frxExportODF,
  frxExportMail, frxExportXML, frxExportXLS, frxDesgn, frxCrypt, frxGZip,
  frxDCtrl, frxDMPExport, frxGradient, frxChBox, frxCross, frxRich, frxOLE,
  frxBarcode, frxChart, Dialogs, Forms {$IF CompilerVersion <= 18.0},
  ConverterRR2FR {$ENDIF};

type
  TDMFastReport = class(TDataModule)
    frxSophieReport: TfrxReport;
    frxSophieFRDesigner: TfrxDesigner;
    frxXLSExport1: TfrxXLSExport;
    frxXMLExport1: TfrxXMLExport;
    frxMailExport1: TfrxMailExport;
    frxODSExport1: TfrxODSExport;
    frxODTExport1: TfrxODTExport;
    frxDOCXExport1: TfrxDOCXExport;
    frxXLSXExport1: TfrxXLSXExport;
    frxHTML4DivExport1: TfrxHTML4DivExport;
    frxHTML5DivExport1: TfrxHTML5DivExport;
    frxPDFExport1: TfrxPDFExport;
    frxHTMLExport1: TfrxHTMLExport;
    frxRTFExport1: TfrxRTFExport;
    frxCSVExport1: TfrxCSVExport;
    frxSimpleTextExport1: TfrxSimpleTextExport;
    frxSophieZEOSComponents: TfrxZEOSComponents;
    frxSophieDBXComponents: TfrxDBXComponents;
    frxSophieIBXComponents: TfrxIBXComponents;
    frxSophieADOComponents: TfrxADOComponents;
    frxSophieBDEComponents: TfrxBDEComponents;
    frxChartObject1: TfrxChartObject;
    frxBarCodeObject1: TfrxBarCodeObject;
    frxOLEObject1: TfrxOLEObject;
    frxRichObject1: TfrxRichObject;
    frxCrossObject1: TfrxCrossObject;
    frxCheckBoxObject1: TfrxCheckBoxObject;
    frxGradientObject1: TfrxGradientObject;
    frxDotMatrixExport1: TfrxDotMatrixExport;
    frxDialogControls1: TfrxDialogControls;
    frxGZipCompressor1: TfrxGZipCompressor;
    frxCrypt1: TfrxCrypt;
    ODlg: TOpenDialog;
    SDlg: TSaveDialog;
    procedure DataModuleCreate(Sender: TObject);
    function frxSophieFRDesignerLoadReport(Report: TfrxReport): Boolean;
    function frxSophieFRDesignerSaveReport(Report: TfrxReport;
      SaveAs: Boolean): Boolean;
    procedure frxSophieFRDesignerShow(Sender: TObject);
  private
    { Private declarations }
    SenderForm: TForm;
  public
    { Public declarations }
  end;

const
  SOPHIE_FR_TITULO = 'Sophie Fast Report';

var
  DMFastReport: TDMFastReport;

implementation

{$R *.dfm}

procedure TDMFastReport.DataModuleCreate(Sender: TObject);
begin
  frxSophieFRDesigner.CloseQuery := False;
  frxSophieFRDesigner.Standalone := True;
  frxSophieReport.DesignReport;
end;

function TDMFastReport.frxSophieFRDesignerLoadReport(
  Report: TfrxReport): Boolean;
begin
  Result := False;
  if (ODlg.Execute) then
  begin
    Report.LoadFromFile(ODlg.FileName);
    SenderForm.Caption := SOPHIE_FR_TITULO + ' - ' + Report.FileName;
    Result := True;
  end;
end;

function TDMFastReport.frxSophieFRDesignerSaveReport(Report: TfrxReport;
  SaveAs: Boolean): Boolean;
var
  FRExist: Boolean;
begin
  Result := False;
  FRExist := FileExists(Report.FileName);
  if ((SaveAs) or (not FRExist)) then
  begin
    SDlg.FileName := Report.FileName;
    if (SDlg.Execute) then
    begin
      Report.FileName := SDlg.FileName;
      Report.SaveToFile(SDlg.FileName);
      Result := True;
    end;
  end
  else
  begin
    Report.SaveToFile(Report.FileName);
    Result := True;
  end;
  if (Result) then
  begin
    SenderForm.Caption := SOPHIE_FR_TITULO + ' - ' + Report.FileName;
  end;
end;

procedure TDMFastReport.frxSophieFRDesignerShow(Sender: TObject);
begin
  if (Sender is TForm) then
  begin
    SenderForm  := (Sender as TForm);
    SenderForm.Caption := SOPHIE_FR_TITULO + ' - SophieReport1.sfr';
    frxSophieReport.FileName := 'SophieReport1.sfr';
  end;
end;

end.
