object DMFastReport: TDMFastReport
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 461
  Width = 684
  object frxSophieReport: TfrxReport
    Version = '5.4.6'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 42960.420734479160000000
    ReportOptions.LastChange = 42960.420734479160000000
    ScriptLanguage = 'PascalScript'
    StoreInDFM = False
    Left = 40
    Top = 56
  end
  object frxSophieFRDesigner: TfrxDesigner
    DefaultScriptLanguage = 'PascalScript'
    DefaultFont.Charset = DEFAULT_CHARSET
    DefaultFont.Color = clWindowText
    DefaultFont.Height = -13
    DefaultFont.Name = 'Arial'
    DefaultFont.Style = []
    DefaultLeftMargin = 10.000000000000000000
    DefaultRightMargin = 10.000000000000000000
    DefaultTopMargin = 10.000000000000000000
    DefaultBottomMargin = 10.000000000000000000
    DefaultPaperSize = 9
    DefaultOrientation = poPortrait
    GradientEnd = 11982554
    GradientStart = clWindow
    TemplatesExt = 'fr3'
    Restrictions = []
    RTLLanguage = False
    MemoParentFont = False
    OnLoadReport = frxSophieFRDesignerLoadReport
    OnSaveReport = frxSophieFRDesignerSaveReport
    OnShow = frxSophieFRDesignerShow
    Left = 40
    Top = 8
  end
  object frxXLSExport1: TfrxXLSExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    ExportEMF = True
    AsText = False
    Background = True
    FastExport = True
    PageBreaks = True
    EmptyLines = True
    SuppressPageHeadersFooters = False
    Left = 160
    Top = 8
  end
  object frxXMLExport1: TfrxXMLExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    Background = True
    Creator = 'FastReport'
    EmptyLines = True
    SuppressPageHeadersFooters = False
    RowsCount = 0
    Split = ssNotSplit
    Left = 160
    Top = 64
  end
  object frxMailExport1: TfrxMailExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    ShowExportDialog = True
    SmtpPort = 25
    UseIniFile = True
    TimeOut = 60
    ConfurmReading = False
    UseMAPI = SMTP
    Left = 224
    Top = 8
  end
  object frxODSExport1: TfrxODSExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    CreationTime = 0.000000000000000000
    DataOnly = False
    PictureType = gpPNG
    Background = True
    Creator = 'FastReport'
    SingleSheet = False
    Language = 'en'
    SuppressPageHeadersFooters = False
    Left = 224
    Top = 64
  end
  object frxODTExport1: TfrxODTExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    CreationTime = 0.000000000000000000
    DataOnly = False
    PictureType = gpPNG
    Background = True
    Creator = 'FastReport'
    SingleSheet = False
    Language = 'en'
    SuppressPageHeadersFooters = False
    Left = 296
    Top = 8
  end
  object frxDOCXExport1: TfrxDOCXExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    OpenAfterExport = False
    PictureType = gpPNG
    Left = 296
    Top = 64
  end
  object frxXLSXExport1: TfrxXLSXExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    ChunkSize = 0
    PictureType = gpPNG
    Left = 368
    Top = 8
  end
  object frxHTML4DivExport1: TfrxHTML4DivExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    OpenAfterExport = False
    HTML5 = False
    MultiPage = False
    Formatted = False
    PictureFormat = pfPNG
    AllPictures = False
    UnifiedPictures = True
    EmbeddedPictures = False
    Navigation = False
    ExportAnchors = True
    EmbeddedCSS = False
    PictureTag = 0
    Left = 368
    Top = 64
  end
  object frxHTML5DivExport1: TfrxHTML5DivExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    OpenAfterExport = False
    HTML5 = True
    MultiPage = False
    Formatted = False
    PictureFormat = pfPNG
    AllPictures = False
    UnifiedPictures = True
    EmbeddedPictures = True
    Navigation = False
    ExportAnchors = True
    EmbeddedCSS = True
    PictureTag = 0
    Left = 440
    Top = 8
  end
  object frxPDFExport1: TfrxPDFExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    PrintOptimized = False
    Outline = False
    Background = False
    HTMLTags = True
    Quality = 95
    Transparency = False
    Author = 'FastReport'
    Subject = 'FastReport PDF export'
    ProtectionFlags = [ePrint, eModify, eCopy, eAnnot]
    HideToolbar = False
    HideMenubar = False
    HideWindowUI = False
    FitWindow = False
    CenterWindow = False
    PrintScaling = False
    PdfA = False
    Left = 440
    Top = 64
  end
  object frxHTMLExport1: TfrxHTMLExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    FixedWidth = True
    Background = False
    Centered = False
    EmptyLines = True
    Print = False
    PictureType = gpPNG
    Left = 520
    Top = 8
  end
  object frxRTFExport1: TfrxRTFExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    PictureType = gpPNG
    Wysiwyg = True
    Creator = 'FastReport'
    SuppressPageHeadersFooters = False
    HeaderFooterMode = hfText
    AutoSize = False
    Left = 520
    Top = 64
  end
  object frxCSVExport1: TfrxCSVExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    Separator = ';'
    OEMCodepage = False
    UTF8 = False
    NoSysSymbols = True
    ForcedQuotes = False
    Left = 600
    Top = 8
  end
  object frxSimpleTextExport1: TfrxSimpleTextExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    Frames = False
    EmptyLines = False
    OEMCodepage = False
    DeleteEmptyColumns = True
    Left = 600
    Top = 64
  end
  object frxSophieZEOSComponents: TfrxZEOSComponents
    Left = 40
    Top = 384
  end
  object frxSophieDBXComponents: TfrxDBXComponents
    Left = 40
    Top = 280
  end
  object frxSophieIBXComponents: TfrxIBXComponents
    Left = 40
    Top = 224
  end
  object frxSophieADOComponents: TfrxADOComponents
    Left = 40
    Top = 168
  end
  object frxSophieBDEComponents: TfrxBDEComponents
    DefaultSession = 'Default'
    Left = 40
    Top = 112
  end
  object frxChartObject1: TfrxChartObject
    Left = 440
    Top = 224
  end
  object frxBarCodeObject1: TfrxBarCodeObject
    Left = 152
    Top = 280
  end
  object frxOLEObject1: TfrxOLEObject
    Left = 296
    Top = 224
  end
  object frxRichObject1: TfrxRichObject
    Left = 152
    Top = 224
  end
  object frxCrossObject1: TfrxCrossObject
    Left = 224
    Top = 224
  end
  object frxCheckBoxObject1: TfrxCheckBoxObject
    Left = 368
    Top = 224
  end
  object frxGradientObject1: TfrxGradientObject
    Left = 368
    Top = 168
  end
  object frxDotMatrixExport1: TfrxDotMatrixExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    EscModel = 0
    GraphicFrames = False
    SaveToFile = False
    UseIniSettings = True
    Left = 440
    Top = 168
  end
  object frxDialogControls1: TfrxDialogControls
    Left = 296
    Top = 168
  end
  object frxGZipCompressor1: TfrxGZipCompressor
    Left = 152
    Top = 168
  end
  object frxCrypt1: TfrxCrypt
    Left = 224
    Top = 168
  end
  object ODlg: TOpenDialog
    Filter = 
      'Archivo de Sophie Fast Report (*.sfr)|*.sfr|Archivo de Fast Repo' +
      'rt 3 (*.fr3)|*.fr3|Todos los Archivos (*.*)|*.*'
    Left = 152
    Top = 352
  end
  object SDlg: TSaveDialog
    DefaultExt = '*.sfr'
    Filter = 
      'Archivo de Sophie Fast Report (*.sfr)|*.sfr|Archivo de Fast Repo' +
      'rt 3 (*.fr3)|*.fr3|Todos los Archivos (*.*)|*.*'
    Title = 'Save Report...'
    Left = 224
    Top = 352
  end
end
