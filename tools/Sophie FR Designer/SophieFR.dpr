program SophieFR;

uses
  Forms,
  DMFR in 'datamodule\DMFR.pas' {DMFastReport: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  {$IF CompilerVersion >= 30.0}
  Application.MainFormOnTaskbar := True;
  {$ENDIF}
  Application.CreateForm(TDMFastReport, DMFastReport);
  Application.Run;
end.
