CREATE VIEW "v_existencias_resumido" AS 
SELECT 
	a.codigo AS codigo_producto,
	a.referencia,
	a.descripcion,
	b.codigo AS codigo_estacion,
	b.descripcion AS descripcion_estacion,
	COALESCE(SUM(c.cantidad),CAST(0 AS NUMERIC)) AS cantidad_entradas,
	COALESCE(SUM(d.cantidad),CAST(0 AS NUMERIC)) AS cantidad_salidas
FROM tbl_productos a
CROSS JOIN tbl_estaciones b
LEFT JOIN tbl_det_movimientos_inventario c ON a.codigo = c.codigo_producto AND b.codigo = c.estacion_entrada
LEFT JOIN tbl_det_movimientos_inventario d ON a.codigo = d.codigo_producto AND b.codigo = d.estacion_salida
GROUP BY a.codigo, a.referencia, a.descripcion, b.codigo, b.descripcion
