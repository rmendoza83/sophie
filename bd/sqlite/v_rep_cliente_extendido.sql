CREATE VIEW "v_rep_cliente_extendido" AS 
SELECT
	a.codigo,
	a.prefijo_rif,
	a.rif,
	a.nit,
	a.razon_social,
	a.sexo,
	a.fecha_nacimiento,
	a.direccion_fiscal_1,
	a.direccion_fiscal_2,
	a.direccion_domicilio_1,
	a.direccion_domicilio_2,
	a.telefonos,
	a.fax,
	a.email,
	a.website,
	a.fecha_ingreso,
	a.contacto,
	a.codigo_vendedor,
	a.codigo_esquema_pago,
	a.codigo_zona,
	a.unique_id,
	b.placa,
	b.marca,
	b.modelo,
	b.anno,
	b.color,
	b.kilometraje,
	b.observaciones,
	c.descripcion AS z_descripcion,
	d.descripcion AS ep_descripcion,
	e.descripcion AS v_descripcion
FROM tbl_clientes a
JOIN tbl_clientes_vehiculos b ON a.codigo = b.codigo_cliente
JOIN tbl_zonas c ON a.codigo_zona = c.codigo
JOIN tbl_esquemas_pago d ON a.codigo_esquema_pago = d.codigo
JOIN tbl_vendedores e ON a.codigo_vendedor = e.codigo
