CREATE VIEW "v_detalle_calculo_pago_comision" AS 
SELECT 
	CAST('FC' AS VARCHAR(10)) AS desde,
	a.numero,
	a.codigo_vendedor,
	a.fecha_ingreso,
	COALESCE(b.codigo_producto,CAST('N/A' AS VARCHAR(20))) AS codigo_producto,
	COALESCE(b.referencia,CAST('N/A' AS VARCHAR(20))) AS referencia,
	COALESCE(b.descripcion,CAST('N/A' AS VARCHAR(80))) AS descripcion,
	COALESCE(b.cantidad,CAST(1 AS NUMERIC)) AS cantidad,
	COALESCE(b.precio,a.monto_neto) AS precio,
	COALESCE(b.p_descuento,a.p_descuento) AS p_descuento,
	COALESCE(b.monto_descuento,a.monto_descuento) AS monto_descuento,
	COALESCE(b.impuesto,a.impuesto) AS impuesto,
	COALESCE(b.p_iva,a.p_iva) AS p_iva,
	COALESCE(b.total,a.total) AS total,
	a.id_facturacion_cliente,
	c.codigo_division,
	c.codigo_linea,
	c.codigo_familia,
	c.codigo_clase,
	c.codigo_manufactura
FROM tbl_facturacion_clientes a
LEFT JOIN tbl_det_facturacion_clientes b ON a.id_facturacion_cliente = b.id_facturacion_cliente
LEFT JOIN tbl_productos c ON b.codigo_producto = c.codigo
WHERE NOT (a.numero IN (
	SELECT tbl_det_pagos_comisiones.numero_documento
	FROM tbl_det_pagos_comisiones
	WHERE tbl_det_pagos_comisiones.desde = 'FC'
))

UNION

SELECT 
	CAST('NCC' AS VARCHAR(10)) AS desde,
	a.numero,
	a.codigo_vendedor,
	a.fecha_ingreso,
	COALESCE(b.codigo_producto,CAST('N/A' AS VARCHAR(20))) AS codigo_producto,
	COALESCE(b.referencia,CAST('N/A' AS VARCHAR(20))) AS referencia,
	COALESCE(b.descripcion,CAST('N/A' AS VARCHAR(80))) AS descripcion,
	COALESCE(b.cantidad,CAST('N/A' AS NUMERIC)) AS cantidad,
	COALESCE(b.precio,a.monto_neto) AS precio,
	COALESCE(b.p_descuento,a.p_descuento) AS p_descuento,
	COALESCE(b.monto_descuento,a.monto_descuento) AS monto_descuento,
	COALESCE(b.impuesto,a.impuesto) AS impuesto,
	COALESCE(b.p_iva,a.p_iva) AS p_iva,
	COALESCE(b.total,a.total) AS total,
	a.id_nota_credito_cliente AS id_facturacion_cliente,
	c.codigo_division,
	c.codigo_linea,
	c.codigo_familia,
	c.codigo_clase,
	c.codigo_manufactura
FROM tbl_nota_credito_clientes a
LEFT JOIN tbl_det_nota_credito_clientes b ON a.id_nota_credito_cliente = b.id_nota_credito_cliente
LEFT JOIN tbl_productos c ON b.codigo_producto = c.codigo
WHERE NOT (a.numero IN (
	SELECT tbl_det_pagos_comisiones.numero_documento
	FROM tbl_det_pagos_comisiones
	WHERE tbl_det_pagos_comisiones.desde = 'NCC'
))
