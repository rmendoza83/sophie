CREATE VIEW "v_rep_detalle_movimientos_inventario" AS 
SELECT 
	a.desde AS dmi_desde,
	a.numero_documento AS dmi_numero_documento,
	a.codigo_producto AS dmi_codigo_producto,
	b.referencia AS pro_referencia_producto,
	b.descripcion AS pro_descripcion_producto,
	b.costo_anterior AS pro_costo_anterior,
	b.us_costo_anterior AS pro_us_costo_anterior,
	b.codigo_division AS pro_codigo_division,
	c.descripcion AS pro_descripcion_division,
	b.codigo_linea AS pro_codigo_linea,
	d.descripcion AS pro_descripcion_linea,
	b.codigo_familia AS pro_codigo_familia,
	e.descripcion AS pro_descripcion_familia,
	b.codigo_clase AS pro_codigo_clase,
	f.descripcion AS pro_descripcion_clase,
	b.codigo_manufactura AS pro_codigo_manufactura,
	g.descripcion AS pro_descripcion_manufactura,
	b.codigo_arancel AS pro_codigo_arancel,
	h.descripcion AS pro_descripcion_arancel,
	b.codigo_unidad AS pro_codigo_unidad,
	i.descripcion AS pro_descripcion_unidad,
	a.cantidad AS dmi_cantidad,
	a.estacion_salida AS dmi_codigo_estacion_salida,
	j.descripcion AS est_descripcion_estacion_salida,
	a.estacion_entrada AS dmi_codigo_estacion_entrada,
	k.descripcion AS est_descripcion_estacion_entrada,
	a.codigo_zona AS dmi_codigo_zona,
	l.descripcion AS zon_descripcion_zona,
	a.precio AS dmi_precio,
	a.p_descuento AS dmi_p_descuento,
	a.monto_descuento AS dmi_monto_descuento,
	a.impuesto AS dmi_impuesto,
	a.p_iva AS dmi_p_iva,
	a.total AS dmi_total,
	a.costo AS dmi_costo,
	a.us_costo AS dmi_us_costo,
	a.costo_fob AS dmi_costo_fob,
	a.fecha AS dmi_fecha,
	a.codigo_cliente AS dmi_codigo_cliente,
	m.rif AS cli_rif,
	m.razon_social AS cli_razon_social,
	a.codigo_proveedor AS dmi_codigo_proveedor,
	n.rif AS prov_rif,
	n.razon_social AS prov_razon_social,
	a.id_movimiento_inventario,
	CASE
		WHEN a.desde = 'FC' OR a.desde = 'NCP' THEN CAST(-1 AS NUMERIC) * a.cantidad
		ELSE a.cantidad
	END AS cantidad_resultante
FROM tbl_det_movimientos_inventario a
LEFT JOIN tbl_productos b ON a.codigo_producto = b.codigo
LEFT JOIN tbl_divisiones c ON b.codigo_division = c.codigo
LEFT JOIN tbl_lineas d ON b.codigo_linea = d.codigo
LEFT JOIN tbl_familias e ON b.codigo_familia = e.codigo
LEFT JOIN tbl_clases f ON b.codigo_clase = f.codigo
LEFT JOIN tbl_manufacturas g ON b.codigo_manufactura = g.codigo
LEFT JOIN tbl_aranceles h ON b.codigo_arancel = h.codigo
LEFT JOIN tbl_unidades i ON b.codigo_unidad = i.codigo
LEFT JOIN tbl_estaciones j ON a.estacion_salida = j.codigo
LEFT JOIN tbl_estaciones k ON a.estacion_entrada = k.codigo
LEFT JOIN tbl_zonas l ON a.codigo_zona = l.codigo
LEFT JOIN tbl_clientes m ON a.codigo_cliente = m.codigo
LEFT JOIN tbl_proveedores n ON a.codigo_proveedor = n.codigo
