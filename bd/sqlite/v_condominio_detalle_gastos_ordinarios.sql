CREATE VIEW "v_condominio_detalle_gastos_ordinarios" AS 
SELECT 
	a.codigo_conjunto_administracion,
	a.codigo_gasto,
	b.descripcion,
	a.monto
FROM tbl_condominio_gastos_ordinarios a
LEFT JOIN tbl_condominio_gastos b ON a.codigo_conjunto_administracion = b.codigo_conjunto_administracion AND a.codigo_gasto = b.codigo
