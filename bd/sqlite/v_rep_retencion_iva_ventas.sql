CREATE VIEW "v_rep_retencion_iva_ventas" AS 
SELECT 
	CAST('01-Fac' AS VARCHAR(10)) AS tipo_documento,
	CAST('FC' AS VARCHAR(10)) AS desde,
	a.numero,
	a.fecha_ingreso,
	a.fecha_contable,
	a.fecha_libros,
	a.sub_total,
	a.impuesto,
	a.total,
	a.p_descuento,
	a.p_iva,
	b.monto_retencion_iva AS iva_retenido,
	a.impuesto - b.monto_retencion_iva AS iva_percibido,
	d.codigo,
	d.razon_social,
	d.prefijo_rif AS prefijo_rif_prov,
	d.rif AS ret_rif_pro,
	e.serie,
	e.numero_control
FROM tbl_facturacion_clientes a
LEFT JOIN tbl_det_liquidacion_cobranzas b ON a.id_cobranza = b.id_cobranza AND b.orden_cobranza = 1
LEFT JOIN tbl_tipos_retenciones c ON b.codigo_retencion_iva = c.codigo
LEFT JOIN tbl_clientes d ON a.codigo_cliente = d.codigo
LEFT JOIN tbl_documentos_legales e ON a.numero = e.numero_documento AND e.desde = 'FC'

UNION

SELECT
	CAST('02-Ndc' AS VARCHAR(10)) AS tipo_documento,
	CAST('NDC' AS VARCHAR(10)) AS desde,
	a.numero,
	a.fecha_ingreso,
	a.fecha_contable,
	a.fecha_libros,
	a.sub_total,
	a.impuesto,
	a.total,
	a.p_descuento,
	a.p_iva,
	b.monto_retencion_iva AS iva_retenido,
	a.impuesto - b.monto_retencion_iva AS iva_percibido,
	d.codigo,
	d.razon_social,
	d.prefijo_rif AS prefijo_rif_prov,
	d.rif AS ret_rif_pro,
	e.serie,
	e.numero_control
FROM tbl_nota_debito_clientes a
LEFT JOIN tbl_det_liquidacion_cobranzas b ON a.id_cobranza = b.id_cobranza AND b.orden_cobranza = 1
LEFT JOIN tbl_tipos_retenciones c ON b.codigo_retencion_iva = c.codigo
LEFT JOIN tbl_clientes d ON a.codigo_cliente = d.codigo
LEFT JOIN tbl_documentos_legales e ON a.numero = e.numero_documento AND e.desde = 'NDC'

UNION

SELECT
	CAST('03-Ncc' AS VARCHAR(10)) AS tipo_documento,
	CAST('NCC' AS VARCHAR(10)) AS desde,
	a.numero,
	a.fecha_ingreso,
	a.fecha_contable,
	a.fecha_libros,
	a.sub_total,
	a.impuesto,
	a.total,
	a.p_descuento,
	a.p_iva,
	b.monto_retencion_iva AS iva_retenido,
	a.impuesto - b.monto_retencion_iva AS iva_percibido,
	d.codigo,
	d.razon_social,
	d.prefijo_rif AS prefijo_rif_prov,
	d.rif AS ret_rif_pro,
	e.serie,
	e.numero_control
FROM tbl_nota_credito_clientes a
LEFT JOIN tbl_det_liquidacion_cobranzas b ON a.id_cobranza = b.id_cobranza AND b.orden_cobranza = 1
LEFT JOIN tbl_tipos_retenciones c ON b.codigo_retencion_iva = c.codigo
LEFT JOIN tbl_clientes d ON a.codigo_cliente = d.codigo
LEFT JOIN tbl_documentos_legales e ON a.numero = e.numero_documento AND e.desde = 'NCC'
