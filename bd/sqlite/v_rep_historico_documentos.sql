CREATE VIEW "v_rep_historico_documentos" AS 
SELECT 
	a.abonado AS abonado_det_liqcob,
	a.codigo_retencion_iva AS codigo_retencion_iva_det_liqcob,
	a.monto_retencion_iva AS monto_retencion_iva_det_liqcob,
	a.codigo_retencion_1 AS codigo_retencion_1_det_liqcob,
	a.codigo_retencion_2 AS codigo_retencion_2_det_liqcob,
	a.codigo_retencion_3 AS codigo_retencion_3_det_liqcob,
	a.codigo_retencion_4 AS codigo_retencion_4_det_liqcob,
	a.codigo_retencion_5 AS codigo_retencion_5_det_liqcob,
	a.monto_retencion_1 AS monto_retencion_1_det_liqcob,
	a.monto_retencion_2 AS monto_retencion_2_det_liqcob,
	a.monto_retencion_3 AS monto_retencion_3_det_liqcob,
	a.monto_retencion_4 AS monto_retencion_4_det_liqcob,
	a.monto_retencion_5 AS monto_retencion_5_det_liqcob,
	b.desde AS desde_cob_cobrada,
	b.numero_documento AS numero_documento_cob_cobrada,
	b.codigo_cliente AS codigo_cliente_cob_cobrada,
	b.fecha_ingreso AS fecha_ingreso_cob_cobrada,
	b.fecha_recepcion AS fecha_recepcion_cob_cobrada,
	b.fecha_libros AS fecha_libro_cob_cobrada,
	b.fecha_contable AS fecha_contable_cob_cobranza,
	b.fecha_cobranza AS fecha_cobranza_cob_cobranza,
	b.monto_neto AS monto_neto_cob_cobrada,
	b.impuesto AS impuesto_cob_cobrada,
	b.total AS total_cob_cobrada,
	b.p_iva AS p_iva_cob_cobrada,
	c.fecha_ingreso AS fecha_pago_liq_cobranza,
	d.razon_social
FROM tbl_det_liquidacion_cobranzas a
JOIN tbl_cobranzas_cobradas b ON a.orden_cobranza = b.orden_cobranza AND a.id_cobranza = b.id_cobranza AND a.numero_cuota = b.numero_cuota
JOIN tbl_liquidacion_cobranzas c ON a.id_liquidacion_cobranza = c.id_liquidacion_cobranza
JOIN tbl_clientes d ON b.codigo_cliente = d.codigo
