CREATE VIEW "v_busqueda_pago_comision" AS 
SELECT 
	a.numero,
	b.codigo AS codigo_vendedor,
	b.descripcion AS nombre_vendedor,
	b.activo AS estatus
FROM tbl_pagos_comisiones a
LEFT JOIN tbl_vendedores b ON a.codigo_vendedor = b.codigo
ORDER BY a.numero
