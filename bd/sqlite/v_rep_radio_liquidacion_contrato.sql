CREATE VIEW "v_rep_radio_liquidacion_contrato" AS 
SELECT 
	a.numero AS numero_liquidacion_contrato,
	a.codigo_cliente,
	a.fecha_ingreso AS fecha_liquidacion,
	a.total AS total_liquidacion,
	a.codigo_zona,
	a.observaciones AS observacion_liquidacion,
	b.item AS item_det_liquidacion_contrato,
	c.numero AS numero_contrato,
	c.codigo_anunciante,
	d.descripcion AS nombre_anunciante,
	e.razon_social,
	e.direccion_fiscal_1,
	e.direccion_fiscal_2,
	e.telefonos,
	c.fecha_inicial AS fecha_inicia_contrato,
	c.fecha_final AS fecha_final_contrato,
	c.numero_orden AS numero_orden_contrato,
	c.total AS total_contrato,
	f.fecha_inicial AS fecha_inicial_distribucion_factura,
	f.fecha_final AS fecha_final_distribucion_factura,
	f.total AS total_distribucion_factura,
	f.total_items AS total_items_distribucion_factura,
	f.numero_documento
FROM tbl_radio_liquidacion_contratos a
JOIN tbl_radio_det_liquidacion_contratos b ON a.id_radio_liquidacion_contrato = b.id_radio_liquidacion_contrato
JOIN tbl_radio_contratos c ON b.id_radio_contrato = c.id_radio_contrato
JOIN tbl_radio_anunciantes d ON c.codigo_anunciante = d.codigo
JOIN tbl_clientes e ON a.codigo_cliente = e.codigo
JOIN v_radio_contratos_distribucion_facturas f ON b.id_radio_contrato = f.id_radio_contrato AND b.item = f.item
