CREATE VIEW "v_rep_resumen_debito_creditos" AS 
SELECT 
	CAST('FC' AS VARCHAR(10))AS desde,
	a.numero,
	a.fecha_ingreso,
	a.fecha_contable,
	a.fecha_libros,
	a.sub_total,
	a.impuesto,
	a.total,
	a.p_descuento,
	a.p_iva,
	b.monto_retencion_iva AS iva_retenido,
	a.impuesto - b.monto_retencion_iva AS iva_percibido
FROM tbl_facturacion_clientes a
LEFT JOIN tbl_det_liquidacion_cobranzas b ON a.id_cobranza = b.id_cobranza AND b.orden_cobranza = 1
LEFT JOIN tbl_tipos_retenciones c ON b.codigo_retencion_iva = c.codigo

UNION

SELECT 
	CAST('FP' AS VARCHAR(10)) AS desde,
	a.numero,
	a.fecha_ingreso,
	a.fecha_contable,
	a.fecha_libros,
	a.sub_total,
	a.impuesto,
	a.total,
	a.p_descuento,
	a.p_iva,
	b.monto_retencion AS iva_retenido,
	a.impuesto - b.monto_retencion AS iva_percibido
FROM tbl_facturacion_proveedores a
LEFT JOIN tbl_det_retenciones_iva b ON a.numero = b.numero_documento AND b.desde = 'FP'
LEFT JOIN tbl_tipos_retenciones c ON b.codigo_retencion = c.codigo
