CREATE VIEW "v_rep_radio_conatel" AS 
SELECT 
	CAST('FC' AS VARCHAR(10)) AS desde,
	a.numero,
	a.fecha_ingreso,
	a.codigo_cliente,
	a.sub_total,
	a.impuesto,
	a.total,
	CAST('' AS VARCHAR(20)) AS numero_control,
	b.razon_social
FROM tbl_facturacion_clientes a
JOIN tbl_clientes b ON a.codigo_cliente = b.codigo

UNION

SELECT 
	CAST('NCC' AS VARCHAR(10)) AS desde,
	a.numero,
	a.fecha_ingreso,
	a.codigo_cliente,
	a.sub_total * CAST(-1 AS NUMERIC) AS sub_total,
	a.impuesto,
	a.total * CAST(-1 AS NUMERIC) AS total,
	CAST('' AS VARCHAR(20)) AS numero_control,
	b.razon_social
FROM tbl_nota_credito_clientes a
JOIN tbl_clientes b ON a.codigo_cliente = b.codigo
