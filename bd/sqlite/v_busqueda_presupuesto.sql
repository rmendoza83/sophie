CREATE VIEW "v_busqueda_presupuesto" AS 
SELECT 
	a.numero,
	a.codigo_cliente,
	CAST((b.prefijo_rif || b.rif) AS VARCHAR(20)) AS rif,
	b.nit,
	b.razon_social,
	b.contacto,
	c.codigo AS codigo_vendedor,
	c.descripcion AS nombre_vendedor
FROM tbl_presupuestos a
LEFT JOIN tbl_clientes b ON a.codigo_cliente = b.codigo
LEFT JOIN tbl_vendedores c ON a.codigo_vendedor = c.codigo
ORDER BY a.id_presupuesto
