CREATE VIEW "v_rep_anticipo_cliente" AS 
SELECT 
	a.numero AS a_numero,
	a.fecha_ingreso AS a_fecha_ingreso,
	a.monto_neto AS a_monto_neto,
	a.concepto AS a_concepto,
	a.observaciones AS a_observaciones,
	a.numero_forma_pago,
	a.numero_documento_bancario,
	b.prefijo_rif,
	b.rif,
	b.telefonos,
	b.razon_social,
	b.direccion_fiscal_1,
	b.direccion_fiscal_2,
	c.codigo AS fpago_codigo,
	c.descripcion AS fpago_descripcion,
	d.codigo AS cbanco_codigo,
	d.entidad AS cbanco_despcricion,
	e.codigo AS bcobranza_codigo,
	e.descripcion AS bcobranza_descripcion
FROM tbl_anticipo_clientes a
JOIN tbl_clientes b ON a.codigo_cliente = b.codigo
JOIN tbl_formas_pago c ON a.codigo_forma_pago = c.codigo
JOIN tbl_cuentas_bancarias d ON a.codigo_cuenta_bancaria = d.codigo
LEFT JOIN tbl_bancos_cobranzas e ON a.codigo_banco_cobranza = e.codigo
