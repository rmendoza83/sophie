CREATE VIEW "v_rep_radio_venta" AS 
SELECT
	a.fecha_inicial,
	a.fecha_final,
	a.fecha_ingreso AS fecha_factura,
	a.total AS total_factura,
	a.numero_documento AS numero_factura,
	a.item,
	a.total_items,
	b.numero AS numero_contrato,
	b.codigo_cliente,
	b.id_radio_contrato,
	b.codigo_vendedor,
	h.descripcion AS nombre_vendedor,
	c.razon_social,
	b.numero_orden,
	b.codigo_anunciante,
	d.descripcion AS descripcion_anunciante,
	e.p_descuento AS porcentaje_descuento,
	e.codigo_programa,
	f.descripcion AS descripcion_programa,
	e.codigo_espacio,
	g.descripcion AS tipo_compra,
	i.codigo AS codigo_emisora,
	i.razon_social AS nombre_emisora
FROM tbl_radio_contratos_facturas_generadas a
JOIN tbl_radio_contratos b ON a.id_radio_contrato = b.id_radio_contrato
JOIN tbl_clientes c ON b.codigo_cliente = c.codigo
JOIN tbl_radio_anunciantes d ON b.codigo_anunciante = d.codigo
JOIN tbl_radio_contratos_programas e ON b.id_radio_contrato = e.id_radio_contrato
JOIN tbl_radio_programas f ON e.codigo_programa = f.codigo
JOIN tbl_radio_espacios g ON e.codigo_espacio = g.codigo
JOIN tbl_vendedores h ON b.codigo_vendedor = h.codigo
JOIN tbl_radio_emisoras i ON f.codigo_emisora = i.codigo

UNION

SELECT
	a.fecha_inicial,
	a.fecha_final,
	a.fecha_ingreso AS fecha_factura,
	a.total AS total_factura,
	CAST('' AS VARCHAR(20)) AS numero_factura,
	a.item,
	a.total_items,
	b.numero AS numero_contrato,
	b.codigo_cliente,
	b.id_radio_contrato,
	b.codigo_vendedor,
	h.descripcion AS nombre_vendedor,
	c.razon_social,
	b.numero_orden,
	b.codigo_anunciante,
	d.descripcion AS descripcion_anunciante,
	e.p_descuento AS porcentaje_descuento,
	e.codigo_programa,
	f.descripcion AS descripcion_programa,
	e.codigo_espacio,
	g.descripcion AS tipo_compra,
	i.codigo AS codigo_emisora,
	i.razon_social AS nombre_emisora
FROM tbl_radio_contratos_facturas_pendientes a
JOIN tbl_radio_contratos b ON a.id_radio_contrato = b.id_radio_contrato
JOIN tbl_clientes c ON b.codigo_cliente = c.codigo
JOIN tbl_radio_anunciantes d ON b.codigo_anunciante = d.codigo
JOIN tbl_radio_contratos_programas e ON b.id_radio_contrato = e.id_radio_contrato
JOIN tbl_radio_programas f ON e.codigo_programa = f.codigo
JOIN tbl_radio_espacios g ON e.codigo_espacio = g.codigo
JOIN tbl_vendedores h ON b.codigo_vendedor = h.codigo
JOIN tbl_radio_emisoras i ON f.codigo_emisora = i.codigo
