CREATE VIEW "v_rep_facturacion_proveedor" AS 
SELECT 
	a.numero AS fp_numero,
	a.fecha_ingreso AS fp_fecha_ingreso,
	a.monto_neto AS fp_monto_neto,
	a.monto_descuento AS fp_monto_descuento,
	a.sub_total AS fp_sub_total,
	a.impuesto AS fp_impuesto,
	a.p_descuento AS fp_p_descuento,
	a.p_iva AS fp_p_iva,
	a.tasa_dolar AS fp_tasa_dolar,
	a.codigo_esquema_pago AS fp_codigo_esquema_pago,
	a.total AS fp_total,
	a.observaciones AS fp_observaciones,
	a.codigo_moneda AS fp_codigo_moneda,
	a.concepto AS fp_concepto,
	b.codigo_producto AS dfp_codigo_producto,
	b.descripcion AS dfp_descripcion,
	b.cantidad AS dfp_cantidad,
	b.costo AS dfp_costo,
	b.p_descuento AS dfp_descuento,
	b.monto_descuento AS dfp_monto_descuento,
	b.impuesto AS dfp_impuesto,
	c.prefijo_rif AS cli_prefijo_rif,
	c.rif AS cli_rif,
	c.telefonos AS cli_telefonos,
	c.razon_social AS cli_razon_social,
	c.direccion_fiscal_1 AS cli_direccion_fiscal_1,
	c.direccion_fiscal_2 AS cli_direccion_fiscal_2,
	d.codigo_division AS pro_codigo_division
FROM tbl_facturacion_proveedores a
LEFT JOIN tbl_det_facturacion_proveedores b ON a.id_facturacion_proveedor = b.id_facturacion_proveedor
JOIN tbl_proveedores c ON a.codigo_proveedor = c.codigo
LEFT JOIN tbl_productos d ON b.codigo_producto = d.codigo
