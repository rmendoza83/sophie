CREATE VIEW "v_rep_radio_contrato" AS 
SELECT 
	a.numero,
	a.fecha_inicial,
	a.fecha_final,
	a.numero_orden,
	a.fecha_orden,
	a.nro_facturas,
	a.codigo_cliente,
	b.razon_social AS razon_social_cliente,
	a.codigo_anunciante,
	c.descripcion AS descripncion_anunciante,
	a.codigo_vendedor,
	d.descripcion AS descripcion_vendedor,
	a.codigo_zona,
	e.descripcion AS descripcion_zona,
	a.codigo_rubro,
	f.descripcion AS descripcion_rubro,
	a.observaciones,
	a.producto,
	a.total,
	a.intercambio,
	a.efectivo,
	a.venta_nacional,
	g.codigo_programa,
	h.descripcion AS descripcion_programa,
	g.codigo_espacio,
	i.descripcion AS descripcion_espacio,
	h.codigo_emisora,
	j.razon_social AS razon_social_emisora,
	g.cunas_diarias,
	g.total_cunas,
	g.duracion,
	g.monto_agencia,
	g.porcentaje,
	g.monto_emisora AS radio_contrato_programa_monto_emisora,
	g.comision AS radio_contrato_programa_comision,
	g.monto_real AS radio_contrato_programa_monto_real,
	g.tarifa AS radio_contrato_programa_tarifa,
	g.costo_bruto AS radio_contrato_programa_costo_bruto,
	g.p_descuento AS radio_contrato_programa_p_descuento,
	k.item AS radio_contratos_facturas_pendientes_item,
	k.total_items AS radio_contratos_facturas_pendientes_total_items,
	k.total AS radio_contratos_facturas_pendientes_total_cuotas,
	k.fecha_inicial AS radio_contratos_facturas_pendientes_fecha_inicial_coutas,
	k.fecha_final AS radio_contratos_facturas_pendientes_fecha_final_coutas,
	k.observaciones AS radio_contratos_facturas_pendientes_observaciones
FROM tbl_radio_contratos a
LEFT JOIN tbl_clientes b ON a.codigo_cliente = b.codigo
LEFT JOIN tbl_radio_anunciantes c ON a.codigo_anunciante = c.codigo
LEFT JOIN tbl_vendedores d ON a.codigo_vendedor = d.codigo
LEFT JOIN tbl_zonas e ON a.codigo_zona = e.codigo
LEFT JOIN tbl_radio_rubros f ON a.codigo_rubro = f.codigo
LEFT JOIN tbl_radio_contratos_programas g ON a.id_radio_contrato = g.id_radio_contrato
LEFT JOIN tbl_radio_programas h ON g.codigo_programa = h.codigo
LEFT JOIN tbl_radio_espacios i ON g.codigo_espacio = i.codigo
LEFT JOIN tbl_radio_emisoras j ON h.codigo_emisora = j.codigo
LEFT JOIN v_radio_contratos_distribucion_facturas k ON a.id_radio_contrato = k.id_radio_contrato
