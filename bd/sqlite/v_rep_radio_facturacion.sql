CREATE VIEW "v_rep_radio_facturacion" AS 
 SELECT
	a.fecha_ingreso AS radio_contratos_facturas_generadas_fecha_ingreso,
	a.fecha_inicial AS radio_contratos_facturas_generadas_fecha_inicial,
	a.fecha_final AS radio_contratos_facturas_generadas_fecha_final,
	a.total AS radio_contratos_facturas_generadas_total,
	a.id_facturacion_cliente,
	b.numero AS radio_contratos_numero_contrato,
	b.codigo_cliente AS radio_contratos_codigo_cliente,
	b.codigo_agencia AS codigo_agencia_contrato,
	b.total AS radio_contratos_total_contrato,
	b.numero_orden,
	b.efectivo,
	b.intercambio,
	b.venta_nacional,
	c.razon_social,
	d.numero AS numero_factura_cliente,
	d.fecha_ingreso AS fecha_factura,
	d.codigo_cliente AS codigo_cliente_factura,
	d.monto_neto AS sub_total_factura,
	d.p_iva AS porcentaje_iva_factura,
	d.impuesto AS impuesto_factura,
	d.total AS total_factura,
	c.razon_social AS nombre_cliente_factura,
	e.codigo_anunciante,
	f.descripcion AS nombre_anunciante,
	CASE
		WHEN b.efectivo THEN d.monto_neto
		ELSE CAST(0 AS NUMERIC)
	END AS total_efectivo,
	CASE
		WHEN b.intercambio THEN d.monto_neto
		ELSE CAST(0 AS NUMERIC)
	END AS total_intercambio,
	CASE
		WHEN b.venta_nacional THEN d.monto_neto
		ELSE CAST(0 AS NUMERIC)
	END AS total_venta_nacional
FROM tbl_radio_contratos_facturas_generadas a
LEFT JOIN tbl_radio_contratos b ON a.id_radio_contrato = b.id_radio_contrato
LEFT JOIN tbl_clientes c ON b.codigo_cliente = c.codigo
LEFT JOIN tbl_facturacion_clientes d ON a.id_facturacion_cliente = d.id_facturacion_cliente
LEFT JOIN tbl_radio_contratos e ON a.id_radio_contrato = e.id_radio_contrato
LEFT JOIN tbl_radio_anunciantes f ON e.codigo_anunciante = f.codigo
