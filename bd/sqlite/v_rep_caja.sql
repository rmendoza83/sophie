CREATE VIEW "v_rep_caja" AS 
SELECT 
	b.desde,
	b.numero_documento,
	b.codigo_moneda,
	b.codigo_cliente,
	b.codigo_vendedor,
	b.codigo_zona,
	b.fecha_ingreso,
	b.fecha_recepcion,
	b.fecha_venc_ingreso,
	b.fecha_venc_recepcion,
	b.fecha_libros,
	b.fecha_contable,
	b.fecha_cobranza,
	b.numero_cuota,
	b.total_cuotas,
	b.orden_cobranza,
	b.monto_neto,
	b.impuesto,
	b.total,
	b.p_iva,
	b.id_cobranza,
	b.id_contado_cobranza,
	a.codigo_forma_pago AS detco_codigo_forma_pago,
	a.monto AS detco_monto,
	a.numero_documento AS detco_numero_documento,
	c.razon_social AS cli_razon_social,
	d.codigo AS zon_codigo,
	d.descripcion AS zon_descripcion,
	e.codigo AS forpag_codigo,
	e.descripcion AS forpag_descripcion
FROM tbl_det_contado_cobranzas a
JOIN tbl_cobranzas_cobradas b ON a.id_contado_cobranza = b.id_contado_cobranza
JOIN tbl_clientes c ON b.codigo_cliente = c.codigo
JOIN tbl_zonas d ON b.codigo_zona = d.codigo
JOIN tbl_formas_pago e ON a.codigo_forma_pago = e.codigo
WHERE a.desde <> 'AC'

UNION

SELECT 
	b.desde,
	b.numero_documento,
	b.codigo_moneda,
	b.codigo_cliente,
	b.codigo_vendedor,
	b.codigo_zona,
	b.fecha_ingreso,
	b.fecha_recepcion,
	b.fecha_venc_ingreso,
	b.fecha_venc_recepcion,
	b.fecha_libros,
	b.fecha_contable,
	b.fecha_cobranza,
	b.numero_cuota,
	b.total_cuotas,
	b.orden_cobranza,
	b.monto_neto,
	b.impuesto,
	b.total,
	b.p_iva,
	b.id_cobranza,
	b.id_contado_cobranza,
	a.codigo_forma_pago AS detco_codigo_forma_pago,
	a.monto AS detco_monto,
	a.numero_documento AS detco_numero_documento,
	c.razon_social AS cli_razon_social,
	d.codigo AS zon_codigo,
	d.descripcion AS zon_descripcion,
	e.codigo AS forpag_codigo,
	e.descripcion AS forpag_descripcion
FROM tbl_det_contado_cobranzas a
JOIN tbl_cuentas_x_cobrar b ON a.id_contado_cobranza = b.id_contado_cobranza
JOIN tbl_clientes c ON b.codigo_cliente = c.codigo
JOIN tbl_zonas d ON b.codigo_zona = d.codigo
JOIN tbl_formas_pago e ON a.codigo_forma_pago = e.codigo
WHERE a.desde = 'AC'
