CREATE VIEW "v_rep_radio_documentos_anulados" AS 
SELECT
	a.desde,
	a.numero_documento AS numero_factura,
	a.serie,
	a.numero_control,
	a.fecha_documento,
	a.codigo_cliente,
	b.razon_social AS nombre_cliente,
	a.codigo_proveedor,
	c.razon_social AS nombre_proveedor,
	a.codigo_zona,
	d.descripcion AS nombre_zona,
	a.sub_total,
	a.impuesto,
	a.total,
	a.p_iva,
	a.hora AS hora_anulado,
	a.fecha AS fecha_anulado
FROM tbl_documentos_anulados a
LEFT JOIN tbl_clientes b ON a.codigo_cliente = b.codigo
LEFT JOIN tbl_proveedores c ON a.codigo_proveedor = b.codigo
LEFT JOIN tbl_zonas d ON a.codigo_zona = d.codigo
