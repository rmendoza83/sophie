CREATE VIEW "v_rep_estado_cuenta_proveedor_analisis" AS 
SELECT 
	a."from",
	a.cxp_numero_documento,
	a.cxp_desde,
	a.cxp_fecha_ingreso,
	a.cxp_fecha_venc_ingreso,
	a.cxp_fecha_recepcion,
	a.cxp_codigo_proveedor,
	a.cxp_codigo_zona,
	a.cxp_monto_neto,
	a.cxp_impuesto,
	a.cxp_total,
	a.cxp_p_iva,
	a.cxp_numero_cuota,
	a.cxp_total_cuotas,
	a.cxp_orden_pago,
	CASE
		WHEN a.cxp_desde = 'FP' THEN 'Factura'
		WHEN a.cxp_desde = 'AP' THEN 'Anticipo'
		WHEN a.cxp_desde = 'NDP' THEN 'Nota Debito'
		WHEN a.cxp_desde = 'NCP' THEN 'Nota Credito'
		ELSE ''
	END AS concepto,
	CASE
		WHEN (CAST(JULIANDAY('now') AS INTEGER) <= CAST(JULIANDAY(a.cxp_fecha_venc_ingreso) AS INTEGER)) THEN
		CASE
				WHEN a.cxp_desde = 'AP' OR a.cxp_desde = 'NCP' THEN a.cxp_total * CAST(-1 AS NUMERIC)
				ELSE a.cxp_total
		END
		ELSE CAST(0 AS NUMERIC)
	END AS monto_no_vencido,
	CASE
			WHEN (CAST(JULIANDAY('now') AS INTEGER) > CAST(JULIANDAY(a.cxp_fecha_venc_ingreso) AS INTEGER)) AND ((CAST(JULIANDAY('now') AS INTEGER) - CAST(JULIANDAY(a.cxp_fecha_venc_ingreso) AS INTEGER)) > 0) AND ((CAST(JULIANDAY('now') AS INTEGER) - CAST(JULIANDAY(a.cxp_fecha_venc_ingreso) AS INTEGER)) <= 30) THEN
			CASE
					WHEN a.cxp_desde = 'AP' OR a.cxp_desde = 'NCP' THEN a.cxp_total * CAST(-1 AS NUMERIC)
					ELSE a.cxp_total
			END
			ELSE CAST(0 AS NUMERIC)
	END AS monto_vencido_30,
	CASE
			WHEN (CAST(JULIANDAY('now') AS INTEGER) > CAST(JULIANDAY(a.cxp_fecha_venc_ingreso) AS INTEGER)) AND ((CAST(JULIANDAY('now') AS INTEGER) - CAST(JULIANDAY(a.cxp_fecha_venc_ingreso) AS INTEGER)) > 30) AND ((CAST(JULIANDAY('now') AS INTEGER) - CAST(JULIANDAY(a.cxp_fecha_venc_ingreso) AS INTEGER)) <= 60) THEN
			CASE
					WHEN a.cxp_desde = 'AP' OR a.cxp_desde = 'NCP' THEN a.cxp_total * CAST(-1 AS NUMERIC)
					ELSE a.cxp_total
			END
			ELSE CAST(0 AS NUMERIC)
	END AS monto_vencido_60,
	CASE
			WHEN (CAST(JULIANDAY('now') AS INTEGER) > CAST(JULIANDAY(a.cxp_fecha_venc_ingreso) AS INTEGER)) AND ((CAST(JULIANDAY('now') AS INTEGER) - CAST(JULIANDAY(a.cxp_fecha_venc_ingreso) AS INTEGER)) > 60) AND ((CAST(JULIANDAY('now') AS INTEGER) - CAST(JULIANDAY(a.cxp_fecha_venc_ingreso) AS INTEGER)) <= 90) THEN
			CASE
					WHEN a.cxp_desde = 'AP' OR a.cxp_desde = 'NCP' THEN a.cxp_total * CAST(-1 AS NUMERIC)
					ELSE a.cxp_total
			END
			ELSE CAST(0 AS NUMERIC)
	END AS monto_vencido_90,
	CASE
			WHEN (CAST(JULIANDAY('now') AS INTEGER) > CAST(JULIANDAY(a.cxp_fecha_venc_ingreso) AS INTEGER)) AND ((CAST(JULIANDAY('now') AS INTEGER) - CAST(JULIANDAY(a.cxp_fecha_venc_ingreso) AS INTEGER)) > 90) THEN
			CASE
					WHEN a.cxp_desde = 'AP' OR a.cxp_desde = 'NCP' THEN a.cxp_total * CAST(-1 AS NUMERIC)
					ELSE a.cxp_total
			END
			ELSE CAST(0 AS NUMERIC)
	END AS monto_vencido_mas90,
	c.prefijo_rif,
	c.rif,
	c.telefonos,
	c.razon_social,
	c.direccion_fiscal_1,
	c.direccion_fiscal_2
FROM ( 
	SELECT 'CXP' AS "from",
		a1.numero_documento AS cxp_numero_documento,
		a1.desde AS cxp_desde,
		a1.fecha_ingreso AS cxp_fecha_ingreso,
		a1.fecha_venc_ingreso AS cxp_fecha_venc_ingreso,
		a1.fecha_recepcion AS cxp_fecha_recepcion,
		a1.codigo_proveedor AS cxp_codigo_proveedor,
		a1.codigo_zona AS cxp_codigo_zona,
		a1.monto_neto AS cxp_monto_neto,
		a1.impuesto AS cxp_impuesto,
		a1.total AS cxp_total,
		a1.p_iva AS cxp_p_iva,
		a1.numero_cuota AS cxp_numero_cuota,
		a1.total_cuotas AS cxp_total_cuotas,
		a1.orden_pago AS cxp_orden_pago
	FROM tbl_cuentas_x_pagar a1

	UNION

	SELECT 
		'PAGADA' AS "from",
		a1.numero_documento AS cxp_numero_documento,
		a1.desde AS cxp_desde,
		a1.fecha_ingreso AS cxp_fecha_ingreso,
		a1.fecha_venc_ingreso AS cxp_fecha_venc_ingreso,
		a1.fecha_recepcion AS cxp_fecha_recepcion,
		a1.codigo_proveedor AS cxp_codigo_proveedor,
		a1.codigo_zona AS cxp_codigo_zona,
		a1.monto_neto AS cxp_monto_neto,
		a1.impuesto AS cxp_impuesto,
		a1.total AS cxp_total,
		a1.p_iva AS cxp_p_iva,
		a1.numero_cuota AS cxp_numero_cuota,
		a1.total_cuotas AS cxp_total_cuotas,
		a1.orden_pago AS cxp_orden_pago
	FROM tbl_pagos_pagados a1
) a
JOIN tbl_proveedores c ON a.cxp_codigo_proveedor = c.codigo
