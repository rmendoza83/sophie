CREATE VIEW "v_rep_nota_debito_cliente" AS 
SELECT 
	a.numero AS ndc_numero,
	a.fecha_ingreso AS ndc_fecha_ingreso,
	a.codigo_vendedor AS ndc_codigo_vendedor,
	a.monto_neto AS ndc_monto_neto,
	a.monto_descuento AS ndc_monto_descuento,
	a.sub_total AS ndc_sub_total,
	a.impuesto AS ndc_impuesto,
	a.p_descuento AS ndc_p_descuento,
	a.p_iva AS ndc_p_iva,
	a.tasa_dolar AS ndc_tasa_dolar,
	a.total AS ndc_total,
	a.observaciones AS ndc_observaciones,
	a.codigo_moneda AS ndc_codigo_moneda,
	a.concepto AS ndc_concepto,
	b.prefijo_rif,
	b.rif,
	b.telefonos,
	b.razon_social,
	b.direccion_fiscal_1,
	b.direccion_fiscal_2
FROM tbl_nota_debito_clientes a
JOIN tbl_clientes b ON a.codigo_cliente = b.codigo
