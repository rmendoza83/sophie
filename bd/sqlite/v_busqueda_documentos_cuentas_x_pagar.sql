CREATE VIEW "v_busqueda_documentos_cuentas_x_pagar" AS 
SELECT 
	a.desde,
	a.numero_documento,
	a.codigo_proveedor,
	b.rif,
	b.nit,
	b.razon_social,
	b.contacto,
	a.fecha_ingreso,
	a.numero_cuota,
	a.total_cuotas,
	a.monto_neto,
	a.impuesto,
	a.total,
	c.monto_neto AS monto_neto_documento,
	c.impuesto AS impuesto_documento,
	c.total AS total_documento,
	a.id_pago
FROM tbl_cuentas_x_pagar a
JOIN tbl_proveedores b ON a.codigo_proveedor = b.codigo
JOIN tbl_facturacion_proveedores c ON a.id_pago = c.id_pago
WHERE a.desde = 'FP'

UNION

SELECT
	a.desde,
	a.numero_documento,
	a.codigo_proveedor,
	b.rif,
	b.nit,
	b.razon_social,
	b.contacto,
	a.fecha_ingreso,
	a.numero_cuota,
	a.total_cuotas,
	a.monto_neto,
	a.impuesto,
	a.total,
	c.monto_neto AS monto_neto_documento,
	c.impuesto AS impuesto_documento,
	c.total AS total_documento,
	a.id_pago
FROM tbl_cuentas_x_pagar a
JOIN tbl_proveedores b ON a.codigo_proveedor = b.codigo
JOIN tbl_nota_credito_proveedores c ON a.id_pago = c.id_pago
WHERE a.desde = 'NCP'

UNION

SELECT
	a.desde,
	a.numero_documento,
	a.codigo_proveedor,
	b.rif,
	b.nit,
	b.razon_social,
	b.contacto,
	a.fecha_ingreso,
	a.numero_cuota,
	a.total_cuotas,
	a.monto_neto,
	a.impuesto,
	a.total,
	c.monto_neto AS monto_neto_documento,
	c.impuesto AS impuesto_documento,
	c.total AS total_documento,
	a.id_pago
FROM tbl_cuentas_x_pagar a
JOIN tbl_proveedores b ON a.codigo_proveedor = b.codigo
JOIN tbl_nota_debito_proveedores c ON a.id_pago = c.id_pago
WHERE a.desde = 'NDP'

UNION

SELECT
	a.desde,
	a.numero_documento,
	a.codigo_proveedor,
	b.rif,
	b.nit,
	b.razon_social,
	b.contacto,
	a.fecha_ingreso,
	a.numero_cuota,
	a.total_cuotas,
	a.monto_neto,
	a.impuesto,
	a.total,
	c.monto_neto AS monto_neto_documento,
	0 AS impuesto_documento,
	c.monto_neto AS total_documento,
	a.id_pago
FROM tbl_cuentas_x_pagar a
JOIN tbl_proveedores b ON a.codigo_proveedor = b.codigo
JOIN tbl_anticipo_proveedores c ON a.id_pago = c.id_pago
WHERE a.desde = 'AP'
