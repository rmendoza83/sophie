CREATE VIEW "v_condominio_busqueda_recibo" AS 
SELECT
	CAST((b.codigo || '-' || SUBSTR('0000' || a.periodo_a,-4,4) || SUBSTR('00' || a.periodo_m,-2,2)) AS VARCHAR(20)) AS codigo,
	b.codigo AS codigo_conjunto_administracion,
	b.descripcion,
	a.periodo_a,
	a.periodo_m,
	a.fecha_ingreso,
	a.fecha_vencimiento
FROM tbl_condominio_recibos a
LEFT JOIN tbl_condominio_conjuntos_administracion b ON a.codigo_conjunto_administracion = b.codigo
ORDER BY b.codigo, a.periodo_a, a.periodo_m
