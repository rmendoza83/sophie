CREATE VIEW "v_rep_condominio_recibos" AS 
SELECT 
	a.codigo_conjunto_administracion AS cr_codigo_conjunto_administracion,
	a.periodo_a AS cr_periodo_a,
	a.periodo_m AS cr_periodo_m,
	a.fecha_ingreso AS cr_fecha_ingreso,
	a.fecha_vencimiento AS cr_fecha_vencimiento,
	a.monto_base AS cr_monto_base,
	a.p_reserva AS cr_p_reserva,
	a.monto_reserva AS cr_monto_reserva,
	a.monto_total AS cr_monto_total,
	b.codigo_inmueble AS cri_codigo_inmueble,
	b.monto_base AS cri_monto_base,
	b.p_reserva AS cri_p_reserva,
	b.monto_reserva AS cri_monto_reserva,
	b.monto_total AS cri_monto_total,
	c.codigo_gasto AS crig_codigo_gasto,
	c.monto AS crig_monto,
	d.descripcion AS cca_descripcion,
	e.codigo_propietario AS ci_codigo_propietario,
	e.descripcion AS ci_descripcion,
	e.p_alicuota AS ci_p_alicuota,
	f.prefijo_rif AS cp_prefijo_rif,
	f.rif AS cp_rif,
	f.razon_social AS cp_razon_social,
	g.descripcion AS cg_descripcion,
	g.extraordinario AS cg_extraordinario,
	h.codigo_gasto AS crgo_codigo_gasto,
	h.monto AS crgo_monto
FROM tbl_condominio_recibos a
JOIN tbl_condominio_recibos_inmuebles b ON a.codigo_conjunto_administracion = b.codigo_conjunto_administracion AND a.periodo_a = b.periodo_a AND a.periodo_m = b.periodo_m
JOIN tbl_condominio_recibos_inmuebles_gastos c ON b.codigo_conjunto_administracion = c.codigo_conjunto_administracion AND b.periodo_a = c.periodo_a AND b.periodo_m = c.periodo_m AND b.codigo_inmueble = c.codigo_inmueble
JOIN tbl_condominio_conjuntos_administracion d ON a.codigo_conjunto_administracion = d.codigo
JOIN tbl_condominio_inmuebles e ON b.codigo_conjunto_administracion = e.codigo_conjunto_administracion AND b.codigo_inmueble = e.codigo
JOIN tbl_condominio_propietarios f ON e.codigo_propietario = f.codigo
JOIN tbl_condominio_gastos g ON c.codigo_conjunto_administracion = g.codigo_conjunto_administracion AND c.codigo_gasto = g.codigo
JOIN tbl_condominio_recibos_gastos_ordinarios h ON c.codigo_conjunto_administracion = h.codigo_conjunto_administracion AND c.periodo_a = h.periodo_a AND c.periodo_m = h.periodo_m AND c.codigo_gasto = h.codigo_gasto
ORDER BY a.codigo_conjunto_administracion, a.periodo_a, a.periodo_m, b.codigo_inmueble, g.extraordinario, g.codigo
