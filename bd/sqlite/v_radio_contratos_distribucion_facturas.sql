CREATE VIEW "v_radio_contratos_distribucion_facturas" AS 
SELECT 
	a.id_radio_contrato,
	a.item,
	a.total_items,
	a.fecha_ingreso,
	a.total,
	a.fecha_inicial,
	a.fecha_final,
	a.observaciones,
	a.id_facturacion_cliente,
	a.numero_documento,
	1 AS facturado
FROM tbl_radio_contratos_facturas_generadas a

UNION

SELECT a.id_radio_contrato,
	a.item,
	a.total_items,
	a.fecha_ingreso,
	a.total,
	a.fecha_inicial,
	a.fecha_final,
	a.observaciones,
	-1 AS id_facturacion_cliente,
	CAST('' AS VARCHAR(20)) AS numero_documento,
	0 AS facturado
FROM tbl_radio_contratos_facturas_pendientes a
