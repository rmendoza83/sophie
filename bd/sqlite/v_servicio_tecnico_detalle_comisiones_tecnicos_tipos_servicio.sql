CREATE VIEW "v_servicio_tecnico_detalle_comisiones_tecnicos_tipos_servicio" AS 
SELECT
	a.codigo_tecnico,
	a.codigo_tipo_servicio,
	b.descripcion,
	a.p_comision,
	a.observaciones
FROM tbl_servicio_tecnico_comisiones_tecnicos_tipos_servicio a
JOIN tbl_servicio_tecnico_tipos_servicio b ON a.codigo_tipo_servicio = b.codigo
