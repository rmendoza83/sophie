CREATE VIEW "v_busqueda_anticipo_proveedores" AS 
SELECT 
	a.numero,
  a.codigo_proveedor,
	CAST((b.prefijo_rif || b.rif) AS VARCHAR(20)) AS rif,
	b.nit,
	b.razon_social,
	b.contacto
FROM tbl_anticipo_proveedores a
LEFT JOIN tbl_proveedores b ON a.codigo_proveedor = b.codigo
ORDER BY a.id_anticipo_proveedor
