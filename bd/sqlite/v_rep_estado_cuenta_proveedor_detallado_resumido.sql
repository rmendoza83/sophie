CREATE VIEW "v_rep_estado_cuenta_cliente_detallado_resumido" AS 
SELECT 
	a."from",
	a.cxc_numero_documento,
	a.cxc_desde,
	a.cxc_fecha_ingreso,
	a.cxc_fecha_venc_ingreso,
	a.cxc_fecha_recepcion,
	a.cxc_codigo_cliente,
	a.cxc_codigo_vendedor,
	a.cxc_codigo_zona,
	a.cxc_monto_neto,
	a.cxc_impuesto,
	a.cxc_total,
	a.cxc_p_iva,
	a.cxc_numero_cuota,
	a.cxc_total_cuotas,
	a.cxc_orden_cobranza,
	CASE
			WHEN a.cxc_desde = 'FC' THEN 'Factura'
			WHEN a.cxc_desde = 'AC' THEN 'Anticipo'
			WHEN a.cxc_desde = 'NDC' THEN 'Nota Debito'
			WHEN a.cxc_desde = 'NCC' THEN 'Nota Credito'
			ELSE ''
	END AS concepto,
	CASE
			WHEN a.cxc_desde = 'AC' OR a.cxc_desde = 'NCC' THEN a.cxc_total
			ELSE CAST(0 AS NUMERIC)
	END AS credito,
	CASE
			WHEN a.cxc_desde = 'FC' OR a.cxc_desde = 'NDC' THEN a.cxc_total
			ELSE CAST(0 AS NUMERIC)
	END AS debito,
	c.prefijo_rif,
	c.rif,
	c.telefonos,
	c.razon_social,
	c.direccion_fiscal_1,
	c.direccion_fiscal_2
FROM ( 
	SELECT 'CXC' AS "from",
		a1.numero_documento AS cxc_numero_documento,
		a1.desde AS cxc_desde,
		a1.fecha_ingreso AS cxc_fecha_ingreso,
		a1.fecha_venc_ingreso AS cxc_fecha_venc_ingreso,
		a1.fecha_recepcion AS cxc_fecha_recepcion,
		a1.codigo_cliente AS cxc_codigo_cliente,
		a1.codigo_vendedor AS cxc_codigo_vendedor,
		a1.codigo_zona AS cxc_codigo_zona,
		a1.monto_neto AS cxc_monto_neto,
		a1.impuesto AS cxc_impuesto,
		a1.total AS cxc_total,
		a1.p_iva AS cxc_p_iva,
		a1.numero_cuota AS cxc_numero_cuota,
		a1.total_cuotas AS cxc_total_cuotas,
		a1.orden_cobranza AS cxc_orden_cobranza
	FROM tbl_cuentas_x_cobrar a1
) a
JOIN tbl_clientes c ON a.cxc_codigo_cliente = c.codigo
