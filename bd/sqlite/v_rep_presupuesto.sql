CREATE VIEW "v_rep_presupuesto" AS 
SELECT 
	a.numero AS p_numero,
	a.fecha_ingreso AS p_fecha_ingreso,
	a.codigo_vendedor AS p_codigo_vendedor,
	a.monto_neto AS p_monto_neto,
	a.monto_descuento AS p_monto_descuento,
	a.sub_total AS p_sub_total,
	a.impuesto AS p_impuesto,
	a.p_descuento AS p_p_descuento,
	a.p_iva AS p_p_iva,
	a.tasa_dolar AS p_tasa_dolar,
	a.codigo_esquema_pago AS p_codigo_esquema_pago,
	a.total AS p_total,
	a.observaciones AS p_observaciones,
	a.codigo_moneda AS p_codigo_moneda,
	a.concepto AS p_concepto,
	b.codigo_producto,
	b.descripcion AS detfac_descripcion,
	b.cantidad AS detfac_cantidad,
	b.precio AS detfac_precio,
	b.p_descuento AS detfac_descuento,
	b.monto_descuento AS detfac_monto_descuento,
	b.impuesto AS detfac_impuesto,
	c.prefijo_rif,
	c.rif,
	c.telefonos,
	c.razon_social,
	c.direccion_fiscal_1,
	c.direccion_fiscal_2,
	d.codigo_division
FROM tbl_presupuestos a
LEFT JOIN tbl_det_presupuestos b ON a.id_presupuesto = b.id_presupuesto
JOIN tbl_clientes c ON a.codigo_cliente = c.codigo
LEFT JOIN tbl_productos d ON b.codigo_producto = d.codigo
