CREATE VIEW "v_radio_detalle_calculo_pago_comision" AS 
SELECT 
	CAST('FC' AS VARCHAR(10)) AS desde,
	a.numero,
	a.codigo_vendedor,
	a.fecha_ingreso,
	a.monto_neto
FROM tbl_facturacion_clientes a
WHERE NOT (a.numero IN (
	SELECT tbl_det_pagos_comisiones.numero_documento
	FROM tbl_det_pagos_comisiones
	WHERE tbl_det_pagos_comisiones.desde = 'FC'
))
