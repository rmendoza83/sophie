CREATE VIEW "v_taller_busqueda_servicio" AS 
SELECT
	a.numero,
	a.codigo_cliente,
	CAST((b.prefijo_rif || b.rif) AS VARCHAR(20)) AS rif,
	b.nit,
	b.razon_social,
	b.contacto,
	c.placa AS placa_vehiculo,
	c.marca AS marca_vehiculo,
	c.modelo AS modelo_vehiculo,
	d.codigo AS codigo_mecanico,
	d.descripcion AS nombre_mecanico,
	a.id_facturacion_cliente
FROM tbl_taller_servicio a
LEFT JOIN tbl_clientes b ON a.codigo_cliente = b.codigo
LEFT JOIN tbl_clientes_vehiculos c ON a.codigo_cliente = c.codigo_cliente AND a.placa_vehiculo_cliente = c.placa
LEFT JOIN tbl_vendedores d ON a.codigo_vendedor = d.codigo
ORDER BY a.id_taller_servicio
