CREATE VIEW "v_rep_radio_comision_vendedor" AS 
SELECT
	a.desde,
	f.numero AS numero_contrato,
	a.codigo_cliente,
	g.razon_social,
	a.numero_documento AS numero_factura,
	f.numero_orden,
	e.total AS monto_neto_factura,
	a.fecha_ingreso AS fecha_factura,
	c.fecha_ingreso AS fecha_liquidacion,
	c.numero AS numero_liquidacion,
	e.fecha_inicial AS _periodo_fecha_inicial,
	e.fecha_final AS _periodo_fecha_final,
	f.codigo_anunciante,
	h.descripcion AS nombre_anunciante,
	a.codigo_vendedor,
	i.descripcion AS nombre_vendedor,
	k.porcentaje AS porcentaje_comision_vendedor,
	CAST((e.total * (k.porcentaje / 100)) AS NUMERIC) AS monto_comision
FROM tbl_cobranzas_cobradas a
JOIN tbl_det_liquidacion_cobranzas b ON a.id_cobranza = b.id_cobranza AND a.numero_cuota = b.numero_cuota AND a.orden_cobranza = b.orden_cobranza
JOIN tbl_liquidacion_cobranzas c ON b.id_liquidacion_cobranza = c.id_liquidacion_cobranza
JOIN tbl_facturacion_clientes d ON a.id_cobranza = d.id_cobranza
JOIN v_radio_contratos_distribucion_facturas e ON d.id_facturacion_cliente = e.id_facturacion_cliente AND e.facturado
LEFT JOIN tbl_radio_contratos f ON e.id_radio_contrato = f.id_radio_contrato
JOIN tbl_clientes g ON a.codigo_cliente = g.codigo
JOIN tbl_radio_anunciantes h ON f.codigo_anunciante = h.codigo
JOIN tbl_vendedores i ON a.codigo_vendedor = i.codigo
JOIN tbl_vendedores_comisiones j ON i.codigo = j.codigo_vendedor
JOIN tbl_tipos_comisiones k ON j.codigo_tipo_comision = k.codigo
