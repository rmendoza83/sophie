CREATE VIEW "v_busqueda_liquidacion_cobranzas" AS 
SELECT 
	a.numero,
	a.codigo_cliente,
	CAST((b.prefijo_rif || b.rif) AS VARCHAR(20)) AS rif,
	b.nit,
	b.razon_social,
	b.contacto
FROM tbl_liquidacion_cobranzas a
LEFT JOIN tbl_clientes b ON a.codigo_cliente = b.codigo
ORDER BY a.codigo_cliente, a.numero
