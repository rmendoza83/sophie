CREATE VIEW "v_busqueda_documentos_retencion_islr" AS 
SELECT 
	CAST('FP' AS VARCHAR(10)) AS desde,
	a.numero,
	a.codigo_proveedor,
	b.rif,
	b.nit,
	b.razon_social,
	b.contacto,
	a.monto_neto
FROM tbl_facturacion_proveedores a
JOIN tbl_proveedores b ON a.codigo_proveedor = b.codigo
WHERE NOT (a.codigo_proveedor || '-' || a.numero) IN ( 
	SELECT (a1.codigo_proveedor || '-' || a2.numero_documento) AS codigo_unico
	FROM tbl_retenciones_islr a1
	JOIN tbl_det_retenciones_islr a2 ON a1.numero = a2.numero
	WHERE a2.desde = 'FP'
)

UNION

SELECT 
	CAST('NDP' AS VARCHAR(10)) AS desde,
	a.numero,
	a.codigo_proveedor,
	b.rif,
	b.nit,
	b.razon_social,
	b.contacto,
	a.monto_neto
FROM tbl_nota_debito_proveedores a
JOIN tbl_proveedores b ON a.codigo_proveedor = b.codigo
WHERE NOT (a.codigo_proveedor || '-' || a.numero) IN ( 
	SELECT (a1.codigo_proveedor || '-' || a2.numero_documento) AS codigo_unico
	FROM tbl_retenciones_islr a1
	JOIN tbl_det_retenciones_islr a2 ON a1.numero = a2.numero
	WHERE a2.desde = 'NDP'
)

UNION

SELECT 
	CAST('AP' AS VARCHAR(10)) AS desde,
	a.numero,
	a.codigo_proveedor,
	b.rif,
	b.nit,
	b.razon_social,
	b.contacto,
	a.monto_neto
FROM tbl_anticipo_proveedores a
JOIN tbl_proveedores b ON a.codigo_proveedor = b.codigo
WHERE NOT (a.codigo_proveedor || '-' || a.numero) IN ( 
	SELECT (a1.codigo_proveedor || '-' || a2.numero_documento) AS codigo_unico
	FROM tbl_retenciones_islr a1
	JOIN tbl_det_retenciones_islr a2 ON a1.numero = a2.numero
	WHERE a2.desde = 'AP'
)
