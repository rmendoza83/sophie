CREATE VIEW "v_rep_analisis_vencimiento_cliente" AS 
SELECT 
	a.numero_documento,
	CASE
		WHEN (CAST(JULIANDAY('now') AS INTEGER) <= CAST(JULIANDAY(a.fecha_venc_ingreso) AS INTEGER)) THEN a.total
		ELSE CAST(0 AS NUMERIC)
	END AS monto_no_vencido,
	CASE
		WHEN (CAST(JULIANDAY('now') AS INTEGER) > CAST(JULIANDAY(a.fecha_venc_ingreso) AS INTEGER)) AND ((CAST(JULIANDAY('now') AS INTEGER) - CAST(JULIANDAY(a.fecha_venc_ingreso) AS INTEGER)) > 0) AND ((CAST(JULIANDAY('now') AS INTEGER) - CAST(JULIANDAY(a.fecha_venc_ingreso) AS INTEGER)) <= 30) THEN a.total
		ELSE CAST(0 AS NUMERIC)
	END AS monto_vencido_30,
	CASE
		WHEN (CAST(JULIANDAY('now') AS INTEGER) > CAST(JULIANDAY(a.fecha_venc_ingreso) AS INTEGER)) AND ((CAST(JULIANDAY('now') AS INTEGER) - CAST(JULIANDAY(a.fecha_venc_ingreso) AS INTEGER)) > 30) AND ((CAST(JULIANDAY('now') AS INTEGER) - CAST(JULIANDAY(a.fecha_venc_ingreso) AS INTEGER)) <= 60) THEN a.total
		ELSE CAST(0 AS NUMERIC)
	END AS monto_vencido_60,
	CASE
		WHEN (CAST(JULIANDAY('now') AS INTEGER) > CAST(JULIANDAY(a.fecha_venc_ingreso) AS INTEGER)) AND ((CAST(JULIANDAY('now') AS INTEGER) - CAST(JULIANDAY(a.fecha_venc_ingreso) AS INTEGER)) > 60) AND ((CAST(JULIANDAY('now') AS INTEGER) - CAST(JULIANDAY(a.fecha_venc_ingreso) AS INTEGER)) <= 90) THEN a.total
		ELSE CAST(0 AS NUMERIC)
	END AS monto_vencido_90,
	CASE
		WHEN (CAST(JULIANDAY('now') AS INTEGER) > CAST(JULIANDAY(a.fecha_venc_ingreso) AS INTEGER)) AND ((CAST(JULIANDAY('now') AS INTEGER) - CAST(JULIANDAY(a.fecha_venc_ingreso) AS INTEGER)) > 90) THEN a.total
		ELSE CAST(0 AS NUMERIC)
	END AS monto_vencido_mas90
FROM tbl_cuentas_x_cobrar a
