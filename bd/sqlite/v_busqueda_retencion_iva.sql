CREATE VIEW "v_busqueda_retencion_iva" AS 
SELECT 
	a.numero,
	a.codigo_proveedor,
	CAST((b.prefijo_rif || b.rif) AS VARCHAR(20)) AS rif,
	b.nit,
	b.razon_social,
	b.contacto
FROM tbl_retenciones_iva a
LEFT JOIN tbl_proveedores b ON a.codigo_proveedor = b.codigo
ORDER BY a.codigo_proveedor, a.numero
