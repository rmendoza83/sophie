CREATE VIEW "v_rep_libro_venta" AS 
SELECT 
	CAST('FC' AS VARCHAR(6)) AS desde,
	a.fecha_libros,
	a.fecha_ingreso,
	CAST((b.prefijo_rif || b.rif) AS VARCHAR(20)) AS rif,
	b.razon_social,
	CAST('' AS VARCHAR(20)) AS numero_importacion,
	a.numero AS numero_factura,
	c.numero_control,
	CAST('' AS VARCHAR(20)) AS numero_nota_debito,
	CAST('' AS VARCHAR(20)) AS numero_nota_credito,
	CAST('01-Reg' AS VARCHAR(20)) AS tipo_transaccion,
	CAST('' AS VARCHAR(20)) AS factura_afectada,
	a.total AS total_factura,
	a.id_facturacion_cliente AS id_documento,
	CASE
		WHEN a.impuesto < ROUND(a.monto_neto * a.p_iva / 100, 4) THEN ROUND(a.monto_neto - a.impuesto * 100 / a.p_iva, 4)
		ELSE CAST(0 AS NUMERIC(18,4))
	END AS exentas_no_gravadas,
	CASE
		WHEN a.impuesto < ROUND(a.monto_neto * a.p_iva / 100, 4) THEN ROUND(a.impuesto * 100 / a.p_iva, 4)
		ELSE a.monto_neto
	END AS alicuota_general_monto_base_factura,
	a.p_iva AS alicuota_general_iva_factura,
	a.impuesto AS alicuota_general_impuesto_factura,
	a.monto_neto AS alicuota_reducida_monto_base_factura,
	a.p_iva AS alicuota_reducida_iva_factura,
	a.impuesto AS alicuota_reducida_impuesto_factura,
	CAST(COALESCE(d.monto_retencion, 0) AS NUMERIC(18,4)) AS iva_retenido_vendedor,
	CAST(COALESCE(a.impuesto - d.monto_retencion, 0) AS NUMERIC(18,4)) AS iva_pagado_no_retenido,
	e.numero_comprobante AS numero_comprobante_iva
FROM tbl_facturacion_clientes a
JOIN tbl_clientes b ON a.codigo_cliente = b.codigo
LEFT JOIN tbl_documentos_legales c ON a.numero = c.numero_documento AND c.desde = 'FC'
LEFT JOIN tbl_det_retenciones_iva d ON a.numero = d.numero_documento AND d.desde = 'FC'
LEFT JOIN tbl_retenciones_iva e ON d.numero = e.numero

UNION

SELECT 
	CAST('NDC' AS VARCHAR(6)) AS desde,
	a.fecha_libros,
	a.fecha_ingreso,
	CAST((b.prefijo_rif || b.rif) AS VARCHAR(20)) AS rif,
	b.razon_social,
	CAST('' AS VARCHAR(20)) AS numero_importacion,
	CAST('' AS VARCHAR(20)) AS numero_factura,
	c.numero_control,
	a.numero AS numero_nota_debito,
	CAST('' AS VARCHAR(20)) AS numero_nota_credito,
	CAST('01-Reg' AS VARCHAR(20)) AS tipo_transaccion,
	CAST('' AS VARCHAR(20)) AS factura_afectada,
	a.total AS total_factura,
	a.id_nota_debito_cliente AS id_documento,
	CASE
		WHEN a.impuesto < ROUND(a.monto_neto * a.p_iva / 100, 4) THEN ROUND(a.monto_neto - a.impuesto * 100 / a.p_iva, 4)
		ELSE CAST(0 AS NUMERIC(18,4))
	END AS exentas_no_gravadas,
	CASE
		WHEN a.impuesto < ROUND(a.monto_neto * a.p_iva / 100, 4) THEN ROUND(a.impuesto * 100 / a.p_iva, 4)
		ELSE a.monto_neto
	END AS alicuota_general_monto_base_factura,
	a.p_iva AS alicuota_general_iva_factura,
	a.impuesto AS alicuota_general_impuesto_factura,
	a.monto_neto AS alicuota_reducida_monto_base_factura,
	a.p_iva AS alicuota_reducida_iva_factura,
	a.impuesto AS alicuota_reducida_impuesto_factura,
	CAST(COALESCE(d.monto_retencion, 0) AS NUMERIC(18,4)) AS iva_retenido_vendedor,
	CAST(COALESCE(a.impuesto - d.monto_retencion, 0) AS NUMERIC(18,4)) AS iva_pagado_no_retenido,
	e.numero_comprobante AS numero_comprobante_iva
FROM tbl_nota_debito_clientes a
JOIN tbl_clientes b ON a.codigo_cliente = b.codigo
LEFT JOIN tbl_documentos_legales c ON a.numero = c.numero_documento AND c.desde = 'NDC'
LEFT JOIN tbl_det_retenciones_iva d ON a.numero = d.numero_documento AND d.desde = 'NDC'
LEFT JOIN tbl_retenciones_iva e ON d.numero = e.numero

UNION

SELECT 
	CAST('NCC' AS VARCHAR(6)) AS desde,
	a.fecha_libros,
	a.fecha_ingreso,
	CAST((b.prefijo_rif || b.rif) AS VARCHAR(20)) AS rif,
	b.razon_social,
	CAST('' AS VARCHAR(20)) AS numero_importacion,
	CAST('' AS VARCHAR(20)) AS numero_factura,
	c.numero_control,
	CAST('' AS VARCHAR(20)) AS numero_nota_debito,
	a.numero AS numero_nota_credito,
	CAST('01-Reg' AS VARCHAR(20)) AS tipo_transaccion,
	CAST('' AS VARCHAR(20)) AS factura_afectada,
	a.total AS total_factura,
	a.id_nota_credito_cliente AS id_documento,
	CASE
		WHEN a.impuesto < ROUND(a.monto_neto * a.p_iva / 100, 4) THEN ROUND(a.monto_neto - a.impuesto * 100 / a.p_iva, 4)
		ELSE CAST(0 AS NUMERIC(18,4))
	END AS exentas_no_gravadas,
	CASE
		WHEN a.impuesto < ROUND(a.monto_neto * a.p_iva / 100, 4) THEN ROUND(a.impuesto * 100 / a.p_iva, 4)
		ELSE a.monto_neto
	END AS alicuota_general_monto_base_factura,
	a.p_iva AS alicuota_general_iva_factura,
	a.impuesto AS alicuota_general_impuesto_factura,
	a.monto_neto AS alicuota_reducida_monto_base_factura,
	a.p_iva AS alicuota_reducida_iva_factura,
	a.impuesto AS alicuota_reducida_impuesto_factura,
	CAST(COALESCE(d.monto_retencion, 0) AS NUMERIC(18,4)) AS iva_retenido_vendedor,
	CAST(COALESCE(a.impuesto - d.monto_retencion, 0) AS NUMERIC(18,4)) AS iva_pagado_no_retenido,
	e.numero_comprobante AS numero_comprobante_iva
	FROM tbl_nota_credito_clientes a
	JOIN tbl_clientes b ON a.codigo_cliente = b.codigo
	LEFT JOIN tbl_documentos_legales c ON a.numero = c.numero_documento AND c.desde = 'NCC'
	LEFT JOIN tbl_det_retenciones_iva d ON a.numero = d.numero_documento AND d.desde = 'NCC'
	LEFT JOIN tbl_retenciones_iva e ON d.numero = e.numero
	