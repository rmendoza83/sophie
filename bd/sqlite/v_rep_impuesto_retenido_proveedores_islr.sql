CREATE VIEW "v_rep_impuesto_retenido_proveedores_islr" AS 
SELECT 
	a.numero AS numero_retencion_islr,
	a.codigo_proveedor,
	a.codigo_zona,
	a.fecha_ingreso,
	a.fecha_contable,
	a.total,
	a.numero_comprobante,
	b.desde AS desde_det_islr,
	b.numero_documento AS numero_documento_det_islr,
	b.codigo_retencion_1 AS codigo_retencion_1_det_islr,
	b.monto_retencion_1 AS monto_retencion_1_det_islr,
	c.descripcion AS descripcion_1,
	b.codigo_retencion_2 AS codigo_retencion_2_det_islr,
	b.monto_retencion_2 AS monto_retencion_2_det_islr,
	c.descripcion AS descripcion_2,
	b.codigo_retencion_3 AS codigo_retencion_3_det_islr,
	b.monto_retencion_3 AS monto_retencion_3_det_islr,
	c.descripcion AS descripcion_3,
	b.codigo_retencion_4 AS codigo_retencion_4_det_islr,
	b.monto_retencion_4 AS monto_retencion_4_det_islr,
	c.descripcion AS descripcion_4,
	b.codigo_retencion_5 AS codigo_retencion_5_det_islr,
	b.monto_retencion_5 AS monto_retencion_5_det_islr,
	c.descripcion AS descripcion_5,
	h.numero AS numero_factura,
	h.fecha_ingreso AS fecha_ingreso_factura,
	h.fecha_recepcion AS fecha_recepcion_factura,
	h.fecha_contable AS fecha_contable_factura,
	h.fecha_libros AS fecha_libros_factura,
	h.monto_neto AS monto_neto_factura,
	i.serie AS serie_legal,
	i.numero_control,
	j.razon_social
FROM tbl_retenciones_islr a
JOIN tbl_det_retenciones_islr b ON a.numero = b.numero
LEFT JOIN tbl_tipos_retenciones c ON b.codigo_retencion_1 = c.codigo
LEFT JOIN tbl_tipos_retenciones d ON b.codigo_retencion_2 = d.codigo
LEFT JOIN tbl_tipos_retenciones e ON b.codigo_retencion_3 = e.codigo
LEFT JOIN tbl_tipos_retenciones f ON b.codigo_retencion_4 = f.codigo
LEFT JOIN tbl_tipos_retenciones g ON b.codigo_retencion_5 = g.codigo
JOIN tbl_facturacion_proveedores h ON b.numero_documento = h.numero AND b.desde = 'FP'
JOIN tbl_documentos_legales i ON b.numero_documento = i.numero_documento AND b.desde = 'FP'
JOIN tbl_proveedores j ON a.codigo_proveedor = j.codigo
ORDER BY b.codigo_retencion_1
