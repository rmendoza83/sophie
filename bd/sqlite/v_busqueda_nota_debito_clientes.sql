CREATE VIEW "v_busqueda_nota_debito_clientes" AS 
SELECT 
	a.numero,
	a.codigo_cliente,
	CAST((b.prefijo_rif || b.rif) AS VARCHAR(20)) AS rif,
	b.nit,
	b.razon_social,
	b.contacto
FROM tbl_nota_debito_clientes a
LEFT JOIN tbl_clientes b ON a.codigo_cliente = b.codigo
ORDER BY a.id_nota_debito_cliente
