CREATE VIEW "v_busqueda_radio_contratos" AS 
 SELECT 
	a.numero,
	a.codigo_cliente,
	CAST((b.prefijo_rif || b.rif) AS VARCHAR(20)) AS rif_cliente,
	b.nit AS nit_cliente,
	b.razon_social AS razon_social_cliente,
	b.contacto AS contacto_cliente,
	a.codigo_anunciante,
	c.descripcion AS descripcion_anunciante,
	a.codigo_agencia,
	CAST((d.prefijo_rif || d.rif) AS VARCHAR(20)) AS rif_agencia,
	d.nit AS nit_agencia,
	d.razon_social AS razon_social_agencia,
	d.contacto AS contacto_agencia,
	a.codigo_vendedor,
	e.descripcion AS descripcion_vendedor,
	a.codigo_rubro,
	f.descripcion AS descripcion_rubro,
	a.numero_orden,
	a.producto,
	a.observaciones
FROM tbl_radio_contratos a
LEFT JOIN tbl_clientes b ON a.codigo_cliente = b.codigo 
LEFT JOIN tbl_radio_anunciantes c ON a.codigo_anunciante = c.codigo 
LEFT JOIN tbl_clientes d ON a.codigo_agencia = d.codigo 
LEFT JOIN tbl_vendedores e ON a.codigo_vendedor = e.codigo 
LEFT JOIN tbl_radio_rubros f ON a.codigo_rubro = f.codigo 
ORDER BY a.id_radio_contrato
