CREATE VIEW "v_busqueda_conteos_inventario" AS 
SELECT 
	a.numero,
	a.codigo_zona,
	b.descripcion AS descripcion_zona,
	a.codigo_estacion,
	c.descripcion AS descripcion_estacion,
	CAST(a.observaciones AS VARCHAR) AS observaciones
FROM tbl_conteos_inventario a
JOIN tbl_zonas b ON a.codigo_zona = b.codigo
JOIN tbl_estaciones c ON a.codigo_estacion = c.codigo
