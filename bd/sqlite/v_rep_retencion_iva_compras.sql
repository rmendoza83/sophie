CREATE VIEW "v_rep_retencion_iva_compras" AS 
SELECT 
	CAST('01-Fac' AS VARCHAR(10)) AS tipo_documento,
	a.numero AS ret_numero,
	a.codigo_proveedor AS ret_codigo_pro,
	a.numero_comprobante,
	c.razon_social,
	c.prefijo_rif AS prefijo_rif_prov,
	c.rif AS ret_rif_pro,
	c.telefonos,
	c.direccion_fiscal_1,
	c.direccion_fiscal_2,
	a.codigo_zona AS ret_codigo_zona,
	a.fecha_ingreso AS ret_fecha_ingreso,
	a.fecha_contable AS ret_fecha_contable,
	a.total AS ret_total,
	b.desde,
	b.numero_documento,
	b.codigo_retencion,
	b.monto_retencion,
	j.fecha_ingreso AS fecha_documento,
	j.sub_total,
	j.impuesto,
	j.total,
	j.p_iva,
	k.numero_control
FROM tbl_retenciones_iva a
JOIN tbl_det_retenciones_iva b ON a.numero = b.numero
JOIN tbl_proveedores c ON a.codigo_proveedor = c.codigo
JOIN tbl_zonas d ON a.codigo_zona = d.codigo
LEFT JOIN tbl_facturacion_proveedores j ON b.numero_documento = j.numero
LEFT JOIN tbl_documentos_legales k ON b.desde = k.desde AND b.numero_documento = k.numero_documento
WHERE b.desde = 'FP'

UNION

SELECT
	CAST('02-Ndp' AS VARCHAR(10)) AS tipo_documento,
	a.numero AS ret_numero,
	a.codigo_proveedor AS ret_codigo_pro,
	a.numero_comprobante,
	c.razon_social,
	c.prefijo_rif AS prefijo_rif_prov,
	c.rif AS ret_rif_pro,
	c.telefonos,
	c.direccion_fiscal_1,
	c.direccion_fiscal_2,
	a.codigo_zona AS ret_codigo_zona,
	a.fecha_ingreso AS ret_fecha_ingreso,
	a.fecha_contable AS ret_fecha_contable,
	a.total AS ret_total,
	b.desde,
	b.numero_documento,
	b.codigo_retencion,
	b.monto_retencion,
	j.fecha_ingreso AS fecha_documento,
	j.sub_total,
	j.impuesto,
	j.total,
	j.p_iva,
	k.numero_control
FROM tbl_retenciones_iva a
JOIN tbl_det_retenciones_iva b ON a.numero = b.numero
JOIN tbl_proveedores c ON a.codigo_proveedor = c.codigo
JOIN tbl_zonas d ON a.codigo_zona = d.codigo
LEFT JOIN tbl_nota_debito_proveedores j ON b.numero_documento = j.numero
LEFT JOIN tbl_documentos_legales k ON b.desde = k.desde AND b.numero_documento = k.numero_documento
WHERE b.desde = 'NDP'

UNION

SELECT
	'01-Ncp' AS tipo_documento,
	a.numero AS ret_numero,
	a.codigo_proveedor AS ret_codigo_pro,
	a.numero_comprobante,
	c.razon_social,
	c.prefijo_rif AS prefijo_rif_prov,
	c.rif AS ret_rif_pro,
	c.telefonos,
	c.direccion_fiscal_1,
	c.direccion_fiscal_2,
	a.codigo_zona AS ret_codigo_zona,
	a.fecha_ingreso AS ret_fecha_ingreso,
	a.fecha_contable AS ret_fecha_contable,
	a.total AS ret_total,
	b.desde,
	b.numero_documento,
	b.codigo_retencion,
	b.monto_retencion,
	j.fecha_ingreso AS fecha_documento,
	j.sub_total,
	j.impuesto,
	j.total,
	j.p_iva,
	k.numero_control
FROM tbl_retenciones_iva a
JOIN tbl_det_retenciones_iva b ON a.numero = b.numero
JOIN tbl_proveedores c ON a.codigo_proveedor = c.codigo
JOIN tbl_zonas d ON a.codigo_zona = d.codigo
LEFT JOIN tbl_nota_credito_proveedores j ON b.numero_documento = j.numero
LEFT JOIN tbl_documentos_legales k ON b.desde = k.desde AND b.numero_documento = k.numero_documento
WHERE b.desde = 'NCP'
