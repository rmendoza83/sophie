CREATE VIEW "v_rep_liquidacion_pagos" AS 
SELECT 
	a.desde,
	a.numero_documento,
	a.codigo_proveedor,
	a.fecha_ingreso AS fecha_ingreso_pagos_pagados,
	a.fecha_venc_ingreso AS fecha_venc_ingreso_pagos_pagados,
	a.fecha_pago AS fecha_pago_pagos_pagado,
	a.numero_cuota,
	a.total_cuotas,
	a.monto_neto AS monto_neto_cobranza_cobrada,
	a.impuesto AS impuesto_cobranza_cobrada,
	CASE
		WHEN a.desde = 'AP' OR a.desde = 'NCP' THEN a.total * CAST(-1 AS NUMERIC)
		ELSE a.total
	END AS total_cobranza_cobrada,
	a.p_iva AS p_iva_cobranza_cobrada,
	CASE
		WHEN a.desde = 'AP' OR a.desde = 'NCP' THEN b.abonado * CAST(-1 AS NUMERIC)
		ELSE b.abonado
	END AS abonado,
	b.codigo_retencion_iva,
	b.monto_retencion_iva,
	l.tarifa AS tarifa_iva,
	l.descripcion AS descripcion_iva,
	b.codigo_retencion_1,
	b.monto_retencion_1,
	g.tarifa AS tarifa_1,
	g.descripcion AS descripcion1,
	b.codigo_retencion_2,
	b.monto_retencion_2,
	h.tarifa AS tarifa_2,
	h.descripcion AS descripcion2,
	b.codigo_retencion_3,
	b.monto_retencion_3,
	i.tarifa AS tarifa_3,
	i.descripcion AS descripcion3,
	b.codigo_retencion_4,
	b.monto_retencion_4,
	j.tarifa AS tarifa_4,
	j.descripcion AS descripcion4,
	b.codigo_retencion_5,
	b.monto_retencion_5,
	k.tarifa AS tarifa_5,
	k.descripcion AS descripcion5,
	b.monto_retencion_1 + b.monto_retencion_2 + b.monto_retencion_3 + b.monto_retencion_4 + b.monto_retencion_5 AS suma_monto_retenido_islr,
	b.monto_retencion_1 + b.monto_retencion_2 + b.monto_retencion_3 + b.monto_retencion_4 + b.monto_retencion_5 + b.monto_retencion_iva AS suma_total_islr_iva,
	c.numero,
	c.fecha_ingreso AS fecha_ingreso_liquidacion_cobranza,
	c.monto_neto AS monto_neto_liquidacion_cobranza,
	c.total_retencion AS total_retencion_liquidacion_cobranza,
	c.total AS total_liquidacion_cobranza,
	c.numero_documento_bancario AS numero_documento_liquidacion_cobranza,
	d.razon_social,
	c.codigo_cuenta_bancaria,
	e.entidad,
	c.codigo_forma_pago,
	f.descripcion
FROM tbl_pagos_pagados a
JOIN tbl_det_liquidacion_pagos b ON a.id_pago = b.id_pago AND a.orden_pago = b.orden_pago AND a.numero_cuota = b.numero_cuota
JOIN tbl_liquidacion_pagos c ON b.id_liquidacion_pago = c.id_liquidacion_pago
JOIN tbl_proveedores d ON a.codigo_proveedor = d.codigo
LEFT JOIN tbl_cuentas_bancarias e ON c.codigo_cuenta_bancaria = e.codigo
LEFT JOIN tbl_formas_pago f ON c.codigo_forma_pago = f.codigo
LEFT JOIN tbl_tipos_retenciones g ON b.codigo_retencion_1 = g.codigo
LEFT JOIN tbl_tipos_retenciones h ON b.codigo_retencion_2 = h.codigo
LEFT JOIN tbl_tipos_retenciones i ON b.codigo_retencion_3 = i.codigo
LEFT JOIN tbl_tipos_retenciones j ON b.codigo_retencion_4 = j.codigo
LEFT JOIN tbl_tipos_retenciones k ON b.codigo_retencion_5 = k.codigo
LEFT JOIN tbl_tipos_retenciones l ON b.codigo_retencion_iva = l.codigo
