CREATE VIEW "v_rep_taller_servicio" AS 
SELECT
	a.numero AS fc_numero,
	a.fecha_ingreso AS fc_fecha_ingreso,
	a.codigo_vendedor AS fc_codigo_vendedor,
	a.monto_neto AS fc_monto_neto,
	a.monto_descuento AS fc_monto_descuento,
	a.sub_total AS fc_sub_total,
	a.impuesto AS fc_impuesto,
	a.p_descuento AS fc_p_descuento,
	a.p_iva AS fc_p_iva,
	a.total AS fc_total,
	a.observaciones AS fc_observaciones,
	a.concepto AS fc_concepto,
	b.codigo_producto,
	b.descripcion AS detfac_descripcion,
	b.cantidad AS detfac_cantidad,
	b.precio AS detfac_precio,
	b.p_descuento AS detfac_descuento,
	b.monto_descuento AS detfac_monto_descuento,
	b.impuesto AS detfac_impuesto,
	c.prefijo_rif,
	c.rif,
	c.telefonos,
	c.razon_social,
	c.direccion_fiscal_1,
	c.direccion_fiscal_2,
	d.codigo_division,
	e.codigo_cliente AS cv_codigo_cliente,
	e.placa AS cv_placa,
	e.marca AS cv_marca,
	e.modelo AS cv_modelo,
	e.anno AS cv_anno,
	e.color AS cv_color,
	e.kilometraje AS cv_kilometraje,
	e.observaciones AS cv_observaciones,
	f.descripcion AS v_descripcion_vendedor
FROM tbl_taller_servicio a
LEFT JOIN tbl_taller_det_servicio b ON a.id_taller_servicio = b.id_taller_servicio
JOIN tbl_clientes c ON a.codigo_cliente = c.codigo
LEFT JOIN tbl_productos d ON b.codigo_producto = d.codigo
JOIN tbl_clientes_vehiculos e ON a.codigo_cliente = e.codigo_cliente AND a.placa_vehiculo_cliente = e.placa
JOIN tbl_vendedores f ON a.codigo_vendedor = f.codigo
