﻿-- View: public.v_condominio_detalle_gastos_ordinarios

-- DROP VIEW public.v_condominio_detalle_gastos_ordinarios;

CREATE OR REPLACE VIEW public.v_condominio_detalle_gastos_ordinarios AS
 SELECT a.codigo_conjunto_administracion,
    a.codigo_gasto,
    b.descripcion,
    a.monto
   FROM tbl_condominio_gastos_ordinarios a
     LEFT JOIN tbl_condominio_gastos b ON a.codigo_conjunto_administracion::text = b.codigo_conjunto_administracion::text AND a.codigo_gasto::text = b.codigo::text;

ALTER TABLE public.v_condominio_detalle_gastos_ordinarios
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_condominio_detalle_gastos_ordinarios TO PUBLIC;
GRANT ALL ON TABLE public.v_condominio_detalle_gastos_ordinarios TO postgres;
