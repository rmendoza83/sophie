﻿-- View: public.v_rep_nota_credito_proveedor

-- DROP VIEW public.v_rep_nota_credito_proveedor;

CREATE OR REPLACE VIEW public.v_rep_nota_credito_proveedor AS
 SELECT a.numero AS ncp_numero,
    a.fecha_ingreso AS ndp_fecha_ingreso,
    a.codigo_proveedor AS ndp_codigo_proveedor,
    a.monto_neto AS ndp_monto_neto,
    a.monto_descuento AS ndp_monto_descuento,
    a.sub_total AS ndp_sub_total,
    a.impuesto AS ndp_impuesto,
    a.p_descuento AS ndp_p_descuento,
    a.p_iva AS ndp_p_iva,
    a.tasa_dolar AS ndp_tasa_dolar,
    a.total AS ndp_total,
    a.observaciones AS ndp_observaciones,
    a.codigo_moneda AS ndp_codigo_moneda,
    a.concepto AS ndp_concepto,
    b.prefijo_rif,
    b.rif,
    b.telefonos,
    b.razon_social,
    b.direccion_fiscal_1,
    b.direccion_fiscal_2
   FROM tbl_nota_credito_proveedores a
     JOIN tbl_proveedores b ON a.codigo_proveedor::text = b.codigo::text;

ALTER TABLE public.v_rep_nota_credito_proveedor
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_rep_nota_credito_proveedor TO PUBLIC;
GRANT ALL ON TABLE public.v_rep_nota_credito_proveedor TO postgres;
