﻿-- View: public.v_rep_operaciones_bancarias

-- DROP VIEW public.v_rep_operaciones_bancarias;

CREATE OR REPLACE VIEW public.v_rep_operaciones_bancarias AS
 SELECT a.codigo_cuenta,
    a.codigo_tipo_operacion_bancaria,
    a.numero,
    a.fecha,
    a.fecha_contable,
        CASE
            WHEN NOT b.egreso THEN a.monto
            ELSE 0::numeric
        END AS monto_debe,
        CASE
            WHEN b.egreso THEN a.monto
            ELSE 0::numeric
        END AS monto_haber,
        CASE
            WHEN NOT b.egreso THEN a.monto
            ELSE a.monto * '-1'::integer::numeric
        END AS saldo,
    a.concepto,
    a.observaciones,
    a.diferido,
    a.conciliado,
    a.fecha_conciliacion,
    a.beneficiario,
    a.login_usuario,
    b.codigo AS titrba_codigo,
    b.descripcion AS titrba_descripcion,
    c.codigo AS cueban_codigo,
    c.entidad AS cueban_entidad,
    c.agencia AS cueban_agencia,
    c.numero AS cueban_numero,
    c.fecha_apertura AS cueban_fecha_apertura,
    c.telefonos AS cueban_telefonos,
    c.contacto AS cueban_contacto,
        CASE
            WHEN a.conciliado THEN a.monto
            ELSE 0::numeric
        END AS monto_conciliado
   FROM tbl_operaciones_bancarias a
     JOIN tbl_tipos_operaciones_bancarias b ON a.codigo_tipo_operacion_bancaria::text = b.codigo::text
     JOIN tbl_cuentas_bancarias c ON a.codigo_cuenta::text = c.codigo::text
  ORDER BY a.codigo_cuenta, a.codigo_tipo_operacion_bancaria, a.fecha;

ALTER TABLE public.v_rep_operaciones_bancarias
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_rep_operaciones_bancarias TO PUBLIC;
GRANT ALL ON TABLE public.v_rep_operaciones_bancarias TO postgres;
