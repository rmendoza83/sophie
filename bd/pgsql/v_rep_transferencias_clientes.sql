﻿-- View: public.v_rep_transferencias_clientes

-- DROP VIEW public.v_rep_transferencias_clientes;

CREATE OR REPLACE VIEW public.v_rep_transferencias_clientes AS
 SELECT a.numero AS tc_numero,
    a.codigo_moneda AS tc_codigo_moneda,
    a.codigo_cliente AS tc_codigo_cliente,
    a.codigo_zona AS tc_codigo_zona,
    a.fecha_ingreso AS tc_fecha_ingreso,
    a.fecha_contable AS tc_fecha_contable,
    a.concepto AS tc_concepto,
    a.observaciones AS tc_observaciones,
    a.total AS tc_total,
    b.numero_cuenta_cliente AS dtc_numero_cuenta_cliente,
    b.codigo_cuenta AS dtc_codigo_cuenta,
    b.codigo_tipo_operacion_bancaria AS dtc_codigo_tipo_operacion_bancaria,
    b.numero AS dtc_numero,
    b.fecha AS dtc_fecha,
    b.monto AS dtc_monto,
    c.prefijo_rif AS c_prefijo_rif,
    c.rif AS c_rif,
    c.telefonos AS c_telefonos,
    c.razon_social AS c_razon_social,
    c.direccion_fiscal_1 AS c_direccion_fiscal_1,
    c.direccion_fiscal_2 AS c_direccion_fiscal_2,
    d.entidad AS cb_entidad,
    d.tipo AS cb_tipo,
    d.numero AS cb_numero,
    ((lpad(''::text, 16, '*'::text) || "substring"(d.numero::text, '.{4}$'::text)))::character varying(20) AS cb_numero_codificado,
    e.entidad AS cbc_entidad,
    e.tipo AS cbc_tipo,
    e.email AS cbc_email
   FROM tbl_transferencias_clientes a
     LEFT JOIN tbl_det_transferencias_clientes b ON a.id_transferencia_cliente = b.id_transferencia_cliente AND a.codigo_cliente::text = b.codigo_cliente::text
     LEFT JOIN tbl_clientes c ON a.codigo_cliente::text = c.codigo::text
     LEFT JOIN tbl_cuentas_bancarias d ON b.codigo_cuenta::text = d.codigo::text
     LEFT JOIN tbl_clientes_cuentas_bancarias e ON b.codigo_cliente::text = e.codigo_cliente::text AND b.numero_cuenta_cliente::text = e.numero_cuenta::text
  ORDER BY a.id_transferencia_cliente;

ALTER TABLE public.v_rep_transferencias_clientes
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_rep_transferencias_clientes TO PUBLIC;
GRANT ALL ON TABLE public.v_rep_transferencias_clientes TO postgres;
