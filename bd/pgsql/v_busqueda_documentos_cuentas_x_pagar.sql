﻿-- View: public.v_busqueda_documentos_cuentas_x_pagar

-- DROP VIEW public.v_busqueda_documentos_cuentas_x_pagar;

CREATE OR REPLACE VIEW public.v_busqueda_documentos_cuentas_x_pagar AS
 SELECT a.desde,
    a.numero_documento,
    a.codigo_proveedor,
    b.rif,
    b.nit,
    b.razon_social,
    b.contacto,
    a.fecha_ingreso,
    a.numero_cuota,
    a.total_cuotas,
    a.monto_neto,
    a.impuesto,
    a.total,
    c.monto_neto AS monto_neto_documento,
    c.impuesto AS impuesto_documento,
    c.total AS total_documento,
    a.id_pago
   FROM tbl_cuentas_x_pagar a
     JOIN tbl_proveedores b ON a.codigo_proveedor::text = b.codigo::text
     JOIN tbl_facturacion_proveedores c ON a.id_pago = c.id_pago
  WHERE a.desde::text = 'FP'::text
UNION
 SELECT a.desde,
    a.numero_documento,
    a.codigo_proveedor,
    b.rif,
    b.nit,
    b.razon_social,
    b.contacto,
    a.fecha_ingreso,
    a.numero_cuota,
    a.total_cuotas,
    a.monto_neto,
    a.impuesto,
    a.total,
    c.monto_neto AS monto_neto_documento,
    c.impuesto AS impuesto_documento,
    c.total AS total_documento,
    a.id_pago
   FROM tbl_cuentas_x_pagar a
     JOIN tbl_proveedores b ON a.codigo_proveedor::text = b.codigo::text
     JOIN tbl_nota_credito_proveedores c ON a.id_pago = c.id_pago
  WHERE a.desde::text = 'NCP'::text
UNION
 SELECT a.desde,
    a.numero_documento,
    a.codigo_proveedor,
    b.rif,
    b.nit,
    b.razon_social,
    b.contacto,
    a.fecha_ingreso,
    a.numero_cuota,
    a.total_cuotas,
    a.monto_neto,
    a.impuesto,
    a.total,
    c.monto_neto AS monto_neto_documento,
    c.impuesto AS impuesto_documento,
    c.total AS total_documento,
    a.id_pago
   FROM tbl_cuentas_x_pagar a
     JOIN tbl_proveedores b ON a.codigo_proveedor::text = b.codigo::text
     JOIN tbl_nota_debito_proveedores c ON a.id_pago = c.id_pago
  WHERE a.desde::text = 'NDP'::text
UNION
 SELECT a.desde,
    a.numero_documento,
    a.codigo_proveedor,
    b.rif,
    b.nit,
    b.razon_social,
    b.contacto,
    a.fecha_ingreso,
    a.numero_cuota,
    a.total_cuotas,
    a.monto_neto,
    a.impuesto,
    a.total,
    c.monto_neto AS monto_neto_documento,
    0 AS impuesto_documento,
    c.monto_neto AS total_documento,
    a.id_pago
   FROM tbl_cuentas_x_pagar a
     JOIN tbl_proveedores b ON a.codigo_proveedor::text = b.codigo::text
     JOIN tbl_anticipo_proveedores c ON a.id_pago = c.id_pago
  WHERE a.desde::text = 'AP'::text;

ALTER TABLE public.v_busqueda_documentos_cuentas_x_pagar
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_busqueda_documentos_cuentas_x_pagar TO PUBLIC;
GRANT ALL ON TABLE public.v_busqueda_documentos_cuentas_x_pagar TO postgres;
