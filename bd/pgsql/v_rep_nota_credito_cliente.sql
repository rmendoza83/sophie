﻿-- View: public.v_rep_nota_credito_cliente

-- DROP VIEW public.v_rep_nota_credito_cliente;

CREATE OR REPLACE VIEW public.v_rep_nota_credito_cliente AS
 SELECT a.numero AS ncc_numero,
    a.numero_factura_asociada,
    a.fecha_ingreso AS ncc_fecha_ingreso,
    a.codigo_vendedor AS ncc_codigo_vendedor,
    a.monto_neto AS ncc_monto_neto,
    a.monto_descuento AS ncc_monto_descuento,
    a.sub_total AS ncc_sub_total,
    a.impuesto AS ncc_impuesto,
    a.p_descuento AS ncc_p_descuento,
    a.p_iva AS ncc_p_iva,
    a.tasa_dolar AS ncc_tasa_dolar,
    a.total AS ncc_total,
    a.observaciones AS ncc_observaciones,
    a.codigo_moneda AS ncc_codigo_moneda,
    a.concepto AS ncc_concepto,
    b.codigo_producto,
    b.descripcion AS detncc_descripcion,
    b.cantidad AS detncc_cantidad,
    b.precio AS detncc_precio,
    b.p_descuento AS detncc_descuento,
    b.monto_descuento AS detncc_monto_descuento,
    b.impuesto AS detncc_impuesto,
    c.prefijo_rif,
    c.rif,
    c.telefonos,
    c.razon_social,
    c.direccion_fiscal_1,
    c.direccion_fiscal_2,
    d.codigo_division,
    e.numero AS fc_numero,
    e.fecha_ingreso AS fc_fecha_ingreso,
    e.total AS fc_total
   FROM tbl_nota_credito_clientes a
     LEFT JOIN tbl_det_nota_credito_clientes b ON a.id_nota_credito_cliente = b.id_nota_credito_cliente
     JOIN tbl_clientes c ON a.codigo_cliente::text = c.codigo::text
     LEFT JOIN tbl_productos d ON b.codigo_producto::text = d.codigo::text
     LEFT JOIN tbl_facturacion_clientes e ON a.numero_factura_asociada::text = e.numero::text;

ALTER TABLE public.v_rep_nota_credito_cliente
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_rep_nota_credito_cliente TO PUBLIC;
GRANT ALL ON TABLE public.v_rep_nota_credito_cliente TO postgres;
