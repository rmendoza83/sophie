﻿-- View: public.v_busqueda_liquidacion_pagos

-- DROP VIEW public.v_busqueda_liquidacion_pagos;

CREATE OR REPLACE VIEW public.v_busqueda_liquidacion_pagos AS
 SELECT a.numero,
    a.codigo_proveedor,
    ((b.prefijo_rif::text || b.rif::text))::character varying(20) AS rif,
    b.nit,
    b.razon_social,
    b.contacto
   FROM tbl_liquidacion_pagos a
     LEFT JOIN tbl_proveedores b ON a.codigo_proveedor::text = b.codigo::text
  ORDER BY a.codigo_proveedor, a.numero;

ALTER TABLE public.v_busqueda_liquidacion_pagos
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_busqueda_liquidacion_pagos TO PUBLIC;
GRANT ALL ON TABLE public.v_busqueda_liquidacion_pagos TO postgres;
