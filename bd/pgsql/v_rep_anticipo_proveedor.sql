﻿-- View: public.v_rep_anticipo_proveedor

-- DROP VIEW public.v_rep_anticipo_proveedor;

CREATE OR REPLACE VIEW public.v_rep_anticipo_proveedor AS
 SELECT a.numero AS a_numero,
    a.fecha_ingreso AS a_fecha_ingreso,
    a.monto_neto AS a_monto_neto,
    a.concepto,
    a.observaciones,
    b.prefijo_rif,
    b.rif,
    b.telefonos,
    b.razon_social,
    b.direccion_fiscal_1,
    b.direccion_fiscal_2
   FROM tbl_anticipo_proveedores a
     JOIN tbl_proveedores b ON a.codigo_proveedor::text = b.codigo::text;

ALTER TABLE public.v_rep_anticipo_proveedor
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_rep_anticipo_proveedor TO PUBLIC;
GRANT ALL ON TABLE public.v_rep_anticipo_proveedor TO postgres;
