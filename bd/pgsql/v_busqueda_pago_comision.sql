﻿-- View: public.v_busqueda_pago_comision

-- DROP VIEW public.v_busqueda_pago_comision;

CREATE OR REPLACE VIEW public.v_busqueda_pago_comision AS
 SELECT a.numero,
    b.codigo AS codigo_vendedor,
    b.descripcion AS nombre_vendedor,
    b.activo AS estatus
   FROM tbl_pagos_comisiones a
     LEFT JOIN tbl_vendedores b ON a.codigo_vendedor::text = b.codigo::text
  ORDER BY a.numero;

ALTER TABLE public.v_busqueda_pago_comision
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_busqueda_pago_comision TO PUBLIC;
GRANT ALL ON TABLE public.v_busqueda_pago_comision TO postgres;
