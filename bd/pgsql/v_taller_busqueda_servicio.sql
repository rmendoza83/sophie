﻿-- View: public.v_taller_busqueda_servicio

-- DROP VIEW public.v_taller_busqueda_servicio;

CREATE OR REPLACE VIEW public.v_taller_busqueda_servicio AS
 SELECT a.numero,
    a.codigo_cliente,
    ((b.prefijo_rif::text || b.rif::text))::character varying(20) AS rif,
    b.nit,
    b.razon_social,
    b.contacto,
    c.placa AS placa_vehiculo,
    c.marca AS marca_vehiculo,
    c.modelo AS modelo_vehiculo,
    d.codigo AS codigo_mecanico,
    d.descripcion AS nombre_mecanico,
    a.id_facturacion_cliente
   FROM tbl_taller_servicio a
     LEFT JOIN tbl_clientes b ON a.codigo_cliente::text = b.codigo::text
     LEFT JOIN tbl_clientes_vehiculos c ON a.codigo_cliente::text = c.codigo_cliente::text AND a.placa_vehiculo_cliente::text = c.placa::text
     LEFT JOIN tbl_vendedores d ON a.codigo_vendedor::text = d.codigo::text
  ORDER BY a.id_taller_servicio;

ALTER TABLE public.v_taller_busqueda_servicio
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_taller_busqueda_servicio TO PUBLIC;
GRANT ALL ON TABLE public.v_taller_busqueda_servicio TO postgres;
