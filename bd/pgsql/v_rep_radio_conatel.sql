﻿-- View: public.v_rep_radio_conatel

-- DROP VIEW public.v_rep_radio_conatel;

CREATE OR REPLACE VIEW public.v_rep_radio_conatel AS
 SELECT 'FC'::text AS desde,
    a.numero,
    a.fecha_ingreso,
    a.codigo_cliente,
    a.sub_total,
    a.impuesto,
    a.total,
    ''::character varying AS numero_control,
    b.razon_social
   FROM tbl_facturacion_clientes a
     JOIN tbl_clientes b ON a.codigo_cliente::text = b.codigo::text
UNION
 SELECT 'NCC'::text AS desde,
    a.numero,
    a.fecha_ingreso,
    a.codigo_cliente,
    a.sub_total * '-1'::integer::numeric AS sub_total,
    a.impuesto,
    a.total * '-1'::integer::numeric AS total,
    ''::character varying AS numero_control,
    b.razon_social
   FROM tbl_nota_credito_clientes a
     JOIN tbl_clientes b ON a.codigo_cliente::text = b.codigo::text;

ALTER TABLE public.v_rep_radio_conatel
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_rep_radio_conatel TO PUBLIC;
GRANT ALL ON TABLE public.v_rep_radio_conatel TO postgres;
