﻿-- View: public.v_rep_libro_compra

-- DROP VIEW public.v_rep_libro_compra;

CREATE OR REPLACE VIEW public.v_rep_libro_compra AS
 SELECT 'FP'::character varying(6) AS desde,
    a.fecha_libros,
    a.fecha_ingreso,
    ((b.prefijo_rif::text || b.rif::text))::character varying(20) AS rif,
    b.razon_social,
    ''::character varying(20) AS numero_importacion,
    a.numero AS numero_factura,
    c.numero_control,
    ''::character varying(20) AS numero_nota_debito,
    ''::character varying(20) AS numero_nota_credito,
    '01-Reg'::character varying(20) AS tipo_transaccion,
    ''::character varying(20) AS factura_afectada,
    a.total AS total_factura,
    a.id_facturacion_proveedor AS id_documento,
        CASE
            WHEN a.impuesto < round(a.monto_neto * a.p_iva / 100::numeric, 4) THEN round(a.monto_neto - a.impuesto * 100::numeric / a.p_iva, 4)
            ELSE 0.0
        END AS exentas_no_gravadas,
        CASE
            WHEN a.impuesto < round(a.monto_neto * a.p_iva / 100::numeric, 4) THEN round(a.impuesto * 100::numeric / a.p_iva, 4)
            ELSE a.monto_neto
        END AS alicuota_general_monto_base_factura,
    a.p_iva AS alicuota_general_iva_factura,
    a.impuesto AS alicuota_general_impuesto_factura,
    a.monto_neto AS alicuota_reducida_monto_base_factura,
    a.p_iva AS alicuota_reducida_iva_factura,
    a.impuesto AS alicuota_reducida_impuesto_factura,
    COALESCE(d.monto_retencion, 0.0000)::numeric(18,4) AS iva_retenido_vendedor,
    COALESCE(a.impuesto - d.monto_retencion, 0.0000)::numeric(18,4) AS iva_pagado_no_retenido,
    e.numero_comprobante AS numero_comprobante_iva
   FROM tbl_facturacion_proveedores a
     JOIN tbl_proveedores b ON a.codigo_proveedor::text = b.codigo::text
     LEFT JOIN tbl_documentos_legales c ON a.numero::text = c.numero_documento::text AND c.desde::text = 'FP'::text
     LEFT JOIN tbl_det_retenciones_iva d ON a.numero::text = d.numero_documento::text AND d.desde = 'FP'::bpchar
     LEFT JOIN tbl_retenciones_iva e ON d.numero::text = e.numero::text
UNION
 SELECT 'NDP'::character varying(6) AS desde,
    a.fecha_libros,
    a.fecha_ingreso,
    ((b.prefijo_rif::text || b.rif::text))::character varying(20) AS rif,
    b.razon_social,
    ''::character varying(20) AS numero_importacion,
    ''::character varying(20) AS numero_factura,
    c.numero_control,
    a.numero AS numero_nota_debito,
    ''::character varying(20) AS numero_nota_credito,
    '01-Reg'::character varying(20) AS tipo_transaccion,
    ''::character varying(20) AS factura_afectada,
    a.total AS total_factura,
    a.id_nota_debito_proveedor AS id_documento,
        CASE
            WHEN a.impuesto < round(a.monto_neto * a.p_iva / 100::numeric, 4) THEN round(a.monto_neto - a.impuesto * 100::numeric / a.p_iva, 4)
            ELSE 0.0
        END AS exentas_no_gravadas,
        CASE
            WHEN a.impuesto < round(a.monto_neto * a.p_iva / 100::numeric, 4) THEN round(a.impuesto * 100::numeric / a.p_iva, 4)
            ELSE a.monto_neto
        END AS alicuota_general_monto_base_factura,
    a.p_iva AS alicuota_general_iva_factura,
    a.impuesto AS alicuota_general_impuesto_factura,
    a.monto_neto AS alicuota_reducida_monto_base_factura,
    a.p_iva AS alicuota_reducida_iva_factura,
    a.impuesto AS alicuota_reducida_impuesto_factura,
    COALESCE(d.monto_retencion, 0.0000)::numeric(18,4) AS iva_retenido_vendedor,
    COALESCE(a.impuesto - d.monto_retencion, 0.0000)::numeric(18,4) AS iva_pagado_no_retenido,
    e.numero_comprobante AS numero_comprobante_iva
   FROM tbl_nota_debito_proveedores a
     JOIN tbl_proveedores b ON a.codigo_proveedor::text = b.codigo::text
     LEFT JOIN tbl_documentos_legales c ON a.numero::text = c.numero_documento::text AND c.desde::text = 'NDP'::text
     LEFT JOIN tbl_det_retenciones_iva d ON a.numero::text = d.numero_documento::text AND d.desde = 'NDP'::bpchar
     LEFT JOIN tbl_retenciones_iva e ON d.numero::text = e.numero::text
UNION
 SELECT 'NCP'::character varying(6) AS desde,
    a.fecha_libros,
    a.fecha_ingreso,
    ((b.prefijo_rif::text || b.rif::text))::character varying(20) AS rif,
    b.razon_social,
    ''::character varying(20) AS numero_importacion,
    ''::character varying(20) AS numero_factura,
    c.numero_control,
    ''::character varying(20) AS numero_nota_debito,
    a.numero AS numero_nota_credito,
    '01-Reg'::character varying(20) AS tipo_transaccion,
    ''::character varying(20) AS factura_afectada,
    a.total AS total_factura,
    a.id_nota_credito_proveedor AS id_documento,
        CASE
            WHEN a.impuesto < round(a.monto_neto * a.p_iva / 100::numeric, 4) THEN round(a.monto_neto - a.impuesto * 100::numeric / a.p_iva, 4)
            ELSE 0.0
        END AS exentas_no_gravadas,
        CASE
            WHEN a.impuesto < round(a.monto_neto * a.p_iva / 100::numeric, 4) THEN round(a.impuesto * 100::numeric / a.p_iva, 4)
            ELSE a.monto_neto
        END AS alicuota_general_monto_base_factura,
    a.p_iva AS alicuota_general_iva_factura,
    a.impuesto AS alicuota_general_impuesto_factura,
    a.monto_neto AS alicuota_reducida_monto_base_factura,
    a.p_iva AS alicuota_reducida_iva_factura,
    a.impuesto AS alicuota_reducida_impuesto_factura,
    COALESCE(d.monto_retencion, 0.0000)::numeric(18,4) AS iva_retenido_vendedor,
    COALESCE(a.impuesto - d.monto_retencion, 0.0000)::numeric(18,4) AS iva_pagado_no_retenido,
    e.numero_comprobante AS numero_comprobante_iva
   FROM tbl_nota_credito_proveedores a
     JOIN tbl_proveedores b ON a.codigo_proveedor::text = b.codigo::text
     LEFT JOIN tbl_documentos_legales c ON a.numero::text = c.numero_documento::text AND c.desde::text = 'NCP'::text
     LEFT JOIN tbl_det_retenciones_iva d ON a.numero::text = d.numero_documento::text AND d.desde = 'NCP'::bpchar
     LEFT JOIN tbl_retenciones_iva e ON d.numero::text = e.numero::text;

ALTER TABLE public.v_rep_libro_compra
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_rep_libro_compra TO PUBLIC;
GRANT ALL ON TABLE public.v_rep_libro_compra TO postgres;
