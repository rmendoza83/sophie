﻿-- View: public.v_rep_productos_comprados

-- DROP VIEW public.v_rep_productos_comprados;

CREATE OR REPLACE VIEW public.v_rep_productos_comprados AS
 SELECT 'FP'::character varying(10) AS desde,
    a.numero,
    a.codigo_zona,
    d.descripcion AS descripcion_zona,
    a.fecha_ingreso,
    a.codigo_proveedor,
    a.monto_neto,
    a.monto_descuento,
    a.sub_total,
    a.impuesto,
    a.p_descuento,
    a.p_iva,
    a.tasa_dolar,
    a.codigo_esquema_pago,
    a.total AS fc_total,
    a.observaciones,
    a.codigo_moneda,
    a.concepto,
    b.codigo_producto,
    b.descripcion AS det_descripcion,
    b.cantidad AS det_cantidad,
    b.costo AS det_costo,
    b.total,
    b.p_descuento AS det_p_descuento,
    b.monto_descuento AS det_monto_descuento,
    b.impuesto AS det_impuesto,
    c.costo_actual AS costo,
    c.codigo_division,
    e.descripcion AS pro_descripcion_division,
    c.codigo_linea,
    f.descripcion AS pro_descripcion_linea,
    c.codigo_familia,
    g.descripcion AS pro_descripcion_familia,
    c.codigo_clase,
    h.descripcion AS pro_descripcion_clase,
    c.codigo_manufactura,
    i.descripcion AS pro_descripcion_manufactura,
    a.codigo_estacion,
    j.descripcion AS descripcion_estacion,
    k.razon_social AS cli_nombre_proveedor,
    l.codigo AS und_codigo_unidad,
    l.descripcion AS und_descripcion_unidad
   FROM tbl_facturacion_proveedores a
     LEFT JOIN tbl_det_facturacion_proveedores b ON a.id_facturacion_proveedor = b.id_facturacion_proveedor
     LEFT JOIN tbl_productos c ON b.codigo_producto::text = c.codigo::text
     JOIN tbl_zonas d ON a.codigo_zona::text = d.codigo::text
     JOIN tbl_divisiones e ON c.codigo_division::text = e.codigo::text
     JOIN tbl_lineas f ON c.codigo_linea::text = f.codigo::text
     JOIN tbl_familias g ON c.codigo_familia::text = g.codigo::text
     JOIN tbl_clases h ON c.codigo_clase::text = h.codigo::text
     JOIN tbl_manufacturas i ON c.codigo_manufactura::text = i.codigo::text
     JOIN tbl_estaciones j ON a.codigo_estacion::text = j.codigo::text
     JOIN tbl_proveedores k ON a.codigo_proveedor::text = k.codigo::text
     JOIN tbl_unidades l ON c.codigo_unidad::text = l.codigo::text
UNION
 SELECT 'NCP'::character varying(10) AS desde,
    a.numero,
    a.codigo_zona,
    d.descripcion AS descripcion_zona,
    a.fecha_ingreso,
    a.codigo_proveedor,
    a.monto_neto,
    a.monto_descuento,
    a.sub_total,
    a.impuesto,
    a.p_descuento,
    a.p_iva,
    a.tasa_dolar,
    ''::character varying(10) AS codigo_esquema_pago,
    a.total AS fc_total,
    a.observaciones,
    a.codigo_moneda,
    a.concepto,
    b.codigo_producto,
    b.descripcion AS det_descripcion,
    b.cantidad * '-1'::integer::numeric AS det_cantidad,
    0 AS det_costo,
    b.total * '-1'::integer::numeric AS total,
    b.p_descuento AS det_p_descuento,
    b.monto_descuento AS det_monto_descuento,
    b.impuesto AS det_impuesto,
    c.costo_actual AS costo,
    c.codigo_division,
    e.descripcion AS pro_descripcion_division,
    c.codigo_linea,
    f.descripcion AS pro_descripcion_linea,
    c.codigo_familia,
    g.descripcion AS pro_descripcion_familia,
    c.codigo_clase,
    h.descripcion AS pro_descripcion_clase,
    c.codigo_manufactura,
    i.descripcion AS pro_descripcion_manufactura,
    a.codigo_estacion,
    j.descripcion AS descripcion_estacion,
    k.razon_social AS cli_nombre_proveedor,
    l.codigo AS und_codigo_unidad,
    l.descripcion AS und_descripcion_unidad
   FROM tbl_nota_credito_proveedores a
     LEFT JOIN tbl_det_nota_credito_proveedores b ON a.id_nota_credito_proveedor = b.id_nota_credito_proveedor
     LEFT JOIN tbl_productos c ON b.codigo_producto::text = c.codigo::text
     JOIN tbl_zonas d ON a.codigo_zona::text = d.codigo::text
     JOIN tbl_divisiones e ON c.codigo_division::text = e.codigo::text
     JOIN tbl_lineas f ON c.codigo_linea::text = f.codigo::text
     JOIN tbl_familias g ON c.codigo_familia::text = g.codigo::text
     JOIN tbl_clases h ON c.codigo_clase::text = h.codigo::text
     JOIN tbl_manufacturas i ON c.codigo_manufactura::text = i.codigo::text
     JOIN tbl_estaciones j ON a.codigo_estacion::text = j.codigo::text
     JOIN tbl_proveedores k ON a.codigo_proveedor::text = k.codigo::text
     JOIN tbl_unidades l ON c.codigo_unidad::text = l.codigo::text;

ALTER TABLE public.v_rep_productos_comprados
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_rep_productos_comprados TO PUBLIC;
GRANT ALL ON TABLE public.v_rep_productos_comprados TO postgres;
