﻿-- View: public.v_rep_estado_cuenta_proveedor_analisis

-- DROP VIEW public.v_rep_estado_cuenta_proveedor_analisis;

CREATE OR REPLACE VIEW public.v_rep_estado_cuenta_proveedor_analisis AS
 SELECT a."from",
    a.cxp_numero_documento,
    a.cxp_desde,
    a.cxp_fecha_ingreso,
    a.cxp_fecha_venc_ingreso,
    a.cxp_fecha_recepcion,
    a.cxp_codigo_proveedor,
    a.cxp_codigo_zona,
    a.cxp_monto_neto,
    a.cxp_impuesto,
    a.cxp_total,
    a.cxp_p_iva,
    a.cxp_numero_cuota,
    a.cxp_total_cuotas,
    a.cxp_orden_pago,
        CASE
            WHEN a.cxp_desde::text = 'FP'::text THEN 'Factura'::text
            WHEN a.cxp_desde::text = 'AP'::text THEN 'Anticipo'::text
            WHEN a.cxp_desde::text = 'NDP'::text THEN 'Nota Debito'::text
            WHEN a.cxp_desde::text = 'NCP'::text THEN 'Nota Credito'::text
            ELSE NULL::text
        END AS concepto,
        CASE
            WHEN 'now'::text::date <= a.cxp_fecha_venc_ingreso THEN
            CASE
                WHEN a.cxp_desde::text = 'AP'::text OR a.cxp_desde::text = 'NCP'::text THEN a.cxp_total * '-1'::integer::numeric
                ELSE a.cxp_total
            END
            ELSE 0::numeric
        END AS monto_no_vencido,
        CASE
            WHEN 'now'::text::date > a.cxp_fecha_venc_ingreso AND ('now'::text::date - a.cxp_fecha_venc_ingreso) > 0 AND ('now'::text::date - a.cxp_fecha_venc_ingreso) <= 30 THEN
            CASE
                WHEN a.cxp_desde::text = 'AP'::text OR a.cxp_desde::text = 'NCP'::text THEN a.cxp_total * '-1'::integer::numeric
                ELSE a.cxp_total
            END
            ELSE 0::numeric
        END AS monto_vencido_30,
        CASE
            WHEN 'now'::text::date > a.cxp_fecha_venc_ingreso AND ('now'::text::date - a.cxp_fecha_venc_ingreso) > 30 AND ('now'::text::date - a.cxp_fecha_venc_ingreso) <= 60 THEN
            CASE
                WHEN a.cxp_desde::text = 'AP'::text OR a.cxp_desde::text = 'NCP'::text THEN a.cxp_total * '-1'::integer::numeric
                ELSE a.cxp_total
            END
            ELSE 0::numeric
        END AS monto_vencido_60,
        CASE
            WHEN 'now'::text::date > a.cxp_fecha_venc_ingreso AND ('now'::text::date - a.cxp_fecha_venc_ingreso) > 60 AND ('now'::text::date - a.cxp_fecha_venc_ingreso) <= 90 THEN
            CASE
                WHEN a.cxp_desde::text = 'AP'::text OR a.cxp_desde::text = 'NCP'::text THEN a.cxp_total * '-1'::integer::numeric
                ELSE a.cxp_total
            END
            ELSE 0::numeric
        END AS monto_vencido_90,
        CASE
            WHEN 'now'::text::date > a.cxp_fecha_venc_ingreso AND ('now'::text::date - a.cxp_fecha_venc_ingreso) > 90 THEN
            CASE
                WHEN a.cxp_desde::text = 'AP'::text OR a.cxp_desde::text = 'NCP'::text THEN a.cxp_total * '-1'::integer::numeric
                ELSE a.cxp_total
            END
            ELSE 0::numeric
        END AS monto_vencido_mas90,
    c.prefijo_rif,
    c.rif,
    c.telefonos,
    c.razon_social,
    c.direccion_fiscal_1,
    c.direccion_fiscal_2
   FROM ( SELECT 'CXP'::text AS "from",
            a_1.numero_documento AS cxp_numero_documento,
            a_1.desde AS cxp_desde,
            a_1.fecha_ingreso AS cxp_fecha_ingreso,
            a_1.fecha_venc_ingreso AS cxp_fecha_venc_ingreso,
            a_1.fecha_recepcion AS cxp_fecha_recepcion,
            a_1.codigo_proveedor AS cxp_codigo_proveedor,
            a_1.codigo_zona AS cxp_codigo_zona,
            a_1.monto_neto AS cxp_monto_neto,
            a_1.impuesto AS cxp_impuesto,
            a_1.total AS cxp_total,
            a_1.p_iva AS cxp_p_iva,
            a_1.numero_cuota AS cxp_numero_cuota,
            a_1.total_cuotas AS cxp_total_cuotas,
            a_1.orden_pago AS cxp_orden_pago
           FROM tbl_cuentas_x_pagar a_1
        UNION
         SELECT 'PAGADA'::text AS "from",
            a_1.numero_documento AS cxp_numero_documento,
            a_1.desde AS cxp_desde,
            a_1.fecha_ingreso AS cxp_fecha_ingreso,
            a_1.fecha_venc_ingreso AS cxp_fecha_venc_ingreso,
            a_1.fecha_recepcion AS cxp_fecha_recepcion,
            a_1.codigo_proveedor AS cxp_codigo_proveedor,
            a_1.codigo_zona AS cxp_codigo_zona,
            a_1.monto_neto AS cxp_monto_neto,
            a_1.impuesto AS cxp_impuesto,
            a_1.total AS cxp_total,
            a_1.p_iva AS cxp_p_iva,
            a_1.numero_cuota AS cxp_numero_cuota,
            a_1.total_cuotas AS cxp_total_cuotas,
            a_1.orden_pago AS cxp_orden_pago
           FROM tbl_pagos_pagados a_1) a
     JOIN tbl_proveedores c ON a.cxp_codigo_proveedor::text = c.codigo::text;

ALTER TABLE public.v_rep_estado_cuenta_proveedor_analisis
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_rep_estado_cuenta_proveedor_analisis TO PUBLIC;
GRANT ALL ON TABLE public.v_rep_estado_cuenta_proveedor_analisis TO postgres;
