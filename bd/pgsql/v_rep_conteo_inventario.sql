﻿-- View: public.v_rep_conteo_inventario

-- DROP VIEW public.v_rep_conteo_inventario;

CREATE OR REPLACE VIEW public.v_rep_conteo_inventario AS
 SELECT a.numero,
    a.fecha AS ci_fecha_conteo,
    a.cantidad_conteo AS ci_cantidad_conteo,
    a.codigo_zona,
    a.codigo_estacion,
    b.cantidad AS dci_cantidad,
    b.descripcion AS detfac_descripcion,
    b.codigo_producto AS dci_codigo_producto,
    b.referencia AS dci_referencia_producto,
    b.descripcion AS dci_descripcion_producto,
    c.codigo_division,
    c.codigo_linea,
    c.codigo_familia,
    c.codigo_clase,
    c.codigo_manufactura,
    c.codigo_arancel
   FROM tbl_conteos_inventario a
     LEFT JOIN tbl_det_conteos_inventario b ON a.id_conteo_inventario = b.id_conteo_inventario
     LEFT JOIN tbl_productos c ON b.codigo_producto::text = c.codigo::text;

ALTER TABLE public.v_rep_conteo_inventario
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_rep_conteo_inventario TO PUBLIC;
GRANT ALL ON TABLE public.v_rep_conteo_inventario TO postgres;
