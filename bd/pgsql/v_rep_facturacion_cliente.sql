﻿-- View: public.v_rep_facturacion_cliente

-- DROP VIEW public.v_rep_facturacion_cliente;

CREATE OR REPLACE VIEW public.v_rep_facturacion_cliente AS
 SELECT a.numero AS fc_numero,
    a.fecha_ingreso AS fc_fecha_ingreso,
    a.codigo_vendedor AS fc_codigo_vendedor,
    a.monto_neto AS fc_monto_neto,
    a.monto_descuento AS fc_monto_descuento,
    a.sub_total AS fc_sub_total,
    a.impuesto AS fc_impuesto,
    a.p_descuento AS fc_p_descuento,
    a.p_iva AS fc_p_iva,
    a.tasa_dolar AS fc_tasa_dolar,
    a.codigo_esquema_pago AS fc_codigo_esquema_pago,
    a.total AS fc_total,
    a.observaciones AS fc_observaciones,
    a.codigo_moneda AS fc_codigo_moneda,
    a.concepto AS fc_concepto,
    b.codigo_producto AS dfc_codigo_producto,
    b.descripcion AS dfc_descripcion,
    b.cantidad AS dfc_cantidad,
    b.precio AS dfc_precio,
    b.p_descuento AS dfc_descuento,
    b.monto_descuento AS dfc_monto_descuento,
    b.impuesto AS dfc_impuesto,
    c.prefijo_rif AS cli_prefijo_rif,
    c.rif AS cli_rif,
    c.telefonos AS cli_telefonos,
    c.razon_social AS cli_razon_social,
    c.direccion_fiscal_1 AS cli_direccion_fiscal_1,
    c.direccion_fiscal_2 AS cli_direccion_fiscal_2,
    d.codigo_division AS pro_codigo_division,
    e.descripcion AS v_descripcion
   FROM tbl_facturacion_clientes a
     LEFT JOIN tbl_det_facturacion_clientes b ON a.id_facturacion_cliente = b.id_facturacion_cliente
     JOIN tbl_clientes c ON a.codigo_cliente::text = c.codigo::text
     LEFT JOIN tbl_productos d ON b.codigo_producto::text = d.codigo::text
     LEFT JOIN tbl_vendedores e ON a.codigo_vendedor::text = e.codigo::text;

ALTER TABLE public.v_rep_facturacion_cliente
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_rep_facturacion_cliente TO PUBLIC;
GRANT ALL ON TABLE public.v_rep_facturacion_cliente TO postgres;
