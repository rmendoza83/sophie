﻿-- View: public.v_rep_pago_comision

-- DROP VIEW public.v_rep_pago_comision;

CREATE OR REPLACE VIEW public.v_rep_pago_comision AS
 SELECT a.numero,
    a.codigo_vendedor,
    a.codigo_zona,
    a.codigo_moneda,
    a.fecha_ingreso,
    a.fecha_pago,
    a.fecha_inicial,
    a.fecha_final,
    a.total,
    b.codigo_tipo_comision AS dpc_codigo_tipo_comision,
    b.desde AS dpc_desde,
    b.numero_documento AS dpc_numero_documento,
    b.base_calculo AS dpc_base_calculo,
    b.porcentaje_comision AS dpc_porcentaje_comision,
    b.cantidad AS dpc_cantidad,
    b.monto_comision AS dpc_monto_comision,
    c.descripcion AS v_descripcion,
    d.descripcion AS tc_descripcion,
    e.descripcion AS tz_descripcion,
    g.fecha_inicial AS periodo_inicial,
    g.fecha_final AS periodo_final
   FROM tbl_pagos_comisiones a
     JOIN tbl_det_pagos_comisiones b ON a.numero::text = b.numero::text
     JOIN tbl_vendedores c ON a.codigo_vendedor::text = c.codigo::text
     JOIN tbl_tipos_comisiones d ON b.codigo_tipo_comision::text = d.codigo::text
     JOIN tbl_zonas e ON a.codigo_zona::text = e.codigo::text
     JOIN tbl_facturacion_clientes f ON b.numero_documento::text = f.numero::text
     JOIN tbl_radio_contratos_facturas_generadas g ON f.id_facturacion_cliente = g.id_facturacion_cliente AND f.numero::text = g.numero_documento::text;

ALTER TABLE public.v_rep_pago_comision
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_rep_pago_comision TO PUBLIC;
GRANT ALL ON TABLE public.v_rep_pago_comision TO postgres;
