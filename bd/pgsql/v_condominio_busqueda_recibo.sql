﻿-- View: public.v_condominio_busqueda_recibo

-- DROP VIEW public.v_condominio_busqueda_recibo;

CREATE OR REPLACE VIEW public.v_condominio_busqueda_recibo AS
 SELECT ((((b.codigo::text || '-'::character varying::text) || lpad(a.periodo_a::character varying(4)::text, 4, '0'::character varying::text)) || lpad(a.periodo_m::character varying(2)::text, 2, '0'::character varying::text)))::character varying(20) AS codigo,
    b.codigo AS codigo_conjunto_administracion,
    b.descripcion,
    a.periodo_a,
    a.periodo_m,
    a.fecha_ingreso,
    a.fecha_vencimiento
   FROM tbl_condominio_recibos a
     LEFT JOIN tbl_condominio_conjuntos_administracion b ON a.codigo_conjunto_administracion::text = b.codigo::text
  ORDER BY b.codigo, a.periodo_a, a.periodo_m;

ALTER TABLE public.v_condominio_busqueda_recibo
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_condominio_busqueda_recibo TO PUBLIC;
GRANT ALL ON TABLE public.v_condominio_busqueda_recibo TO postgres;
