﻿-- View: public.v_rep_radio_pre_factura

-- DROP VIEW public.v_rep_radio_pre_factura;

CREATE OR REPLACE VIEW public.v_rep_radio_pre_factura AS
 SELECT a.item,
    a.total_items,
    a.fecha_ingreso AS radio_contratos_facturas_pendientes_fecha_ingreso,
    a.fecha_inicial AS radio_contratos_facturas_pendientes_fecha_inicial,
    a.fecha_final AS radio_contratos_facturas_pendientes_fecha_final,
    a.total AS radio_contratos_facturas_pendientes_total,
    b.numero AS radio_contratos_numero_contrato,
    b.codigo_cliente AS radio_contratos_codigo_cliente,
    b.total AS radio_contratos_total_contrato,
    b.numero_orden,
    c.razon_social
   FROM tbl_radio_contratos_facturas_pendientes a
     LEFT JOIN tbl_radio_contratos b ON a.id_radio_contrato = b.id_radio_contrato
     LEFT JOIN tbl_clientes c ON b.codigo_cliente::text = c.codigo::text;

ALTER TABLE public.v_rep_radio_pre_factura
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_rep_radio_pre_factura TO PUBLIC;
GRANT ALL ON TABLE public.v_rep_radio_pre_factura TO postgres;
