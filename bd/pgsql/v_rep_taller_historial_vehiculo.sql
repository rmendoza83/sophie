﻿-- View: public.v_rep_taller_historial_vehiculo

-- DROP VIEW public.v_rep_taller_historial_vehiculo;

CREATE OR REPLACE VIEW public.v_rep_taller_historial_vehiculo AS
 SELECT a.numero,
    a.codigo_cliente,
    a.placa_vehiculo_cliente,
    a.codigo_vendedor AS ts_codigo_mecanico,
    c.descripcion AS v_nombre_mecanico,
    a.codigo_zona,
    a.fecha_ingreso,
    a.kilometraje,
    a.concepto,
    a.observaciones,
    a.forma_libre,
    b.codigo AS c_codigo_cliente,
    b.prefijo_rif AS c_prefijo_rif,
    b.rif AS c_rif,
    b.razon_social AS c_nombre_cliente,
    b.telefonos AS c_telefono_cliente,
    d.placa AS cv_placa_vehiculo,
    d.marca AS cv_marca_vehiculo,
    d.modelo AS cv_modelo_vehiculo,
    d.anno AS anno_vehiculo,
    d.color AS color_vehiculo,
    d.kilometraje AS cv_kilometraje_vehiculo,
    d.observaciones AS cv_observaciones_vehiculo,
    e.codigo_producto AS tds_codigo_producto,
    e.descripcion AS tds_descripcion,
    e.cantidad AS tds_cantidad,
    e.precio AS tds_precio,
    e.total AS tds_total,
    e.observaciones AS tds_observaciones
   FROM tbl_taller_servicio a
     LEFT JOIN tbl_clientes b ON a.codigo_cliente::text = b.codigo::text
     LEFT JOIN tbl_vendedores c ON a.codigo_vendedor::text = c.codigo::text
     LEFT JOIN tbl_clientes_vehiculos d ON a.codigo_cliente::text = d.codigo_cliente::text AND a.placa_vehiculo_cliente::text = d.placa::text
     LEFT JOIN tbl_taller_det_servicio e ON a.id_taller_servicio = e.id_taller_servicio;

ALTER TABLE public.v_rep_taller_historial_vehiculo
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_rep_taller_historial_vehiculo TO PUBLIC;
GRANT ALL ON TABLE public.v_rep_taller_historial_vehiculo TO postgres;
