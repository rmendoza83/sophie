﻿-- View: public.v_existencias_resumido

-- DROP VIEW public.v_existencias_resumido;

CREATE OR REPLACE VIEW public.v_existencias_resumido AS
 SELECT a.codigo AS codigo_producto,
    a.referencia,
    a.descripcion,
    b.codigo AS codigo_estacion,
    b.descripcion AS descripcion_estacion,
    COALESCE(sum(c.cantidad), 0::numeric) AS cantidad_entradas,
    COALESCE(sum(d.cantidad), 0::numeric) AS cantidad_salidas
   FROM tbl_productos a
     CROSS JOIN tbl_estaciones b
     LEFT JOIN tbl_det_movimientos_inventario c ON a.codigo::text = c.codigo_producto::text AND b.codigo::text = c.estacion_entrada::text
     LEFT JOIN tbl_det_movimientos_inventario d ON a.codigo::text = d.codigo_producto::text AND b.codigo::text = d.estacion_salida::text
  GROUP BY a.codigo, a.referencia, a.descripcion, b.codigo, b.descripcion;

ALTER TABLE public.v_existencias_resumido
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_existencias_resumido TO PUBLIC;
GRANT ALL ON TABLE public.v_existencias_resumido TO postgres;
