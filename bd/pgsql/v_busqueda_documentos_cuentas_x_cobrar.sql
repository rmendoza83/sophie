﻿-- View: public.v_busqueda_documentos_cuentas_x_cobrar

-- DROP VIEW public.v_busqueda_documentos_cuentas_x_cobrar;

CREATE OR REPLACE VIEW public.v_busqueda_documentos_cuentas_x_cobrar AS
 SELECT a.desde,
    a.numero_documento,
    a.codigo_cliente,
    b.rif,
    b.nit,
    b.razon_social,
    b.contacto,
    a.fecha_ingreso,
    a.numero_cuota,
    a.total_cuotas,
    a.monto_neto,
    a.impuesto,
    a.total,
    c.monto_neto AS monto_neto_documento,
    c.impuesto AS impuesto_documento,
    c.total AS total_documento
   FROM tbl_cuentas_x_cobrar a
     JOIN tbl_clientes b ON a.codigo_cliente::text = b.codigo::text
     JOIN tbl_facturacion_clientes c ON a.id_cobranza = c.id_cobranza
  WHERE a.desde::text = 'FC'::text
UNION
 SELECT a.desde,
    a.numero_documento,
    a.codigo_cliente,
    b.rif,
    b.nit,
    b.razon_social,
    b.contacto,
    a.fecha_ingreso,
    a.numero_cuota,
    a.total_cuotas,
    a.monto_neto,
    a.impuesto,
    a.total,
    c.monto_neto AS monto_neto_documento,
    c.impuesto AS impuesto_documento,
    c.total AS total_documento
   FROM tbl_cuentas_x_cobrar a
     JOIN tbl_clientes b ON a.codigo_cliente::text = b.codigo::text
     JOIN tbl_nota_credito_clientes c ON a.id_cobranza = c.id_cobranza
  WHERE a.desde::text = 'NCC'::text
UNION
 SELECT a.desde,
    a.numero_documento,
    a.codigo_cliente,
    b.rif,
    b.nit,
    b.razon_social,
    b.contacto,
    a.fecha_ingreso,
    a.numero_cuota,
    a.total_cuotas,
    a.monto_neto,
    a.impuesto,
    a.total,
    c.monto_neto AS monto_neto_documento,
    c.impuesto AS impuesto_documento,
    c.total AS total_documento
   FROM tbl_cuentas_x_cobrar a
     JOIN tbl_clientes b ON a.codigo_cliente::text = b.codigo::text
     JOIN tbl_nota_debito_clientes c ON a.id_cobranza = c.id_cobranza
  WHERE a.desde::text = 'NDC'::text
UNION
 SELECT a.desde,
    a.numero_documento,
    a.codigo_cliente,
    b.rif,
    b.nit,
    b.razon_social,
    b.contacto,
    a.fecha_ingreso,
    a.numero_cuota,
    a.total_cuotas,
    a.monto_neto,
    a.impuesto,
    a.total,
    c.monto_neto AS monto_neto_documento,
    0 AS impuesto_documento,
    c.monto_neto AS total_documento
   FROM tbl_cuentas_x_cobrar a
     JOIN tbl_clientes b ON a.codigo_cliente::text = b.codigo::text
     JOIN tbl_anticipo_clientes c ON a.id_cobranza = c.id_cobranza
  WHERE a.desde::text = 'AC'::text;

ALTER TABLE public.v_busqueda_documentos_cuentas_x_cobrar
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_busqueda_documentos_cuentas_x_cobrar TO PUBLIC;
GRANT ALL ON TABLE public.v_busqueda_documentos_cuentas_x_cobrar TO postgres;
