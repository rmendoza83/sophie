﻿-- View: public.v_busqueda_conteos_inventario

-- DROP VIEW public.v_busqueda_conteos_inventario;

CREATE OR REPLACE VIEW public.v_busqueda_conteos_inventario AS
 SELECT a.numero,
    a.codigo_zona,
    b.descripcion AS descripcion_zona,
    a.codigo_estacion,
    c.descripcion AS descripcion_estacion,
    a.observaciones::character varying AS observaciones
   FROM tbl_conteos_inventario a
     JOIN tbl_zonas b ON a.codigo_zona::text = b.codigo::text
     JOIN tbl_estaciones c ON a.codigo_estacion::text = c.codigo::text;

ALTER TABLE public.v_busqueda_conteos_inventario
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_busqueda_conteos_inventario TO PUBLIC;
GRANT ALL ON TABLE public.v_busqueda_conteos_inventario TO postgres;
