﻿-- View: public.v_busqueda_anticipo_proveedores

-- DROP VIEW public.v_busqueda_anticipo_proveedores;

CREATE OR REPLACE VIEW public.v_busqueda_anticipo_proveedores AS
 SELECT a.numero,
    a.codigo_proveedor,
    ((b.prefijo_rif::text || b.rif::text))::character varying(20) AS rif,
    b.nit,
    b.razon_social,
    b.contacto
   FROM tbl_anticipo_proveedores a
     LEFT JOIN tbl_proveedores b ON a.codigo_proveedor::text = b.codigo::text
  ORDER BY a.id_anticipo_proveedor;

ALTER TABLE public.v_busqueda_anticipo_proveedores
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_busqueda_anticipo_proveedores TO PUBLIC;
GRANT ALL ON TABLE public.v_busqueda_anticipo_proveedores TO postgres;
