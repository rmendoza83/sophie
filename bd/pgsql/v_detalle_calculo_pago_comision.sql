﻿-- View: public.v_detalle_calculo_pago_comision

-- DROP VIEW public.v_detalle_calculo_pago_comision;

CREATE OR REPLACE VIEW public.v_detalle_calculo_pago_comision AS
 SELECT 'FC'::character varying(10) AS desde,
    a.numero,
    a.codigo_vendedor,
    a.fecha_ingreso,
    COALESCE(b.codigo_producto, 'N/A'::character varying(20)) AS codigo_producto,
    COALESCE(b.referencia, 'N/A'::character varying(20)) AS referencia,
    COALESCE(b.descripcion, 'N/A'::character varying(80)) AS descripcion,
    COALESCE(b.cantidad, 1::numeric) AS cantidad,
    COALESCE(b.precio, a.monto_neto) AS precio,
    COALESCE(b.p_descuento, a.p_descuento) AS p_descuento,
    COALESCE(b.monto_descuento, a.monto_descuento) AS monto_descuento,
    COALESCE(b.impuesto, a.impuesto) AS impuesto,
    COALESCE(b.p_iva, a.p_iva) AS p_iva,
    COALESCE(b.total, a.total) AS total,
    a.id_facturacion_cliente,
    c.codigo_division,
    c.codigo_linea,
    c.codigo_familia,
    c.codigo_clase,
    c.codigo_manufactura
   FROM tbl_facturacion_clientes a
     LEFT JOIN tbl_det_facturacion_clientes b ON a.id_facturacion_cliente = b.id_facturacion_cliente
     LEFT JOIN tbl_productos c ON b.codigo_producto::text = c.codigo::text
  WHERE NOT (a.numero::text IN ( SELECT tbl_det_pagos_comisiones.numero_documento
           FROM tbl_det_pagos_comisiones
          WHERE tbl_det_pagos_comisiones.desde::text = 'FC'::text))
UNION
 SELECT 'NCC'::character varying(10) AS desde,
    a.numero,
    a.codigo_vendedor,
    a.fecha_ingreso,
    COALESCE(b.codigo_producto, 'N/A'::character varying(20)) AS codigo_producto,
    COALESCE(b.referencia, 'N/A'::character varying(20)) AS referencia,
    COALESCE(b.descripcion, 'N/A'::character varying(80)) AS descripcion,
    COALESCE(b.cantidad, 1::numeric) AS cantidad,
    COALESCE(b.precio, a.monto_neto) AS precio,
    COALESCE(b.p_descuento, a.p_descuento) AS p_descuento,
    COALESCE(b.monto_descuento, a.monto_descuento) AS monto_descuento,
    COALESCE(b.impuesto, a.impuesto) AS impuesto,
    COALESCE(b.p_iva, a.p_iva) AS p_iva,
    COALESCE(b.total, a.total) AS total,
    a.id_nota_credito_cliente AS id_facturacion_cliente,
    c.codigo_division,
    c.codigo_linea,
    c.codigo_familia,
    c.codigo_clase,
    c.codigo_manufactura
   FROM tbl_nota_credito_clientes a
     LEFT JOIN tbl_det_nota_credito_clientes b ON a.id_nota_credito_cliente = b.id_nota_credito_cliente
     LEFT JOIN tbl_productos c ON b.codigo_producto::text = c.codigo::text
  WHERE NOT (a.numero::text IN ( SELECT tbl_det_pagos_comisiones.numero_documento
           FROM tbl_det_pagos_comisiones
          WHERE tbl_det_pagos_comisiones.desde::text = 'NCC'::text));

ALTER TABLE public.v_detalle_calculo_pago_comision
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_detalle_calculo_pago_comision TO PUBLIC;
GRANT ALL ON TABLE public.v_detalle_calculo_pago_comision TO postgres;
