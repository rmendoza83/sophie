﻿-- View: public.v_rep_estado_cuenta_cliente_detallado_resumido

-- DROP VIEW public.v_rep_estado_cuenta_cliente_detallado_resumido;

CREATE OR REPLACE VIEW public.v_rep_estado_cuenta_cliente_detallado_resumido AS
 SELECT a."from",
    a.cxc_numero_documento,
    a.cxc_desde,
    a.cxc_fecha_ingreso,
    a.cxc_fecha_venc_ingreso,
    a.cxc_fecha_recepcion,
    a.cxc_codigo_cliente,
    a.cxc_codigo_vendedor,
    a.cxc_codigo_zona,
    a.cxc_monto_neto,
    a.cxc_impuesto,
    a.cxc_total,
    a.cxc_p_iva,
    a.cxc_numero_cuota,
    a.cxc_total_cuotas,
    a.cxc_orden_cobranza,
        CASE
            WHEN a.cxc_desde::text = 'FC'::text THEN 'Factura'::text
            WHEN a.cxc_desde::text = 'AC'::text THEN 'Anticipo'::text
            WHEN a.cxc_desde::text = 'NDC'::text THEN 'Nota Debito'::text
            WHEN a.cxc_desde::text = 'NCC'::text THEN 'Nota Credito'::text
            ELSE NULL::text
        END AS concepto,
        CASE
            WHEN a.cxc_desde::text = 'AC'::text OR a.cxc_desde::text = 'NCC'::text THEN a.cxc_total
            ELSE 0::numeric
        END AS credito,
        CASE
            WHEN a.cxc_desde::text = 'FC'::text OR a.cxc_desde::text = 'NDC'::text THEN a.cxc_total
            ELSE 0::numeric
        END AS debito,
    c.prefijo_rif,
    c.rif,
    c.telefonos,
    c.razon_social,
    c.direccion_fiscal_1,
    c.direccion_fiscal_2
   FROM ( SELECT 'CXC'::text AS "from",
            a_1.numero_documento AS cxc_numero_documento,
            a_1.desde AS cxc_desde,
            a_1.fecha_ingreso AS cxc_fecha_ingreso,
            a_1.fecha_venc_ingreso AS cxc_fecha_venc_ingreso,
            a_1.fecha_recepcion AS cxc_fecha_recepcion,
            a_1.codigo_cliente AS cxc_codigo_cliente,
            a_1.codigo_vendedor AS cxc_codigo_vendedor,
            a_1.codigo_zona AS cxc_codigo_zona,
            a_1.monto_neto AS cxc_monto_neto,
            a_1.impuesto AS cxc_impuesto,
            a_1.total AS cxc_total,
            a_1.p_iva AS cxc_p_iva,
            a_1.numero_cuota AS cxc_numero_cuota,
            a_1.total_cuotas AS cxc_total_cuotas,
            a_1.orden_cobranza AS cxc_orden_cobranza
           FROM tbl_cuentas_x_cobrar a_1) a
     JOIN tbl_clientes c ON a.cxc_codigo_cliente::text = c.codigo::text;

ALTER TABLE public.v_rep_estado_cuenta_cliente_detallado_resumido
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_rep_estado_cuenta_cliente_detallado_resumido TO PUBLIC;
GRANT ALL ON TABLE public.v_rep_estado_cuenta_cliente_detallado_resumido TO postgres;
