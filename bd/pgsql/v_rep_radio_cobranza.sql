﻿-- View: public.v_rep_radio_cobranza

-- DROP VIEW public.v_rep_radio_cobranza;

CREATE OR REPLACE VIEW public.v_rep_radio_cobranza AS
 SELECT a.id_radio_contrato,
    a.item,
    a.total_items,
    a.fecha_ingreso AS fecha_factura_generada,
    a.total AS total_facturas_generadas,
    a.fecha_inicial,
    a.fecha_final,
    a.observaciones AS observaciones_factura_generadas,
    b.numero AS numero_factura_cliente,
    d.razon_social,
    f.fecha_ingreso AS fecha_liquidacion_cobranza,
    f.total AS total_liquidacion_cobranza,
    f.numero AS numero_liquidacion_cobranza,
    g.codigo_tipo_operacion_bancaria AS tipo_operacion_bancaria,
    g.numero AS numero_movimiento_bancario,
    h.descripcion AS descripcion_tipo_operacion_bancaria
   FROM tbl_radio_contratos_facturas_generadas a
     JOIN tbl_facturacion_clientes b ON a.id_facturacion_cliente = b.id_facturacion_cliente
     JOIN tbl_cobranzas_cobradas c ON b.id_cobranza = c.id_cobranza
     LEFT JOIN tbl_clientes d ON b.codigo_cliente::text = d.codigo::text
     LEFT JOIN tbl_det_liquidacion_cobranzas e ON c.id_cobranza = e.id_cobranza
     LEFT JOIN tbl_liquidacion_cobranzas f ON e.id_liquidacion_cobranza = f.id_liquidacion_cobranza
     LEFT JOIN tbl_operaciones_bancarias g ON f.id_operacion_bancaria = g.id_operacion_bancaria
     LEFT JOIN tbl_tipos_operaciones_bancarias h ON g.codigo_tipo_operacion_bancaria::text = h.codigo::text;

ALTER TABLE public.v_rep_radio_cobranza
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_rep_radio_cobranza TO PUBLIC;
GRANT ALL ON TABLE public.v_rep_radio_cobranza TO postgres;
