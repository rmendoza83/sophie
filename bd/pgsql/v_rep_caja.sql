﻿-- View: public.v_rep_caja

-- DROP VIEW public.v_rep_caja;

CREATE OR REPLACE VIEW public.v_rep_caja AS
 SELECT b.desde,
    b.numero_documento,
    b.codigo_moneda,
    b.codigo_cliente,
    b.codigo_vendedor,
    b.codigo_zona,
    b.fecha_ingreso,
    b.fecha_recepcion,
    b.fecha_venc_ingreso,
    b.fecha_venc_recepcion,
    b.fecha_libros,
    b.fecha_contable,
    b.fecha_cobranza,
    b.numero_cuota,
    b.total_cuotas,
    b.orden_cobranza,
    b.monto_neto,
    b.impuesto,
    b.total,
    b.p_iva,
    b.id_cobranza,
    b.id_contado_cobranza,
    a.codigo_forma_pago AS detco_codigo_forma_pago,
    a.monto AS detco_monto,
    a.numero_documento AS detco_numero_documento,
    c.razon_social AS cli_razon_social,
    d.codigo AS zon_codigo,
    d.descripcion AS zon_descripcion,
    e.codigo AS forpag_codigo,
    e.descripcion AS forpag_descripcion
   FROM tbl_det_contado_cobranzas a
     JOIN tbl_cobranzas_cobradas b ON a.id_contado_cobranza = b.id_contado_cobranza
     JOIN tbl_clientes c ON b.codigo_cliente::text = c.codigo::text
     JOIN tbl_zonas d ON b.codigo_zona::text = d.codigo::text
     JOIN tbl_formas_pago e ON a.codigo_forma_pago::text = e.codigo::text
  WHERE a.desde::text <> 'AC'::text
UNION
 SELECT b.desde,
    b.numero_documento,
    b.codigo_moneda,
    b.codigo_cliente,
    b.codigo_vendedor,
    b.codigo_zona,
    b.fecha_ingreso,
    b.fecha_recepcion,
    b.fecha_venc_ingreso,
    b.fecha_venc_recepcion,
    b.fecha_libros,
    b.fecha_contable,
    b.fecha_cobranza,
    b.numero_cuota,
    b.total_cuotas,
    b.orden_cobranza,
    b.monto_neto,
    b.impuesto,
    b.total,
    b.p_iva,
    b.id_cobranza,
    b.id_contado_cobranza,
    a.codigo_forma_pago AS detco_codigo_forma_pago,
    a.monto AS detco_monto,
    a.numero_documento AS detco_numero_documento,
    c.razon_social AS cli_razon_social,
    d.codigo AS zon_codigo,
    d.descripcion AS zon_descripcion,
    e.codigo AS forpag_codigo,
    e.descripcion AS forpag_descripcion
   FROM tbl_det_contado_cobranzas a
     JOIN tbl_cuentas_x_cobrar b ON a.id_contado_cobranza = b.id_contado_cobranza
     JOIN tbl_clientes c ON b.codigo_cliente::text = c.codigo::text
     JOIN tbl_zonas d ON b.codigo_zona::text = d.codigo::text
     JOIN tbl_formas_pago e ON a.codigo_forma_pago::text = e.codigo::text
  WHERE a.desde::text = 'AC'::text;

ALTER TABLE public.v_rep_caja
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_rep_caja TO PUBLIC;
GRANT ALL ON TABLE public.v_rep_caja TO postgres;
