﻿-- View: public.v_rep_retencion_islr

-- DROP VIEW public.v_rep_retencion_islr;

CREATE OR REPLACE VIEW public.v_rep_retencion_islr AS
 SELECT a.numero AS ret_numero,
    a.codigo_proveedor AS ret_codigo_pro,
    a.numero_comprobante,
    c.prefijo_rif AS prefijo_rif_prov,
    c.rif AS ret_rif_pro,
    a.codigo_zona AS ret_codigo_zona,
    a.fecha_ingreso AS ret_fecha_ingreso,
    a.fecha_contable AS ret_fecha_contable,
    a.total AS ret_total,
    b.desde,
    b.numero_documento,
    b.codigo_retencion_1,
    b.monto_retencion_1,
    e.descripcion AS descripcion_ret_1,
    e.tarifa AS tarifa_ret_1,
    b.codigo_retencion_2,
    b.monto_retencion_2,
    f.descripcion AS descripcion_ret_2,
    f.tarifa AS tarifa_ret_2,
    b.codigo_retencion_3,
    b.monto_retencion_3,
    g.descripcion AS descripcion_ret_3,
    g.tarifa AS tarifa_ret_3,
    b.codigo_retencion_4,
    b.monto_retencion_4,
    h.descripcion AS descripcion_ret_4,
    h.tarifa AS tarifa_ret_4,
    b.codigo_retencion_5,
    b.monto_retencion_5,
    i.descripcion AS descripcion_ret_5,
    i.tarifa AS tarifa_ret_5,
    c.razon_social,
    c.direccion_fiscal_1,
    c.direccion_fiscal_2,
    d.descripcion AS descripcion_zona,
    j.monto_neto,
    j.impuesto,
    j.total
   FROM tbl_retenciones_islr a
     JOIN tbl_det_retenciones_islr b ON a.numero::text = b.numero::text
     JOIN tbl_proveedores c ON a.codigo_proveedor::text = c.codigo::text
     JOIN tbl_zonas d ON a.codigo_zona::text = d.codigo::text
     LEFT JOIN tbl_tipos_retenciones e ON b.codigo_retencion_1::text = e.codigo::text
     LEFT JOIN tbl_tipos_retenciones f ON b.codigo_retencion_2::text = f.codigo::text
     LEFT JOIN tbl_tipos_retenciones g ON b.codigo_retencion_3::text = g.codigo::text
     LEFT JOIN tbl_tipos_retenciones h ON b.codigo_retencion_4::text = h.codigo::text
     LEFT JOIN tbl_tipos_retenciones i ON b.codigo_retencion_5::text = i.codigo::text
     LEFT JOIN tbl_facturacion_proveedores j ON b.numero_documento::text = j.numero::text
  WHERE b.desde::text = 'FP'::text
UNION
 SELECT a.numero AS ret_numero,
    a.codigo_proveedor AS ret_codigo_pro,
    a.numero_comprobante,
    c.prefijo_rif AS prefijo_rif_prov,
    c.rif AS ret_rif_pro,
    a.codigo_zona AS ret_codigo_zona,
    a.fecha_ingreso AS ret_fecha_ingreso,
    a.fecha_contable AS ret_fecha_contable,
    a.total AS ret_total,
    b.desde,
    b.numero_documento,
    b.codigo_retencion_1,
    b.monto_retencion_1,
    e.descripcion AS descripcion_ret_1,
    e.tarifa AS tarifa_ret_1,
    b.codigo_retencion_2,
    b.monto_retencion_2,
    f.descripcion AS descripcion_ret_2,
    f.tarifa AS tarifa_ret_2,
    b.codigo_retencion_3,
    b.monto_retencion_3,
    g.descripcion AS descripcion_ret_3,
    g.tarifa AS tarifa_ret_3,
    b.codigo_retencion_4,
    b.monto_retencion_4,
    h.descripcion AS descripcion_ret_4,
    h.tarifa AS tarifa_ret_4,
    b.codigo_retencion_5,
    b.monto_retencion_5,
    i.descripcion AS descripcion_ret_5,
    i.tarifa AS tarifa_ret_5,
    c.razon_social,
    c.direccion_fiscal_1,
    c.direccion_fiscal_2,
    d.descripcion AS descripcion_zona,
    j.monto_neto,
    j.impuesto,
    j.total
   FROM tbl_retenciones_islr a
     JOIN tbl_det_retenciones_islr b ON a.numero::text = b.numero::text
     JOIN tbl_proveedores c ON a.codigo_proveedor::text = c.codigo::text
     JOIN tbl_zonas d ON a.codigo_zona::text = d.codigo::text
     LEFT JOIN tbl_tipos_retenciones e ON b.codigo_retencion_1::text = e.codigo::text
     LEFT JOIN tbl_tipos_retenciones f ON b.codigo_retencion_2::text = f.codigo::text
     LEFT JOIN tbl_tipos_retenciones g ON b.codigo_retencion_3::text = g.codigo::text
     LEFT JOIN tbl_tipos_retenciones h ON b.codigo_retencion_4::text = h.codigo::text
     LEFT JOIN tbl_tipos_retenciones i ON b.codigo_retencion_5::text = i.codigo::text
     LEFT JOIN tbl_nota_debito_proveedores j ON b.numero_documento::text = j.numero::text
  WHERE b.desde::text = 'NDP'::text
UNION
 SELECT a.numero AS ret_numero,
    a.codigo_proveedor AS ret_codigo_pro,
    a.numero_comprobante,
    c.prefijo_rif AS prefijo_rif_prov,
    c.rif AS ret_rif_pro,
    a.codigo_zona AS ret_codigo_zona,
    a.fecha_ingreso AS ret_fecha_ingreso,
    a.fecha_contable AS ret_fecha_contable,
    a.total AS ret_total,
    b.desde,
    b.numero_documento,
    b.codigo_retencion_1,
    b.monto_retencion_1,
    e.descripcion AS descripcion_ret_1,
    e.tarifa AS tarifa_ret_1,
    b.codigo_retencion_2,
    b.monto_retencion_2,
    f.descripcion AS descripcion_ret_2,
    f.tarifa AS tarifa_ret_2,
    b.codigo_retencion_3,
    b.monto_retencion_3,
    g.descripcion AS descripcion_ret_3,
    g.tarifa AS tarifa_ret_3,
    b.codigo_retencion_4,
    b.monto_retencion_4,
    h.descripcion AS descripcion_ret_4,
    h.tarifa AS tarifa_ret_4,
    b.codigo_retencion_5,
    b.monto_retencion_5,
    i.descripcion AS descripcion_ret_5,
    i.tarifa AS tarifa_ret_5,
    c.razon_social,
    c.direccion_fiscal_1,
    c.direccion_fiscal_2,
    d.descripcion AS descripcion_zona,
    j.monto_neto,
    0 AS impuesto,
    j.monto_neto AS total
   FROM tbl_retenciones_islr a
     JOIN tbl_det_retenciones_islr b ON a.numero::text = b.numero::text
     JOIN tbl_proveedores c ON a.codigo_proveedor::text = c.codigo::text
     JOIN tbl_zonas d ON a.codigo_zona::text = d.codigo::text
     LEFT JOIN tbl_tipos_retenciones e ON b.codigo_retencion_1::text = e.codigo::text
     LEFT JOIN tbl_tipos_retenciones f ON b.codigo_retencion_2::text = f.codigo::text
     LEFT JOIN tbl_tipos_retenciones g ON b.codigo_retencion_3::text = g.codigo::text
     LEFT JOIN tbl_tipos_retenciones h ON b.codigo_retencion_4::text = h.codigo::text
     LEFT JOIN tbl_tipos_retenciones i ON b.codigo_retencion_5::text = i.codigo::text
     LEFT JOIN tbl_anticipo_proveedores j ON b.numero_documento::text = j.numero::text
  WHERE b.desde::text = 'AP'::text;

ALTER TABLE public.v_rep_retencion_islr
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_rep_retencion_islr TO PUBLIC;
GRANT ALL ON TABLE public.v_rep_retencion_islr TO postgres;
