﻿-- View: public.v_rep_retencion_iva_ventas

-- DROP VIEW public.v_rep_retencion_iva_ventas;

CREATE OR REPLACE VIEW public.v_rep_retencion_iva_ventas AS
 SELECT '01-Fac'::text AS tipo_documento,
    'FC'::text AS desde,
    a.numero,
    a.fecha_ingreso,
    a.fecha_contable,
    a.fecha_libros,
    a.sub_total,
    a.impuesto,
    a.total,
    a.p_descuento,
    a.p_iva,
    b.monto_retencion_iva AS iva_retenido,
    a.impuesto - b.monto_retencion_iva AS iva_percibido,
    d.codigo,
    d.razon_social,
    d.prefijo_rif AS prefijo_rif_prov,
    d.rif AS ret_rif_pro,
    e.serie,
    e.numero_control
   FROM tbl_facturacion_clientes a
     LEFT JOIN tbl_det_liquidacion_cobranzas b ON a.id_cobranza = b.id_cobranza AND b.orden_cobranza = 1
     LEFT JOIN tbl_tipos_retenciones c ON b.codigo_retencion_iva::text = c.codigo::text
     LEFT JOIN tbl_clientes d ON a.codigo_cliente::text = d.codigo::text
     LEFT JOIN tbl_documentos_legales e ON a.numero::text = e.numero_documento::text AND e.desde::text = 'FC'::text
UNION
 SELECT '02-Ndc'::text AS tipo_documento,
    'NDC'::text AS desde,
    a.numero,
    a.fecha_ingreso,
    a.fecha_contable,
    a.fecha_libros,
    a.sub_total,
    a.impuesto,
    a.total,
    a.p_descuento,
    a.p_iva,
    b.monto_retencion_iva AS iva_retenido,
    a.impuesto - b.monto_retencion_iva AS iva_percibido,
    d.codigo,
    d.razon_social,
    d.prefijo_rif AS prefijo_rif_prov,
    d.rif AS ret_rif_pro,
    e.serie,
    e.numero_control
   FROM tbl_nota_debito_clientes a
     LEFT JOIN tbl_det_liquidacion_cobranzas b ON a.id_cobranza = b.id_cobranza AND b.orden_cobranza = 1
     LEFT JOIN tbl_tipos_retenciones c ON b.codigo_retencion_iva::text = c.codigo::text
     LEFT JOIN tbl_clientes d ON a.codigo_cliente::text = d.codigo::text
     LEFT JOIN tbl_documentos_legales e ON a.numero::text = e.numero_documento::text AND e.desde::text = 'NDC'::text
UNION
 SELECT '03-Ncc'::text AS tipo_documento,
    'NCC'::text AS desde,
    a.numero,
    a.fecha_ingreso,
    a.fecha_contable,
    a.fecha_libros,
    a.sub_total,
    a.impuesto,
    a.total,
    a.p_descuento,
    a.p_iva,
    b.monto_retencion_iva AS iva_retenido,
    a.impuesto - b.monto_retencion_iva AS iva_percibido,
    d.codigo,
    d.razon_social,
    d.prefijo_rif AS prefijo_rif_prov,
    d.rif AS ret_rif_pro,
    e.serie,
    e.numero_control
   FROM tbl_nota_credito_clientes a
     LEFT JOIN tbl_det_liquidacion_cobranzas b ON a.id_cobranza = b.id_cobranza AND b.orden_cobranza = 1
     LEFT JOIN tbl_tipos_retenciones c ON b.codigo_retencion_iva::text = c.codigo::text
     LEFT JOIN tbl_clientes d ON a.codigo_cliente::text = d.codigo::text
     LEFT JOIN tbl_documentos_legales e ON a.numero::text = e.numero_documento::text AND e.desde::text = 'NCC'::text;

ALTER TABLE public.v_rep_retencion_iva_ventas
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_rep_retencion_iva_ventas TO PUBLIC;
GRANT ALL ON TABLE public.v_rep_retencion_iva_ventas TO postgres;
