﻿-- View: public.v_rep_analisis_vencimiento_cliente

-- DROP VIEW public.v_rep_analisis_vencimiento_cliente;

CREATE OR REPLACE VIEW public.v_rep_analisis_vencimiento_cliente AS
 SELECT a.numero_documento,
        CASE
            WHEN 'now'::text::date <= a.fecha_venc_ingreso THEN a.total
            ELSE 0::numeric
        END AS monto_no_vencido,
        CASE
            WHEN 'now'::text::date > a.fecha_venc_ingreso AND ('now'::text::date - a.fecha_venc_ingreso) > 0 AND ('now'::text::date - a.fecha_venc_ingreso) <= 30 THEN a.total
            ELSE 0::numeric
        END AS monto_vencido_30,
        CASE
            WHEN 'now'::text::date > a.fecha_venc_ingreso AND ('now'::text::date - a.fecha_venc_ingreso) > 30 AND ('now'::text::date - a.fecha_venc_ingreso) <= 60 THEN a.total
            ELSE 0::numeric
        END AS monto_vencido_60,
        CASE
            WHEN 'now'::text::date > a.fecha_venc_ingreso AND ('now'::text::date - a.fecha_venc_ingreso) > 60 AND ('now'::text::date - a.fecha_venc_ingreso) <= 90 THEN a.total
            ELSE 0::numeric
        END AS monto_vencido_90,
        CASE
            WHEN 'now'::text::date > a.fecha_venc_ingreso AND ('now'::text::date - a.fecha_venc_ingreso) > 90 THEN a.total
            ELSE 0::numeric
        END AS monto_vencido_mas90
   FROM tbl_cuentas_x_cobrar a;

ALTER TABLE public.v_rep_analisis_vencimiento_cliente
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_rep_analisis_vencimiento_cliente TO PUBLIC;
GRANT ALL ON TABLE public.v_rep_analisis_vencimiento_cliente TO postgres;
