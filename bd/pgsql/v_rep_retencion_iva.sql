﻿-- View: public.v_rep_retencion_iva

-- DROP VIEW public.v_rep_retencion_iva;

CREATE OR REPLACE VIEW public.v_rep_retencion_iva AS
 SELECT a.numero AS ret_numero,
    a.codigo_proveedor AS ret_codigo_pro,
    a.numero_comprobante,
    c.razon_social,
    c.prefijo_rif AS prefijo_rif_prov,
    c.rif AS ret_rif_pro,
    c.telefonos,
    c.direccion_fiscal_1,
    c.direccion_fiscal_2,
    a.codigo_zona AS ret_codigo_zona,
    a.fecha_ingreso AS ret_fecha_ingreso,
    a.fecha_contable AS ret_fecha_contable,
    a.total AS ret_total,
    b.desde,
    b.numero_documento,
    b.codigo_retencion,
    b.monto_retencion,
    j.fecha_ingreso AS fecha_documento,
    j.sub_total,
    j.impuesto,
    j.total,
    j.p_iva,
    i.numero_control
   FROM tbl_retenciones_iva a
     JOIN tbl_det_retenciones_iva b ON a.numero::text = b.numero::text
     JOIN tbl_proveedores c ON a.codigo_proveedor::text = c.codigo::text
     JOIN tbl_zonas d ON a.codigo_zona::text = d.codigo::text
     LEFT JOIN tbl_facturacion_proveedores j ON b.numero_documento::text = j.numero::text
     JOIN tbl_documentos_legales i ON b.desde = i.desde::bpchar AND b.numero_documento::text = i.numero_documento::text
  WHERE b.desde::text = 'FP'::text
UNION
 SELECT a.numero AS ret_numero,
    a.codigo_proveedor AS ret_codigo_pro,
    a.numero_comprobante,
    c.razon_social,
    c.prefijo_rif AS prefijo_rif_prov,
    c.rif AS ret_rif_pro,
    c.telefonos,
    c.direccion_fiscal_1,
    c.direccion_fiscal_2,
    a.codigo_zona AS ret_codigo_zona,
    a.fecha_ingreso AS ret_fecha_ingreso,
    a.fecha_contable AS ret_fecha_contable,
    a.total AS ret_total,
    b.desde,
    b.numero_documento,
    b.codigo_retencion,
    b.monto_retencion,
    j.fecha_ingreso AS fecha_documento,
    j.sub_total,
    j.impuesto,
    j.total,
    j.p_iva,
    i.numero_control
   FROM tbl_retenciones_iva a
     JOIN tbl_det_retenciones_iva b ON a.numero::text = b.numero::text
     JOIN tbl_proveedores c ON a.codigo_proveedor::text = c.codigo::text
     JOIN tbl_zonas d ON a.codigo_zona::text = d.codigo::text
     LEFT JOIN tbl_nota_debito_proveedores j ON b.numero_documento::text = j.numero::text
     JOIN tbl_documentos_legales i ON b.desde = i.desde::bpchar AND b.numero_documento::text = i.numero_documento::text
  WHERE b.desde::text = 'NDP'::text
UNION
 SELECT a.numero AS ret_numero,
    a.codigo_proveedor AS ret_codigo_pro,
    a.numero_comprobante,
    c.razon_social,
    c.prefijo_rif AS prefijo_rif_prov,
    c.rif AS ret_rif_pro,
    c.telefonos,
    c.direccion_fiscal_1,
    c.direccion_fiscal_2,
    a.codigo_zona AS ret_codigo_zona,
    a.fecha_ingreso AS ret_fecha_ingreso,
    a.fecha_contable AS ret_fecha_contable,
    a.total AS ret_total,
    b.desde,
    b.numero_documento,
    b.codigo_retencion,
    b.monto_retencion,
    j.fecha_ingreso AS fecha_documento,
    j.sub_total,
    j.impuesto,
    j.total,
    j.p_iva,
    i.numero_control
   FROM tbl_retenciones_iva a
     JOIN tbl_det_retenciones_iva b ON a.numero::text = b.numero::text
     JOIN tbl_proveedores c ON a.codigo_proveedor::text = c.codigo::text
     JOIN tbl_zonas d ON a.codigo_zona::text = d.codigo::text
     LEFT JOIN tbl_nota_credito_proveedores j ON b.numero_documento::text = j.numero::text
     JOIN tbl_documentos_legales i ON b.desde = i.desde::bpchar AND b.numero_documento::text = i.numero_documento::text
  WHERE b.desde::text = 'NCP'::text;

ALTER TABLE public.v_rep_retencion_iva
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_rep_retencion_iva TO PUBLIC;
GRANT ALL ON TABLE public.v_rep_retencion_iva TO postgres;
