﻿-- View: public.v_rep_radio_facturacion_cliente

-- DROP VIEW public.v_rep_radio_facturacion_cliente;

CREATE OR REPLACE VIEW public.v_rep_radio_facturacion_cliente AS
 SELECT a.numero AS fc_numero,
    a.fecha_ingreso AS fc_fecha_ingreso,
    a.codigo_vendedor AS fc_codigo_vendedor,
    a.monto_neto AS fc_monto_neto,
    a.monto_descuento AS fc_monto_descuento,
    a.sub_total AS fc_sub_total,
    a.impuesto AS fc_impuesto,
    a.p_descuento AS fc_p_descuento,
    a.p_iva AS fc_p_iva,
    a.tasa_dolar AS fc_tasa_dolar,
    a.codigo_esquema_pago AS fc_codigo_esquema_pago,
    bb.descripcion AS descripcion_esquema_pago,
    a.total AS fc_total,
    a.observaciones AS fc_observaciones,
    a.codigo_moneda AS fc_codigo_moneda,
    a.concepto AS fc_concepto,
    b.codigo_producto,
    b.descripcion AS detfac_descripcion,
    b.cantidad AS detfac_cantidad,
    b.precio AS detfac_precio,
    b.p_descuento AS detfac_descuento,
    b.monto_descuento AS detfac_monto_descuento,
    b.impuesto AS detfac_impuesto,
    c.prefijo_rif,
    c.rif,
    c.telefonos,
    c.razon_social,
    c.direccion_fiscal_1,
    c.direccion_fiscal_2,
    d.fecha_inicial AS fecha_inicial_distribucion_factura,
    d.fecha_final AS fecha_final_distribucion_factura,
    e.producto AS producto_contrato,
    e.numero_orden,
    f.codigo_programa,
    g.descripcion AS descripcion_programa,
    h.descripcion AS descripcion_espacio
   FROM tbl_facturacion_clientes a
     LEFT JOIN tbl_det_facturacion_clientes b ON a.id_facturacion_cliente = b.id_facturacion_cliente
     JOIN tbl_esquemas_pago bb ON a.codigo_esquema_pago::text = bb.codigo::text
     JOIN tbl_clientes c ON a.codigo_cliente::text = c.codigo::text
     JOIN v_radio_contratos_distribucion_facturas d ON a.id_facturacion_cliente = d.id_facturacion_cliente
     JOIN tbl_radio_contratos e ON d.id_radio_contrato = e.id_radio_contrato
     JOIN tbl_radio_contratos_programas f ON d.id_radio_contrato = f.id_radio_contrato
     JOIN tbl_radio_programas g ON f.codigo_programa::text = g.codigo::text
     JOIN tbl_radio_espacios h ON f.codigo_espacio::text = h.codigo::text;

ALTER TABLE public.v_rep_radio_facturacion_cliente
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_rep_radio_facturacion_cliente TO PUBLIC;
GRANT ALL ON TABLE public.v_rep_radio_facturacion_cliente TO postgres;
