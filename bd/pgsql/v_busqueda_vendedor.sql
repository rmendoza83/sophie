﻿-- View: public.v_busqueda_vendedor

-- DROP VIEW public.v_busqueda_vendedor;

CREATE OR REPLACE VIEW public.v_busqueda_vendedor AS
 SELECT a.codigo,
    a.descripcion,
    a.activo
   FROM tbl_vendedores a;

ALTER TABLE public.v_busqueda_vendedor
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_busqueda_vendedor TO PUBLIC;
GRANT ALL ON TABLE public.v_busqueda_vendedor TO postgres;
