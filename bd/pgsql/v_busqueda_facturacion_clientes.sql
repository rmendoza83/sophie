﻿-- View: public.v_busqueda_facturacion_clientes

-- DROP VIEW public.v_busqueda_facturacion_clientes;

CREATE OR REPLACE VIEW public.v_busqueda_facturacion_clientes AS
 SELECT a.numero,
    a.numero_pedido,
    a.codigo_cliente,
    ((b.prefijo_rif::text || b.rif::text))::character varying(20) AS rif,
    b.nit,
    b.razon_social,
    b.contacto,
    c.codigo AS codigo_vendedor,
    c.descripcion AS nombre_vendedor
   FROM tbl_facturacion_clientes a
     LEFT JOIN tbl_clientes b ON a.codigo_cliente::text = b.codigo::text
     LEFT JOIN tbl_vendedores c ON a.codigo_vendedor::text = c.codigo::text
  ORDER BY a.id_facturacion_cliente;

ALTER TABLE public.v_busqueda_facturacion_clientes
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_busqueda_facturacion_clientes TO PUBLIC;
GRANT ALL ON TABLE public.v_busqueda_facturacion_clientes TO postgres;
