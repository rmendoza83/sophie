﻿-- View: public.v_rep_caja_egreso

-- DROP VIEW public.v_rep_caja_egreso;

CREATE OR REPLACE VIEW public.v_rep_caja_egreso AS
 SELECT b.desde,
    b.numero_documento,
    b.codigo_moneda,
    b.codigo_proveedor,
    b.codigo_zona,
    b.fecha_ingreso,
    b.fecha_recepcion,
    b.fecha_venc_ingreso,
    b.fecha_venc_recepcion,
    b.fecha_libros,
    b.fecha_contable,
    b.fecha_pago,
    b.numero_cuota,
    b.total_cuotas,
    b.orden_pago,
    b.monto_neto,
    b.impuesto,
    b.total,
    b.p_iva,
    b.id_pago,
    b.id_contado_pago,
    a.codigo_forma_pago AS detco_codigo_forma_pago,
    a.monto AS detco_monto,
    a.numero_cuenta AS detpa_numero_cuenta,
    a.numero_auxiliar AS detpa_numero_auxiliar,
    c.razon_social AS cli_razon_social,
    d.codigo AS zon_codigo,
    d.descripcion AS zon_descripcion,
    e.codigo AS forpag_codigo,
    e.descripcion AS forpag_descripcion
   FROM tbl_det_contado_pagos a
     JOIN tbl_pagos_pagados b ON a.id_contado_pago = b.id_contado_pago
     JOIN tbl_proveedores c ON b.codigo_proveedor::text = c.codigo::text
     JOIN tbl_zonas d ON b.codigo_zona::text = d.codigo::text
     JOIN tbl_formas_pago e ON a.codigo_forma_pago::text = e.codigo::text
  WHERE a.desde::text <> 'AP'::text
UNION
 SELECT b.desde,
    b.numero_documento,
    b.codigo_moneda,
    b.codigo_proveedor,
    b.codigo_zona,
    b.fecha_ingreso,
    b.fecha_recepcion,
    b.fecha_venc_ingreso,
    b.fecha_venc_recepcion,
    b.fecha_libros,
    b.fecha_contable,
    b.fecha_pago,
    b.numero_cuota,
    b.total_cuotas,
    b.orden_pago,
    b.monto_neto,
    b.impuesto,
    b.total,
    b.p_iva,
    b.id_pago,
    b.id_contado_pago,
    a.codigo_forma_pago AS detco_codigo_forma_pago,
    a.monto AS detco_monto,
    a.numero_cuenta AS detpa_numero_cuenta,
    a.numero_auxiliar AS detpa_numero_auxiliar,
    c.razon_social AS cli_razon_social,
    d.codigo AS zon_codigo,
    d.descripcion AS zon_descripcion,
    e.codigo AS forpag_codigo,
    e.descripcion AS forpag_descripcion
   FROM tbl_det_contado_pagos a
     JOIN tbl_cuentas_x_pagar b ON a.id_contado_pago = b.id_contado_pago
     JOIN tbl_proveedores c ON b.codigo_proveedor::text = c.codigo::text
     JOIN tbl_zonas d ON b.codigo_zona::text = d.codigo::text
     JOIN tbl_formas_pago e ON a.codigo_forma_pago::text = e.codigo::text
  WHERE a.desde::text = 'AP'::text;

ALTER TABLE public.v_rep_caja_egreso
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_rep_caja_egreso TO PUBLIC;
GRANT ALL ON TABLE public.v_rep_caja_egreso TO postgres;
