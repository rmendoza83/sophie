﻿-- View: public.v_busqueda_documentos_retencion_iva

-- DROP VIEW public.v_busqueda_documentos_retencion_iva;

CREATE OR REPLACE VIEW public.v_busqueda_documentos_retencion_iva AS
 SELECT 'FP'::character varying(10) AS desde,
    a.numero,
    a.codigo_proveedor,
    b.rif,
    b.nit,
    b.razon_social,
    b.contacto,
    a.monto_neto,
    a.impuesto
   FROM tbl_facturacion_proveedores a
     JOIN tbl_proveedores b ON a.codigo_proveedor::text = b.codigo::text
  WHERE NOT ((a.codigo_proveedor::text || '-'::text) || a.numero::text IN ( SELECT (a_1.codigo_proveedor::text || '-'::text) || b_1.numero_documento::text AS codigo_unico
           FROM tbl_retenciones_iva a_1
             JOIN tbl_det_retenciones_iva b_1 ON a_1.numero::text = b_1.numero::text
          WHERE b_1.desde::text = 'FP'::text))
UNION
 SELECT 'NDP'::character varying(10) AS desde,
    a.numero,
    a.codigo_proveedor,
    b.rif,
    b.nit,
    b.razon_social,
    b.contacto,
    a.monto_neto,
    a.impuesto
   FROM tbl_nota_debito_proveedores a
     JOIN tbl_proveedores b ON a.codigo_proveedor::text = b.codigo::text
  WHERE NOT ((a.codigo_proveedor::text || '-'::text) || a.numero::text IN ( SELECT (a_1.codigo_proveedor::text || '-'::text) || b_1.numero_documento::text AS codigo_unico
           FROM tbl_retenciones_iva a_1
             JOIN tbl_det_retenciones_iva b_1 ON a_1.numero::text = b_1.numero::text
          WHERE b_1.desde::text = 'NDP'::text))
UNION
 SELECT 'NCP'::character varying(10) AS desde,
    a.numero,
    a.codigo_proveedor,
    b.rif,
    b.nit,
    b.razon_social,
    b.contacto,
    a.monto_neto,
    a.impuesto
   FROM tbl_nota_credito_proveedores a
     JOIN tbl_proveedores b ON a.codigo_proveedor::text = b.codigo::text
  WHERE NOT ((a.codigo_proveedor::text || '-'::text) || a.numero::text IN ( SELECT (a_1.codigo_proveedor::text || '-'::text) || b_1.numero_documento::text AS codigo_unico
           FROM tbl_retenciones_iva a_1
             JOIN tbl_det_retenciones_iva b_1 ON a_1.numero::text = b_1.numero::text
          WHERE b_1.desde::text = 'NCP'::text));

ALTER TABLE public.v_busqueda_documentos_retencion_iva
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_busqueda_documentos_retencion_iva TO PUBLIC;
GRANT ALL ON TABLE public.v_busqueda_documentos_retencion_iva TO postgres;
