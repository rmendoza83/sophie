﻿-- View: public.v_busqueda_transferencias_clientes

-- DROP VIEW public.v_busqueda_transferencias_clientes;

CREATE OR REPLACE VIEW public.v_busqueda_transferencias_clientes AS
 SELECT a.numero,
    a.codigo_cliente,
    ((b.prefijo_rif::text || b.rif::text))::character varying(20) AS rif,
    b.nit,
    b.razon_social,
    b.contacto
   FROM tbl_transferencias_clientes a
     LEFT JOIN tbl_clientes b ON a.codigo_cliente::text = b.codigo::text
  ORDER BY a.id_transferencia_cliente;

ALTER TABLE public.v_busqueda_transferencias_clientes
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_busqueda_transferencias_clientes TO PUBLIC;
GRANT ALL ON TABLE public.v_busqueda_transferencias_clientes TO postgres;
