﻿-- View: public.v_busqueda_presupuesto

-- DROP VIEW public.v_busqueda_presupuesto;

CREATE OR REPLACE VIEW public.v_busqueda_presupuesto AS
 SELECT a.numero,
    a.codigo_cliente,
    ((b.prefijo_rif::text || b.rif::text))::character varying(20) AS rif,
    b.nit,
    b.razon_social,
    b.contacto,
    c.codigo AS codigo_vendedor,
    c.descripcion AS nombre_vendedor
   FROM tbl_presupuestos a
     LEFT JOIN tbl_clientes b ON a.codigo_cliente::text = b.codigo::text
     LEFT JOIN tbl_vendedores c ON a.codigo_vendedor::text = c.codigo::text
  ORDER BY a.id_presupuesto;

ALTER TABLE public.v_busqueda_presupuesto
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_busqueda_presupuesto TO PUBLIC;
GRANT ALL ON TABLE public.v_busqueda_presupuesto TO postgres;
