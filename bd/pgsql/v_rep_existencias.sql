﻿-- View: public.v_rep_existencias

-- DROP VIEW public.v_rep_existencias;

CREATE OR REPLACE VIEW public.v_rep_existencias AS
 SELECT a.desde,
    a.numero_documento,
    a.codigo_producto,
    a.cantidad,
    a.estacion_salida,
    a.estacion_entrada,
    a.codigo_zona,
    a.precio,
    a.p_descuento,
    a.monto_descuento,
    a.impuesto,
    a.p_iva,
    a.total,
    a.costo,
    a.us_costo,
    a.costo_fob,
    a.fecha,
    a.codigo_cliente,
    a.codigo_proveedor,
    a.id_movimiento_inventario,
    b.descripcion,
    b.codigo_unidad,
    b.codigo_division,
    b.codigo_linea,
    b.codigo_familia,
    b.codigo_clase,
    b.codigo_manufactura,
    b.codigo_arancel,
    b.fecha_ingreso,
    b.inventario,
        CASE
            WHEN a.desde::text = 'FC'::text THEN a.cantidad * '-1'::integer::numeric
            WHEN a.desde::text = 'FP'::text THEN a.cantidad
            ELSE 0::numeric
        END AS cant_restante,
        CASE
            WHEN a.desde::text = 'FP'::text THEN a.precio
            ELSE 0::numeric
        END AS precio_compra,
        CASE
            WHEN a.desde::text = 'FC'::text THEN a.cantidad * '-1'::integer::numeric * a.precio
            WHEN a.desde::text = 'FP'::text THEN a.cantidad * a.precio
            ELSE 0::numeric
        END AS monto_neto,
        CASE
            WHEN a.desde::text = 'FP'::text THEN a.cantidad
            ELSE 0::numeric
        END AS cantidad_entradas,
        CASE
            WHEN a.desde::text = 'FP'::text THEN a.cantidad * a.precio
            ELSE 0::numeric
        END AS monto_entradas,
        CASE
            WHEN a.desde::text = 'FC'::text THEN a.cantidad
            ELSE 0::numeric
        END AS cantidad_salidas,
        CASE
            WHEN a.desde::text = 'FC'::text THEN a.cantidad * a.precio
            ELSE 0::numeric
        END AS monto_salidas,
    b.p_iva_venta AS iva_venta_producto,
    b.p_iva_compra AS iva_compra_producto,
    b.precio AS precio_producto,
    b.p_descuento AS descuento_producto,
    b.costo_anterior AS costo_anterior_producto,
    b.fecha_u_compra AS fecha_u_compra_producto,
    b.us_costo_anterior AS us_costo_anterior_producto,
    b.costo_actual AS costo_actual_producto,
    b.costo_fob AS costo_fob_producto,
    c.descripcion AS descripcion_division,
    d.descripcion AS descripcion_linea,
    e.descripcion AS descripcion_familia,
    f.descripcion AS descripcion_clase,
    g.descripcion AS descripcion_aranceles,
    h.descripcion AS descripcion_manufactura,
    i.descripcion AS descripcion_zonas,
    j.razon_social AS nombre_cliente,
    k.razon_social AS nombre_proveedor,
    l.descripcion AS descripcion_unidad
   FROM tbl_det_movimientos_inventario a
     JOIN tbl_productos b ON a.codigo_producto::text = b.codigo::text
     JOIN tbl_divisiones c ON b.codigo_division::text = c.codigo::text
     JOIN tbl_lineas d ON b.codigo_linea::text = d.codigo::text
     JOIN tbl_familias e ON b.codigo_familia::text = e.codigo::text
     JOIN tbl_clases f ON b.codigo_clase::text = f.codigo::text
     JOIN tbl_aranceles g ON b.codigo_arancel::text = g.codigo::text
     JOIN tbl_manufacturas h ON b.codigo_manufactura::text = h.codigo::text
     JOIN tbl_zonas i ON a.codigo_zona::text = i.codigo::text
     LEFT JOIN tbl_clientes j ON a.codigo_cliente::text = j.codigo::text
     LEFT JOIN tbl_proveedores k ON a.codigo_proveedor::text = k.codigo::text
     JOIN tbl_unidades l ON b.codigo_unidad::text = l.codigo::text
  ORDER BY a.fecha;

ALTER TABLE public.v_rep_existencias
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_rep_existencias TO PUBLIC;
GRANT ALL ON TABLE public.v_rep_existencias TO postgres;
