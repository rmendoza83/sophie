﻿-- View: public.v_busqueda_movimientos_inventario

-- DROP VIEW public.v_busqueda_movimientos_inventario;

CREATE OR REPLACE VIEW public.v_busqueda_movimientos_inventario AS
 SELECT a.numero,
    a.concepto,
    a.login_usuario
   FROM tbl_movimientos_inventario a;

ALTER TABLE public.v_busqueda_movimientos_inventario
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_busqueda_movimientos_inventario TO PUBLIC;
GRANT ALL ON TABLE public.v_busqueda_movimientos_inventario TO postgres;
