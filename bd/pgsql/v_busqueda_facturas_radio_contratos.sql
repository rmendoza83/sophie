﻿-- View: public.v_busqueda_facturas_radio_contratos

-- DROP VIEW public.v_busqueda_facturas_radio_contratos;

CREATE OR REPLACE VIEW public.v_busqueda_facturas_radio_contratos AS
 SELECT b.numero AS numero_contrato,
    b.codigo_cliente,
    c.rif,
    c.nit,
    c.razon_social,
    c.contacto,
    b.total AS total_radio_contrato,
    a.id_radio_contrato,
    a.item,
    a.total_items,
    a.fecha_ingreso,
    a.total,
    a.fecha_inicial,
    a.fecha_final,
    a.observaciones
   FROM tbl_radio_contratos_facturas_pendientes a
     JOIN tbl_radio_contratos b ON a.id_radio_contrato = b.id_radio_contrato
     JOIN tbl_clientes c ON b.codigo_cliente::text = c.codigo::text;

ALTER TABLE public.v_busqueda_facturas_radio_contratos
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_busqueda_facturas_radio_contratos TO PUBLIC;
GRANT ALL ON TABLE public.v_busqueda_facturas_radio_contratos TO postgres;
