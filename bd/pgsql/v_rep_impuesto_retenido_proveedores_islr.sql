﻿-- View: public.v_rep_impuesto_retenido_proveedores_islr

-- DROP VIEW public.v_rep_impuesto_retenido_proveedores_islr;

CREATE OR REPLACE VIEW public.v_rep_impuesto_retenido_proveedores_islr AS
 SELECT a.numero AS numero_retencion_islr,
    a.codigo_proveedor,
    a.codigo_zona,
    a.fecha_ingreso,
    a.fecha_contable,
    a.total,
    a.numero_comprobante,
    b.desde AS desde_det_islr,
    b.numero_documento AS numero_documento_det_islr,
    b.codigo_retencion_1 AS codigo_retencion_1_det_islr,
    b.monto_retencion_1 AS monto_retencion_1_det_islr,
    c.descripcion AS descripcion_1,
    b.codigo_retencion_2 AS codigo_retencion_2_det_islr,
    b.monto_retencion_2 AS monto_retencion_2_det_islr,
    c.descripcion AS descripcion_2,
    b.codigo_retencion_3 AS codigo_retencion_3_det_islr,
    b.monto_retencion_3 AS monto_retencion_3_det_islr,
    c.descripcion AS descripcion_3,
    b.codigo_retencion_4 AS codigo_retencion_4_det_islr,
    b.monto_retencion_4 AS monto_retencion_4_det_islr,
    c.descripcion AS descripcion_4,
    b.codigo_retencion_5 AS codigo_retencion_5_det_islr,
    b.monto_retencion_5 AS monto_retencion_5_det_islr,
    c.descripcion AS descripcion_5,
    h.numero AS numero_factura,
    h.fecha_ingreso AS fecha_ingreso_factura,
    h.fecha_recepcion AS fecha_recepcion_factura,
    h.fecha_contable AS fecha_contable_factura,
    h.fecha_libros AS fecha_libros_factura,
    h.monto_neto AS monto_neto_factura,
    i.serie AS serie_legal,
    i.numero_control,
    j.razon_social
   FROM tbl_retenciones_islr a
     JOIN tbl_det_retenciones_islr b ON a.numero::text = b.numero::text
     LEFT JOIN tbl_tipos_retenciones c ON b.codigo_retencion_1::text = c.codigo::text
     LEFT JOIN tbl_tipos_retenciones d ON b.codigo_retencion_2::text = d.codigo::text
     LEFT JOIN tbl_tipos_retenciones e ON b.codigo_retencion_3::text = e.codigo::text
     LEFT JOIN tbl_tipos_retenciones f ON b.codigo_retencion_4::text = f.codigo::text
     LEFT JOIN tbl_tipos_retenciones g ON b.codigo_retencion_5::text = g.codigo::text
     JOIN tbl_facturacion_proveedores h ON b.numero_documento::text = h.numero::text AND b.desde::text = 'FP'::text
     JOIN tbl_documentos_legales i ON b.numero_documento::text = i.numero_documento::text AND b.desde::text = 'FP'::text
     JOIN tbl_proveedores j ON a.codigo_proveedor::text = j.codigo::text
  ORDER BY b.codigo_retencion_1;

ALTER TABLE public.v_rep_impuesto_retenido_proveedores_islr
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_rep_impuesto_retenido_proveedores_islr TO PUBLIC;
GRANT ALL ON TABLE public.v_rep_impuesto_retenido_proveedores_islr TO postgres;
