﻿-- View: public.v_radio_contratos_distribucion_facturas

-- DROP VIEW public.v_radio_contratos_distribucion_facturas;

CREATE OR REPLACE VIEW public.v_radio_contratos_distribucion_facturas AS
 SELECT a.id_radio_contrato,
    a.item,
    a.total_items,
    a.fecha_ingreso,
    a.total,
    a.fecha_inicial,
    a.fecha_final,
    a.observaciones,
    a.id_facturacion_cliente,
    a.numero_documento,
    true AS facturado
   FROM tbl_radio_contratos_facturas_generadas a
UNION
 SELECT a.id_radio_contrato,
    a.item,
    a.total_items,
    a.fecha_ingreso,
    a.total,
    a.fecha_inicial,
    a.fecha_final,
    a.observaciones,
    '-1'::integer AS id_facturacion_cliente,
    ''::character varying(20) AS numero_documento,
    false AS facturado
   FROM tbl_radio_contratos_facturas_pendientes a;

ALTER TABLE public.v_radio_contratos_distribucion_facturas
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_radio_contratos_distribucion_facturas TO PUBLIC;
GRANT ALL ON TABLE public.v_radio_contratos_distribucion_facturas TO postgres;
