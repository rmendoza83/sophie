﻿-- View: public.v_rep_resumen_debito_creditos

-- DROP VIEW public.v_rep_resumen_debito_creditos;

CREATE OR REPLACE VIEW public.v_rep_resumen_debito_creditos AS
 SELECT 'FC'::text AS desde,
    a.numero,
    a.fecha_ingreso,
    a.fecha_contable,
    a.fecha_libros,
    a.sub_total,
    a.impuesto,
    a.total,
    a.p_descuento,
    a.p_iva,
    b.monto_retencion_iva AS iva_retenido,
    a.impuesto - b.monto_retencion_iva AS iva_percibido
   FROM tbl_facturacion_clientes a
     LEFT JOIN tbl_det_liquidacion_cobranzas b ON a.id_cobranza = b.id_cobranza AND b.orden_cobranza = 1
     LEFT JOIN tbl_tipos_retenciones c ON b.codigo_retencion_iva::text = c.codigo::text
UNION
 SELECT 'FP'::text AS desde,
    a.numero,
    a.fecha_ingreso,
    a.fecha_contable,
    a.fecha_libros,
    a.sub_total,
    a.impuesto,
    a.total,
    a.p_descuento,
    a.p_iva,
    b.monto_retencion AS iva_retenido,
    a.impuesto - b.monto_retencion AS iva_percibido
   FROM tbl_facturacion_proveedores a
     LEFT JOIN tbl_det_retenciones_iva b ON a.numero::text = b.numero_documento::text AND b.desde = 'FP'::bpchar
     LEFT JOIN tbl_tipos_retenciones c ON b.codigo_retencion::text = c.codigo::text;

ALTER TABLE public.v_rep_resumen_debito_creditos
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_rep_resumen_debito_creditos TO PUBLIC;
GRANT ALL ON TABLE public.v_rep_resumen_debito_creditos TO postgres;
