﻿-- View: public.v_busqueda_retencion_islr

-- DROP VIEW public.v_busqueda_retencion_islr;

CREATE OR REPLACE VIEW public.v_busqueda_retencion_islr AS
 SELECT a.numero,
    a.codigo_proveedor,
    ((b.prefijo_rif::text || b.rif::text))::character varying(20) AS rif,
    b.nit,
    b.razon_social,
    b.contacto
   FROM tbl_retenciones_islr a
     LEFT JOIN tbl_proveedores b ON a.codigo_proveedor::text = b.codigo::text
  ORDER BY a.codigo_proveedor, a.numero;

ALTER TABLE public.v_busqueda_retencion_islr
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_busqueda_retencion_islr TO PUBLIC;
GRANT ALL ON TABLE public.v_busqueda_retencion_islr TO postgres;
