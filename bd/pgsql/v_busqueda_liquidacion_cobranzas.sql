﻿-- View: public.v_busqueda_liquidacion_cobranzas

-- DROP VIEW public.v_busqueda_liquidacion_cobranzas;

CREATE OR REPLACE VIEW public.v_busqueda_liquidacion_cobranzas AS
 SELECT a.numero,
    a.codigo_cliente,
    ((b.prefijo_rif::text || b.rif::text))::character varying(20) AS rif,
    b.nit,
    b.razon_social,
    b.contacto
   FROM tbl_liquidacion_cobranzas a
     LEFT JOIN tbl_clientes b ON a.codigo_cliente::text = b.codigo::text
  ORDER BY a.codigo_cliente, a.numero;

ALTER TABLE public.v_busqueda_liquidacion_cobranzas
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_busqueda_liquidacion_cobranzas TO PUBLIC;
GRANT ALL ON TABLE public.v_busqueda_liquidacion_cobranzas TO postgres;
