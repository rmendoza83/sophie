﻿-- View: public.v_radio_detalle_calculo_pago_comision

-- DROP VIEW public.v_radio_detalle_calculo_pago_comision;

CREATE OR REPLACE VIEW public.v_radio_detalle_calculo_pago_comision AS
 SELECT 'FC'::character varying(10) AS desde,
    a.numero,
    a.codigo_vendedor,
    a.fecha_ingreso,
    a.monto_neto
   FROM tbl_facturacion_clientes a
  WHERE NOT (a.numero::text IN ( SELECT tbl_det_pagos_comisiones.numero_documento
           FROM tbl_det_pagos_comisiones
          WHERE tbl_det_pagos_comisiones.desde::text = 'FC'::text));

ALTER TABLE public.v_radio_detalle_calculo_pago_comision
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_radio_detalle_calculo_pago_comision TO PUBLIC;
GRANT ALL ON TABLE public.v_radio_detalle_calculo_pago_comision TO postgres;
