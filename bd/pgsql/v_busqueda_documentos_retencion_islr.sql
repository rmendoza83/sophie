﻿-- View: public.v_busqueda_documentos_retencion_islr

-- DROP VIEW public.v_busqueda_documentos_retencion_islr;

CREATE OR REPLACE VIEW public.v_busqueda_documentos_retencion_islr AS
 SELECT 'FP'::character varying(10) AS desde,
    a.numero,
    a.codigo_proveedor,
    b.rif,
    b.nit,
    b.razon_social,
    b.contacto,
    a.monto_neto
   FROM tbl_facturacion_proveedores a
     JOIN tbl_proveedores b ON a.codigo_proveedor::text = b.codigo::text
  WHERE NOT ((a.codigo_proveedor::text || '-'::text) || a.numero::text IN ( SELECT (a_1.codigo_proveedor::text || '-'::text) || b_1.numero_documento::text AS codigo_unico
           FROM tbl_retenciones_islr a_1
             JOIN tbl_det_retenciones_islr b_1 ON a_1.numero::text = b_1.numero::text
          WHERE b_1.desde::text = 'FP'::text))
UNION
 SELECT 'NDP'::character varying(10) AS desde,
    a.numero,
    a.codigo_proveedor,
    b.rif,
    b.nit,
    b.razon_social,
    b.contacto,
    a.monto_neto
   FROM tbl_nota_debito_proveedores a
     JOIN tbl_proveedores b ON a.codigo_proveedor::text = b.codigo::text
  WHERE NOT ((a.codigo_proveedor::text || '-'::text) || a.numero::text IN ( SELECT (a_1.codigo_proveedor::text || '-'::text) || b_1.numero_documento::text AS codigo_unico
           FROM tbl_retenciones_islr a_1
             JOIN tbl_det_retenciones_islr b_1 ON a_1.numero::text = b_1.numero::text
          WHERE b_1.desde::text = 'NDP'::text))
UNION
 SELECT 'AP'::character varying(10) AS desde,
    a.numero,
    a.codigo_proveedor,
    b.rif,
    b.nit,
    b.razon_social,
    b.contacto,
    a.monto_neto
   FROM tbl_anticipo_proveedores a
     JOIN tbl_proveedores b ON a.codigo_proveedor::text = b.codigo::text
  WHERE NOT ((a.codigo_proveedor::text || '-'::text) || a.numero::text IN ( SELECT (a_1.codigo_proveedor::text || '-'::text) || b_1.numero_documento::text AS codigo_unico
           FROM tbl_retenciones_islr a_1
             JOIN tbl_det_retenciones_islr b_1 ON a_1.numero::text = b_1.numero::text
          WHERE b_1.desde::text = 'AP'::text));

ALTER TABLE public.v_busqueda_documentos_retencion_islr
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_busqueda_documentos_retencion_islr TO PUBLIC;
GRANT ALL ON TABLE public.v_busqueda_documentos_retencion_islr TO postgres;
