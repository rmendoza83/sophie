﻿-- View: public.v_rep_pago_comision_x_pagar

-- DROP VIEW public.v_rep_pago_comision_x_pagar;

CREATE OR REPLACE VIEW public.v_rep_pago_comision_x_pagar AS
 SELECT a.desde,
    a.codigo_vendedor AS cc_codigo_vendedor,
    a.codigo_zona AS cc_codigo_zona,
    a.fecha_ingreso AS cc_fecha_factura,
    a.fecha_cobranza AS cc_fecha_cobranza,
    b.numero AS fc_numero_factura,
    c.codigo_producto AS dfc_codigo_producto,
    c.descripcion AS dfc_descripcion_producto,
    c.cantidad AS dfc_cantidad,
    c.impuesto AS dfc_impuesto,
    c.total AS dfc_total,
    d.codigo AS v_codigo_vendedor,
    d.descripcion AS v_nombre,
    d.activo AS v_activo,
    d.codigo_zona AS v_codigo_zona_vendedor,
    f.codigo AS tc_codigo_comision,
    f.descripcion AS tc_nombre_comision,
    f.porcentaje AS tc_porcentaje_comision,
    f.codigo_division AS tc_codigo_division,
    f.codigo_linea AS tc_codigo_linea,
    f.codigo_familia AS tc_codigo_familia,
    f.codigo_clase AS tc_codigo_clase,
    f.codigo_manufactura AS tc_codigo_manufactura,
    f.comision_cantidad AS tc_comision_cantidad,
        CASE
            WHEN f.comision_cantidad = true THEN f.monto_cantidad
            ELSE c.precio * c.cantidad
        END::numeric(18,4) AS tc_factor_pago,
    h.descripcion AS z_nombre_zona,
        CASE
            WHEN f.codigo_division::text = g.codigo_division::text THEN c.cantidad * f.monto_cantidad
            WHEN f.comision_cantidad = false THEN c.precio * c.cantidad * (f.porcentaje / 100::numeric)
            ELSE 0::numeric
        END::numeric(18,4) AS monto_comision
   FROM tbl_cobranzas_cobradas a
     JOIN tbl_facturacion_clientes b ON a.id_cobranza = b.id_cobranza
     JOIN tbl_det_facturacion_clientes c ON b.id_facturacion_cliente = c.id_facturacion_cliente
     JOIN tbl_vendedores d ON b.codigo_vendedor::text = d.codigo::text
     JOIN tbl_vendedores_comisiones e ON d.codigo::text = e.codigo_vendedor::text
     JOIN tbl_tipos_comisiones f ON e.codigo_tipo_comision::text = f.codigo::text
     JOIN tbl_productos g ON c.codigo_producto::text = g.codigo::text
     JOIN tbl_zonas h ON a.codigo_zona::text = h.codigo::text
  WHERE a.desde::text = 'FC'::text;

ALTER TABLE public.v_rep_pago_comision_x_pagar
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_rep_pago_comision_x_pagar TO PUBLIC;
GRANT ALL ON TABLE public.v_rep_pago_comision_x_pagar TO postgres;
