﻿-- View: public.v_busqueda_facturacion_proveedores

-- DROP VIEW public.v_busqueda_facturacion_proveedores;

CREATE OR REPLACE VIEW public.v_busqueda_facturacion_proveedores AS
 SELECT a.numero,
    a.numero_pedido,
    a.codigo_proveedor,
    ((b.prefijo_rif::text || b.rif::text))::character varying(20) AS rif,
    b.nit,
    b.razon_social,
    b.contacto
   FROM tbl_facturacion_proveedores a
     LEFT JOIN tbl_proveedores b ON a.codigo_proveedor::text = b.codigo::text
  ORDER BY a.id_facturacion_proveedor;

ALTER TABLE public.v_busqueda_facturacion_proveedores
    OWNER TO postgres;

GRANT ALL ON TABLE public.v_busqueda_facturacion_proveedores TO PUBLIC;
GRANT ALL ON TABLE public.v_busqueda_facturacion_proveedores TO postgres;
