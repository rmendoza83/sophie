unit GenericoDetalle;

{$I sophie.inc}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls, ToolWin, DB, ImgList, XPMan, Grids, DBGrids,
  Utils, DBClient, ClsParametro, ImageList;

type
  TFrmGenericoDetalle = class(TForm)
    TemaXP: TXPManifest;
    Imagenes: TImageList;
    DS: TDataSource;
    BH: TToolBar;
    TBtnAgregar: TToolButton;
    TBtnEditar: TToolButton;
    TBtnEliminar: TToolButton;
    TBtnS1: TToolButton;
    TBtnGuardar: TToolButton;
    TBtnCancelar: TToolButton;
    TBtnS2: TToolButton;
    TBtnRefrescar: TToolButton;
    TBtnS3: TToolButton;
    TBtnImprimir: TToolButton;
    PanelMaestro: TPanel;
    TBtnBuscar: TToolButton;
    PageDetalles: TPageControl;
    TabDetalle: TTabSheet;
    GridDetalle: TDBGrid;
    Data: TClientDataSet;
    SB: TStatusBar;
    procedure FormCreate(Sender: TObject);
    procedure GridDetalleKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TBtnEliminarClick(Sender: TObject);
    procedure TBtnRefrescarClick(Sender: TObject);
    procedure TBtnImprimirClick(Sender: TObject);
    procedure TBtnGuardarClick(Sender: TObject);
    procedure TBtnEditarClick(Sender: TObject);
    procedure TBtnCancelarClick(Sender: TObject);
    procedure TBtnAgregarClick(Sender: TObject);
    procedure TBtnBuscarClick(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    ProcAgregar: TProcObject;
    ProcEditar: TProcObject;
    ProcEliminar: TProcObject;
    ProcGuardar: TProcObject;
    ProcCancelar: TProcObject;
    ProcRefrescar: TProcObject;
    ProcImprimir: TProcObject;
    ProcBuscar: TProcObject;
    Agregando: Boolean;
    Editando: Boolean;
    Eliminando: Boolean;
    Parametro: TParametro;
    procedure HabilitarEdicion(Value: Boolean);
    function Validar: Boolean; virtual; abstract;
    procedure Detalle; virtual; abstract;
  public
    { Public declarations }
  end;

var
  FrmGenericoDetalle: TFrmGenericoDetalle;

implementation

{$R *.dfm}

{ TFrmGenericoDetalle }

procedure TFrmGenericoDetalle.FormCreate(Sender: TObject);
begin
  ProcAgregar := nil;
  ProcEditar := nil;
  ProcEliminar := nil;
  ProcGuardar := nil;
  ProcCancelar := nil;
  ProcRefrescar := nil;
  ProcImprimir := nil;
  ProcBuscar := nil;
  PageDetalles.ActivePage := TabDetalle;
  GridDetalle.DataSource := DS;
  Parametro := TParametro.Create;
  //Asignando Decimales por Defecto
  Parametro.SetNombreParametro('numero_decimales');
  if (not Parametro.Buscar) then
  begin
    Parametro.SetCategoria('GENERAL');
    Parametro.SetTipoDato('INTEGER');
    Parametro.SetValor('2');
    Parametro.Insertar;
  end;
  BuscarDisplayFormat(Self,Parametro.ObtenerValorAsInteger);
end;

procedure TFrmGenericoDetalle.GridDetalleKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Agregando or Editando) then
  begin
    case Key of
      VK_DELETE:
      begin
        if (not Data.IsEmpty) then
        begin
          if (MessageBox(Self.Handle, '�Esta Seguro?', MsgTituloAdvertencia, MB_ICONWARNING or MB_YESNO or MB_TOPMOST or MB_TASKMODAL) = mrYes) then
          begin
            Data.Delete;
          end;
        end;
      end;
      VK_INSERT:
      begin
        Detalle;
        Abort;
      end;
    end;
  end;
end;

procedure TFrmGenericoDetalle.HabilitarEdicion(Value: Boolean);
begin
  TBtnAgregar.Enabled := not Value;
  TBtnEditar.Enabled := not Value;
  TBtnEliminar.Enabled := not Value;
  TBtnRefrescar.Enabled := not Value;
  TBtnImprimir.Enabled := not Value;
  TBtnBuscar.Enabled := not Value;
  TBtnGuardar.Enabled := Value;
  TBtnCancelar.Enabled := Value;
  PanelMaestro.Enabled := Value;
  PageDetalles.Enabled := Value;
end;

procedure TFrmGenericoDetalle.TBtnAgregarClick(Sender: TObject);
begin
  if (Assigned(ProcAgregar)) then
  begin
    Agregando := True;
    Editando := False;
    Eliminando := False;
    HabilitarEdicion(True);
    ProcAgregar;
  end;
end;

procedure TFrmGenericoDetalle.TBtnBuscarClick(Sender: TObject);
begin
  if (Assigned(ProcBuscar)) then
  begin
    ProcBuscar;
  end;
end;

procedure TFrmGenericoDetalle.TBtnCancelarClick(Sender: TObject);
begin
  if (MessageBox(Self.Handle, '�Esta Seguro Que Desea Cancelar?', PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_YESNO or MB_TOPMOST or MB_TASKMODAL) = mrYes) then
  begin
    if (Assigned(ProcCancelar)) then
    begin
      ProcCancelar;
      Agregando := False;
      Editando := False;
      Eliminando := False;
      HabilitarEdicion(False);
    end;
  end;
end;

procedure TFrmGenericoDetalle.TBtnEditarClick(Sender: TObject);
begin
  if (Assigned(ProcEditar)) then
  begin
    ProcEditar;
    Agregando := False;
    Editando := True;
    Eliminando := False;
    HabilitarEdicion(True);
  end;
end;

procedure TFrmGenericoDetalle.TBtnEliminarClick(Sender: TObject);
begin
  if (MessageBox(Self.Handle, '�Esta Seguro Que Desea Eliminar?', PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_YESNO or MB_TOPMOST or MB_TASKMODAL) = mrYes) then
  begin
    if (Assigned(ProcEliminar)) then
    begin
      Agregando := False;
      Editando := False;
      Eliminando := True;
      ProcEliminar;
      Eliminando := False;
      ProcRefrescar;
    end;
  end;
end;

procedure TFrmGenericoDetalle.TBtnGuardarClick(Sender: TObject);
begin
  if (Validar) then
  begin
    if (MessageBox(Self.Handle, '�Esta Seguro Que Los Datos Estan Correctos?', PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_YESNO or MB_TOPMOST or MB_TASKMODAL) = mrYes) then
    begin
      if (Assigned(ProcGuardar)) then
      begin
        ProcGuardar;
        Agregando := False;
        Editando := False;
        Eliminando := False;
        HabilitarEdicion(False);
        ProcRefrescar;
      end;
    end;
  end;
end;

procedure TFrmGenericoDetalle.TBtnImprimirClick(Sender: TObject);
begin
  if (Assigned(ProcImprimir)) then
  begin
    ProcImprimir;
  end;
end;

procedure TFrmGenericoDetalle.TBtnRefrescarClick(Sender: TObject);
begin
  if ((not Agregando) and (not Editando) and (not Eliminando)) then
  begin
    if (Assigned(ProcRefrescar)) then
    begin
      ProcRefrescar;
    end;
  end;
end;

end.
