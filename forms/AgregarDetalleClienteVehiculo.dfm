inherited FrmAgregarDetalleClienteVehiculo: TFrmAgregarDetalleClienteVehiculo
  ClientHeight = 306
  ExplicitHeight = 330
  PixelsPerInch = 96
  TextHeight = 13
  inherited LblCodigo: TLabel
    Visible = False
  end
  object LblPlaca: TLabel [1]
    Left = 59
    Top = 30
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'Placa:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object LblMarca: TLabel [2]
    Left = 54
    Top = 54
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'Marca:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object LblModelo: TLabel [3]
    Left = 48
    Top = 78
    Width = 44
    Height = 13
    Alignment = taRightJustify
    Caption = 'Modelo:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object LblAnno: TLabel [4]
    Left = 67
    Top = 102
    Width = 25
    Height = 13
    Alignment = taRightJustify
    Caption = 'A'#241'o:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object LblColor: TLabel [5]
    Left = 60
    Top = 126
    Width = 32
    Height = 13
    Alignment = taRightJustify
    Caption = 'Color:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object LblKilometraje: TLabel [6]
    Left = 23
    Top = 149
    Width = 69
    Height = 13
    Alignment = taRightJustify
    Caption = 'Kilometraje:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object LblObservaciones: TLabel [7]
    Left = 6
    Top = 174
    Width = 86
    Height = 13
    Alignment = taRightJustify
    Caption = 'Observaciones:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  inherited TxtCodigo: TEdit
    Visible = False
  end
  inherited PanelBotones: TPanel
    Top = 264
    Height = 42
    TabOrder = 9
    ExplicitTop = 264
    ExplicitHeight = 42
    inherited Aceptar: TBitBtn
      ModalResult = 0
    end
  end
  inherited BtnBuscar: TBitBtn
    TabOrder = 1
    Visible = False
  end
  object TxtPlaca: TEdit
    Left = 98
    Top = 27
    Width = 150
    Height = 21
    MaxLength = 10
    TabOrder = 2
  end
  object TxtAnno: TCurrencyEdit
    Left = 98
    Top = 99
    Width = 149
    Height = 21
    Margins.Left = 4
    Margins.Top = 1
    DecimalPlaces = 0
    DisplayFormat = '#,##0'
    MaxLength = 5
    TabOrder = 5
  end
  object TxtKilometraje: TCurrencyEdit
    Left = 98
    Top = 147
    Width = 149
    Height = 21
    Margins.Left = 4
    Margins.Top = 1
    DecimalPlaces = 0
    DisplayFormat = '#,##0'
    MaxLength = 10
    TabOrder = 7
  end
  object TxtObservaciones: TMemo
    Left = 98
    Top = 171
    Width = 238
    Height = 89
    MaxLength = 256
    ScrollBars = ssVertical
    TabOrder = 8
  end
  object TxtMarca: TEdit
    Left = 98
    Top = 51
    Width = 150
    Height = 21
    MaxLength = 20
    TabOrder = 3
  end
  object TxtModelo: TEdit
    Left = 98
    Top = 75
    Width = 150
    Height = 21
    MaxLength = 20
    TabOrder = 4
  end
  object TxtColor: TEdit
    Left = 98
    Top = 123
    Width = 150
    Height = 21
    MaxLength = 20
    TabOrder = 6
  end
end
