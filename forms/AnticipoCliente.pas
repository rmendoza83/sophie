unit AnticipoCliente;

{$I sophie.inc}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GenericoDetalle, DB, DBClient, ImgList, XPMan, ComCtrls, Grids,
  DBGrids, Utils, ExtCtrls, ToolWin, rxCurrEdit, Mask, rxToolEdit, StdCtrls,
  Buttons, RxLookup, ClsAnticipoCliente, ClsBusquedaAnticipoCliente, ClsCliente,
  ClsZona, ClsMoneda, ClsCuentaxCobrar, GenericoBuscador, ClsReporte,
  ClsParametro, ClsFormaPago, ClsOperacionBancaria, ClsBancoCobranza,
  ClsCuentaBancaria, ClsCobranzaCobrada, ClsDocumentoAnulado,
  ClsDetalleContadoCobranza{$IFDEF DELPHI10}, ImageList{$ENDIF};

type
  TFrmAnticipoCliente = class(TFrmGenericoDetalle)
    LblNumero: TLabel;
    TxtNumero: TEdit;
    LblCodigoCliente: TLabel;
    CmbCodigoCliente: TRxDBLookupCombo;
    BtnBuscarCliente: TBitBtn;
    LblCodigoMoneda: TLabel;
    CmbCodigoMoneda: TRxDBLookupCombo;
    BtnBuscarMoneda: TBitBtn;
    LblCodigoZona: TLabel;
    CmbCodigoZona: TRxDBLookupCombo;
    BtnBuscarZona: TBitBtn;
    DatFechaIngreso: TDateEdit;
    LblFechaIngreso: TLabel;
    LblRecepcion: TLabel;
    LblFechaContable: TLabel;
    LblFechaLibros: TLabel;
    DatFechaLibros: TDateEdit;
    DatFechaContable: TDateEdit;
    DatFechaRecepcion: TDateEdit;
    LblMontoNeto: TLabel;
    TxtMontoNeto: TCurrencyEdit;
    TabConcepto: TTabSheet;
    TxtConcepto: TRichEdit;
    DSCliente: TDataSource;
    DSZona: TDataSource;
    DSMoneda: TDataSource;
    TabObservaciones: TTabSheet;
    TxtObservaciones: TRichEdit;
    LblFormaPago: TLabel;
    CmbCodigoFormaPago: TRxDBLookupCombo;
    BtnBuscarFormaPago: TBitBtn;
    LblCuentaBancaria: TLabel;
    CmbCodigoCuentaBancaria: TRxDBLookupCombo;
    LblNumeroDocumento: TLabel;
    TxtNumeroDocumentoBancario: TEdit;
    CmbBancoCobranza: TRxDBLookupCombo;
    LblBancoCobranza: TLabel;
    DSFormaPago: TDataSource;
    BtnBuscarCuentaBancaria: TBitBtn;
    DSCuentaBancaria: TDataSource;
    DatFechaOperacionBancaria: TDateEdit;
    LblFechaOperacionBancaria: TLabel;
    BtnBancoCobro: TBitBtn;
    DSBancoCobro: TDataSource;
    TxtNumeroFormaPago: TEdit;
    LblNumeroFormaPago: TLabel;
    procedure BtnBancoCobroClick(Sender: TObject);
    procedure BtnBuscarCuentaBancariaClick(Sender: TObject);
    procedure BtnBuscarFormaPagoClick(Sender: TObject);
    procedure TBtnEliminarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CmbCodigoClienteExit(Sender: TObject);
    procedure CmbCodigoClienteChange(Sender: TObject);
    procedure BtnBuscarMonedaClick(Sender: TObject);
    procedure BtnBuscarZonaClick(Sender: TObject);
    procedure BtnBuscarClienteClick(Sender: TObject);
  private
    { Private declarations }
    Clase: TAnticipoCliente;
    CuentaxCobrar: TCuentaxCobrar;
    CobranzaCobrada: TCobranzaCobrada;
    BuscadorAnticipoCliente: TBusquedaAnticipoCliente;
    Cliente: TCliente;
    Zona: TZona;
    Moneda: TMoneda;
    FormaPago: TFormaPago;
    OperacionBancaria: TOperacionBancaria;
    CuentaBancaria: TCuentaBancaria;
    DetalleContadoCobranza: TDetalleContadoCobranza;
    Reporte: TReporte;
    Parametro: TParametro;
    DocumentoAnulado: TDocumentoAnulado;
    DataBuscadorAnticipoCliente: TClientDataSet;
    DataCliente: TClientDataSet;
    DataZona: TClientDataSet;
    DataMoneda: TClientDataSet;
    DataFormaPago: TClientDataSet;
    DataCuentaBancaria: TClientDataSet;
    DataBancoCobro: TClientDataSet;
    procedure Agregar;
    procedure Editar;
    procedure Eliminar;
    procedure Guardar;
    procedure Cancelar;
    procedure Refrescar;
    procedure Imprimir;
    procedure Buscar;
    procedure Limpiar;
    procedure Asignar;
    procedure Obtener;
    procedure Mover;
    procedure GuardarCuentaxCobrar;
    procedure GuardarOperacionesBancarias;
    procedure GuardarContadoCobranza;
    //procedure GuardarCobranzaCobrada;
  protected
    { Protected declarations }
    function Validar: Boolean; override;
  public
    { Public declarations }
  end;

var
  FrmAnticipoCliente: TFrmAnticipoCliente;

implementation

uses
  ActividadSophie,
  PModule,
  DetalleContadoCobranza
  {$IFDEF USE_RAVE_REPORTS}
  , DMRR
  {$ENDIF}
  {$IFDEF USE_FAST_REPORTS}
  , DMFR
  {$ENDIF}
  ;

{$R *.dfm}

{ TFrmAnticipoCliente }

procedure TFrmAnticipoCliente.Agregar;
begin
  //Verificando
  Limpiar;
  //Asignando Valores por Defecto
  CmbCodigoMoneda.Value := Parametro.ObtenerValor('codigo_moneda');
  CmbCodigoZona.Value := Parametro.ObtenerValor('codigo_zona');
  CmbCodigoCliente.SetFocus;
end;

procedure TFrmAnticipoCliente.Asignar;
begin
  with Clase do
  begin
    TxtNumero.Text := GetNumero;
    CmbCodigoCliente.KeyValue := GetCodigoCliente;
    CmbCodigoZona.KeyValue := GetCodigoZona;
    CmbCodigoMoneda.KeyValue := GetCodigoMoneda;
    DatFechaIngreso.Date := GetFechaIngreso;
    DatFechaRecepcion.Date := GetFechaRecepcion;
    DatFechaContable.Date := GetFechaContable;
    DatFechaLibros.Date := GetFechaLibros;
    TxtConcepto.Text := GetConcepto;
    TxtObservaciones.Text := GetObservaciones;
    TxtMontoNeto.Value := GetMontoNeto;
    CmbCodigoFormaPago.KeyValue := GetCodigoFormaPago;
    TxtNumeroFormaPago.Text := GetNumeroFormaPago;
    CmbCodigoCuentaBancaria.KeyValue := GetCodigoCuentaBancaria;
    TxtNumeroDocumentoBancario.Text := GetNumeroDocumentoBancario;
    CmbBancoCobranza.KeyValue := GetCodigoBancoCobranza;
    DatFechaOperacionBancaria.Date := GetFechaOperacionBancaria;
    DS.DataSet := Data;
  end;
end;

procedure TFrmAnticipoCliente.BtnBancoCobroClick(Sender: TObject);
begin
  Buscador(Self,BancoCobranza,'codigo','Buscar Banco de Cobro',nil,DataBancoCobro);
  CmbBancoCobranza.KeyValue := DataBancoCobro.FieldValues['codigo'];
end;

procedure TFrmAnticipoCliente.BtnBuscarClienteClick(Sender: TObject);
begin
  Buscador(Self,Cliente,'codigo','Buscar Cliente',nil,DataCliente);
  CmbCodigoCliente.KeyValue := DataCliente.FieldValues['codigo'];
  CmbCodigoClienteExit(Sender);
end;

procedure TFrmAnticipoCliente.BtnBuscarFormaPagoClick(Sender: TObject);
begin
  Buscador(Self,FormaPago,'codigo','Buscar Forma de Pago',nil,DataFormaPago);
  CmbCodigoFormaPago.KeyValue := DataFormaPago.FieldValues['codigo'];
end;

procedure TFrmAnticipoCliente.BtnBuscarMonedaClick(Sender: TObject);
begin
  Buscador(Self,Moneda,'codigo','Buscar Moneda',nil,DataMoneda);
  CmbCodigoMoneda.KeyValue := DataMoneda.FieldValues['codigo'];
end;

procedure TFrmAnticipoCliente.BtnBuscarZonaClick(Sender: TObject);
begin
  Buscador(Self,Zona,'codigo','Buscar Zona',nil,DataZona);
  CmbCodigoZona.KeyValue := DataZona.FieldValues['codigo'];
end;

procedure TFrmAnticipoCliente.BtnBuscarCuentaBancariaClick(Sender: TObject);
begin
  Buscador(Self,CuentaBancaria,'codigo','Buscar Cuenta Bancaria',nil,DataCuentaBancaria);
  CmbCodigoCuentaBancaria.KeyValue := DataCuentaBancaria.FieldValues['codigo'];
end;

procedure TFrmAnticipoCliente.Buscar;
begin
  if (Buscador(Self,BuscadorAnticipoCliente,'numero','Buscar Anticipo de Cliente',nil,DataBuscadorAnticipoCliente)) then
  begin
    Clase.SetNumero(DataBuscadorAnticipoCliente.FieldValues['numero']);
    Clase.BuscarNumero;
    Asignar;
  end;
end;

procedure TFrmAnticipoCliente.Cancelar;
begin
  if (Agregando) then
  begin
    Limpiar;
  end
  else
  begin
    Clase.SetNumero(TxtNumero.Text);
    Clase.BuscarNumero;
    Asignar;
  end;
end;

procedure TFrmAnticipoCliente.CmbCodigoClienteChange(Sender: TObject);
begin
  inherited;
  SB.Panels[0].Text := DataCliente.FieldValues['razon_social'];
end;

procedure TFrmAnticipoCliente.CmbCodigoClienteExit(Sender: TObject);
begin
  if (Length(Trim(CmbCodigoCliente.Text)) > 0) then
  begin
    Cliente.SetCodigo(CmbCodigoCliente.Value);
    Cliente.BuscarCodigo;
    CmbCodigoZona.Value := Cliente.GetCodigoZona;
  end;
end;

procedure TFrmAnticipoCliente.Editar;
begin
  if (Length(Trim(TxtNumero.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
end;

procedure TFrmAnticipoCliente.Eliminar;
var
  sw: Boolean;
begin
  //Se Valida Que La Anticipo de Cliente Pueda Ser Anulado
  sw := False;
  if (Clase.GetIdCobranza <> -1) then //No Puede Anularse
  begin
    CobranzaCobrada.SetIdCobranza(clase.GetIdCobranza);
    if (CobranzaCobrada.BuscarIdCobranza) then
    begin
      sw := True;
    end;
  end;
  //Consultando Si Puede Eliminarse El Anticipo de Cliente
  if (sw = False) then
  begin
    //Se Busca El Anticipo!
    Clase.SetNumero(TxtNumero.Text);
    Clase.BuscarNumero;
    //Verificando Si Se Elimina La Operacion Bancaria
    if (Clase.GetIdOperacionBancaria <> -1) then
    begin
      if (MessageBox(Self.Handle, 'Este Anticipo Tiene una Operaci�n Bancaria Asociada y Ser� Eliminada!!!'+#13+#10+''+#13+#10+'�Esta Seguro que Desea Continuar?' , PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_YESNO or MB_TOPMOST or MB_TASKMODAL) = mrYes) then
      begin
        OperacionBancaria.SetIdOperacionBancaria(Clase.GetIdOperacionBancaria);
        OperacionBancaria.BuscarIdOperacionBancaria;
        OperacionBancaria.Eliminar;
        ///////////////////////////////
        // Procesando Eliminacion!!! //
        ///////////////////////////////
        //Se Eliminan Los Detalles De la Cobranza
        CuentaxCobrar.EliminarIdCobranza(Clase.GetIdCobranza);
        //Registrando El Documento Anulado
        with DocumentoAnulado do
        begin
          SetDesde('AC');
          SetNumeroDocumento(Clase.GetNumero);
          SetSerie('');
          SetNumeroControl('');
          SetFechaDocumento(Clase.GetFechaIngreso);
          SetCodigoCliente(Clase.GetCodigoCliente);
          SetCodigoProveedor('');
          SetCodigoZona(Clase.GetCodigoZona);
          SetSubTotal(Clase.GetMontoNeto);
          SetImpuesto(0);
          SetTotal(Clase.GetMontoNeto);
          SetPIva(0);
          SetLoginUsuario(P.User.GetLoginUsuario);
          SetFecha(Date);
          SetHora(Time);
          Insertar;
        end;
        Clase.Eliminar;
        MessageBox(Self.Handle, 'El Anticipo de Cliente Se Ha Eliminado Con Exito!!!',PChar(MsgTituloInformacion), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
        Refrescar;
        Limpiar;
      end
      else
      begin
        Abort;
      end;
    end
    else
    begin
      ///////////////////////////////
      // Procesando Eliminacion!!! //
      ///////////////////////////////
      //Se Eliminan Los Detalles De la Cobranza
      CuentaxCobrar.EliminarIdCobranza(Clase.GetIdCobranza);
      //Registrando El Documento Anulado
      with DocumentoAnulado do
      begin
        SetDesde('AC');
        SetNumeroDocumento(Clase.GetNumero);
        SetSerie('');
        SetNumeroControl('');
        SetFechaDocumento(Clase.GetFechaIngreso);
        SetCodigoCliente(Clase.GetCodigoCliente);
        SetCodigoProveedor('');
        SetCodigoZona(Clase.GetCodigoZona);
        SetSubTotal(Clase.GetMontoNeto);
        SetImpuesto(0);
        SetTotal(Clase.GetMontoNeto);
        SetPIva(0);
        SetLoginUsuario(P.User.GetLoginUsuario);
        SetFecha(Date);
        SetHora(Time);
        Insertar;
      end;
      Clase.Eliminar;
      MessageBox(Self.Handle, 'El Anticipo de Cliente Se Ha Eliminado Con Exito!!!',PChar(MsgTituloInformacion), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      Refrescar;
      Limpiar;
    end;
  end
  else
  begin
    MessageBox(Self.Handle, 'El Anticipo de Cliente No Puede Ser Eliminado!!!'+#13+#10+''+#13+#10+'Para Poder Eliminar Un Anticipo de Cliente Se Debe Hacer Desde La Ultima Liquidaci�n Generada Asociada A Este Anticipo!', PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_APPLMODAL);
    Abort;
  end;
end;

procedure TFrmAnticipoCliente.FormCreate(Sender: TObject);
begin
  inherited;
  ProcAgregar := Agregar;
  ProcEditar := Editar;
  ProcEliminar := Eliminar;
  ProcGuardar := Guardar;
  ProcCancelar := Cancelar;
  ProcRefrescar := Refrescar;
  ProcImprimir := Imprimir;
  ProcBuscar := Buscar;
  FrmActividadSophie.Show;
  FrmActividadSophie.Actividad('Cargando N�cleo...');
  Clase := TAnticipoCliente.Create;
  //Inicializando DataSets Secundarios
  BuscadorAnticipoCliente := TBusquedaAnticipoCliente.Create;
  Cliente := TCliente.Create;
  Moneda := TMoneda.Create;
  Zona := TZona.Create;
  FormaPago := TFormaPago.Create;
  OperacionBancaria := TOperacionBancaria.Create;
  CuentaBancaria := TCuentaBancaria.Create;
  BancoCobranza := TBancoCobranza.Create;
  Reporte := TReporte.Create;
  Parametro := TParametro.Create;
  DocumentoAnulado := TDocumentoAnulado.Create;
  CuentaxCobrar := TCuentaxCobrar.Create;
  CobranzaCobrada := TCobranzaCobrada.Create;
  DetalleContadoCobranza := TDetalleContadoCobranza.Create;
  FrmActividadSophie.Actividad('Cargando Clientes...');
  DataCliente := Cliente.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Monedas...');
  DataMoneda := Moneda.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Zonas...');
  DataZona := Zona.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Forma de Pagos...');
  DataFormaPago := FormaPago.ObtenerComboCobranza;
  FrmActividadSophie.Actividad('Cargando Cuentas Bancarias...');
  DataCuentaBancaria := CuentaBancaria.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Bancos de Cobranzas...');
  DataBancoCobro := BancoCobranza.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Resto del N�cleo...');
  DataBuscadorAnticipoCliente := BuscadorAnticipoCliente.ObtenerLista;
  DataCliente.First;
  DataZona.First;
  DataMoneda.First;
  DataFormaPago.First;
  DataCuentaBancaria.First;
  DataBancoCobro.First;
  DataBuscadorAnticipoCliente.First;
  DSCliente.DataSet := DataCliente;
  DSZona.DataSet := DataZona;
  DSMoneda.DataSet := DataMoneda;
  DSFormaPago.DataSet := DataFormaPago;
  DSCuentaBancaria.DataSet := DataCuentaBancaria;
  DSBancoCobro.DataSet := DataBancoCobro;
  ConfigurarRxDBLookupCombo(CmbCodigoCliente,DSCliente,'codigo','codigo;razon_social');
  ConfigurarRxDBLookupCombo(CmbCodigoZona,DSZona,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoMoneda,DSMoneda,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoFormaPago,DSFormaPago,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoCuentaBancaria,DSCuentaBancaria,'codigo','codigo;entidad');
  ConfigurarRxDBLookupCombo(CmbBancoCobranza,DSBancoCobro,'codigo','codigo;descripcion');
  DS.DataSet := Data;
  GridDetalle.DataSource := DS;
  FrmActividadSophie.Close;
  TabDetalle.TabVisible := False;
  PageDetalles.ActivePage := TabConcepto;
  //Buscar;
end;

procedure TFrmAnticipoCliente.Guardar;
begin
  Obtener;
  if (Agregando) then
  begin
    //Agregando...
    //Guardando Encabezado
    Clase.Insertar;
    TxtNumero.Text := Clase.GetNumero;
    //Guardando Cuenta x Cobrar
    GuardarCuentaxCobrar;
    //Guardando Detalle de Contado
    GuardarContadoCobranza;
    //Guardando Operaciones Bancarias
    GuardarOperacionesBancarias;
    Application.ProcessMessages;
    MessageBox(Self.Handle, 'El Anticipo de Cliente Ha Sido Agregado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    //Se Imprime El Anticipo
    MessageBox(Self.Handle, 'Presione "Aceptar" Para Proceder A Imprimir El Anticipo de Cliente!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    with Reporte do
    begin
      AgregarParametro('NumeroAnticipoCliente',Clase.GetNumero);
      AgregarParametro('MontoLetra',NumeroALetras(Clase.GetMontoNeto));
      begin
        ImprimeReporte('AnticipoCliente',trDocumento);
      end;
    end;
  end
  else
  begin
    //Editando...
    Clase.Modificar;
    //Guardando Cuenta x Cobrar
    GuardarCuentaxCobrar;
    //Guardando Detalle de Contado
    GuardarContadoCobranza;
    //Guardando Operaciones Bancarias
    GuardarOperacionesBancarias;
    MessageBox(Self.Handle, 'El Anticipo de Cliente Ha Sido Editado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end;
end;

procedure TFrmAnticipoCliente.GuardarContadoCobranza;
begin
  if (Editando) then
  begin
    DetalleContadoCobranza.EliminarIdContadoCobranza(Clase.GetIdContadoCobranza);
  end;
  with DetalleContadoCobranza	do
  begin
    SetIdContadoCobranza(Clase.GetIdContadoCobranza);
    SetCodigoFormaPago(Clase.GetCodigoFormaPago);
    SetMonto(Clase.GetMontoNeto);
    SetDesde('AC');
    SetCodigoBanco(Clase.GetCodigoBancoCobranza);
    SetNumeroDocumento(Clase.GetNumeroFormaPago);
    SetNumeroAuxiliar(Clase.GetNumero);
    SetReferencia('');
    SetFecha(Clase.GetFechaIngreso);
    SetLoginUsuario(Clase.GetLoginUsuario);
    Insertar;
  end;
end;

procedure TFrmAnticipoCliente.GuardarCuentaxCobrar;
begin
  if (Editando) then
  begin
    CuentaxCobrar.EliminarIdCobranza(Clase.GetIdCobranza);
  end;
  with CuentaxCobrar do //Se Inserta la CuentaxCobrar
  begin
    SetDesde('AC');
    SetNumeroDocumento(Clase.GetNumero);
    SetCodigoMoneda(Clase.GetCodigoMoneda);
    SetCodigoCliente(Clase.GetCodigoCliente);
    SetCodigoVendedor('');
    SetCodigoZona(Clase.GetCodigoZona);
    SetFechaIngreso(Clase.GetFechaIngreso);
    SetFechaVencIngreso(Clase.GetFechaIngreso);
    SetFechaRecepcion(Clase.GetFechaRecepcion);
    SetFechaVencRecepcion(Clase.GetFechaRecepcion);
    SetFechaContable(Clase.GetFechaContable);
    SetFechaCobranza(clase.GetFechaIngreso);
    SetFechaLibros(Clase.GetFechaLibros);
    SetNumeroCuota(-1);
    SetTotalCuotas(-1);
    SetOrdenCobranza(1);
    SetMontoNeto(Clase.GetMontoNeto);
    SetImpuesto(0);
    SetTotal(Clase.GetMontoNeto);
    SetPIva(0);
    SetIdCobranza(Clase.GetIdCobranza);
    SetIdContadoCobranza(Clase.GetIdContadoCobranza);
    Insertar;
  end;
end;

procedure TFrmAnticipoCliente.GuardarOperacionesBancarias;
begin
  if (DataFormaPago['operacion_bancaria']) then
  begin
    with OperacionBancaria do
    begin
      SetCodigoCuenta(CmbCodigoCuentaBancaria.Text);
      SetCodigoTipoOperacion(DataFormaPago['codigo_tipo_operacion_bancaria']);
      SetNumero(TxtNumeroDocumentoBancario.Text);
      SetFecha(DatFechaOperacionBancaria.Date);
      SetFechaContable(DatFechaOperacionBancaria.Date);
      SetMonto(Clase.GetMontoNeto);
      SetConcepto('Anticipo de Cliente Generado N� ' + Clase.GetNumero);
      SetObservaciones('');
      SetDiferido(False);
      SetConciliado(False);
      SetFechaConciliacion(Date);
      SetBeneficiario('');
      SetLoginUsuario(P.User.GetLoginUsuario);
      Insertar;
    end;
    //Actualizando IdOperacionBancaria en Anticipo
    with Clase do
    begin
      SetIdOperacionBancaria(OperacionBancaria.GetIdOperacionBancaria);
      Modificar;
    end;
  end;
end;

procedure TFrmAnticipoCliente.Imprimir;
begin
  if (Length(Trim(TxtNumero.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
  with Reporte do
  begin
    AgregarParametro('NumeroAnticipoCliente',Clase.GetNumero);
    AgregarParametro('MontoLetra',NumeroALetras(Clase.GetMontoNeto));
    EmiteReporte('AnticipoCliente',trDocumento);
  end;
end;

procedure TFrmAnticipoCliente.Limpiar;
begin
  TxtNumero.Text := '';
  CmbCodigoCliente.KeyValue := '';
  CmbCodigoZona.KeyValue := '';
  CmbCodigoMoneda.KeyValue := '';
  DatFechaIngreso.Date := Date;
  DatFechaRecepcion.Date := Date;
  DatFechaContable.Date := Date;
  DatFechaLibros.Date := Date;
  TxtConcepto.Text := '';
  TxtObservaciones.Text := '';
  TxtMontoNeto.Value := 0;
  CmbCodigoFormaPago.KeyValue := '';
  TxtNumeroFormaPago.Text := '';
  CmbCodigoCuentaBancaria.KeyValue := '';
  TxtNumeroDocumentoBancario.Text := '';
  CmbBancoCobranza.KeyValue := '';
  DatFechaOperacionBancaria.Date := Date;
  //Status Bar
  SB.Panels[0].Text := '';
end;

procedure TFrmAnticipoCliente.Mover;
begin

end;

procedure TFrmAnticipoCliente.Obtener;
begin
  with Clase do
  begin
    SetNumero(TxtNumero.Text);
    SetCodigoCliente(CmbCodigoCliente.KeyValue);
    SetCodigoZona(CmbCodigoZona.KeyValue);
    SetCodigoMoneda(CmbCodigoMoneda.KeyValue);
    SetFechaIngreso(DatFechaIngreso.Date);
    SetFechaRecepcion(DatFechaRecepcion.Date);
    SetFechaContable(DatFechaContable.Date);
    SetFechaLibros(DatFechaLibros.Date);
    SetConcepto(TxtConcepto.Text);
    SetObservaciones(TxtObservaciones.Text);
    SetMontoNeto(TxtMontoNeto.Value);
    SetLoginUsuario(P.User.GetLoginUsuario);
    SetCodigoFormaPago(CmbCodigoFormaPago.Text);
    SetNumeroFormaPago(TxtNumeroFormaPago.Text);
    SetCodigoCuentaBancaria(CmbCodigoCuentaBancaria.Text);
    SetNumeroDocumentoBancario(TxtNumeroDocumentoBancario.Text);
    SetCodigoBancoCobranza(CmbBancoCobranza.Text);
    SetFechaOperacionBancaria(DatFechaOperacionBancaria.Date);
    if (Agregando) then
    begin
      SetTiempoIngreso(Now);
      SetIdAnticipoCliente(-1);
      SetIdCobranza(-1);
      SetIdContadoCobranza(-1);
      SetIdOperacionBancaria(-1);
    end;
  end;
end;

procedure TFrmAnticipoCliente.Refrescar;
begin
  //Se Actualiza El DataBuscador
  DataBuscadorAnticipoCliente := BuscadorAnticipoCliente.ObtenerLista;
  //Se Actualizan Los demas Componentes
  DataCliente := Cliente.ObtenerCombo;
  DataMoneda := Moneda.ObtenerCombo;
  DataZona := Zona.ObtenerCombo;
  DataFormaPago := FormaPago.ObtenerComboCobranza;
  DataCuentaBancaria := CuentaBancaria.ObtenerCombo;
  DataBancoCobro := BancoCobranza.ObtenerCombo;
  DataCliente.First;
  DataZona.First;
  DataMoneda.First;
  DataFormaPago.First;
  DataCuentaBancaria.First;
  DataBancoCobro.First;
  DataBuscadorAnticipoCliente.First;
  DSCliente.DataSet := DataCliente;
  DSZona.DataSet := DataZona;
  DSMoneda.DataSet := DataMoneda;
  DSFormaPago.DataSet := DataFormaPago;
  DSCuentaBancaria.DataSet := DataCuentaBancaria;
  DSBancoCobro.DataSet := DataBancoCobro;
end;

procedure TFrmAnticipoCliente.TBtnEliminarClick(Sender: TObject);
begin
  if (Length(Trim(TxtNumero.Text)) > 0) then
  begin
    inherited;
  end
  else
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
end;

function TFrmAnticipoCliente.Validar: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  //Validando Cliente
  if (Length(Trim(CmbCodigoCliente.Text)) = 0) then
  begin
    ListaErrores.Add('El Cliente No Puede Estar En Blanco!!!');
  end;
  //Validando Fechas
  if ((DatFechaIngreso.Date > DatFechaRecepcion.Date) or (DatFechaIngreso.Date > DatFechaContable.Date) or (DatFechaIngreso.Date > DatFechaLibros.Date)) then
  begin
    ListaErrores.Add('La Fecha de Ingreso Debe Ser Menor a las Fechas de Recepcion, Contable y Libros Respectivamente!!!');
  end;
  //Validando Forma de Pago
  if (Length(Trim(CmbCodigoFormaPago.Text)) = 0) then
  begin
    ListaErrores.Add('La Forma de Pago No Puede Estar En Blanco!!!');
  end;
  //Validando Seleccion de Forma de Pago
  if (DataFormaPago.FieldValues['operacion_bancaria']) then
  begin
    if (Length(Trim(CmbCodigoCuentaBancaria.Text)) = 0) then
    begin
      ListaErrores.Add('La Cuenta Bancaria No Puede Estar En Blanco!!!');
    end;
    if (Length(Trim(TxtNumeroDocumentoBancario.Text)) = 0) then
    begin
      ListaErrores.Add('El Numero de Documento No Puede Estar En Blanco!!!');
    end;
  end
  else
  begin
    if (DataFormaPago.FieldValues['anticipo'] or
        DataFormaPago.FieldValues['nc'] or
        DataFormaPago.FieldValues['nd']) then
    begin
      if (Length(Trim(TxtNumeroDocumentoBancario.Text)) = 0) then
      begin
        ListaErrores.Add('El Numero de Documento No Puede Estar En Blanco!!!');
      end;
    end;
  end;
  //Validando Concepto
  if (Length(Trim(TxtConcepto.Text)) = 0) then
  begin
    ListaErrores.Add('El Concepto No Puede Estar En Blanco!!!');
  end;
  if (TxtMontoNeto.Value <= 0) then
  begin
    ListaErrores.Add('El Monto Neto Debe Ser Positivo!!!');
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
