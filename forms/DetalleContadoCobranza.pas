unit DetalleContadoCobranza;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Mask, rxToolEdit, rxCurrEdit, ExtCtrls, Grids,
  DBGrids, RxLookup, DB, DBClient, Utils, ClsDetalleContadoCobranza,
  ClsBancoCobranza, ClsFormaPago, GenericoBuscador, ClsConsecutivo,
  ClsAnticipoCliente, ClsNotaCreditoCliente;

type
  TFrmDetalleContadoCobranza = class(TForm)
    GrpDatos: TGroupBox;
    PanelBotones: TPanel;
    GrpTotalRecibido: TGroupBox;
    LblRecibido: TLabel;
    TxtRecibido: TCurrencyEdit;
    LblVuelto: TLabel;
    BtnCancelar: TBitBtn;
    BtnAceptar: TBitBtn;
    TxtVuelto: TCurrencyEdit;
    GridDetalleContado: TDBGrid;
    LblCodigoFormaPago: TLabel;
    CmbCodigoFormaPago: TRxDBLookupCombo;
    LblMonto: TLabel;
    TxtMonto: TCurrencyEdit;
    LblFecha: TLabel;
    DatFecha: TDateEdit;
    LblBancoCobranza: TLabel;
    CmbCodigoBancoCobranza: TRxDBLookupCombo;
    TxtNumeroAuxiliar: TEdit;
    TxtNumero: TEdit;
    TxtReferencia: TEdit;
    LblNumero: TLabel;
    LblNumeroAuxiliar: TLabel;
    LblReferencia: TLabel;
    DS: TDataSource;
    LblTotal: TLabel;
    TxtTotal: TLabel;
    LblRestan: TLabel;
    TxtRestan: TLabel;
    Data: TClientDataSet;
    Datacodigo_forma_pago: TStringField;
    Datamonto: TFloatField;
    Datacodigo_banco: TStringField;
    Datanumero_documento: TStringField;
    Datanumero_auxiliar: TStringField;
    Datareferencia: TStringField;
    Datafecha: TDateField;
    Datalogin_usuario: TStringField;
    DSFormaPago: TDataSource;
    DSBancoCobranza: TDataSource;
    BtnAgregar: TBitBtn;
    BtnBuscarBancoCobranza: TBitBtn;
    BtnBuscarCodigoFormaPago: TBitBtn;
    procedure TxtRecibidoExit(Sender: TObject);
    procedure DataAfterPost(DataSet: TDataSet);
    procedure BtnAceptarClick(Sender: TObject);
    procedure BtnCancelarClick(Sender: TObject);
    procedure BtnAgregarClick(Sender: TObject);
    procedure BtnBuscarBancoCobranzaClick(Sender: TObject);
    procedure BtnBuscarCodigoFormaPagoClick(Sender: TObject);
    procedure GridDetalleContadoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TxtNumeroExit(Sender: TObject);
  private
    { Private declarations }
    Desde: string;
    CodigoCliente: string;
    Total: Currency;
    Restan: Currency;
    Agregando: Boolean;
    Editando: Boolean;
    Clase: TDetalleContadoCobranza;
    IdContadoCobranza: Integer;
    BancoCobranza: TBancoCobranza;
    FormaPago: TFormaPago;
    AnticipoCliente: TAnticipoCliente;
    NotaCreditoCliente: TNotaCreditoCliente;
    DataBancoCobranza: TClientDataSet;
    DataFormaPago: TClientDataSet;
    function Validar: Boolean;
  public
    { Public declarations }
    Aceptado: Boolean;
    constructor Crear(AOwner: TComponent; AAgregando, AEditando: Boolean; AIdContadoCobranza: Integer; AMonto: Double; ADesde: string; ACodigoCliente: string);
    procedure Actualizar;
    function GetIdContadoCobranza: Integer;
  end;

var
  FrmDetalleContadoCobranza: TFrmDetalleContadoCobranza;

implementation

uses
  PModule, ClsGenericoBD;

{$R *.dfm}

{ TFrmDetalleContadoCobranza }

procedure TFrmDetalleContadoCobranza.Actualizar;
begin
  //Actualizando la Suma de las Formas de Pago
  Restan := 0;
  with Data do
  begin
    First;
    while (not Eof) do
    begin
      Restan := Restan + (Trunc(FieldByName('monto').AsCurrency * 10000) / 10000);
      Next;
    end;
  end;
  Restan := Total - Restan;
  //Limpiando Pantalla
  CmbCodigoFormaPago.Value := '';
  TxtMonto.Value := Restan;
  DatFecha.Date := Date;
  CmbCodigoBancoCobranza.Value := '';
  TxtNumero.Text := '';
  TxtNumeroAuxiliar.Text := '';
  TxtReferencia.Text := '';
  TxtTotal.Caption := FormatFloat('#,##0.0000',Total);
  TxtRestan.Caption := FormatFloat('#,##0.0000',Restan);
end;

procedure TFrmDetalleContadoCobranza.BtnAceptarClick(Sender: TObject);
var
  Consecutivo: TConsecutivo;
begin
  if (MessageBox(Self.Handle, '�Esta Seguro Que Los Datos Estan Correctos?', PChar(MsgTituloInformacion), MB_ICONQUESTION or MB_YESNO or MB_TOPMOST or MB_TASKMODAL) = mrYes) then
  begin
    if (Restan = 0) then
    begin
      Aceptado := True;
      //Se Guarda El Detalle De Contado
      Consecutivo := TConsecutivo.Create;
      if (Agregando) then
      begin
        IdContadoCobranza := Consecutivo.ObtenerConsecutivo('id_contado_cobranza');
      end
      else
      begin
        if (IdContadoCobranza = -1) then //Caso de Documento que cambia de Credito a Contado
        begin
          IdContadoCobranza := Consecutivo.ObtenerConsecutivo('id_contado_cobranza');
        end;
      end;
      //Eliminando Anteriores en Caso de Editar
      if (Editando) then
      begin
        Clase.EliminarIdContadoCobranza(IdContadoCobranza);
      end;      
      with Data do
      begin
        DisableControls;
        First;
        while (not Eof) do
        begin
          with Clase do
          begin
            SetIdContadoCobranza(IdContadoCobranza);
            SetCodigoFormaPago(FieldValues['codigo_forma_pago']);
            SetMonto(FieldValues['monto']);
            SetDesde(Desde);
            SetCodigoBanco(FieldValues['codigo_banco']);
            SetNumeroDocumento(FieldValues['numero_documento']);
            SetNumeroAuxiliar(FieldValues['numero_auxiliar']);
            SetReferencia(FieldValues['referencia']);
            SetFecha(FieldValues['fecha']);
            SetLoginUsuario(FieldValues['login_usuario']);
            Insertar;
          end;
          Next;
        end;
        EnableControls;
      end;
      Close;
    end
    else
    begin
      MessageBox(Self.Handle, 'El Detalle de Contado No Cuadra!!!', PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    end;
  end;
end;

procedure TFrmDetalleContadoCobranza.BtnAgregarClick(Sender: TObject);
begin
  if ((Length(Trim(CmbCodigoFormaPago.Value)) > 0) and (Restan > 0) and (TxtMonto.Value > 0) and ((Trunc(TxtMonto.Value * 10000) / 10000) <= (Trunc(Restan * 10000) / 10000))) then
  begin
    if (Validar) then
    begin
      with Data do
      begin
        Insert;
        FieldValues['codigo_forma_pago'] := CmbCodigoFormaPago.Value;
        FieldValues['monto'] := TxtMonto.Value;
        FieldValues['codigo_banco']:= CmbCodigoBancoCobranza.Value;
        FieldValues['numero_documento'] := TxtNumero.Text;
        FieldValues['numero_auxiliar'] := TxtNumeroAuxiliar.Text;
        FieldValues['referencia'] := TxtReferencia.Text;
        FieldValues['fecha'] := DatFecha.Date;
        FieldValues['login_usuario'] := P.User.GetLoginUsuario;
        Post;
      end;
    end;
  end;  
end;

procedure TFrmDetalleContadoCobranza.BtnBuscarBancoCobranzaClick(
  Sender: TObject);
begin
  Buscador(Self,BancoCobranza,'codigo','Buscar Banco de Cobranza',nil,DataBancoCobranza);
  CmbCodigoBancoCobranza.KeyValue := DataBancoCobranza.FieldValues['codigo'];
end;

procedure TFrmDetalleContadoCobranza.BtnBuscarCodigoFormaPagoClick(
  Sender: TObject);
begin
  Buscador(Self,FormaPago,'codigo','Buscar Forma de Pago',nil,DataFormaPago);
  CmbCodigoFormaPago.KeyValue := DataFormaPago.FieldValues['codigo'];
end;

procedure TFrmDetalleContadoCobranza.BtnCancelarClick(Sender: TObject);
begin
  Close;
end;

constructor TFrmDetalleContadoCobranza.Crear(AOwner: TComponent; AAgregando,
  AEditando: Boolean; AIdContadoCobranza: Integer; AMonto: Double;
  ADesde: string; ACodigoCliente: string);
begin
  inherited Create(AOwner);
  Agregando := AAgregando;
  Editando := AEditando;
  IdContadoCobranza := AIdContadoCobranza;
  Desde := ADesde;
  CodigoCliente := ACodigoCliente;
  Total := AMonto;
  Restan := AMonto;
  Aceptado := False;
  //Inicializando DataSets Secundarios
  Clase := TDetalleContadoCobranza.Create;
  BancoCobranza := TBancoCobranza.Create;
  FormaPago := TFormaPago.Create;
  AnticipoCliente := TAnticipoCliente.Create;
  NotaCreditoCliente := TNotaCreditoCliente.Create;
  DataBancoCobranza := BancoCobranza.ObtenerCombo;
  DataFormaPago := FormaPago.ObtenerCombo;
  DataBancoCobranza.First;
  DataFormaPago.First;
  DSBancoCobranza.DataSet := DataBancoCobranza;
  DSFormaPago.DataSet := DataFormaPago;
  ConfigurarRxDBLookupCombo(CmbCodigoBancoCobranza,DSBancoCobranza,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoFormaPago,DSFormaPago,'codigo','codigo;descripcion');
  DS.DataSet := Data;
  GridDetalleContado.DataSource := DS;
  Actualizar;
end;

procedure TFrmDetalleContadoCobranza.DataAfterPost(DataSet: TDataSet);
begin
  Actualizar;
end;

function TFrmDetalleContadoCobranza.GetIdContadoCobranza: Integer;
begin
  Result := IdContadoCobranza;
end;

procedure TFrmDetalleContadoCobranza.GridDetalleContadoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  case Key of
    VK_DELETE:
    begin
      if (not Data.IsEmpty) then
      begin
        if (MessageBox(Self.Handle, '�Esta Seguro?', MsgTituloAdvertencia, MB_ICONWARNING or MB_YESNO or MB_TOPMOST or MB_TASKMODAL) = mrYes) then
        begin
          Data.Delete;
        end;
      end;
    end;
  end;
end;

procedure TFrmDetalleContadoCobranza.TxtNumeroExit(Sender: TObject);
begin
  if (Length(Trim(TxtNumero.Text)) > 0) then
  begin
    FormaPago.SetCodigo(CmbCodigoFormaPago.Value);
    FormaPago.Buscar;
    if ((FormaPago.GetAnticipo) or (FormaPago.GetNc)) then
    begin
      if (FormaPago.GetAnticipo) then
      begin
        with AnticipoCliente do
        begin
          SetNumero(Trim(TxtNumero.Text));
          if (BuscarNumero) then
          begin
            if (GetCodigoCliente = CodigoCliente) then //Validando Codigo Cliente
            begin
              if (GetMontoNeto > Total) then
              begin
                MessageBox(Self.Handle, PChar('El Anticipo Especificado No Es Aplicable porque su Monto es Mayor al Monto a Facturar!!!'), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
                TxtNumero.Text := '';
                TxtNumero.SetFocus;
              end
              else
              begin
                MessageBox(Self.Handle, PChar('Atenci�n: Se Ajustar� el Monto a Aplicar seg�n el Monto del Anticipo Especificado!!!'), PChar(MsgTituloError), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
                //Ajustando Valores del Monto
                TxtMonto.Value := GetMontoNeto;
              end;
            end
            else
            begin
              MessageBox(Self.Handle, PChar('El Numero del Documento Especificado Existe! pero No Pertenece a este Cliente!!'), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
              TxtNumero.Text := '';
              TxtNumero.SetFocus;
            end;
          end
          else
          begin
            MessageBox(Self.Handle, PChar('El Numero del Documento Especificado No Existe!!!'), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
            TxtNumero.Text := '';
            TxtNumero.SetFocus;
          end;
        end;
      end
      else
      begin
        if (FormaPago.GetNc) then
        begin
          with NotaCreditoCliente do
          begin
            SetNumero(Trim(TxtNumero.Text));
            if (BuscarNumero) then
            begin
              if (GetCodigoCliente = CodigoCliente) then //Validando Codigo Cliente
              begin
                if (GetMontoNeto > Total) then
                begin
                  MessageBox(Self.Handle, PChar('La Nota de Cr�dito Especificada No Es Aplicable porque su Monto es Mayor al Monto a Facturar!!!'), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
                  TxtNumero.Text := '';
                  TxtNumero.SetFocus;
                end
                else
                begin
                  MessageBox(Self.Handle, PChar('Atenci�n: Se Ajustar� el Monto a Aplicar seg�n el Monto de la Nota de Cr�dito Especificada!!!'), PChar(MsgTituloError), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
                  //Ajustando Valores del Monto
                  TxtMonto.Value := GetTotal;
                end;
              end
              else
              begin
                MessageBox(Self.Handle, PChar('El Numero del Documento Especificado Existe! pero No Pertenece a este Cliente!!'), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
                TxtNumero.Text := '';
                TxtNumero.SetFocus;
              end;
            end
            else
            begin
              MessageBox(Self.Handle, PChar('El Numero del Documento Especificado No Existe!!!'), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
              TxtNumero.Text := '';
              TxtNumero.SetFocus;
            end;
          end;
        end;
      end;
    end;
  end;
end;

procedure TFrmDetalleContadoCobranza.TxtRecibidoExit(Sender: TObject);
var
  Efectivo: Double;
  ClaseFormaPago: TFormaPago;
begin
  ClaseFormaPago := TFormaPago.Create;
  Efectivo := 0;
  with Data do
  begin
    First;
    while (not Eof) do
    begin
      ClaseFormaPago.SetCodigo(FieldValues['codigo_forma_pago']);
      ClaseFormaPago.Buscar;
      if ((not ClaseFormaPago.GetCheque) and (not ClaseFormaPago.GetNc) and
          (not ClaseFormaPago.GetNd) and (not ClaseFormaPago.GetAnticipo)) then
      begin
        Efectivo  := Efectivo + FieldValues['monto'];
      end;
      Next;
    end;
  end;
  if ((TxtRecibido.Value - Efectivo) > 0) then
  begin
    TxtVuelto.Value := TxtRecibido.Value - Efectivo;
  end
  else
  begin
    TxtVuelto.Value := 0;
  end;
end;

function TFrmDetalleContadoCobranza.Validar: Boolean;
var
  ClaseFormaPago: TFormaPago;
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  ClaseFormaPago := TFormaPago.Create;
  ClaseFormaPago.SetCodigo(CmbCodigoFormaPago.Value);
  ClaseFormaPago.Buscar;
  with ClaseFormaPago do
  begin
    //Validando Transaccion Bancaria
    if (GetOperacionBancaria) then
    begin
      //Validando Codigo Banco
      if (Length(Trim(CmbCodigoBancoCobranza.Value)) = 0) then
      begin
        ListaErrores.Add('El Banco No Puede Estar En Blanco!!!');
      end;
      //Validando Numero de la Operacion Bancaria
      if (Length(Trim(TxtNumero.Text)) = 0) then
      begin
        ListaErrores.Add('El N�mero de la Operacion Bancaria (Numero Doc) No Puede Estar En Blanco!!!');
      end;
    end;
    //Validando Campo Cheque
    if (GetCheque) then
    begin
      //Validando Codigo Banco
      if (Length(Trim(CmbCodigoBancoCobranza.Value)) = 0) then
      begin
        ListaErrores.Add('El Banco No Puede Estar En Blanco!!!');
      end;
      //Validando Numero del Cheque en Numero Doc
      if (Length(Trim(TxtNumero.Text)) = 0) then
      begin
        ListaErrores.Add('El N�mero del Cheque (Numero Doc) No Puede Estar En Blanco!!!');
      end;
    end;
    //Validando Nota de Credito, Nota de Debito o Anticipo
    if (GetNc or GetNd or GetAnticipo) then
    begin
      //Validando Numero de la Nota o Anticipo en Numero Doc
      if (Length(Trim(TxtNumero.Text)) = 0) then
      begin
        ListaErrores.Add('El N�mero de la Nota de Debito, Credito o Anticipo (Numero Doc) No Puede Estar En Blanco!!!');
      end;
      //Validando Numero y Monto del Anticipo
      if (GetAnticipo) then
      begin
        AnticipoCliente.SetNumero(Trim(TxtNumero.Text));
        if (AnticipoCliente.BuscarNumero) then
        begin
          if (AnticipoCliente.GetMontoNeto <> TxtMonto.Value) then
          begin
            ListaErrores.Add('El Monto del Anticipo Especificado debe Coincidir con el Anticipo Registrado en el Sistema!!!');
          end;
        end
        else
        begin
          ListaErrores.Add('El Anticipo Especificado No Existe!!!');
        end;
      end;
      //Validando Numero y Monto de la Nota de Cr�dito
      if (GetNc) then
      begin
        NotaCreditoCliente.SetNumero(Trim(TxtNumero.Text));
        if (NotaCreditoCliente.BuscarNumero) then
        begin
          if (NotaCreditoCliente.GetTotal <> TxtMonto.Value) then
          begin
            ListaErrores.Add('El Monto de la Nota de Cr�dito Especificada debe Coincidir con la Nota de Cr�dito Registrada en el Sistema!!!');
          end;
        end
        else
        begin
          ListaErrores.Add('La Nota de Cr�dito Especificada No Existe!!!');
        end;
      end;
    end;
  end;
  //Validando Repeticion del Efectivo
  with Data do
  begin
    DisableControls;
    First;
    while (not Eof) do
    begin
      ClaseFormaPago.SetCodigo(FieldValues['codigo_forma_pago']);
      ClaseFormaPago.Buscar;
      if ((not ClaseFormaPago.GetCheque) and (not ClaseFormaPago.GetNc) and
          (not ClaseFormaPago.GetNd) and (not ClaseFormaPago.GetAnticipo)) then
      begin
        if (ClaseFormaPago.GetCodigo = CmbCodigoFormaPago.Value) then
        begin
          ListaErrores.Add('La Forma de Pago esta Repetida!!!');
          Break;
        end;
      end;
      Next;
    end;
    EnableControls;
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
