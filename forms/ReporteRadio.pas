unit ReporteRadio;

{$I sophie.inc}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, RxLookup, Mask, rxToolEdit, Buttons, ExtCtrls, RXCtrls,
  DB, DBClient, ClsReporte, ClsUser, DBCtrls, ClsCliente, ClsVendedor,
  ClsRadioEmisora;

type
  TFrmReporteRadio = class(TForm)
    GrpListadoReportes: TRadioGroup;
    GrpParametrosReporte: TGroupBox;
    SLinea: TShape;
    LblRangoCliente: TRxLabel;
    LblPeriodo: TRxLabel;
    LblRangoEmisora: TRxLabel;
    LblTipoReporte: TRxLabel;
    LblClaseReporte: TRxLabel;
    PanelBotones: TPanel;
    BtnEmitir: TBitBtn;
    BtnInicializar: TBitBtn;
    BtnCerrar: TBitBtn;
    GrpPeriodo: TGroupBox;
    LblFechaAl: TLabel;
    LblFechaDesde: TLabel;
    LblFechaHasta: TLabel;
    DatFechaAl: TDateEdit;
    DatFechaDesde: TDateEdit;
    DatFechaHasta: TDateEdit;
    GrpRangoEmisora: TGroupBox;
    LblEmisoraDesde: TLabel;
    LblEmisoraHasta: TLabel;
    CmbEmisoraDesde: TRxDBLookupCombo;
    CmbEmisoraHasta: TRxDBLookupCombo;
    GrpTipoReporte: TGroupBox;
    CmbTipoReporte: TComboBox;
    GrpClaseReporte: TGroupBox;
    CmbClaseReporte: TComboBox;
    GrpRangoCliente: TGroupBox;
    LblClienteDesde: TLabel;
    LblClienteHasta: TLabel;
    CmbClienteDesde: TRxDBLookupCombo;
    CmbClienteHasta: TRxDBLookupCombo;
    DSClienteDesde: TDataSource;
    DSClienteHasta: TDataSource;
    DSEmisoraDesde: TDataSource;
    DSEmisoraHasta: TDataSource;
    LblRangoVendedor: TRxLabel;
    GrpRangoVendedor: TGroupBox;
    LblVendedorDesde: TLabel;
    LblVendedorHasta: TLabel;
    CmbVendedorDesde: TRxDBLookupCombo;
    CmbVendedorHasta: TRxDBLookupCombo;
    DSVendedorDesde: TDataSource;
    DSVendedorHasta: TDataSource;
    procedure LblRangoVendedorDblClick(Sender: TObject);
    procedure CmbTipoReporteClick(Sender: TObject);
    procedure CmbVendedorDesdeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CmbVendedorDesdeCloseUp(Sender: TObject);
    procedure CmbEmisoraDesdeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure BtnCerrarClick(Sender: TObject);
    procedure BtnInicializarClick(Sender: TObject);
    procedure BtnEmitirClick(Sender: TObject);
    procedure LblClienteHastaDblClick(Sender: TObject);
    procedure LblEmisoraHastaDblClick(Sender: TObject);
    procedure CmbEmisoraDesdeCloseUp(Sender: TObject);
    procedure LblEmisoraDesdeDblClick(Sender: TObject);
    procedure CmbClienteDesdeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CmbClienteDesdeCloseUp(Sender: TObject);
    procedure LblClienteDesdeDblClick(Sender: TObject);
    procedure GrpListadoReportesClick(Sender: TObject);
  private
    { Private declarations }
    Reporte: TReporte;
    User: TUser;
    (* Clases Temporales a Utilizar para la Seleccion de Parametros *)
    Cliente: TCliente;
    Vendedor: TVendedor;
    RadioEmisora: TRadioEmisora;
    (* Fin Clases Temporales *)
    (* Controles Adicionales *)
    DataClienteDesde: TClientDataSet;
    DataClienteHasta: TClientDataSet;
    DataRadioEmisoraDesde: TClientDataSet;
    DataRadioEmisoraHasta: TClientDataSet;
    DataVendedorDesde: TClientDataSet;
    DataVendedorHasta: TClientDataSet;
    (* Fin Controles Adicionales *)
    procedure SeleccionReporte;
    procedure Inicializar;
    procedure CargarParametros;
    procedure EmitirReporte;
    procedure Deshabilitar;
    function Validar: Boolean;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent; ALoginUsuario: String); reintroduce;
  end;

var
  FrmReporteRadio: TFrmReporteRadio;

implementation

uses
  ActividadSophie,
  GenericoBuscador,
  Utils
  {$IFDEF USE_RAVE_REPORTS}
  , DMRR
  {$ENDIF}
  {$IFDEF USE_FAST_REPORTS}
  , DMFR
  {$ENDIF}
  ;

{$R *.dfm}

procedure TFrmReporteRadio.SeleccionReporte;
begin
  Deshabilitar;
  //Activacion de Controles Segun Convenga
  DatFechaAl.Date := Date;
  DatFechaDesde.Date := Date;
  DatFechaHasta.Date := Date;
  case GrpListadoReportes.ItemIndex of
    0: //Pre-Facturacion Contratos
    begin
      LblPeriodo.Enabled := True;
      LblFechaAl.Enabled := False; DatFechaAl.Enabled := False;
      LblFechaDesde.Enabled := True; DatFechaDesde.Enabled := True;
      LblFechaHasta.Enabled := True; DatFechaHasta.Enabled := True;
      LblRangoCliente.Enabled := False;
      LblClienteDesde.Enabled := False; CmbClienteDesde.Enabled := False;
      LblClienteHasta.Enabled := False; CmbClienteHasta.Enabled := False;
      LblRangoEmisora.Enabled := False;
      LblEmisoraDesde.Enabled := False; CmbEmisoraDesde.Enabled := False;
      LblEmisoraHasta.Enabled := False; CmbEmisoraHasta.Enabled := False;
      LblRangoVendedor.Enabled := False;
      LblVendedorDesde.Enabled := False; CmbVendedorDesde.Enabled := False;
      LblVendedorHasta.Enabled := False; CmbVendedorHasta.Enabled := False;
      LblTipoReporte.Enabled := False;
      CmbTipoReporte.Enabled := False;
    end;
    1: //Reportes de Facturaci�n
    begin
      LblPeriodo.Enabled := True;
      LblFechaAl.Enabled := False; DatFechaAl.Enabled := False;
      LblFechaDesde.Enabled := True; DatFechaDesde.Enabled := True;
      LblFechaHasta.Enabled := True; DatFechaHasta.Enabled := True;
      LblRangoCliente.Enabled := False;
      LblClienteDesde.Enabled := False; CmbClienteDesde.Enabled := False;
      LblClienteHasta.Enabled := False; CmbClienteHasta.Enabled := False;
      LblRangoEmisora.Enabled := False;
      LblEmisoraDesde.Enabled := False; CmbEmisoraDesde.Enabled := False;
      LblEmisoraHasta.Enabled := False; CmbEmisoraHasta.Enabled := False;
      LblRangoVendedor.Enabled := False;
      LblVendedorDesde.Enabled := False; CmbVendedorDesde.Enabled := False;
      LblVendedorHasta.Enabled := False; CmbVendedorHasta.Enabled := False;
      LblTipoReporte.Enabled := True;
      CmbTipoReporte.Enabled := True;
      CmbTipoReporte.Items.Clear;
      CmbTipoReporte.Items.Add('General');
      CmbTipoReporte.ItemIndex := 0;
      LblClaseReporte.Enabled := True;
      CmbClaseReporte.Enabled := True;
      CmbClaseReporte.Items.Clear;
      CmbClaseReporte.Items.Add('Numero');
      CmbClaseReporte.Items.Add('Agencias');
      CmbClaseReporte.Items.Add('Clientes');
      CmbClaseReporte.Items.Add('Efectivo');
      CmbClaseReporte.Items.Add('Intercambio');
      CmbClaseReporte.Items.Add('Venta Nacional');
      CmbClaseReporte.Items.Add('Anuladas');
      CmbClaseReporte.ItemIndex := 0;
    end;
    2://Reportes de Ventas
    begin
      LblPeriodo.Enabled := True;
      LblFechaAl.Enabled := False; DatFechaAl.Enabled := False;
      LblFechaDesde.Enabled := True; DatFechaDesde.Enabled := True;
      LblFechaHasta.Enabled := True; DatFechaHasta.Enabled := True;
      LblRangoCliente.Enabled := False;
      LblClienteDesde.Enabled := False; CmbClienteDesde.Enabled := False;
      LblClienteHasta.Enabled := False; CmbClienteHasta.Enabled := False;
      LblRangoEmisora.Enabled := False;
      LblEmisoraDesde.Enabled := False; CmbEmisoraDesde.Enabled := False;
      LblEmisoraHasta.Enabled := False; CmbEmisoraHasta.Enabled := False;
      LblRangoVendedor.Enabled := False;
      LblVendedorDesde.Enabled := False; CmbVendedorDesde.Enabled := False;
      LblVendedorHasta.Enabled := False; CmbVendedorHasta.Enabled := False;
      LblTipoReporte.Enabled := True;
      CmbTipoReporte.Enabled := True;
      CmbTipoReporte.Items.Clear;
      CmbTipoReporte.Items.Add('General');
      CmbTipoReporte.ItemIndex := 0;
      LblClaseReporte.Enabled := True;
      CmbClaseReporte.Enabled := True;
      CmbClaseReporte.Items.Clear;
      CmbClaseReporte.Items.Add('Clientes');
      CmbClaseReporte.Items.Add('Agencias');
      CmbClaseReporte.Items.Add('Anunciantes');
      CmbClaseReporte.Items.Add('Emisoras');
      CmbClaseReporte.Items.Add('Vendedor');
      CmbClaseReporte.Items.Add('Vendedor(R)');
      CmbClaseReporte.ItemIndex := 0;
    end;
    3://Reporte de Impuestos Conatel
    begin
      LblPeriodo.Enabled := True;
      LblFechaAl.Enabled := False; DatFechaAl.Enabled := False;
      LblFechaDesde.Enabled := True; DatFechaDesde.Enabled := True;
      LblFechaHasta.Enabled := True; DatFechaHasta.Enabled := True;
      LblRangoCliente.Enabled := False;
      LblClienteDesde.Enabled := False; CmbClienteDesde.Enabled := False;
      LblClienteHasta.Enabled := False; CmbClienteHasta.Enabled := False;
      LblRangoEmisora.Enabled := False;
      LblEmisoraDesde.Enabled := False; CmbEmisoraDesde.Enabled := False;
      LblEmisoraHasta.Enabled := False; CmbEmisoraHasta.Enabled := False;
      LblRangoVendedor.Enabled := False;
      LblVendedorDesde.Enabled := False; CmbVendedorDesde.Enabled := False;
      LblVendedorHasta.Enabled := False; CmbVendedorHasta.Enabled := False;
      LblTipoReporte.Enabled := True;
      CmbTipoReporte.Enabled := True;
      CmbTipoReporte.Items.Clear;
      CmbTipoReporte.Items.Add('General');
      CmbTipoReporte.ItemIndex := 0;
      LblClaseReporte.Enabled := True;
      CmbClaseReporte.Enabled := True;
      CmbClaseReporte.Items.Clear;
      CmbClaseReporte.Items.Add('Impuesto Mensual');
      CmbClaseReporte.Items.Add('Impuesto Taser');
      CmbClaseReporte.ItemIndex := 0;
    end;
    4://Reporte de Cobranzas
    begin
      LblPeriodo.Enabled := True;
      LblFechaAl.Enabled := False; DatFechaAl.Enabled := False;
      LblFechaDesde.Enabled := True; DatFechaDesde.Enabled := True;
      LblFechaHasta.Enabled := True; DatFechaHasta.Enabled := True;
      LblRangoCliente.Enabled := False;
      LblClienteDesde.Enabled := False; CmbClienteDesde.Enabled := False;
      LblClienteHasta.Enabled := False; CmbClienteHasta.Enabled := False;
      LblRangoEmisora.Enabled := False;
      LblEmisoraDesde.Enabled := False; CmbEmisoraDesde.Enabled := False;
      LblEmisoraHasta.Enabled := False; CmbEmisoraHasta.Enabled := False;
      LblRangoVendedor.Enabled := False;
      LblVendedorDesde.Enabled := False; CmbVendedorDesde.Enabled := False;
      LblVendedorHasta.Enabled := False; CmbVendedorHasta.Enabled := False;
      LblTipoReporte.Enabled := False;
      CmbTipoReporte.Enabled := False;
    end;
    5://Reporte de Comisiones por Pagar
    begin
      LblPeriodo.Enabled := True;
      LblFechaAl.Enabled := False; DatFechaAl.Enabled := False;
      LblFechaDesde.Enabled := True; DatFechaDesde.Enabled := True;
      LblFechaHasta.Enabled := True; DatFechaHasta.Enabled := True;
      LblRangoCliente.Enabled := False;
      LblClienteDesde.Enabled := False; CmbClienteDesde.Enabled := False;
      LblClienteHasta.Enabled := False; CmbClienteHasta.Enabled := False;
      LblRangoEmisora.Enabled := False;
      LblEmisoraDesde.Enabled := False; CmbEmisoraDesde.Enabled := False;
      LblEmisoraHasta.Enabled := False; CmbEmisoraHasta.Enabled := False;
      LblRangoVendedor.Enabled := True;
      LblVendedorDesde.Enabled := True; CmbVendedorDesde.Enabled := True;
      LblVendedorHasta.Enabled := True; CmbVendedorHasta.Enabled := True;
      LblTipoReporte.Enabled := True;
      CmbTipoReporte.Enabled := True;
      CmbTipoReporte.Items.Clear;
      CmbTipoReporte.Items.Add('General');
      CmbTipoReporte.ItemIndex := 0;
      LblClaseReporte.Enabled := True;
      CmbClaseReporte.Enabled := True;
      CmbClaseReporte.Items.Clear;
      CmbClaseReporte.Items.Add('Detallado');
      CmbClaseReporte.Items.Add('Resumido');
      CmbClaseReporte.ItemIndex := 0;
    end;
    6:
    begin
      LblPeriodo.Enabled := True;
      LblFechaAl.Enabled := False; DatFechaAl.Enabled := False;
      LblFechaDesde.Enabled := True; DatFechaDesde.Enabled := True;
      LblFechaHasta.Enabled := True; DatFechaHasta.Enabled := True;
      LblRangoCliente.Enabled := False;
      LblClienteDesde.Enabled := False; CmbClienteDesde.Enabled := False;
      LblClienteHasta.Enabled := False; CmbClienteHasta.Enabled := False;
      LblRangoEmisora.Enabled := False;
      LblEmisoraDesde.Enabled := False; CmbEmisoraDesde.Enabled := False;
      LblEmisoraHasta.Enabled := False; CmbEmisoraHasta.Enabled := False;
      LblRangoVendedor.Enabled := False;
      LblVendedorDesde.Enabled := False; CmbVendedorDesde.Enabled := False;
      LblVendedorHasta.Enabled := False; CmbVendedorHasta.Enabled := False;
      LblTipoReporte.Enabled := False;
      CmbTipoReporte.Enabled := False;
    end;
  end;
  Application.ProcessMessages;
  SetAutoDropDownWidth(CmbTipoReporte);
  SetAutoDropDownWidth(CmbClaseReporte);
end;

procedure TFrmReporteRadio.Inicializar;
begin
  GrpListadoReportes.ItemIndex := 0;
  GrpListadoReportesClick(nil);
end;

procedure TFrmReporteRadio.LblClienteDesdeDblClick(Sender: TObject);
begin
  if (Buscador(Self,Cliente,'codigo','Buscar Cliente',nil,DataClienteDesde)) then
  begin
    CmbClienteDesde.KeyValue := DataClienteDesde.FieldValues['codigo'];
    CmbClienteDesdeCloseUp(Sender);
  end;
end;

procedure TFrmReporteRadio.LblClienteHastaDblClick(Sender: TObject);
begin
  if (Buscador(Self,Cliente,'codigo','Buscar Cliente',nil,DataClienteHasta)) then
  begin
    CmbClienteHasta.KeyValue := DataClienteHasta.FieldValues['codigo'];
  end;
end;

procedure TFrmReporteRadio.LblEmisoraDesdeDblClick(Sender: TObject);
begin
  if (Buscador(Self,RadioEmisora,'codigo','Buscar Emisora',nil,DataRadioEmisoraDesde)) then
  begin
    CmbEmisoraDesde.KeyValue := DataRadioEmisoraDesde.FieldValues['codigo'];
    CmbEmisoraDesdeCloseUp(Sender);
  end;
end;

procedure TFrmReporteRadio.LblEmisoraHastaDblClick(Sender: TObject);
begin
  if (Buscador(Self,RadioEmisora,'codigo','Buscar Emisora',nil,DataRadioEmisoraHasta)) then
  begin
    CmbEmisoraHasta.KeyValue := DataRadioEmisoraHasta.FieldValues['codigo'];
  end;
end;

procedure TFrmReporteRadio.LblRangoVendedorDblClick(Sender: TObject);
begin
  CmbVendedorDesde.KeyValue := '';
  CmbVendedorHasta.KeyValue := '';
end;

procedure TFrmReporteRadio.CargarParametros;
var
  TituloReporte: String;
  TipoReporte: String;
  ClaseReporte: String;
begin
  with Reporte do
  begin
    AgregarParametro('LoginUsuario',User.GetLoginUsuario);
    AgregarParametro('NombreUsuario',User.GetNombres + ', ' + User.GetApellidos);
    AgregarParametro('TituloReporte','');
    AgregarParametro('FechaAl',UpperCase(FormatDateTime('yyyy-mm-dd',DatFechaAl.Date)));
    AgregarParametro('FechaDesde',UpperCase(FormatDateTime('yyyy-mm-dd',DatFechaDesde.Date)));
    AgregarParametro('FechaHasta',UpperCase(FormatDateTime('yyyy-mm-dd',DatFechaHasta.Date)));
    AgregarParametro('Periodo','Desde ' + FormatDateTime('dd/mm/yyyy',DatFechaDesde.Date) + ' Hasta ' + FormatDateTime('dd/mm/yyyy',DatFechaHasta.Date));
  end;
  case GrpListadoReportes.ItemIndex of
    0: //Pre-Facturaci�n de Contratos
    begin
      TituloReporte := 'Pre-Facturaci�n de Contratos';
      Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
      Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
      Reporte.AgregarParametro('Periodo','Desde ' + FormatDateTime('dd/mm/yyyy',DatFechaDesde.Date) + ' Hasta ' + FormatDateTime('dd/mm/yyyy',DatFechaHasta.Date));
    end;
    1: //Reportes de Facturaci�n
    begin
      TituloReporte := 'General de Facturas';
      TipoReporte := CmbTipoReporte.Text;
      Case CmbTipoReporte.ItemIndex of
        0:// Por Numero
        begin
          ClaseReporte := CmbClaseReporte.Text;
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        1:// Por Agencia
        begin
          ClaseReporte := CmbClaseReporte.Text;
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        2:// Por Cliente
        begin
          ClaseReporte := CmbClaseReporte.Text;
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        3:// Por Efectivo
        begin
          ClaseReporte := CmbClaseReporte.Text;
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        4:// Por Intercambio
        begin
          ClaseReporte := CmbClaseReporte.Text;
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        5:// Por Venta Nacional
        begin
          ClaseReporte := CmbClaseReporte.Text;
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        6:// Por Venta Anulada
        begin
          ClaseReporte := CmbClaseReporte.Text;
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
      end;
    end;
    2://Reporte de Ventas
    begin
      TituloReporte := 'General de Ventas';
      TipoReporte := CmbTipoReporte.Text;
      Case CmbTipoReporte.ItemIndex of
        0:// Ventas por Clientes
        begin
          ClaseReporte := CmbClaseReporte.Text;
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
      end;
    end;
    3://Reporte Impuestos Conatel
    begin
        Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
        Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
        Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
    end;
    4://Reporte Cobranzas
    begin
        TituloReporte := 'Reporte de Cobranzas';
        Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
        Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
        Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
    end;
    5://Reporte Comisiones por Pagar
    begin
        Reporte.AgregarParametro('VendedorDesde',CmbVendedorDesde.Text);
        Reporte.AgregarParametro('VendedorHasta',CmbVendedorHasta.Text);        
        Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
        Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
        Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
    end;
    6://Reporte Documentos Anulados
    begin
        Reporte.AgregarParametro('VendedorDesde',CmbVendedorDesde.Text);
        Reporte.AgregarParametro('VendedorHasta',CmbVendedorHasta.Text);
        Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
        Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
        Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
    end;
  end;
  Reporte.AgregarParametro('TituloReporte',TituloReporte);
  Reporte.AgregarParametro('TipoReporte',TipoReporte);
  Reporte.AgregarParametro('ClaseReporte',ClaseReporte);
end;

procedure TFrmReporteRadio.EmitirReporte;
var
  NombreReporte: string;
  SQL: TStringList;
  Where: string;
  OrderBy: string;

  function AgregarWhere(Old, New: string): string;
  begin
    if (Length(Trim(Old)) > 0) then
    begin
      Result := Old + ' AND ' + New;
    end
    else
    begin
      Result := New;
    end;
  end;

begin
  Screen.Cursor := crHourGlass;
  SQL := TStringList.Create;
  Where := '';
  OrderBy := '';
  case GrpListadoReportes.ItemIndex of
    0: //Pre-Facturaci�n de Contratos
    begin
      if (Validar) then
      begin
        Where := AgregarWhere(Where, '(radio_contratos_facturas_pendientes_fecha_inicial BETWEEN ' + QuotedStr(':FechaDesde') + ' AND ' + QuotedStr(':FechaHasta') + ')');
        NombreReporte :='RadioPreFacturacion';
        //Modificando Query SQL
        SQL.Add('SELECT *');
        SQL.Add('FROM public.v_rep_radio_pre_facturacion');
        if (Length(Trim(Where)) > 0) then
        begin
          SQL.Add('WHERE ' + Where);
        end;
        if (Length(Trim(OrderBy)) > 0) then
        begin
          SQL.Add(OrderBy);
        end;
        Reporte.ModificarSQLDriverDataView('DvrRadioPreFactura',SQL.Text);
      end;
    end;
    1: //Reportes de Facturaci�n
    begin
      if (Validar) then
      begin
        case CmbTipoReporte.ItemIndex of
          0: //General
          begin
            case CmbClaseReporte.ItemIndex of
              0: // Clase Reporte Numero
              begin
                NombreReporte :='RadioFacturacionNumero';
                OrderBy := 'ORDER BY numero_factura_cliente';
              end;
              1: // Clase Reporte Agencia
              begin
                NombreReporte :='RadioFacturacionAgencia';
                OrderBy := 'ORDER BY numero_factura_cliente,codigo_agencia_contrato';
              end;
              2: // Clase Reporte Cliente
              begin
                NombreReporte :='RadioFacturacionClientes';
                OrderBy := 'ORDER BY numero_factura_cliente,radio_contratos_codigo_cliente';
              end;
              3: // Clase Reporte Efectivo
              begin
                NombreReporte :='RadioFacturacionEfectivo';
                Where := AgregarWhere(Where, 'efectivo');
                OrderBy := 'ORDER BY numero_factura_cliente,efectivo';
              end;
              4: // Clase Reporte Intercambio
              begin
                NombreReporte :='RadioFacturacionIntercambio';
                Where := AgregarWhere(Where, 'intercambio');
                OrderBy := 'ORDER BY numero_factura_cliente,intercambio';
              end;
              5: // Clase Reporte Venta Nacional
              begin
                NombreReporte :='RadioFacturacionVentaNacional';
                Where := AgregarWhere(Where, 'venta_nacional');
                OrderBy := 'ORDER BY numero_factura_cliente,venta_nacional';
              end;
              6: // Clase Reporte Anuladas
              begin
                NombreReporte :='RadioFacturacionAnulada';
              end;
            end;
          end;
        end;
        //Modificando Query SQL
        SQL.Add('SELECT *');
        SQL.Add('FROM public.v_rep_radio_facturacion');
        if (Length(Trim(Where)) > 0) then
        begin
          SQL.Add('WHERE ' + Where);
        end;
        if (Length(Trim(OrderBy)) > 0) then
        begin
          SQL.Add(OrderBy);
        end;
        Reporte.ModificarSQLDriverDataView('DvrRadioFacturacion',SQL.Text);
      end;
    end;
    2://Reporte de Ventas
    begin
      case CmbTipoReporte.ItemIndex of
          0: //General
          begin
            case CmbClaseReporte.ItemIndex of
              0: // Clase Reporte Clientes
              begin
                NombreReporte :='RadioVentaCliente';
              end;
              1: // Clase Reporte Agencias
              begin
                NombreReporte :='RadioVentaAgencia';
              end;
              2: // Clase Reporte Anunciantes
              begin
                NombreReporte :='RadioVentaAnunciante';
              end;
              3: // Clase Reporte Emisoras
              begin
                NombreReporte :='RadioVentaEmisora';
              end;
              4: // Clase Reporte Vendedor
              begin
                NombreReporte :='RadioVentaVendedor';
              end;
              5: // Clase Reporte Vendedores
              begin
                NombreReporte :='RadioVentaVendedores';
              end;
            end;
          end;
      end;
    end;
    3://Reporte Impuestos Conatel - Contale (Taser)
    begin
      case CmbTipoReporte.ItemIndex of
        0: //General
        begin
          case CmbClaseReporte.ItemIndex of
            0: // Clase Reporte Conatel Mensual
            begin
              NombreReporte :='RadioConatel';
            end;
            1: // Clase Reporte Conatel Taser (Anual)
            begin
              NombreReporte :='RadioConatelTaser';
            end;
          end;
        end;
      end;
    end;
    4://Reporte Cobranzas
    begin
      NombreReporte :='RadioCobranza';
    end;
    5://Reporte Comisiones por Pagar
    begin
      case CmbTipoReporte.ItemIndex of
        0: //General
        begin
          case CmbClaseReporte.ItemIndex of
            0://Detallado
            begin
              NombreReporte :='RadioComisiones';
              //Determinando Query del Reporte para todos los Vendedores
              if (CmbVendedorDesde.Text = '') and (CmbVendedorHasta.Text = '') then
              begin
                SQL.Add('SELECT *');
                SQL.Add('FROM public.v_rep_radio_comision_vendedor');
                SQL.Add('WHERE (fecha_liquidacion  BETWEEN ' + QuotedStr(':FechaDesde') + ' AND '+QuotedStr(':FechaHasta')+')');
                SQL.Add('ORDER BY numero_contrato');
                Reporte.ModificarSQLDriverDataView('DvrRadioComision',SQL.Text);
              end;              
            end;
            1://Resumido
            begin            
              NombreReporte :='RadioComisionesResumido';
              //Determinando Query del Reporte para todos los Vendedores
              if (CmbVendedorDesde.Text = '') and (CmbVendedorHasta.Text = '') then
              begin
                SQL.Add('SELECT *');
                SQL.Add('FROM public.v_rep_radio_comision_vendedor');
                SQL.Add('WHERE (fecha_liquidacion  BETWEEN ' + QuotedStr(':FechaDesde') + ' AND '+QuotedStr(':FechaHasta')+')');
                SQL.Add('ORDER BY numero_contrato');
                Reporte.ModificarSQLDriverDataView('DvrRadioComision',SQL.Text);
              end;
            end;
          end;
        end;
      end;
    end;
    6://Reporte Documentos Anulados
    begin
      NombreReporte :='RadioDocumentosAnulados';
    end;
  end;
  CargarParametros;
  if (not Reporte.EmiteReporte(NombreReporte,trGeneral)) then
  begin
    MessageBox(Self.Handle, 'Los Par�metros Seleccionados No Devolvieron Resultados!!!', PChar(MsgTituloInformacion), MB_ICONEXCLAMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end;
  Screen.Cursor := crDefault;
end;

procedure TFrmReporteRadio.FormCreate(Sender: TObject);
begin
  Inicializar;
end;

procedure TFrmReporteRadio.Deshabilitar;
begin
  //Periodos
  LblPeriodo.Enabled := False;
  LblFechaAl.Enabled := False; DatFechaAl.Enabled := False;
  LblFechaDesde.Enabled := False; DatFechaDesde.Enabled := False;
  LblFechaHasta.Enabled := False; DatFechaHasta.Enabled := False;
  DatFechaAl.Date := Date;
  DatFechaDesde.Date := Date;
  DatFechaHasta.Date := Date;
  //Rango de Clientes
  LblRangoCliente.Enabled := False;
  LblClienteDesde.Enabled := False; CmbClienteDesde.Enabled := False;
  LblClienteHasta.Enabled := False; CmbClienteHasta.Enabled := False;
  CmbClienteDesde.KeyValue := '';
  CmbClienteHasta.KeyValue := '';
  //Rango de Emisoras
  LblRangoEmisora.Enabled := False;
  LblEmisoraDesde.Enabled := False; CmbEmisoraDesde.Enabled := False;
  LblEmisoraHasta.Enabled := False; CmbEmisoraHasta.Enabled := False;
  CmbEmisoraDesde.KeyValue := '';
  CmbEmisoraHasta.KeyValue := '';
  //Rango de Vendedores
  LblRangoVendedor.Enabled := False;
  LblVendedorDesde.Enabled := False; CmbVendedorDesde.Enabled := False;
  LblVendedorHasta.Enabled := False; CmbVendedorHasta.Enabled := False;
  CmbVendedorDesde.KeyValue := '';
  CmbVendedorHasta.KeyValue := '';
  //Tipo de Reporte
  LblTipoReporte.Enabled := False;
  CmbTipoReporte.Enabled := False;
  CmbTipoReporte.Clear;
  CmbTipoReporte.Items.Clear;
  //Clase de Reporte
  LblClaseReporte.Enabled := False;
  CmbClaseReporte.Enabled := False;
  CmbClaseReporte.Clear;
  CmbClaseReporte.Items.Clear;
end;

constructor TFrmReporteRadio.Create(AOwner: TComponent;
  ALoginUsuario: String);
begin
  inherited Create(AOwner);
  User := TUser.Create;
  User.SetLoginUsuario(ALoginUsuario);
  User.Buscar;
  FrmActividadSophie.Show;
  FrmActividadSophie.Actividad('Cargando N�cleo...');
  Reporte := TReporte.Create;
  (* Inicializando Clases Temporales *)
  Cliente := TCliente.Create;
  RadioEmisora := TRadioEmisora.Create;
  Vendedor:= TVendedor.Create;
  (* Inicializando Controles Adicionales *)
  FrmActividadSophie.Actividad('Cargando Clientes...');
  DataClienteDesde := Cliente.ObtenerCombo;
  DataClienteHasta := Cliente.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Emisoras...');
  DataRadioEmisoraDesde := RadioEmisora.ObtenerCombo;
  DataRadioEmisoraHasta := RadioEmisora.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Vendedores...');
  DataVendedorDesde := Vendedor.ObtenerCombo;
  DataVendedorHasta := Vendedor.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Resto del N�cleo...');
  DSClienteDesde.DataSet := DataClienteDesde;
  DSClienteHasta.DataSet := DataClienteHasta;
  DSEmisoraDesde.DataSet := DataRadioEmisoraDesde;
  DSEmisoraHasta.DataSet := DataRadioEmisoraHasta;
  DSVendedorDesde.DataSet := DataVendedorDesde;
  DSVendedorHasta.DataSet := DataVendedorHasta;
  //Configurando RxDBLookupComboBox
  ConfigurarRxDBLookupCombo(CmbClienteDesde,DSClienteDesde,'codigo','codigo;razon_social');
  ConfigurarRxDBLookupCombo(CmbClienteHasta,DSClienteHasta,'codigo','codigo;razon_social');
  ConfigurarRxDBLookupCombo(CmbEmisoraDesde,DSEmisoraDesde,'codigo','codigo;razon_social');
  ConfigurarRxDBLookupCombo(CmbEmisoraHasta,DSEmisoraHasta,'codigo','codigo;razon_social');
  ConfigurarRxDBLookupCombo(CmbVendedorDesde,DSVendedorDesde,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbVendedorHasta,DSVendedorHasta,'codigo','codigo;descripcion');
  FrmActividadSophie.Close;
end;

procedure TFrmReporteRadio.BtnCerrarClick(Sender: TObject);
begin
  Close;
end;

procedure TFrmReporteRadio.BtnEmitirClick(Sender: TObject);
begin
  if (Validar) then
  begin
    if (MessageBox(Self.Handle, '�Esta Seguro Que Los Parametros Estan Correctos?', PChar(MsgTituloInformacion), MB_ICONQUESTION or MB_YESNO or MB_TOPMOST or MB_TASKMODAL) = mrYes) then
    begin
      EmitirReporte;
    end;
  end;
end;

procedure TFrmReporteRadio.BtnInicializarClick(Sender: TObject);
begin
  Inicializar;
end;

procedure TFrmReporteRadio.GrpListadoReportesClick(Sender: TObject);
begin
  SeleccionReporte;
end;

procedure TFrmReporteRadio.CmbClienteDesdeCloseUp(Sender: TObject);
begin
  CmbClienteHasta.KeyValue := CmbClienteDesde.KeyValue;
end;

procedure TFrmReporteRadio.CmbClienteDesdeKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if (Key = VK_ESCAPE) then
  begin
    CmbClienteHasta.KeyValue := '';
  end;
end;

procedure TFrmReporteRadio.CmbEmisoraDesdeCloseUp(Sender: TObject);
begin
  CmbEmisoraHasta.KeyValue := CmbEmisoraDesde.KeyValue;
end;

procedure TFrmReporteRadio.CmbEmisoraDesdeKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if (Key = VK_ESCAPE) then
  begin
    CmbEmisoraHasta.KeyValue := '';
  end;
end;

procedure TFrmReporteRadio.CmbVendedorDesdeCloseUp(Sender: TObject);
begin
  CmbVendedorHasta.KeyValue := CmbVendedorDesde.KeyValue;
end;

procedure TFrmReporteRadio.CmbVendedorDesdeKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if (Key = VK_ESCAPE) then
  begin
    CmbVendedorHasta.KeyValue := '';
  end;
end;

procedure TFrmReporteRadio.CmbTipoReporteClick(Sender: TObject);
begin
  case GrpListadoReportes.ItemIndex of
    0:
    begin
    end;
  end;
end;

function TFrmReporteRadio.Validar: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  (* Validando Informaci�n de Reportes Generales *)
  //Validando Fechas
  if (DatFechaDesde.Date > DatFechaHasta.Date) then
  begin
    ListaErrores.Add('La Fecha de Desde debe ser Menor a la Fecha Hasta!!!');
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
