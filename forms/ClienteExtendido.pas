unit ClienteExtendido;

{$I sophie.inc}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GenericoDetalle, StdCtrls, Buttons, rxCurrEdit, Mask, rxToolEdit,
  RxLookup, DB, DBClient, ImgList, XPMan, ComCtrls, Grids, DBGrids, ExtCtrls,
  ToolWin, Utils, ClsCliente, ClsVendedor, ClsZona, ClsEsquemaPago,
  ClsClienteVehiculo, ClsClienteCuentaBancaria, GenericoBuscador, ClsReporte,
  ClsParametro{$IFDEF DELPHI10}, ImageList{$ENDIF};

type
  TFrmClienteExtendido = class(TFrmGenericoDetalle)
    TabDireccion: TTabSheet;
    LblDireccionFiscal: TLabel;
    TxtDireccionFiscal1: TEdit;
    TxtDireccionFiscal2: TEdit;
    LblDireccionDomicilio: TLabel;
    TxtDireccionDomicilio1: TEdit;
    TxtDireccionDomicilio2: TEdit;
    TabVehiculos: TTabSheet;
    GridVehiculos: TDBGrid;
    TabCuentasBancarias: TTabSheet;
    LblCodigo: TLabel;
    TxtCodigo: TEdit;
    LblRif: TLabel;
    CmbPrefijoRif: TComboBox;
    TxtRif: TEdit;
    LblRazonSocial: TLabel;
    TxtRazonSocial: TEdit;
    LblFechaIngreso: TLabel;
    DatFechaIngreso: TDateEdit;
    LblNit: TLabel;
    TxtNit: TEdit;
    LblTelefonos: TLabel;
    lblEmail: TLabel;
    LblFax: TLabel;
    LblWebsite: TLabel;
    TxtTelefonos: TEdit;
    TxtEmail: TEdit;
    TxtFax: TEdit;
    TxtWebsite: TEdit;
    LblFechaNacimiento: TLabel;
    LblSexo: TLabel;
    TxtEdad: TLabel;
    DatFechaNacimiento: TDateEdit;
    CmbSexo: TComboBox;
    LblCodigoVendedor: TLabel;
    CmbCodigoVendedor: TRxDBLookupCombo;
    BtnBuscarVendedor: TBitBtn;
    LblCodigoZona: TLabel;
    CmbCodigoZona: TRxDBLookupCombo;
    BtnBuscarZona: TBitBtn;
    LblCodigoEsquemaPago: TLabel;
    CmbCodigoEsquemaPago: TRxDBLookupCombo;
    BtnEsquemaPago: TBitBtn;
    DSVendedor: TDataSource;
    DSZona: TDataSource;
    DSEsquemaPago: TDataSource;
    LblContacto: TLabel;
    TxtContacto: TEdit;
    DSClienteVehiculo: TDataSource;
    DataClienteVehiculo: TClientDataSet;
    DataClienteVehiculoplaca: TStringField;
    DataClienteVehiculomarca: TStringField;
    DataClienteVehiculomodelo: TStringField;
    DataClienteVehiculoanno: TIntegerField;
    DataClienteVehiculocolor: TStringField;
    DataClienteVehiculokilometraje: TFloatField;
    DataClienteVehiculoobservaciones: TStringField;
    GridCuentaBancaria: TDBGrid;
    DataClienteCuentaBancaria: TClientDataSet;
    DSClienteCuentaBancaria: TDataSource;
    DataClienteCuentaBancarianumero_cuenta: TStringField;
    DataClienteCuentaBancariaentidad: TStringField;
    DataClienteCuentaBancariatipo: TStringField;
    DataClienteCuentaBancariaemail: TStringField;
    DataClienteCuentaBancariaobservaciones: TStringField;
    procedure BtnBuscarVendedorClick(Sender: TObject);
    procedure BtnBuscarZonaClick(Sender: TObject);
    procedure BtnEsquemaPagoClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TBtnEditarClick(Sender: TObject);
  private
    { Private declarations }
    Clase: TCliente;
    BuscadorCliente: TCliente;
    Vendedor: TVendedor;
    Zona: TZona;
    EsquemaPago: TEsquemaPago;
    ClienteVehiculo: TClienteVehiculo;
    ClienteCuentaBancaria: TClienteCuentaBancaria;
    Reporte: TReporte;
    Parametro: TParametro;
    DataBuscadorCliente: TClientDataSet;
    DataVendedor: TClientDataSet;
    DataZona: TClientDataSet;
    DataEsquemaPago: TClientDataSet;
    procedure Agregar;
    procedure Editar;
    procedure Eliminar;
    procedure Guardar;
    procedure Cancelar;
    procedure Refrescar;
    procedure Imprimir;
    procedure Buscar;
    procedure Limpiar;
    procedure Asignar;
    procedure Obtener;
    procedure Mover;
    procedure GuardarDetalles;
  protected
    { Protected declarations }
    function Validar: Boolean; override;
    procedure Detalle; override;
  public
    { Public declarations }
  end;

var
  FrmClienteExtendido: TFrmClienteExtendido;

implementation

uses
  ClsAnticipoCliente,
  ClsFacturacionCliente,
  ClsNotaCreditoCliente,
  ClsNotaDebitoCliente,
  ActividadSophie,
  AgregarDetalleClienteVehiculo,
  AgregarDetalleClienteCuentaBancaria
  {$IFDEF USE_RAVE_REPORTS}
  , DMRR
  {$ENDIF}
  {$IFDEF USE_FAST_REPORTS}
  , DMFR
  {$ENDIF}
  ;

{$R *.dfm}

{ TFrmClienteExtendido }

procedure TFrmClienteExtendido.Agregar;
begin
  //Verificando
  Limpiar;
  //Asignando Valores por Defecto
  CmbCodigoZona.Value := Parametro.ObtenerValor('codigo_zona');
  CmbCodigoEsquemaPago.Value := Parametro.ObtenerValor('codigo_esquema_pago');
  TxtCodigo.SetFocus;
end;

procedure TFrmClienteExtendido.Asignar;
begin
  with Clase do
  begin
    TxtCodigo.Text := GetCodigo;
    CmbPrefijoRif.ItemIndex := CmbPrefijoRif.Items.IndexOf(GetPrefijoRif);
    TxtRif.Text := GetRif;
    TxtNit.Text := GetNit;
    CmbSexo.ItemIndex := CmbSexo.Items.IndexOf(GetSexo);
    DatFechaNacimiento.Date := GetFechaNacimiento;
    TxtRazonSocial.Text := GetRazonSocial;
    TxtTelefonos.Text := GetTelefonos;
    TxtFax.Text := GetFax;
    TxtEmail.Text := GetEmail;
    TxtWebsite.Text := GetWebsite;
    DatFechaIngreso.Date := GetFechaIngreso;
    TxtContacto.Text := GetContacto;
    CmbCodigoVendedor.KeyValue := GetCodigoVendedor;
    CmbCodigoZona.KeyValue := GetCodigoZona;
    CmbCodigoEsquemaPago.KeyValue := GetCodigoEsquemaPago;
    TxtDireccionFiscal1.Text := GetDireccionFiscal1;
    TxtDireccionFiscal2.Text := GetDireccionFiscal2;
    TxtDireccionDomicilio1.Text := GetDireccionDomicilio1;
    TxtDireccionDomicilio2.Text := GetDireccionDomicilio2;
    DataVendedor.Locate('codigo',CmbCodigoVendedor.KeyValue,[loCaseInsensitive]);
    DataZona.Locate('codigo',CmbCodigoZona.KeyValue,[loCaseInsensitive]);
    DataEsquemaPago.Locate('codigo',CmbCodigoEsquemaPago.KeyValue,[loCaseInsensitive]);
    //Cargando Detalles
    DataClienteVehiculo.CloneCursor(ClienteVehiculo.ObtenerCodigoCliente(GetCodigo),False,True);
    DSClienteVehiculo.DataSet := DataClienteVehiculo;
    TabDireccion.TabVisible := True;
    PageDetalles.ActivePage := TabDireccion;
  end;
end;

procedure TFrmClienteExtendido.BtnBuscarVendedorClick(Sender: TObject);
begin
  Buscador(Self,Vendedor,'codigo','Busqueda de Vendedor',nil,DataVendedor);
  CmbCodigoVendedor.KeyValue := DataVendedor.FieldValues['codigo'];
end;

procedure TFrmClienteExtendido.BtnBuscarZonaClick(Sender: TObject);
begin
  Buscador(Self,Zona,'codigo','Busqueda de Zona',nil,DataZona);
  CmbCodigoZona.KeyValue := DataZona.FieldValues['codigo'];
end;

procedure TFrmClienteExtendido.BtnEsquemaPagoClick(Sender: TObject);
begin
  Buscador(Self,EsquemaPago,'codigo','Busqueda de Esquema de Pago',nil,DataEsquemaPago);
  CmbCodigoEsquemaPago.KeyValue := DataEsquemaPago.FieldValues['codigo'];
end;

procedure TFrmClienteExtendido.Buscar;
begin
  if (Buscador(Self,BuscadorCliente,'codigo','Buscar Cliente',nil,DataBuscadorCliente)) then
  begin
    Clase.SetCodigo(DataBuscadorCliente.FieldValues['codigo']);
    Clase.BuscarCodigo;
    Asignar;
  end;
end;

procedure TFrmClienteExtendido.Cancelar;
begin
  if (Agregando) then
  begin
    Limpiar;
  end
  else
  begin
    Clase.SetCodigo(TxtCodigo.Text);
    Clase.BuscarCodigo;
    Asignar;
    TxtCodigo.Enabled := True;
  end;
end;

procedure TFrmClienteExtendido.Detalle;
begin
  inherited;
  case PageDetalles.ActivePageIndex of
    0: //TabDetalle
      begin
      end;
    1: //TabDireccion
      begin
      end;
    2: //TabVehiculo
      begin
        FrmAgregarDetalleClienteVehiculo := TFrmAgregarDetalleClienteVehiculo.CrearDetalle(Self,DataClienteVehiculo);
        FrmAgregarDetalleClienteVehiculo.ShowModal;
        FreeAndNil(FrmAgregarDetalleClienteVehiculo);
      end;
    3: //TabCuentasBancarias
      begin
        FrmAgregarDetalleClienteCuentaBancaria := TFrmAgregarDetalleClienteCuentaBancaria.CrearDetalle(Self,DataClienteCuentaBancaria);
        FrmAgregarDetalleClienteCuentaBancaria.ShowModal;
        FreeAndNil(FrmAgregarDetalleClienteCuentaBancaria);        
      end;
  end;
end;

procedure TFrmClienteExtendido.Editar;
begin
  if (Length(Trim(TxtCodigo.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
  TxtCodigo.Enabled := False;
end;

procedure TFrmClienteExtendido.Eliminar;
var
  AnticipoCliente: TAnticipoCliente;
  FacturacionCliente: TFacturacionCliente;
  NotaCreditoCliente: TNotaCreditoCliente;
  NotaDebitoCliente: TNotaDebitoCliente;
  sw: Boolean;
begin
  //Se Valida Que El Cliente No Posee Movimientos En El Sistema
  sw := True;
  //Verificando Anticipos
  AnticipoCliente := TAnticipoCliente.Create;
  AnticipoCliente.SetCodigoCliente(Clase.GetCodigo);
  if (AnticipoCliente.BuscarCodigoCliente) then
  begin
    sw := False;
  end
  else
  begin
    //Verificando Facturas
    FacturacionCliente := TFacturacionCliente.Create;
    FacturacionCliente.SetCodigoCliente(Clase.GetCodigo);
    if (FacturacionCliente.BuscarCodigoCliente) then
    begin
      sw := False;
    end
    else
    begin
      //Verificando Notas de Credito
      NotaCreditoCliente := TNotaCreditoCliente.Create;
      NotaCreditoCliente.SetCodigoCliente(Clase.GetCodigo);
      if (NotaCreditoCliente.BuscarCodigoCliente) then
      begin
        sw := False;
      end
      else
      begin
        //Verificando Notas de Debito
        NotaDebitoCliente := TNotaDebitoCliente.Create;
        NotaDebitoCliente.SetCodigoCliente(Clase.GetCodigo);
        if (NotaDebitoCliente.Buscar) then
        begin
          sw := False;
        end;
      end;
    end;
  end;
  if (sw) then
  begin
    //Se Busca La Factura!
    Clase.SetCodigo(TxtCodigo.Text);
    Clase.BuscarCodigo;
    //Se Eliminan Los Detalles
    //DetFacturacionCliente.EliminarIdFacturacionCliente(Clase.GetIdFacturacionCliente);

    Clase.Eliminar;
    MessageBox(Self.Handle, 'El Cliente Se Ha Eliminado Con Exito!!!',PChar(MsgTituloInformacion), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Refrescar;
    Limpiar;
  end
  else
  begin
    MessageBox(Self.Handle, 'El Cliente Presenta Movimientos En El Sistema y No Puede Ser Eliminado!!!',PChar(MsgTituloAdvertencia), MB_ICONEXCLAMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end;
end;

procedure TFrmClienteExtendido.FormCreate(Sender: TObject);
begin
  inherited;
  ProcAgregar := Agregar;
  ProcEditar := Editar;
  ProcEliminar := Eliminar;
  ProcGuardar := Guardar;
  ProcCancelar := Cancelar;
  ProcRefrescar := Refrescar;
  ProcImprimir := Imprimir;
  ProcBuscar := Buscar;
  FrmActividadSophie.Show;
  FrmActividadSophie.Actividad('Cargando N�cleo...');
  Clase := TCliente.Create;
  //Inicializando DataSets Secundarios
  BuscadorCliente := TCliente.Create;
  Vendedor := TVendedor.Create;
  Zona := TZona.Create;
  EsquemaPago := TEsquemaPago.Create;
  Reporte := TReporte.Create;
  Parametro := TParametro.Create;
  ClienteVehiculo := TClienteVehiculo.Create;
  ClienteCuentaBancaria := TClienteCuentaBancaria.Create;
  DataVendedor := Vendedor.ObtenerLista;
  DataZona := Zona.ObtenerLista;
  DataEsquemaPago := EsquemaPago.ObtenerLista;
  DataBuscadorCliente := BuscadorCliente.ObtenerLista;
  DataVendedor.First;
  DataZona.First;
  DataEsquemaPago.First;
  DataBuscadorCliente.First;
  DSVendedor.DataSet:= DataVendedor;
  DSZona.DataSet:= DataZona;
  DSEsquemaPago.DataSet:= DataEsquemaPago;
  ConfigurarRxDBLookupCombo(CmbCodigoVendedor,DSVendedor,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoZona,DSZona,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoEsquemaPago,DSEsquemaPago,'codigo','codigo;descripcion');
  //Configurando Tabs
  TabDetalle.TabVisible := False;
  TabDireccion.TabVisible := True;
  PageDetalles.ActivePage := TabDireccion;
  DS.DataSet := Data;
  GridDetalle.DataSource := DS;
  DSClienteVehiculo.DataSet := DataClienteVehiculo;
  GridVehiculos.DataSource := DSClienteVehiculo;
  DSClienteCuentaBancaria.DataSet := DataClienteCuentaBancaria;
  GridCuentaBancaria.DataSource := DSClienteCuentaBancaria;
  FrmActividadSophie.Close;
end;

procedure TFrmClienteExtendido.Guardar;
begin
  Obtener;
  if (Agregando) then
  begin
    //Agregando...
    //Se Busca el Codigo
    Clase.SetCodigo(TxtCodigo.Text);
    if (Clase.BuscarCodigo) then
    begin
      MessageBox(Self.Handle, 'El C�digo Ya Existe!!!', PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      TxtCodigo.SetFocus;
      Abort;
    end
    else
    begin
      Clase.Insertar;
      MessageBox(Self.Handle, 'El Cliente Ha Sido Agregado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      //Guardando Detalles
      GuardarDetalles;
    end;
  end
  else
  begin
    //Editando...
    Clase.Modificar;
    //Guardando Detalles
    ClienteVehiculo.EliminarCodigoCliente(Clase.GetCodigo);
    GuardarDetalles;
    MessageBox(Self.Handle, 'El Cliente Ha Sido Editado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    TxtCodigo.Enabled := True;
  end;
  TabDireccion.TabVisible := True;
  PageDetalles.ActivePage := TabDireccion;
end;

procedure TFrmClienteExtendido.GuardarDetalles;
begin
  //Guardando Vehiculos
  with DataClienteVehiculo do
  begin
    DisableControls;
    First;
    while (not Eof) do
    begin
      with ClienteVehiculo do
      begin
        SetCodigoCliente(Clase.GetCodigo);
        SetPlaca(FieldValues['placa']);
        SetMarca(FieldValues['marca']);
        SetModelo(FieldValues['modelo']);
        SetAnno(FieldValues['anno']);
        SetColor(FieldValues['color']);
        SetKilometraje(FieldValues['kilometraje']);
        SetObservaciones(FieldValues['observaciones']);
        Insertar;
      end;
      Next;
    end;
    EnableControls;
  end;
  //Guardando Cuentas Bancarias
  with DataClienteCuentaBancaria do
  begin
    DisableControls;
    First;
    while (not Eof) do
    begin
      with ClienteCuentaBancaria do
      begin
        SetCodigoCliente(Clase.GetCodigo);
        SetNumeroCuenta(FieldValues['numero_cuenta']);
        SetEntidad(FieldValues['entidad']);
        SetTipo(FieldValues['tipo']);
        SetEmail(FieldValues['email']);
        SetObservaciones(FieldValues['observaciones']);
        Insertar;
      end;
      Next;
    end;
    EnableControls;
  end;  
end;

procedure TFrmClienteExtendido.Imprimir;
begin
  with Reporte do
  begin
    AgregarParametro('CodigoCliente',Clase.GetCodigo);
    EmiteReporte('ClienteExtendido',trGeneral);
  end;
end;

procedure TFrmClienteExtendido.Limpiar;
begin
  TxtCodigo.Text := '';
  CmbPrefijoRif.Text := '';
  TxtRif.Text := '';
  TxtNit.Text := '';
  CmbSexo.Text := '';
  DatFechaNacimiento.Date := Now;
  TxtRazonSocial.Text := '';
  TxtTelefonos.Text := '';
  TxtFax.Text := '';
  TxtEmail.Text := '';
  TxtWebsite.Text := '';
  DatFechaIngreso.Date := Now;
  TxtContacto.Text := '';
  CmbCodigoVendedor.KeyValue := '';
  CmbCodigoZona.KeyValue := '';
  CmbCodigoEsquemaPago.KeyValue := '';
  TxtDireccionFiscal1.Text := '';
  TxtDireccionFiscal2.Text := '';
  TxtDireccionDomicilio1.Text := '';
  TxtDireccionDomicilio2.Text := '';
  //Status Bar
  SB.Panels[0].Text := '';
//  Data.EmptyDataSet;
  DataClienteVehiculo.EmptyDataSet;
  TabDireccion.TabVisible := True;
  PageDetalles.ActivePage := TabDireccion;
end;

procedure TFrmClienteExtendido.Mover;
begin

end;

procedure TFrmClienteExtendido.Obtener;
begin
  with Clase do
  begin
    SetCodigo(TxtCodigo.Text);
    SetPrefijoRif(CmbPrefijoRif.Text);
    SetRif(TxtRif.Text);
    SetNit(TxtNit.Text);
    SetSexo(CmbSexo.Text[1]);
    SetFechaNacimiento(DatFechaNacimiento.Date);
    SetRazonSocial(TxtRazonSocial.Text);
    SetTelefonos(TxtTelefonos.Text);
    SetFax(TxtFax.Text);
    SetEmail(TxtEmail.Text);
    SetWebsite(TxtWebsite.Text);
    SetFechaIngreso(DatFechaIngreso.Date);
    SetContacto(TxtContacto.Text);
    SetCodigoVendedor(CmbCodigoVendedor.Text);
    SetCodigoZona(CmbCodigoZona.Text);
    SetCodigoEsquemaPago(CmbCodigoEsquemaPago.Text);
    SetDireccionFiscal1(TxtDireccionFiscal1.Text);
    SetDireccionFiscal2(TxtDireccionFiscal2.Text);
    SetDireccionDomicilio1(TxtDireccionDomicilio1.Text);
    SetDireccionDomicilio2(TxtDireccionDomicilio2.Text);
  end;
end;

procedure TFrmClienteExtendido.Refrescar;
begin
  //Se Actualiza El DataBuscador
  DataBuscadorCliente := BuscadorCliente.ObtenerLista;
  //Se Actualizan Los demas Componentes
  DataVendedor := Vendedor.ObtenerCombo;
  DataZona := Zona.ObtenerCombo;
  DataEsquemaPago := EsquemaPago.ObtenerCombo;
  DataVendedor.First;
  DataZona.First;
  DataEsquemaPago.First;
  DataBuscadorCliente.First;
  DSVendedor.DataSet := DataVendedor;
  DSZona.DataSet := DataZona;
  DSEsquemaPago.DataSet := DataEsquemaPago;
end;

procedure TFrmClienteExtendido.TBtnEditarClick(Sender: TObject);
begin
  inherited;
  CmbPrefijoRif.SetFocus;
end;

function TFrmClienteExtendido.Validar: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  (* Validando Informacion del Cliente *)
  //Validando Codigo
  if (Length(Trim(TxtCodigo.Text)) = 0) then
  begin
    ListaErrores.Add('El C�digo No Puede Estar En Blanco!!!');
  end;
  //Validando Prefijo
  if (Length(Trim(CmbPrefijoRif.Text)) = 0) then
  begin
    ListaErrores.Add('Debe Asignar un Prefijo Ej. "E" o "V"!!!');
  end;
  //Validando Cedula
  if (Length(Trim(TxtRif.Text)) = 0) then
  begin
    ListaErrores.Add('La C�dula No Puede Estar En Blanco!!!');
  end;
  //Validando Nit
  if (Length(Trim(TxtNit.Text)) = 0) then
  begin
    ListaErrores.Add('El Nit No Puede Estar En Blanco!!!');
  end;
  //Validando Sexo
  if (Length(Trim(CmbSexo.Text)) = 0) then
  begin
    ListaErrores.Add('El Campo Sexo No Puede Estar En Blanco!!!');
  end;
  //Validando Razon Social
  if (Length(Trim(TxtRazonSocial.Text)) = 0) then
  begin
    ListaErrores.Add('La Raz�n Social No Puede Estar En Blanco!!!');
  end;
  //Validando Telefonos
  if (Length(Trim(TxtTelefonos.Text)) = 0) then
  begin
    ListaErrores.Add('Los Tel�fonos No Puede Estar En Blanco!!!');
  end;
  //Validando Codigo Vendedor
  if (Length(Trim(CmbCodigoVendedor.Text)) = 0) then
  begin
    ListaErrores.Add('Debe Seleccionar el Vendedor!!!');
  end;
  //Validando Codigo Zona
  if (Length(Trim(CmbCodigoZona.Text)) = 0) then
  begin
    ListaErrores.Add('Debe Seleccionar La Zona!!!');
  end;
  //Validando Esquema de Pago
  if (Length(Trim(CmbCodigoZona.Text)) = 0) then
  begin
    ListaErrores.Add('Debe Seleccionar el Esquema de Pago!!!');
  end;
  //Validando Direccion Fiscal
  if (Length(Trim(TxtDireccionFiscal1.Text)) = 0) then
  begin
    ListaErrores.Add('La Direcci�n Fiscal No Puede Estar En Blanco!!!');
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
