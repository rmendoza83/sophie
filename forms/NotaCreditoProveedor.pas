unit NotaCreditoProveedor;

{$I sophie.inc}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GenericoDetalle, DB, DBClient, ImgList, XPMan, ComCtrls, Grids,
  DBGrids, ExtCtrls, ToolWin, rxCurrEdit, Mask, rxToolEdit, StdCtrls, Buttons,
  RxLookup, Utils, ClsNotaCreditoProveedor, ClsProveedor, ClsEstacion,ClsZona,
  ClsMoneda, ClsReporte, ClsParametro, ClsBusquedaNotaCreditoProveedor,
  GenericoBuscador, ClsDetNotaCreditoProveedor, ClsDocumentoLegal,
  ClsDocumentoAnulado, ClsDetMovimientoInventario, ClsCuentaxPagar,
  ImageList;

type
  TFrmNotaCreditoProveedor = class(TFrmGenericoDetalle)
    LblNumero: TLabel;
    TxtNumero: TEdit;
    LblNumeroFacturaAsociada: TLabel;
    TxtNumeroFacturaAsociada: TEdit;
    LblCodigoProveedor: TLabel;
    CmbCodigoProveedor: TRxDBLookupCombo;
    BtnBuscarProveedor: TBitBtn;
    LblCodigoEstacion: TLabel;
    CmbCodigoEstacion: TRxDBLookupCombo;
    BtnBuscarEstacion: TBitBtn;
    LblCodigoZona: TLabel;
    CmbCodigoZona: TRxDBLookupCombo;
    BtnBuscarZona: TBitBtn;
    LblMoneda: TLabel;
    CmbCodigoMoneda: TRxDBLookupCombo;
    BtnBuscarMoneda: TBitBtn;
    DatFechaIngreso: TDateEdit;
    LblRecepcion: TLabel;
    DatFechaRecepcion: TDateEdit;
    LblFechaIngreso: TLabel;
    LblFechaContable: TLabel;
    LblFechaLibros: TLabel;
    DatFechaLibros: TDateEdit;
    DatFechaContable: TDateEdit;
    LblMontoNeto: TLabel;
    LblMontoDescuento: TLabel;
    LblSubTotal: TLabel;
    LblImpuesto: TLabel;
    LblTotal: TLabel;
    TxtTotal: TCurrencyEdit;
    TxtImpuesto: TCurrencyEdit;
    TxtSubTotal: TCurrencyEdit;
    TxtMontoDescuento: TCurrencyEdit;
    TxtMontoNeto: TCurrencyEdit;
    DSMoneda: TDataSource;
    DSZona: TDataSource;
    DSEstacion: TDataSource;
    DSProveedor: TDataSource;
    TabConcepto: TTabSheet;
    TxtConcepto: TRichEdit;
    TabObservaciones: TTabSheet;
    TxtObservaciones: TRichEdit;
    Datacodigo_producto: TStringField;
    Datareferencia: TStringField;
    Datadescripcion: TStringField;
    Datacantidad: TFloatField;
    Dataprecio: TFloatField;
    Datap_descuento: TFloatField;
    Datamonto_descuento: TFloatField;
    Dataimpuesto: TFloatField;
    Datap_iva: TFloatField;
    Datatotal: TFloatField;
    Dataid_nota_credito_cliente: TIntegerField;
    TxtPIVA: TLabel;
    TxtPDescuento: TLabel;
    procedure DataAfterDelete(DataSet: TDataSet);
    procedure TxtMontoNetoExit(Sender: TObject);
    procedure TxtNumeroFacturaAsociadaExit(Sender: TObject);
    procedure BtnBuscarMonedaClick(Sender: TObject);
    procedure BtnBuscarEstacionClick(Sender: TObject);
    procedure BtnBuscarZonaClick(Sender: TObject);
    procedure BtnBuscarProveedorClick(Sender: TObject);
    procedure CmbCodigoProveedorExit(Sender: TObject);
    procedure CmbCodigoProveedorChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DataAfterPost(DataSet: TDataSet);
    procedure TxtPDescuentoDblClick(Sender: TObject);
    procedure LblImpuestoDblClick(Sender: TObject);
    procedure LblMontoDescuentoDblClick(Sender: TObject);
    procedure LblMontoNetoDblClick(Sender: TObject);
    procedure TxtPIVADblClick(Sender: TObject);
    procedure TBtnEliminarClick(Sender: TObject);
  private
    { Private declarations }
    Clase: TNotaCreditoProveedor;
    DetNotaCreditoProveedor: TDetnotaCreditoProveedor;
    MovimientoInventario: TDetMovimientoInventario;
    CuentaxPagar: TCuentaxPagar;
   // PagoPagado: TPagoPagado;
    BuscadorNotaCreditoProveedor: TBusquedaNotaCreditoProveedor;
    Proveedor: TProveedor;
    Estacion: TEstacion;
    Zona: TZona;
    Moneda: TMoneda;
    Reporte: TReporte;
    Parametro: TParametro;
    DocumentoLegal: TDocumentoLegal;
    DocumentoAnulado: TDocumentoAnulado;
    DataBuscadorNotaCreditoProveedor: TClientDataSet;
    DataProveedor: TClientDataSet;
    DataEstacion: TClientDataSet;
    DataZona: TClientDataSet;
    DataMoneda: TClientDataSet;
    procedure Agregar;
    procedure Editar;
    procedure Eliminar;
    procedure Guardar;
    procedure Cancelar;
    procedure Refrescar;
    procedure Imprimir;
    procedure Buscar;
    procedure Limpiar;
    procedure Asignar;
    procedure Obtener;
    procedure Mover;
    procedure GuardarDetalles;
    procedure CalcularMontos;
    procedure CalcularMontosCabecera;
    procedure GuardarCuentaxPagar;
  protected
    { Protected declarations }
    function Validar: Boolean; override;
    procedure Detalle; override;
  public
    { Public declarations }
  end;

var
  FrmNotaCreditoProveedor: TFrmNotaCreditoProveedor;

implementation

uses
  ActividadSophie,
  PModule,
  ClsGenericoBD,
  ClsFacturacionProveedor,
  ClsDetFacturacionProveedor,
  AgregarDetalleNotaCreditoProveedor,
  SolicitarMonto,
  DocumentoLegal
  {$IFDEF USE_RAVE_REPORTS}
  , DMRR
  {$ENDIF}
  {$IFDEF USE_FAST_REPORTS}
  , DMFR
  {$ENDIF}
  ;

{$R *.dfm}

{ TFrmNotaCreditoProveedor }

procedure TFrmNotaCreditoProveedor.Agregar;
begin
  //Verificando
  Limpiar;
  //Asignando Valores por Defecto
  TxtPIVA.Caption := FormatFloat('#,##0.0000%',Parametro.ObtenerValor('p_iva'));
  Clase.SetPIva(Parametro.ObtenerValor('p_iva'));
  CmbCodigoZona.Value := Parametro.ObtenerValor('codigo_zona');
  CmbCodigoMoneda.Value := Parametro.ObtenerValor('codigo_moneda');
  CmbCodigoProveedor.SetFocus;
end;

procedure TFrmNotaCreditoProveedor.Asignar;
begin
  if (Clase.GetFormaLibre) then
  begin
    TabDetalle.TabVisible := False;
    PageDetalles.ActivePage := TabConcepto;
    TxtMontoNeto.ReadOnly := False;
  end
  else
  begin
    TabDetalle.TabVisible := True;
    PageDetalles.ActivePage := TabDetalle;
    TxtMontoNeto.ReadOnly := True;
  end;
  with Clase do
  begin
    TxtNumero.Text := GetNumero;
    //TxtNumeroFacturaAsociada.Text := GetNumeroFacturaAsociada;
    CmbCodigoProveedor.KeyValue := GetCodigoProveedor;
    //CmbCodigoVendedor.KeyValue := GetCodigoVendedor;
    CmbCodigoEstacion.KeyValue := GetCodigoEstacion;
    CmbCodigoZona.KeyValue := GetCodigoZona;
    CmbCodigoMoneda.KeyValue := GetCodigoMoneda;
    DatFechaIngreso.Date := GetFechaIngreso;
    DatFechaRecepcion.Date := GetFechaRecepcion;
    DatFechaContable.Date := GetFechaContable;
    DatFechaLibros.Date := GetFechaLibros;
    TxtConcepto.Text := GetConcepto;
    TxtObservaciones.Text := GetObservaciones;
    TxtMontoNeto.Value := GetMontoNeto;
    TxtMontoDescuento.Value := GetMontoDescuento;
    TxtSubTotal.Value := GetSubTotal;
    TxtImpuesto.Value := GetImpuesto;
    TxtTotal.Value := GetTotal;
    TxtPDescuento.Caption := FormatFloat('###0.0000%',GetPDescuento);
    TxtPIVA.Caption := FormatFloat('###0.0000%',GetPIva);
    Data.CloneCursor(DetNotaCreditoProveedor.ObtenerIdNotaCreditoProveedor(GetIdNotaCreditoProveedor),False,True);
    DS.DataSet := Data;
  end;
end;

procedure TFrmNotaCreditoProveedor.BtnBuscarEstacionClick(Sender: TObject);
begin
  Buscador(Self,Estacion,'codigo','Buscar Estacion',nil,DataEstacion);
  CmbCodigoEstacion.KeyValue := DataEstacion.FieldValues['codigo'];
end;

procedure TFrmNotaCreditoProveedor.BtnBuscarMonedaClick(Sender: TObject);
begin
  Buscador(Self,Moneda,'codigo','Buscar Moneda',nil,DataMoneda);
  CmbCodigoMoneda.KeyValue := DataMoneda.FieldValues['codigo'];
end;

procedure TFrmNotaCreditoProveedor.BtnBuscarProveedorClick(Sender: TObject);
begin
  Buscador(Self,Proveedor,'codigo','Buscar Proveedor',nil,DataProveedor);
  CmbCodigoProveedor.KeyValue := DataProveedor.FieldValues['codigo'];
  CmbCodigoProveedorExit(Sender);
end;

procedure TFrmNotaCreditoProveedor.BtnBuscarZonaClick(Sender: TObject);
begin
  Buscador(Self,Zona,'codigo','Buscar Zona',nil,DataZona);
  CmbCodigoZona.KeyValue := DataZona.FieldValues['codigo'];
end;

procedure TFrmNotaCreditoProveedor.Buscar;
begin
  if (Buscador(Self,BuscadorNotaCreditoProveedor,'numero','Buscar Nota de Credito',nil,DataBuscadorNotaCreditoProveedor)) then
  begin
    Clase.SetNumero(DataBuscadorNotaCreditoProveedor.FieldValues['numero']);
    Clase.BuscarNumero;
    Asignar;
  end;
end;

procedure TFrmNotaCreditoProveedor.CalcularMontos;
var
  Neto, Descuento, Impuesto: Currency;
begin
  Neto := 0;
  Descuento := 0;
  Impuesto := 0;
  with Data do
  begin
    DisableControls;
    First;
    while (not Eof) do
    begin
      Neto := Neto + ((FieldValues['precio'] * FieldValues['cantidad']) - FieldValues['impuesto']);
      Descuento := Descuento + FieldValues['monto_descuento'];
      Impuesto := Impuesto + FieldValues['impuesto'];
      Next;
    end;
    EnableControls;
  end;
  //Asignando Montos
  TxtMontoNeto.Value := Neto;
  //Determinando si hay un Descuento Manual
  if ((TxtMontoDescuento.Value > 0) and (StrToFloat(StringReplace(TxtPDescuento.Caption,'%','',[rfReplaceAll])) > 0) and (Descuento = 0)) then
  begin
    Descuento := TxtMontoDescuento.Value;
  end;
  TxtMontoDescuento.Value := Descuento;
  TxtSubTotal.Value := Neto - Descuento;
  TxtImpuesto.Value := Impuesto;
  TxtTotal.Value := (Neto - Descuento) + Impuesto;
end;

procedure TFrmNotaCreditoProveedor.CalcularMontosCabecera;
begin
  TxtSubTotal.Value := TxtMontoNeto.Value - TxtMontoDescuento.Value;
  TxtTotal.Value := TxtSubTotal.Value + TxtImpuesto.Value;
end;

procedure TFrmNotaCreditoProveedor.GuardarCuentaxPagar;
begin
  if (Editando) then
  begin
    CuentaxPagar.EliminarIdPago(Clase.GetIdPago);
  end;
  with CuentaxPagar do //Se Inserta la CuentaxPagar
  begin
    SetDesde('NCP');
    SetNumeroDocumento(Clase.GetNumero);
    SetCodigoMoneda(Clase.GetCodigoMoneda);
    SetCodigoProveedor(Clase.GetCodigoProveedor);
    SetCodigoZona(Clase.GetCodigoZona);
    SetFechaIngreso(Clase.GetFechaIngreso);
    SetFechaVencIngreso(Clase.GetFechaIngreso);
    SetFechaRecepcion(Clase.GetFechaRecepcion);
    SetFechaVencRecepcion(Clase.GetFechaRecepcion);
    SetFechaContable(Clase.GetFechaContable);
    SetFechaPago(Clase.GetFechaIngreso);
    SetFechaLibros(Clase.GetFechaLibros);
    SetNumeroCuota(-1);
    SetTotalCuotas(-1);
    SetOrdenPago(1);
    SetMontoNeto(Clase.GetMontoNeto);
    SetImpuesto(Clase.GetImpuesto);
    SetTotal(Clase.GetTotal);
    SetPIva(Clase.GetPIva);
    SetIdPago(Clase.GetIdPago);
    Insertar;
  end;
end;

procedure TFrmNotaCreditoProveedor.Cancelar;
begin
  if (Agregando) then
  begin
    Limpiar;
  end
  else
  begin
    Clase.SetNumero(TxtNumero.Text);
    Clase.BuscarNumero;
    Asignar;
  end;
end;

procedure TFrmNotaCreditoProveedor.CmbCodigoProveedorChange(Sender: TObject);
begin
  inherited;
  SB.Panels[0].Text := DataProveedor.FieldValues['razon_social'];
end;

procedure TFrmNotaCreditoProveedor.CmbCodigoProveedorExit(Sender: TObject);
begin
  if (Length(Trim(CmbCodigoProveedor.Text)) > 0) then
  begin
    if (Agregando) then
    begin
      Proveedor.SetCodigo(CmbCodigoProveedor.Value);
      Proveedor.BuscarCodigo;
      CmbCodigoZona.Value := Proveedor.GetCodigoZona;
    end;
  end;
end;

procedure TFrmNotaCreditoProveedor.DataAfterDelete(DataSet: TDataSet);
begin
  inherited;
  CalcularMontos;
end;

procedure TFrmNotaCreditoProveedor.DataAfterPost(DataSet: TDataSet);
begin
  inherited;
  CalcularMontos;
end;

procedure TFrmNotaCreditoProveedor.Detalle;
begin
  inherited;
  FrmAgregarDetalleNotaCreditoProveedor := TFrmAgregarDetalleNotaCreditoProveedor.CrearDetalle(Self,Data);
  FrmAgregarDetalleNotaCreditoProveedor.ShowModal;
  FreeAndNil(FrmAgregarDetalleNotaCreditoProveedor);
end;

procedure TFrmNotaCreditoProveedor.Editar;
begin
  if (Length(Trim(TxtNumero.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
end;

procedure TFrmNotaCreditoProveedor.Eliminar;
begin
  //Se Busca La Nota de Cr�dito!
  Clase.SetNumero(TxtNumero.Text);
  Clase.BuscarNumero;
  //Se Registra El Documento Anulado y Eliminan Registros del Documento Legal
  DocumentoLegal.SetDesde('NCP');
  DocumentoLegal.SetNumeroDocumento(Clase.GetNumero);
  if (DocumentoLegal.Buscar) then
  begin
    //Registrando El Documento Anulado
    with DocumentoAnulado do
    begin
      SetDesde('NCP');
      SetNumeroDocumento(Clase.GetNumero);
      SetSerie(DocumentoLegal.GetSerie);
      SetNumeroControl(DocumentoLegal.GetNumeroControl);
      SetFechaDocumento(Clase.GetFechaIngreso);
      SetCodigoCliente('');
      SetCodigoProveedor(Clase.GetCodigoProveedor);
      SetCodigoZona(Clase.GetCodigoZona);
      SetSubTotal(Clase.GetSubTotal);
      SetImpuesto(Clase.GetImpuesto);
      SetTotal(Clase.GetTotal);
      SetPIva(Clase.GetPIva);
      SetLoginUsuario(P.User.GetLoginUsuario);
      SetFecha(Date);
      SetHora(Time);
      Insertar;
    end;
    //Borrando Documento Legal
    DocumentoLegal.Eliminar;
  end;
  Clase.Eliminar;
  MessageBox(Self.Handle, 'La Nota de Cr�dito de Proveedor Se Ha Eliminado Con Exito!!!',PChar(MsgTituloInformacion), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  Refrescar;
  Limpiar;
end;

procedure TFrmNotaCreditoProveedor.FormCreate(Sender: TObject);
begin
  inherited;
  ProcAgregar := Agregar;
  ProcEditar := Editar;
  ProcEliminar := Eliminar;
  ProcGuardar := Guardar;
  ProcCancelar := Cancelar;
  ProcRefrescar := Refrescar;
  ProcImprimir := Imprimir;
  ProcBuscar := Buscar;
  FrmActividadSophie.Show;
  FrmActividadSophie.Actividad('Cargando N�cleo...');
  Clase := TNotaCreditoProveedor.Create;
  //Inicializando DataSets Secundarios
  BuscadorNotaCreditoProveedor := TBusquedaNotaCreditoProveedor.Create;
  Proveedor := TProveedor.Create;
  Estacion := TEstacion.Create;
  Zona := TZona.Create;
  Moneda := TMoneda.Create;
  Reporte := TReporte.Create;
  Parametro := TParametro.Create;
  DetNotaCreditoProveedor := TDetNotaCreditoProveedor.Create;
  CuentaxPagar := TCuentaxPagar.Create;
  DocumentoLegal := TDocumentoLegal.Create;
  DocumentoAnulado := TDocumentoAnulado.Create;
  FrmActividadSophie.Actividad('Cargando Proveedores...');
  DataProveedor := Proveedor.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Estaciones...');
  DataEstacion := Estacion.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Zonas...');
  DataZona := Zona.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Monedas...');
  DataMoneda := Moneda.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Resto del N�cleo...');
  DataBuscadorNotaCreditoProveedor := BuscadorNotaCreditoProveedor.ObtenerLista;
  DataProveedor.First;
  DataEstacion.First;
  DataZona.First;
  DataMoneda.First;
  DSProveedor.DataSet := DataProveedor;
  DSEstacion.DataSet := DataEstacion;
  DSZona.DataSet := DataZona;
  DSMoneda.DataSet := DataMoneda;
  ConfigurarRxDBLookupCombo(CmbCodigoProveedor,DSProveedor,'codigo','codigo;razon_social');
  ConfigurarRxDBLookupCombo(CmbCodigoEstacion,DSEstacion,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoZona,DSZona,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoMoneda,DSMoneda,'codigo','codigo;descripcion');
  DS.DataSet := Data;
  GridDetalle.DataSource := DS;
  FrmActividadSophie.Close;
   //Status Bar
  SB.Panels[0].Text := '';
  Data.EmptyDataSet;
  //Formato Concepto Libre
  TxtMontoNeto.ReadOnly := True;
  TabDetalle.TabVisible := True;
  PageDetalles.ActivePage := TabDetalle;
  //Buscar;}
end;

procedure TFrmNotaCreditoProveedor.Guardar;
var
  SwDocumentoLegal: Boolean;
begin
  CalcularMontosCabecera;
  //Data.EmptyDataSet;
  Obtener;
  if (Agregando) then
  begin
    //Agregando...
    //Guardando Encabezado
    Clase.Insertar;
    //Solicitando Datos Legales
    FrmDocumentoLegal := TFrmDocumentoLegal.CreateInsert(Self,'NCP',Clase.GetNumero,'','');
    FrmDocumentoLegal.ShowModal;
    if FrmDocumentoLegal.Aceptado then
    begin //Se Guardan Los Datos Legales
      DocumentoLegal.SetDesde(FrmDocumentoLegal.Desde);
      DocumentoLegal.SetNumeroDocumento(Clase.GetNumero);
      DocumentoLegal.SetSerie(FrmDocumentoLegal.Serie);
      DocumentoLegal.SetNumeroControl(FrmDocumentoLegal.NumeroDocumento);
      DocumentoLegal.SetLoginUsuario(P.User.GetLoginUsuario);
      DocumentoLegal.SetFecha(Date);
      DocumentoLegal.SetHora(Time);
      DocumentoLegal.Insertar;
    end;
    FreeAndNil(FrmDocumentoLegal);
    Application.ProcessMessages;
    //Guardando Detalles
    GuardarDetalles;
    //Guardando Pago Pagado
//    GuardarPagoPagado;
    //Guardando Cuenta x Pagar
    GuardarCuentaxPagar;
    //Guardando Movimiento de Inventario
//    GuardarMovimientoInventario;

    MessageBox(Self.Handle, 'La Nota de Cr�dito Ha Sido Agregada Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    //Se Imprime la Nota de D�bito
    MessageBox(Self.Handle, 'Presione "Aceptar" Para Proceder A Imprimir La Nota de Cr�dito de Proveedor!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    with Reporte do
    begin
      AgregarParametro('NumeroNotaCreditoProveedor',Clase.GetNumero);
      AgregarParametro('MontoLetra',NumeroALetras(Clase.GetTotal));
      begin
        ImprimeReporte('NotaCreditoProveedor',trDocumento);
      end;
    end;
  end
  else
  begin
    //Editando...
    Clase.Modificar;
    //Solicitando Datos Legales
    DocumentoLegal.SetDesde('NCP');
    DocumentoLegal.SetNumeroDocumento(Clase.GetNumero);
    SwDocumentoLegal := DocumentoLegal.Buscar;
    if (SwDocumentoLegal) then
    begin
      FrmDocumentoLegal := TFrmDocumentoLegal.CreateEdit(Self,'NCP',Clase.GetNumero,DocumentoLegal.GetSerie,DocumentoLegal.GetNumeroControl);
    end
    else
    begin
      FrmDocumentoLegal := TFrmDocumentoLegal.CreateInsert(Self,'NCP',Clase.GetNumero,'','');
    end;
    FrmDocumentoLegal.ShowModal;
    if FrmDocumentoLegal.Aceptado then
    begin //Se Guardan Los Datos Legales
      DocumentoLegal.SetSerie(FrmDocumentoLegal.Serie);
      DocumentoLegal.SetNumeroControl(FrmDocumentoLegal.NumeroControl);
      DocumentoLegal.SetLoginUsuario(P.User.GetLoginUsuario);
      DocumentoLegal.SetFecha(Date);
      DocumentoLegal.SetHora(Time);
      if (SwDocumentoLegal) then
      begin
        DocumentoLegal.Modificar;
      end
      else
      begin
        DocumentoLegal.Insertar;
      end;
    end;
    FreeAndNil(FrmDocumentoLegal);
    //Actualizando Detalles (Eliminando Anteriores y Cargando los Nuevos)
    DetNotaCreditoProveedor.EliminarIdNotaCreditoProveedor(Clase.GetIdNotaCreditoProveedor);
    //Guardando Detalles
    GuardarDetalles;
    //Guardando Pago Pagado
//    GuardarPagoPagado;
    //Guardando Cuenta x Pagar
    GuardarCuentaxPagar;
    //Guardando Movimiento de Inventario
//    GuardarMovimientoInventario;
    MessageBox(Self.Handle, 'La Nota de Cr�dito de Proveedor Ha Sido Editada Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end;
  PageDetalles.ActivePage := TabConcepto;
end;

procedure TFrmNotaCreditoProveedor.GuardarDetalles;
begin
  with Data do
  begin
    DisableControls;
    First;
    while (not Eof) do
    begin
      with DetNotaCreditoProveedor do
      begin
        SetCodigoProducto(FieldValues['codigo_producto']);
        SetReferencia(FieldValues['referencia']);
        SetDescripcion(FieldValues['descripcion']);
        SetCantidad(FieldValues['cantidad']);
        SetPrecio(FieldValues['precio']);
        SetPDescuento(FieldValues['p_descuento']);
        SetMontoDescuento(FieldValues['monto_descuento']);
        SetImpuesto(FieldValues['impuesto']);
        SetPIva(FieldValues['p_iva']);
        SetTotal(FieldValues['total']);
        SetIdNotaCreditoProveedor(Clase.GetIdNotaCreditoProveedor);
        Insertar;
      end;
      Next;
    end;
    EnableControls;
  end;
end;

procedure TFrmNotaCreditoProveedor.Imprimir;
begin
  if (Length(Trim(TxtNumero.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
  with Reporte do
  begin
    AgregarParametro('NumeroNotaCreditoProveedor',Clase.GetNumero);
    AgregarParametro('MontoLetra',NumeroALetras(Clase.GetTotal));
    EmiteReporte('NotaCreditoProveedorFormaLibre',trDocumento);
  end;
end;

procedure TFrmNotaCreditoProveedor.LblImpuestoDblClick(Sender: TObject);
var
  Aux: Currency;
begin
  if (Clase.GetFormaLibre) then
  begin
    Aux := 0;
    Solicitar(Self,'Impuesto',TxtImpuesto.Value,Aux);
    if (Aux >= 0) then
    begin
      TxtImpuesto.Value := Aux;
      CalcularMontosCabecera;
    end;
  end
  else
  begin
    MessageBox(Self.Handle, 'La Nota de Cr�dito No Es de Forma "Concepto Libre"', PChar(MsgTituloError), MB_ICONSTOP or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end;
end;

procedure TFrmNotaCreditoProveedor.LblMontoDescuentoDblClick(Sender: TObject);
var
  Aux: Currency;
begin
  Aux := 0;
  Solicitar(Self,'Monto Descuento',TxtMontoDescuento.Value,Aux);
  if (Aux >= 0) then
  begin
    TxtMontoDescuento.Value := Aux;
    TxtPDescuento.Caption := FormatFloat('#,##0.0000%',Aux * 100 / TxtMontoNeto.Value);
    if (Clase.GetFormaLibre) then
    begin
      CalcularMontosCabecera;
    end
    else
    begin
      CalcularMontos;
    end;
  end;
end;

procedure TFrmNotaCreditoProveedor.LblMontoNetoDblClick(Sender: TObject);
begin
  inherited;
  if (Data.IsEmpty) then
  begin
    if (MessageBox(Self.Handle, '�Desea Activar Forma Concepto Libre?', PChar(MsgTituloInformacion), MB_ICONQUESTION or MB_YESNO or MB_TOPMOST or MB_TASKMODAL) = mrYes) then
    begin
      Clase.SetFormaLibre(True);
      TabDetalle.TabVisible := False;
      PageDetalles.ActivePage := TabConcepto;
      TxtMontoNeto.ReadOnly := False;
    end
    else
    begin
      Clase.SetFormaLibre(False);
      TabDetalle.TabVisible := True;
      PageDetalles.ActivePage := TabDetalle;
      TxtMontoNeto.ReadOnly := True;
      TxtMontoNeto.Value := 0;
      TxtMontoDescuento.Value := 0;
      TxtPDescuento.Caption := FormatFloat('#,##0.0000%',0);
      TxtSubTotal.Value := 0;
      TxtImpuesto.Value := 0;
      TxtTotal.Value := 0;
    end;
  end;
end;

procedure TFrmNotaCreditoProveedor.Limpiar;
begin
  TxtNumero.Text := '';
  CmbCodigoProveedor.KeyValue := '';
  CmbCodigoZona.KeyValue := '';
  CmbCodigoMoneda.KeyValue := '';
  DatFechaIngreso.Date := Date;
  DatFechaRecepcion.Date := Date;
  DatFechaContable.Date := Date;
  DatFechaLibros.Date := Date;
  TxtConcepto.Text := '';
  TxtObservaciones.Text := '';
  TxtMontoNeto.Value := 0;
  TxtMontoDescuento.Value := 0;
  TxtSubTotal.Value := 0;
  TxtImpuesto.Value := 0;
  TxtTotal.Value := 0;
  TxtPDescuento.Caption := FormatFloat('###0.0000%',0);
  TxtPIVA.Caption := FormatFloat('###0.0000%',0);
  //Status Bar
  SB.Panels[0].Text := '';
  Data.EmptyDataSet;
  //Formato Concepto Libre
  TxtMontoNeto.ReadOnly := True;
  TabDetalle.TabVisible := True;
  PageDetalles.ActivePage := TabDetalle;
  Clase.SetFormaLibre(False);
end;

procedure TFrmNotaCreditoProveedor.Mover;
begin

end;

procedure TFrmNotaCreditoProveedor.Obtener;
begin
  with Clase do
  begin
    SetNumero(TxtNumero.Text);
    SetCodigoProveedor(CmbCodigoProveedor.KeyValue);
    SetCodigoZona(CmbCodigoZona.KeyValue);
    SetCodigoEstacion(CmbCodigoEstacion.KeyValue);
    SetCodigoMoneda(CmbCodigoMoneda.KeyValue);
    SetFechaIngreso(DatFechaIngreso.Date);
    SetFechaRecepcion(DatFechaRecepcion.Date);
    SetFechaContable(DatFechaContable.Date);
    SetFechaLibros(DatFechaLibros.Date);
    SetConcepto(TxtConcepto.Text);
    SetObservaciones(TxtObservaciones.Text);
    SetMontoNeto(TxtMontoNeto.Value);
    SetMontoDescuento(TxtMontoDescuento.Value);
    SetSubTotal(TxtSubTotal.Value);
    SetImpuesto(TxtImpuesto.Value);
    SetTotal(TxtTotal.Value);
    SetPDescuento(FormatearFloat(StringReplace(TxtPDescuento.Caption,'%','',[rfReplaceAll])));
    SetPIva(FormatearFloat(StringReplace(TxtPIVA.Caption,'%','',[rfReplaceAll])));
    SetTasaDolar(0); //***//
    SetLoginUsuario(P.User.GetLoginUsuario);
    if (Agregando) then
    begin
      SetTiempoIngreso(Now);
      SetIdNotaCreditoProveedor(-1);
      SetIdPago(-1);
      //SetIdContadoPago(-1);
    end;
  end;
end;

procedure TFrmNotaCreditoProveedor.Refrescar;
begin
  //Se Actualizan Los demas Componentes
  DataProveedor := Proveedor.ObtenerCombo;
  DataZona := Zona.ObtenerCombo;
  DataMoneda := Moneda.ObtenerCombo;
  DataBuscadorNotaCreditoProveedor := BuscadorNotaCreditoProveedor.ObtenerLista;
  DataProveedor.First;
  DataZona.First;
  DataMoneda.First;
  DSProveedor.DataSet := DataProveedor;
  DSZona.DataSet := DataZona;
  DSMoneda.DataSet := DataMoneda;
end;

procedure TFrmNotaCreditoProveedor.TBtnEliminarClick(Sender: TObject);
begin
  if (Length(Trim(TxtNumero.Text)) > 0) then
  begin
    inherited;
  end
  else
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
end;

procedure TFrmNotaCreditoProveedor.TxtMontoNetoExit(Sender: TObject);
begin
  CalcularMontosCabecera;
end;

procedure TFrmNotaCreditoProveedor.TxtNumeroFacturaAsociadaExit(
  Sender: TObject);
var
  FacturacionProveedor: TFacturacionProveedor;
  DetFacturacionCliente: TDetFacturacionProveedor;
  DataDetFacturacionProveedor: TClientDataSet;
begin
  if (Agregando) then
  begin
   { Clase.SetNumeroFacturaAsociada(Trim(TxtNumeroFacturaAsociada.Text));
    if (not Clase.BuscarNumeroFacturaAsociada) then
    begin
      if (Length(Trim(TxtNumeroFacturaAsociada.Text)) > 0) then
      begin
        FacturacionCliente := TFacturacionCliente.Create;
        FacturacionCliente.SetNumero(Trim(TxtNumeroFacturaAsociada.Text));
        if (FacturacionCliente.BuscarNumero) then
        begin
          with Clase do
          begin
            SetNumero('');
            SetNumeroFacturaAsociada(TxtNumeroFacturaAsociada.Text);
            SetCodigoCliente(FacturacionCliente.GetCodigoCliente);
            SetCodigoVendedor(FacturacionCliente.GetCodigoVendedor);
            SetCodigoEstacion(Parametro.ObtenerValor('codigo_estacion_entrada'));
            SetCodigoZona(FacturacionCliente.GetCodigoZona);
            SetCodigoMoneda(FacturacionCliente.GetCodigoMoneda);
            SetFechaIngreso(Date);
            SetFechaRecepcion(Date);
            SetFechaContable(Date);
            SetFechaLibros(Date);
            SetConcepto(FacturacionCliente.GetConcepto);
            SetObservaciones(FacturacionCliente.GetObservaciones);
            SetMontoNeto(FacturacionCliente.GetMontoNeto);
            SetMontoDescuento(FacturacionCliente.GetMontoDescuento);
            SetSubTotal(FacturacionCliente.GetSubTotal);
            SetImpuesto(FacturacionCliente.GetImpuesto);
            SetTotal(FacturacionCliente.GetTotal);
            SetPDescuento(FacturacionCliente.GetPDescuento);
            SetPIva(FacturacionCliente.GetPIva);
            SetTasaDolar(0); //***//
            SetLoginUsuario(P.User.GetLoginUsuario);
            if (Agregando) then
            begin
              SetTiempoIngreso(Now);
              SetIdNotaCreditoCliente(-1);
              SetIdCobranza(-1);
              SetIdMovimientoInventario(-1);
              //SetIdContadoCobranza(-1);
            end;
            Asignar;
            DetFacturacionCliente := TDetFacturacionCliente.Create;
            DataDetFacturacionCliente := DetFacturacionCliente.ObtenerIdFacturacionCliente(FacturacionCliente.GetIdFacturacionCliente);
            with DataDetFacturacionCliente do
            begin
              First;
              while (not Eof) do
              begin
                Self.Data.Insert;
                Self.Data.FieldValues['codigo_producto'] := FieldValues['codigo_producto'];
                Self.Data.FieldValues['referencia'] := FieldValues['referencia'];
                Self.Data.FieldValues['descripcion'] := FieldValues['descripcion'];
                Self.Data.FieldValues['cantidad'] := FieldValues['cantidad'];
                Self.Data.FieldValues['precio'] := FieldValues['precio'];
                Self.Data.FieldValues['p_descuento'] := FieldValues['p_descuento'];
                Self.Data.FieldValues['monto_descuento'] := FieldValues['monto_descuento'];
                Self.Data.FieldValues['impuesto'] := FieldValues['impuesto'];
                Self.Data.FieldValues['p_iva'] := FieldValues['p_iva'];
                Self.Data.FieldValues['total'] := FieldValues['total'];
                Self.Data.FieldValues['id_nota_credito_cliente'] := -1;
                Self.Data.Post;
                Next;
              end;
            end;
            DS.DataSet := Data;
          end;
        end;
      end;
    end
    else
    begin
      MessageBox(Self.Handle, 'El N�mero de Factura ya esta Asociado una Nota de Cr�dito ', PChar(MsgTituloInformacion), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      Limpiar;
    end;}
  end;
end;

procedure TFrmNotaCreditoProveedor.TxtPDescuentoDblClick(Sender: TObject);
var
  Aux: Currency;
begin
  Aux := 0;
  Solicitar(Self,'(%) Descuento',StrToFloat(StringReplace(TxtPDescuento.Caption,'%','',[rfReplaceAll])),Aux);
  if (Aux >= 0) then
  begin
    TxtPDescuento.Caption := FormatFloat('#,##0.0000%',Aux);
    TxtMontoDescuento.Value := Aux * TxtMontoNeto.Value / 100;
    if (Clase.GetFormaLibre) then
    begin
      CalcularMontosCabecera;
    end
    else
    begin
      CalcularMontos;
    end;
  end;
end;

procedure TFrmNotaCreditoProveedor.TxtPIVADblClick(Sender: TObject);
var
  Aux: Currency;
  Exentos: Boolean;
  BMark: TBookmark;
begin
  Aux := 0;
  Solicitar(Self,'(%) I.V.A.',StrToFloat(StringReplace(TxtPIVA.Caption,'%','',[rfReplaceAll])),Aux);
  if (Aux >= 0) then
  begin
    TxtPIVA.Caption := FormatFloat('#,##0.0000%',Aux);
    TxtImpuesto.Value := Aux * TxtMontoNeto.Value / 100;
    CalcularMontosCabecera;
  end
  else
  begin
    Aux := 0;
    Solicitar(Self,'(%) I.V.A.',StrToFloat(StringReplace(TxtPIVA.Caption,'%','',[rfReplaceAll])),Aux);
    if (Aux >= 0) then
    begin
      TxtPIVA.Caption := FormatFloat('#,##0.0000%',Aux);
      if (not Data.IsEmpty) then
      begin
        if (MessageBox(Self.Handle, '�Desea Actualizar El Porcentaje De Impuesto En Todos Los Renglones?', PChar(MsgTituloInformacion), MB_ICONQUESTION or MB_YESNO or MB_TOPMOST or MB_TASKMODAL) = mrYes) then
        begin
          if (MessageBox(Self.Handle, '�Incluir Renglones Exentos de I.V.A.?', PChar(MsgTituloInformacion), MB_ICONQUESTION or MB_YESNO or MB_TOPMOST or MB_TASKMODAL) = mrYes) then
          begin
            Exentos := True;
          end
          else
          begin
            Exentos := False;
          end;
          //Actualizando PIVA en Renglones
          with Data do
          begin
            DisableControls;
            First;
            while (not Eof) do
            begin
              if ((FieldValues['impuesto'] > 0) or (Exentos)) then
              begin
                //Utilizando Punteros de los Registros (Bookmarks)
                //Debido a una deficiencia en la edicion de ClientDataSets
                BMark := GetBookmark;
                Edit;
                FieldValues['p_iva'] := Aux;
                FieldValues['impuesto'] := (FieldValues['costo'] * FieldValues['cantidad']) / (FieldValues['p_iva'] / 100);
                Post;
                GotoBookmark(BMark);
              end;
              Next;
            end;
            EnableControls;
          end;
          MessageBox(Self.Handle, 'Renglones Actualizados Correctamente', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
        end;
      end;
      CalcularMontos;
    end;
  end;
end;

function TFrmNotaCreditoProveedor.Validar: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  //Validando Numero Nota de Debito
  if (Length(Trim(TxtNumero.Text)) = 0) then
  begin
    ListaErrores.Add('El Numero de la Nota de Cr�dito No Puede Estar En Blanco!!!');
  end;
  //Validando Proveedor
  if (Length(Trim(CmbCodigoProveedor.Text)) = 0) then
  begin
    ListaErrores.Add('El Proveedor No Puede Estar En Blanco!!!');
  end;
  //Validando Fechas
  if ((DatFechaIngreso.Date > DatFechaRecepcion.Date) or (DatFechaIngreso.Date > DatFechaContable.Date) or (DatFechaIngreso.Date > DatFechaLibros.Date)) then
  begin
    ListaErrores.Add('La Fecha de Ingreso Debe Ser Menor a las Fechas de Recepcion, Contable y Libros Respectivamente!!!');
  end;
  //Validando Detalle o Forma Libre
  if (Clase.GetFormaLibre) then
  begin
    if (Length(Trim(TxtConcepto.Text)) = 0) then
    begin
      ListaErrores.Add('El Concepto No Puede Estar En Blanco!!!');
    end;
    if (TxtMontoNeto.Value <= 0) then
    begin
      ListaErrores.Add('El Monto Neto Debe Ser Positivo!!!');
    end;
    if (TxtMontoDescuento.Value < 0) then
    begin
      ListaErrores.Add('El Monto de Descuento No Debe Ser Negativo!!!');
    end;
    if (TxtImpuesto.Value < 0) then
    begin
      ListaErrores.Add('El Impuesto No Debe Ser Negativo!!!');
    end;
  end
  else
  begin
    if (Data.RecordCount = 0) then
    begin
      ListaErrores.Add('El Detalle De La Nota de Cr�dito No Puede Estar Vacio!!!');
    end;
  end;
  //Validando Monto de Impuesto
  if (TxtImpuesto.Value = 0) then
  begin
    if (MessageBox(Self.Handle, '�El Valor del Impuesto esta en 0 Aun Asi Desea Continuar?', PChar(MsgTituloInformacion), MB_ICONWARNING or MB_YESNO or MB_TOPMOST or MB_TASKMODAL) = mrYes) then
    begin
      Clase.Insertar;
      TxtNumero.Text := Clase.GetNumero;
      Application.ProcessMessages;
    end
    else
    begin
      TxtImpuesto.SetFocus;
      Abort;
    end;
  end;
  //Validando Total
  if (TxtTotal.Value <= 0) then
  begin
    ListaErrores.Add('El Total Debe Ser Positivo!!!');
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
