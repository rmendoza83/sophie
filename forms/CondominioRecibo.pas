unit CondominioRecibo;

{$I sophie.inc}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GenericoDetalle, DB, DBClient, ImgList, XPMan, ComCtrls, Grids,
  DBGrids, ExtCtrls, ToolWin, RxLookup, StdCtrls, Buttons, rxCurrEdit, Mask,
  rxToolEdit, DBCtrls, Utils, ClsCondominioRecibo, ClsCondominioReciboInmueble,
  ClsCondominioReciboInmuebleGasto, ClsCondominioReciboGastoOrdinario,
  ClsCondominioConjuntoAdministracion, ClsCondominioInmueble,
  ClsCondominioPropietario, ClsCondominioGastoOrdinario, ClsCondominioGasto,
  GenericoBuscador, ClsCondominioBusquedaRecibo, ClsReporte, ClsParametro
  {$IFDEF DELPHI10}, ImageList{$ENDIF};

type
  TFrmCondominioRecibo = class(TFrmGenericoDetalle)
    LblCodigoConjuntoAdministracion: TLabel;
    LblAnno: TLabel;
    CmbCodigoConjuntoAdministracion: TRxDBLookupCombo;
    BtnBuscarConjuntoAdministracion: TBitBtn;
    LblCodigoEstacion: TLabel;
    LblFechaIngreso: TLabel;
    DatFechaIngreso: TDateEdit;
    LblFechaVencimiento: TLabel;
    DatFechaVencimiento: TDateEdit;
    LblMontoNeto: TLabel;
    LblMontoReserva: TLabel;
    LblMontoTotal: TLabel;
    TxtPReserva: TLabel;
    TxtMontoNeto: TCurrencyEdit;
    TxtMontoReserva: TCurrencyEdit;
    TxtMontoTotal: TCurrencyEdit;
    GrpDetalleInmueble: TGroupBox;
    LblInmueble: TLabel;
    CmbCodigoInmueble: TRxDBLookupCombo;
    BtnBuscarInmueble: TBitBtn;
    LblMontoNetoInmueble: TLabel;
    LblMontoReservaInmueble: TLabel;
    LblMontoTotalInmueble: TLabel;
    TxtMontoNetoInmueble: TCurrencyEdit;
    TxtMontoReservaInmueble: TCurrencyEdit;
    TxtMontoTotalInmueble: TCurrencyEdit;
    CmbMes: TComboBox;
    CmbAnno: TComboBox;
    TxtCodigoInmueble: TDBText;
    DSConjuntoAdministracion: TDataSource;
    DSInmueble: TDataSource;
    Datacodigo_inmueble: TStringField;
    Datacodigo_gasto: TStringField;
    Datadescripcion: TStringField;
    Datamonto: TCurrencyField;
    Dataextraordinario: TBooleanField;
    procedure BtnBuscarConjuntoAdministracionClick(Sender: TObject);
    procedure BtnBuscarInmuebleClick(Sender: TObject);
    procedure CmbCodigoConjuntoAdministracionChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TxtPReservaDblClick(Sender: TObject);
    procedure TBtnEliminarClick(Sender: TObject);
    procedure CmbAnnoChange(Sender: TObject);
    procedure CmbMesChange(Sender: TObject);
    procedure CmbCodigoInmuebleChange(Sender: TObject);
    procedure GridDetalleDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
  private
    { Private declarations }
    Clase: TCondominioRecibo;
    CondominioReciboInmueble: TCondominioReciboInmueble;
    CondominioReciboInmuebleGasto: TCondominioReciboInmuebleGasto;
    CondominioReciboGastoOrdinario: TCondominioReciboGastoOrdinario;
    CondominioConjuntoAdministracion: TCondominioConjuntoAdministracion;
    CondominioInmueble: TCondominioInmueble;
    CondominioPropietario: TCondominioPropietario;
    CondominioGastoOrdinario: TCondominioGastoOrdinario;
    CondominioGasto: TCondominioGasto;
    BuscadorCondominioRecibo: TCondominioBusquedaRecibo;
    Reporte: TReporte;
    Parametro: TParametro;
    DataCondominioBuscadorRecibo: TClientDataSet;
    DataCondominioConjuntoAdministracion: TClientDataSet;
    DataCondominioInmueble: TClientDataSet;
    PReserva: Currency;
    procedure Agregar;
    procedure Editar;
    procedure Eliminar;
    procedure Guardar;
    procedure Cancelar;
    procedure Refrescar;
    procedure Imprimir;
    procedure Buscar;
    procedure Limpiar;
    procedure Asignar;
    procedure Obtener;
    procedure Mover;
    procedure GuardarDetalles;
    procedure CalcularMontos;
    procedure HabilitarDetalleInmueble;
    procedure CargarDetalleInmueble;
  protected
    { Protected declarations }
    function Validar: Boolean; override;
    procedure Detalle; override;
  public
    { Public declarations }
  end;

var
  FrmCondominioRecibo: TFrmCondominioRecibo;

implementation

uses
  ActividadSophie,
  PModule,
  CondominioAgregarDetalleRecibo,
  SolicitarMonto,
  ClsGenericoBD
  {$IFDEF USE_RAVE_REPORTS}
  , DMRR
  {$ENDIF}
  {$IFDEF USE_FAST_REPORTS}
  , DMFR
  {$ENDIF}
  ;

{$R *.dfm}

procedure TFrmCondominioRecibo.Agregar;
begin
  //Verificando
  Limpiar;
  //Asignando Valores por Defecto
  PReserva := Parametro.ObtenerValor('p_reserva');
  TxtPReserva.Caption := FormatFloat('#,##0.0000%',PReserva);
  //Clase.SetPIva(Parametro.ObtenerValor('p_iva'));
  CmbCodigoConjuntoAdministracion.KeyValue := VarToStr(Parametro.ObtenerValor('codigo_conjunto_administracion'));
  CmbCodigoConjuntoAdministracion.SetFocus;
end;

procedure TFrmCondominioRecibo.Asignar;
begin
  with Clase do
  begin
    CmbCodigoConjuntoAdministracion.KeyValue := Clase.GetCodigoConjuntoAdministracion;
    CmbAnno.Text := IntToStr(Clase.GetPeriodoA);
    CmbMes.ItemIndex := Clase.GetPeriodoM;
    DatFechaIngreso.Date := Clase.GetFechaIngreso;
    DatFechaVencimiento.Date := Clase.GetFechaVencimiento;
    TxtMontoNeto.Value := Clase.GetMontoBase;
    TxtMontoReserva.Value := Clase.GetMontoReserva;
    TxtPReserva.Caption := FormatFloat('#,##0.0000%',Clase.GetPReserva);
    PReserva := Clase.GetPReserva;
    TxtMontoTotal.Value := Clase.GetMontoTotal;
  end;
end;

procedure TFrmCondominioRecibo.BtnBuscarConjuntoAdministracionClick(
  Sender: TObject);
begin
  Buscador(Self,CondominioConjuntoAdministracion,'codigo','Buscar Conjunto de Administraci�n',nil,DataCondominioConjuntoAdministracion);
  CmbCodigoConjuntoAdministracion.KeyValue := VarToStr(DataCondominioConjuntoAdministracion.FieldValues['codigo']);
end;

procedure TFrmCondominioRecibo.BtnBuscarInmuebleClick(Sender: TObject);
begin
  Buscador(Self,CondominioInmueble,'codigo','Buscar Inmueble',nil,DataCondominioInmueble,'"codigo_conjunto_administracion" = ' + QuotedStr(VarToStr(CmbCodigoConjuntoAdministracion.KeyValue)));
  CmbCodigoInmueble.KeyValue := VarToStr(DataCondominioInmueble.FieldValues['codigo']);
end;

procedure TFrmCondominioRecibo.Buscar;
begin
  if (Buscador(Self,BuscadorCondominioRecibo,'codigo','Buscar Recibo',nil,DataCondominioBuscadorRecibo)) then
  begin
    Clase.SetCodigoConjuntoAdministracion(DataCondominioBuscadorRecibo.FieldValues['codigo_conjunto_administracion']);
    Clase.SetPeriodoA(DataCondominioBuscadorRecibo.FieldValues['periodo_a']);
    Clase.SetPeriodoM(DataCondominioBuscadorRecibo.FieldValues['periodo_m']);
    Clase.Buscar;
    Asignar;
    HabilitarDetalleInmueble;
  end;
end;

procedure TFrmCondominioRecibo.CalcularMontos;
var
  MontoNeto, MontoNetoReserva: Currency;
begin
  MontoNeto := 0;
  MontoNetoReserva := 0;
  with Data do
  begin
    DisableControls;
    First;
    while (not Eof) do
    begin
      CondominioGasto.SetCodigoConjuntoAdministracion(VarToStr(CmbCodigoConjuntoAdministracion.KeyValue));
      CondominioGasto.SetCodigo(FieldValues['codigo_gasto']);
      CondominioGasto.Buscar;
      MontoNeto := MontoNeto + FieldValues['monto'];
      if (not CondominioGasto.GetExtraordinario) then
      begin
        MontoNetoReserva := MontoNetoReserva + FieldValues['monto'];
      end;      
      Next;
    end;
    EnableControls;
  end;
  //Asignando Montos
  TxtMontoNeto.Value := MontoNeto;
  TxtMontoReserva.Value := MontoNetoReserva * (PReserva / 100);
  TxtMontoTotal.Value := TxtMontoNeto.Value + TxtMontoReserva.Value;
end;

procedure TFrmCondominioRecibo.Cancelar;
begin
  if (Agregando) then
  begin
    Limpiar;
  end
  else
  begin
    Clase.SetCodigoConjuntoAdministracion(VarToStr(CmbCodigoConjuntoAdministracion.KeyValue));
    Clase.SetPeriodoA(StrToInt(CmbAnno.Text));
    Clase.SetPeriodoM(CmbMes.ItemIndex);
    Clase.Buscar;
    Asignar;
  end;
end;

procedure TFrmCondominioRecibo.HabilitarDetalleInmueble;
var
  DataInmueble: TClientDataSet;
  DataGastoOrdinario: TClientDataSet;
  DataReciboInmuebleGasto: TClientDataSet;
  Monto: Currency;
begin
  if (Length(Trim(CmbCodigoConjuntoAdministracion.Text)) > 0) and (CmbAnno.ItemIndex > 0) and (CmbMes.ItemIndex > 0) then
  begin
    //Validando Que Los Recibo del Periodo Especificados
    Clase.SetCodigoConjuntoAdministracion(VarToStr(CmbCodigoConjuntoAdministracion.KeyValue));
    Clase.SetPeriodoA(StrToInt(CmbAnno.Text));
    Clase.SetPeriodoM(CmbMes.ItemIndex);
    if (not Clase.Buscar) then
    begin
      //Habilitando Detalles del Inmueble
      GrpDetalleInmueble.Enabled := True;
      if (Agregando) then
      begin
        //Generando Detalles de los Gastos Ordinarios de todos los Inmuebles
        Data.EmptyDataSet;
        DataInmueble := CondominioInmueble.ObtenerConjuntoAdministracion(VarToStr(CmbCodigoConjuntoAdministracion.KeyValue));
        DataGastoOrdinario := CondominioGastoOrdinario.ObtenerCodigoConjuntoAdministracion(VarToStr(CmbCodigoConjuntoAdministracion.KeyValue));
        //Recorriendo Inmuebles
        DataInmueble.First;
        while (not DataInmueble.Eof) do
        begin
          //Recorriendo Gastos Ordinarios
          DataGastoOrdinario.First;
          while (not DataGastoOrdinario.Eof) do
          begin
            //Ubicando Gasto
            CondominioGasto.SetCodigoConjuntoAdministracion(VarToStr(CmbCodigoConjuntoAdministracion.KeyValue));
            CondominioGasto.SetCodigo(DataGastoOrdinario.FieldValues['codigo_gasto']);
            CondominioGasto.Buscar;
            //Determinando Monto del Gasto
            if (CondominioGasto.GetMontoFijo) then //Monto Pre-Fijado
            begin
              Monto := CondominioGasto.GetMonto;
            end
            else
            begin
              if (CondominioGasto.GetUsarAlicuotaParticular) then //Alicuota Particular
              begin
                Monto := (DataGastoOrdinario.FieldValues['monto'] * (CondominioGasto.GetPAlicuota / 100));
              end
              else //Alicuota del Inmueble
              begin
                Monto := (DataGastoOrdinario.FieldValues['monto'] * (DataInmueble.FieldValues['p_alicuota'] / 100));
              end;
            end;
            //Cargando Datos
            Data.Append;
            Data.FieldValues['codigo_inmueble'] := DataInmueble.FieldValues['codigo'];
            Data.FieldValues['codigo_gasto'] := DataGastoOrdinario.FieldValues['codigo_gasto'];
            Data.FieldValues['descripcion'] := CondominioGasto.GetDescripcion;
            Data.FieldValues['monto'] := Monto;
            Data.FieldValues['extraordinario'] := CondominioGasto.GetExtraordinario;
            Data.Post;
            DataGastoOrdinario.Next;
          end;
          DataInmueble.Next;
        end;
        CalcularMontos;
      end
      else //Editando, Se Carga desde la Base de Datos
      begin
        Data.EmptyDataSet;
        DataReciboInmuebleGasto := CondominioReciboInmuebleGasto.ObtenerRecibos(VarToStr(CmbCodigoConjuntoAdministracion.KeyValue),StrToInt(CmbAnno.Text),CmbMes.ItemIndex);
        DataReciboInmuebleGasto.First;
        while (not DataReciboInmuebleGasto.Eof) do
        begin
          //Ubicando Gasto
          CondominioGasto.SetCodigoConjuntoAdministracion(VarToStr(DataReciboInmuebleGasto.FieldValues['codigo_conjunto_administracion']));
          CondominioGasto.SetCodigo(VarToStr(DataReciboInmuebleGasto.FieldValues['codigo_gasto']));
          CondominioGasto.Buscar;
          //Cargando Datos
          Data.Append;
          Data.FieldValues['codigo_inmueble'] := DataReciboInmuebleGasto.FieldValues['codigo_inmueble'];
          Data.FieldValues['codigo_gasto'] := DataReciboInmuebleGasto.FieldValues['codigo_gasto'];
          Data.FieldValues['descripcion'] := CondominioGasto.GetDescripcion;
          Data.FieldValues['monto'] := DataReciboInmuebleGasto.FieldValues['monto'];
          Data.FieldValues['extraordinario'] := CondominioGasto.GetExtraordinario;
          Data.Post;
          DataReciboInmuebleGasto.Next;
        end;
      end;
    end
    else
    begin
      //Mostrando Mensaje
      MessageBox(Self.Handle, 'Los Recibos Para Este Conjunto y Periodo Especificado Ya Fueron Generados!!!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      Limpiar;
      CmbCodigoConjuntoAdministracion.SetFocus;      
      GrpDetalleInmueble.Enabled := False;
    end;
  end
  else
  begin
    GrpDetalleInmueble.Enabled := False;
  end;
  DS.DataSet := nil;
end;

procedure TFrmCondominioRecibo.CargarDetalleInmueble;
var
  Total: Currency;
begin
  Data.Filtered := False;
  CalcularMontos;
  Data.Filter := 'codigo_inmueble = ' + QuotedStr(VarToStr(CmbCodigoInmueble.KeyValue));
  Data.Filtered := True;
  DS.DataSet := Data;
  //Totalizando por Inmueble
  Data.First;
  Total := 0;
  while (not Data.Eof) do
  begin
    Total := Total + Data.FieldValues['monto'];
    Data.Next;
  end;
  //Especificando Totales del Inmueble
  TxtMontoNetoInmueble.Value := Total;
  TxtMontoReservaInmueble.Value := TxtMontoNetoInmueble.Value * (PReserva / 100);
  TxtMontoTotalInmueble.Value := TxtMontoNetoInmueble.Value + TxtMontoReservaInmueble.Value;
end;

procedure TFrmCondominioRecibo.CmbAnnoChange(Sender: TObject);
begin
  HabilitarDetalleInmueble;
end;

procedure TFrmCondominioRecibo.CmbCodigoConjuntoAdministracionChange(
  Sender: TObject);
begin
  SB.Panels[0].Text := DataCondominioConjuntoAdministracion.FieldValues['descripcion'];
  HabilitarDetalleInmueble;
  DataCondominioInmueble := CondominioInmueble.ObtenerConjuntoAdministracion(VarToStr(CmbCodigoConjuntoAdministracion.KeyValue));
  DSInmueble.DataSet := DataCondominioInmueble;
end;

procedure TFrmCondominioRecibo.CmbCodigoInmuebleChange(Sender: TObject);
begin
  CargarDetalleInmueble;
end;

procedure TFrmCondominioRecibo.CmbMesChange(Sender: TObject);
begin
  HabilitarDetalleInmueble;
end;

procedure TFrmCondominioRecibo.Editar;
begin
  if (Length(Trim(CmbCodigoConjuntoAdministracion.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
end;

procedure TFrmCondominioRecibo.Eliminar;
begin
  //Se Busca Los Recibos
  Clase.SetCodigoConjuntoAdministracion(VarToStr(CmbCodigoConjuntoAdministracion.KeyValue));
  Clase.SetPeriodoA(StrToInt(CmbAnno.Text));
  Clase.SetPeriodoM(CmbMes.ItemIndex);
  Clase.Buscar;
  //Se Eliminan Los Detalles y Tablas Asociadas a este periodo
  CondominioReciboInmueble.EliminarRecibos(VarToStr(CmbCodigoConjuntoAdministracion.KeyValue),StrToInt(CmbAnno.Text),CmbMes.ItemIndex);
  CondominioReciboInmuebleGasto.EliminarRecibos(VarToStr(CmbCodigoConjuntoAdministracion.KeyValue),StrToInt(CmbAnno.Text),CmbMes.ItemIndex);
  Clase.Eliminar;
  MessageBox(Self.Handle, 'Los Recibos Se Han Eliminado Con Exito!!!',PChar(MsgTituloInformacion), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  Refrescar;
  Limpiar;
end;

procedure TFrmCondominioRecibo.FormCreate(Sender: TObject);
var
  i, a: Integer;
begin
  inherited;
  ProcAgregar := Agregar;
  ProcEditar := Editar;
  ProcEliminar := Eliminar;
  ProcGuardar := Guardar;
  ProcCancelar := Cancelar;
  ProcRefrescar := Refrescar;
  ProcImprimir := Imprimir;
  ProcBuscar := Buscar;
  FrmActividadSophie.Show;
  FrmActividadSophie.Actividad('Cargando N�cleo...');
  Clase := TCondominioRecibo.Create;
  //Inicializando DataSets Secundarios
  BuscadorCondominioRecibo := TCondominioBusquedaRecibo.Create;
  CondominioConjuntoAdministracion := TCondominioConjuntoAdministracion.Create;
  CondominioInmueble := TCondominioInmueble.Create;
  CondominioPropietario := TCondominioPropietario.Create;
  CondominioGastoOrdinario := TCondominioGastoOrdinario.Create;
  CondominioGasto := TCondominioGasto.Create;
  Reporte := TReporte.Create;
  Parametro := TParametro.Create;
  CondominioReciboInmueble := TCondominioReciboInmueble.Create;
  CondominioReciboInmuebleGasto := TCondominioReciboInmuebleGasto.Create;
  CondominioReciboGastoOrdinario := TCondominioReciboGastoOrdinario.Create;
  FrmActividadSophie.Actividad('Cargando Conjuntos de Administracion...');
  DataCondominioConjuntoAdministracion := CondominioConjuntoAdministracion.ObtenerLista;
  FrmActividadSophie.Actividad('Cargando Inmuebles...');
  DataCondominioInmueble := CondominioInmueble.ObtenerLista;
  FrmActividadSophie.Actividad('Cargando Resto del N�cleo...');
  DataCondominioBuscadorRecibo := BuscadorCondominioRecibo.ObtenerLista;
  DataCondominioConjuntoAdministracion.First;
  DataCondominioInmueble.First;
  DataCondominioBuscadorRecibo.First;
  DSConjuntoAdministracion.DataSet := DataCondominioConjuntoAdministracion;
  DSInmueble.DataSet := DataCondominioInmueble;
  ConfigurarRxDBLookupCombo(CmbCodigoConjuntoAdministracion,DSConjuntoAdministracion,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoInmueble,DSInmueble,'codigo','codigo;descripcion');
  TxtCodigoInmueble.DataSource := DSInmueble;
  TxtCodigoInmueble.DataField := 'descripcion';
  DS.DataSet := Data;
  GridDetalle.DataSource := DS;
  FrmActividadSophie.Close;
  //Cargando Combo A�os
  CmbAnno.Items.Add('Seleccione...');
  a := StrToIntDef(FormatDateTime('YYYY',Date),2016); 
  for i := a - 2 to a + 2 do
  begin
    CmbAnno.Items.Add(IntToStr(i));
  end;
end;

procedure TFrmCondominioRecibo.GridDetalleDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  inherited;
  if ((Agregando) or (Editando)) then
  begin
    if (Column.Field.DataSet.FieldValues['extraordinario']) then
    begin
      with Sender as TDBGrid do
      begin
        Canvas.Font.Color := clGray;
        DefaultDrawColumnCell(Rect,DataCol,Column,State);
      end;
    end;
  end;
end;

procedure TFrmCondominioRecibo.Guardar;
begin
  Obtener;
  if (Agregando) then
  begin
    //Agregando...
    //Guardando Encabezado
    Clase.Insertar;
    //Guardando Detalles
    GuardarDetalles;
    MessageBox(Self.Handle, 'Los Recibos Han Sido Generados Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    //Se Imprime la Factura
//    MessageBox(Self.Handle, 'Presione "Aceptar" Para Proceder A Imprimir La Factura!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end
  else
  begin
    //Editando...
    Clase.Modificar;
    //Actualizando Detalles (Eliminando Anteriores y Cargando los Nuevos)
    CondominioReciboInmueble.EliminarRecibos(Clase.GetCodigoConjuntoAdministracion,Clase.GetPeriodoA,Clase.GetPeriodoM);
    CondominioReciboInmuebleGasto.EliminarRecibos(Clase.GetCodigoConjuntoAdministracion,Clase.GetPeriodoA,Clase.GetPeriodoM);
    //Guardando Detalles
    GuardarDetalles;
    MessageBox(Self.Handle, 'Los Recibos Han Sido Editados Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end;
end;

procedure TFrmCondominioRecibo.GuardarDetalles;
var
  AuxMontoNeto, AuxMontoNetoReserva: Currency;
  DataCondominioGastoOrdinario: TClientDataSet;
begin
  //Recorriendo Por Inmuebles
  Data.DisableControls;
  DataCondominioInmueble.First;
  while (not DataCondominioInmueble.Eof) do
  begin
    //Filtrando Datos por Inmueble
    Data.Filtered := False;
    Data.Filter := 'codigo_inmueble = ' + QuotedStr(DataCondominioInmueble.FieldValues['codigo']);
    Data.Filtered := True;
    Data.First;
    AuxMontoNeto := 0;
    AuxMontoNetoReserva := 0;
    while (not Data.Eof) do
    begin
      CondominioGasto.SetCodigoConjuntoAdministracion(VarToStr(CmbCodigoConjuntoAdministracion.KeyValue));
      CondominioGasto.SetCodigo(Data.FieldValues['codigo_gasto']);
      CondominioGasto.Buscar;
      //Agregando Gasto del Inmueble
      CondominioReciboInmuebleGasto.SetCodigoConjuntoAdministracion(VarToStr(CmbCodigoConjuntoAdministracion.KeyValue));
      CondominioReciboInmuebleGasto.SetPeriodoA(StrToInt(CmbAnno.Text));
      CondominioReciboInmuebleGasto.SetPeriodoM(CmbMes.ItemIndex);
      CondominioReciboInmuebleGasto.SetCodigoInmueble(VarToStr(Data.FieldValues['codigo_inmueble']));
      CondominioReciboInmuebleGasto.SetCodigoGasto(VarToStr(Data.FieldValues['codigo_gasto']));
      CondominioReciboInmuebleGasto.SetMonto(Data.FieldValues['monto']);
      CondominioReciboInmuebleGasto.Insertar;
      //Sumando Monto Neto para Reserva
      AuxMontoNeto := AuxMontoNeto + Data.FieldValues['monto'];
      if (not CondominioGasto.GetExtraordinario) then
      begin
        AuxMontoNetoReserva := AuxMontoNetoReserva + Data.FieldValues['monto'];
      end;
      Data.Next;
    end;
    //Agregando Resumen del Inmueble
    CondominioReciboInmueble.SetCodigoConjuntoAdministracion(VarToStr(CmbCodigoConjuntoAdministracion.KeyValue));
    CondominioReciboInmueble.SetPeriodoA(StrToInt(CmbAnno.Text));
    CondominioReciboInmueble.SetPeriodoM(CmbMes.ItemIndex);
    CondominioReciboInmueble.SetCodigoInmueble(VarToStr(DataCondominioInmueble.FieldValues['codigo']));
    CondominioReciboInmueble.SetMontoBase(AuxMontoNeto);
    CondominioReciboInmueble.SetPReserva(PReserva);
    CondominioReciboInmueble.SetMontoReserva(AuxMontoNetoReserva * (PReserva / 100));
    CondominioReciboInmueble.SetMontoTotal(CondominioReciboInmueble.GetMontoBase + CondominioReciboInmueble.GetMontoReserva);
    CondominioReciboInmueble.Insertar;
    DataCondominioInmueble.Next;
  end;
  //Guardando Configuracion de Gastos Ordinarios del Recibo
  DataCondominioGastoOrdinario := CondominioGastoOrdinario.ObtenerCodigoConjuntoAdministracion(Clase.GetCodigoConjuntoAdministracion);
  DataCondominioGastoOrdinario.First;
  while (not DataCondominioGastoOrdinario.Eof) do
  begin
    with CondominioReciboGastoOrdinario do
    begin
      SetCodigoConjuntoAdministracion(Clase.GetCodigoConjuntoAdministracion);
      SetPeriodoA(Clase.GetPeriodoA);
      SetPeriodoM(Clase.GetPeriodoM);
      SetCodigoGasto(DataCondominioGastoOrdinario.FieldValues['codigo_gasto']);
      SetMonto(DataCondominioGastoOrdinario.FieldValues['monto']);
      Insertar;
    end;
    DataCondominioGastoOrdinario.Next;
  end;
  Data.EnableControls;
end;

procedure TFrmCondominioRecibo.Imprimir;
begin
  if (Length(Trim(CmbCodigoConjuntoAdministracion.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end
  else
  begin
    with Reporte do
    begin
      AgregarParametro('CodigoConjuntoAdministracion',Clase.GetCodigoConjuntoAdministracion);
      AgregarParametro('PeriodoA',IntToStr(Clase.GetPeriodoA));
      AgregarParametro('PeriodoM',IntToStr(Clase.GetPeriodoM));
      EmiteReporte('CondominioRecibo',trDocumento);
    end;
  end;
end;

procedure TFrmCondominioRecibo.Limpiar;
begin
  CmbCodigoConjuntoAdministracion.KeyValue := '';
  CmbAnno.ItemIndex := 0;
  CmbMes.ItemIndex := 0;
  DatFechaIngreso.Date := Date;
  DatFechaVencimiento.Date := Date;
  TxtMontoNeto.Value := 0;
  TxtMontoReserva.Value := 0;
  TxtMontoTotal.Value := 0;
  TxtPReserva.Caption := FormatFloat('###0.0000%',0);
  //Status Bar
  SB.Panels[0].Text := '';
  Data.EmptyDataSet;
end;

procedure TFrmCondominioRecibo.Mover;
begin

end;

procedure TFrmCondominioRecibo.Obtener;
begin
  with Clase do
  begin
    SetCodigoConjuntoAdministracion(VarToStr(CmbCodigoConjuntoAdministracion.KeyValue));
    SetPeriodoA(StrToInt(CmbAnno.Text));
    SetPeriodoM(CmbMes.ItemIndex);
    SetFechaIngreso(DatFechaIngreso.Date);
    SetFechaVencimiento(DatFechaVencimiento.Date);
    SetMontoBase(TxtMontoNeto.Value);
    SetPReserva(PReserva);
    SetMontoReserva(TxtMontoReserva.Value);
    SetMontoTotal(TxtMontoTotal.Value);
  end;
end;

procedure TFrmCondominioRecibo.Refrescar;
begin
  //Se Actualiza El DataBuscador
  //DataBuscadorFacturacionCliente := BuscadorFacturacionCliente.ObtenerLista;
  //Se Actualizan Los demas Componentes
  DataCondominioConjuntoAdministracion := CondominioConjuntoAdministracion.ObtenerLista;
  DataCondominioInmueble := CondominioInmueble.ObtenerLista;
  DataCondominioConjuntoAdministracion.First;
  DataCondominioInmueble.First;
//  DataBuscadorFacturacionCliente.First;
  DSConjuntoAdministracion.DataSet := DataCondominioConjuntoAdministracion;
  DSInmueble.DataSet := DataCondominioInmueble;
end;

procedure TFrmCondominioRecibo.TxtPReservaDblClick(Sender: TObject);
var
  Aux: Currency;
begin
  Aux := 0;
  Solicitar(Self,'(%) de Reserva',PReserva,Aux);
  if (Aux >= 0) then
  begin
    PReserva := Aux;
    CalcularMontos;
  end;
end;

procedure TFrmCondominioRecibo.TBtnEliminarClick(Sender: TObject);
begin
  if (Length(Trim(CmbCodigoConjuntoAdministracion.Text)) > 0) then
  begin
    inherited;
  end
  else
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
end;

function TFrmCondominioRecibo.Validar: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  (*
  //Validando Cliente
  if (Length(Trim(CmbCodigoCliente.Text)) = 0) then
  begin
    ListaErrores.Add('El Cliente No Puede Estar En Blanco!!!');
  end;
  //Validando Vendedor
  if (Length(Trim(CmbCodigoVendedor.Text)) = 0) then
  begin
    ListaErrores.Add('El Vendedor No Puede Estar En Blanco!!!');
  end;
  //Validando Monto
  if (TxtTotal.Value <= 0) then
  begin
    ListaErrores.Add('El Total Debe Ser Positivo!!!');
  end;
  //Validando Fechas
  if ((DatFechaIngreso.Date > DatFechaRecepcion.Date) or (DatFechaIngreso.Date > DatFechaContable.Date) or (DatFechaIngreso.Date > DatFechaLibros.Date)) then
  begin
    ListaErrores.Add('La Fecha de Ingreso Debe Ser Menor a las Fechas de Recepcion, Contable y Libros Respectivamente!!!');
  end;
  //Validando Detalle o Forma Libre
  if (Clase.GetFormaLibre) then
  begin
    if (Length(Trim(TxtConcepto.Text)) = 0) then
    begin
      ListaErrores.Add('El Concepto No Puede Estar En Blanco!!!');
    end;
    if (TxtMontoNeto.Value <= 0) then
    begin
      ListaErrores.Add('El Monto Neto Debe Ser Positivo!!!');
    end;
    if (TxtMontoDescuento.Value < 0) then
    begin
      ListaErrores.Add('El Monto de Descuento No Debe Ser Negativo!!!');
    end;
    if (TxtImpuesto.Value < 0) then
    begin
      ListaErrores.Add('El Impuesto No Debe Ser Negativo!!!');
    end;
  end
  else
  begin
    if (Data.RecordCount = 0) then
    begin
      ListaErrores.Add('El Detalle De La Factura No Puede Estar Vacio!!!');
    end;
  end;
  *)
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

procedure TFrmCondominioRecibo.Detalle;
begin
  inherited;
  FrmCondominioAgregarDetalleRecibo := TFrmCondominioAgregarDetalleRecibo.CrearDetalle(Self,Data);
  FrmCondominioAgregarDetalleRecibo.CodigoConjuntoAdministracion := VarToStr(CmbCodigoConjuntoAdministracion.KeyValue);
  FrmCondominioAgregarDetalleRecibo.CodigoInmueble := VarToStr(CmbCodigoInmueble.KeyValue);
  FrmCondominioAgregarDetalleRecibo.ShowModal;
  FreeAndNil(FrmCondominioAgregarDetalleRecibo);
end;

end.
