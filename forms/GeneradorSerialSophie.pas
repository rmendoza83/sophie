unit GeneradorSerialSophie;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, mxProtector, XPMan, StdCtrls, Buttons, Utils;

type
  TFrmGeneradorSerialSophie = class(TForm)
    LblTitulo: TLabel;
    LblUsuario: TLabel;
    LblSerial: TLabel;
    TxtUsuario: TEdit;
    TxtSerial: TEdit;
    BtnSalir: TBitBtn;
    BtnGenerar: TBitBtn;
    TemaXP: TXPManifest;
    MXP: TmxProtector;
    procedure BtnGenerarClick(Sender: TObject);
    procedure BtnSalirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmGeneradorSerialSophie: TFrmGeneradorSerialSophie;

implementation

{$R *.dfm}

uses
  Clipbrd;

procedure TFrmGeneradorSerialSophie.BtnGenerarClick(Sender: TObject);
begin
  if (Length(Trim(TxtUsuario.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Debe Indicar el Usuario!!!', PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_APPLMODAL);
    TxtUsuario.SetFocus;
  end
  else
  begin
    if (MessageBox(Self.Handle, PChar('�Los Datos Estan Correctos?'), PChar(MsgTituloInformacion), MB_ICONQUESTION or MB_YESNO or MB_TOPMOST or MB_APPLMODAL) = mrYes) then
    begin
      with MXP do
      begin
        TxtSerial.Text := GenerateSerialNumber(TxtUsuario.Text);
        Clipboard.AsText := TxtSerial.Text;
        MessageBox(Self.Handle, 'Serial Generado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_APPLMODAL);
        BtnSalir.SetFocus;
      end;
    end;
  end;
end;

procedure TFrmGeneradorSerialSophie.BtnSalirClick(Sender: TObject);
begin
  Close;
end;

end.
