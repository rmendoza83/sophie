unit AgregarDetalleTransferenciaCliente;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GenericoAgregarDetalle, StdCtrls, Buttons, ExtCtrls, Mask,
  rxToolEdit, rxCurrEdit, RxLookup, DB, ClsClienteCuentaBancaria,
  ClsCuentaBancaria, Utils, DBClient, GenericoBuscador;

type
  TFrmAgregarDetalleTransferenciaCliente = class(TFrmGenericoAgregarDetalle)
    LblNumeroCuentaCliente: TLabel;
    LblNumero: TLabel;
    LblMonto: TLabel;
    TxtNumero: TEdit;
    CmbNumeroCuentaCliente: TRxDBLookupCombo;
    BtnBuscarNumeroCuentaCliente: TBitBtn;
    TxtMonto: TCurrencyEdit;
    LblCodigoCuenta: TLabel;
    CmbCodigoCuenta: TRxDBLookupCombo;
    BtnBuscarCodigoCuenta: TBitBtn;
    DSCuentaBancaria: TDataSource;
    DSNumeroCuentaCliente: TDataSource;
    procedure FormCreate(Sender: TObject);
    procedure BtnBuscarNumeroCuentaClienteClick(Sender: TObject);
    procedure BtnBuscarCodigoCuentaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    CodigoCliente: string;
    ClienteCuentaBancaria: TClienteCuentaBancaria;
    CuentaBancaria: TCuentaBancaria;
    DataClienteCuentaBancaria: TClientDataSet;
    DataCuentaBancaria: TClientDataSet;
  protected
    function Buscar: Boolean; override;
    procedure Agregar; override;
    procedure Limpiar; override;
  public
    { Public declarations }
    constructor CrearDetalle(AOwner: TComponent; DataSet: TClientDataSet; ACodigoCliente: string); overload;
  end;

var
  FrmAgregarDetalleTransferenciaCliente: TFrmAgregarDetalleTransferenciaCliente;

implementation

{$R *.dfm}

{ TFrmAgregarDetalleTransferenciaCliente }

procedure TFrmAgregarDetalleTransferenciaCliente.Agregar;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  (* Validando Informacion de la Transferencia *)
  //Validando Numero de Cuenta
  if (Length(Trim(CmbNumeroCuentaCliente.Text)) = 0) then
  begin
    ListaErrores.Add('El Numero de Cuenta del Cliente No Puede Estar En Blanco!!!');
  end;
  //Validando Cuenta Saliente
  if (Length(Trim(CmbCodigoCuenta.Text)) = 0) then
  begin
    ListaErrores.Add('La Cuenta Bancaria Saliente No Puede Estar En Blanco!!!');
  end;
  //Validando Numero
  if (Length(Trim(TxtNumero.Text)) = 0) then
  begin
    ListaErrores.Add('El Numero de la Transacción No Puede Estar En Blanco!!!');
  end;
  //Validando Monto
  if (Length(Trim(TxtMonto.Text)) = 0) then
  begin
    ListaErrores.Add('El Monto No Puede Estar En Blanco!!!');
  end;
  if (TxtMonto.Value <= 0) then
  begin
    ListaErrores.Add('El Monto Debe Ser Mayor a Cero!!!');  
  end;  
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    CmbNumeroCuentaCliente.SetFocus;
  end
  else
  begin
    with Data do
    begin
      Append;
      FieldValues['codigo_cliente'] := CodigoCliente;
      FieldValues['numero_cuenta_cliente'] := CmbNumeroCuentaCliente.KeyValue;
      FieldValues['codigo_cuenta'] := CmbCodigoCuenta.KeyValue;
      FieldValues['codigo_tipo_operacion_bancaria'] := '';
      FieldValues['numero'] := TxtNumero.Text;
      FieldValues['fecha'] := Date;
      FieldValues['monto'] := TxtMonto.Value;
      FieldValues['id_operacion_bancaria'] := -1;
      FieldValues['id_transferencia_cliente'] := -1;
      Post;
    end;
    inherited;
  end;
end;

procedure TFrmAgregarDetalleTransferenciaCliente.BtnBuscarCodigoCuentaClick(
  Sender: TObject);
begin
  Buscador(Self,CuentaBancaria,'codigo','Buscar Cuenta Bancaria Saliente',nil,DataCuentaBancaria);
  CmbCodigoCuenta.KeyValue := DataCuentaBancaria.FieldValues['codigo'];
end;

procedure TFrmAgregarDetalleTransferenciaCliente.BtnBuscarNumeroCuentaClienteClick(
  Sender: TObject);
begin
  Buscador(Self,ClienteCuentaBancaria,'numero_cuenta','Buscar Cuenta Bancaria Cliente',nil,DataClienteCuentaBancaria);
  CmbNumeroCuentaCliente.KeyValue := DataClienteCuentaBancaria.FieldValues['numero_cuenta'];
end;

function TFrmAgregarDetalleTransferenciaCliente.Buscar: Boolean;
begin
  Result := False;
end;

constructor TFrmAgregarDetalleTransferenciaCliente.CrearDetalle(
  AOwner: TComponent; DataSet: TClientDataSet; ACodigoCliente: string);
begin
  inherited CrearDetalle(AOwner,DataSet);
  CodigoCliente := ACodigoCliente;
end;

procedure TFrmAgregarDetalleTransferenciaCliente.FormCreate(Sender: TObject);
begin
  inherited;
  ClienteCuentaBancaria := TClienteCuentaBancaria.Create;
  CuentaBancaria := TCuentaBancaria.Create;
  DataClienteCuentaBancaria := ClienteCuentaBancaria.ObtenerComboCodigoCliente(CodigoCliente);
  DataCuentaBancaria := CuentaBancaria.ObtenerCombo;
  DataClienteCuentaBancaria.First;
  DataCuentaBancaria.First;
  DSNumeroCuentaCliente.DataSet := DataClienteCuentaBancaria;
  DSCuentaBancaria.DataSet := DataCuentaBancaria;
  //Configurando ComboBoxes;
  ConfigurarRxDBLookupCombo(CmbNumeroCuentaCliente,DSNumeroCuentaCliente,'numero_cuenta','numero_cuenta;entidad;tipo');
  ConfigurarRxDBLookupCombo(CmbCodigoCuenta,DSCuentaBancaria,'codigo','codigo;entidad;numero');
end;

procedure TFrmAgregarDetalleTransferenciaCliente.FormShow(Sender: TObject);
begin
  inherited;
  CmbNumeroCuentaCliente.SetFocus;
end;

procedure TFrmAgregarDetalleTransferenciaCliente.Limpiar;
begin
  inherited;
  CmbNumeroCuentaCliente.KeyValue := '';
  CmbCodigoCuenta.KeyValue := '';
  TxtNumero.Text := '';
  TxtMonto.Text := '';
end;

end.
