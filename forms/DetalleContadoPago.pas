unit DetalleContadoPago;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, DBClient, ExtCtrls, Grids, DBGrids, StdCtrls, Buttons,
  rxToolEdit, Mask, rxCurrEdit, RxLookup, Utils, ClsDetalleContadoPago,
  ClsBancoCobranza, ClsFormaPago, GenericoBuscador, ClsConsecutivo;

type
  TFrmDetalleContadoPago = class(TForm)
    GrpDatos: TGroupBox;
    LblCodigoFormaPago: TLabel;
    LblMonto: TLabel;
    LblFecha: TLabel;
    LblBancoCobranza: TLabel;
    LblNumero: TLabel;
    LblNumeroAuxiliar: TLabel;
    LblReferencia: TLabel;
    LblTotal: TLabel;
    TxtTotal: TLabel;
    LblRestan: TLabel;
    TxtRestan: TLabel;
    CmbCodigoFormaPago: TRxDBLookupCombo;
    TxtMonto: TCurrencyEdit;
    DatFecha: TDateEdit;
    CmbCodigoBancoCobranza: TRxDBLookupCombo;
    TxtNumeroAuxiliar: TEdit;
    TxtNumero: TEdit;
    TxtReferencia: TEdit;
    BtnAgregar: TBitBtn;
    BtnBuscarBancoCobranza: TBitBtn;
    BtnBuscarCodigoFormaPago: TBitBtn;
    GridDetalleContado: TDBGrid;
    PanelBotones: TPanel;
    GrpTotalRecibido: TGroupBox;
    LblRecibido: TLabel;
    LblVuelto: TLabel;
    TxtRecibido: TCurrencyEdit;
    TxtVuelto: TCurrencyEdit;
    BtnCancelar: TBitBtn;
    BtnAceptar: TBitBtn;
    DSFormaPago: TDataSource;
    DSBancoCobranza: TDataSource;
    DS: TDataSource;
    Data: TClientDataSet;
    Datacodigo_forma_pago: TStringField;
    Datamonto: TFloatField;
    Datacodigo_banco: TStringField;
    Datanumero_documento: TStringField;
    Datanumero_auxiliar: TStringField;
    Datareferencia: TStringField;
    Datafecha: TDateField;
    Datalogin_usuario: TStringField;
    procedure GridDetalleContadoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtnBuscarCodigoFormaPagoClick(Sender: TObject);
    procedure BtnBuscarBancoCobranzaClick(Sender: TObject);
    procedure BtnAgregarClick(Sender: TObject);
    procedure BtnCancelarClick(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure DataAfterPost(DataSet: TDataSet);
    procedure TxtRecibidoExit(Sender: TObject);
  private
    { Private declarations }
    Desde: string;
    Total: Currency;
    Restan: Currency;
    Agregando: Boolean;
    Editando: Boolean;
    Clase: TDetalleContadoPago;
    IdContadoPago: Integer;
    BancoCobranza: TBancoCobranza;
    FormaPago: TFormaPago;
    DataBancoCobranza: TClientDataSet;
    DataFormaPago: TClientDataSet;
    function Validar: Boolean;
  public
    { Public declarations }
    Aceptado: Boolean;
    constructor Crear(AOwner: TComponent; AAgregando, AEditando: Boolean; AIdContadoPago: Integer; AMonto: Double; ADesde: string);
    procedure Actualizar;
    function GetIdContadoPago: Integer;
  end;

var
  FrmDetalleContadoPago: TFrmDetalleContadoPago;

implementation

uses
  PModule, ClsGenericoBD;

{$R *.dfm}

{ TFrmDetalleContadoPago }

procedure TFrmDetalleContadoPago.Actualizar;
begin
  //Actualizando la Suma de las Formas de Pago
  Restan := 0;
  with Data do
  begin
    First;
    while (not Eof) do
    begin
      Restan := Restan + (Trunc(FieldByName('monto').AsCurrency * 10000) / 10000);
      Next;
    end;
  end;
  Restan := Total - Restan;
  //Limpiando Pantalla
  CmbCodigoFormaPago.Value := '';
  TxtMonto.Value := Restan;
  DatFecha.Date := Date;
  CmbCodigoBancoCobranza.Value := '';
  TxtNumero.Text := '';
  TxtNumeroAuxiliar.Text := '';
  TxtReferencia.Text := '';
  TxtTotal.Caption := FormatFloat('#,##0.0000',Total);
  TxtRestan.Caption := FormatFloat('#,##0.0000',Restan);
end;

procedure TFrmDetalleContadoPago.BtnAceptarClick(Sender: TObject);
var
  Consecutivo: TConsecutivo;
begin
  if (MessageBox(Self.Handle, '�Esta Seguro Que Los Datos Estan Correctos?', PChar(MsgTituloInformacion), MB_ICONQUESTION or MB_YESNO or MB_TOPMOST or MB_TASKMODAL) = mrYes) then
  begin
    if (Restan = 0) then
    begin
      Aceptado := True;
      //Se Guarda El Detalle De Contado
      Consecutivo := TConsecutivo.Create;
      if (Agregando) then
      begin
        IdContadoPago := Consecutivo.ObtenerConsecutivo('id_contado_pago');
      end
      else
      begin
        if (IdContadoPago = -1) then //Caso de Documento que cambia de Credito a Contado
        begin
          IdContadoPago := Consecutivo.ObtenerConsecutivo('id_contado_pago');
        end;
      end;
      //Eliminando Anteriores en Caso de Editar
      if (Editando) then
      begin
        Clase.EliminarIdContadoPago(IdContadoPago);
      end;
      with Data do
      begin
        DisableControls;
        First;
        while (not Eof) do
        begin
          with Clase do
          begin
            SetIdContadoPago(IdContadoPago);
            SetCodigoFormaPago(FieldValues['codigo_forma_pago']);
            SetMonto(FieldValues['monto']);
            SetDesde(Desde);	
            SetCodigoBanco(FieldValues['codigo_banco']);
            SetNumeroDocumento(FieldValues['numero_documento']);
            SetNumeroAuxiliar(FieldValues['numero_auxiliar']);
            SetReferencia(FieldValues['referencia']);
            SetFecha(FieldValues['fecha']);
            SetLoginUsuario(FieldValues['login_usuario']);
            Insertar;
          end;
          Next;
        end;
        EnableControls;
      end;
      Close;
    end
    else
    begin
      MessageBox(Self.Handle, 'El Detalle de Contado No Cuadra!!!', PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    end;
  end;
end;

procedure TFrmDetalleContadoPago.BtnAgregarClick(Sender: TObject);
begin
  if ((Length(Trim(CmbCodigoFormaPago.Value)) > 0) and (Restan > 0) and (TxtMonto.Value > 0) and ((Trunc(TxtMonto.Value * 10000) / 10000) <= (Trunc(Restan * 10000) / 10000))) then
  begin
    if (Validar) then
    begin
      with Data do
      begin
        Insert;
        FieldValues['codigo_forma_pago'] := CmbCodigoFormaPago.Value;
        FieldValues['monto'] := TxtMonto.Value;
        FieldValues['codigo_banco']:= CmbCodigoBancoCobranza.Value;
        FieldValues['numero_documento'] := TxtNumero.Text;
        FieldValues['numero_auxiliar'] := TxtNumeroAuxiliar.Text;
        FieldValues['referencia'] := TxtReferencia.Text;
        FieldValues['fecha'] := DatFecha.Date;
        FieldValues['login_usuario'] := P.User.GetLoginUsuario;
        Post;
      end;
    end;
  end;
end;

procedure TFrmDetalleContadoPago.BtnBuscarBancoCobranzaClick(Sender: TObject);
begin
  Buscador(Self,BancoCobranza,'codigo','Buscar Banco',nil,DataBancoCobranza);
  CmbCodigoBancoCobranza.KeyValue := DataBancoCobranza.FieldValues['codigo'];
end;

procedure TFrmDetalleContadoPago.BtnBuscarCodigoFormaPagoClick(Sender: TObject);
begin
  Buscador(Self,FormaPago,'codigo','Buscar Forma de Pago',nil,DataFormaPago);
  CmbCodigoFormaPago.KeyValue := DataFormaPago.FieldValues['codigo'];
end;

procedure TFrmDetalleContadoPago.BtnCancelarClick(Sender: TObject);
begin
  Close;
end;

constructor TFrmDetalleContadoPago.Crear(AOwner: TComponent; AAgregando,
  AEditando: Boolean; AIdContadoPago: Integer; AMonto: Double; ADesde: string);
begin
  inherited Create(AOwner);
  Agregando := AAgregando;
  Editando := AEditando;
  IdContadoPago := AIdContadoPago;
  Desde := ADesde;
  Total := AMonto;
  Restan := AMonto;
  Aceptado := False;
  //Inicializando DataSets Secundarios
  Clase := TDetalleContadoPago.Create;
  BancoCobranza := TBancoCobranza.Create;
  FormaPago := TFormaPago.Create;
  DataBancoCobranza := BancoCobranza.ObtenerCombo;
  DataFormaPago := FormaPago.ObtenerCombo;
  DataBancoCobranza.First;
  DataFormaPago.First;
  DSBancoCobranza.DataSet := DataBancoCobranza;
  DSFormaPago.DataSet := DataFormaPago;
  ConfigurarRxDBLookupCombo(CmbCodigoBancoCobranza,DSBancoCobranza,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoFormaPago,DSFormaPago,'codigo','codigo;descripcion');
  DS.DataSet := Data;
  GridDetalleContado.DataSource := DS;
  Actualizar;
end;

procedure TFrmDetalleContadoPago.DataAfterPost(DataSet: TDataSet);
begin
  Actualizar;
end;

function TFrmDetalleContadoPago.GetIdContadoPago: Integer;
begin
  Result := IdContadoPago;
end;

procedure TFrmDetalleContadoPago.GridDetalleContadoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  case Key of
    VK_DELETE:
    begin
      if (not Data.IsEmpty) then
      begin
        if (MessageBox(Self.Handle, '�Esta Seguro?', MsgTituloAdvertencia, MB_ICONWARNING or MB_YESNO or MB_TOPMOST or MB_TASKMODAL) = mrYes) then
        begin    
          Data.Delete;
        end;
      end;
    end;
  end;
end;

procedure TFrmDetalleContadoPago.TxtRecibidoExit(Sender: TObject);
var
  Efectivo: Double;
  ClaseFormaPago: TFormaPago;
begin
  ClaseFormaPago := TFormaPago.Create;
  Efectivo := 0;
  with Data do
  begin
    First;
    while (not Eof) do
    begin
      ClaseFormaPago.SetCodigo(FieldValues['codigo_forma_pago']);
      ClaseFormaPago.Buscar;
      if ((not ClaseFormaPago.GetCheque) and (not ClaseFormaPago.GetNc) and
          (not ClaseFormaPago.GetNd) and (not ClaseFormaPago.GetAnticipo)) then
      begin
        Efectivo  := Efectivo + FieldValues['monto'];
      end;
      Next;
    end;
  end;
  if ((TxtRecibido.Value - Efectivo) > 0) then
  begin
    TxtVuelto.Value := TxtRecibido.Value - Efectivo;
  end
  else
  begin
    TxtVuelto.Value := 0;
  end;
end;

function TFrmDetalleContadoPago.Validar: Boolean;
var
  ClaseFormaPago: TFormaPago;
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  ClaseFormaPago := TFormaPago.Create;
  ClaseFormaPago.SetCodigo(CmbCodigoFormaPago.Value);
  ClaseFormaPago.Buscar;
  with ClaseFormaPago do
  begin
    //Validando Campo Cheque
    if (GetCheque) then
    begin
      //Validando Codigo Banco
      if (Length(Trim(CmbCodigoBancoCobranza.Value)) = 0) then
      begin
        ListaErrores.Add('El Banco No Puede Estar En Blanco!!!');
      end;
      //Validando Numero del Cheque en Numero Doc
      if (Length(Trim(TxtNumero.Text)) = 0) then
      begin
        ListaErrores.Add('El N�mero del Cheque (Numero Doc) No Puede Estar En Blanco!!!');
      end;
    end;
    //Validando Nota de Credito, Nota de Debito o Anticipo
    if (GetNc or GetNd or GetAnticipo) then
    begin
      //Validando Numero de la Nota o Anticipo en Numero Doc
      if (Length(Trim(TxtNumero.Text)) = 0) then
      begin
        ListaErrores.Add('El N�mero de la Nota de Debito, Credito o Anticipo (Numero Doc) No Puede Estar En Blanco!!!');
      end;
    end;
  end;
  //Validando Repeticion del Efectivo
  with Data do
  begin
    DisableControls;
    First;
    while (not Eof) do
    begin
      ClaseFormaPago.SetCodigo(FieldValues['codigo_forma_pago']);
      ClaseFormaPago.Buscar;
      if ((not ClaseFormaPago.GetCheque) and (not ClaseFormaPago.GetNc) and
          (not ClaseFormaPago.GetNd) and (not ClaseFormaPago.GetAnticipo)) then
      begin
        if (ClaseFormaPago.GetCodigo = CmbCodigoFormaPago.Value) then
        begin
          ListaErrores.Add('La Forma de Pago esta Repetida!!!');
          Break;
        end;
      end;
      Next;
    end;
    EnableControls;
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
