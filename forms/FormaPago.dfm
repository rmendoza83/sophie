inherited FrmFormaPago: TFrmFormaPago
  Caption = 'Sophie - Formas de Pago'
  ExplicitWidth = 598
  ExplicitHeight = 396
  PixelsPerInch = 96
  TextHeight = 13
  inherited Pag: TPageControl
    inherited TabBusqueda: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 24
      ExplicitWidth = 584
      ExplicitHeight = 304
    end
    inherited TabDatos: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 24
      ExplicitWidth = 584
      ExplicitHeight = 304
      object LblCodigo: TLabel
        Left = 72
        Top = 11
        Width = 41
        Height = 13
        Alignment = taRightJustify
        Caption = 'C'#243'digo:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblDescripcion: TLabel
        Left = 45
        Top = 35
        Width = 68
        Height = 13
        Alignment = taRightJustify
        Caption = 'Descripci'#243'n:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label1: TLabel
        Left = 35
        Top = 60
        Width = 78
        Height = 13
        Alignment = taRightJustify
        Caption = 'C'#243'digo Banco:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 68
        Top = 84
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = 'Cheque:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 363
        Top = 61
        Width = 49
        Height = 13
        Alignment = taRightJustify
        Caption = 'Anticipo:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label4: TLabel
        Left = 158
        Top = 60
        Width = 57
        Height = 13
        Alignment = taRightJustify
        Caption = 'N. Cr'#233'dito:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label5: TLabel
        Left = 162
        Top = 84
        Width = 53
        Height = 13
        Alignment = taRightJustify
        Caption = 'N. D'#233'bito:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 258
        Top = 60
        Width = 56
        Height = 13
        Alignment = taRightJustify
        Caption = 'Cobranza:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label7: TLabel
        Left = 283
        Top = 84
        Width = 31
        Height = 13
        Alignment = taRightJustify
        Caption = 'Pago:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblOperacionBancaria: TLabel
        Left = 1
        Top = 108
        Width = 112
        Height = 13
        Alignment = taRightJustify
        Caption = 'Operaci'#243'n Bancaria:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblCodigoTipoOperacionBancaria: TLabel
        Left = 155
        Top = 108
        Width = 139
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo Operaci'#243'n Bancaria:'
        Enabled = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object TxtCodigoTipoOperacionBancaria: TDBText
        Left = 456
        Top = 108
        Width = 159
        Height = 13
        AutoSize = True
        Enabled = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object TxtCodigo: TEdit
        Left = 119
        Top = 8
        Width = 150
        Height = 21
        TabOrder = 0
      end
      object TxtDescripcion: TEdit
        Left = 119
        Top = 32
        Width = 338
        Height = 21
        TabOrder = 1
      end
      object TxtCodigoBanco: TCheckBox
        Left = 119
        Top = 59
        Width = 15
        Height = 17
        TabOrder = 2
      end
      object TxtCheque: TCheckBox
        Left = 119
        Top = 83
        Width = 15
        Height = 17
        TabOrder = 3
      end
      object TxtAnticipo: TCheckBox
        Left = 416
        Top = 59
        Width = 15
        Height = 17
        TabOrder = 8
      end
      object TxtNc: TCheckBox
        Left = 221
        Top = 59
        Width = 15
        Height = 17
        TabOrder = 4
      end
      object TxtNd: TCheckBox
        Left = 221
        Top = 83
        Width = 15
        Height = 17
        TabOrder = 5
      end
      object TxtCobranza: TCheckBox
        Left = 320
        Top = 59
        Width = 15
        Height = 17
        TabOrder = 6
      end
      object TxtPago: TCheckBox
        Left = 320
        Top = 83
        Width = 15
        Height = 17
        TabOrder = 7
      end
      object ChkCodigoTipoOperacionBancaria: TCheckBox
        Left = 119
        Top = 107
        Width = 15
        Height = 17
        TabOrder = 9
        OnClick = ChkCodigoTipoOperacionBancariaClick
      end
      object CmbCodigoTipoOperacionBancaria: TRxDBLookupCombo
        Left = 300
        Top = 106
        Width = 150
        Height = 21
        DropDownCount = 20
        DropDownWidth = 400
        Enabled = False
        TabOrder = 10
      end
    end
  end
  object DSTipoOperacionBancaria: TDataSource
    Left = 16
    Top = 192
  end
end
