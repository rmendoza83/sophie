object FrmReporteGeneral: TFrmReporteGeneral
  Left = 227
  Top = 174
  Caption = 'Sophie - Reportes Generales'
  ClientHeight = 446
  ClientWidth = 801
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GrpListadoReportes: TRadioGroup
    Left = 0
    Top = 0
    Width = 232
    Height = 446
    Align = alLeft
    Caption = 'Listado de Reportes:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clGreen
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    Items.Strings = (
      'Balance de Caja (Ingresos)'
      'Balance de Caja (Egresos)'
      'Estados de Cuenta Bancarios'
      'Estado de Cuenta de Clientes'
      'Estado de Cuenta de Proveedores'
      'Comisiones Vendedores'
      'Comisiones Vendedores por Pagar'
      'Productos Vendidos'
      'Productos Comprados'
      'Libro de Compras'
      'Libro de Ventas'
      'Retenci'#243'n I.V.A (Compras - Ventas)'
      'Impuesto Retenido a Proveedores'
      'Movimientos de Inventarios'
      'Exist. Inventario / Libro Inventario')
    ParentFont = False
    TabOrder = 0
    OnClick = GrpListadoReportesClick
  end
  object GrpParametrosReporte: TGroupBox
    Left = 232
    Top = 0
    Width = 569
    Height = 446
    Align = alClient
    Caption = 'Par'#225'metros del Reporte:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    object SLinea: TShape
      Left = 16
      Top = 333
      Width = 543
      Height = 2
      Pen.Color = clNavy
    end
    object LblRangoCliente: TRxLabel
      Left = 199
      Top = 25
      Width = 111
      Height = 13
      Cursor = crHandPoint
      Hint = 'Hacer doble clic para limpiar campos...'
      Caption = 'Rango de Clientes:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      OnDblClick = LblRangoClienteDblClick
    end
    object LblPeriodo: TRxLabel
      Left = 16
      Top = 25
      Width = 50
      Height = 13
      Cursor = crHandPoint
      Hint = 'Hacer doble clic para inicializar fechas...'
      Caption = 'Periodo:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      OnDblClick = LblPeriodoDblClick
    end
    object LblRangoProveedor: TRxLabel
      Left = 382
      Top = 25
      Width = 137
      Height = 13
      Cursor = crHandPoint
      Hint = 'Hacer doble clic para limpiar campos...'
      Caption = 'Rango de Proveedores:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      OnDblClick = LblRangoProveedorDblClick
    end
    object LblRangoProducto: TRxLabel
      Left = 16
      Top = 137
      Width = 123
      Height = 13
      Cursor = crHandPoint
      Hint = 'Hacer doble clic para limpiar campos...'
      Caption = 'Rango de Productos:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      OnDblClick = LblRangoProductoDblClick
    end
    object LblClasificacionProducto: TRxLabel
      Left = 199
      Top = 137
      Width = 153
      Height = 13
      Cursor = crHandPoint
      Hint = 'Hacer doble clic para limpiar campos...'
      Caption = 'Clasificaciones Productos:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      OnDblClick = LblClasificacionProductoDblClick
    end
    object LblOtros: TRxLabel
      Left = 382
      Top = 137
      Width = 37
      Height = 13
      Cursor = crHandPoint
      Hint = 'Hacer doble clic para limpiar campos...'
      Caption = 'Otros:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      OnDblClick = LblOtrosDblClick
    end
    object LblTipoReporte: TRxLabel
      Left = 16
      Top = 341
      Width = 81
      Height = 13
      Caption = 'Tipo Reporte:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      ParentShowHint = False
      ShowHint = False
    end
    object LblClaseReporte: TRxLabel
      Left = 199
      Top = 341
      Width = 87
      Height = 13
      Caption = 'Clase Reporte:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      ParentShowHint = False
      ShowHint = False
    end
    object LblTipoOrdenamiento: TRxLabel
      Left = 382
      Top = 341
      Width = 114
      Height = 13
      Caption = 'Tipo Ordenamiento:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      ParentShowHint = False
      ShowHint = False
    end
    object PanelBotones: TPanel
      Left = 2
      Top = 403
      Width = 565
      Height = 41
      Align = alBottom
      TabOrder = 0
      object BtnEmitir: TBitBtn
        Left = 8
        Top = 8
        Width = 90
        Height = 25
        Caption = '&Emitir'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        Glyph.Data = {
          36060000424D3606000000000000360400002800000020000000100000000100
          08000000000000020000520B0000520B00000001000000000000000000003300
          00006600000099000000CC000000FF0000000033000033330000663300009933
          0000CC330000FF33000000660000336600006666000099660000CC660000FF66
          000000990000339900006699000099990000CC990000FF99000000CC000033CC
          000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
          0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
          330000333300333333006633330099333300CC333300FF333300006633003366
          33006666330099663300CC663300FF6633000099330033993300669933009999
          3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
          330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
          66006600660099006600CC006600FF0066000033660033336600663366009933
          6600CC336600FF33660000666600336666006666660099666600CC666600FF66
          660000996600339966006699660099996600CC996600FF99660000CC660033CC
          660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
          6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
          990000339900333399006633990099339900CC339900FF339900006699003366
          99006666990099669900CC669900FF6699000099990033999900669999009999
          9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
          990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
          CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
          CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
          CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
          CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
          CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
          FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
          FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
          FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
          FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
          000000808000800000008000800080800000C0C0C00080808000191919004C4C
          4C00B2B2B200E5E5E5005A1E1E00783C3C0096646400C8969600FFC8C800465F
          82005591B9006EB9D7008CD2E600B4E6F000D8E9EC0099A8AC00646F7100E2EF
          F100C56A31000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000EEEEEEEEEEAA
          EEEEEEEEEEEEEEEEEEEEEEEEEEEEEE81EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEAA
          A2EEEEEEEEEEEEEEEEEEEEEEEEEEEE8181EEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
          AAA2EEEEEEEEEEEEEEEEEEEEEEEEEEEE8181EEEEEEEEEEEEEEEEEEEEEEEEEEEE
          AAD5A2EEEEEEEEEEEEEEEEEEEEEEEEEE81E381EEEEEEEEEEEEEEEEEEEEEEAAA2
          A2A2D4A2EEEEEEEEEEEEEEEEEEEE81818181AC81EEEEEEEEEEEEEEEEEEEEAAD5
          D4D4D4D4A2EEEEEEEEEEEEEEEEEE81E3ACACACAC81EEEEEEEEEEEEEEEEEEEEAA
          D5D4A2AAAAAAEEEEEEEEEEEEEEEEEE81E3AC81818181EEEEEEEEEEEEEEEEEEAA
          D5D4D4A2EEEEEEEEEEEEEEEEEEEEEE81E3ACAC81EEEEEEEEEEEEEEEEAAA2A2A2
          A2D5D4D4A2EEEEEEEEEEEEEE8181818181E3ACAC81EEEEEEEEEEEEEEAAD5D5D4
          D4D4D4D4D4A2EEEEEEEEEEEE81E3E3ACACACACACAC81EEEEEEEEEEEEEEAAD5D5
          D4D4A2AAAAAAEEEEEEEEEEEEEE81E3E3ACAC81818181EEEEEEEEEEEEEEAAD5D5
          D5D4D4A2EEEEEEEEEEEEEEEEEE81E3E3E3ACAC81EEEEEEEEEEEEEEEEEEEEAAD5
          D5D5D4D4A2EEEEEEEEEEEEEEEEEE81E3E3E3ACAC81EEEEEEEEEEEEEEEEEEAAD5
          D5D5D4D4D4A2EEEEEEEEEEEEEEEE81E3E3E3ACACAC81EEEEEEEEEEEEEEEEEEAA
          D5D5D5D4D4D4A2EEEEEEEEEEEEEEEE81E3E3E3ACACAC81EEEEEEEEEEEEEEEEAA
          AAAAAAAAAAAAAAAAEEEEEEEEEEEEEE818181818181818181EEEE}
        NumGlyphs = 2
        ParentFont = False
        TabOrder = 0
        OnClick = BtnEmitirClick
      end
      object BtnInicializar: TBitBtn
        Left = 104
        Top = 8
        Width = 90
        Height = 25
        Caption = '&Inicializar'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        Glyph.Data = {
          36060000424D3606000000000000360400002800000020000000100000000100
          08000000000000020000510D0000510D00000001000000000000000000003300
          00006600000099000000CC000000FF0000000033000033330000663300009933
          0000CC330000FF33000000660000336600006666000099660000CC660000FF66
          000000990000339900006699000099990000CC990000FF99000000CC000033CC
          000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
          0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
          330000333300333333006633330099333300CC333300FF333300006633003366
          33006666330099663300CC663300FF6633000099330033993300669933009999
          3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
          330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
          66006600660099006600CC006600FF0066000033660033336600663366009933
          6600CC336600FF33660000666600336666006666660099666600CC666600FF66
          660000996600339966006699660099996600CC996600FF99660000CC660033CC
          660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
          6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
          990000339900333399006633990099339900CC339900FF339900006699003366
          99006666990099669900CC669900FF6699000099990033999900669999009999
          9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
          990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
          CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
          CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
          CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
          CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
          CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
          FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
          FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
          FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
          FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
          000000808000800000008000800080800000C0C0C00080808000191919004C4C
          4C00B2B2B200E5E5E5005A1E1E00783C3C0096646400C8969600FFC8C800465F
          82005591B9006EB9D7008CD2E600B4E6F000D8E9EC0099A8AC00646F7100E2EF
          F100C56A31000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000EED7EED7D7EE
          EEEEEEEEEEEEEEEEEEEEEED7EED7D7EEEEEEEEEEEEEEEEEEEEEED7D7D7D7D7D7
          D7EEEEEEEEEEEEEEEEEED7D7D7D7D7D7D7EEEEEEEEEEEEEEEEEED7D7D7D7D7DF
          6C6C6CEEEEEEEEEEEEEED7D7D7D7D7DF565656EEEEEEEEEEEEEED7D7D7D7DF90
          6C6C6C6CEEEEEEEEEEEED7D7D7D7DF8156565656EEEEEEEEEEEED7D7D7D79090
          906C6C6C6CEEEEEEEEEED7D7D7D781818156565656EEEEEEEEEED7EED7EEB490
          90906C6C6C7EEEEEEEEED7EED7EEAC81818156565656EEEEEEEEEEEEEEEEB4B4
          9090906C7EA87EEEEEEEEEEEEEEEACAC81818156568156EEEEEEEEEEEEEEEEB4
          B490907EA8A8A87EEEEEEEEEEEEEEEACAC81815681818156EEEEEEEEEEEEEEEE
          B4D8A8D2D2A8A8A87EEEEEEEEEEEEEEEAC5681ACAC81818156EEEEEEEEEEEEEE
          EEA8D2D7DED2A8A8A87EEEEEEEEEEEEEEE81ACD7DEAC81818156EEEEEEEEEEEE
          EEEEA8D2D7DED2A87E09EEEEEEEEEEEEEEEE81ACD7DEAC815656EEEEEEEEEEEE
          EEEEEEA8D2D7D27E1009EEEEEEEEEEEEEEEEEE81ACD7AC568156EEEEEEEEEEEE
          EEEEEEEEA8D2A8101010EEEEEEEEEEEEEEEEEEEE81AC81818181EEEEEEEEEEEE
          EEEEEEEEEEA817171010EEEEEEEEEEEEEEEEEEEEEE81ACAC8181EEEEEEEEEEEE
          EEEEEEEEEEEE09171710EEEEEEEEEEEEEEEEEEEEEEEE56ACAC81EEEEEEEEEEEE
          EEEEEEEEEEEEEE091717EEEEEEEEEEEEEEEEEEEEEEEEEE56ACAC}
        NumGlyphs = 2
        ParentFont = False
        TabOrder = 1
        OnClick = BtnInicializarClick
      end
      object BtnCerrar: TBitBtn
        Left = 200
        Top = 8
        Width = 90
        Height = 25
        Caption = '&Cerrar'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        Glyph.Data = {
          36060000424D3606000000000000360400002800000020000000100000000100
          08000000000000020000710B0000710B00000001000000000000000000003300
          00006600000099000000CC000000FF0000000033000033330000663300009933
          0000CC330000FF33000000660000336600006666000099660000CC660000FF66
          000000990000339900006699000099990000CC990000FF99000000CC000033CC
          000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
          0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
          330000333300333333006633330099333300CC333300FF333300006633003366
          33006666330099663300CC663300FF6633000099330033993300669933009999
          3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
          330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
          66006600660099006600CC006600FF0066000033660033336600663366009933
          6600CC336600FF33660000666600336666006666660099666600CC666600FF66
          660000996600339966006699660099996600CC996600FF99660000CC660033CC
          660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
          6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
          990000339900333399006633990099339900CC339900FF339900006699003366
          99006666990099669900CC669900FF6699000099990033999900669999009999
          9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
          990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
          CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
          CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
          CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
          CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
          CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
          FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
          FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
          FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
          FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
          000000808000800000008000800080800000C0C0C00080808000191919004C4C
          4C00B2B2B200E5E5E5005A1E1E00783C3C0096646400C8969600FFC8C800465F
          82005591B9006EB9D7008CD2E600B4E6F000D8E9EC0099A8AC00646F7100E2EF
          F100C56A31000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000EEEEEEEEEEEE
          F1EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEF1EEEEEEEEEEEEEEEEEEEEEEEEF1E3AC
          E3F1EEEEEEEEEEEEEEEEEEEEEEF1EEACE3F1EEEEEEEEEEEEEEEEEEF1E3E28257
          57E2ACE3F1EEEEEEEEEEEEF1EEE2818181E2ACEEF1EEEEEEEEEEE382578282D7
          578181E2E3EEEEEEEEEEEE81818181D7818181E2EEEEEEEEEEEE57828989ADD7
          57797979F1EEEEEEEEEE8181DEDEACD781818181F1EEEEEEEEEE57898989ADD7
          57AAAAA2D7ADEEEEEEEE81DEDEDEACD781DEDE81D7ACEEEEEEEE57898989ADD7
          57AACEA3AD10EEEEEEEE81DEDEDEACD781DEAC81AC81EEEEEEEE5789825EADD7
          57ABCFE21110EEEEEEEE81DE8181ACD781ACACE28181EEEEEEEE578957D7ADD7
          57ABDE101010101010EE81DE56D7ACD781ACDE818181818181EE57898257ADD7
          57EE10101010101010EE81DE8156ACD781E381818181818181EE57898989ADD7
          57EE82101010101010EE81DEDEDEACD781E381818181818181EE57898989ADD7
          57ACF1821110EEEEEEEE81DEDEDEACD781ACF1818181EEEEEEEE57898989ADD7
          57ABEEAB8910EEEEEEEE81DEDEDEACD781ACE3ACDE81EEEEEEEE57828989ADD7
          57ACEEA3EE89EEEEEEEE8181DEDEACD781ACE381EEDEEEEEEEEEEEDE5E8288D7
          57A2A2A2EEEEEEEEEEEEEEDE8181DED781818181EEEEEEEEEEEEEEEEEEAC8257
          57EEEEEEEEEEEEEEEEEEEEEEEEAC818181EEEEEEEEEEEEEEEEEE}
        NumGlyphs = 2
        ParentFont = False
        TabOrder = 2
        OnClick = BtnCerrarClick
      end
    end
    object GrpPeriodo: TGroupBox
      Left = 16
      Top = 44
      Width = 177
      Height = 85
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentColor = False
      ParentFont = False
      TabOrder = 1
      object LblFechaAl: TLabel
        Left = 38
        Top = 14
        Width = 12
        Height = 13
        Alignment = taRightJustify
        Caption = 'Al:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object LblFechaDesde: TLabel
        Left = 16
        Top = 38
        Width = 34
        Height = 13
        Alignment = taRightJustify
        Caption = 'Desde:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object LblFechaHasta: TLabel
        Left = 19
        Top = 62
        Width = 31
        Height = 13
        Alignment = taRightJustify
        Caption = 'Hasta:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object DatFechaAl: TDateEdit
        Left = 61
        Top = 10
        Width = 100
        Height = 21
        DefaultToday = True
        DialogTitle = 'Seleccione Fecha'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        YearDigits = dyFour
        TabOrder = 0
        Text = '15/11/2011'
      end
      object DatFechaDesde: TDateEdit
        Left = 61
        Top = 34
        Width = 100
        Height = 21
        DefaultToday = True
        DialogTitle = 'Seleccione Fecha'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        YearDigits = dyFour
        TabOrder = 1
        Text = '15/11/2011'
      end
      object DatFechaHasta: TDateEdit
        Left = 61
        Top = 58
        Width = 100
        Height = 21
        DefaultToday = True
        DialogTitle = 'Seleccione Fecha'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        YearDigits = dyFour
        TabOrder = 2
        Text = '15/11/2011'
      end
    end
    object GrpRangoProveedor: TGroupBox
      Left = 382
      Top = 44
      Width = 177
      Height = 86
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentColor = False
      ParentFont = False
      TabOrder = 3
      object LblProveedorDesde: TLabel
        Left = 18
        Top = 14
        Width = 34
        Height = 13
        Cursor = crHandPoint
        Hint = 'Hacer doble clic para lanzar buscador...'
        Alignment = taRightJustify
        Caption = 'Desde:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        OnDblClick = LblProveedorDesdeDblClick
      end
      object LblProveedorHasta: TLabel
        Left = 21
        Top = 38
        Width = 31
        Height = 13
        Cursor = crHandPoint
        Hint = 'Hacer doble clic para lanzar buscador...'
        Alignment = taRightJustify
        Caption = 'Hasta:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        OnDblClick = LblProveedorHastaDblClick
      end
      object CmbProveedorDesde: TRxDBLookupCombo
        Left = 61
        Top = 10
        Width = 100
        Height = 21
        DropDownCount = 20
        DropDownWidth = 400
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnCloseUp = CmbProveedorDesdeCloseUp
        OnExit = CmbProveedorDesdeCloseUp
        OnKeyDown = CmbProveedorDesdeKeyDown
      end
      object CmbProveedorHasta: TRxDBLookupCombo
        Left = 61
        Top = 34
        Width = 100
        Height = 21
        DropDownCount = 20
        DropDownWidth = 400
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
      end
    end
    object GrpRangoProducto: TGroupBox
      Left = 16
      Top = 156
      Width = 177
      Height = 85
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentColor = False
      ParentFont = False
      TabOrder = 4
      object LblProductoDesde: TLabel
        Left = 16
        Top = 13
        Width = 34
        Height = 13
        Cursor = crHandPoint
        Hint = 'Hacer doble clic para lanzar buscador...'
        Alignment = taRightJustify
        Caption = 'Desde:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        OnDblClick = LblProductoDesdeDblClick
      end
      object LblProductoHasta: TLabel
        Left = 19
        Top = 37
        Width = 31
        Height = 13
        Cursor = crHandPoint
        Hint = 'Hacer doble clic para lanzar buscador...'
        Alignment = taRightJustify
        Caption = 'Hasta:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        OnDblClick = LblProductoHastaDblClick
      end
      object CmbProductoDesde: TRxDBLookupCombo
        Left = 61
        Top = 10
        Width = 100
        Height = 21
        DropDownCount = 20
        DropDownWidth = 400
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnCloseUp = CmbProductoDesdeCloseUp
        OnExit = CmbProductoDesdeCloseUp
        OnKeyDown = CmbProductoDesdeKeyDown
      end
      object CmbProductoHasta: TRxDBLookupCombo
        Left = 61
        Top = 34
        Width = 100
        Height = 21
        DropDownCount = 20
        DropDownWidth = 400
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
      end
    end
    object GrpClasificacionesProducto: TGroupBox
      Left = 199
      Top = 156
      Width = 177
      Height = 161
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentColor = False
      ParentFont = False
      TabOrder = 5
      object LblClasif1: TLabel
        Left = 13
        Top = 13
        Width = 37
        Height = 13
        Cursor = crHandPoint
        Hint = 'Hacer doble clic para lanzar buscador...'
        Caption = 'Clasif 1:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        OnDblClick = LblClasif1DblClick
      end
      object LblClasif2: TLabel
        Left = 13
        Top = 35
        Width = 37
        Height = 13
        Cursor = crHandPoint
        Hint = 'Hacer doble clic para lanzar buscador...'
        Alignment = taRightJustify
        Caption = 'Clasif 2:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        OnDblClick = LblClasif2DblClick
      end
      object LblClasif3: TLabel
        Left = 13
        Top = 59
        Width = 37
        Height = 13
        Cursor = crHandPoint
        Hint = 'Hacer doble clic para lanzar buscador...'
        Alignment = taRightJustify
        Caption = 'Clasif 3:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        OnDblClick = LblClasif3DblClick
      end
      object LblClasif4: TLabel
        Left = 13
        Top = 83
        Width = 37
        Height = 13
        Cursor = crHandPoint
        Hint = 'Hacer doble clic para lanzar buscador...'
        Alignment = taRightJustify
        Caption = 'Clasif 4:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        OnDblClick = LblClasif4DblClick
      end
      object LblClasif5: TLabel
        Left = 13
        Top = 107
        Width = 37
        Height = 13
        Cursor = crHandPoint
        Hint = 'Hacer doble clic para lanzar buscador...'
        Alignment = taRightJustify
        Caption = 'Clasif 5:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        OnDblClick = LblClasif5DblClick
      end
      object LblClasif6: TLabel
        Left = 13
        Top = 131
        Width = 37
        Height = 13
        Cursor = crHandPoint
        Hint = 'Hacer doble clic para lanzar buscador...'
        Alignment = taRightJustify
        Caption = 'Clasif 6:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        OnDblClick = LblClasif6DblClick
      end
      object CmbClasif1: TRxDBLookupCombo
        Left = 61
        Top = 10
        Width = 100
        Height = 21
        DropDownCount = 20
        DropDownWidth = 400
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnChange = CmbClasif1Change
      end
      object CmbClasif2: TRxDBLookupCombo
        Left = 61
        Top = 34
        Width = 100
        Height = 21
        DropDownCount = 20
        DropDownWidth = 400
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        OnChange = CmbClasif2Change
      end
      object CmbClasif3: TRxDBLookupCombo
        Left = 61
        Top = 58
        Width = 100
        Height = 21
        DropDownCount = 20
        DropDownWidth = 400
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        OnChange = CmbClasif3Change
      end
      object CmbClasif4: TRxDBLookupCombo
        Left = 61
        Top = 82
        Width = 100
        Height = 21
        DropDownCount = 20
        DropDownWidth = 400
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
        OnChange = CmbClasif4Change
      end
      object CmbClasif5: TRxDBLookupCombo
        Left = 61
        Top = 106
        Width = 100
        Height = 21
        DropDownCount = 20
        DropDownWidth = 400
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
        OnChange = CmbClasif5Change
      end
      object CmbClasif6: TRxDBLookupCombo
        Left = 61
        Top = 130
        Width = 100
        Height = 21
        DropDownCount = 20
        DropDownWidth = 400
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
        OnChange = CmbClasif6Change
      end
    end
    object GrpOtros: TGroupBox
      Left = 382
      Top = 156
      Width = 177
      Height = 161
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentColor = False
      ParentFont = False
      TabOrder = 6
      object LblEstacion: TLabel
        Left = 8
        Top = 37
        Width = 44
        Height = 13
        Cursor = crHandPoint
        Hint = 'Hacer doble clic para lanzar buscador...'
        Alignment = taRightJustify
        Caption = 'Estaci'#243'n:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        OnDblClick = LblEstacionDblClick
      end
      object LblZona: TLabel
        Left = 24
        Top = 13
        Width = 28
        Height = 13
        Cursor = crHandPoint
        Hint = 'Hacer doble clic para lanzar buscador...'
        Alignment = taRightJustify
        Caption = 'Zona:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        OnDblClick = LblZonaDblClick
      end
      object LblMoneda: TLabel
        Left = 10
        Top = 61
        Width = 42
        Height = 13
        Cursor = crHandPoint
        Hint = 'Hacer doble clic para lanzar buscador...'
        Alignment = taRightJustify
        Caption = 'Moneda:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        OnDblClick = LblMonedaDblClick
      end
      object LblCuentaBancaria: TLabel
        Left = 5
        Top = 85
        Width = 47
        Height = 13
        Cursor = crHandPoint
        Hint = 'Hacer doble clic para lanzar buscador...'
        Alignment = taRightJustify
        Caption = 'Cta.Banc:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        OnDblClick = LblCuentaBancariaDblClick
      end
      object LblVendedor: TLabel
        Left = 3
        Top = 108
        Width = 49
        Height = 13
        Cursor = crHandPoint
        Hint = 'Hacer doble clic para lanzar buscador...'
        Alignment = taRightJustify
        Caption = 'Vendedor:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        OnDblClick = LblVendedorDblClick
      end
      object CmbEstacion: TRxDBLookupCombo
        Left = 61
        Top = 34
        Width = 100
        Height = 21
        DropDownCount = 20
        DropDownWidth = 400
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
      end
      object CmbZona: TRxDBLookupCombo
        Left = 61
        Top = 10
        Width = 100
        Height = 21
        DropDownCount = 20
        DropDownWidth = 400
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
      end
      object CmbMoneda: TRxDBLookupCombo
        Left = 61
        Top = 58
        Width = 100
        Height = 21
        DropDownCount = 20
        DropDownWidth = 400
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
      end
      object CmbCuentaBancaria: TRxDBLookupCombo
        Left = 61
        Top = 82
        Width = 100
        Height = 21
        DropDownCount = 20
        DropDownWidth = 400
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
      end
      object CmbVendedor: TRxDBLookupCombo
        Left = 61
        Top = 105
        Width = 100
        Height = 21
        DropDownCount = 20
        DropDownWidth = 400
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
      end
    end
    object GrpTipoReporte: TGroupBox
      Left = 16
      Top = 359
      Width = 177
      Height = 38
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentColor = False
      ParentFont = False
      TabOrder = 7
      object CmbTipoReporte: TComboBox
        Left = 16
        Top = 11
        Width = 145
        Height = 21
        Style = csDropDownList
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnClick = CmbTipoReporteClick
      end
    end
    object GrpClaseReporte: TGroupBox
      Left = 199
      Top = 359
      Width = 177
      Height = 38
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentColor = False
      ParentFont = False
      TabOrder = 8
      object CmbClaseReporte: TComboBox
        Left = 16
        Top = 11
        Width = 145
        Height = 21
        Style = csDropDownList
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnClick = CmbTipoReporteClick
      end
    end
    object GrpRangoCliente: TGroupBox
      Left = 199
      Top = 44
      Width = 177
      Height = 86
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentColor = False
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      object LblClienteDesde: TLabel
        Left = 16
        Top = 14
        Width = 34
        Height = 13
        Cursor = crHandPoint
        Hint = 'Hacer doble clic para lanzar buscador...'
        Alignment = taRightJustify
        Caption = 'Desde:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = False
        OnDblClick = LblClienteDesdeDblClick
      end
      object LblClienteHasta: TLabel
        Left = 19
        Top = 38
        Width = 31
        Height = 13
        Cursor = crHandPoint
        Hint = 'Hacer doble clic para lanzar buscador...'
        Alignment = taRightJustify
        Caption = 'Hasta:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = False
        OnDblClick = LblClienteHastaDblClick
      end
      object CmbClienteDesde: TRxDBLookupCombo
        Left = 56
        Top = 10
        Width = 100
        Height = 21
        DropDownCount = 20
        DropDownWidth = 400
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnCloseUp = CmbClienteDesdeCloseUp
        OnExit = CmbClienteDesdeCloseUp
        OnKeyDown = CmbClienteDesdeKeyDown
      end
      object CmbClienteHasta: TRxDBLookupCombo
        Left = 56
        Top = 34
        Width = 100
        Height = 21
        DropDownCount = 20
        DropDownWidth = 400
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
      end
    end
    object GrpTipoOrdenamiento: TGroupBox
      Left = 382
      Top = 359
      Width = 177
      Height = 38
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentColor = False
      ParentFont = False
      TabOrder = 9
      object CmbTipoOrdenamiento: TComboBox
        Left = 16
        Top = 11
        Width = 145
        Height = 21
        Style = csDropDownList
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnClick = CmbTipoReporteClick
      end
    end
  end
  object TemaXP: TXPManifest
    Left = 8
    Top = 8
  end
  object DSClienteDesde: TDataSource
    Left = 224
    Top = 296
  end
  object DSClienteHasta: TDataSource
    Left = 256
    Top = 296
  end
  object DSProveedorHasta: TDataSource
    Left = 320
    Top = 296
  end
  object DSProveedorDesde: TDataSource
    Left = 288
    Top = 296
  end
  object DSProductoDesde: TDataSource
    Left = 352
    Top = 296
  end
  object DSProductoHasta: TDataSource
    Left = 384
    Top = 296
  end
  object DSClasif1: TDataSource
    Left = 416
    Top = 296
  end
  object DSClasif2: TDataSource
    Left = 448
    Top = 296
  end
  object DSClasif3: TDataSource
    Left = 480
    Top = 296
  end
  object DSClasif4: TDataSource
    Left = 512
    Top = 296
  end
  object DSClasif5: TDataSource
    Left = 544
    Top = 296
  end
  object DSClasif6: TDataSource
    Left = 576
    Top = 296
  end
  object DSEstacion: TDataSource
    Left = 608
    Top = 296
  end
  object DSZona: TDataSource
    Left = 640
    Top = 296
  end
  object DSMoneda: TDataSource
    Left = 672
    Top = 296
  end
  object DSCuentaBancaria: TDataSource
    Left = 704
    Top = 296
  end
  object DsVendedor: TDataSource
    Left = 736
    Top = 296
  end
end
