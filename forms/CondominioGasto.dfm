inherited FrmCondominioGasto: TFrmCondominioGasto
  Caption = 'Sophie - Gastos'
  PixelsPerInch = 96
  TextHeight = 13
  inherited Pag: TPageControl
    ActivePage = TabDatos
    inherited TabDatos: TTabSheet
      ExplicitLeft = 8
      object LblCodigo: TLabel
        Left = 122
        Top = 31
        Width = 41
        Height = 13
        Alignment = taRightJustify
        Caption = 'C'#243'digo:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblDescripcion: TLabel
        Left = 95
        Top = 55
        Width = 68
        Height = 13
        Alignment = taRightJustify
        Caption = 'Descripci'#243'n:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblUsarAlicuotaInmueble: TLabel
        Left = 37
        Top = 195
        Width = 126
        Height = 13
        Alignment = taRightJustify
        Caption = 'Alicuota del Inmueble:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblUsarAlicuotaParticular: TLabel
        Left = 56
        Top = 219
        Width = 107
        Height = 13
        Alignment = taRightJustify
        Caption = 'Alicuota Particular:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblExtraordinario: TLabel
        Left = 79
        Top = 267
        Width = 84
        Height = 13
        Alignment = taRightJustify
        Caption = 'Extraordinario:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblMontoFijo: TLabel
        Left = 101
        Top = 243
        Width = 62
        Height = 13
        Alignment = taRightJustify
        Caption = 'Monto Fijo:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object TxtCodigoConjuntoAdministracion: TDBText
        Left = 325
        Top = 7
        Width = 162
        Height = 13
        AutoSize = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object LblCodigoConjuntoAdministracion: TLabel
        Left = 4
        Top = 7
        Width = 159
        Height = 13
        Alignment = taRightJustify
        Caption = 'Conjunto de Administraci'#243'n:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblPAlicuota: TLabel
        Left = 203
        Top = 219
        Width = 75
        Height = 13
        Alignment = taRightJustify
        Caption = '(%) Alicuota:'
        Enabled = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblMonto: TLabel
        Left = 239
        Top = 243
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Monto:'
        Enabled = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblCodigoTipoGasto: TLabel
        Left = 83
        Top = 171
        Width = 80
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo de Gasto:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object TxtCodigoTipoGasto: TDBText
        Left = 325
        Top = 171
        Width = 97
        Height = 13
        AutoSize = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object LblDescripcionLarga: TLabel
        Left = 60
        Top = 79
        Width = 103
        Height = 13
        Alignment = taRightJustify
        Caption = 'Descripci'#243'n Larga:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object TxtCodigo: TEdit
        Left = 169
        Top = 28
        Width = 150
        Height = 21
        TabOrder = 1
      end
      object TxtDescripcion: TEdit
        Left = 169
        Top = 52
        Width = 338
        Height = 21
        TabOrder = 2
      end
      object ChkUsarAlicuotaInmueble: TCheckBox
        Left = 169
        Top = 194
        Width = 15
        Height = 17
        TabOrder = 5
        OnClick = ChkUsarAlicuotaInmuebleClick
      end
      object ChkUsarAlicuotaParticular: TCheckBox
        Left = 169
        Top = 218
        Width = 15
        Height = 17
        TabOrder = 6
        OnClick = ChkUsarAlicuotaParticularClick
      end
      object ChkExtraordinario: TCheckBox
        Left = 169
        Top = 266
        Width = 15
        Height = 17
        TabOrder = 10
      end
      object ChkMontoFijo: TCheckBox
        Left = 169
        Top = 242
        Width = 15
        Height = 17
        TabOrder = 8
        OnClick = ChkMontoFijoClick
      end
      object CmbCodigoConjuntoAdministracion: TRxDBLookupCombo
        Left = 169
        Top = 4
        Width = 150
        Height = 21
        DropDownCount = 20
        DropDownWidth = 400
        TabOrder = 0
      end
      object TxtPAlicuota: TCurrencyEdit
        Left = 285
        Top = 216
        Width = 100
        Height = 21
        Margins.Left = 4
        Margins.Top = 1
        DecimalPlaces = 4
        DisplayFormat = '#,##0.0000'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 7
      end
      object TxtMonto: TCurrencyEdit
        Left = 285
        Top = 240
        Width = 100
        Height = 21
        Margins.Left = 4
        Margins.Top = 1
        DecimalPlaces = 4
        DisplayFormat = '#,##0.0000'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 9
      end
      object CmbCodigoTipoGasto: TRxDBLookupCombo
        Left = 169
        Top = 168
        Width = 150
        Height = 21
        DropDownCount = 20
        DropDownWidth = 400
        TabOrder = 4
      end
      object TxtDescripcionLarga: TMemo
        Left = 169
        Top = 76
        Width = 338
        Height = 89
        TabOrder = 3
      end
    end
  end
  object DSConjuntoAdministracion: TDataSource
    Left = 16
    Top = 228
  end
  object DSTipoGasto: TDataSource
    Left = 48
    Top = 228
  end
end
