unit SophieSplashScreen;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, pngimage, frxClass;

type
  TFrmSophieSplashScreen = class(TForm)
    Image1: TImage;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmSophieSplashScreen: TFrmSophieSplashScreen;

implementation

//uses
//uses
  //GR32, GR32_PNG, GR32_PortableNetworkGraphic;
//  PNGImage, Math;

{$R *.dfm}

(*
procedure MergePNGLayer(Layer1,Layer2: TPNGObject; Const aLeft,aTop:Integer);
var
  x, y: Integer;
  SL1,  SL2,  SLBlended : pRGBLine;
  aSL1, aSL2, aSLBlended: PByteArray;
  blendCoeff: single;
  blendedPNG, Lay2buff: TPNGObject;
begin
  blendedPNG:=TPNGObject.Create;
  blendedPNG.Assign(Layer1);
  Lay2buff:=TPNGObject.Create;
  Lay2buff.Assign(Layer2);
//  SetPNGCanvasSize(Layer2,Layer1.Width,Layer1.Height,aLeft,aTop);
  for y := 0 to Layer1.Height-1 do
  begin
    SL1 := Layer1.Scanline[y];
    SL2 := Layer2.Scanline[y];
    aSL1 := Layer1.AlphaScanline[y];
    aSL2 := Layer2.AlphaScanline[y];
    SLBlended := blendedPNG.Scanline[y];
    aSLBlended := blendedPNG.AlphaScanline[y];
    for x := 0 to Layer1.Width-1 do
    begin
      blendCoeff:=aSL1[x] * 100/255/100;
      aSLBlended[x] := round(aSL2[x] + (aSL1[x]-aSL2[x]) * blendCoeff);
      SLBlended[x].rgbtRed   := round(SL2[x].rgbtRed + (SL1[x].rgbtRed-SL2[x].rgbtRed) * blendCoeff);
      SLBlended[x].rgbtGreen := round(SL2[x].rgbtGreen + (SL1[x].rgbtGreen-SL2[x].rgbtGreen) * blendCoeff);
      SLBlended[x].rgbtBlue  := round(SL2[x].rgbtBlue + (SL1[x].rgbtBlue-SL2[x].rgbtBlue) * blendCoeff);
    end;
  end;
  Layer1.Assign(blendedPNG);
  Layer2.Assign(Lay2buff);
  blendedPNG.Free;
  Lay2buff.Free;
end;
*)

{
function LoadPNGintoBitmap32 (destBitmap: TBitmap32;
                               srcStream: TStream;
                               out transparent: Boolean): boolean; overload;
var
   PNGObject: TPNGObject;
   TransparentColor: TColor32;
   PixelPtr: PColor32;
   AlphaPtr: PByte;
   X, Y: Integer;
begin
   PNGObject := nil;
   try
     result := false;
     PNGObject := TPngObject.Create;
     PNGObject.LoadFromStream(srcStream);

     destBitmap.Assign(PNGObject);
     destBitmap.ResetAlpha;

     case PNGObject.TransparencyMode of
       ptmPartial:
         begin
           if (PNGObject.Header.ColorType = COLOR_GRAYSCALEALPHA) or
              (PNGObject.Header.ColorType = COLOR_RGBALPHA) then
           begin
             PixelPtr := PColor32(@destBitmap.Bits[0]);
             for Y := 0 to destBitmap.Height - 1 do
             begin
               AlphaPtr := PByte(PNGObject.AlphaScanline[Y]);
               for X := 0 to destBitmap.Width - 1 do
               begin
                 PixelPtr^ := (PixelPtr^ and $00FFFFFF) or
(TColor32(AlphaPtr^) shl 24);
                 Inc(PixelPtr);
                 Inc(AlphaPtr);
               end;
             end;
             transparent := True;
           end;
         end;
       ptmBit:
         begin
           TransparentColor := Color32(PNGObject.TransparentColor);
           PixelPtr := PColor32(@destBitmap.Bits[0]);
           for X := 0 to (destBitmap.Height - 1) * (destBitmap.Width - 1) do
           begin
             if PixelPtr^ = TransparentColor then
               PixelPtr^ := PixelPtr^ and $00FFFFFF;
             Inc(PixelPtr);
           end;
           transparent := True;
         end;
       ptmNone:
         transparent := False;
     end;
     result := true;
   finally
     if Assigned(PNGObject) then PNGObject.Free;
   end;
end;

function LoadPNGintoBitmap32 (destBitmap: TBitmap32;
                               filename: String;
                               out transparent: boolean): boolean; overload;
var
   FileStream: TFileStream;
begin
   result := false;
   try
     FileStream := TFileStream.Create(filename, fmOpenRead);
     try
       result := LoadPNGintoBitmap32(destBitmap, FileStream, transparent);
     finally
       FileStream.Free;
     end;
   except
   end;
end;

function Bitmap32ToPNG (sourceBitmap: TBitmap32;
                         paletted, transparent: Boolean;
                         bgColor: TColor;
                         compressionLevel: TCompressionLevel = 9;
                         interlaceMethod: TInterlaceMethod = imNone):
tPNGObject;
var
   bm: TBitmap;
   png: TPngObject;
   TRNS: TCHUNKtRNS;
   p: pngImage.PByteArray;
   x, y: Integer;
begin
   Result := nil;
   png := TPngObject.Create;
   try
     bm := TBitmap.Create;
     try
       bm.Assign (sourceBitmap);        // convert data into bitmap
       // force paletted on TBitmap, transparent for the web must be 8bit
       if paletted then
         bm.PixelFormat := pf8bit;
       png.interlaceMethod := interlaceMethod;
       png.compressionLevel := compressionLevel;
       png.Assign(bm);                  // convert bitmap into PNG
     finally
       FreeAndNil(bm);
     end;
     if transparent then begin
       if png.Header.ColorType in [COLOR_PALETTE] then begin
         if (png.Chunks.ItemFromClass(TChunktRNS) = nil) then
          png.CreateAlpha;
         TRNS := png.Chunks.ItemFromClass(TChunktRNS) as TChunktRNS;
         if Assigned(TRNS) then TRNS.TransparentColor := bgColor;
       end;
       if png.Header.ColorType in [COLOR_RGB, COLOR_GRAYSCALE] then
        png.CreateAlpha;
       if png.Header.ColorType in [COLOR_RGBALPHA, COLOR_GRAYSCALEALPHA] then
       begin
         for y := 0 to png.Header.Height - 1 do begin
           p := png.AlphaScanline[y];
           for x := 0 to png.Header.Width - 1
           do p[x] := AlphaComponent(sourceBitmap.Pixel[x,y]);  //TARGB(bm32.Pixel[x,y]).a;
         end;
       end;
     end;
     Result := png;
   except
     png.Free;
   end;
end;}
//*)

(*
procedure DrawPngWithAlpha(Src, Dest: TPNGObject; const R: TRect);
var
  X, Y: Integer;
  Alpha: PByte;
begin
  Src.Draw(Dest.Canvas, R);
  // I have no idea why standard implementation of TPNGObject.Draw doesn't apply transparency.
  for Y := R.Top to R.Bottom - 1 do
    for X := R.Left to R.Right - 1 do
    begin
      Alpha := @Dest.AlphaScanline[Y]^[X];
      Alpha^ := Min(255, Alpha^ + Src.AlphaScanline[Y - R.Top]^[X - R.Left]);
    end;
end;
*)

procedure TFrmSophieSplashScreen.FormShow(Sender: TObject);
//var
//  Img: TPortableNetworkGraphic32;
//  Bitmap: TBitmap32;

  //Stream: TMemoryStream;
//  ImagenPNG: TPNGObject;
 // AlphaPNG: TPNGObject;
 // Bitmap: TBitmap;
 // B32: TBitmap32;
 // Point: TPoint;
 // Transparent: Boolean;
begin
//  Stream := TMemoryStream.Create;

(*
  with TPortableNetworkGraphic32.Create do
  begin
    try
//      Image321.Bitmap.CombineMode := cmBlend;

      LoadFromFile(ExtractFilePath(Application.ExeName) + '\imagenes\Acerca.png');
      AssignTo(Image321.Bitmap);
    finally
      Free;
    end;
  end;
  *)
(*
  Bitmap := TBitmap.Create;
  Point.X := 0;
  Point.Y := 0;
//  Bitmap.PixelFormat := pf32bit;
  Bitmap.HandleType := bmDIB;
  Bitmap.Width := 450;
  Bitmap.Height := 350;
  ImagenPNG := TPNGObject.Create;
  AlphaPNG := TPNGObject.CreateBlank(COLOR_RGBALPHA,16,450,350);
//  ImagenPNG.InterlaceMethod := imAdam7;
  ImagenPNG.LoadFromFile(ExtractFilePath(Application.ExeName) + '\imagenes\Acerca.png');
//  AlphaPNG.LoadFromFile(ExtractFilePath(Application.ExeName) + '\imagenes\Acerca.png');
//  ImagenPNG.CreateAlpha;
//  ImagenPNG.Filters
  DrawPngWithAlpha(ImagenPNG,AlphaPNG,Rect(0,0,ImagenPNG.Width,ImagenPNG.Height));
  //Bitmap.Canvas.Draw(0,0,ImagenPNG);
  Bitmap.Canvas.Draw(0,0,AlphaPNG);
  Image1.Picture.Bitmap := Bitmap;
  *)
  (*
  ImagenPNG := TPNGObject.Create;
  AlphaPNG := TPNGObject.CreateBlank(COLOR_RGBALPHA,16,450,350);
  ImagenPNG.LoadFromFile(ExtractFilePath(Application.ExeName) + '\imagenes\Acerca.png');
  MergePNGLayer(AlphaPNG,ImagenPNG, 0, 0);
  Image1.Picture.Assign(AlphaPNG);
  *)
  (*
  B32 := TBitmap32.Create;
  Transparent := True;
  LoadPNGintoBitmap32(B32,ExtractFilePath(Application.ExeName) + '\imagenes\Acerca.png',Transparent);
  if Transparent then
     B32.DrawMode := dmBlend
  else
     B32.DrawMode := dmOpaque;
  AlphaPNG := Bitmap32ToPNG(B32,False,Transparent,clBtnFace);
  Image1.Picture.Assign(AlphaPNG);
//Image321.Bitmap.Assign(B32);
*)

end;

end.
