inherited FrmAgregarDetalleRadioLiquidacionContrato: TFrmAgregarDetalleRadioLiquidacionContrato
  Caption = 'Sophie - Agregar Distribuci'#243'n de Factura...'
  ClientHeight = 172
  OnCreate = FormCreate
  ExplicitWidth = 350
  ExplicitHeight = 196
  PixelsPerInch = 96
  TextHeight = 13
  object LblNumeroContrato: TLabel [1]
    Left = 11
    Top = 35
    Width = 47
    Height = 13
    Alignment = taRightJustify
    Caption = 'Contrato:'
  end
  object LblCliente: TLabel [2]
    Left = 21
    Top = 83
    Width = 37
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cliente:'
  end
  object LblItem: TLabel [3]
    Left = 32
    Top = 59
    Width = 26
    Height = 13
    Alignment = taRightJustify
    Caption = 'Item:'
  end
  object LblTotal: TLabel [4]
    Left = 30
    Top = 107
    Width = 28
    Height = 13
    Alignment = taRightJustify
    Caption = 'Total:'
  end
  object LblRazonSocialCliente: TLabel [5]
    Left = 170
    Top = 83
    Width = 3
    Height = 13
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  inherited TxtCodigo: TEdit
    Font.Style = [fsItalic]
    ParentFont = False
    Text = '[Realice una B'#250'squeda, Presione F3]'
  end
  inherited PanelBotones: TPanel
    Top = 132
    ExplicitTop = 132
  end
  inherited BtnBuscar: TBitBtn
    OnClick = BtnBuscarClick
  end
  object TxtItem: TEdit
    Left = 64
    Top = 56
    Width = 75
    Height = 21
    Enabled = False
    TabOrder = 3
  end
  object TxtNumeroContrato: TEdit
    Left = 64
    Top = 32
    Width = 100
    Height = 21
    Enabled = False
    TabOrder = 4
  end
  object TxtCodigoCliente: TEdit
    Left = 64
    Top = 80
    Width = 100
    Height = 21
    Enabled = False
    TabOrder = 5
  end
  object TxtTotal: TCurrencyEdit
    Left = 64
    Top = 104
    Width = 100
    Height = 21
    Margins.Left = 4
    Margins.Top = 1
    DecimalPlaces = 4
    DisplayFormat = '#,##0.0000'
    Enabled = False
    TabOrder = 6
  end
end
