unit LiquidacionCobranza;

{$I sophie.inc}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GenericoDetalle, DB, DBClient, ImgList, XPMan, ComCtrls, Grids,
  DBGrids, ExtCtrls, ToolWin, StdCtrls, Buttons, rxCurrEdit, Mask, rxToolEdit,
  RxLookup, Utils, ClsLiquidacionCobranza, ClsDetLiquidacionCobranza,
  ClsTipoRetencion, ClsAnticipoCliente, ClsFacturacionCliente,
  ClsNotaDebitoCliente, ClsNotaCreditoCliente, ClsCliente, ClsZona,
  ClsFormaPago, ClsCuentaBancaria, ClsBancoCobranza, ClsMoneda,
  ClsBusquedaLiquidacionCobranza, ClsCobranzaCobrada, ClsCuentaxCobrar,
  ClsOperacionBancaria, GenericoBuscador, ClsReporte, ClsParametro
  {$IFDEF DELPHI10}, ImageList{$ENDIF};

type

  TFrmLiquidacionCobranza = class(TFrmGenericoDetalle)
    TabObservaciones: TTabSheet;
    TxtObservaciones: TRichEdit;
    LblNumero: TLabel;
    LblCodigoCliente: TLabel;
    LblCodigoCuentaBancaria: TLabel;
    LblCodigoZona: TLabel;
    LblCodigoEsquemaPago: TLabel;
    LblMoneda: TLabel;
    LblFechaIngreso: TLabel;
    LblMontoNeto: TLabel;
    TxtNumero: TEdit;
    CmbCodigoCliente: TRxDBLookupCombo;
    DatFechaIngreso: TDateEdit;
    CmbCodigoCuentaBancaria: TRxDBLookupCombo;
    DatFechaContable: TDateEdit;
    CmbCodigoZona: TRxDBLookupCombo;
    CmbCodigoFormaPago: TRxDBLookupCombo;
    CmbCodigoMoneda: TRxDBLookupCombo;
    BtnBuscarCliente: TBitBtn;
    BtnBuscarCuentaBancaria: TBitBtn;
    BtnBuscarZona: TBitBtn;
    BtnBuscarFormaPago: TBitBtn;
    BtnBuscarMoneda: TBitBtn;
    TxtNumeroFormaPago: TEdit;
    LblNumeroFormaPago: TLabel;
    LblFechaBancaria: TLabel;
    DatFechaBancaria: TDateEdit;
    TxtNumeroDocumentoBancario: TEdit;
    LblNumeroDocumentoBancario: TLabel;
    LblCodigoBancoCobranza: TLabel;
    CmbCodigoBancoCobranza: TRxDBLookupCombo;
    BtnBuscarBancoCobranza: TBitBtn;
    LblTotalRetencion: TLabel;
    TxtTotalRetencion: TCurrencyEdit;
    Dataid_cobranza: TIntegerField;
    Dataorden_cobranza: TIntegerField;
    Dataabonado: TFloatField;
    Datacodigo_retencion_iva: TStringField;
    Datamonto_retencion_iva: TFloatField;
    Datacodigo_retencion_1: TStringField;
    Datacodigo_retencion_2: TStringField;
    Datacodigo_retencion_3: TStringField;
    Datacodigo_retencion_4: TStringField;
    Datacodigo_retencion_5: TStringField;
    Datamonto_retencion_1: TFloatField;
    Datamonto_retencion_2: TFloatField;
    Datamonto_retencion_3: TFloatField;
    Datamonto_retencion_4: TFloatField;
    Datamonto_retencion_5: TFloatField;
    Datadesde: TStringField;
    Datanumero_documento: TStringField;
    Datamonto_base: TFloatField;
    Datap_iva: TFloatField;
    Dataimpuesto: TFloatField;
    Datatotal: TFloatField;
    Datadescripcion_retencion_iva: TStringField;
    Datadescripcion_retencion_1: TStringField;
    Datadescripcion_retencion_2: TStringField;
    Datadescripcion_retencion_3: TStringField;
    Datadescripcion_retencion_4: TStringField;
    Datadescripcion_retencion_5: TStringField;
    DSCliente: TDataSource;
    DSZona: TDataSource;
    DSMoneda: TDataSource;
    DSFormaPago: TDataSource;
    DSCuentaBancaria: TDataSource;
    DSBancoCobranza: TDataSource;
    TxtMontoNeto: TCurrencyEdit;
    Datanumero_cuota: TIntegerField;
    LblTotal: TLabel;
    TxtTotal: TCurrencyEdit;
    LblFechaContable: TLabel;
    procedure GridDetalleDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure BtnBuscarBancoCobranzaClick(Sender: TObject);
    procedure BtnBuscarCuentaBancariaClick(Sender: TObject);
    procedure BtnBuscarFormaPagoClick(Sender: TObject);
    procedure BtnBuscarMonedaClick(Sender: TObject);
    procedure BtnBuscarZonaClick(Sender: TObject);
    procedure BtnBuscarClienteClick(Sender: TObject);
    procedure CmbCodigoClienteExit(Sender: TObject);
    procedure CmbCodigoClienteChange(Sender: TObject);
    procedure DataAfterPost(DataSet: TDataSet);
    procedure GridDetalleKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure TBtnEliminarClick(Sender: TObject);
  private
    { Private declarations }
    Clase: TLiquidacionCobranza;
    DetLiquidacionCobranza: TDetLiquidacionCobranza;
    BuscadorLiquidacionCobranza: TBusquedaLiquidacionCobranza;
    Cliente: TCliente;
    Zona: TZona;
    Moneda: TMoneda;
    FormaPago: TFormaPago;
    OperacionBancaria: TOperacionBancaria;
    CuentaBancaria: TCuentaBancaria;
    BancoCobranza: TBancoCobranza;
    AnticipoCliente: TAnticipoCliente;
    FacturaCliente: TFacturacionCliente;
    NotaDebitoCliente: TNotaDebitoCliente;
    NotaCreditoCliente: TNotaCreditoCliente;
    TipoRetencion: TTipoRetencion;
    CuentaxCobrar: TCuentaxCobrar;
    CobranzaCobrada: TCobranzaCobrada;
    Reporte: TReporte;
    Parametro: TParametro;
    DataBuscadorLiquidacionCobranza: TClientDataSet;
    DataCliente: TClientDataSet;
    DataZona: TClientDataSet;
    DataMoneda: TClientDataSet;
    DataFormaPago: TClientDataSet;
    DataCuentaBancaria: TClientDataSet;
    DataBancoCobranza: TClientDataSet;
    procedure Agregar;
    procedure Editar;
    procedure Eliminar;
    procedure Guardar;
    procedure Cancelar;
    procedure Refrescar;
    procedure Imprimir;
    procedure Buscar;
    procedure Limpiar;
    procedure Asignar;
    procedure Obtener;
    procedure Mover;
    procedure GuardarDetalles;
    procedure GuardarOperacionesBancarias;
    procedure CalcularMontos;
  protected
    { Protected declarations }
    function Validar: Boolean; override;
    procedure Detalle; override;
  public
    { Public declarations }
  end;

var
  FrmLiquidacionCobranza: TFrmLiquidacionCobranza;

implementation

uses
  ActividadSophie,
  PModule,
  ClsGenericoBD,
  AgregarDetalleLiquidacionCobranza,
  Math,
  Types
  {$IFDEF USE_RAVE_REPORTS}
  , DMRR
  {$ENDIF}
  {$IFDEF USE_FAST_REPORTS}
  , DMFR
  {$ENDIF}
  ;
  
{$R *.dfm}

{ TFrmLiquidacionCobranza }

procedure TFrmLiquidacionCobranza.Agregar;
begin
  //Verificando
  Limpiar;
  //Asignando Valores por Defecto
  CmbCodigoZona.Value := Parametro.ObtenerValor('codigo_zona');
  CmbCodigoMoneda.Value := Parametro.ObtenerValor('codigo_moneda');
  CmbCodigoCliente.SetFocus;
end;

procedure TFrmLiquidacionCobranza.Asignar;
var
  DataDetalle: TClientDataSet;
  Campo: string;
  i: Integer;

  function BuscarDescripcionRetencion(varCodigo: string): string;
  begin
    Result := '';
    if (Length(Trim(varCodigo)) > 0) then
    begin
      TipoRetencion.SetCodigo(varCodigo);
      TipoRetencion.Buscar;
      Result := TipoRetencion.GetDescripcion;
    end;       
  end;

begin
  with Clase do
  begin
    TxtNumero.Text := GetNumero;
    CmbCodigoMoneda.KeyValue := GetCodigoMoneda;
    CmbCodigoCliente.KeyValue := GetCodigoCliente;
    CmbCodigoZona.KeyValue := GetCodigoZona;
    DatFechaIngreso.Date := GetFechaIngreso;
    DatFechaContable.Date := GetFechaContable;
    TxtObservaciones.Text := GetObservaciones;
    TxtTotal.Value := GetTotal;
    TxtTotalRetencion.Value := GetTotalRetencion;
    CmbCodigoFormaPago.KeyValue := GetCodigoFormaPago;
    TxtNumeroFormaPago.Text := GetNumeroFormaPago;
    CmbCodigoCuentaBancaria.KeyValue := GetCodigoCuentaBancaria;
    TxtNumeroDocumentoBancario.Text := GetNumeroDocumentoBancario;
    CmbCodigoBancoCobranza.KeyValue := GetCodigoBancoCobranza;
    DatFechaBancaria.Date := GetFechaOperacionBancaria;
    //Cargando DataSet Secundario "Data"
    DataDetalle := TClientDataSet.Create(Self);
    DataDetalle.CloneCursor(DetLiquidacionCobranza.ObtenerIdLiquidacionCobranza(GetIdLiquidacionCobranza),False,True);
    Data.EmptyDataSet;
    DataDetalle.First;
    while (not DataDetalle.Eof) do
    begin
      Data.Insert;
      //Asignando Campos Similares
      for i := 0 to DataDetalle.FieldCount - 1 do
      begin
        Campo := DataDetalle.FieldDefs[i].Name;
        if (Data.FieldDefs.IndexOf(Campo) <> -1) then
        begin
          Data.FieldValues[Campo] := DataDetalle.FieldValues[Campo];
        end;        
      end;
      CobranzaCobrada.SetIdCobranza(DataDetalle.FieldValues['id_cobranza']);
      CobranzaCobrada.SetNumeroCuota(DataDetalle.FieldValues['numero_cuota']);
      CobranzaCobrada.SetOrdenCobranza(DataDetalle.FieldValues['orden_cobranza']);
      CobranzaCobrada.Buscar;
      Data.FieldValues['desde'] := CobranzaCobrada.GetDesde;
      Data.FieldValues['numero_documento'] := CobranzaCobrada.GetNumeroDocumento;
      //Asignando Campos Faltantes
      if (CobranzaCobrada.GetDesde = 'AC') then
      begin //Anticipo de Cliente
        AnticipoCliente.SetNumero(CobranzaCobrada.GetNumeroDocumento);
        AnticipoCliente.BuscarNumero;
        Data.FieldValues['monto_base'] := AnticipoCliente.GetMontoNeto;
        Data.FieldValues['p_iva'] := 0;
        Data.FieldValues['impuesto'] := 0;
        Data.FieldValues['total'] := AnticipoCliente.GetMontoNeto;
      end
      else
      begin
        if (CobranzaCobrada.GetDesde = 'NDC') then
        begin //Nota Debito a Cliente
          NotaDebitoCliente.SetNumero(CobranzaCobrada.GetNumeroDocumento);
          NotaDebitoCliente.BuscarNumero;
          Data.FieldValues['monto_base'] := NotaDebitoCliente.GetMontoNeto;
          Data.FieldValues['p_iva'] := NotaDebitoCliente.GetPIva;
          Data.FieldValues['impuesto'] := NotaDebitoCliente.GetImpuesto;
          Data.FieldValues['total'] := NotaDebitoCliente.GetTotal;
        end
        else
        begin 
          if (CobranzaCobrada.GetDesde = 'NCC') then
          begin //Nota Credito de Cliente
            NotaCreditoCliente.SetNumero(CobranzaCobrada.GetNumeroDocumento);
            NotaCreditoCliente.BuscarNumero;
            Data.FieldValues['monto_base'] := NotaCreditoCliente.GetMontoNeto;
            Data.FieldValues['p_iva'] := NotaCreditoCliente.GetPIva;
            Data.FieldValues['impuesto'] := NotaCreditoCliente.GetImpuesto;
            Data.FieldValues['total'] := NotaCreditoCliente.GetTotal;
          end
          else
          begin //Factura de Cliente
            FacturaCliente.SetNumero(CobranzaCobrada.GetNumeroDocumento);
            FacturaCliente.BuscarNumero;
            Data.FieldValues['monto_base'] := FacturaCliente.GetMontoNeto;
            Data.FieldValues['p_iva'] := FacturaCliente.GetPIva;
            Data.FieldValues['impuesto'] := FacturaCliente.GetImpuesto;
            Data.FieldValues['total'] := FacturaCliente.GetTotal;
          end;
        end;
      end;
      Data.FieldValues['descripcion_retencion_iva'] := BuscarDescripcionRetencion(DataDetalle.FieldValues['codigo_retencion_iva']);
      Data.FieldValues['descripcion_retencion_1'] := BuscarDescripcionRetencion(DataDetalle.FieldValues['codigo_retencion_1']);
      Data.FieldValues['descripcion_retencion_2'] := BuscarDescripcionRetencion(DataDetalle.FieldValues['codigo_retencion_2']);
      Data.FieldValues['descripcion_retencion_3'] := BuscarDescripcionRetencion(DataDetalle.FieldValues['codigo_retencion_3']);
      Data.FieldValues['descripcion_retencion_4'] := BuscarDescripcionRetencion(DataDetalle.FieldValues['codigo_retencion_4']);
      Data.FieldValues['descripcion_retencion_5'] := BuscarDescripcionRetencion(DataDetalle.FieldValues['codigo_retencion_5']);
      Data.Post;
      DataDetalle.Next;
    end;
    DS.DataSet := Data;
  end;
end;

procedure TFrmLiquidacionCobranza.BtnBuscarBancoCobranzaClick(Sender: TObject);
begin
  Buscador(Self,BancoCobranza,'codigo','Buscar Banco de Cobranza',nil,DataBancoCobranza);
  CmbCodigoBancoCobranza.KeyValue := DataBancoCobranza.FieldValues['codigo'];
end;

procedure TFrmLiquidacionCobranza.BtnBuscarClienteClick(Sender: TObject);
begin
  Buscador(Self,Cliente,'codigo','Buscar Cliente',nil,DataCliente);
  CmbCodigoCliente.KeyValue := DataCliente.FieldValues['codigo'];
  CmbCodigoClienteExit(Sender);
end;

procedure TFrmLiquidacionCobranza.BtnBuscarCuentaBancariaClick(Sender: TObject);
begin
  Buscador(Self,CuentaBancaria,'codigo','Buscar Cuenta Bancaria',nil,DataCuentaBancaria);
  CmbCodigoCuentaBancaria.KeyValue := DataCuentaBancaria.FieldValues['codigo'];
end;

procedure TFrmLiquidacionCobranza.BtnBuscarFormaPagoClick(Sender: TObject);
begin
  Buscador(Self,FormaPago,'codigo','Buscar Forma de Pago',nil,DataFormaPago);
  CmbCodigoFormaPago.KeyValue := DataFormaPago.FieldValues['codigo'];
end;

procedure TFrmLiquidacionCobranza.BtnBuscarMonedaClick(Sender: TObject);
begin
  Buscador(Self,Moneda,'codigo','Buscar Moneda',nil,DataMoneda);
  CmbCodigoMoneda.KeyValue := DataMoneda.FieldValues['codigo'];
end;

procedure TFrmLiquidacionCobranza.BtnBuscarZonaClick(Sender: TObject);
begin
  Buscador(Self,Zona,'codigo','Buscar Zona',nil,DataZona);
  CmbCodigoZona.KeyValue := DataZona.FieldValues['codigo'];
end;

procedure TFrmLiquidacionCobranza.Buscar;
begin
  if (Buscador(Self,BuscadorLiquidacionCobranza,'numero','Buscar Liquidaci�n',nil,DataBuscadorLiquidacionCobranza)) then
  begin
    Clase.SetNumero(DataBuscadorLiquidacionCobranza.FieldValues['numero']);
    Clase.BuscarNumero;
    Asignar;
  end;
end;

procedure TFrmLiquidacionCobranza.CalcularMontos;
var
  Neto: Currency;
  TotalRetencion: Currency;
begin
  Neto := 0;
  TotalRetencion := 0;
  with Data do
  begin
    DisableControls;
    First;
    while (not Eof) do
    begin
      if ((FieldValues['desde'] = 'AC') or (FieldValues['desde'] = 'NCC')) then
      begin
        Neto := Neto + (FieldValues['abonado'] * -1);
      end
      else
      begin
        Neto := Neto + FieldValues['abonado'];
      end;
      TotalRetencion := TotalRetencion + (FieldValues['monto_retencion_iva'] +
                                          FieldValues['monto_retencion_1'] +
                                          FieldValues['monto_retencion_2'] +
                                          FieldValues['monto_retencion_3'] +
                                          FieldValues['monto_retencion_4'] +
                                          FieldValues['monto_retencion_5']);
      Next;
    end;
    EnableControls;
  end;
  //Asignando Montos
  TxtMontoNeto.Value := Neto;
  TxtTotal.Value := Neto - TotalRetencion;
  TxtTotalRetencion.Value := TotalRetencion;
end;

procedure TFrmLiquidacionCobranza.Cancelar;
begin
  if (Agregando) then
  begin
    Limpiar;
  end
  else
  begin
    Clase.SetNumero(TxtNumero.Text);
    Clase.BuscarNumero;
    Asignar;
  end;
end;

procedure TFrmLiquidacionCobranza.CmbCodigoClienteChange(Sender: TObject);
begin
  SB.Panels[0].Text := DataCliente.FieldValues['razon_social'];
end;

procedure TFrmLiquidacionCobranza.CmbCodigoClienteExit(Sender: TObject);
begin
  if (Length(Trim(CmbCodigoCliente.Text)) > 0) then
  begin
    if (Agregando) then
    begin
      Cliente.SetCodigo(CmbCodigoCliente.Value);
      Cliente.BuscarCodigo;
      CmbCodigoZona.Value := Cliente.GetCodigoZona;
    end;
  end;
end;

procedure TFrmLiquidacionCobranza.DataAfterPost(DataSet: TDataSet);
begin
  CalcularMontos;
end;

procedure TFrmLiquidacionCobranza.Detalle;
begin
  inherited;
  FrmAgregarDetalleLiquidacionCobranza := TFrmAgregarDetalleLiquidacionCobranza.CrearDetalle(Self,Data);
  FrmAgregarDetalleLiquidacionCobranza.CodigoCliente := CmbCodigoCliente.Text;
  FrmAgregarDetalleLiquidacionCobranza.ShowModal;
  FreeAndNil(FrmAgregarDetalleLiquidacionCobranza);
end;

procedure TFrmLiquidacionCobranza.Editar;
begin
  if (Length(Trim(TxtNumero.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end
  else
  begin
    MessageBox(Self.Handle, 'Las Liquidaciones de Cobranzas No Pueden Ser Editadas!!!', PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_APPLMODAL);
    Abort;
  end;
end;

procedure TFrmLiquidacionCobranza.Eliminar;
var
  MontoNeto: Double;
  sw: Boolean;
begin
  //Verificando Que La Liquidacion de Cobranza Pueda Ser Eliminada!!!
  //Verificando Orden de Cobranza por cada Item
  sw := False;
  with Data do
  begin
    First;
    while (not Eof) do
    begin
      CobranzaCobrada.SetIdCobranza(FieldValues['id_cobranza']);
      CobranzaCobrada.SetNumeroCuota(FieldValues['numero_cuota']);
      CobranzaCobrada.SetOrdenCobranza(FieldValues['orden_cobranza']);
      if (CobranzaCobrada.VerificarOrdenCobranza) then
      begin
        //Existe Una Cobranza Mas Reciente!!!
        sw := True;
        Break;
      end;
      Next;
    end;    
  end;
  //Consultando Si Puede Eliminarse La Liquidacion de Cobranza
  if (not sw) then
  begin
    //////////////////////////////////
    // Reversando Items de Cobranza //
    //////////////////////////////////
    with Data do
    begin
      First;
      while (not Eof) do
      begin
        //Ubicando CobranzaCobrada
        CobranzaCobrada.SetIdCobranza(FieldValues['id_cobranza']);
        CobranzaCobrada.SetNumeroCuota(FieldValues['numero_cuota']);
        CobranzaCobrada.SetOrdenCobranza(FieldValues['orden_cobranza']);
        CobranzaCobrada.Buscar;
        //Verificando Si Existe Una Cuenta x Cobrar Pendiente
        CuentaxCobrar.SetDesde(FieldValues['desde']);
        CuentaxCobrar.SetNumeroDocumento(FieldValues['numero_documento']);
        CuentaxCobrar.SetNumeroCuota(FieldValues['numero_cuota']);
        if (CuentaxCobrar.BuscarCobranza) then
        begin
          //Eliminado CuentaxCobrar Pendiente
          CuentaxCobrar.Eliminar;
          //Moviendo CobranzaCobrada a CuentaxCobrar (Solo Lo Necesario)
          CuentaxCobrar.SetOrdenCobranza(CobranzaCobrada.GetOrdenCobranza);
          CuentaxCobrar.SetTotal(CuentaxCobrar.GetTotal + CobranzaCobrada.GetTotal);
          //Calculando Nuevo Monto Neto e Impuesto
          MontoNeto := CuentaxCobrar.GetTotal / (1 + (CuentaxCobrar.GetPIva / 100));
          MontoNeto := Trunc(MontoNeto * Power(10,4)) / Power(10,4);
          CuentaxCobrar.SetMontoNeto(MontoNeto);
          CuentaxCobrar.SetImpuesto(CuentaxCobrar.GetTotal - MontoNeto);
          CuentaxCobrar.Insertar;
          //Borrando CobranzaCobrada
          CobranzaCobrada.Eliminar;
        end
        else
        begin
          //Moviendo CobranzaCobrada a CuentaxCobrar
          //Moviendo Entre Clases
          CuentaxCobrar.SetDesde(CobranzaCobrada.GetDesde);
          CuentaxCobrar.SetNumeroDocumento(CobranzaCobrada.GetNumeroDocumento);
          CuentaxCobrar.SetCodigoMoneda(CobranzaCobrada.GetCodigoMoneda);
          CuentaxCobrar.SetCodigoCliente(CobranzaCobrada.GetCodigoCliente);
          CuentaxCobrar.SetCodigoVendedor(CobranzaCobrada.GetCodigoVendedor);
          CuentaxCobrar.SetCodigoZona(CobranzaCobrada.GetCodigoZona);
          CuentaxCobrar.SetFechaIngreso(CobranzaCobrada.GetFechaIngreso);
          CuentaxCobrar.SetFechaRecepcion(CobranzaCobrada.GetFechaRecepcion);
          CuentaxCobrar.SetFechaVencIngreso(CobranzaCobrada.GetFechaVencIngreso);
          CuentaxCobrar.SetFechaVencRecepcion(CobranzaCobrada.GetFechaVencRecepcion);
          CuentaxCobrar.SetFechaLibros(CobranzaCobrada.GetFechaLibros);
          CuentaxCobrar.SetFechaContable(CobranzaCobrada.GetFechaContable);
          CuentaxCobrar.SetFechaCobranza(CobranzaCobrada.GetFechaCobranza);
          CuentaxCobrar.SetNumeroCuota(CobranzaCobrada.GetNumeroCuota);
          CuentaxCobrar.SetTotalCuotas(CobranzaCobrada.GetTotalCuotas);
          CuentaxCobrar.SetOrdenCobranza(CobranzaCobrada.GetOrdenCobranza);
          CuentaxCobrar.SetMontoNeto(CobranzaCobrada.GetMontoNeto);
          CuentaxCobrar.SetImpuesto(CobranzaCobrada.GetImpuesto);
          CuentaxCobrar.SetTotal(CobranzaCobrada.GetTotal);
          CuentaxCobrar.SetPIva(CobranzaCobrada.GetPIva);
          CuentaxCobrar.SetIdCobranza(CobranzaCobrada.GetIdCobranza);
          CuentaxCobrar.SetIdContadoCobranza(CobranzaCobrada.GetIdContadoCobranza);
          CuentaxCobrar.Insertar;
          //Borrando CobranzaCobrada
          CobranzaCobrada.Eliminar;
        end;
        Next;
      end;
    end;
    ///////////////////////////////
    // Procesando Eliminacion!!! //
    ///////////////////////////////
    //Verificando Si Se Elimina La Operacion Bancaria
    if (Clase.GetIdOperacionBancaria <> -1) then
    begin
      OperacionBancaria.SetIdOperacionBancaria(Clase.GetIdOperacionBancaria);
      OperacionBancaria.BuscarIdOperacionBancaria;
      OperacionBancaria.Eliminar;
    end;    
    //Se Busca La Liquidacion de Cobranza
    Clase.SetNumero(TxtNumero.Text);
    Clase.BuscarNumero;
    //Se Eliminan Los Detalles
    DetLiquidacionCobranza.EliminarIdLiquidacionCobranza(Clase.GetIdLiquidacionCobranza);
    Clase.Eliminar;
    MessageBox(Self.Handle, 'La Liquidaci�n de Cobranza Se Ha Eliminado Con Exito!!!',PChar(MsgTituloInformacion), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Refrescar;
    Limpiar;
  end
  else
  begin
    MessageBox(Self.Handle, 'La Liquidaci�n de Cobranza No Puede Ser Eliminada!!!'+#13+#10+''+#13+#10+'Para Poder Eliminar Una Liquidaci�n de Cobranza Se Debe Hacer Desde La Ultima Liquidaci�n Generada Asociada A Los Documentos Presentados En Esta Liquidaci�n!', PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_APPLMODAL);
    Abort;
  end;
end;

procedure TFrmLiquidacionCobranza.FormCreate(Sender: TObject);
begin
  inherited;
  ProcAgregar := Agregar;
  ProcEditar := Editar;
  ProcEliminar := Eliminar;
  ProcGuardar := Guardar;
  ProcCancelar := Cancelar;
  ProcRefrescar := Refrescar;
  ProcImprimir := Imprimir;
  ProcBuscar := Buscar;
  FrmActividadSophie.Show;
  FrmActividadSophie.Actividad('Cargando N�cleo...');
  Clase := TLiquidacionCobranza.Create;
  //Inicializando DataSets Secundarios
  BuscadorLiquidacionCobranza := TBusquedaLiquidacionCobranza.Create;
  Cliente := TCliente.Create;
  Zona := TZona.Create;
  Moneda := TMoneda.Create;
  FormaPago := TFormaPago.Create;
  OperacionBancaria := TOperacionBancaria.Create;
  CuentaBancaria := TCuentaBancaria.Create;
  BancoCobranza := TBancoCobranza.Create;
  AnticipoCliente := TAnticipoCliente.Create;
  FacturaCliente := TFacturacionCliente.Create;
  NotaDebitoCliente := TNotaDebitoCliente.Create;
  NotaCreditoCliente := TNotaCreditoCliente.Create;
  TipoRetencion := TTipoRetencion.Create;
  CuentaxCobrar := TCuentaxCobrar.Create;
  CobranzaCobrada := TCobranzaCobrada.Create;
  Reporte := TReporte.Create;
  Parametro := TParametro.Create;
  DetLiquidacionCobranza := TDetLiquidacionCobranza.Create;
  FrmActividadSophie.Actividad('Cargando Clientes...');
  DataCliente := Cliente.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Zonas...');
  DataZona := Zona.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Monedas...');
  DataMoneda := Moneda.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Formas de Pagos...');
  DataFormaPago := FormaPago.ObtenerComboCobranza;
  FrmActividadSophie.Actividad('Cargando Cuentas Bancarias...');
  DataCuentaBancaria := CuentaBancaria.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Bancos de Cobranzas...');
  DataBancoCobranza := BancoCobranza.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Resto del N�cleo...');
  DataBuscadorLiquidacionCobranza := BuscadorLiquidacionCobranza.ObtenerLista;
  DataCliente.First;
  DataZona.First;
  DataMoneda.First;
  DataFormaPago.First;
  DataCuentaBancaria.First;
  DataBancoCobranza.First;
  DataBuscadorLiquidacionCobranza.First;
  DSCliente.DataSet := DataCliente;
  DSZona.DataSet := DataZona;
  DSMoneda.DataSet := DataMoneda;
  DSFormaPago.DataSet := DataFormaPago;
  DSCuentaBancaria.DataSet := DataCuentaBancaria;
  DSBancoCobranza.DataSet := DataBancoCobranza;
  ConfigurarRxDBLookupCombo(CmbCodigoCliente,DSCliente,'codigo','codigo;razon_social');
  ConfigurarRxDBLookupCombo(CmbCodigoZona,DSZona,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoMoneda,DSMoneda,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoFormaPago,DSFormaPago,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoCuentaBancaria,DSCuentaBancaria,'codigo','codigo;entidad;numero');
  ConfigurarRxDBLookupCombo(CmbCodigoBancoCobranza,DSBancoCobranza,'codigo','codigo;descripcion');
  DS.DataSet := Data;
  GridDetalle.DataSource := DS;
  FrmActividadSophie.Close;
  //Buscar;
end;

procedure TFrmLiquidacionCobranza.GridDetalleDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if ((Data.FieldByName('desde').AsString = 'AC') or (Data.FieldByName('desde').AsString = 'NCC')) then
  begin
    GridDetalle.Canvas.Font.Color := clRed;
  end
  else
  begin
    GridDetalle.Canvas.Font.Color := clBlue;
  end;
  GridDetalle.DefaultDrawColumnCell(Rect,DataCol,Column,State);
end;

procedure TFrmLiquidacionCobranza.GridDetalleKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if (Agregando or Editando) then
  begin
    if (((Key = VK_INSERT) or (Key = VK_DELETE)) and (Length(Trim(CmbCodigoCliente.Text)) = 0)) then
    begin
      MessageBox(Self.Handle, 'Seleccione Primero un Cliente!!!', PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      CmbCodigoCliente.SetFocus;
      CmbCodigoCliente.DropDown;
      Abort;
    end;
  end;
  //Llamando a Evento Padre
  inherited;
end;

procedure TFrmLiquidacionCobranza.Guardar;
begin
  CalcularMontos;
  Obtener;
  if (Agregando) then
  begin
    //Agregando...
    //Guardando Encabezado
    Clase.Insertar;
    TxtNumero.Text := Clase.GetNumero;
    Application.ProcessMessages;
    //Guardando Detalles
    GuardarDetalles;
    //Guardando Operaciones Bancarias
    GuardarOperacionesBancarias;
    MessageBox(Self.Handle, 'La Liquidaci�n de Cobranza Ha Sido Agregada Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    //Se Imprime La Liquidacion
    MessageBox(Self.Handle, 'Presione "Aceptar" Para Proceder A Imprimir La Liquidaci�n!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    with Reporte do
    begin
      AgregarParametro('Numero',Clase.GetNumero);
      AgregarParametro('MontoLetra',NumeroALetras(Clase.GetTotal));
      EmiteReporte('LiquidacionCobranza',trGeneral);
    end;
  end
  else
  begin
    //Editando...
    Clase.Modificar;
    //Actualizando Detalles (Eliminando Anteriores y Cargando los Nuevos)
    DetLiquidacionCobranza.EliminarIdLiquidacionCobranza(Clase.GetIdLiquidacionCobranza);
    //Guardando Detalles
    GuardarDetalles;
    //Guardando Operaciones Bancarias
    GuardarOperacionesBancarias;
    MessageBox(Self.Handle, 'La Liquidaci�n de Cobranza Ha Sido Editada Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end;
  //Actualizando Dataset de Busqueda
  DataBuscadorLiquidacionCobranza := BuscadorLiquidacionCobranza.ObtenerLista;
end;

procedure TFrmLiquidacionCobranza.GuardarDetalles;
var
  Total: Double;
begin
  with Data do
  begin
    DisableControls;
    First;
    while (not Eof) do
    begin
      //Guardando Detalle de la Cobranza
      with DetLiquidacionCobranza do
      begin
        SetIdLiquidacionCobranza(Clase.GetIdLiquidacionCobranza);
        SetIdCobranza(FieldValues['id_cobranza']);
        SetNumeroCuota(FieldValues['numero_cuota']);
        SetOrdenCobranza(FieldValues['orden_cobranza']);
        SetAbonado(FieldValues['abonado']);
        SetCodigoRetencionIva(FieldValues['codigo_retencion_iva']);
        SetMontoRetencionIva(FieldValues['monto_retencion_iva']);
        SetCodigoRetencion1(FieldValues['codigo_retencion_1']);
        SetMontoRetencion1(FieldValues['monto_retencion_1']);
        SetCodigoRetencion2(FieldValues['codigo_retencion_2']);
        SetMontoRetencion2(FieldValues['monto_retencion_2']);
        SetCodigoRetencion3(FieldValues['codigo_retencion_3']);
        SetMontoRetencion3(FieldValues['monto_retencion_3']);
        SetCodigoRetencion4(FieldValues['codigo_retencion_4']);
        SetMontoRetencion4(FieldValues['monto_retencion_4']);
        SetCodigoRetencion5(FieldValues['codigo_retencion_5']);
        SetMontoRetencion5(FieldValues['monto_retencion_5']);
        Insertar;
      end;
      //Actualizando Las Cobranzas (CuentasxCobrar y CobranzaCobrada)
      //Se Busca el Renglon de la Cuenta por Cobrar
      CuentaxCobrar.SetDesde(FieldValues['desde']);
      CuentaxCobrar.SetNumeroDocumento(FieldValues['numero_documento']);
      CuentaxCobrar.SetNumeroCuota(FieldValues['numero_cuota']);
      CuentaxCobrar.BuscarCobranza;
      if (SameValue(FieldValues['abonado'],CuentaxCobrar.GetTotal)) then
      begin //Caso de Abonar Completo
        //Se Mueve la Cuenta x Cobrar directamente a la Cobranza Cobrada
        CobranzaCobrada.SetDesde(CuentaxCobrar.GetDesde);
        CobranzaCobrada.SetNumeroDocumento(CuentaxCobrar.GetNumeroDocumento);
        CobranzaCobrada.SetCodigoMoneda(CuentaxCobrar.GetCodigoMoneda);
        CobranzaCobrada.SetCodigoCliente(CuentaxCobrar.GetCodigoCliente);
        CobranzaCobrada.SetCodigoVendedor(CuentaxCobrar.GetCodigoVendedor);
        CobranzaCobrada.SetCodigoZona(CuentaxCobrar.GetCodigoZona);
        CobranzaCobrada.SetFechaIngreso(CuentaxCobrar.GetFechaIngreso);
        CobranzaCobrada.SetFechaRecepcion(CuentaxCobrar.GetFechaRecepcion);
        CobranzaCobrada.SetFechaVencIngreso(CuentaxCobrar.GetFechaVencIngreso);
        CobranzaCobrada.SetFechaVencRecepcion(CuentaxCobrar.GetFechaVencRecepcion);
        CobranzaCobrada.SetFechaLibros(CuentaxCobrar.GetFechaLibros);
        CobranzaCobrada.SetFechaContable(CuentaxCobrar.GetFechaContable);
        CobranzaCobrada.SetFechaCobranza(Date);
        CobranzaCobrada.SetNumeroCuota(CuentaxCobrar.GetNumeroCuota);
        CobranzaCobrada.SetTotalCuotas(CuentaxCobrar.GetTotalCuotas);
        CobranzaCobrada.SetOrdenCobranza(CuentaxCobrar.GetOrdenCobranza);
        CobranzaCobrada.SetMontoNeto(CuentaxCobrar.GetMontoNeto);
        CobranzaCobrada.SetImpuesto(CuentaxCobrar.GetImpuesto);
        CobranzaCobrada.SetTotal(CuentaxCobrar.GetTotal);
        CobranzaCobrada.SetPIva(CuentaxCobrar.GetPIva);
        CobranzaCobrada.SetIdCobranza(CuentaxCobrar.GetIdCobranza);
        CobranzaCobrada.SetIdContadoCobranza(CuentaxCobrar.GetIdContadoCobranza);
        CobranzaCobrada.Insertar;
        //Se Elimina La Cuenta x Cobrar
        CuentaxCobrar.Eliminar;
      end
      else
      begin //Caso de Abonar Incompleto
        //Se Registra La Cuenta x Cobrar en Cobranza Cobrada Segun Abono
        CobranzaCobrada.SetDesde(CuentaxCobrar.GetDesde);
        CobranzaCobrada.SetNumeroDocumento(CuentaxCobrar.GetNumeroDocumento);
        CobranzaCobrada.SetCodigoMoneda(CuentaxCobrar.GetCodigoMoneda);
        CobranzaCobrada.SetCodigoCliente(CuentaxCobrar.GetCodigoCliente);
        CobranzaCobrada.SetCodigoVendedor(CuentaxCobrar.GetCodigoVendedor);
        CobranzaCobrada.SetCodigoZona(CuentaxCobrar.GetCodigoZona);
        CobranzaCobrada.SetFechaIngreso(CuentaxCobrar.GetFechaIngreso);
        CobranzaCobrada.SetFechaRecepcion(CuentaxCobrar.GetFechaRecepcion);
        CobranzaCobrada.SetFechaVencIngreso(CuentaxCobrar.GetFechaVencIngreso);
        CobranzaCobrada.SetFechaVencRecepcion(CuentaxCobrar.GetFechaVencRecepcion);
        CobranzaCobrada.SetFechaLibros(CuentaxCobrar.GetFechaLibros);
        CobranzaCobrada.SetFechaContable(CuentaxCobrar.GetFechaContable);
        CobranzaCobrada.SetFechaCobranza(Date);
        CobranzaCobrada.SetNumeroCuota(CuentaxCobrar.GetNumeroCuota);
        CobranzaCobrada.SetTotalCuotas(CuentaxCobrar.GetTotalCuotas);
        CobranzaCobrada.SetOrdenCobranza(CuentaxCobrar.GetOrdenCobranza);
        CobranzaCobrada.SetMontoNeto(FieldValues['abonado'] / (1 + (CuentaxCobrar.GetPIva / 100)));
        CobranzaCobrada.SetImpuesto(FieldValues['abonado'] - CobranzaCobrada.GetMontoNeto);
        CobranzaCobrada.SetTotal(FieldValues['abonado']);
        CobranzaCobrada.SetPIva(CuentaxCobrar.GetPIva);
        CobranzaCobrada.SetIdCobranza(CuentaxCobrar.GetIdCobranza);
        CobranzaCobrada.SetIdContadoCobranza(CuentaxCobrar.GetIdContadoCobranza);
        CobranzaCobrada.Insertar;
        //Se Elimina La Cuenta x Cobrar
        CuentaxCobrar.Eliminar;
        //Se Registra La Cuenta x Cobrar Pendiente Segun Abono
        //Se Actualiza El Orden de la Cobranza y Los Montos
        Total := CuentaxCobrar.GetTotal;
        CuentaxCobrar.SetOrdenCobranza(CuentaxCobrar.GetOrdenCobranza + 1);
        CuentaxCobrar.SetMontoNeto((Total - FieldValues['abonado']) / (1 + (CuentaxCobrar.GetPIva / 100)));
        CuentaxCobrar.SetImpuesto((Total - FieldValues['abonado']) - CuentaxCobrar.GetMontoNeto);
        CuentaxCobrar.SetTotal(Total - FieldValues['abonado']);
        CuentaxCobrar.Insertar;
      end;
      Next;
    end;
    EnableControls;
  end;
end;

procedure TFrmLiquidacionCobranza.GuardarOperacionesBancarias;
begin
  if (DataFormaPago['operacion_bancaria']) then
  begin
    with OperacionBancaria do
    begin
      SetCodigoCuenta(CmbCodigoCuentaBancaria.Text);
      SetCodigoTipoOperacion(DataFormaPago['codigo_tipo_operacion_bancaria']);
      SetNumero(TxtNumeroDocumentoBancario.Text);
      SetFecha(DatFechaBancaria.Date);
      SetFechaContable(DatFechaBancaria.Date);
      SetMonto(Clase.GetMontoNeto);
      SetConcepto('Liquidaci�n de Cobranza Generada N� ' + Clase.GetNumero);
      SetObservaciones('');
      SetDiferido(False);
      SetConciliado(False);
      SetFechaConciliacion(Date);
      SetBeneficiario('');
      SetLoginUsuario(P.User.GetLoginUsuario);
      Insertar;
    end;
    //Actualizando IdOperacionBancaria en la Liquidacion
    with Clase do
    begin
      SetIdOperacionBancaria(OperacionBancaria.GetIdOperacionBancaria);
      Modificar;
    end;
  end;
end;

procedure TFrmLiquidacionCobranza.Imprimir;
begin
  if (Length(Trim(TxtNumero.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
  with Reporte do
  begin
    AgregarParametro('Numero',Clase.GetNumero);
    AgregarParametro('MontoLetra',NumeroALetras(Clase.GetTotal));
    EmiteReporte('LiquidacionCobranza',trGeneral);
  end;
end;

procedure TFrmLiquidacionCobranza.Limpiar;
begin
  TxtNumero.Text := '';
  CmbCodigoFormaPago.KeyValue := '';
  TxtNumeroFormaPago.Text := '';
  CmbCodigoCuentaBancaria.KeyValue := '';
  TxtNumeroDocumentoBancario.Text := '';
  CmbCodigoBancoCobranza.KeyValue := '';
  CmbCodigoCliente.KeyValue := '';
  CmbCodigoZona.KeyValue := '';
  DatFechaIngreso.Date := Date;
  DatFechaContable.Date := Date;
  DatFechaBancaria.Date := Date;
  CmbCodigoMoneda.KeyValue := '';
  TxtMontoNeto.Value := 0;
  TxtTotalRetencion.Value := 0;
  TxtTotal.Value := 0;
  TxtObservaciones.Text := '';
  //Status Bar
  SB.Panels[0].Text := '';
  Data.EmptyDataSet;
end;

procedure TFrmLiquidacionCobranza.Mover;
begin

end;

procedure TFrmLiquidacionCobranza.Obtener;
begin
  with Clase do
  begin
    SetNumero(TxtNumero.Text);
    SetCodigoMoneda(CmbCodigoMoneda.Text);
    SetCodigoCliente(CmbCodigoCliente.Text);
    SetCodigoZona(CmbCodigoZona.Text);
    SetFechaIngreso(DatFechaIngreso.Date);
    SetFechaContable(DatFechaContable.Date);
    SetObservaciones(TxtObservaciones.Text);
    SetMontoNeto(TxtMontoNeto.Value);
    SetTotalRetencion(TxtTotalRetencion.Value);
    SetTotal(TxtTotal.Value);
    SetCodigoFormaPago(CmbCodigoFormaPago.Text);
    SetNumeroFormaPago(TxtNumeroFormaPago.Text);
    SetCodigoCuentaBancaria(CmbCodigoCuentaBancaria.Text);
    SetNumeroDocumentoBancario(TxtNumeroDocumentoBancario.Text);
    SetCodigoBancoCobranza(CmbCodigoBancoCobranza.Text);
    SetFechaOperacionBancaria(DatFechaBancaria.Date);
    SetIdLiquidacionCobranza(-1);
    SetIdOperacionBancaria(-1);
  end;
end;

procedure TFrmLiquidacionCobranza.Refrescar;
begin
  //Actualizando Dataset de Busqueda
  DataBuscadorLiquidacionCobranza := BuscadorLiquidacionCobranza.ObtenerLista;
  //Se Actualizan Los demas Componentes
  DataCliente := Cliente.ObtenerCombo;
  DataZona := Zona.ObtenerCombo;
  DataMoneda := Moneda.ObtenerCombo;
  DataFormaPago := FormaPago.ObtenerComboCobranza;
  DataCuentaBancaria := CuentaBancaria.ObtenerCombo;
  DataBancoCobranza := BancoCobranza.ObtenerCombo;
  DataCliente.First;
  DataZona.First;
  DataMoneda.First;
  DataFormaPago.First;
  DataCuentaBancaria.First;
  DataBancoCobranza.First;
  DataBuscadorLiquidacionCobranza.First;
  DSCliente.DataSet := DataCliente;
  DSZona.DataSet := DataZona;
  DSMoneda.DataSet := DataMoneda;
  DSFormaPago.DataSet := DataFormaPago;
  DSCuentaBancaria.DataSet := DataCuentaBancaria;
  DSBancoCobranza.DataSet := DataBancoCobranza;
end;

procedure TFrmLiquidacionCobranza.TBtnEliminarClick(Sender: TObject);
begin
  if (Length(Trim(TxtNumero.Text)) > 0) then
  begin
    inherited;
  end
  else
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
end;

function TFrmLiquidacionCobranza.Validar: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  //Validando Cliente
  if (Length(Trim(CmbCodigoCliente.Text)) = 0) then
  begin
    ListaErrores.Add('El Cliente No Puede Estar En Blanco!!!');
  end;
  //Validando Zona
  if (Length(Trim(CmbCodigoZona.Text)) = 0) then
  begin
    ListaErrores.Add('La Zona No Puede Estar En Blanco!!!');
  end;
  //Validando Moneda
  if (Length(Trim(CmbCodigoMoneda.Text)) = 0) then
  begin
    ListaErrores.Add('La Moneda No Puede Estar En Blanco!!!');
  end;
  //Validando Forma de Pago
  if (Length(Trim(CmbCodigoFormaPago.Text)) = 0) then
  begin
    ListaErrores.Add('La Forma de Pago No Puede Estar En Blanco!!!');
  end
  else
  begin
    //Validando Nota de Debito
    if ((not DataFormaPago.FieldValues['operacion_bancaria']) AND ((DataFormaPago.FieldValues['nd']) OR (DataFormaPago.FieldValues['nc']) OR (DataFormaPago.FieldValues['anticipo']) OR (DataFormaPago.FieldValues['cheque']))) then
    begin
      if (Length(Trim(TxtNumeroFormaPago.Text)) = 0) then
      begin
        ListaErrores.Add('Debe Definir El Numero del Documento Asociado a la Forma de Pago!!!');
      end;
    end;
    //Validando Datos de Operaciones Bancarias
    if (DataFormaPago.FieldValues['operacion_bancaria']) then
    begin
      //Validando Cuenta Bancaria
      if (Length(Trim(CmbCodigoCuentaBancaria.Text)) = 0) then
      begin
        ListaErrores.Add('La Cuenta Bancaria No Puede Estar En Blanco!!!');
      end;
      if (Length(Trim(TxtNumeroDocumentoBancario.Text)) = 0) then
      begin
        ListaErrores.Add('El Numero de la Operacion Bancaria No Puede Estar En Blanco!!!');
      end;
    end;
    //Validando El Banco de Cobranza
    if (DataFormaPago.FieldValues['codigo_banco']) then
    begin
      if (Length(Trim(CmbCodigoBancoCobranza.Text)) = 0) then
      begin
        ListaErrores.Add('Debe Definir un Banco Para la Cobranza!!!');
      end;
    end;
  end;
  //Validando Monto
  if (TxtTotal.Value <= 0) then
  begin
    ListaErrores.Add('El Total Debe Ser Positivo!!!');
  end;
  //Validando Fechas
  if (DatFechaIngreso.Date > DatFechaContable.Date) then
  begin
    ListaErrores.Add('La Fecha de Ingreso Debe Ser Menor o Igual a la Fecha Contable!!!');
  end;
  //Validando Detalle
  if (Data.RecordCount = 0) then
  begin
    ListaErrores.Add('El Detalle De La Liquidaci�n No Puede Estar Vacio!!!');
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
