unit ProductoExtendido;

{$I sophie.inc}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GenericoDetalle, DB, DBClient, ImgList, XPMan, ComCtrls, Grids,
  DBGrids, ExtCtrls, ToolWin, Mask, rxToolEdit, rxCurrEdit, StdCtrls, Buttons,
  RxLookup, DBCtrls, Utils, ClsProducto, ClsDivision, ClsLinea, ClsFamilia,
  ClsClase, ClsManufactura, ClsArancel, ClsUnidad, ClsProductoPrecio,
  ClsTipoPrecioProducto, GenericoBuscador, ClsReporte, ClsParametro
  {$IFDEF DELPHI10}, ImageList{$ENDIF};

type
  TFrmProductoExtendido = class(TFrmGenericoDetalle)
    LblCodigo: TLabel;
    LblReferencia: TLabel;
    LblDescripcion: TLabel;
    LblPrecio: TLabel;
    LblPDescuento: TLabel;
    TxtCodigo: TEdit;
    TxtReferencia: TEdit;
    TxtDescripcion: TEdit;
    TxtPrecio: TCurrencyEdit;
    TxtPDescuento: TCurrencyEdit;
    GrpIVA: TGroupBox;
    LblPIVAVenta: TLabel;
    LblPIVACompra: TLabel;
    TxtPIVAVenta: TCurrencyEdit;
    TxtPIVACompra: TCurrencyEdit;
    TabCaracteristicas: TTabSheet;
    LblDivision: TLabel;
    TxtDescripcionDivision: TDBText;
    LblLinea: TLabel;
    TxtDescripcionLinea: TDBText;
    LblFamilia: TLabel;
    TxtDescripcionFamilia: TDBText;
    LblManufactura: TLabel;
    TxtDescripcionClase: TDBText;
    LblClase: TLabel;
    TxtDescripcionManufactura: TDBText;
    LblCodigoArancel: TLabel;
    TxtDescripcionArancel: TDBText;
    LblUnidad: TLabel;
    TxtDescripcionUnidad: TDBText;
    CmbCodigoDivision: TRxDBLookupCombo;
    BtnBuscarDivision: TBitBtn;
    CmbCodigoLinea: TRxDBLookupCombo;
    BtnBuscarLinea: TBitBtn;
    CmbCodigoFamilia: TRxDBLookupCombo;
    BtnBuscarFamilia: TBitBtn;
    CmbCodigoClase: TRxDBLookupCombo;
    BtnBuscarClase: TBitBtn;
    CmbCodigoManufactura: TRxDBLookupCombo;
    BtnBuscarManufactura: TBitBtn;
    CmbCodigoArancel: TRxDBLookupCombo;
    BtnBuscarArancel: TBitBtn;
    CmbCodigoUnidad: TRxDBLookupCombo;
    BtnBuscarUnidad: TBitBtn;
    DSDivision: TDataSource;
    DSLinea: TDataSource;
    DSFamilia: TDataSource;
    DSClase: TDataSource;
    DSManufactura: TDataSource;
    DSArancel: TDataSource;
    DSUnidad: TDataSource;
    TabCostoOtro: TTabSheet;
    GrpCostos: TGroupBox;
    LblCostoAnterior: TLabel;
    LblCostoActual: TLabel;
    LblCostoFOB: TLabel;
    LblCostoAnteriorUS: TLabel;
    LblCostoActualUS: TLabel;
    TxtCostoAnterior: TCurrencyEdit;
    TxtCostoActual: TCurrencyEdit;
    TxtCostoFOB: TCurrencyEdit;
    TxtCostoAnteriorUS: TCurrencyEdit;
    TxtCostoActualUS: TCurrencyEdit;
    LblUbicacion: TLabel;
    TxtUbicacion: TEdit;
    TabProductoPrecio: TTabSheet;
    ChkInventario: TCheckBox;
    LblPeso: TLabel;
    TxtPeso: TCurrencyEdit;
    LblVolumen: TLabel;
    TxtVolumen: TCurrencyEdit;
    LblFechaIngreso: TLabel;
    TxtFechaIngreso: TLabel;
    LblFechaUltimaCompra: TLabel;
    TxtFechaUltimaCompra: TLabel;
    GridProductoPrecio: TDBGrid;
    DSProductoPrecio: TDataSource;
    DataProductoPrecio: TClientDataSet;
    DataProductoPreciocodigo_producto: TStringField;
    DataProductoPreciocodigo_tipo_precio_producto: TStringField;
    DataProductoPreciodescripcion: TStringField;
    DataProductoPrecioprecio: TFloatField;
    procedure FormCreate(Sender: TObject);
    procedure TBtnEditarClick(Sender: TObject);
    procedure BtnBuscarDivisionClick(Sender: TObject);
    procedure BtnBuscarLineaClick(Sender: TObject);
    procedure BtnBuscarFamiliaClick(Sender: TObject);
    procedure BtnBuscarClaseClick(Sender: TObject);
    procedure BtnBuscarManufacturaClick(Sender: TObject);
    procedure BtnBuscarArancelClick(Sender: TObject);
    procedure BtnBuscarUnidadClick(Sender: TObject);
    procedure TxtPrecioExit(Sender: TObject);
    procedure DataProductoPrecioprecioSetText(Sender: TField;
      const Text: string);
  private
    { Private declarations }
    Clase: TProducto;
    BuscadorProducto: TProducto;
    Division: TDivision;
    Linea: TLinea;
    Familia: TFamilia;
    ClaseClase: TClase;
    Manufactura: TManufactura;
    Arancel: TArancel;
    Unidad: TUnidad;
    ProductoPrecio: TProductoPrecio;
    TipoPrecioProducto: TTipoPrecioProducto;
    DataDivision: TClientDataSet;
    DataLinea: TClientDataSet;
    DataFamilia: TClientDataSet;
    DataClase: TClientDataSet;
    DataManufactura: TClientDataSet;
    DataArancel: TClientDataSet;
    DataUnidad: TClientDataSet;
    Reporte: TReporte;
    Parametro: TParametro;
    DataBuscadorProducto: TClientDataSet;
    DataTipoPrecioProducto: TClientDataSet;
    procedure Agregar;
    procedure Editar;
    procedure Eliminar;
    procedure Guardar;
    procedure Cancelar;
    procedure Refrescar;
    procedure Imprimir;
    procedure Buscar;
    procedure Limpiar;
    procedure Asignar;
    procedure Obtener;
    procedure Mover;
    procedure GuardarDetalles;
  protected
    { Protected declarations }
    function Validar: Boolean; override;
    procedure Detalle; override;
  public
    { Public declarations }
  end;

var
  FrmProductoExtendido: TFrmProductoExtendido;

implementation

uses
  ActividadSophie
  {$IFDEF USE_RAVE_REPORTS}
  , DMRR
  {$ENDIF}
  ;

{$R *.dfm}

procedure TFrmProductoExtendido.Agregar;
begin
  //Verificando
  Limpiar;
  //Asignando Valores por Defecto
  TxtPIVAVenta.Value := Parametro.ObtenerValor('p_iva');
  TxtPIVACompra.Value := Parametro.ObtenerValor('p_iva');
  TxtCodigo.SetFocus;
  //Agregando Items de Tipos de Precios
  DataTipoPrecioProducto := TipoPrecioProducto.ObtenerLista;
  DataProductoPrecio.EmptyDataSet;
  with DataTipoPrecioProducto do
  begin
    First;
    while (not Eof) do
    begin
      DataProductoPrecio.Append;
      DataProductoPrecio.FieldValues['codigo_tipo_precio_producto'] := FieldValues['codigo'];
      DataProductoPrecio.FieldValues['descripcion'] := FieldValues['descripcion'];
      DataProductoPrecio.FieldValues['precio'] := 0;
      DataProductoPrecio.Post;
      Next;
    end;
    DataProductoPrecio.First;    
  end;
end;

procedure TFrmProductoExtendido.Asignar;
begin
  with Clase do
  begin
    TxtCodigo.Text := GetCodigo;
    TxtReferencia.Text := GetReferencia;
    //TxtUniqueID.Caption := IntToStr(GetUniqueId);
    TxtDescripcion.Text := GetDescripcion;
    TxtPrecio.Value := GetPrecio;
    TxtPDescuento.Value := GetPDescuento;
    TxtPIVAVenta.Value := GetPIvaVenta;
    TxtPIVACompra.Value := GetPIvaCompra;
    CmbCodigoDivision.KeyValue := GetCodigoDivision;
    CmbCodigoLinea.KeyValue := GetCodigoLinea;
    CmbCodigoFamilia.KeyValue := GetCodigoFamilia;
    CmbCodigoClase.KeyValue := GetCodigoClase;
    CmbCodigoManufactura.KeyValue := GetCodigoManufactura;
    CmbCodigoArancel.KeyValue := GetCodigoArancel;
    CmbCodigoUnidad.KeyValue := GetCodigoUnidad;
    TxtUbicacion.Text := GetUbicacion;
    ChkInventario.Checked := GetInventario;
    TxtCostoAnterior.Value := GetCostoAnterior;
    TxtCostoActual.Value := GetCostoActual;
    TxtCostoFOB.Value := GetCostoFob;
    TxtCostoAnteriorUS.Value := GetUsCostoAnterior;
    TxtCostoActualUS.Value := GetUsCostoActual;
    TxtPeso.Value := GetPeso;
    TxtVolumen.Value := GetVolumen;
    TxtFechaIngreso.Caption := FormatDateTime('dd/mm/yyyy',GetFechaIngreso);
    TxtFechaUltimaCompra.Caption := FormatDateTime('dd/mm/yyyy', GetFechaUCompra);
    DataDivision.Locate('codigo',CmbCodigoDivision.KeyValue,[loCaseInsensitive]);
    DataLinea.Locate('codigo',CmbCodigoLinea.KeyValue,[loCaseInsensitive]);
    DataFamilia.Locate('codigo',CmbCodigoFamilia.KeyValue,[loCaseInsensitive]);
    DataClase.Locate('codigo',CmbCodigoClase.KeyValue,[loCaseInsensitive]);
    DataManufactura.Locate('codigo',CmbCodigoManufactura.KeyValue,[loCaseInsensitive]);
    DataArancel.Locate('codigo',CmbCodigoArancel.KeyValue,[loCaseInsensitive]);
    DataUnidad.Locate('codigo',CmbCodigoUnidad.KeyValue,[loCaseInsensitive]);
    //Cargando Detalles
    DataProductoPrecio.EmptyDataSet;
    DataProductoPrecio.Data := ProductoPrecio.ObtenerCodigoProducto(GetCodigo).Data;
    //DataProductoPrecio.CloneCursor(ProductoPrecio.ObtenerCodigoProducto(GetCodigo),False,True);
    DSProductoPrecio.DataSet := DataProductoPrecio;
    TabCaracteristicas.TabVisible := True;
    PageDetalles.ActivePage := TabCaracteristicas;
  end;
end;

procedure TFrmProductoExtendido.BtnBuscarArancelClick(Sender: TObject);
begin
  Buscador(Self,Arancel,'codigo','Busqueda de Aranceles',nil,DataArancel);
  CmbCodigoArancel.KeyValue := DataArancel.FieldValues['codigo'];
end;

procedure TFrmProductoExtendido.BtnBuscarClaseClick(Sender: TObject);
begin
  Buscador(Self,ClaseClase,'codigo','Busqueda de Clases',nil,DataClase);
  CmbCodigoClase.KeyValue := DataClase.FieldValues['codigo'];
end;

procedure TFrmProductoExtendido.BtnBuscarDivisionClick(Sender: TObject);
begin
  Buscador(Self,Division,'codigo','Busqueda de Divisiones',nil,DataDivision);
  CmbCodigoDivision.KeyValue := DataDivision.FieldValues['codigo'];
end;

procedure TFrmProductoExtendido.BtnBuscarFamiliaClick(Sender: TObject);
begin
  Buscador(Self,Familia,'codigo','Busqueda de Familias',nil,DataFamilia);
  CmbCodigoFamilia.KeyValue := DataFamilia.FieldValues['codigo'];
end;

procedure TFrmProductoExtendido.BtnBuscarLineaClick(Sender: TObject);
begin
  Buscador(Self,Linea,'codigo','Busqueda de Lineas',nil,DataLinea);
  CmbCodigoLinea.KeyValue := DataLinea.FieldValues['codigo'];
end;

procedure TFrmProductoExtendido.BtnBuscarManufacturaClick(Sender: TObject);
begin
  Buscador(Self,Manufactura,'codigo','Busqueda de Manufacturas',nil,DataManufactura);
  CmbCodigoManufactura.KeyValue := DataManufactura.FieldValues['codigo'];
end;

procedure TFrmProductoExtendido.BtnBuscarUnidadClick(Sender: TObject);
begin
  Buscador(Self,Unidad,'codigo','Busqueda de Unidades',nil,DataUnidad);
  CmbCodigoUnidad.KeyValue := DataUnidad.FieldValues['codigo'];
end;

procedure TFrmProductoExtendido.Buscar;
begin
  if (Buscador(Self,BuscadorProducto,'codigo','Buscar Producto',nil,DataBuscadorProducto)) then
  begin
    Clase.SetCodigo(DataBuscadorProducto.FieldValues['codigo']);
    Clase.BuscarCodigo;
    Asignar;
  end;
end;

procedure TFrmProductoExtendido.Cancelar;
begin
  if (Agregando) then
  begin
    Limpiar;
  end
  else
  begin
    Clase.SetCodigo(TxtCodigo.Text);
    Clase.BuscarCodigo;
    Asignar;
    TxtCodigo.Enabled := True;
  end;
end;

procedure TFrmProductoExtendido.DataProductoPrecioprecioSetText(Sender: TField;
  const Text: string);
begin
  inherited;
  Sender.AsFloat := StrToFloatDef(Text,0)
end;

procedure TFrmProductoExtendido.Detalle;
begin
  inherited;
  case PageDetalles.ActivePageIndex of
    0: //TabDetalle
      begin
      end;
    1: //TabCaracteristicas
      begin
      end;
    2: //TabCostoOtros
      begin
      end;
    3: //TabProductoPrecio
      begin
        {FrmAgregarDetalleClienteVehiculo := TFrmAgregarDetalleClienteVehiculo.CrearDetalle(Self,DataClienteVehiculo);
        FrmAgregarDetalleClienteVehiculo.ShowModal;
        FreeAndNil(FrmAgregarDetalleClienteVehiculo);}
      end;
  end;
end;

procedure TFrmProductoExtendido.Editar;
begin
  if (Length(Trim(TxtCodigo.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
  TxtCodigo.Enabled := False;
end;

procedure TFrmProductoExtendido.Eliminar;
begin
  //Se Busca el Elemento
  Clase.SetCodigo(TxtCodigo.Text);
  Clase.BuscarCodigo;
  Clase.Eliminar;
  //Eliminando Tipos de Precio del Producto
  ProductoPrecio.EliminarCodigoProducto(Clase.GetCodigo);
  MessageBox(Self.Handle, 'El Producto Se Ha Eliminado Con Exito!!!',PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  Refrescar;
  Limpiar;
end;

procedure TFrmProductoExtendido.FormCreate(Sender: TObject);
begin
  inherited;
  ProcAgregar := Agregar;
  ProcEditar := Editar;
  ProcEliminar := Eliminar;
  ProcGuardar := Guardar;
  ProcCancelar := Cancelar;
  ProcRefrescar := Refrescar;
  ProcImprimir := Imprimir;
  ProcBuscar := Buscar;
  FrmActividadSophie.Show;
  FrmActividadSophie.Actividad('Cargando N�cleo...');
  Clase := TProducto.Create;
  //Inicializando DataSets Secundarios
  BuscadorProducto := TProducto.Create;
  Division := TDivision.Create;
  Linea := TLinea.Create;
  Familia := TFamilia.Create;
  ClaseClase := TClase.Create;
  Manufactura := TManufactura.Create;
  Arancel := TArancel.Create;
  Unidad := TUnidad.Create;
  Reporte := TReporte.Create;
  Parametro := TParametro.Create;
  ProductoPrecio := TProductoPrecio.Create;
  TipoPrecioProducto := TTipoPrecioProducto.Create;
  DataBuscadorProducto := BuscadorProducto.ObtenerLista;
  DataDivision := Division.ObtenerLista;
  DataLinea := Linea.ObtenerLista;
  DataFamilia := Familia.ObtenerLista;
  DataClase := ClaseClase.ObtenerLista;
  DataManufactura := Manufactura.ObtenerLista;
  DataArancel := Arancel.ObtenerLista;
  DataUnidad := Unidad.ObtenerLista;
  DataDivision.First;
  DataLinea.First;
  DataFamilia.First;
  DataClase.First;
  DataManufactura.First;
  DataArancel.First;
  DataUnidad.First;
  DSDivision.DataSet := DataDivision;
  DSLinea.DataSet := DataLinea;
  DSFamilia.DataSet := DataFamilia;
  DSClase.DataSet := DataClase;
  DSManufactura.DataSet := DataManufactura;
  DSArancel.DataSet := DataArancel;
  DSUnidad.DataSet := DataUnidad;
  TxtDescripcionDivision.DataSource := DSDivision;
  TxtDescripcionDivision.DataField := 'descripcion';
  ConfigurarRxDBLookupCombo(CmbCodigoDivision,DSDivision,'codigo','codigo;descripcion');
  TxtDescripcionLinea.DataSource := DSLinea;
  TxtDescripcionLinea.DataField := 'descripcion';
  ConfigurarRxDBLookupCombo(CmbCodigoLinea,DSLinea,'codigo','codigo;descripcion');
  TxtDescripcionFamilia.DataSource := DSFamilia;
  TxtDescripcionFamilia.DataField := 'descripcion';
  ConfigurarRxDBLookupCombo(CmbCodigoFamilia,DSFamilia,'codigo','codigo;descripcion');
  TxtDescripcionClase.DataSource := DSClase;
  TxtDescripcionClase.DataField := 'descripcion';
  ConfigurarRxDBLookupCombo(CmbCodigoClase,DSClase,'codigo','codigo;descripcion');
  TxtDescripcionManufactura.DataSource := DSManufactura;
  TxtDescripcionManufactura.DataField := 'descripcion';
  ConfigurarRxDBLookupCombo(CmbCodigoManufactura,DSManufactura,'codigo','codigo;descripcion');
  TxtDescripcionArancel.DataSource := DSArancel;
  TxtDescripcionArancel.DataField := 'descripcion';
  ConfigurarRxDBLookupCombo(CmbCodigoArancel,DSArancel,'codigo','codigo;descripcion');
  TxtDescripcionUnidad.DataSource := DSUnidad;
  TxtDescripcionUnidad.DataField := 'descripcion';
  ConfigurarRxDBLookupCombo(CmbCodigoUnidad,DSUnidad,'codigo','codigo;descripcion');
  //Configurando Tabs
  TabDetalle.TabVisible := False;
  TabCaracteristicas.TabVisible := True;
  PageDetalles.ActivePage := TabCaracteristicas;
  DS.DataSet := Data;
  GridDetalle.DataSource := DS;
  DSProductoPrecio.DataSet := DataProductoPrecio;
  GridProductoPrecio.DataSource := DSProductoPrecio;
  Parametro.SetNombreParametro('usar_tipos_precios');
  if (not Parametro.Buscar) then
  begin
    TabProductoPrecio.TabVisible := False;
  end
  else
  begin
    if (not Parametro.ObtenerValorAsBoolean) then
    begin
      TabProductoPrecio.TabVisible := False;
    end;
  end;
  FrmActividadSophie.Close;
end;

procedure TFrmProductoExtendido.Guardar;
begin
  Obtener;
  if (Agregando) then
  begin
    //Agregando...
    //Se Busca el Codigo
    Clase.SetCodigo(TxtCodigo.Text);
    if (Clase.BuscarCodigo) then
    begin
      MessageBox(Self.Handle, 'El C�digo Ya Existe!!!', PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      TxtCodigo.SetFocus;
      Abort;
    end
    else
    begin
      Clase.Insertar;
      MessageBox(Self.Handle, 'El Producto Ha Sido Agregado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      //Guardando Detalles
      GuardarDetalles;
    end;
  end
  else
  begin
    //Editando...
    Clase.Modificar;
    //Guardando Detalles
    ProductoPrecio.EliminarCodigoProducto(Clase.GetCodigo);
    GuardarDetalles;
    MessageBox(Self.Handle, 'El Producto Ha Sido Editado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    TxtCodigo.Enabled := True;
  end;
  TabCaracteristicas.TabVisible := True;
  PageDetalles.ActivePage := TabCaracteristicas;
end;

procedure TFrmProductoExtendido.GuardarDetalles;
begin
  //Guardando Tipos de Precio
  with DataProductoPrecio do
  begin
    DisableControls;
    First;
    while (not Eof) do
    begin
      with ProductoPrecio do
      begin
        SetCodigoProducto(Clase.GetCodigo);
        SetCodigoTipoPrecioProducto(FieldValues['codigo_tipo_precio_producto']);
        SetDescripcion(FieldValues['descripcion']);
        SetPrecio(FieldValues['precio']);
        Insertar;
      end;
      Next;
    end;
    EnableControls;
    First;
  end;
end;

procedure TFrmProductoExtendido.Imprimir;
begin

end;

procedure TFrmProductoExtendido.Limpiar;
begin
  TxtCodigo.Text := '';
  TxtReferencia.Text := '';
  TxtDescripcion.Text := '';
  TxtPrecio.Text := '';
  TxtPDescuento.Text := '';
  TxtPIVAVenta.Text := '';
  TxtPIVACompra.Text := '';
  CmbCodigoDivision.KeyValue := '';
  CmbCodigoLinea.KeyValue := '';
  CmbCodigoFamilia.KeyValue := '';
  CmbCodigoClase.KeyValue := '';
  CmbCodigoManufactura.KeyValue := '';
  CmbCodigoArancel.KeyValue := '';
  CmbCodigoUnidad.KeyValue := '';
  TxtUbicacion.Text := '';
  TxtCostoAnterior.Text := '';
  TxtCostoActual.Text := '';
  TxtCostoFOB.Text := '';
  TxtCostoAnteriorUS.Text := '';
  TxtCostoActualUS.Text := '';
  TxtPeso.Text := '';
  TxtVolumen.Text := '';
  TxtFechaIngreso.Caption := '';
  TxtFechaUltimaCompra.Caption := '';
  ChkInventario.Checked := False;
  //Status Bar
  SB.Panels[0].Text := '';
//  Data.EmptyDataSet;
  DataProductoPrecio.EmptyDataSet;
  TabCaracteristicas.TabVisible := True;
  PageDetalles.ActivePage := TabCaracteristicas;
end;

procedure TFrmProductoExtendido.Mover;
begin

end;

procedure TFrmProductoExtendido.Obtener;
begin
  with Clase do
  begin
    SetCodigo(TxtCodigo.Text);
    SetReferencia(TxtReferencia.Text);
    SetDescripcion(TxtDescripcion.Text);
    SetCodigoDivision(CmbCodigoDivision.Text);
    SetCodigoLinea(CmbCodigoLinea.Text);
    SetCodigoFamilia(CmbCodigoFamilia.Text);
    SetCodigoClase(CmbCodigoClase.Text);
    SetCodigoManufactura(CmbCodigoManufactura.Text);
    SetCodigoArancel(CmbCodigoArancel.Text);
    SetCodigoUnidad(CmbCodigoUnidad.Text);
    //SetFechaIngreso();
    SetPeso(TxtPeso.Value);
    SetVolumen(TxtVolumen.Value);
    SetPIvaVenta(TxtPIVAVenta.Value);
    SetPIvaCompra(TxtPIVACompra.Value);
    SetPrecio(TxtPrecio.Value);
    SetPDescuento(TxtPDescuento.Value);
    SetCostoAnterior(TxtCostoAnterior.Value);
    SetCostoActual(TxtCostoActual.Value);
    //SetFechaUCompra();
    SetUsCostoAnterior(TxtCostoAnteriorUS.Value);
    SetUsCostoActual(TxtCostoActualUS.Value);
    SetCostoFob(TxtCostoFOB.Value);
    SetUbicacion(TxtUbicacion.Text);
    SetInventario(ChkInventario.Checked);
  end;
end;

procedure TFrmProductoExtendido.Refrescar;
begin
  //Se Actualiza El DataBuscador
  DataBuscadorProducto := BuscadorProducto.ObtenerLista;
  //DataProductoPrecio := ProductoPrecio.ObtenerLista;
  //Se Actualizan Los demas Componentes
  DataDivision := Division.ObtenerLista;
  DataLinea := Linea.ObtenerLista;
  DataFamilia := Familia.ObtenerLista;
  DataClase := ClaseClase.ObtenerLista;
  DataManufactura := Manufactura.ObtenerLista;
  DataArancel := Arancel.ObtenerLista;
  DataUnidad := Unidad.ObtenerLista;
  DataDivision.First;
  DataLinea.First;
  DataFamilia.First;
  DataClase.First;
  DataManufactura.First;
  DataArancel.First;
  DataUnidad.First;
  DSDivision.DataSet := DataDivision;
  DSLinea.DataSet := DataLinea;
  DSFamilia.DataSet := DataFamilia;
  DSClase.DataSet := DataClase;
  DSManufactura.DataSet := DataManufactura;
  DSArancel.DataSet := DataArancel;
  DSUnidad.DataSet := DataUnidad;
end;

procedure TFrmProductoExtendido.TBtnEditarClick(Sender: TObject);
begin
  inherited;
  TxtReferencia.SetFocus;
end;

procedure TFrmProductoExtendido.TxtPrecioExit(Sender: TObject);
begin
  inherited;
  Parametro.SetNombreParametro('usar_tipos_precios');
  if ((Parametro.Buscar) and (Parametro.ObtenerValorAsBoolean)) then
  begin
    //Actualizando Tipos de Precios (Solo Cuando se Agrega)
    if (Agregando) then
    begin
      with DataProductoPrecio do
      begin
        DisableControls;
        First;
        while (not Eof) do
        begin
          Edit;
          FieldValues['precio'] := TxtPrecio.Value;
          Post;
          Next;
        end;
        EnableControls;
        First;
      end;
    end;
    //Consultando Al Usuario si Desea Actualizar los Tipos de Precios de Acuerdo al % de Calculo Automatico
    if (MessageBox(Self.Handle, '�Desea Calcular Automaticamente Los Tipos de Precio?', PChar(MsgTituloInformacion), MB_ICONQUESTION or MB_YESNO or MB_TOPMOST or MB_TASKMODAL) = mrYes) then
    begin
      with DataProductoPrecio do
      begin
        DisableControls;
        First;
        while (not Eof) do
        begin
          TipoPrecioProducto.SetCodigo(FieldValues['codigo_tipo_precio_producto']);
          TipoPrecioProducto.Buscar;
          Edit;
          if (TipoPrecioProducto.GetCalculoAutomatico) then
          begin
            if (TipoPrecioProducto.GetPCalculoAutomatico > 0) then //Se Incrementa de acuerdo al %
            begin
              FieldValues['precio'] := TxtPrecio.Value + ((TxtPrecio.Value * TipoPrecioProducto.GetPCalculoAutomatico) / 100);
            end
            else  //Se Decrementa de acuerdo al %
            begin
              FieldValues['precio'] := TxtPrecio.Value - ((TxtPrecio.Value * Abs(TipoPrecioProducto.GetPCalculoAutomatico)) / 100);
              if (FieldValues['precio'] < 0) then
              begin
                FieldValues['precio'] := 0;
              end;
            end;
          end
          else
          begin
            if (TipoPrecioProducto.GetCalculoMontoFijo) then
            begin
              if (TipoPrecioProducto.GetMontoFijo > 0) then //Se Incrementa de acuerdo al Monto Fijo
              begin
                FieldValues['precio'] := TxtPrecio.Value + TipoPrecioProducto.GetMontoFijo;
              end
              else //Se Decrementa de acuerdo al Monto Fijo
              begin
                FieldValues['precio'] := TxtPrecio.Value - Abs(TipoPrecioProducto.GetMontoFijo);
                if (FieldValues['precio'] < 0) then
                begin
                  FieldValues['precio'] := 0;
                end;
              end;
            end;
          end;
          Post;
          Next;
        end;
        EnableControls;
        First;
      end;
    end;    
  end;    
end;

function TFrmProductoExtendido.Validar: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  (* Validando Informacion del Producto *)
  //Validando Codigo
  if (Length(Trim(TxtCodigo.Text)) = 0) then
  begin
    ListaErrores.Add('El C�digo No Puede Estar En Blanco!!!');
  end;
  //Validando Referencia
  if (Length(Trim(TxtReferencia.Text)) = 0) then
  begin
    ListaErrores.Add('La Referencia No Puede Estar En Blanco!!!');
  end;
  //Validando Descripcion
  if (Length(Trim(TxtDescripcion.Text)) = 0) then
  begin
    ListaErrores.Add('La Descripci�n No Puede Estar En Blanco!!!');
  end;
  //Validando C�digo de la Division
  if (Length(Trim(CmbCodigoDivision.Text)) = 0) then
  begin
    ListaErrores.Add('El C�digo de Divisi�n No Puede Estar En Blanco!!!');
  end;
  //Validando C�digo de la L�nea
  if (Length(Trim(CmbCodigoLinea.Text)) = 0) then
  begin
    ListaErrores.Add('El C�digo de la L�nea No Puede Estar En Blanco!!!');
  end;
  //Validando C�digo de la Familia
  if (Length(Trim(CmbCodigoFamilia.Text)) = 0) then
  begin
    ListaErrores.Add('El C�digo de la Familia No Puede Estar En Blanco!!!');
  end;
  //Validando C�digo de la Clase
  if (Length(Trim(CmbCodigoClase.Text)) = 0) then
  begin
    ListaErrores.Add('El C�digo de la Clase No Puede Estar En Blanco!!!');
  end;
  //Validando C�digo de la Manufactura
  if (Length(Trim(CmbCodigoManufactura.Text)) = 0) then
  begin
    ListaErrores.Add('El C�digo de la Manufactura No Puede Estar En Blanco!!!');
  end;
  //Validando C�digo de la Unidad
  if (Length(Trim(CmbCodigoUnidad.Text)) = 0) then
  begin
    ListaErrores.Add('El C�digo de la Unidad No Puede Estar En Blanco!!!');
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
