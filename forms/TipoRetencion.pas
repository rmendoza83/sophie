unit TipoRetencion;

{$I sophie.inc}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Generico, XPMan, ImgList, DB, Grids, DBGrids, StdCtrls, Buttons,
  ExtCtrls, ComCtrls, ToolWin, Mask, rxToolEdit, rxCurrEdit, DBClient,
  Utils, ClsTipoRetencion{$IFDEF DELPHI10}, ImageList{$ENDIF};

type
  TFrmTipoRetencion = class(TFrmGenerico)
    LblCodigo: TLabel;
    TxtCodigo: TEdit;
    LblDescripcion: TLabel;
    TxtDescripcion: TEdit;
    LblRetencionIva: TLabel;
    ChkRetencionIva: TCheckBox;
    LblBase: TLabel;
    TxtBase: TCurrencyEdit;
    LblTarifa: TLabel;
    TxtTarifa: TCurrencyEdit;
    LblSustraendo: TLabel;
    TxtSustraendo: TCurrencyEdit;
    LblCobranza: TLabel;
    ChkCobranza: TCheckBox;
    LblPago: TLabel;
    ChkPago: TCheckBox;
    procedure GrdBusquedaCellClick(Column: TColumn);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    Clase: TTipoRetencion;
    Data: TClientDataSet;
    procedure Agregar;
    procedure Editar;
    procedure Eliminar;
    procedure Guardar;
    procedure Cancelar;
    procedure Refrescar;
    procedure Imprimir;
    procedure Buscar;
    procedure Limpiar;
    procedure Asignar;
    procedure Obtener;
    procedure Mover;
  protected
    function Validar: Boolean; override;
  public
    { Public declarations }
  end;

var
  FrmTipoRetencion: TFrmTipoRetencion;

implementation

{$R *.dfm}

{ TFrmTipoRetencion }

procedure TFrmTipoRetencion.Agregar;
begin
  //Verificando
  Limpiar;
  TxtCodigo.SetFocus;
end;

procedure TFrmTipoRetencion.Asignar;
begin
  with Clase do
  begin
    TxtCodigo.Text := GetCodigo;
    TxtDescripcion.Text := GetDescripcion;
    ChkRetencionIva.Checked := GetRetencionIVA;
    TxtBase.Value := GetBase;
    TxtTarifa.Value := GetTarifa;
    TxtSustraendo.Value := GetSustraendo;
    ChkCobranza.Checked := GetCobranza;
    ChkPago.Checked := GetPago;
  end;
end;

procedure TFrmTipoRetencion.Buscar;
begin
  Screen.Cursor := crSQLWait;
  if (Length(Trim(TxtCriterio.Text)) > 0) then
  begin
    Data := Clase.ObtenerListaCriterio(Trim(TxtCriterio.Text));
  end
  else
  begin
    Data := Clase.ObtenerLista;
  end;
  Data.First;
  DS.DataSet := Data;
  GrdBusqueda.Refresh;
  Screen.Cursor := crDefault;
end;

procedure TFrmTipoRetencion.Cancelar;
begin
  TxtCodigo.Enabled := True;
end;

procedure TFrmTipoRetencion.Editar;
begin
  //Se Busca el Elemento
  Clase.SetCodigo(Data.FieldValues['codigo']);
  Clase.Buscar;
  Asignar;
  TxtCodigo.Enabled := False;
  TxtDescripcion.SetFocus;
end;

procedure TFrmTipoRetencion.Eliminar;
begin
  //Se Busca el Elemento
  Clase.SetCodigo(Data.FieldValues['codigo']);
  Clase.Buscar;
  Clase.Eliminar;
  MessageBox(Self.Handle, 'El Tipo de Retencion Ha Sido Eliminado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  Refrescar;
  Limpiar;
end;

procedure TFrmTipoRetencion.FormCreate(Sender: TObject);
begin
  ProcAgregar := Agregar;
  ProcEditar := Editar;
  ProcEliminar := Eliminar;
  ProcGuardar := Guardar;
  ProcCancelar := Cancelar;
  ProcRefrescar := Refrescar;
  ProcImprimir := Imprimir;
  ProcBuscar := Buscar;
  Clase := TTipoRetencion.Create;
  Buscar;
end;

procedure TFrmTipoRetencion.GrdBusquedaCellClick(Column: TColumn);
begin
  Mover;
end;

procedure TFrmTipoRetencion.Guardar;
begin
  if (Agregando) then
  begin
    //Agregando...
    //Se Busca el Codigo
    Clase.SetCodigo(TxtCodigo.Text);
    if (Clase.Buscar) then
    begin
      MessageBox(Self.Handle, 'El C�digo Ya Existe!!!', PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      TxtCodigo.SetFocus;
      Abort;
    end
    else
    begin
      Obtener;
      Clase.Insertar;
      MessageBox(Self.Handle, 'El Tipo de Retencion Ha Sido Agregado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    end;
  end
  else
  begin
    //Editando...
    Obtener;
    Clase.Modificar;
    MessageBox(Self.Handle, 'El Tipo de Retencion Ha Sido Editado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    TxtCodigo.Enabled := True;
  end;
  Refrescar;
  Limpiar;
end;

procedure TFrmTipoRetencion.Imprimir;
begin

end;

procedure TFrmTipoRetencion.Limpiar;
begin
  TxtCodigo.Text := '';
  TxtDescripcion.Text := '';
  ChkRetencionIva.Checked := False;
  TxtBase.Text := '';
  TxtTarifa.Text := '';
  TxtSustraendo.Text := '';
  ChkCobranza.Checked := False;
  ChkPago.Checked := False;
end;

procedure TFrmTipoRetencion.Mover;
var
  DSTemp: TDataSet;
begin
  if (DS.DataSet.RecordCount > 0) then
  begin
    DSTemp := DS.DataSet;
    with Clase do
    begin
      SetCodigo(DSTemp.FieldValues['codigo']);
      SetDescripcion(DSTemp.FieldValues['descripcion']);
      SetRetencionIVA(DSTemp.FieldValues['retencion_iva']);
      SetBase(DSTemp.FieldValues['base']);
      SetTarifa(DSTemp.FieldValues['tarifa']);
      SetSustraendo(DSTemp.FieldValues['sustraendo']);
      SetCobranza(DSTemp.FieldValues['cobranza']);
      SetPago(DSTemp.FieldValues['pago']);
    end;
    Asignar;
  end
  else
  begin
    Limpiar;
  end;
end;

procedure TFrmTipoRetencion.Obtener;
begin
  with Clase do
  begin
    SetCodigo(TxtCodigo.Text);
    SetDescripcion(TxtDescripcion.Text);
    SetRetencionIVA(ChkRetencionIva.Checked);
    SetBase(TxtBase.Value);
    SetTarifa(TxtTarifa.Value);
    SetSustraendo(TxtSustraendo.Value);
    SetCobranza(ChkCobranza.Checked);
    SetPago(ChkPago.Checked);
  end;
end;

procedure TFrmTipoRetencion.Refrescar;
begin
  TxtCriterio.Text := '';
  Buscar;
end;

function TFrmTipoRetencion.Validar: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  (* Validando Informacion de la Cuenta Bancaria *)
  //Validando Codigo
  if (Length(Trim(TxtCodigo.Text)) = 0) then
  begin
    ListaErrores.Add('El C�digo No Puede Estar En Blanco!!!');
  end;
  //Validando Descripci�n
  if (Length(Trim(TxtDescripcion.Text)) = 0) then
  begin
    ListaErrores.Add('La Descripci�n No Puede Estar En Blanco!!!');
  end;
  //Validando El Porcentaje de la Base
  if (Length(Trim(TxtBase.Text)) = 0) then
  begin
    ListaErrores.Add('La Base Debe Ser Distinto de 0!!!');
  end;
  //Validando El Porcentaje de la Tarifa
  if (Length(Trim(TxtTarifa.Text)) = 0) then
  begin
    ListaErrores.Add('La Tarifa Debe Ser Distinto de 0!!!');
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
