unit NotaDebitoProveedor;

{$I sophie.inc}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GenericoDetalle, DB, DBClient, ImgList, XPMan, ComCtrls, Grids,
  DBGrids, ExtCtrls, ToolWin, StdCtrls, Buttons, RxLookup, Mask, rxToolEdit,
  Utils, rxCurrEdit, ClsNotaDebitoProveedor, ClsProveedor, ClsZona,
  ClsEsquemaPago, ClsMoneda, ClsReporte, ClsParametro,
  ClsBusquedaNotaDebitoProveedor, GenericoBuscador, ClsDetNotaCreditoProveedor,
  ClsDocumentoLegal, ClsDocumentoAnulado{$IFDEF DELPHI10}, ImageList{$ENDIF};

type
  TFrmNotaDebitoProveedor = class(TFrmGenericoDetalle)
    DatFechaLibros: TDateEdit;
    LblFechaLibros: TLabel;
    LblFechaContable: TLabel;
    DatFechaContable: TDateEdit;
    DatFechaRecepcion: TDateEdit;
    LblRecepcion: TLabel;
    LblFechaIngreso: TLabel;
    DatFechaIngreso: TDateEdit;
    LblMoneda: TLabel;
    CmbCodigoMoneda: TRxDBLookupCombo;
    BtnBuscarMoneda: TBitBtn;
    TabConcepto: TTabSheet;
    TxtConcepto: TRichEdit;
    TabObservaciones: TTabSheet;
    TxtObservaciones: TRichEdit;
    DSProveedor: TDataSource;
    DSZona: TDataSource;
    DSEsquemaPago: TDataSource;
    DSMoneda: TDataSource;
    LblCodigoProveedor: TLabel;
    CmbCodigoProveedor: TRxDBLookupCombo;
    BtnBuscarProveedor: TBitBtn;
    LblCodigoZona: TLabel;
    CmbCodigoZona: TRxDBLookupCombo;
    BtnBuscarZona: TBitBtn;
    LblEsquemaPago: TLabel;
    CmbCodigoEsquemaPago: TRxDBLookupCombo;
    BtnBuscarEsquemaPago: TBitBtn;
    LblNumero: TLabel;
    TxtNumero: TEdit;
    LblMontoNeto: TLabel;
    TxtMontoNeto: TCurrencyEdit;
    LblMontoDescuento: TLabel;
    TxtMontoDescuento: TCurrencyEdit;
    TxtPDescuento: TLabel;
    LblSubTotal: TLabel;
    TxtSubTotal: TCurrencyEdit;
    LblImpuesto: TLabel;
    TxtImpuesto: TCurrencyEdit;
    TxtPIVA: TLabel;
    LblTotal: TLabel;
    TxtTotal: TCurrencyEdit;
    procedure TxtMontoNetoExit(Sender: TObject);
    procedure TxtPIVADblClick(Sender: TObject);
    procedure LblImpuestoDblClick(Sender: TObject);
    procedure TxtPDescuentoDblClick(Sender: TObject);
    procedure LblMontoDescuentoDblClick(Sender: TObject);
    procedure BtnBuscarMonedaClick(Sender: TObject);
    procedure BtnBuscarEsquemaPagoClick(Sender: TObject);
    procedure BtnBuscarZonaClick(Sender: TObject);
    procedure CmbCodigoProveedorExit(Sender: TObject);
    procedure CmbCodigoProveedorChange(Sender: TObject);
    procedure BtnBuscarProveedorClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TBtnEliminarClick(Sender: TObject);
  private
    { Private declarations }
    Clase: TNotaDebitoProveedor;
    BuscadorNotaDebitoProveedor: TBusquedaNotaDebitoProveedor;
    Proveedor: TProveedor;
    Zona: TZona;
    EsquemaPago: TEsquemaPago;
    Moneda: TMoneda;
    Reporte: TReporte;
    Parametro: TParametro;
    DocumentoLegal: TDocumentoLegal;
    DocumentoAnulado: TDocumentoAnulado;
    DataBuscadorNotaDebitoProveedor: TClientDataSet;
    DataProveedor: TClientDataSet;
    DataZona: TClientDataSet;
    DataEsquemaPago: TClientDataSet;
    DataMoneda: TClientDataSet;
    procedure Agregar;
    procedure Editar;
    procedure Eliminar;
    procedure Guardar;
    procedure Cancelar;
    procedure Refrescar;
    procedure Imprimir;
    procedure Buscar;
    procedure Limpiar;
    procedure Asignar;
    procedure Obtener;
    procedure Mover;
    procedure CalcularMontosCabecera;
  protected
    { Protected declarations }
    function Validar: Boolean; override;
  public
    { Public declarations }
  end;

var
  FrmNotaDebitoProveedor: TFrmNotaDebitoProveedor;

implementation

uses
  ActividadSophie,
  PModule,
  ClsGenericoBD,
  SolicitarMonto,
  DocumentoLegal
  {$IFDEF USE_RAVE_REPORTS}
  , DMRR
  {$ENDIF}
  {$IFDEF USE_FAST_REPORTS}
  , DMFR
  {$ENDIF}
  ;

{$R *.dfm}

{ TFrmNotaDebitoProveedor }

procedure TFrmNotaDebitoProveedor.Agregar;
begin
  //Verificando
  Limpiar;
  //Asignando Valores por Defecto
  TxtPIVA.Caption := FormatFloat('#,##0.0000%',Parametro.ObtenerValor('p_iva'));
  Clase.SetPIva(Parametro.ObtenerValor('p_iva'));
  CmbCodigoZona.Value := Parametro.ObtenerValor('codigo_zona');
  CmbCodigoEsquemaPago.Value := Parametro.ObtenerValor('codigo_esquema_pago');
  CmbCodigoMoneda.Value := Parametro.ObtenerValor('codigo_moneda');
  CmbCodigoProveedor.SetFocus;
end;

procedure TFrmNotaDebitoProveedor.Asignar;
begin
  with Clase do
  begin
    TxtNumero.Text := GetNumero;
    CmbCodigoProveedor.KeyValue := GetCodigoProveedor;
    CmbCodigoZona.KeyValue := GetCodigoZona;
    CmbCodigoEsquemaPago.KeyValue := GetCodigoEsquemaPago;
    CmbCodigoMoneda.KeyValue := GetCodigoMoneda;
    DatFechaIngreso.Date := GetFechaIngreso;
    DatFechaRecepcion.Date := GetFechaRecepcion;
    DatFechaContable.Date := GetFechaContable;
    DatFechaLibros.Date := GetFechaLibros;
    TxtConcepto.Text := GetConcepto;
    TxtObservaciones.Text := GetObservaciones;
    TxtMontoNeto.Value := GetMontoNeto;
    TxtMontoDescuento.Value := GetMontoDescuento;
    TxtSubTotal.Value := GetSubTotal;
    TxtImpuesto.Value := GetImpuesto;
    TxtTotal.Value := GetTotal;
    TxtPDescuento.Caption := FormatFloat('###0.0000%',GetPDescuento);
    TxtPIVA.Caption := FormatFloat('###0.0000%',GetPIva);
    DS.DataSet := Data;
  end;
end;

procedure TFrmNotaDebitoProveedor.BtnBuscarEsquemaPagoClick(Sender: TObject);
begin
  Buscador(Self,EsquemaPago,'codigo','Buscar Esquema de Pago',nil,DataEsquemaPago);
  CmbCodigoEsquemaPago.KeyValue := DataEsquemaPago.FieldValues['codigo'];
end;

procedure TFrmNotaDebitoProveedor.BtnBuscarMonedaClick(Sender: TObject);
begin
  Buscador(Self,Moneda,'codigo','Buscar Moneda',nil,DataMoneda);
  CmbCodigoMoneda.KeyValue := DataMoneda.FieldValues['codigo'];
end;

procedure TFrmNotaDebitoProveedor.BtnBuscarProveedorClick(Sender: TObject);
begin
  Buscador(Self,Proveedor,'codigo','Buscar Proveedor',nil,DataProveedor);
  CmbCodigoProveedor.KeyValue := DataProveedor.FieldValues['codigo'];
  CmbCodigoProveedorExit(Sender);
end;

procedure TFrmNotaDebitoProveedor.BtnBuscarZonaClick(Sender: TObject);
begin
  Buscador(Self,Zona,'codigo','Buscar Zona',nil,DataZona);
  CmbCodigoZona.KeyValue := DataZona.FieldValues['codigo'];
end;

procedure TFrmNotaDebitoProveedor.Buscar;
begin
  if (Buscador(Self,BuscadorNotaDebitoProveedor,'numero','Buscar Nota de Debito',nil,DataBuscadorNotaDebitoProveedor)) then
  begin
    Clase.SetNumero(DataBuscadorNotaDebitoProveedor.FieldValues['numero']);
    Clase.BuscarNumero;
    Asignar;
  end;
end;

procedure TFrmNotaDebitoProveedor.CalcularMontosCabecera;
begin
  TxtSubTotal.Value := TxtMontoNeto.Value - TxtMontoDescuento.Value;
  TxtTotal.Value := TxtSubTotal.Value + TxtImpuesto.Value;
end;

procedure TFrmNotaDebitoProveedor.Cancelar;
begin
  if (Agregando) then
  begin
    Limpiar;
  end
  else
  begin
    Clase.SetNumero(TxtNumero.Text);
    Clase.BuscarNumero;
    Asignar;
  end;
end;

procedure TFrmNotaDebitoProveedor.CmbCodigoProveedorChange(Sender: TObject);
begin
  inherited;
  SB.Panels[0].Text := DataProveedor.FieldValues['razon_social'];
end;

procedure TFrmNotaDebitoProveedor.CmbCodigoProveedorExit(Sender: TObject);
begin
  if (Length(Trim(CmbCodigoProveedor.Text)) > 0) then
  begin
    if (Agregando) then
    begin
      Proveedor.SetCodigo(CmbCodigoProveedor.Value);
      Proveedor.BuscarCodigo;
      CmbCodigoZona.Value := Proveedor.GetCodigoZona;
      CmbCodigoEsquemaPago.Value := Proveedor.GetCodigoEsquemaPago;
    end;
  end;
end;

procedure TFrmNotaDebitoProveedor.Editar;
begin
  if (Length(Trim(TxtNumero.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
end;

procedure TFrmNotaDebitoProveedor.Eliminar;
begin
  //Se Busca La Nota de Debito!
  Clase.SetNumero(TxtNumero.Text);
  Clase.BuscarNumero;
  //Se Registra El Documento Anulado y Eliminan Registros del Documento Legal
  DocumentoLegal.SetDesde('NDP');
  DocumentoLegal.SetNumeroDocumento(Clase.GetNumero);
  if (DocumentoLegal.Buscar) then
  begin
    //Registrando El Documento Anulado
    with DocumentoAnulado do
    begin
      SetDesde('NDP');
      SetNumeroDocumento(Clase.GetNumero);
      SetSerie(DocumentoLegal.GetSerie);
      SetNumeroControl(DocumentoLegal.GetNumeroControl);
      SetFechaDocumento(Clase.GetFechaIngreso);
      SetCodigoCliente('');
      SetCodigoProveedor(Clase.GetCodigoProveedor);
      SetCodigoZona(Clase.GetCodigoZona);
      SetSubTotal(Clase.GetSubTotal);
      SetImpuesto(Clase.GetImpuesto);
      SetTotal(Clase.GetTotal);
      SetPIva(Clase.GetPIva);
      SetLoginUsuario(P.User.GetLoginUsuario);
      SetFecha(Date);
      SetHora(Time);
      Insertar;
    end;
    //Borrando Documento Legal
    DocumentoLegal.Eliminar;
  end;
  Clase.Eliminar;
  MessageBox(Self.Handle, 'La Nota de D�bito de Proveedor Se Ha Eliminado Con Exito!!!',PChar(MsgTituloInformacion), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  Refrescar;
  Limpiar;
end;

procedure TFrmNotaDebitoProveedor.FormCreate(Sender: TObject);
begin
  inherited;
  ProcAgregar := Agregar;
  ProcEditar := Editar;
  ProcEliminar := Eliminar;
  ProcGuardar := Guardar;
  ProcCancelar := Cancelar;
  ProcRefrescar := Refrescar;
  ProcImprimir := Imprimir;
  ProcBuscar := Buscar;
  FrmActividadSophie.Show;
  FrmActividadSophie.Actividad('Cargando N�cleo...');
  Clase := TNotaDebitoProveedor.Create;
  //Inicializando DataSets Secundarios
  BuscadorNotaDebitoProveedor := TBusquedaNotaDebitoProveedor.Create;
  Proveedor := TProveedor.Create;
  Zona := TZona.Create;
  EsquemaPago := TEsquemaPago.Create;
  Moneda := TMoneda.Create;
  Reporte := TReporte.Create;
  Parametro := TParametro.Create;
  DocumentoLegal := TDocumentoLegal.Create;
  DocumentoAnulado := TDocumentoAnulado.Create;
  FrmActividadSophie.Actividad('Cargando Proveedores...');
  DataProveedor := Proveedor.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Zonas...');
  DataZona := Zona.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Esquemas de Pago...');
  DataEsquemaPago := EsquemaPago.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Monedas...');
  DataMoneda := Moneda.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Resto del N�cleo...');
  DataBuscadorNotaDebitoProveedor := BuscadorNotaDebitoProveedor.ObtenerLista;
  DataProveedor.First;
  DataZona.First;
  DataEsquemaPago.First;
  DataMoneda.First;
  DSProveedor.DataSet := DataProveedor;
  DSZona.DataSet := DataZona;
  DSEsquemaPago.DataSet := DataEsquemaPago;
  DSMoneda.DataSet := DataMoneda;
  ConfigurarRxDBLookupCombo(CmbCodigoProveedor,DSProveedor,'codigo','codigo;razon_social');
  ConfigurarRxDBLookupCombo(CmbCodigoZona,DSZona,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoEsquemaPago,DSEsquemaPago,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoMoneda,DSMoneda,'codigo','codigo;descripcion');
  DS.DataSet := Data;
  GridDetalle.DataSource := DS;
  FrmActividadSophie.Close;
  //Ocultado la pesta�a de Detalles
  TabDetalle.TabVisible := False;
  //Desactivando el ReadOnly
  TxtMontoNeto.ReadOnly := False;
  TxtImpuesto.ReadOnly := False;
  //Buscar;
end;

procedure TFrmNotaDebitoProveedor.Guardar;
var
  SwDocumentoLegal: Boolean;
begin
  CalcularMontosCabecera;
  //Data.EmptyDataSet;
  Obtener;
  if (Agregando) then
  begin
    //Agregando...
    //Guardando Encabezado
    Clase.Insertar;
    //Solicitando Datos Legales
    FrmDocumentoLegal := TFrmDocumentoLegal.CreateInsert(Self,'NDP',Clase.GetNumero,'','');
    FrmDocumentoLegal.ShowModal;
    if FrmDocumentoLegal.Aceptado then
    begin //Se Guardan Los Datos Legales
      DocumentoLegal.SetDesde(FrmDocumentoLegal.Desde);
      DocumentoLegal.SetNumeroDocumento(Clase.GetNumero);
      DocumentoLegal.SetSerie(FrmDocumentoLegal.Serie);
      DocumentoLegal.SetNumeroControl(FrmDocumentoLegal.NumeroDocumento);
      DocumentoLegal.SetLoginUsuario(P.User.GetLoginUsuario);
      DocumentoLegal.SetFecha(Date);
      DocumentoLegal.SetHora(Time);
      DocumentoLegal.Insertar;
    end;
    FreeAndNil(FrmDocumentoLegal);
    Application.ProcessMessages;
    MessageBox(Self.Handle, 'La Nota de D�bito Ha Sido Agregada Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    //Se Imprime la Nota de D�bito
    MessageBox(Self.Handle, 'Presione "Aceptar" Para Proceder A Imprimir La Nota de D�bito de Proveedor!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    with Reporte do
    begin
      AgregarParametro('NumeroNotaDebitoProveedor',Clase.GetNumero);
      AgregarParametro('MontoLetra',NumeroALetras(Clase.GetTotal));
      begin
        ImprimeReporte('NotaDebitoProveedor',trDocumento);
      end;
    end;
  end
  else
  begin
    //Editando...
    Clase.Modificar;
    //Solicitando Datos Legales
    DocumentoLegal.SetDesde('NDP');
    DocumentoLegal.SetNumeroDocumento(Clase.GetNumero);
    SwDocumentoLegal := DocumentoLegal.Buscar;
    if (SwDocumentoLegal) then
    begin
      FrmDocumentoLegal := TFrmDocumentoLegal.CreateEdit(Self,'NDP',Clase.GetNumero,DocumentoLegal.GetSerie,DocumentoLegal.GetNumeroControl);
    end
    else
    begin
      FrmDocumentoLegal := TFrmDocumentoLegal.CreateInsert(Self,'NDP',Clase.GetNumero,'','');
    end;
    FrmDocumentoLegal.ShowModal;
    if FrmDocumentoLegal.Aceptado then
    begin //Se Guardan Los Datos Legales
      DocumentoLegal.SetSerie(FrmDocumentoLegal.Serie);
      DocumentoLegal.SetNumeroControl(FrmDocumentoLegal.NumeroControl);
      DocumentoLegal.SetLoginUsuario(P.User.GetLoginUsuario);
      DocumentoLegal.SetFecha(Date);
      DocumentoLegal.SetHora(Time);
      if (SwDocumentoLegal) then
      begin
        DocumentoLegal.Modificar;
      end
      else
      begin
        DocumentoLegal.Insertar;
      end;
    end;
    FreeAndNil(FrmDocumentoLegal);
    MessageBox(Self.Handle, 'La Nota de D�bito de Proveedor Ha Sido Editada Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end;
  PageDetalles.ActivePage := TabConcepto;
end;

procedure TFrmNotaDebitoProveedor.Imprimir;
begin
  if (Length(Trim(TxtNumero.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
  with Reporte do
  begin
    AgregarParametro('NumeroNotaDebitoProveedor',Clase.GetNumero);
    AgregarParametro('MontoLetra',NumeroALetras(Clase.GetTotal));
    EmiteReporte('NotaDebitoProveedorFormaLibre',trDocumento);
  end;
end;

procedure TFrmNotaDebitoProveedor.LblImpuestoDblClick(Sender: TObject);
var
  Aux: Currency;
begin
  Aux := 0;
  Solicitar(Self,'Impuesto',TxtImpuesto.Value,Aux);
  if (Aux >= 0) then
  begin
    TxtImpuesto.Value := Aux;
    CalcularMontosCabecera;
  end;
end;

procedure TFrmNotaDebitoProveedor.LblMontoDescuentoDblClick(Sender: TObject);
var
  Aux: Currency;
begin
  Aux := 0;
  Solicitar(Self,'Monto Descuento',TxtMontoDescuento.Value,Aux);
  if (Aux > 0) then
  begin
    TxtMontoDescuento.Value := Aux;
    TxtPDescuento.Caption := FormatFloat('#,##0.0000%',Aux * 100 / TxtMontoNeto.Value);
    CalcularMontosCabecera;
  end
  else
  begin
    TxtMontoDescuento.Value := Aux;
    TxtPDescuento.Caption := FormatFloat('#,##0.0000%',Aux);
    CalcularMontosCabecera;
  end;
end;

procedure TFrmNotaDebitoProveedor.Limpiar;
begin
  TxtNumero.Text := '';
  CmbCodigoProveedor.KeyValue := '';
  CmbCodigoZona.KeyValue := '';
  CmbCodigoEsquemaPago.KeyValue := '';
  CmbCodigoMoneda.KeyValue := '';
  DatFechaIngreso.Date := Date;
  DatFechaRecepcion.Date := Date;
  DatFechaContable.Date := Date;
  DatFechaLibros.Date := Date;
  TxtConcepto.Text := '';
  TxtObservaciones.Text := '';
  TxtMontoNeto.Value := 0;
  TxtMontoDescuento.Value := 0;
  TxtSubTotal.Value := 0;
  TxtImpuesto.Value := 0;
  TxtTotal.Value := 0;
  TxtPDescuento.Caption := FormatFloat('###0.0000%',0);
  TxtPIVA.Caption := FormatFloat('###0.0000%',0);
  //Status Bar
  SB.Panels[0].Text := '';
  //Data.EmptyDataSet;
  //Formato Concepto Libre
{  TxtMontoNeto.ReadOnly := True;
  TabDetalle.TabVisible := True;
  PageDetalles.ActivePage := TabDetalle;}
end;

procedure TFrmNotaDebitoProveedor.Mover;
begin

end;

procedure TFrmNotaDebitoProveedor.Obtener;
begin
  with Clase do
  begin
    SetNumero(TxtNumero.Text);
    SetCodigoProveedor(CmbCodigoProveedor.KeyValue);
    SetCodigoZona(CmbCodigoZona.KeyValue);
    SetCodigoEsquemaPago(CmbCodigoEsquemaPago.KeyValue);
    SetCodigoMoneda(CmbCodigoMoneda.KeyValue);
    SetFechaIngreso(DatFechaIngreso.Date);
    SetFechaRecepcion(DatFechaRecepcion.Date);
    SetFechaContable(DatFechaContable.Date);
    SetFechaLibros(DatFechaLibros.Date);
    SetConcepto(TxtConcepto.Text);
    SetObservaciones(TxtObservaciones.Text);
    SetMontoNeto(TxtMontoNeto.Value);
    SetMontoDescuento(TxtMontoDescuento.Value);
    SetSubTotal(TxtSubTotal.Value);
    SetImpuesto(TxtImpuesto.Value);
    SetTotal(TxtTotal.Value);
    SetPDescuento(FormatearFloat(StringReplace(TxtPDescuento.Caption,'%','',[rfReplaceAll])));
    SetPIva(FormatearFloat(StringReplace(TxtPIVA.Caption,'%','',[rfReplaceAll])));
    SetTasaDolar(0); //***//
    SetLoginUsuario(P.User.GetLoginUsuario);
    if (Agregando) then
    begin
      SetTiempoIngreso(Now);
      SetIdNotaDebitoProveedor(-1);
      SetIdPago(-1);
      SetIdContadoPago(-1);
    end;
  end;
end;

procedure TFrmNotaDebitoProveedor.Refrescar;
begin
  //Se Actualizan Los demas Componentes
  DataProveedor := Proveedor.ObtenerCombo;
  DataZona := Zona.ObtenerCombo;
  DataEsquemaPago := EsquemaPago.ObtenerCombo;
  DataMoneda := Moneda.ObtenerCombo;
  DataBuscadorNotaDebitoProveedor := BuscadorNotaDebitoProveedor.ObtenerLista;
  DataProveedor.First;
  DataZona.First;
  DataEsquemaPago.First;
  DataMoneda.First;
  DSProveedor.DataSet := DataProveedor;
  DSZona.DataSet := DataZona;
  DSEsquemaPago.DataSet := DataEsquemaPago;
  DSMoneda.DataSet := DataMoneda;
end;

procedure TFrmNotaDebitoProveedor.TBtnEliminarClick(Sender: TObject);
begin
  if (Length(Trim(TxtNumero.Text)) > 0) then
  begin
    inherited;
  end
  else
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
end;

procedure TFrmNotaDebitoProveedor.TxtMontoNetoExit(Sender: TObject);
begin
  CalcularMontosCabecera;
end;

procedure TFrmNotaDebitoProveedor.TxtPDescuentoDblClick(Sender: TObject);
var
  Aux: Currency;
begin
  Aux := 0;
  Solicitar(Self,'(%) Descuento',StrToFloat(StringReplace(TxtPDescuento.Caption,'%','',[rfReplaceAll])),Aux);
  if (Aux > 0) then
  begin
    TxtPDescuento.Caption := FormatFloat('#,##0.0000%',Aux);
    TxtMontoDescuento.Value := Aux * TxtMontoNeto.Value / 100;
    CalcularMontosCabecera;
  end
  else
  begin
    TxtPDescuento.Caption := FormatFloat('#,##0.0000%',Aux);
    TxtMontoDescuento.Value := Aux;
    CalcularMontosCabecera;
  end;
end;

procedure TFrmNotaDebitoProveedor.TxtPIVADblClick(Sender: TObject);
var
  Aux: Currency;
begin
  Aux := 0;
  Solicitar(Self,'(%) I.V.A.',StrToFloat(StringReplace(TxtPIVA.Caption,'%','',[rfReplaceAll])),Aux);
  if (Aux >= 0) then
  begin
    TxtPIVA.Caption := FormatFloat('#,##0.0000%',Aux);
    TxtImpuesto.Value := Aux * TxtMontoNeto.Value / 100;
    CalcularMontosCabecera;
  end;
end;

function TFrmNotaDebitoProveedor.Validar: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  //Validando Numero Nota de Debito
  if (Length(Trim(TxtNumero.Text)) = 0) then
  begin
    ListaErrores.Add('El Numero de la Nota de D�bito No Puede Estar En Blanco!!!');
  end;
  //Validando Proveedor
  if (Length(Trim(CmbCodigoProveedor.Text)) = 0) then
  begin
    ListaErrores.Add('El Proveedor No Puede Estar En Blanco!!!');
  end;
  //Validando Fechas
  if ((DatFechaIngreso.Date > DatFechaRecepcion.Date) or (DatFechaIngreso.Date > DatFechaContable.Date) or (DatFechaIngreso.Date > DatFechaLibros.Date)) then
  begin
    ListaErrores.Add('La Fecha de Ingreso Debe Ser Menor a las Fechas de Recepcion, Contable y Libros Respectivamente!!!');
  end;
  //Validando Detalle o Forma Libre
  if (Length(Trim(TxtConcepto.Text)) = 0) then
  begin
    ListaErrores.Add('El Concepto No Puede Estar En Blanco!!!');
  end;
  if (TxtMontoNeto.Value <= 0) then
  begin
    ListaErrores.Add('El Monto Neto Debe Ser Positivo!!!');
  end;
  if (TxtMontoDescuento.Value < 0) then
  begin
    ListaErrores.Add('El Monto de Descuento No Debe Ser Negativo!!!');
  end;
  if (TxtImpuesto.Value < 0) then
  begin
    ListaErrores.Add('El Impuesto No Debe Ser Negativo!!!');
  end;
  //Validando Total
  if (TxtTotal.Value <= 0) then
  begin
    ListaErrores.Add('El Total Debe Ser Positivo!!!');
  end;
  //Validando Esquema de Pago
  if Length(Trim(CmbCodigoEsquemaPago.KeyValue)) = 0 then
  begin
    ListaErrores.Add('El Esquema de Pago No Puede Estar en Blanco');
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
