unit GenericoEditarDetalle;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, DBClient, ClsParametro, Utils;

type
  TFrmGenericoEditarDetalle = class(TForm)
    PanelBotones: TPanel;
    Aceptar: TBitBtn;
    BtnCerrar: TBitBtn;
    procedure BtnCerrarClick(Sender: TObject);
    procedure AceptarClick(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    Data: TClientDataSet;
    Parametro: TParametro;
    procedure Agregar; virtual; abstract;
    procedure Mover; virtual; abstract;
  public
    { Public declarations }
    constructor CrearDetalle(AOwner: TComponent; DataSet: TClientDataSet); virtual;
  end;

var
  FrmGenericoEditarDetalle: TFrmGenericoEditarDetalle;

implementation

{$R *.dfm}

{ TForm1 }

procedure TFrmGenericoEditarDetalle.AceptarClick(Sender: TObject);
begin
  Agregar;
end;

procedure TFrmGenericoEditarDetalle.BtnCerrarClick(Sender: TObject);
begin
  Close;
end;

constructor TFrmGenericoEditarDetalle.CrearDetalle(AOwner: TComponent; DataSet: TClientDataSet);
begin
  inherited Create(AOwner);
  Data := DataSet;
  Parametro := TParametro.Create;
  //Asignando Decimales por Defecto
  Parametro.SetNombreParametro('numero_decimales');
  if (not Parametro.Buscar) then
  begin
    Parametro.SetCategoria('GENERAL');
    Parametro.SetTipoDato('INTEGER');
    Parametro.SetValor('2');
    Parametro.Insertar;
  end;
  BuscarDisplayFormat(Self,Parametro.ObtenerValorAsInteger);
  Mover;
end;

end.
