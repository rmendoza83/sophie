unit RadioLiquidacionContrato;

{$I sophie.inc}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GenericoDetalle, DB, DBClient, ImgList, XPMan, ComCtrls, Grids,
  DBGrids, ExtCtrls, ToolWin, rxCurrEdit, Mask, rxToolEdit, StdCtrls, Buttons,
  RxLookup, Utils, ClsRadioLiquidacionContrato, ClsRadioDetLiquidacionContrato,
  ClsCliente, ClsZona, ClsBusquedaFacturaRadioContrato,
  ClsBusquedaRadioLiquidacionContrato, ClsRadioContratoFacturaPendiente,
  ClsRadioContratoFacturaGenerada, ClsRadioContratoDistribucionFactura,
  ClsRadioContrato, ClsRadioContratoPrograma, ClsRadioPrograma,
  ClsFacturacionCliente, ClsCuentaxCobrar, ClsCobranzaCobrada, ClsEsquemaPago,
  GenericoBuscador, ClsDetFacturacionCliente, ClsDetMovimientoInventario,
  ClsDocumentoLegal, ClsDocumentoAnulado, ClsReporte, ClsParametro
  {$IFDEF DELPHI10}, ImageList{$ENDIF};

type
  TFrmRadioLiquidacionContrato = class(TFrmGenericoDetalle)
    LblNumero: TLabel;
    LblCodigoCliente: TLabel;
    TxtNumero: TEdit;
    CmbCodigoCliente: TRxDBLookupCombo;
    BtnBuscarCliente: TBitBtn;
    LblCodigoZona: TLabel;
    CmbCodigoZona: TRxDBLookupCombo;
    BtnBuscarZona: TBitBtn;
    LblFechaIngreso: TLabel;
    LblTotal: TLabel;
    LblFechaContable: TLabel;
    DatFechaIngreso: TDateEdit;
    TxtTotal: TCurrencyEdit;
    DatFechaContable: TDateEdit;
    Datanumero: TStringField;
    Datatotal_contrato: TFloatField;
    Dataitem: TIntegerField;
    Datatotal_items: TIntegerField;
    Datafecha_ingreso: TDateField;
    Datatotal: TFloatField;
    Datafecha_inicial: TDateField;
    Datafecha_final: TDateField;
    Dataobservaciones: TStringField;
    Dataid_radio_contrato: TIntegerField;
    LblFechaFactura: TLabel;
    DatFechaFactura: TDateEdit;
    TabObservaciones: TTabSheet;
    TxtObservaciones: TRichEdit;
    DSCliente: TDataSource;
    DSZona: TDataSource;
    procedure GridDetalleKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure DataAfterPost(DataSet: TDataSet);
    procedure CmbCodigoClienteChange(Sender: TObject);
    procedure CmbCodigoClienteExit(Sender: TObject);
    procedure BtnBuscarZonaClick(Sender: TObject);
    procedure BtnBuscarClienteClick(Sender: TObject);
    private
    { Private declarations }
    Clase: TRadioLiquidacionContrato;
    RadioDetLiquidacionContrato: TRadioDetLiquidacionContrato;
    BuscadorRadioLiquidacionContrato: TBusquedaRadioLiquidacionContrato;
    Cliente: TCliente;
    Zona: TZona;
    RadioContrato: TRadioContrato;
    RadioContratoFacturaPendiente: TRadioContratoFacturaPendiente;
    RadioContratoFacturaGenerada: TRadioContratoFacturaGenerada;
    RadioContratoDistribucionFactura: TRadioContratoDistribucionFactura;
    RadioContratoPrograma: TRadioContratoPrograma;
    RadioPrograma: TRadioPrograma;
    FacturacionCliente: TFacturacionCliente;
    CuentaxCobrar: TCuentaxCobrar;
    CobranzaCobrada: TCobranzaCobrada;
    DetFacturacionCliente: TDetFacturacionCliente;
    MovimientoInventario: TDetMovimientoInventario;
    DocumentoLegal: TDocumentoLegal;
    DocumentoAnulado: TDocumentoAnulado;
    EsquemaPago: TEsquemaPago;
    Reporte: TReporte;
    Parametro: TParametro;
    DataBuscadorRadioLiquidacionContrato: TClientDataSet;
    DataCliente: TClientDataSet;
    DataZona: TClientDataSet;
    procedure Agregar;
    procedure Editar;
    procedure Eliminar;
    procedure Guardar;
    procedure Cancelar;
    procedure Refrescar;
    procedure Imprimir;
    procedure Buscar;
    procedure Limpiar;
    procedure Asignar;
    procedure Obtener;
    procedure Mover;
    procedure GuardarDetalles;
    procedure CalcularMontos;
  protected
    { Protected declarations }
    function Validar: Boolean; override;
    procedure Detalle; override;
  public
    { Public declarations }
  end;

var
  FrmRadioLiquidacionContrato: TFrmRadioLiquidacionContrato;

implementation

uses
  ActividadSophie,
  PModule,
  ClsGenericoBD,
  AgregarDetalleRadioLiquidacionContrato,
  Math,
  Types
  {$IFDEF USE_RAVE_REPORTS}
  , DMRR
  {$ENDIF}
  {$IFDEF USE_FAST_REPORTS}
  , DMFR
  {$ENDIF}
  ;

{$R *.dfm}

{ TFrmRadioLiquidacionContrato }

procedure TFrmRadioLiquidacionContrato.Agregar;
begin
  //Verificando
  Limpiar;
  //Asignando Valores por Defecto
  CmbCodigoZona.Value := Parametro.ObtenerValor('codigo_zona');
  CmbCodigoCliente.SetFocus;
end;

procedure TFrmRadioLiquidacionContrato.Asignar;
var
  DataDetalle: TClientDataSet;
  Campo: string;
  i: Integer;
begin
  with Clase do
  begin
    TxtNumero.Text := GetNumero;
    CmbCodigoCliente.KeyValue := GetCodigoCliente;
    CmbCodigoZona.KeyValue := GetCodigoZona;
    DatFechaIngreso.Date := GetFechaIngreso;
    DatFechaContable.Date := GetFechaContable;
    DatFechaFactura.Date := GetFechaFactura;
    TxtTotal.Value := GetTotal;
    TxtObservaciones.Text := GetObservaciones; 
    //Cargando DataSet Secundario "Data"
    DataDetalle := TClientDataSet.Create(Self);
    DataDetalle.CloneCursor(RadioDetLiquidacionContrato.ObtenerIdRadioLiquidacionContrato(GetIdRadioLiquidacionContrato),False,True);
    Data.EmptyDataSet;
    DataDetalle.First;
    while (not DataDetalle.Eof) do
    begin
      Data.Append;
      //Asignando Campos Similares
      for i := 0 to DataDetalle.FieldCount - 1 do
      begin
        Campo := DataDetalle.FieldDefs[i].Name;
        if (Data.FieldDefs.IndexOf(Campo) <> -1) then
        begin
          Data.FieldValues[Campo] := DataDetalle.FieldValues[Campo];
        end;        
      end;
      //Se Ubica La Distribucion de Factura
      RadioContratoDistribucionFactura.SetIdRadioContrato(DataDetalle.FieldValues['id_radio_contrato']);
      RadioContratoDistribucionFactura.SetItem(DataDetalle.FieldValues['item']);
      RadioContratoDistribucionFactura.Buscar;
      Data.FieldValues['total_items'] := RadioContratoDistribucionFactura.GetTotalItems;
      Data.FieldValues['fecha_ingreso'] := RadioContratoDistribucionFactura.GetFechaIngreso;
      Data.FieldValues['total'] := RadioContratoDistribucionFactura.GetTotal;
      Data.FieldValues['fecha_inicial'] := RadioContratoDistribucionFactura.GetFechaInicial;
      Data.FieldValues['fecha_final'] := RadioContratoDistribucionFactura.GetFechaFinal;
      Data.FieldValues['observaciones'] := RadioContratoDistribucionFactura.GetObservaciones;
      //Se Ubica El Contrato
      RadioContrato.SetIdRadioContrato(DataDetalle.FieldValues['id_radio_contrato']);
      RadioContrato.Buscar;
      Data.FieldValues['numero_contrato'] := RadioContrato.GetNumero;
      Data.FieldValues['total_contrato'] := RadioContrato.GetTotal;
      Data.Post;
      DataDetalle.Next;
    end;
    DS.DataSet := Data;
    DataDetalle.Free;
  end;
end;

procedure TFrmRadioLiquidacionContrato.BtnBuscarClienteClick(Sender: TObject);
begin
  Buscador(Self,Cliente,'codigo','Buscar Cliente',nil,DataCliente);
  CmbCodigoCliente.KeyValue := DataCliente.FieldValues['codigo'];
  CmbCodigoClienteExit(Sender);
end;

procedure TFrmRadioLiquidacionContrato.BtnBuscarZonaClick(Sender: TObject);
begin
  Buscador(Self,Zona,'codigo','Buscar Zona',nil,DataZona);
  CmbCodigoZona.KeyValue := DataZona.FieldValues['codigo'];
end;

procedure TFrmRadioLiquidacionContrato.Buscar;
begin
  if (Buscador(Self,BuscadorRadioLiquidacionContrato,'numero','Buscar Liquidaci�n de Contrato [Radio]',nil,DataBuscadorRadioLiquidacionContrato)) then
  begin
    Clase.SetNumero(DataBuscadorRadioLiquidacionContrato.FieldValues['numero']);
    Clase.BuscarNumero;
    Asignar;
  end;
end;

procedure TFrmRadioLiquidacionContrato.CalcularMontos;
var
  Neto: Currency;
begin
  Neto := 0;
  with Data do
  begin
    DisableControls;
    First;
    while (not Eof) do
    begin
      Neto := Neto + FieldValues['total'];
      Next;
    end;
    EnableControls;
  end;
  //Asignando Montos
  TxtTotal.Value := Neto;
end;

procedure TFrmRadioLiquidacionContrato.Cancelar;
begin
  if (Agregando) then
  begin
    Limpiar;
  end
  else
  begin
    Clase.SetNumero(TxtNumero.Text);
    Clase.BuscarNumero;
    Asignar;
  end;
end;

procedure TFrmRadioLiquidacionContrato.CmbCodigoClienteChange(Sender: TObject);
begin
  SB.Panels[0].Text := DataCliente.FieldValues['razon_social'];
end;

procedure TFrmRadioLiquidacionContrato.CmbCodigoClienteExit(Sender: TObject);
begin
  if (Length(Trim(CmbCodigoCliente.Text)) > 0) then
  begin
    if (Agregando) then
    begin
      Cliente.SetCodigo(CmbCodigoCliente.Value);
      Cliente.BuscarCodigo;
      CmbCodigoZona.Value := Cliente.GetCodigoZona;
    end;
  end;
end;

procedure TFrmRadioLiquidacionContrato.DataAfterPost(DataSet: TDataSet);
begin
  CalcularMontos;
end;

procedure TFrmRadioLiquidacionContrato.Detalle;
begin
  inherited;
  FrmAgregarDetalleRadioLiquidacionContrato := TFrmAgregarDetalleRadioLiquidacionContrato.CrearDetalle(Self,Data);
  FrmAgregarDetalleRadioLiquidacionContrato.CodigoCliente := CmbCodigoCliente.Text;
  FrmAgregarDetalleRadioLiquidacionContrato.ShowModal;
  FreeAndNil(FrmAgregarDetalleRadioLiquidacionContrato);
end;

procedure TFrmRadioLiquidacionContrato.Editar;
begin
  if (Length(Trim(TxtNumero.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
end;

procedure TFrmRadioLiquidacionContrato.Eliminar;
var
  DsDetalle: TClientDataSet;
  sw: Boolean;
begin
  //Se Valida Que La Liquidacion de Contrato Pueda Ser Anulada
  sw := True;
  DsDetalle := RadioDetLiquidacionContrato.ObtenerIdRadioLiquidacionContrato(Clase.GetIdRadioLiquidacionContrato);
  DsDetalle.First;
  while ((not DsDetalle.Eof) and (sw)) do
  begin
    //Se Ubica La Factura Generada
    RadioContratoFacturaGenerada.SetIdRadioContrato(DsDetalle.FieldValues['id_radio_contrato']);
    RadioContratoFacturaGenerada.SetItem(DsDetalle.FieldValues['item']);
    RadioContratoFacturaGenerada.Buscar;
    //Se Ubica La Factura de Cliente
    FacturacionCliente.SetIdFacturacionCliente(RadioContratoFacturaGenerada.GetIdFacturacionCliente);
    FacturacionCliente.Buscar;
    if (FacturacionCliente.GetIdContadoCobranza = -1) then //Puede Anularse
    begin
      //Verificando si Tiene una Cobranza Realizada
      CobranzaCobrada.SetIdCobranza(FacturacionCliente.GetIdCobranza);
      if (CobranzaCobrada.BuscarIdCobranza) then
      begin
        sw := False;
      end;
    end;
    DsDetalle.Next;
  end;
  if (sw) then
  begin
    //Eliminando Facturas y Generando Acciones Necesarias
    with Data do
    begin
      DisableControls;
      First;
      while (not Eof) do
      begin
        //Eliminando Factura
        //Ubicando Factura Generada
        RadioContratoFacturaGenerada.SetIdRadioContrato(FieldValues['id_radio_contrato']);
        RadioContratoFacturaGenerada.SetItem(FieldValues['item']);
        RadioContratoFacturaGenerada.Buscar;
        //Ubicando Factura de Cliente
        FacturacionCliente.SetIdFacturacionCliente(RadioContratoFacturaGenerada.GetIdFacturacionCliente);
        //Se Eliminan Los Detalles
        DetFacturacionCliente.EliminarIdFacturacionCliente(FacturacionCliente.GetIdFacturacionCliente);
        //Se Eliminan Los Movimientos de Inventario
        MovimientoInventario.EliminarIdMovimientoInventario(FacturacionCliente.GetIdMovimientoInventario);
        //Se Eliminan Los Detalles De la Cobranza
        CuentaxCobrar.EliminarIdCobranza(FacturacionCliente.GetIdCobranza);
        //Se Registra El Documento Anulado y Eliminan Registros del Documento Legal
        DocumentoLegal.SetDesde('FC');
        DocumentoLegal.SetNumeroDocumento(FacturacionCliente.GetNumero);
        if (DocumentoLegal.Buscar) then
        begin
          //Registrando El Documento Anulado
          with DocumentoAnulado do
          begin
            SetDesde('FC');
            SetNumeroDocumento(FacturacionCliente.GetNumero);
            SetSerie(DocumentoLegal.GetSerie);
            SetNumeroControl(DocumentoLegal.GetNumeroControl);
            SetFechaDocumento(Clase.GetFechaIngreso);
            SetCodigoCliente(Clase.GetCodigoCliente);
            SetCodigoProveedor('');
            SetCodigoZona(Clase.GetCodigoZona);
            SetSubTotal(FacturacionCliente.GetSubTotal);
            SetImpuesto(FacturacionCliente.GetImpuesto);
            SetTotal(FacturacionCliente.GetTotal);
            SetPIva(FacturacionCliente.GetPIva);
            SetLoginUsuario(P.User.GetLoginUsuario);
            SetFecha(Date);
            SetHora(Time);
            Insertar;
          end;
          //Borrando Documento Legal
          DocumentoLegal.Eliminar;
        end;
        FacturacionCliente.Eliminar;
        //Revertiendo Factura Generada a Factura Pendiente
        with RadioContratoFacturaPendiente do
        begin
          SetIdRadioContrato(RadioContratoFacturaGenerada.GetIdRadioContrato);
          SetItem(RadioContratoFacturaGenerada.GetItem);
          SetTotalItems(RadioContratoFacturaGenerada.GetTotalItems);
          SetFechaIngreso(RadioContratoFacturaGenerada.GetFechaIngreso);
          SetTotal(RadioContratoFacturaGenerada.GetTotal);
          SetFechaInicial(RadioContratoFacturaGenerada.GetFechaInicial);
          SetFechaFinal(RadioContratoFacturaGenerada.GetFechaFinal);
          SetObservaciones(RadioContratoFacturaGenerada.GetObservaciones);
          Insertar;
        end;
        //Eliminando Factura Generada
        RadioContratoFacturaGenerada.Eliminar;
        //Siguiente Detalle...
        Next;
      end;
      EnableControls;      
    end;
    //Se Busca La Liquidacion De Contrato
    Clase.SetNumero(TxtNumero.Text);
    Clase.BuscarNumero;
    //Se Eliminan Los Detalles
    RadioDetLiquidacionContrato.EliminarIdRadioLiquidacionContrato(Clase.GetIdRadioLiquidacionContrato);
    Clase.Eliminar;
    MessageBox(Self.Handle, 'La Liquidaci�n de Contrato Se Ha Eliminado Con Exito!!!',PChar(MsgTituloInformacion), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Refrescar;
    Limpiar;
  end
  else
  begin
    MessageBox(Self.Handle, 'La Liquidaci�n de Contrato Presenta Algunas Facturas con Liquidaciones Asociadas y No Puede Ser Eliminado!!!',PChar(MsgTituloAdvertencia), MB_ICONEXCLAMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end;
end;

procedure TFrmRadioLiquidacionContrato.FormCreate(Sender: TObject);
begin
  inherited;
  ProcAgregar := Agregar;
  ProcEditar := Editar;
  ProcEliminar := Eliminar;
  ProcGuardar := Guardar;
  ProcCancelar := Cancelar;
  ProcRefrescar := Refrescar;
  ProcImprimir := Imprimir;
  ProcBuscar := Buscar;
  FrmActividadSophie.Show;
  FrmActividadSophie.Actividad('Cargando N�cleo...');
  Clase := TRadioLiquidacionContrato.Create;
  //Inicializando DataSets Secundarios
  BuscadorRadioLiquidacionContrato := TBusquedaRadioLiquidacionContrato.Create;
  Cliente := TCliente.Create;
  Zona := TZona.Create;
  RadioContrato := TRadioContrato.Create;
  RadioContratoFacturaPendiente := TRadioContratoFacturaPendiente.Create;
  RadioContratoFacturaGenerada := TRadioContratoFacturaGenerada.Create;
  RadioContratoDistribucionFactura := TRadioContratoDistribucionFactura.Create;
  RadioContratoPrograma := TRadioContratoPrograma.Create;
  RadioPrograma := TRadioPrograma.Create;
  FacturacionCliente := TFacturacionCliente.Create;
  CuentaxCobrar := TCuentaxCobrar.Create;
  CobranzaCobrada := TCobranzaCobrada.Create;
  DetFacturacionCliente := TDetFacturacionCliente.Create;
  MovimientoInventario := TDetMovimientoInventario.Create;
  DocumentoLegal := TDocumentoLegal.Create;
  DocumentoAnulado := TDocumentoAnulado.Create;
  EsquemaPago := TEsquemaPago.Create;
  Reporte := TReporte.Create;
  Parametro := TParametro.Create;
  RadioDetLiquidacionContrato := TRadioDetLiquidacionContrato.Create;
  FrmActividadSophie.Actividad('Cargando Clientes...');
  DataCliente := Cliente.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Zonas...');
  DataZona := Zona.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Resto del N�cleo...');
  DataBuscadorRadioLiquidacionContrato := BuscadorRadioLiquidacionContrato.ObtenerLista;
  DataCliente.First;
  DataZona.First;
  DataBuscadorRadioLiquidacionContrato.First;
  DSCliente.DataSet := DataCliente;
  DSZona.DataSet := DataZona;
  ConfigurarRxDBLookupCombo(CmbCodigoCliente,DSCliente,'codigo','codigo;razon_social');
  ConfigurarRxDBLookupCombo(CmbCodigoZona,DSZona,'codigo','codigo;descripcion');
  DS.DataSet := Data;
  GridDetalle.DataSource := DS;
  FrmActividadSophie.Close;
  //Buscar;
end;

procedure TFrmRadioLiquidacionContrato.GridDetalleKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if (Agregando or Editando) then
  begin
    if (((Key = VK_INSERT) or (Key = VK_DELETE)) and (Length(Trim(CmbCodigoCliente.Text)) = 0)) then
    begin
      MessageBox(Self.Handle, 'Seleccione Primero un Cliente!!!', PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      CmbCodigoCliente.SetFocus;
      CmbCodigoCliente.DropDown;
      Abort;
    end;
  end;
  //Llamando a Evento Padre
  inherited;
end;

procedure TFrmRadioLiquidacionContrato.Guardar;
begin
  CalcularMontos;
  Obtener;
  if (Agregando) then
  begin
    //Agregando...
    //Guardando Encabezado
    Clase.Insertar;
    TxtNumero.Text := Clase.GetNumero;
    Application.ProcessMessages;
    //Guardando Detalles
    GuardarDetalles;
    MessageBox(Self.Handle, 'La Liquidaci�n de Contrato Ha Sido Agregada Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    //Se Imprime La Liquidacion
    MessageBox(Self.Handle, 'Presione "Aceptar" Para Proceder A Imprimir La Liquidaci�n de Contrato!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    with Reporte do
    begin
      AgregarParametro('Numero',Clase.GetNumero);
      AgregarParametro('MontoLetra',NumeroALetras(Clase.GetTotal));
      EmiteReporte('RadioLiquidacionContrato',trGeneral);
    end;
  end
  else
  begin
    //Editando...
    Clase.Modificar;
    //Actualizando Detalles (Eliminando Anteriores y Cargando los Nuevos)
    RadioDetLiquidacionContrato.EliminarIdRadioLiquidacionContrato(Clase.GetIdRadioLiquidacionContrato);
    //Guardando Detalles
    GuardarDetalles;
    MessageBox(Self.Handle, 'La Liquidaci�n de Contrato Ha Sido Editada Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end;
  //Actualizando Dataset de Busqueda
  DataBuscadorRadioLiquidacionContrato := BuscadorRadioLiquidacionContrato.ObtenerLista;
end;

procedure TFrmRadioLiquidacionContrato.GuardarDetalles;
var
  Dias: Integer;
begin
  //Ubicando Esquema de Pago por Defecto
  EsquemaPago.SetCodigo(Parametro.ObtenerValor('codigo_esquema_pago_radio'));
  EsquemaPago.Buscar;
  if (EsquemaPago.GetCuotas) then
  begin
    Dias := EsquemaPago.GetDiasCuota;
  end
  else
  begin
    if (EsquemaPago.GetDias) then
    begin
      Dias := EsquemaPago.GetCantidadDias;
    end
    else
    begin
      Dias := 0;
    end;
  end;
  with Data do
  begin
    DisableControls;
    First;
    while (not Eof) do
    begin
      //Guardando Detalle de la Liquidacion de Contrato
      with RadioDetLiquidacionContrato do
      begin
        SetIdRadioLiquidacionContrato(Clase.GetIdRadioLiquidacionContrato);
        SetIdRadioContrato(FieldValues['id_radio_contrato']);
        SetItem(FieldValues['item']);
        Insertar;
      end;
      //Generando Factura y Cuenta por Cobrar
      //Ubicando Datos del Contrato
      RadioContrato.SetNumero(FieldValues['numero_contrato']);
      RadioContrato.BuscarNumero;
      RadioContratoPrograma.SetIdRadioContrato(RadioContrato.GetIdRadioContrato);
      RadioContratoPrograma.BuscarIdRadioContrato;
      RadioPrograma.SetCodigo(RadioContratoPrograma.GetCodigoPrograma);
      RadioPrograma.Buscar;
      //Guardando Factura
      with FacturacionCliente do
      begin
        SetNumero('');
        SetNumeroPedido('Indefinido');
        SetCodigoCliente(CmbCodigoCliente.KeyValue);
        SetCodigoVendedor(RadioContrato.GetCodigoVendedor);
        SetCodigoEstacion(Parametro.ObtenerValor('codigo_estacion_radio'));
        SetCodigoZona(RadioContrato.GetCodigoZona);
        SetCodigoEsquemaPago(Parametro.ObtenerValor('codigo_esquema_pago_radio'));
        SetCodigoMoneda(Parametro.ObtenerValor('codigo_moneda'));
        SetFechaIngreso(DatFechaFactura.Date);
        SetFechaRecepcion(DatFechaFactura.Date);
        SetFechaContable(DatFechaFactura.Date);
        SetFechaLibros(DatFechaFactura.Date);
        SetConcepto(UpperCase('FACTURA AUTOMATICA SEGUN CONTRATO No.'+ RadioContrato.GetNumero+#13+#10+
                    RadioPrograma.GetDescripcion));
        SetFormaLibre(True);
        SetObservaciones(FieldValues['observaciones']);
        SetMontoNeto(FieldValues['total']);
        SetMontoDescuento(0);
        SetSubTotal(GetMontoNeto);
        SetImpuesto(GetMontoNeto * (Parametro.ObtenerValor('p_iva') / 100));
        SetTotal(GetMontoNeto + GetImpuesto);
        SetPDescuento(0);
        SetPIva(Parametro.ObtenerValor('p_iva'));
        SetTasaDolar(0); //***//
        SetLoginUsuario(P.User.GetLoginUsuario);
        SetTiempoIngreso(Now);
        SetIdFacturacionCliente(-1);
        SetIdCobranza(-1);
        SetIdMovimientoInventario(-1);
        SetIdContadoCobranza(-1);
        Insertar;
      end;
      //Guardando Cuenta x Cobrar
      with CuentaxCobrar do
      begin
        SetDesde('FC');
        SetNumeroDocumento(FacturacionCliente.GetNumero);
        SetCodigoMoneda(FacturacionCliente.GetCodigoMoneda);
        SetCodigoCliente(FacturacionCliente.GetCodigoCliente);
        SetCodigoVendedor(FacturacionCliente.GetCodigoVendedor);
        SetCodigoZona(FacturacionCliente.GetCodigoZona);
        SetFechaIngreso(FacturacionCliente.GetFechaIngreso);
        SetFechaRecepcion(FacturacionCliente.GetFechaRecepcion);
        SetFechaVencIngreso(FacturacionCliente.GetFechaIngreso+Dias);
        SetFechaVencRecepcion(FacturacionCliente.GetFechaRecepcion+Dias);
        SetFechaLibros(FacturacionCliente.GetFechaLibros);
        SetFechaContable(FacturacionCliente.GetFechaContable);
        SetFechaCobranza(Date);
        SetNumeroCuota(-1);
        SetTotalCuotas(-1);
        SetOrdenCobranza(1);
        SetMontoNeto(FacturacionCliente.GetMontoNeto);
        SetImpuesto(FacturacionCliente.GetImpuesto);
        SetTotal(FacturacionCliente.GetTotal);
        SetPIva(FacturacionCliente.GetPIva);
        SetIdCobranza(FacturacionCliente.GetIdCobranza);
        SetIdContadoCobranza(-1);
        Insertar;
      end;
      //Moviendo Distribucion de Facturas (De Pendientes a Generada)
      //Ubicando Factura Pendiente
      RadioContratoFacturaPendiente.SetIdRadioContrato(RadioContrato.GetIdRadioContrato);
      RadioContratoFacturaPendiente.SetItem(FieldValues['item']);
      RadioContratoFacturaPendiente.Buscar;
      //Registrando Factura Generada
      with RadioContratoFacturaGenerada do
      begin
        SetIdRadioContrato(RadioContratoFacturaPendiente.GetIdRadioContrato);
        SetItem(RadioContratoFacturaPendiente.GetItem);
        SetTotalItems(RadioContratoFacturaPendiente.GetTotalItems);
        SetFechaIngreso(RadioContratoFacturaPendiente.GetFechaIngreso);
        SetTotal(RadioContratoFacturaPendiente.GetTotal);
        SetFechaInicial(RadioContratoFacturaPendiente.GetFechaInicial);
        SetFechaFinal(RadioContratoFacturaPendiente.GetFechaFinal);
        SetObservaciones(RadioContratoFacturaPendiente.GetObservaciones);
        SetIdFacturacionCliente(FacturacionCliente.GetIdFacturacionCliente);
        SetNumeroDocumento(FacturacionCliente.GetNumero);
        Insertar;
      end;
      //Eliminando Factura Pendiente
      RadioContratoFacturaPendiente.Eliminar;
      //Fin de la Liquidacion del Item
      Next;
    end;
    EnableControls;
  end;
end;

procedure TFrmRadioLiquidacionContrato.Imprimir;
begin
  if (Length(Trim(TxtNumero.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
  with Reporte do
  begin
    AgregarParametro('Numero',Clase.GetNumero);
    AgregarParametro('MontoLetra',NumeroALetras(Clase.GetTotal));
    EmiteReporte('RadioLiquidacionContrato',trGeneral);
  end;
end;

procedure TFrmRadioLiquidacionContrato.Limpiar;
begin
  TxtNumero.Text := '';
  CmbCodigoCliente.KeyValue := '';
  CmbCodigoZona.KeyValue := '';
  DatFechaIngreso.Date := Date;
  DatFechaContable.Date := Date;
  DatFechaFactura.Date := Date;
  TxtTotal.Value := 0;
  TxtObservaciones.Text := '';
  //Status Bar
  SB.Panels[0].Text := '';
  Data.EmptyDataSet;
end;

procedure TFrmRadioLiquidacionContrato.Mover;
begin

end;

procedure TFrmRadioLiquidacionContrato.Obtener;
begin
  with Clase do
  begin
    SetNumero(TxtNumero.Text);
    SetCodigoCliente(CmbCodigoCliente.Text);
    SetCodigoZona(CmbCodigoZona.Text);
    SetFechaIngreso(DatFechaIngreso.Date);
    SetFechaContable(DatFechaContable.Date);
    SetFechaFactura(DatFechaFactura.Date);
    SetTotal(TxtTotal.Value);
    SetObservaciones(TxtObservaciones.Text);
    SetIdRadioLiquidacionContrato(-1);
  end;
end;

procedure TFrmRadioLiquidacionContrato.Refrescar;
begin
  //Actualizando Dataset de Busqueda
  DataBuscadorRadioLiquidacionContrato := BuscadorRadioLiquidacionContrato.ObtenerLista;
  //Se Actualizan Los Demas Componentes
  DataCliente := Cliente.ObtenerCombo;
  DataZona := Zona.ObtenerCombo;
  DataBuscadorRadioLiquidacionContrato := BuscadorRadioLiquidacionContrato.ObtenerLista;
  DataCliente.First;
  DataZona.First;
  DataBuscadorRadioLiquidacionContrato.First;
  DSCliente.DataSet := DataCliente;
  DSZona.DataSet := DataZona;
end;

function TFrmRadioLiquidacionContrato.Validar: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  //Validando Cliente
  if (Length(Trim(CmbCodigoCliente.Text)) = 0) then
  begin
    ListaErrores.Add('El Cliente No Puede Estar En Blanco!!!');
  end;
  //Validando Zona
  if (Length(Trim(CmbCodigoZona.Text)) = 0) then
  begin
    ListaErrores.Add('La Zona No Puede Estar En Blanco!!!');
  end;
  //Validando Monto
  if (TxtTotal.Value <= 0) then
  begin
    ListaErrores.Add('El Total Debe Ser Positivo!!!');
  end;
  //Validando Fechas
  if (DatFechaIngreso.Date > DatFechaContable.Date) then
  begin
    ListaErrores.Add('La Fecha de Ingreso Debe Ser Menor a la Fecha Contable!!!');
  end;
  //Validando Detalle
  if (Data.RecordCount = 0) then
  begin
    ListaErrores.Add('El Detalle De La Liquidaci�n No Puede Estar Vacio!!!');
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
