unit AgregarDetalleFacturacionProveedor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GenericoAgregarDetalle, StdCtrls, Buttons, ExtCtrls, Mask,
  rxToolEdit, rxCurrEdit, ClsProducto, ClsDetFacturacionCliente,
  GenericoBuscador, DB, DBClient, Utils;

type
  TFrmAgregarDetalleFacturacionProveedor = class(TFrmGenericoAgregarDetalle)
    LblCantidad: TLabel;
    TxtCantidad: TCurrencyEdit;
    TxtDescripcionProducto: TEdit;
    TxtCosto: TCurrencyEdit;
    LblCantidadPedida: TLabel;
    TxtCantidadPedida: TCurrencyEdit;
    LblCosto: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure BtnBuscarClick(Sender: TObject);
    procedure TxtCantidadKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    Producto: TProducto;
  protected
    function Buscar: Boolean; override;
    procedure Agregar; override;
    procedure Limpiar; override;
  public
    { Public declarations }
  end;

var
  FrmAgregarDetalleFacturacionProveedor: TFrmAgregarDetalleFacturacionProveedor;

implementation

{$R *.dfm}

{ TFrmAgregarDetalleFacturacionProveedor }

procedure TFrmAgregarDetalleFacturacionProveedor.Agregar;
begin
  with Data do
  begin
    //Verificando si existe el codigo en el Grid
    if (Locate('codigo_producto',TxtCodigo.Text,[loCaseInsensitive])) then
    begin
      Edit;
      FieldValues['cantidad'] := FieldValues['cantidad'] + TxtCantidad.Value;
      FieldValues['monto_descuento'] := (TxtCosto.Value * FieldValues['cantidad']) * (Producto.GetPDescuento / 100);
      FieldValues['impuesto'] := (TxtCosto.Value * FieldValues['cantidad']) * (Producto.GetPIvaCompra / 100);
      FieldValues['total'] := TxtCosto.Value * FieldValues['cantidad'];
    end
    else
    begin
      Append;
      FieldValues['codigo_producto'] := Producto.GetCodigo;
      FieldValues['referencia'] := Producto.GetReferencia;
      FieldValues['descripcion'] := Producto.GetDescripcion;
      FieldValues['cantidad'] := TxtCantidad.Value;
      FieldValues['cantidad_pendiente'] := TxtCantidadPedida.Value;
      FieldValues['costo'] := TxtCosto.Value;
      FieldValues['p_descuento'] := Producto.GetPDescuento;
      FieldValues['monto_descuento'] := (TxtCosto.Value * TxtCantidad.Value) * (Producto.GetPDescuento / 100);
      FieldValues['impuesto'] := (TxtCosto.Value * TxtCantidad.Value) * (Producto.GetPIvaCompra / 100);
      FieldValues['p_iva'] := Producto.GetPIvaCompra;
      FieldValues['total'] := TxtCosto.Value * TxtCantidad.Value;
      FieldValues['id_facturacion_proveedor'] := -1;
    end;
    Post;
  end;
  inherited;
end;

procedure TFrmAgregarDetalleFacturacionProveedor.BtnBuscarClick(
  Sender: TObject);
begin
  inherited;
  Buscador(Self,Producto,'codigo','Buscar Producto',TxtCodigo,nil);
  if (Length(Trim(TxtCodigo.Text)) > 0) then
  begin
    ComponenteFoco.SetFocus;
  end;
end;

function TFrmAgregarDetalleFacturacionProveedor.Buscar: Boolean;
begin
  Producto.SetCodigo(TxtCodigo.Text);
  Result := Producto.BuscarCodigo;
  if (Result) then
  begin
    TxtDescripcionProducto.Text := Producto.GetDescripcion;
  end
  else
  begin
    MessageBox(Self.Handle, Cadena('El Codigo No Existe!!!'), PChar(MsgTituloError), MB_ICONSTOP or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Limpiar;
  end;
end;

procedure TFrmAgregarDetalleFacturacionProveedor.FormCreate(Sender: TObject);
begin
  inherited;
  ComponenteFoco := (TxtCantidad as TWinControl);
  Producto := TProducto.Create;
end;

procedure TFrmAgregarDetalleFacturacionProveedor.Limpiar;
begin
  inherited;
  TxtDescripcionProducto.Text := '';
  TxtCantidad.Value := 1;
  TxtCantidadPedida.Value := 1;
  TxtCosto.Value := 0;
end;

procedure TFrmAgregarDetalleFacturacionProveedor.TxtCantidadKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  case Key of
    VK_RETURN:
      Agregar;
  end;
end;

end.
