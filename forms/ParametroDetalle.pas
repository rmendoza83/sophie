unit ParametroDetalle;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Mask, rxToolEdit, rxCurrEdit,
  ClsParametro;

type
  TFrmParametroDetalle = class(TForm)
    LblNombreParametro: TLabel;
    LblTipoDato: TLabel;
    LblValor: TLabel;
    TxtNombreParametro: TEdit;
    CmbTipoDato: TComboBox;
    TxtValor: TEdit;
    PanelBotones: TPanel;
    Aceptar: TBitBtn;
    BtnCerrar: TBitBtn;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure AceptarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtnCerrarClick(Sender: TObject);
  private
    { Private declarations }
    Agregando: Boolean;
    Editando: Boolean;
    Parametro: TParametro;
    NombreParametro: string;
    Categoria: string;
  public
    { Public declarations }
    Aceptado: Boolean;
    constructor CrearAgregando(AOwner: TComponent; varCategoria: string);
    constructor CrearEditando(AOwner: TComponent; varNombreParametro, varCategoria: string);
  end;

var
  FrmParametroDetalle: TFrmParametroDetalle;

implementation

uses ClsGenericoBD;

{$R *.dfm}

procedure TFrmParametroDetalle.AceptarClick(Sender: TObject);
begin
  Aceptado := True;
  Close;
end;

procedure TFrmParametroDetalle.BtnCerrarClick(Sender: TObject);
begin
  Close;
end;

constructor TFrmParametroDetalle.CrearAgregando(AOwner: TComponent; varCategoria: string);
begin
  inherited Create(AOwner);
  Agregando := True;
  Editando := False;
  Categoria := varCategoria;
end;

constructor TFrmParametroDetalle.CrearEditando(AOwner: TComponent;
  varNombreParametro, varCategoria: string);
begin
  inherited Create(AOwner);
  Agregando := False;
  Editando := True;
  NombreParametro := varNombreParametro;
  Categoria := varCategoria;
end;

procedure TFrmParametroDetalle.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if (Aceptado) then
  begin
    if (Agregando) then
    begin
      with Parametro do
      begin
        SetNombreParametro(TxtNombreParametro.Text);
        SetCategoria(Categoria);
        SetTipoDato(CmbTipoDato.Text);
        SetValor(TxtValor.Text);
        Insertar;
      end;
    end
    else
    begin
      with Parametro do
      begin
        SetNombreParametro(NombreParametro);
        Buscar;
        SetTipoDato(CmbTipoDato.Text);
        SetValor(TxtValor.Text);
        Modificar;
      end;
    end;
  end;
end;

procedure TFrmParametroDetalle.FormCreate(Sender: TObject);
begin
  Aceptado := False;
  Parametro := TParametro.Create;
  if (Editando) then
  begin
    Parametro.SetNombreParametro(NombreParametro);
    Parametro.Buscar;
    TxtNombreParametro.Text := Parametro.GetNombreParametro;
    CmbTipoDato.ItemIndex := CmbTipoDato.Items.IndexOf(Parametro.GetTipoDato);
    TxtValor.Text := Parametro.ObtenerValorAsString;
  end;
end;

end.
