unit EditarDetalleRadioContratoDistribucionFacturacion;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GenericoEditarDetalle, StdCtrls, Buttons, ExtCtrls, rxCurrEdit, Mask,
  rxToolEdit, DBClient;

type
  TFrmEditarDetalleRadioContratoDistribucionFacturacion = class(TFrmGenericoEditarDetalle)
    LblFechaInicial: TLabel;
    LblFechaFinal: TLabel;
    DatFechaInicial: TDateEdit;
    DatFechaFinal: TDateEdit;
    LblTotal: TLabel;
    TxtTotal: TCurrencyEdit;
    Label1: TLabel;
    DatFecha: TDateEdit;
    TxtObservaciones: TMemo;
    LblObservaciones: TLabel;
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Agregar; override;
    procedure Mover; override;
  public
    { Public declarations }
    constructor CrearDetalle(AOwner: TComponent; DataSet: TClientDataSet); override;
  end;

var
  FrmEditarDetalleRadioContratoDistribucionFacturacion: TFrmEditarDetalleRadioContratoDistribucionFacturacion;

implementation

uses DB;

{$R *.dfm}

{ TFrmEditarDetalleRadioContratoDistribucionFacturacion }

procedure TFrmEditarDetalleRadioContratoDistribucionFacturacion.Agregar;
begin
  with Data do
  begin
    Edit;
    FieldValues['fecha_ingreso'] := DatFecha.Date;
    FieldValues['total'] := TxtTotal.Value;
    FieldValues['fecha_inicial'] := DatFechaInicial.Date;
    FieldValues['fecha_final'] := DatFechaFinal.Date;
    FieldValues['observaciones'] := TxtObservaciones.Text;
    Post;
  end;
end;

constructor TFrmEditarDetalleRadioContratoDistribucionFacturacion.CrearDetalle(
  AOwner: TComponent; DataSet: TClientDataSet);
begin
  inherited CrearDetalle(AOwner,DataSet);
  Caption := Caption + ' [Item ' +  IntToStr(Data.FieldValues['item']) + '/' + IntToStr(Data.FieldValues['total_items']) + ']'; 
end;

procedure TFrmEditarDetalleRadioContratoDistribucionFacturacion.Mover;
begin
  DatFecha.Date := Data.FieldValues['fecha_ingreso'];
  TxtTotal.Value := Data.FieldValues['total'];
  DatFechaInicial.Date := Data.FieldValues['fecha_inicial'];
  DatFechaFinal.Date := Data.FieldValues['fecha_final'];
  TxtObservaciones.Text := Data.FieldValues['observaciones'];
end;

end.
