unit PagoComision;

{$I sophie.inc}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GenericoDetalle, rxCurrEdit, Mask, rxToolEdit, StdCtrls, Buttons,
  RxLookup, DB, DBClient, ImgList, XPMan, ComCtrls, Grids, DBGrids, ExtCtrls,
  ToolWin, Utils, ClsDetPagoComision, ClsVendedor, ClsZona, ClsMoneda,
  ClsPagoComision, ClsBusquedaPagoComision, GenericoBuscador, ClsReporte,
  ClsParametro, ClsDetalleCalculoPagoComision, ClsTipoComision,
  ClsVendedorComision, ClsRadioDetalleCalculoPagoComision
  {$IFDEF DELPHI10}, ImageList{$ENDIF};

type
  TFrmPagoComision = class(TFrmGenericoDetalle)
    TxtNumero: TEdit;
    LblNumero: TLabel;
    LblCodigoVendedor: TLabel;
    CmbCodigoVendedor: TRxDBLookupCombo;
    BtnBuscarVendedor: TBitBtn;
    LblCodigoZona: TLabel;
    CmbCodigoZona: TRxDBLookupCombo;
    BtnBuscarZona: TBitBtn;
    LblFechaIngreso: TLabel;
    DatFechaIngreso: TDateEdit;
    LblFechaPago: TLabel;
    DatFechaPago: TDateEdit;
    LblFechaInicial: TLabel;
    DatFechaInicial: TDateEdit;
    LblFechaFinal: TLabel;
    LblTotal: TLabel;
    TxtTotal: TCurrencyEdit;
    DSVendedor: TDataSource;
    DSZona: TDataSource;
    DatFechaFinal: TDateEdit;
    BtnGenerarComision: TBitBtn;
    LblCodigoMoneda: TLabel;
    CmbCodigoMoneda: TRxDBLookupCombo;
    BtnBuscarMoneda: TBitBtn;
    DSMoneda: TDataSource;
    Datacodigo_tipo_comision: TStringField;
    Datadesde: TStringField;
    Datanumero_documento: TStringField;
    Database_calculo: TFloatField;
    Dataporcentaje_comision: TFloatField;
    Datacantidad: TFloatField;
    Datamonto_comision: TFloatField;
    procedure TBtnEliminarClick(Sender: TObject);
    procedure DataAfterPost(DataSet: TDataSet);
    procedure GridDetalleKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtnGenerarComisionClick(Sender: TObject);
    procedure CmbCodigoVendedorChange(Sender: TObject);
    procedure BtnBuscarZonaClick(Sender: TObject);
    procedure BtnBuscarMonedaClick(Sender: TObject);
    procedure BtnBuscarVendedorClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    Clase: TPagoComision;
    DetPagoComision: TDetPagoComision;
    BuscadorPagoComision: TBusquedaPagoComision;
    DetalleCalculoPagoComision: TDetalleCalculoPagoComision;
    RadioDetalleCalculoPagoComision: TRadioDetalleCalculoPagoComision;
    TipoComision: TTipoComision;
    VendedorComision: TVendedorComision;
    Vendedor: TVendedor;
    Zona: TZona;
    Moneda: TMoneda;
    Reporte: TReporte;
    Parametro: TParametro;
    DataDetalleCalculoPagoComision: TClientDataSet;
    DataVendedorComision: TClientDataSet;
    DataVendedor: TClientDataSet;
    DataZona: TClientDataSet;
    DataMoneda: TClientDataSet;
    DataBuscadorPagoComision: TClientDataSet;
    procedure Agregar;
    procedure Editar;
    procedure Eliminar;
    procedure Guardar;
    procedure Cancelar;
    procedure Refrescar;
    procedure Imprimir;
    procedure Buscar;
    procedure Limpiar;
    procedure Asignar;
    procedure Obtener;
    procedure Mover;
    procedure GenerarComisiones;
    procedure DeterminarComisiones;
    procedure DeterminarComisionesRadio;
    procedure GuardarDetalles;
    procedure CalcularMontosCabecera;
protected
    { Protected declarations }
    function Validar: Boolean; override;
//    procedure Detalle; override;
  public
    { Public declarations }
  end;

var
  FrmPagoComision: TFrmPagoComision;

implementation

uses
  ClsGenericoBD
  {$IFDEF USE_RAVE_REPORTS}
  , DMRR
  {$ENDIF}
  {$IFDEF USE_FAST_REPORTS}
  , DMFR
  {$ENDIF}
  ;

{$R *.dfm}

{ TFrmPagoComision }

procedure TFrmPagoComision.Agregar;
begin
  //Verificando
  Limpiar;
  //Asignando Valores por Defecto
  CmbCodigoZona.Value := Parametro.ObtenerValor('codigo_zona');
  CmbCodigoMoneda.Value := Parametro.ObtenerValor('codigo_moneda');
end;

procedure TFrmPagoComision.Asignar;
begin
  with Clase do
  begin
    TxtNumero.Text := GetNumero;
    CmbCodigoVendedor.KeyValue := GetCodigoVendedor;
    CmbCodigoZona.KeyValue := GetCodigoZona;
    CmbCodigoMoneda.KeyValue := GetCodigoMoneda;
    DatFechaIngreso.Date := GetFechaIngreso;
    DatFechaPago.Date := GetFechaPago;
    DatFechaInicial.Date := GetFechaInicial;
    DatFechaFinal.Date := GetFechaFinal;
    TxtTotal.Value := GetTotal;
    Data.CloneCursor(DetPagoComision.ObtenerNumero(GetNumero),False,True);
    DS.DataSet := Data;
  end;
end;

procedure TFrmPagoComision.BtnBuscarVendedorClick(Sender: TObject);
begin
  Buscador(Self,Vendedor,'codigo','Buscar Vendedor',nil,DataVendedor);
  CmbCodigoVendedor.KeyValue := DataVendedor.FieldValues['codigo'];
end;

procedure TFrmPagoComision.BtnBuscarZonaClick(Sender: TObject);
begin
  Buscador(Self,Zona,'codigo','Buscar Zona',nil,DataZona);
  CmbCodigoZona.KeyValue := DataZona.FieldValues['codigo'];
end;

procedure TFrmPagoComision.BtnBuscarMonedaClick(Sender: TObject);
begin
  Buscador(Self,Moneda,'codigo','Buscar Moneda',nil,DataMoneda);
  CmbCodigoMoneda.KeyValue := DataMoneda.FieldValues['codigo'];
end;

procedure TFrmPagoComision.BtnGenerarComisionClick(Sender: TObject);
begin  
  if (MessageBox(Self.Handle, '�Esta Seguro Que Los Par�metros Son Correctos?', MsgTituloInformacion, MB_ICONQUESTION or MB_YESNO or MB_TOPMOST or MB_TASKMODAL) = mrYes) then
  begin
    GenerarComisiones;  
  end;
end;

procedure TFrmPagoComision.Buscar;
begin
  if (Buscador(Self,BuscadorPagoComision,'numero','Buscar Comision',nil,DataBuscadorPagoComision)) then
  begin
    Clase.SetNumero(DataBuscadorPagoComision.FieldValues['numero']);
    Clase.Buscar;
    Asignar;
  end;
end;

procedure TFrmPagoComision.DataAfterPost(DataSet: TDataSet);
begin
  inherited;
  CalcularMontosCabecera;
end;

procedure TFrmPagoComision.DeterminarComisiones;
var
  BaseCalculo: Currency;
  SumaCantidad: Currency;
  AuxNumeroDocumento: string;
begin
  Screen.Cursor := crHourGlass;
  //Borrando Datos Anteriores
  if (not Data.IsEmpty) then
  begin
    Data.EmptyDataSet;
  end;
  DataVendedorComision := VendedorComision.ObtenerCodigoVendedor(CmbCodigoVendedor.Text);
  DataDetalleCalculoPagoComision := DetalleCalculoPagoComision.ObtenerVendedor(CmbCodigoVendedor.Text,DatFechaInicial.Date,DatFechaFinal.Date);
  DataVendedorComision.First;
  while (not DataVendedorComision.Eof) do
  begin
    TipoComision.SetCodigo(DataVendedorComision.FieldValues['codigo_tipo_comision']);
    TipoComision.Buscar;
    DataDetalleCalculoPagoComision.First;
    AuxNumeroDocumento := DataDetalleCalculoPagoComision.FieldByName('numero').AsString;
    BaseCalculo := 0;
    SumaCantidad := 0;
    while (not DataDetalleCalculoPagoComision.Eof) do
    begin
      if (Length(Trim(TipoComision.GetCodigoDivision)) = 0) and
         (Length(Trim(TipoComision.GetCodigoLinea)) = 0) and
         (Length(Trim(TipoComision.GetCodigoFamilia)) = 0) and
         (Length(Trim(TipoComision.GetCodigoClase)) = 0) and
         (Length(Trim(TipoComision.GetCodigoManufactura)) = 0) then
      begin
        if (not TipoComision.GetComisionCantidad) then //Si la Comision NO es Basada en Cantidades
        begin
          BaseCalculo := BaseCalculo + (DataDetalleCalculoPagoComision.FieldValues['precio'] * DataDetalleCalculoPagoComision.FieldValues['cantidad']);
        end
        else
        begin
          BaseCalculo := TipoComision.GetMontoCantidad;
          SumaCantidad := SumaCantidad + DataDetalleCalculoPagoComision.FieldValues['cantidad'];
        end;
      end
      else //Caso Condicionales
      begin
        if (((DataDetalleCalculoPagoComision.FieldValues['codigo_division'] = TipoComision.GetCodigoDivision) or (Length(Trim(TipoComision.GetCodigoDivision)) = 0)) and
            ((DataDetalleCalculoPagoComision.FieldValues['codigo_linea'] = TipoComision.GetCodigoLinea) or (Length(Trim(TipoComision.GetCodigoLinea)) = 0)) and
            ((DataDetalleCalculoPagoComision.FieldValues['codigo_familia'] = TipoComision.GetCodigoFamilia) or (Length(Trim(TipoComision.GetCodigoFamilia)) = 0)) and
            ((DataDetalleCalculoPagoComision.FieldValues['codigo_clase'] = TipoComision.GetCodigoClase) or (Length(Trim(TipoComision.GetCodigoClase)) = 0)) and
            ((DataDetalleCalculoPagoComision.FieldValues['codigo_manufactura'] = TipoComision.GetCodigoManufactura) or (Length(Trim(TipoComision.GetCodigoManufactura)) = 0))) then
        begin
          if (not TipoComision.GetComisionCantidad) then //Si la Comision NO es Basada en Cantidades
          begin
            BaseCalculo := BaseCalculo + (DataDetalleCalculoPagoComision.FieldValues['precio'] * DataDetalleCalculoPagoComision.FieldValues['cantidad']);
          end
          else
          begin
            BaseCalculo := TipoComision.GetMontoCantidad;
            SumaCantidad := SumaCantidad + DataDetalleCalculoPagoComision.FieldValues['cantidad'];
          end;
        end;
      end;
      DataDetalleCalculoPagoComision.Next;
      if ((DataDetalleCalculoPagoComision.Eof) or (AuxNumeroDocumento <> DataDetalleCalculoPagoComision.FieldByName('numero').AsString)) then
      begin
        if (BaseCalculo > 0) then
        begin
          with Data do
          begin
            Append;
            FieldValues['codigo_tipo_comision'] := TipoComision.GetCodigo;
            FieldValues['desde'] := DataDetalleCalculoPagoComision.FieldValues['desde'];
            FieldValues['numero_documento'] := AuxNumeroDocumento;
            FieldValues['base_calculo'] := BaseCalculo;
            FieldValues['porcentaje_comision'] := TipoComision.GetPorcentaje;
            FieldValues['cantidad'] := SumaCantidad;
            if (DataDetalleCalculoPagoComision.FieldValues['desde'] = 'FC') then
            begin
              if (not TipoComision.GetComisionCantidad) then //Si la Comision NO es Basada en Cantidades
              begin
                FieldValues['monto_comision'] := BaseCalculo * (TipoComision.GetPorcentaje / 100);
              end
              else
              begin
                FieldValues['monto_comision'] := BaseCalculo * SumaCantidad;
              end;
            end
            else
            begin
              if (not TipoComision.GetComisionCantidad) then //Si la Comision NO es Basada en Cantidades
              begin
                FieldValues['monto_comision'] := (BaseCalculo * (TipoComision.GetPorcentaje / 100)) * -1;
              end
              else
              begin
                FieldValues['monto_comision'] := BaseCalculo * SumaCantidad;
              end;
            end;
            Post;
          end;
          if (not DataDetalleCalculoPagoComision.Eof) then
          begin
            AuxNumeroDocumento := DataDetalleCalculoPagoComision.FieldByName('numero').AsString;
            BaseCalculo := 0;
            SumaCantidad := 0;
          end;
        end;
      end;
    end;
    DataVendedorComision.Next;
  end;
  Screen.Cursor := crDefault;
  Application.ProcessMessages;
  CalcularMontosCabecera;
end;

procedure TFrmPagoComision.DeterminarComisionesRadio;
var
  BaseCalculo: Currency;
  AuxNumeroDocumento: string;
begin
  Screen.Cursor := crHourGlass;
  //Borrando Datos Anteriores
  if (not Data.IsEmpty) then
  begin
    Data.EmptyDataSet;
  end;
  DataVendedorComision := VendedorComision.ObtenerCodigoVendedor(CmbCodigoVendedor.Text);
  DataDetalleCalculoPagoComision := RadioDetalleCalculoPagoComision.ObtenerVendedor(CmbCodigoVendedor.Text,DatFechaInicial.Date,DatFechaFinal.Date);
  DataVendedorComision.First;
  while (not DataVendedorComision.Eof) do
  begin
    TipoComision.SetCodigo(DataVendedorComision.FieldValues['codigo_tipo_comision']);
    TipoComision.Buscar;
    DataDetalleCalculoPagoComision.First;
    while (not DataDetalleCalculoPagoComision.Eof) do
    begin
      BaseCalculo := DataDetalleCalculoPagoComision.FieldValues['monto_neto'];
      with Data do
      begin
        Append;
        FieldValues['codigo_tipo_comision'] := TipoComision.GetCodigo;
        FieldValues['desde'] := DataDetalleCalculoPagoComision.FieldValues['desde'];
        FieldValues['numero_documento'] := DataDetalleCalculoPagoComision.FieldValues['numero'];
        FieldValues['base_calculo'] := BaseCalculo;
        FieldValues['porcentaje_comision'] := TipoComision.GetPorcentaje;
        FieldValues['cantidad'] := 0;
        if (DataDetalleCalculoPagoComision.FieldValues['desde'] = 'FC') then
        begin
          FieldValues['monto_comision'] := BaseCalculo * (TipoComision.GetPorcentaje / 100);
        end
        else
        begin
          FieldValues['monto_comision'] := (BaseCalculo * (TipoComision.GetPorcentaje / 100)) * -1;
        end;
        Post;
      end;
      DataDetalleCalculoPagoComision.Next;
    end;
    DataVendedorComision.Next;
  end;
  Screen.Cursor := crDefault;
  Application.ProcessMessages;
  CalcularMontosCabecera;
end;

procedure TFrmPagoComision.Cancelar;
begin
  if (Agregando) then
  begin
    Limpiar;
  end
  else
  begin
    Clase.SetNumero(TxtNumero.Text);
    Clase.Buscar;
    Asignar;
  end;
end;

procedure TFrmPagoComision.CmbCodigoVendedorChange(Sender: TObject);
begin
  SB.Panels[0].Text := DataVendedor.FieldValues['descripcion'];
  CmbCodigoZona.KeyValue := DataVendedor.FieldValues['codigo_zona'];
end;

procedure TFrmPagoComision.Editar;
begin
  if (Length(Trim(TxtNumero.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
end;

procedure TFrmPagoComision.Eliminar;
begin
  //Se Busca El Regitro de Comision!
  Clase.SetNumero(TxtNumero.Text);
  Clase.Buscar;
  //Se Eliminan Los Detalles
  DetPagoComision.EliminarNumero(Clase.GetNumero);
  Clase.Eliminar;
  MessageBox(Self.Handle, 'El Registro Se Ha Eliminado Con Exito!!!',PChar(MsgTituloInformacion), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  Refrescar;
  Limpiar;
end;

procedure TFrmPagoComision.FormCreate(Sender: TObject);
begin
  inherited;
  ProcAgregar := Agregar;
  ProcEditar := Editar;
  ProcEliminar := Eliminar;
  ProcGuardar := Guardar;
  ProcCancelar := Cancelar;
  ProcRefrescar := Refrescar;
  ProcImprimir := Imprimir;
  ProcBuscar := Buscar;
  Clase := TPagoComision.Create;
  //Inicializando DataSets Secundarios
  BuscadorPagoComision := TBusquedaPagoComision.Create;
  DetPagoComision := TDetPagoComision.Create;
  Vendedor := TVendedor.Create;
  Zona := TZona.Create;
  Moneda := TMoneda.Create;
  Reporte := TReporte.Create;
  Parametro := TParametro.Create;
  DetalleCalculoPagoComision := TDetalleCalculoPagoComision.Create;
  RadioDetalleCalculoPagoComision := TRadioDetalleCalculoPagoComision.Create;
  VendedorComision := TVendedorComision.Create;
  TipoComision := TTipoComision.Create;
  DataVendedor := Vendedor.ObtenerCombo;
  DataZona := Zona.ObtenerCombo;
  DataMoneda := Moneda.ObtenerCombo;
  DataBuscadorPagoComision := BuscadorPagoComision.ObtenerLista;
  DataVendedor.First;
  DataZona.First;
  DataMoneda.First;
  DSVendedor.DataSet := DataVendedor;
  DSZona.DataSet := DataZona;
  DSMoneda.DataSet := DataMoneda;
  ConfigurarRxDBLookupCombo(CmbCodigoVendedor,DSVendedor,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoZona,DSZona,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoMoneda,DSMoneda,'codigo','codigo;descripcion');
  DS.DataSet := Data;
  GridDetalle.DataSource := DS;
end;

procedure TFrmPagoComision.GenerarComisiones;
begin
  //Verificando si el Modulo de Radio esta activado
  Parametro.SetNombreParametro('modulo_radio_activado');
  if (Parametro.Buscar) then
  begin
    if (Parametro.ObtenerValor('modulo_radio_activado') = True) then
    begin
      DeterminarComisionesRadio;
      Exit;
    end;
  end;
  DeterminarComisiones;
end;

procedure TFrmPagoComision.GridDetalleKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_INSERT) then
  begin
    Abort;
  end;   
  inherited;
  CalcularMontosCabecera;
end;

procedure TFrmPagoComision.Guardar;
begin
  Obtener;
  if (Agregando) then
  begin
    //Agregando...
    //Guardando Encabezado
    Clase.Insertar;
    TxtNumero.Text := Clase.GetNumero;
    Application.ProcessMessages;
    //Guardando Detalles
    GuardarDetalles;
    MessageBox(Self.Handle, 'El Pago de Comisiones Ha Sido Agregado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    //Se Imprime la Factura
    MessageBox(Self.Handle, 'Presione "Aceptar" Para Proceder A Imprimir El Pago de Comisi�n!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    with Reporte do
    begin
      AgregarParametro('NumeroComision',Clase.GetNumero);
      AgregarParametro('MontoLetra',NumeroALetras(Clase.GetTotal));
      EmiteReporte('PagoComision',trGeneral);
    end;
  end
  else
  begin
    //Editando...
    Clase.Modificar;
    //Actualizando Detalles (Eliminando Anteriores y Cargando los Nuevos)
    DetPagoComision.EliminarNumero(Clase.GetNumero);
    GuardarDetalles;
    MessageBox(Self.Handle, 'El Pago de Comisiones Ha Sido Editado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end;
  CalcularMontosCabecera;
  //Actualizando Dataset de Busqueda
  DataBuscadorPagoComision := BuscadorPagoComision.ObtenerLista;
end;

procedure TFrmPagoComision.GuardarDetalles;
begin
  with Data do
  begin
    DisableControls;
    First;
    while (not Eof) do
    begin
      with DetPagoComision do
      begin
        SetNumero(Clase.GetNumero);
        SetCodigoTipoComision(FieldValues['codigo_tipo_comision']);
        SetDesde(FieldValues['desde']);
        SetNumeroDocumento(FieldValues['numero_documento']);
        SetBaseCalculo(FieldValues['base_calculo']);
        SetPorcentajeComision(FieldValues['porcentaje_comision']);
        SetCantidad(FieldValues['cantidad']);
        SetMontoComision(FieldValues['monto_comision']);
        Insertar;
      end;
      Next;
    end;
    EnableControls;
  end;
end;

procedure TFrmPagoComision.Imprimir;
begin
  if (Length(Trim(TxtNumero.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
  with Reporte do
  begin
    AgregarParametro('NumeroComision',Clase.GetNumero);
    AgregarParametro('MontoLetra',NumeroALetras(Clase.GetTotal));
    EmiteReporte('PagoComision',trGeneral);
  end;
end;

procedure TFrmPagoComision.Limpiar;
begin
  TxtNumero.Text := '';
  CmbCodigoVendedor.KeyValue := '';
  CmbCodigoZona.KeyValue := '';
  CmbCodigoMoneda.KeyValue := '';
  DatFechaIngreso.Date := Date;
  DatFechaPago.Date := Date;
  DatFechaInicial.Date := Date;
  DatFechaFinal.Date := Date;
  TxtTotal.Value := 0;
  //Status Bar
  SB.Panels[0].Text := '';
  Data.EmptyDataSet;
end;

procedure TFrmPagoComision.Mover;
var
  DSTemp: TDataSet;
begin
  if (DS.DataSet.RecordCount > 0) then
  begin
    DSTemp := DS.DataSet;
    with Clase do
    begin
      SetNumero(TxtNumero.Text);
      SetCodigoVendedor(CmbCodigoVendedor.KeyValue);
      SetCodigoZona(CmbCodigoZona.KeyValue);
      SetCodigoMoneda(CmbCodigoMoneda.KeyValue);
      SetFechaIngreso(DatFechaIngreso.Date);
      SetFechaPago(DatFechaPago.Date);
      SetFechaInicial(DatFechaInicial.Date);
      SetFechaFinal(DatFechaFinal.Date);
      SetTotal(TxtTotal.Value);
    end;
    Asignar;
  end
  else
  begin
    Limpiar;
  end;
end;

procedure TFrmPagoComision.Obtener;
begin
  with Clase do
  begin
    SetNumero(TxtNumero.Text);
    SetCodigoVendedor(CmbCodigoVendedor.KeyValue);
    SetCodigoZona(CmbCodigoZona.KeyValue);
    SetCodigoMoneda(CmbCodigoMoneda.KeyValue);
    SetFechaIngreso(DatFechaIngreso.Date);
    SetFechaPago(DatFechaPago.Date);
    SetFechaInicial(DatFechaInicial.Date);
    SetFechaFinal(DatFechaFinal.Date);
    SetTotal(TxtTotal.Value);
  end;
end;

procedure TFrmPagoComision.Refrescar;
begin
  //Actualizando Dataset de Busqueda
  DataBuscadorPagoComision := BuscadorPagoComision.ObtenerLista;
  //Se Actualizan Los demas Componentes
  DataVendedor := Vendedor.ObtenerCombo;
  DataZona := Zona.ObtenerCombo;
  DataMoneda := Moneda.ObtenerCombo;
  DataVendedor.First;
  DataZona.First;
  DataMoneda.First;
  DSVendedor.DataSet := DataVendedor;
  DSZona.DataSet := DataZona;
  DSMoneda.DataSet := DataMoneda;
end;

procedure TFrmPagoComision.TBtnEliminarClick(Sender: TObject);
begin
  if (Length(Trim(TxtNumero.Text)) > 0) then
  begin
    inherited;
  end
  else
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
end;

function TFrmPagoComision.Validar: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  //Validando Vendedor
  if (Length(Trim(CmbCodigoVendedor.Text)) = 0) then
  begin
    ListaErrores.Add('El Vendedor No Puede Estar En Blanco!!!');
  end;
  //Validando Zona
  if (Length(Trim(CmbCodigoZona.Text)) = 0) then
  begin
    ListaErrores.Add('La Zona No Puede Estar En Blanco!!!');
  end;
  //Validando Moneda
  if (Length(Trim(CmbCodigoMoneda.Text)) = 0) then
  begin
    ListaErrores.Add('La Moneda No Puede Estar En Blanco!!!');
  end;
  //Validando Fechas Inicial y Final
  if (DatFechaInicial.Date > DatFechaFinal.Date) then
  begin
    ListaErrores.Add('La Fecha de Inicial Debe Ser Menor a la Fecha Final!!!');
  end;
  //Validando Fecha de Pago
  if ((DatFechaPago.Date < DatFechaInicial.Date) or (DatFechaPago.Date < DatFechaFinal.Date)) then
  begin
    ListaErrores.Add('La Fecha de Pago NO Debe Ser Menor a la Fecha Inicial o la Fecha Final!!!');
  end;
  //Validando Monto
  if (TxtTotal.Value <= 0) then
  begin
    ListaErrores.Add('El Monto Neto Debe Ser Positivo!!!');
  end
  else
  begin
    if (Data.RecordCount = 0) then
    begin
      ListaErrores.Add('El Detalle De La Comisi�n No Puede Esta Vacio!!!');
    end;
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

procedure TFrmPagoComision.CalcularMontosCabecera;
var
  TotalCabecera: Currency;
begin
  TotalCabecera := 0;
  with Data do
  begin
    DisableControls;
    First;
    while (not Eof) do
    begin
      TotalCabecera := TotalCabecera + (FieldValues['monto_comision']);
      Next;
    end;
    EnableControls;
  end;
  //Asignado Montos
  TxtTotal.Value:= TotalCabecera;
end;

end.
