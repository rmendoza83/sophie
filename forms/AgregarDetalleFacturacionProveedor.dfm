inherited FrmAgregarDetalleFacturacionProveedor: TFrmAgregarDetalleFacturacionProveedor
  ClientHeight = 175
  OnCreate = FormCreate
  ExplicitWidth = 350
  ExplicitHeight = 199
  PixelsPerInch = 96
  TextHeight = 13
  object LblCantidad: TLabel [1]
    Left = 11
    Top = 64
    Width = 47
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cantidad:'
  end
  object LblCantidadPedida: TLabel [2]
    Left = 8
    Top = 88
    Width = 50
    Height = 13
    Alignment = taRightJustify
    Caption = 'C. Pedida:'
  end
  object LblCosto: TLabel [3]
    Left = 26
    Top = 112
    Width = 32
    Height = 13
    Alignment = taRightJustify
    Caption = 'Costo:'
  end
  inherited PanelBotones: TPanel
    Top = 135
    TabOrder = 6
    ExplicitTop = 135
  end
  inherited BtnBuscar: TBitBtn
    TabOrder = 1
    OnClick = BtnBuscarClick
  end
  object TxtCantidad: TCurrencyEdit
    Left = 64
    Top = 61
    Width = 75
    Height = 21
    Margins.Left = 4
    Margins.Top = 1
    DecimalPlaces = 4
    DisplayFormat = '#,##0.0000'
    TabOrder = 3
    Value = 1.000000000000000000
    OnKeyDown = TxtCantidadKeyDown
  end
  object TxtDescripcionProducto: TEdit
    Left = 64
    Top = 35
    Width = 249
    Height = 21
    Enabled = False
    TabOrder = 2
    OnKeyDown = TxtCodigoKeyDown
  end
  object TxtCosto: TCurrencyEdit
    Left = 64
    Top = 109
    Width = 100
    Height = 21
    Margins.Left = 4
    Margins.Top = 1
    DecimalPlaces = 4
    DisplayFormat = '#,##0.0000'
    TabOrder = 5
    OnKeyDown = TxtCantidadKeyDown
  end
  object TxtCantidadPedida: TCurrencyEdit
    Left = 64
    Top = 85
    Width = 75
    Height = 21
    Margins.Left = 4
    Margins.Top = 1
    DecimalPlaces = 4
    DisplayFormat = '#,##0.0000'
    TabOrder = 4
    Value = 1.000000000000000000
    OnKeyDown = TxtCantidadKeyDown
  end
end
