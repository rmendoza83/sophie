unit GenericoAgregarDetalle;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, StdCtrls, DBClient, Buttons, ExtCtrls, ClsParametro, Utils;

type
  TFrmGenericoAgregarDetalle = class(TForm)
    LblCodigo: TLabel;
    TxtCodigo: TEdit;
    PanelBotones: TPanel;
    BtnBuscar: TBitBtn;
    Aceptar: TBitBtn;
    BtnCerrar: TBitBtn;
    procedure FormShow(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure TxtCodigoExit(Sender: TObject);
    procedure TxtCodigoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure AceptarClick(Sender: TObject);
    procedure BtnCerrarClick(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    Data: TClientDataSet;
    ComponenteFoco: TWinControl;
    Parametro: TParametro;
    function Buscar: Boolean; virtual; abstract;
    procedure Agregar; virtual;
    procedure Limpiar; virtual;
  public
    { Public declarations }
    constructor CrearDetalle(AOwner: TComponent; DataSet: TClientDataSet);
  end;

var
  FrmGenericoAgregarDetalle: TFrmGenericoAgregarDetalle;

implementation

{$R *.dfm}

procedure TFrmGenericoAgregarDetalle.AceptarClick(Sender: TObject);
begin
  if (TxtCodigo.Visible) and (TxtCodigo.Enabled) and (Length(Trim(TxtCodigo.Text)) = 0) then
  begin
    Abort;
  end;
  Agregar;
end;

procedure TFrmGenericoAgregarDetalle.Agregar;
begin
  Limpiar;
end;

procedure TFrmGenericoAgregarDetalle.BtnCerrarClick(Sender: TObject);
begin
  Close;
end;

constructor TFrmGenericoAgregarDetalle.CrearDetalle(AOwner: TComponent;
  DataSet: TClientDataSet);
begin
  inherited Create(AOwner);
  Data := DataSet;
  Parametro := TParametro.Create;
  //Asignando Decimales por Defecto
  Parametro.SetNombreParametro('numero_decimales');
  if (not Parametro.Buscar) then
  begin
    Parametro.SetCategoria('GENERAL');
    Parametro.SetTipoDato('INTEGER');
    Parametro.SetValor('2');
    Parametro.Insertar;
  end;
  BuscarDisplayFormat(Self,Parametro.ObtenerValorAsInteger);
end;

procedure TFrmGenericoAgregarDetalle.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    VK_ESCAPE:
      Close;
    VK_F3:
      BtnBuscar.Click;
  end;
end;

procedure TFrmGenericoAgregarDetalle.FormShow(Sender: TObject);
begin
  if (TxtCodigo.Enabled) and (TxtCodigo.Visible) then
  begin
    TxtCodigo.SetFocus;
  end;
end;

procedure TFrmGenericoAgregarDetalle.Limpiar;
begin
  TxtCodigo.Text := '';
  if (TxtCodigo.Enabled) and (TxtCodigo.Visible) then
  begin
    TxtCodigo.SetFocus;
  end;
end;

procedure TFrmGenericoAgregarDetalle.TxtCodigoExit(Sender: TObject);
begin
  if (Length(Trim(TxtCodigo.Text)) > 0) then
  begin
    if (Buscar) then
    begin
      if (Assigned(ComponenteFoco)) then
      begin
        if (ComponenteFoco.Enabled) then
        begin
          ComponenteFoco.SetFocus;
        end;
      end;
    end;
  end;
end;

procedure TFrmGenericoAgregarDetalle.TxtCodigoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  case Key of
    VK_RETURN:
      if (Length(Trim(TxtCodigo.Text)) > 0) then
      begin
        if (Buscar) then
        begin
          if (Assigned(ComponenteFoco)) then
          begin
            if (ComponenteFoco.Enabled) then
            begin
              ComponenteFoco.SetFocus;
            end;
          end;
        end;
      end;
  end;
end;

end.
