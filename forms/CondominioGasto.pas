unit CondominioGasto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Generico, XPMan, ImgList, DB, Grids, DBGrids, StdCtrls, Buttons,
  ExtCtrls, ComCtrls, ToolWin, RxLookup, DBCtrls, Mask, rxToolEdit, rxCurrEdit,
  ClsCondominioGasto, DBClient, Utils, ClsCondominioConjuntoAdministracion,
  ClsCondominioTipoGasto, ClsParametro;

type
  TFrmCondominioGasto = class(TFrmGenerico)
    LblCodigo: TLabel;
    LblDescripcion: TLabel;
    LblUsarAlicuotaInmueble: TLabel;
    LblUsarAlicuotaParticular: TLabel;
    LblExtraordinario: TLabel;
    LblMontoFijo: TLabel;
    TxtCodigoConjuntoAdministracion: TDBText;
    TxtCodigo: TEdit;
    TxtDescripcion: TEdit;
    ChkUsarAlicuotaInmueble: TCheckBox;
    ChkUsarAlicuotaParticular: TCheckBox;
    ChkExtraordinario: TCheckBox;
    ChkMontoFijo: TCheckBox;
    CmbCodigoConjuntoAdministracion: TRxDBLookupCombo;
    LblCodigoConjuntoAdministracion: TLabel;
    TxtPAlicuota: TCurrencyEdit;
    LblPAlicuota: TLabel;
    LblMonto: TLabel;
    TxtMonto: TCurrencyEdit;
    DSConjuntoAdministracion: TDataSource;
    LblCodigoTipoGasto: TLabel;
    CmbCodigoTipoGasto: TRxDBLookupCombo;
    TxtCodigoTipoGasto: TDBText;
    DSTipoGasto: TDataSource;
    LblDescripcionLarga: TLabel;
    TxtDescripcionLarga: TMemo;
    procedure FormCreate(Sender: TObject);
    procedure GrdBusquedaCellClick(Column: TColumn);
    procedure ChkUsarAlicuotaParticularClick(Sender: TObject);
    procedure ChkMontoFijoClick(Sender: TObject);
    procedure ChkUsarAlicuotaInmuebleClick(Sender: TObject);
  private
    { Private declarations }
    Clase: TCondominioGasto;
    ConjuntoAdministracion: TCondominioConjuntoAdministracion;
    TipoGasto: TCondominioTipoGasto;
    Parametro: TParametro;
    Data: TClientDataSet;
    DataConjuntoAdministracion: TClientDataSet;
    DataTipoGasto: TClientDataSet;
    procedure Agregar;
    procedure Editar;
    procedure Eliminar;
    procedure Guardar;
    procedure Cancelar;
    procedure Refrescar;
    procedure Imprimir;
    procedure Buscar;
    procedure Limpiar;
    procedure Asignar;
    procedure Obtener;
    procedure Mover;
  protected
    function Validar: Boolean; override;
  public
    { Public declarations }
  end;

var
  FrmCondominioGasto: TFrmCondominioGasto;

implementation

{$R *.dfm}

{ TFrmCondominioGasto }

procedure TFrmCondominioGasto.Agregar;
begin
  //Verificando
  Limpiar;
  CmbCodigoConjuntoAdministracion.Value := Parametro.ObtenerValor('codigo_conjunto_administracion');
  CmbCodigoConjuntoAdministracion.SetFocus;
end;

procedure TFrmCondominioGasto.Asignar;
begin
  with Clase do
  begin
    CmbCodigoConjuntoAdministracion.KeyValue := GetCodigoConjuntoAdministracion;
    TxtCodigo.Text := GetCodigo;
    TxtDescripcion.Text := GetDescripcion;
    TxtDescripcionLarga.Text := GetDescripcionLarga;
    CmbCodigoTipoGasto.KeyValue := GetCodigoTipoGasto;
    ChkUsarAlicuotaInmueble.Checked := GetUsarAlicuotaInmueble;
    ChkUsarAlicuotaParticular.Checked := GetUsarAlicuotaParticular;
    ChkUsarAlicuotaParticularClick(nil);
    TxtPAlicuota.Value := GetPAlicuota;
    ChkMontoFijo.Checked := GetMontoFijo;
    ChkMontoFijoClick(nil);
    TxtMonto.Value := GetMonto;
    ChkExtraordinario.Checked := GetExtraordinario;
  end;
end;

procedure TFrmCondominioGasto.Buscar;
begin
  Screen.Cursor := crSQLWait;
  if (Length(Trim(TxtCriterio.Text)) > 0) then
  begin
    Data := Clase.ObtenerListaCriterio(Trim(TxtCriterio.Text));
  end
  else
  begin
    Data := Clase.ObtenerLista;
  end;
  Data.First;
  DS.DataSet := Data;
  GrdBusqueda.Refresh;
  Screen.Cursor := crDefault;
end;

procedure TFrmCondominioGasto.Cancelar;
begin
  CmbCodigoConjuntoAdministracion.Enabled := True;
  TxtCodigo.Enabled := True;
end;

procedure TFrmCondominioGasto.ChkMontoFijoClick(Sender: TObject);
begin
  LblMonto.Enabled := ChkMontoFijo.Checked;
  TxtMonto.Enabled := ChkMontoFijo.Checked;
  if (not ChkMontoFijo.Checked) then
  begin
    TxtMonto.Value := 0;
  end
  else
  begin
    ChkUsarAlicuotaInmueble.Checked := False;
    ChkUsarAlicuotaParticular.Checked := False;
    ChkUsarAlicuotaParticularClick(nil);
  end;
end;

procedure TFrmCondominioGasto.ChkUsarAlicuotaInmuebleClick(Sender: TObject);
begin
  if (ChkUsarAlicuotaInmueble.Checked) then
  begin
    ChkUsarAlicuotaParticular.Checked := False;
    ChkUsarAlicuotaParticularClick(nil);
    ChkMontoFijo.Checked := False;
    ChkMontoFijoClick(nil);
  end;
end;

procedure TFrmCondominioGasto.ChkUsarAlicuotaParticularClick(Sender: TObject);
begin
  LblPAlicuota.Enabled := ChkUsarAlicuotaParticular.Checked;
  TxtPAlicuota.Enabled := ChkUsarAlicuotaParticular.Checked;
  if (not ChkUsarAlicuotaParticular.Checked) then
  begin
    TxtPAlicuota.Value := 0;
  end
  else
  begin
    ChkUsarAlicuotaInmueble.Checked := False;
    ChkMontoFijo.Checked := False;
    ChkMontoFijoClick(nil);
  end;
end;

procedure TFrmCondominioGasto.Editar;
begin
  //Se Busca el Elemento
  Clase.SetCodigoConjuntoAdministracion(Data.FieldValues['codigo_conjunto_administracion']);
  Clase.SetCodigo(Data.FieldValues['codigo']);
  Clase.Buscar;
  Asignar;
  CmbCodigoConjuntoAdministracion.Enabled := False;
  TxtCodigo.Enabled := False;
  TxtDescripcion.SetFocus;
end;

procedure TFrmCondominioGasto.Eliminar;
begin
  //Se Busca el Elemento
  Clase.SetCodigoConjuntoAdministracion(Data.FieldValues['codigo_conjunto_administracion']);
  Clase.SetCodigo(Data.FieldValues['codigo']);
  Clase.Buscar;
  Clase.Eliminar;
  MessageBox(Self.Handle, 'El Gasto de Condominio Ha Sido Eliminado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  Refrescar;
  Limpiar;
end;

procedure TFrmCondominioGasto.FormCreate(Sender: TObject);
begin
  inherited;
  ProcAgregar := Agregar;
  ProcEditar := Editar;
  ProcEliminar := Eliminar;
  ProcGuardar := Guardar;
  ProcCancelar := Cancelar;
  ProcRefrescar := Refrescar;
  ProcImprimir := Imprimir;
  ProcBuscar := Buscar;
  Clase := TCondominioGasto.Create;
  //Inicializando DataSets Secundarios
  ConjuntoAdministracion := TCondominioConjuntoAdministracion.Create;
  TipoGasto := TCondominioTipoGasto.Create;
  Parametro := TParametro.Create; 
  DataConjuntoAdministracion := ConjuntoAdministracion.ObtenerLista;
  DataConjuntoAdministracion.First;
  DataTipoGasto := TipoGasto.ObtenerLista;
  DataTipoGasto.First;
  DSConjuntoAdministracion.DataSet := DataConjuntoAdministracion;
  DSTipoGasto.DataSet := DataTipoGasto;
  TxtCodigoConjuntoAdministracion.DataSource := DSConjuntoAdministracion;
  TxtCodigoConjuntoAdministracion.DataField := 'descripcion';
  TxtCodigoTipoGasto.DataSource := DSTipoGasto;
  TxtCodigoTipoGasto.DataField := 'descripcion';
  ConfigurarRxDBLookupCombo(CmbCodigoConjuntoAdministracion,DSConjuntoAdministracion,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoTipoGasto,DSTipoGasto,'codigo','codigo;descripcion');
  Buscar;
end;

procedure TFrmCondominioGasto.GrdBusquedaCellClick(Column: TColumn);
begin
  inherited;
  Mover;
end;

procedure TFrmCondominioGasto.Guardar;
begin
  if (Agregando) then
  begin
    //Agregando...
    //Se Busca el Codigo
    Clase.SetCodigoConjuntoAdministracion(CmbCodigoConjuntoAdministracion.KeyValue);
    Clase.SetCodigo(TxtCodigo.Text);
    if (Clase.Buscar) then
    begin
      MessageBox(Self.Handle, 'El Código Ya Existe Para El Conjunto de Administración Seleccionado!!!', PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      TxtCodigo.SetFocus;
      Abort;
    end
    else
    begin
      Obtener;
      Clase.Insertar;
      MessageBox(Self.Handle, 'El Gasto de Condominio Ha Sido Agregado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    end;
  end
  else
  begin
    //Editando...
    Obtener;
    Clase.Modificar;
    MessageBox(Self.Handle, 'El Gasto de Condominio Ha Sido Editado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    CmbCodigoConjuntoAdministracion.Enabled := True;    
    TxtCodigo.Enabled := True;
  end;
  Refrescar;
  Limpiar;
end;

procedure TFrmCondominioGasto.Imprimir;
begin

end;

procedure TFrmCondominioGasto.Limpiar;
begin
  DataConjuntoAdministracion.First;
  CmbCodigoConjuntoAdministracion.KeyValue := '';
  TxtCodigo.Text := '';
  TxtDescripcion.Text := '';
  TxtDescripcionLarga.Text := '';
  DataTipoGasto.First;
  CmbCodigoTipoGasto.KeyValue := '';
  ChkUsarAlicuotaInmueble.Checked := False;
  ChkUsarAlicuotaParticular.Checked := False;
  ChkUsarAlicuotaParticularClick(nil);
  ChkMontoFijo.Checked := False;
  ChkMontoFijoClick(nil);
  ChkExtraordinario.Checked := False;
end;

procedure TFrmCondominioGasto.Mover;
var
  DSTemp: TDataSet;
begin
  if (DS.DataSet.RecordCount > 0) then
  begin
    DSTemp := DS.DataSet;
    with Clase do
    begin
      SetCodigoConjuntoAdministracion(DSTemp.FieldValues['codigo_conjunto_administracion']);
      SetCodigo(DSTemp.FieldValues['codigo']);
      SetDescripcion(DSTemp.FieldValues['descripcion']);
      SetDescripcionLarga(DSTemp.FieldValues['descripcion_larga']);
      SetCodigoTipoGasto(DSTemp.FieldValues['codigo_tipo_gasto']);
      SetUsarAlicuotaInmueble(DSTemp.FieldValues['usar_alicuota_inmueble']);
      SetUsarAlicuotaParticular(DSTemp.FieldValues['usar_alicuota_particular']);
      SetPAlicuota(DSTemp.FieldValues['p_alicuota']);
      SetMontoFijo(DSTemp.FieldValues['monto_fijo']);
      SetMonto(DSTemp.FieldValues['monto']);
      SetExtraordinario(DSTemp.FieldValues['extraordinario']);
    end;
    Asignar;
  end
  else
  begin
    Limpiar;
  end;
end;

procedure TFrmCondominioGasto.Obtener;
begin
  with Clase do
  begin
    SetCodigoConjuntoAdministracion(VarToStr(CmbCodigoConjuntoAdministracion.KeyValue));
    SetCodigo(TxtCodigo.Text);
    SetDescripcion(TxtDescripcion.Text);
    SetDescripcionLarga(TxtDescripcionLarga.Text);
    SetCodigoTipoGasto(VarToStr(CmbCodigoTipoGasto.KeyValue));
    SetUsarAlicuotaInmueble(ChkUsarAlicuotaInmueble.Checked);
    SetUsarAlicuotaParticular(ChkUsarAlicuotaParticular.Checked);
    SetPAlicuota(TxtPAlicuota.Value);
    SetMontoFijo(ChkMontoFijo.Checked);
    SetMonto(TxtMonto.Value);
    SetExtraordinario(ChkExtraordinario.Checked);
  end;
end;

procedure TFrmCondominioGasto.Refrescar;
begin
  TxtCriterio.Text := '';
  Buscar;
end;

function TFrmCondominioGasto.Validar: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  (* Validando Informacion del Gasto de Condominio *)
  //Validando Codigo Conjunto Administracion
  if (Length(Trim(CmbCodigoConjuntoAdministracion.Text)) = 0) then
  begin
    ListaErrores.Add('El Conjunto de Administración No Puede Estar En Blanco!!!');
  end;
  //Validando Codigo
  if (Length(Trim(TxtCodigo.Text)) = 0) then
  begin
    ListaErrores.Add('El Código No Puede Estar En Blanco!!!');
  end;
  //Validando Descripcion
  if (Length(Trim(TxtDescripcion.Text)) = 0) then
  begin
    ListaErrores.Add('La Descripción No Puede Estar En Blanco!!!');
  end;
  //Validando Descripcion Larga
  if (Length(Trim(TxtDescripcionLarga.Text)) = 0) then
  begin
    ListaErrores.Add('La Descripción Larga No Puede Estar En Blanco!!!');
  end;
  //Validando Codigo Tipo de Gasto de Condominio
  if (Length(Trim(CmbCodigoTipoGasto.Text)) = 0) then
  begin
    ListaErrores.Add('El Tipo de Gasto del Condominio No Puede Estar En Blanco!!!');
  end;
  //Validando Alicuota Particular
  if (ChkUsarAlicuotaParticular.Checked) then
  begin
    if (TxtPAlicuota.Value <= 0) then
    begin
      ListaErrores.Add('El Porcentaje de Alicuota Particular No Puede ser Cero (0,0000)!!!');
    end;
  end;
  //Validando Monto Fijo
  if (ChkMontoFijo.Checked) then
  begin
    if (TxtMonto.Value <= 0) then
    begin
      ListaErrores.Add('El Monto Fijo No Puede ser Cero (0,0000)!!!');
    end;
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
