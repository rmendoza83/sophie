unit ReporteTaller;

{$I sophie.inc}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, RxLookup, Mask, rxToolEdit, Buttons, ExtCtrls, RXCtrls,
  DB, DBClient, ClsReporte, ClsUser, DBCtrls, ClsCliente, ClsVendedor,
  ClsProducto;

type
  TFrmReporteTaller = class(TForm)
    GrpListadoReportes: TRadioGroup;
    GrpParametrosReporte: TGroupBox;
    SLinea: TShape;
    LblRangoCliente: TRxLabel;
    LblPeriodo: TRxLabel;
    LblRangoProducto: TRxLabel;
    LblTipoReporte: TRxLabel;
    PanelBotones: TPanel;
    BtnEmitir: TBitBtn;
    BtnInicializar: TBitBtn;
    BtnCerrar: TBitBtn;
    GrpPeriodo: TGroupBox;
    LblFechaAl: TLabel;
    LblFechaDesde: TLabel;
    LblFechaHasta: TLabel;
    DatFechaAl: TDateEdit;
    DatFechaDesde: TDateEdit;
    DatFechaHasta: TDateEdit;
    GrpRangoProducto: TGroupBox;
    LblProductoDesde: TLabel;
    LblProductoHasta: TLabel;
    CmbProductoDesde: TRxDBLookupCombo;
    CmbProductoHasta: TRxDBLookupCombo;
    GrpTipoReporte: TGroupBox;
    CmbTipoReporte: TComboBox;
    GrpRangoCliente: TGroupBox;
    LblClienteDesde: TLabel;
    LblClienteHasta: TLabel;
    CmbClienteDesde: TRxDBLookupCombo;
    CmbClienteHasta: TRxDBLookupCombo;
    GrpRangoVendedor: TGroupBox;
    LblVendedorDesde: TLabel;
    LblVendedorHasta: TLabel;
    CmbVendedorDesde: TRxDBLookupCombo;
    CmbVendedorHasta: TRxDBLookupCombo;
    LblRangoVendedor: TRxLabel;
    DSClienteHasta: TDataSource;
    DSClienteDesde: TDataSource;
    DSProductoDesde: TDataSource;
    DSProductoHasta: TDataSource;
    DSVendedorDesde: TDataSource;
    DSVendedorHasta: TDataSource;
    GrpTipoOrdenamiento: TGroupBox;
    CmbTipoOrdenamiento: TComboBox;
    LblTipoOrdenamiento: TRxLabel;
    procedure GrpListadoReportesClick(Sender: TObject);
    procedure FormDblClick(Sender: TObject);
    procedure LblRangoClienteDblClick(Sender: TObject);
    procedure LblRangoProductoDblClick(Sender: TObject);
    procedure LblRangoVendedorDblClick(Sender: TObject);
    procedure BtnCerrarClick(Sender: TObject);
    procedure BtnInicializarClick(Sender: TObject);
    procedure BtnEmitirClick(Sender: TObject);
    procedure CmbClienteDesdeCloseUp(Sender: TObject);
    procedure CmbClienteDesdeExit(Sender: TObject);
    procedure CmbClienteDesdeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CmbVendedorDesdeCloseUp(Sender: TObject);
    procedure CmbVendedorDesdeExit(Sender: TObject);
    procedure CmbVendedorDesdeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    Reporte: TReporte;
    User: TUser;
    (* Clases Temporales a Utilizar para la Seleccion de Parametros *)
    Cliente: TCliente;
    Vendedor: TVendedor;
    Producto: TProducto;
    (* Fin Clases Temporales *)
    (* Controles Adicionales *)
    DataClienteDesde: TClientDataSet;
    DataClienteHasta: TClientDataSet;
    DataProductoDesde: TClientDataSet;
    DataProductoHasta: TClientDataSet;
    DataVendedorDesde: TClientDataSet;
    DataVendedorHasta: TClientDataSet;
    DataVendedor: TClientDataSet;
    (* Fin Controles Adicionales *)
    procedure SeleccionReporte;
    procedure Inicializar;
    procedure CargarParametros;
    procedure EmitirReporte;
    procedure Deshabilitar;
    function Validar: Boolean;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent; ALoginUsuario: String); reintroduce;
  end;

var
  FrmReporteTaller: TFrmReporteTaller;

implementation

uses
  ActividadSophie,
  GenericoBuscador,
  Utils
  {$IFDEF USE_RAVE_REPORTS}
  , DMRR
  {$ENDIF}
  {$IFDEF USE_FAST_REPORTS}
  , DMFR
  {$ENDIF}
  ;

{$R *.dfm}

{ TFrmReporteTaller }

procedure TFrmReporteTaller.BtnCerrarClick(Sender: TObject);
begin
  Close;
end;

procedure TFrmReporteTaller.BtnEmitirClick(Sender: TObject);
begin
  if (Validar) then
  begin
    if (MessageBox(Self.Handle, '�Esta Seguro Que Los Parametros Estan Correctos?', PChar(MsgTituloInformacion), MB_ICONQUESTION or MB_YESNO or MB_TOPMOST or MB_TASKMODAL) = mrYes) then
    begin
      EmitirReporte;
    end;
  end;
end;

procedure TFrmReporteTaller.BtnInicializarClick(Sender: TObject);
begin
  Inicializar;
end;

procedure TFrmReporteTaller.CargarParametros;
var
  TituloReporte: String;
  TipoReporte: String;
  TipoOrdenamiento: String;
begin
  with Reporte do
  begin
    AgregarParametro('LoginUsuario',User.GetLoginUsuario);
    AgregarParametro('NombreUsuario',User.GetNombres + ', ' + User.GetApellidos);
    AgregarParametro('TituloReporte','');
    AgregarParametro('FechaAl',UpperCase(FormatDateTime('yyyy-mm-dd',DatFechaAl.Date)));
    AgregarParametro('FechaDesde',UpperCase(FormatDateTime('yyyy-mm-dd',DatFechaDesde.Date)));
    AgregarParametro('FechaHasta',UpperCase(FormatDateTime('yyyy-mm-dd',DatFechaHasta.Date)));
    AgregarParametro('Periodo','Desde ' + FormatDateTime('dd/mm/yyyy',DatFechaDesde.Date) + ' Hasta ' + FormatDateTime('dd/mm/yyyy',DatFechaHasta.Date));
  end;
  case GrpListadoReportes.ItemIndex of
    0: //Historial de Vehiculos
    begin
      TituloReporte := 'Historial de Vehiculos';
      TipoReporte := CmbTipoReporte.Text;
      if (CmbClienteDesde.Text = '') AND (CmbClienteHasta.Text = '') then
      begin
        Reporte.AgregarParametro('ClienteDesde','TODOS');
      end
      else
      begin
        Reporte.AgregarParametro('ClienteDesde',CmbClienteDesde.Text + ' - ' + CmbClienteHasta.Text);
      end;      
      Case CmbTipoReporte.ItemIndex of
        0: //Detallado
        begin
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        1: //Resumido
        begin
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
      end;
    end;
    1: //Comisiones Mecanicos
    begin
      TituloReporte := 'Comisiones Mecanicos';
      TipoReporte := CmbTipoReporte.Text;
      if (CmbVendedorDesde.Text = '') then
      begin
        Reporte.AgregarParametro('VendedorDesde','TODOS');
        Reporte.AgregarParametro('VendedorHasta','');
      end
      else
      begin
        Reporte.AgregarParametro('VendedorDesde',CmbVendedorDesde.Text + ' ' + DataVendedorDesde.FieldValues['descripcion']);
        Reporte.AgregarParametro('VendedorHasta',' - ' + CmbVendedorHasta.Text + ' ' + DataVendedorHasta.FieldValues['descripcion']);
      end;
      Case CmbTipoReporte.ItemIndex of
        0: //Detallado
        begin
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        1: //Resumido
        begin
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
      end;
    end;
  end;
  Reporte.AgregarParametro('TituloReporte',TituloReporte);
  Reporte.AgregarParametro('TipoReporte',TipoReporte);
  Reporte.AgregarParametro('TipoOrdenamiento',TipoOrdenamiento);
end;

procedure TFrmReporteTaller.CmbClienteDesdeCloseUp(Sender: TObject);
begin
  CmbClienteHasta.KeyValue := CmbClienteDesde.KeyValue;
end;

procedure TFrmReporteTaller.CmbClienteDesdeExit(Sender: TObject);
begin
  CmbClienteHasta.KeyValue := CmbClienteDesde.KeyValue;
end;

procedure TFrmReporteTaller.CmbClienteDesdeKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if (Key = VK_ESCAPE) then
  begin
    CmbClienteHasta.KeyValue := '';
  end;
end;

procedure TFrmReporteTaller.CmbVendedorDesdeCloseUp(Sender: TObject);
begin
  CmbVendedorHasta.KeyValue := CmbVendedorDesde.KeyValue;
end;

procedure TFrmReporteTaller.CmbVendedorDesdeExit(Sender: TObject);
begin
  CmbVendedorHasta.KeyValue := CmbVendedorDesde.KeyValue;
end;

procedure TFrmReporteTaller.CmbVendedorDesdeKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if (Key = VK_ESCAPE) then
  begin
    CmbVendedorHasta.KeyValue := '';
  end;
end;

constructor TFrmReporteTaller.Create(AOwner: TComponent; ALoginUsuario: String);
begin
  inherited Create(AOwner);
  User := TUser.Create;
  User.SetLoginUsuario(ALoginUsuario);
  User.Buscar;
  FrmActividadSophie.Show;
  FrmActividadSophie.Actividad('Cargando N�cleo...');
  Reporte := TReporte.Create;
  (* Inicializando Clases Temporales *)
  Cliente := TCliente.Create;
  Producto := TProducto.Create;
  Vendedor:= TVendedor.Create;
  (* Inicializando Controles Adicionales *)
  FrmActividadSophie.Actividad('Cargando Clientes...');
  DataClienteDesde := Cliente.ObtenerCombo;
  DataClienteHasta := Cliente.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Productos...');
  //DataProductoDesde := Producto.ObtenerCombo;
  DataProductoHasta := Producto.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Cuentas Vendedores...');
  DataVendedorDesde := Vendedor.ObtenerCombo;
  DataVendedorHasta := Vendedor.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Resto del N�cleo...');
  DSClienteDesde.DataSet := DataClienteDesde;
  DSClienteHasta.DataSet := DataClienteHasta;
  //DSProductoDesde.DataSet := DataProductoDesde;
  DSProductoHasta.DataSet := DataProductoHasta;
  DSVendedorDesde.DataSet := DataVendedorDesde;
  DSVendedorHasta.DataSet := DataVendedorHasta;
  //Configurando RxDBLookupComboBox
  ConfigurarRxDBLookupCombo(CmbClienteDesde,DSClienteDesde,'codigo','codigo;razon_social');
  ConfigurarRxDBLookupCombo(CmbClienteHasta,DSClienteHasta,'codigo','codigo;razon_social');
  ConfigurarRxDBLookupCombo(CmbProductoDesde,DSProductoDesde,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbProductoHasta,DSProductoHasta,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbVendedorDesde,DSVendedorDesde,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbVendedorHasta,DSVendedorHasta,'codigo','codigo;descripcion');
  FrmActividadSophie.Close;
end;

procedure TFrmReporteTaller.Deshabilitar;
begin
  //Periodos
  LblPeriodo.Enabled := False;
  LblFechaAl.Enabled := False; DatFechaAl.Enabled := False;
  LblFechaDesde.Enabled := False; DatFechaDesde.Enabled := False;
  LblFechaHasta.Enabled := False; DatFechaHasta.Enabled := False;
  DatFechaAl.Date := Date;
  DatFechaDesde.Date := Date;
  DatFechaHasta.Date := Date;
  //Rango de Clientes
  LblRangoCliente.Enabled := False;
  LblClienteDesde.Enabled := False; CmbClienteDesde.Enabled := False;
  LblClienteHasta.Enabled := False; CmbClienteHasta.Enabled := False;
  CmbClienteDesde.KeyValue := '';
  CmbClienteHasta.KeyValue := '';
  //Rango de Productos
  LblRangoProducto.Enabled := False;
  LblProductoDesde.Enabled := False; CmbProductoDesde.Enabled := False;
  LblProductoDesde.Enabled := False; CmbProductoHasta.Enabled := False;
  CmbProductoDesde.KeyValue := '';
  CmbProductoHasta.KeyValue := '';
  //Rango de Vendedores
  LblRangoVendedor.Enabled := False;
  LblVendedorDesde.Enabled := False; CmbVendedorDesde.Enabled := False;
  LblVendedorHasta.Enabled := False; CmbVendedorHasta.Enabled := False;
  CmbVendedorDesde.KeyValue := '';
  CmbVendedorHasta.KeyValue := '';
  //Tipo de Reporte
  LblTipoReporte.Enabled := False;
  CmbTipoReporte.Enabled := False;
  CmbTipoReporte.Clear;
  CmbTipoReporte.Items.Clear;
  //Tipo Ordenamiento
  LblTipoOrdenamiento.Enabled := False;
  CmbTipoOrdenamiento.Enabled := False;
  CmbTipoOrdenamiento.Clear;
  CmbTipoOrdenamiento.Items.Clear;
end;

procedure TFrmReporteTaller.EmitirReporte;
var
  NombreReporte: string;
  SQL: TStringList;
  Where: string;
  OrderBy: string;

  function AgregarWhere(Old, New: string): string;
  begin
    if (Length(Trim(Old)) > 0) then
    begin
      Result := Old + ' AND ' + New;
    end
    else
    begin
      Result := New;
    end;
  end;

begin
  Screen.Cursor := crHourGlass;
  SQL := TStringList.Create;
  Where := '';
  OrderBy := '';
  case GrpListadoReportes.ItemIndex of
    0: // Reporte Historial de Vehiculos
    begin
      case CmbTipoReporte.ItemIndex of
        0: // Reporte Detallado
        begin
          if (Validar) then
          begin
            NombreReporte := 'TallerHistorialVehiculoDetallado';
            Where := AgregarWhere(Where, '(fecha_ingreso BETWEEN ' + QuotedStr(FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date))+ ' AND ' + QuotedStr(FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date)) + ')');
            //Considerando Clientes
            if (CmbClienteDesde.Text <> '') and (CmbClienteHasta.Text <> '') then
            begin
              Where := AgregarWhere(Where, '(codigo_cliente BETWEEN ' + QuotedStr(CmbClienteDesde.Text) + ' AND ' + QuotedStr(CmbClienteHasta.Text) + ')');
            end;
            case CmbTipoOrdenamiento.ItemIndex of
              0://Orden de Servicio
              begin
                OrderBy := 'ORDER BY numero';
              end;
              1://Fecha de Servicio
              begin
                OrderBy := 'ORDER BY fecha_ingreso';
              end;
            end;
            SQL.Add('SELECT *');
            SQL.Add('FROM public.v_rep_taller_historial_vehiculo');
            SQL.Add('WHERE ' + Where);
            if (Length(Trim(OrderBy)) > 0) then
            begin
              SQL.Add(OrderBy);
            end;
            Reporte.ModificarSQLDriverDataView('DvrTallerHistorialVehiculo',SQL.Text);
          end;
        end;
        1: // Reporte Resumido
        begin
          if (Validar) then
          begin
            Where := AgregarWhere(Where, '(fecha_ingreso BETWEEN ' + QuotedStr(FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date))+ ' AND ' + QuotedStr(FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date)) + ')');
            //Considerando Clientes
            if (CmbClienteDesde.Text <> '') and (CmbClienteHasta.Text <> '') then
            begin
              Where := AgregarWhere(Where, '(codigo_cliente BETWEEN ' + QuotedStr(CmbClienteDesde.Text) + ' AND ' + QuotedStr(CmbClienteHasta.Text) + ')');
            end;
            SQL.Add('SELECT *');
            SQL.Add('FROM public.v_rep_taller_historial_vehiculo');
            SQL.Add('WHERE ' + Where);
            NombreReporte := 'TallerHistorialVehiculoResumido';
          end;
        end;
      end;
    end;
    1: // Reporte Comisiones Mecanicos
    begin
      case CmbTipoReporte.ItemIndex of
        0: // Reporte Detallado
        begin
          if (Validar) then
          begin
            Case CmbTipoOrdenamiento.ItemIndex of
              0: //Por Pagar
              begin
                NombreReporte := 'TallerPagoComisionXPagarDetallado';
              //Determinando Query del Reporte para todos los Vendedores
                if (CmbVendedorDesde.Text = '') then
                begin
                  SQL.Add('SELECT *');
                  SQL.Add('FROM public.v_rep_taller_comision_x_pagar');
                  SQL.Add('WHERE (cc_fecha_factura BETWEEN ' + QuotedStr(':FechaDesde') + ' AND ' + QuotedStr(':FechaHasta')+')');
                  SQL.Add('ORDER BY cc_codigo_vendedor');
                  Reporte.ModificarSQLDriverDataView('DvrTallerComisionXPagar',SQL.Text);
                end
                else
                begin
                  if (CmbVendedorDesde.Text <> '')then
                  begin
                    SQL.Add('SELECT *');
                    SQL.Add('FROM public.v_rep_taller_comision_x_pagar');
                    SQL.Add('WHERE (cc_fecha_factura BETWEEN ' + QuotedStr(':FechaDesde') + ' AND ' + QuotedStr(':FechaHasta')+')' + ' AND ' + 'cc_codigo_vendedor BETWEEN ' + QuotedStr(CmbVendedorDesde.Text) + ' AND ' + QuotedStr(CmbVendedorHasta.Text));
                    SQL.Add('ORDER BY cc_codigo_vendedor');
                    Reporte.ModificarSQLDriverDataView('DvrTallerComisionXPagar',SQL.Text);
                  end
                end;
              end;
              1: //Pagadas
              begin
                NombreReporte := 'TallerPagoComisionDetallado';
              //Determinando Query del Reporte para todos los Vendedores
                if (CmbVendedorDesde.Text = '') then
                begin
                  SQL.Add('SELECT *');
                  SQL.Add('FROM public.v_rep_pago_comision');
                  SQL.Add('WHERE (fecha_pago BETWEEN ' + QuotedStr(':FechaDesde') + ' AND ' + QuotedStr(':FechaHasta')+')');
                  SQL.Add('ORDER BY codigo_vendedor');
                  Reporte.ModificarSQLDriverDataView('DvrComisionDetallado',SQL.Text);
                end
                else
                begin
                  if (CmbVendedorDesde.Text <> '')then
                  begin
                    SQL.Add('SELECT *');
                    SQL.Add('FROM public.v_rep_pago_comision');
                    SQL.Add('WHERE (fecha_pago BETWEEN ' + QuotedStr(':FechaDesde') + ' AND ' + QuotedStr(':FechaHasta')+')' + ' AND ' + 'codigo_vendedor BETWEEN ' + QuotedStr(CmbVendedorDesde.Text) + ' AND ' + QuotedStr(CmbVendedorHasta.Text));
                    SQL.Add('ORDER BY codigo_vendedor');
                    Reporte.ModificarSQLDriverDataView('DvrComisionDetallado',SQL.Text);
                  end
                end;
              end;
            end;
          end;
        end;
        1: // Reporte Resumido
        begin
          if (Validar) then
          begin
            Case CmbTipoOrdenamiento.ItemIndex of
              0: //Por Pagar
              begin
                NombreReporte := 'TallerPagoComisionXPagarResumido';
              //Determinando Query del Reporte para todos los Vendedores
                if (CmbVendedorDesde.Text = '') then
                begin
                  SQL.Add('SELECT *');
                  SQL.Add('FROM public.v_rep_taller_comision_x_pagar');
                  SQL.Add('WHERE (cc_fecha_factura BETWEEN ' + QuotedStr(':FechaDesde') + ' AND ' + QuotedStr(':FechaHasta')+')');
                  SQL.Add('ORDER BY cc_codigo_vendedor');
                  Reporte.ModificarSQLDriverDataView('DvrPagoComisionXPagar',SQL.Text);
                end
                else
                begin
                  if (CmbVendedorDesde.Text <> '')then
                  begin
                    SQL.Add('SELECT *');
                    SQL.Add('FROM public.v_rep_taller_comision_x_pagar');
                    SQL.Add('WHERE (cc_fecha_factura BETWEEN ' + QuotedStr(':FechaDesde') + ' AND ' + QuotedStr(':FechaHasta')+')' + ' AND ' + 'cc_codigo_vendedor BETWEEN ' + QuotedStr(CmbVendedorDesde.Text) + ' AND ' + QuotedStr(CmbVendedorHasta.Text));
                    SQL.Add('ORDER BY cc_codigo_vendedor');
                    Reporte.ModificarSQLDriverDataView('DvrPagoComisionXPagar',SQL.Text);
                  end
                end;
              end;
              1: //Pagadas
              begin
                NombreReporte := 'TallerPagoComisionResumido';
              //Determinando Query del Reporte para todos los Vendedores
                if (CmbVendedorDesde.Text = '') then
                begin
                  SQL.Add('SELECT *');
                  SQL.Add('FROM public.v_rep_pago_comision');
                  SQL.Add('WHERE (fecha_pago BETWEEN ' + QuotedStr(':FechaDesde') + ' AND ' + QuotedStr(':FechaHasta')+')');
                  SQL.Add('ORDER BY codigo_vendedor');
                  Reporte.ModificarSQLDriverDataView('DvrComisionDetallado',SQL.Text);
                end
                else
                begin
                  if (CmbVendedorDesde.Text <> '')then
                  begin
                    SQL.Add('SELECT *');
                    SQL.Add('FROM public.v_rep_pago_comision');
                    SQL.Add('WHERE (fecha_pago BETWEEN ' + QuotedStr(':FechaDesde') + ' AND ' + QuotedStr(':FechaHasta')+')' + ' AND ' + 'codigo_vendedor BETWEEN ' + QuotedStr(CmbVendedorDesde.Text) + ' AND ' + QuotedStr(CmbVendedorHasta.Text));
                    SQL.Add('ORDER BY codigo_vendedor');
                    Reporte.ModificarSQLDriverDataView('DvrComisionDetallado',SQL.Text);
                  end
                end;
              end;
            end;
          end;
        end;
      end;
    end;
  end;
  CargarParametros;
  if (not Reporte.EmiteReporte(NombreReporte,trGeneral)) then
  begin
    MessageBox(Self.Handle, 'Los Par�metros Seleccionados No Devolvieron Resultados!!!', PChar(MsgTituloInformacion), MB_ICONEXCLAMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end;
  Screen.Cursor := crDefault;
end;

procedure TFrmReporteTaller.FormDblClick(Sender: TObject);
begin
  DatFechaAl.Date := Date;
  DatFechaDesde.Date := Date;
  DatFechaHasta.Date := Date;
end;

procedure TFrmReporteTaller.GrpListadoReportesClick(Sender: TObject);
begin
  SeleccionReporte;
end;

procedure TFrmReporteTaller.Inicializar;
begin
  GrpListadoReportes.ItemIndex := 0;
  GrpListadoReportesClick(nil);
  LblTipoOrdenamiento.Caption := 'Tipo Ordenamiento';
end;

procedure TFrmReporteTaller.LblRangoClienteDblClick(Sender: TObject);
begin
  CmbClienteDesde.KeyValue := '';
  CmbClienteHasta.KeyValue := '';
end;

procedure TFrmReporteTaller.LblRangoProductoDblClick(Sender: TObject);
begin
  CmbProductoDesde.KeyValue := '';
  CmbProductoHasta.KeyValue := '';
end;

procedure TFrmReporteTaller.LblRangoVendedorDblClick(Sender: TObject);
begin
  CmbVendedorDesde.KeyValue := '';
  CmbVendedorHasta.KeyValue := '';
end;

procedure TFrmReporteTaller.SeleccionReporte;
begin
  Deshabilitar;
  //Activacion de Controles Segun Convenga
  DatFechaAl.Date := Date;
  DatFechaDesde.Date := Date;
  DatFechaHasta.Date := Date;
  case GrpListadoReportes.ItemIndex of
    0: //Reporte Historial de Vehiculos
    begin
      LblPeriodo.Enabled := True;
      LblFechaAl.Enabled := False; DatFechaAl.Enabled := False;
      LblFechaDesde.Enabled := True; DatFechaDesde.Enabled := True;
      LblFechaHasta.Enabled := True; DatFechaHasta.Enabled := True;
      LblRangoCliente.Enabled := True;
      LblClienteDesde.Enabled := True; LblClienteHasta.Enabled := True;
      CmbClienteDesde.Enabled := True; CmbClienteHasta.Enabled := True;
      LblRangoProducto.Enabled := False;
      LblProductoDesde.Enabled := False; LblProductoHasta.Enabled := False;
      CmbProductoDesde.Enabled := False; CmbProductoHasta.Enabled := False;
      LblRangoVendedor.Enabled := False;
      LblVendedorDesde.Enabled := False; LblVendedorHasta.Enabled := False;
      CmbVendedorDesde.Enabled := False; CmbVendedorHasta.Enabled := False;
      LblTipoReporte.Enabled := True;
      CmbTipoReporte.Enabled := True;
      CmbTipoReporte.Items.Clear;
      CmbTipoReporte.Items.Add('Detallado');
      CmbTipoReporte.Items.Add('Resumido');
      CmbTipoReporte.ItemIndex := 0;
      LblTipoOrdenamiento.Enabled := True;
      LblTipoOrdenamiento.Caption := 'Tipo Ordenamiento';
      CmbTipoOrdenamiento.Enabled := True;
      CmbTipoOrdenamiento.Items.Clear;
      CmbTipoOrdenamiento.Items.Add('Orden Servicio');
      CmbTipoOrdenamiento.Items.Add('Fecha Servicio');
      CmbTipoOrdenamiento.ItemIndex := 0;
    end;
    1: //Rerporte Comiiones Mecanicos
    begin
      LblPeriodo.Enabled := True;
      LblFechaAl.Enabled := False; DatFechaAl.Enabled := False;
      LblFechaDesde.Enabled := True; DatFechaDesde.Enabled := True;
      LblFechaHasta.Enabled := True; DatFechaHasta.Enabled := True;
      LblRangoCliente.Enabled := False;
      LblClienteDesde.Enabled := False; LblClienteHasta.Enabled := False;
      CmbClienteDesde.Enabled := False; CmbClienteHasta.Enabled := False;
      LblRangoProducto.Enabled := False;
      LblProductoDesde.Enabled := False; LblProductoHasta.Enabled := False;
      CmbProductoDesde.Enabled := False; CmbProductoHasta.Enabled := False;
      LblRangoVendedor.Enabled := True;
      LblVendedorDesde.Enabled := True; LblVendedorHasta.Enabled := True;
      CmbVendedorDesde.Enabled := True; CmbVendedorHasta.Enabled := True;
      LblTipoReporte.Enabled := True;
      CmbTipoReporte.Enabled := True;
      CmbTipoReporte.Items.Clear;
      CmbTipoReporte.Items.Add('Detallado');
      CmbTipoReporte.Items.Add('Resumido');
      CmbTipoReporte.ItemIndex := 0;     
      LblTipoOrdenamiento.Enabled := True;
      LblTipoOrdenamiento.Caption := 'Clase Reporte';
      CmbTipoOrdenamiento.Enabled := True;
      CmbTipoOrdenamiento.Items.Clear;
      CmbTipoOrdenamiento.Items.Add('Por Pagar');
      CmbTipoOrdenamiento.Items.Add('Pagadas');
      CmbTipoOrdenamiento.ItemIndex := 0;
    end;
  end;
  Application.ProcessMessages;
  SetAutoDropDownWidth(CmbTipoReporte);
  SetAutoDropDownWidth(CmbTipoOrdenamiento);
end;

function TFrmReporteTaller.Validar: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  (* Validando Informaci�n de Reportes Talleres *)
  //Validando Fechas
  if (DatFechaDesde.Date > DatFechaHasta.Date) then
  begin
    ListaErrores.Add('La Fecha de Desde debe ser Menor a la Fecha Hasta!!!');
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
