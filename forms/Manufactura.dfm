inherited FrmManufactura: TFrmManufactura
  Caption = 'Sophie - Manufacturas'
  PixelsPerInch = 96
  TextHeight = 13
  inherited Pag: TPageControl
    ActivePage = TabBusqueda
    inherited TabBusqueda: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 24
      ExplicitWidth = 584
      ExplicitHeight = 304
      inherited GrdBusqueda: TDBGrid
        OnCellClick = GrdBusquedaCellClick
      end
    end
    inherited TabDatos: TTabSheet
      object LblDescripcion: TLabel
        Left = 45
        Top = 35
        Width = 68
        Height = 13
        Alignment = taRightJustify
        Caption = 'Descripci'#243'n:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblCodigo: TLabel
        Left = 72
        Top = 11
        Width = 41
        Height = 13
        Alignment = taRightJustify
        Caption = 'C'#243'digo:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object TxtCodigo: TEdit
        Left = 119
        Top = 8
        Width = 150
        Height = 21
        TabOrder = 0
      end
      object TxtDescripcion: TEdit
        Left = 119
        Top = 32
        Width = 150
        Height = 21
        TabOrder = 1
      end
    end
  end
end
