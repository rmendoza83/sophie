unit Main;

{$I sophie.inc}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls, Menus, ImgList, ClsUser, XPMan, DBClient,
  ClsMenu, Utils, ClsParametro, PModule, Clipbrd, ImageList;

type

  TFrmMain = class(TForm)
    SBMain: TStatusBar;
    SBSecond: TStatusBar;
    PanelMain: TPanel;
    MenuPrincipal: TMainMenu;
    ItemMenuPrincipal: TMenuItem;
    ItemMenuCerrarSesion: TMenuItem;
    ItemMenuCambiarContrasena: TMenuItem;
    ItemMenuN1: TMenuItem;
    ItemMenuParametrosSistema: TMenuItem;
    ItemMenuN2: TMenuItem;
    ItemMenuSalir: TMenuItem;
    TimerFecha: TTimer;
    Imagenes: TImageList;
    TemaXP: TXPManifest;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TimerFechaTimer(Sender: TObject);
    procedure ItemMenuCerrarSesionClick(Sender: TObject);
    procedure ItemMenuCambiarContrasenaClick(Sender: TObject);
    procedure ItemMenuSalirClick(Sender: TObject);
    procedure ItemMenuParametrosSistemaClick(Sender: TObject);
  private
    { Private declarations }
    ArrayForm: TArrayString;
    Parametro: TParametro;
    SophieNotify: TSophieNotify;
    procedure ClickMenuPrincipal(Sender: TObject);
    procedure DblClickKeyGenerator(Sender: TObject);
    procedure EliminarItemsMenu(varMenu: TMenuItem);
    procedure CargarMenu;
    function Login: Boolean;
    procedure ReleaseForms;
    procedure IniForms;
  public
    { Public declarations }
    SesionIniciada: Boolean;
  end;

var
  FrmMain: TFrmMain;

implementation

uses
  DMBD,
  CambiarClave,
  ActividadSophie,
  Login,
  GeneradorSerialSophie,
  Parametros,
  ValidarLicencia;

{$R *.dfm}
{$WARN SYMBOL_PLATFORM OFF}

{ TFrmMain }

procedure TFrmMain.CargarMenu;
var
  Item: TMenuItem;
  ItemsGuardados: array of TMenuItem;
  i: Integer;
  ClaseMenu: TMenuSophie;
  Ds: TClientDataSet;
begin
  //Eliminando Los Items Agregados Dinamicamente...
  EliminarItemsMenu(MenuPrincipal.Items);
  ClaseMenu := TMenuSophie.Create;
  Ds := ClaseMenu.ObtenerListaLoginUsuario(P.User.GetLoginUsuario);
  with Ds do
    begin
      First;
      SetLength(ItemsGuardados,NivelProfundidad);
      while (not Eof) do
        begin
          //Creando Menu
          Item := TMenuItem.Create(Self);
          Item.Name := 'Registro' + IntToStr(RecNo);
          Item.Caption := FieldValues['titulo'];
          if (not (FieldValues['texto_ayuda'] = Null)) then
          begin
            Item.Hint := FieldValues['texto_ayuda'];
          end;
          Item.Enabled := FieldValues['activo'];
          Item.Visible := FieldValues['visible'];
          Item.ImageIndex := FieldByName('idx_imagen').AsInteger;
          Item.Tag := FieldValues['id_modulo'];
          i := Length(FieldValues['orden']) div 2;
          if (FieldValues['menu_padre']) then
            begin
              if (i = 1) then
                begin
                  MenuPrincipal.Items.Add(Item);
                end
              else
                begin
                  ItemsGuardados[i - 2].Add(Item);
                end;
              ItemsGuardados[i - 1] := Item;
            end
          else
            begin
              Item.OnClick := ClickMenuPrincipal;
              ItemsGuardados[i - 2].Add(Item);
            end;
          Next;
        end;
      Close;
    end;
end;

procedure TFrmMain.ClickMenuPrincipal(Sender: TObject);
begin
  P.ModuleForm((Sender as TMenuItem).Tag);
end;

procedure TFrmMain.DblClickKeyGenerator(Sender: TObject);
begin
  if (P.InValidUser) then
  begin
    Application.CreateForm(TFrmGeneradorSerialSophie,FrmGeneradorSerialSophie);
    FrmGeneradorSerialSophie.ShowModal;
    FreeAndNil(FrmGeneradorSerialSophie);
  end;  
end;

procedure TFrmMain.EliminarItemsMenu(varMenu: TMenuItem);
var
  i: Integer;
begin
  for i := varMenu.Count - 1 downto 0 do
  begin
    if (varMenu.Items[i].Count > 0) then
      begin
        EliminarItemsMenu(varMenu.Items[i]);
        if (Pos(Cadena('Registro'),varMenu.Items[i].Name) > 0) then
          begin
            varMenu.Items[i].Free;
          end;
      end
    else
      begin
        if (Pos(Cadena('Registro'),varMenu.Items[i].Name) > 0) then
          begin
            varMenu.Items[i].Free;
          end;
      end;
  end;
end;

procedure TFrmMain.IniForms;
begin
  FrmCambiarClave := nil;
  //Configurando Formularios Que No Se Eliminaran en "ReleaseForms"
  SetLength(ArrayForm,2);
  ArrayForm[0] := 'FrmMain';
  ArrayForm[1] := 'FrmActividadSophie';
  {$IFDEF DELPHI2006}
    ThousandSeparator := ',';
    DecimalSeparator := '.';
    TimeAMString := 'AM';
    TimePMString := 'PM';
  {$ELSE}
    {$IFDEF DELPHI10}
      FormatSettings.ThousandSeparator := ',';
      FormatSettings.DecimalSeparator := '.';
      FormatSettings.TimeAMString := 'AM';
      FormatSettings.TimePMString := 'PM';
    {$ENDIF}
  {$ENDIF}
end;

function TFrmMain.Login: Boolean;
var
  xModalResult: TModalResult;
  TempUser: TUser;
begin
  Result := True;
  ReleaseForms;
  //Verificando si el Programa esta corriendo desde Delphi
  if (not CheckDebug) then
  begin
    //Lanzado Ventana de Inicio de Sesion
    FrmLogin := TFrmLogin.Create(Self);
    xModalResult := FrmLogin.ShowModal;
    P.User.SetLoginUsuario(FrmLogin.LoginUsuario);
    P.User.Buscar;
    FreeAndNil(FrmLogin);
    if (xModalResult = mrCancel) then
    begin
      Result := False;
    end;
  end
  else
  begin
    //Determinando cual usuario usara por defecto
    TempUser := TUser.Create;
    //Verificando Reinaldo Mendoza
    if (Pos(Cadena('RAMS'),UpperCase(P.HostName)) > 0) then
    begin
      TempUser.SetLoginUsuario('RM');
      if (TempUser.Buscar) then
      begin
        P.User.SetLoginUsuario(TempUser.GetLoginUsuario);
        P.User.Buscar;
      end
      else
      begin
        TempUser.SetLoginUsuario('rmendoza');
        if (TempUser.Buscar) then
        begin
          P.User.SetLoginUsuario(TempUser.GetLoginUsuario);
          P.User.Buscar;
        end
        else
        begin
          //Show Error Message
          Result := False;
        end;
      end;
    end
    else
    begin
      //Verificando Eliud Duno
      if (Pos(Cadena('DUNO'),UpperCase(P.HostName)) > 0) then
      begin
        TempUser.SetLoginUsuario('ED');
        if (TempUser.Buscar) then
        begin
          P.User.SetLoginUsuario(TempUser.GetLoginUsuario);
          P.User.Buscar;
        end
        else
        begin
          TempUser.SetLoginUsuario('eduno');
          if (TempUser.Buscar) then
          begin
            P.User.SetLoginUsuario(TempUser.GetLoginUsuario);
            P.User.Buscar;
          end
          else
          begin
            //Show Error Message
            Result := False;
          end;
        end;
      end
      else //Desarrollador Invalido
      begin
        Result := False;
      end;
    end;
    TempUser.Free;
  end;
  if (Result) then
  begin
    //Seteando Parametros de la Barra de Estado Principal y Secundaria
    SBMain.Panels[0].Text := Parametro.ObtenerValor('nombre_empresa') + ' [Sesi�n Iniciada Por: ' + P.User.GetLoginUsuario + '(' + P.User.GetNombres + ',' + P.User.GetApellidos + ')]';
    SBSecond.Panels[0].Text := 'Host: ' + P.HostName + ' / IP: ' + P.DireccionIP;
    SBSecond.Panels[1].Text := 'Conexi�n Zeos::' + BD.GetProtocolo;
  end;
end;

procedure TFrmMain.ReleaseForms;
var
  i, j: Integer;
  sw: Boolean;
begin
  for i := (Screen.FormCount - 1) downto 0 do
  begin
    if (Length(Trim(Screen.Forms[i].Name)) > 0) then
    begin
      sw := True;
      for j := Low(ArrayForm) to High(ArrayForm) do
      begin
        if (Screen.Forms[i].Name = ArrayForm[j]) then
        begin
          sw := False;
          Break;
        end;        
      end;
      if (sw) then
      begin
        Screen.Forms[i].Release;
      end;
    end;
  end;
  IniForms;
end;

procedure TFrmMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ReleaseForms;
  TimerFecha.Enabled := False;
  if (not CheckDebug) then
  begin
    SophieNotify.Terminate;
    SophieNotify.WaitFor;
    SophieNotify.Destroy;
  end;
  Action := caFree;
end;

procedure TFrmMain.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if (MessageBox(Self.Handle, PChar('�Esta Seguro que Desea Salir del Sistema?'), PChar(MsgTituloInformacion), MB_ICONQUESTION or MB_YESNO or MB_TOPMOST or MB_TASKMODAL) = mrYes) then
  begin
    CanClose := True;
  end
  else
  begin
    CanClose := False;
  end;               
end;

procedure TFrmMain.FormCreate(Sender: TObject);
var
  PanelKeyGenerator: TPanel;
begin
  Screen.Cursor := crHourGlass;
  Application.CreateForm(TBD,BD);
  Screen.Cursor := crHourGlass;
  Application.CreateForm(TP,P);
  Screen.Cursor := crDefault;
  Parametro := TParametro.Create;
  //Verificando Validacion del Sistema
  if (not CheckDebug) then
  begin
    Application.CreateForm(TFrmValidarLicencia,FrmValidarLicencia);
    if (not FrmValidarLicencia.MXP.IsRegistered) then
    begin
      FrmValidarLicencia.ShowModal;
    end;
    FreeAndNil(FrmValidarLicencia);
  end;
  if (P.SophieValidado or CheckDebug) then
  begin
    if (not CheckDebug) then
    begin
      SophieNotify := TSophieNotify.Create;
      with SophieNotify do
      begin
        Priority := tpHigher;
        FreeOnTerminate := False;
        SetHostName(P.HostName);
        SetIPDirection(P.DireccionIP);
        {$IFDEF DELPHI2006}
        Resume;
        {$ELSE}
          {$IFDEF DELPHI10}
            Start;
          {$ENDIF}
        {$ENDIF}
      end;
    end;
    IniForms;
    if (not Application.Terminated) then
    begin
      if (not Login) then
      begin
        SesionIniciada := False;
        Application.Terminate;
      end
      else
      begin
        P.User.Buscar;
        SesionIniciada := True;
        CargarMenu;
        PanelMain.Align := alClient;
        //Generando Panel Para el Lanzamiento de Generador de Claves
        Randomize;
        PanelKeyGenerator := TPanel.Create(Self);
        PanelKeyGenerator.Name := 'PanelKeyGenerator';
        PanelKeyGenerator.Caption := '';
        PanelKeyGenerator.Width := 24;
        PanelKeyGenerator.Height := 24;
        PanelKeyGenerator.Left := Random(PanelMain.ClientWidth - PanelKeyGenerator.Width);
        PanelKeyGenerator.Top := Random(PanelMain.ClientHeight - PanelKeyGenerator.Height);
        PanelKeyGenerator.Cursor := crSQLWait;
        PanelKeyGenerator.OnDblClick := DblClickKeyGenerator;
        PanelKeyGenerator.Parent := PanelMain;
        if (not P.InValidUser) then
        begin
          PanelKeyGenerator.BevelInner := bvNone;
          PanelKeyGenerator.BevelKind := bkNone;
          PanelKeyGenerator.BevelOuter := bvNone;
        end;
      end;
    end;
  end
  else
  begin
    Application.Terminate;
  end;
end;

procedure TFrmMain.FormResize(Sender: TObject);
var
  Panel: TPanel;
begin
  SBMain.Panels[0].Width := (SBMain.Width * 60) Div 100;
  SBMain.Panels[1].Width := (SBMain.Width * 10) Div 100;
  SBMain.Panels[2].Width := (SBMain.Width * 25) Div 100;
  SBSecond.Panels[0].Width := SBSecond.Width - 200;
  SBSecond.Panels[1].Width := 200;
  //Reconfigurando Posicion del KeyGenerator
  Panel := TPanel(PanelMain.FindChildControl('PanelKeyGenerator'));
  if (Assigned(Panel)) then
  begin
    Panel.Left := Random(PanelMain.ClientWidth - Panel.Width);
    Panel.Top := Random(PanelMain.ClientHeight - Panel.Height);
    if (not P.InValidUser) then
    begin
      Panel.BevelInner := bvNone;
      Panel.BevelKind := bkNone;
      Panel.BevelOuter := bvNone;
    end;
  end;  
end;

procedure TFrmMain.FormShow(Sender: TObject);
begin
  if (SesionIniciada) then
  begin
    WindowState := wsMaximized;
  end;
end;

procedure TFrmMain.TimerFechaTimer(Sender: TObject);
begin
  {$IFDEF DELPHI2006}
    SBMain.Panels[2].Text := FormatDateTime(LongDateFormat + ' hh:nn:ss AMPM',Now);
  {$ELSE}
    {$IFDEF DELPHI10}
      SBMain.Panels[2].Text := FormatDateTime(FormatSettings.LongDateFormat + ' hh:nn:ss AMPM',Now);
    {$ENDIF}
  {$ENDIF}
end;

procedure TFrmMain.ItemMenuCerrarSesionClick(Sender: TObject);
begin
  ReleaseForms;
  WindowState := wsNormal;
  Hide;
  if (not Login) then
    begin
      SesionIniciada := False;
      Application.Terminate;
    end
  else
    begin
      SesionIniciada := True;
      CargarMenu;
      Show;
    end;
end;

procedure TFrmMain.ItemMenuCambiarContrasenaClick(Sender: TObject);
begin
  FrmCambiarClave := TFrmCambiarClave.Create(Self,P.User.GetLoginUsuario);
  FrmCambiarClave.ShowModal;
  FreeAndNil(FrmCambiarClave);
end;

procedure TFrmMain.ItemMenuSalirClick(Sender: TObject);
begin
  Close;
end;

procedure TFrmMain.ItemMenuParametrosSistemaClick(Sender: TObject);
begin
  FrmParametros := TFrmParametros.Create(Self);
  FrmParametros.ShowModal;
  FreeAndNil(FrmParametros);
end;

end.
