unit AnticipoProveedor;

{$I sophie.inc}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GenericoDetalle, DB, DBClient, ImgList, XPMan, ComCtrls, Grids,
  DBGrids, ExtCtrls, ToolWin, rxCurrEdit, Mask, rxToolEdit, StdCtrls, Buttons,
  RxLookup, Utils, ClsAnticipoProveedor, ClsBusquedaAnticipoProveedor,
  ClsProveedor, ClsZona, ClsMoneda, ClsCuentaxPagar, GenericoBuscador,
  ClsReporte, ClsParametro, ClsPagoPagado, ClsFormaPago, ClsOperacionBancaria,
  ClsCuentaBancaria, ClsDocumentoAnulado{$IFDEF DELPHI10}, ImageList{$ENDIF};

type
  TFrmAnticipoProveedor = class(TFrmGenericoDetalle)
    TabConcepto: TTabSheet;
    TabObservacion: TTabSheet;
    TxtConcepto: TRichEdit;
    DSProveedor: TDataSource;
    DSMoneda: TDataSource;
    DSZona: TDataSource;
    TxtObservaciones: TRichEdit;
    LblNumero: TLabel;
    LblCodigoProveedor: TLabel;
    LblCodigoMoneda: TLabel;
    LblCodigoZona: TLabel;
    LblFechaIngreso: TLabel;
    LblRecepcion: TLabel;
    LblFechaContable: TLabel;
    LblFechaLibros: TLabel;
    LblMontoNeto: TLabel;
    LblFormaPago: TLabel;
    LblCuentaBancaria: TLabel;
    LblNumeroDocumento: TLabel;
    LblFechaOperacionBancaria: TLabel;
    LblNumeroFormaPago: TLabel;
    TxtNumero: TEdit;
    CmbCodigoProveedor: TRxDBLookupCombo;
    BtnBuscarProveedor: TBitBtn;
    CmbCodigoMoneda: TRxDBLookupCombo;
    BtnBuscarMoneda: TBitBtn;
    CmbCodigoZona: TRxDBLookupCombo;
    BtnBuscarZona: TBitBtn;
    DatFechaIngreso: TDateEdit;
    DatFechaLibros: TDateEdit;
    DatFechaContable: TDateEdit;
    DatFechaRecepcion: TDateEdit;
    TxtMontoNeto: TCurrencyEdit;
    CmbCodigoFormaPago: TRxDBLookupCombo;
    BtnBuscarFormaPago: TBitBtn;
    CmbCodigoCuentaBancaria: TRxDBLookupCombo;
    TxtNumeroDocumentoBancario: TEdit;
    BtnBuscarCuentaBancaria: TBitBtn;
    DatFechaOperacionBancaria: TDateEdit;
    TxtNumeroFormaPago: TEdit;
    DSFormaPago: TDataSource;
    DSCuentaBancaria: TDataSource;
    procedure TBtnEliminarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CmbCodigoProveedorExit(Sender: TObject);
    procedure CmbCodigoProveedorChange(Sender: TObject);
    procedure BtnBuscarMonedaClick(Sender: TObject);
    procedure BtnBuscarZonaClick(Sender: TObject);
    procedure BtnBuscarProveedorClick(Sender: TObject);
    procedure BtnBuscarFormaPagoClick(Sender: TObject);
    procedure BtnBuscarCuentaBancariaClick(Sender: TObject);
  private
    { Private declarations }
    Clase: TAnticipoProveedor;
    CuentaxPagar: TCuentaxPagar;
    PagoPagado: TPagoPagado;
    BuscadorAnticipoProveedor: TBusquedaAnticipoProveedor;
    Proveedor: TProveedor;
    Zona: TZona;
    Moneda: TMoneda;
    FormaPago: TFormaPago;
    OperacionBancaria: TOperacionBancaria;
    CuentaBancaria: TCuentaBancaria;
    Reporte: TReporte;
    Parametro: TParametro;
    DocumentoAnulado: TDocumentoAnulado;
    DataBuscadorAnticipoProveedor: TClientDataSet;
    DataProveedor: TClientDataSet;
    DataZona: TClientDataSet;
    DataMoneda: TClientDataSet;
    DataFormaPago: TClientDataSet;
    DataCuentaBancaria: TClientDataSet;
    procedure Agregar;
    procedure Editar;
    procedure Eliminar;
    procedure Guardar;
    procedure Cancelar;
    procedure Refrescar;
    procedure Imprimir;
    procedure Buscar;
    procedure Limpiar;
    procedure Asignar;
    procedure Obtener;
    procedure Mover;
    procedure GuardarCuentaxPagar;
    procedure GuardarOperacionesBancarias;
  protected
    { Protected declarations }
    function Validar: Boolean; override;
  public
    { Public declarations }
  end;

var
  FrmAnticipoProveedor: TFrmAnticipoProveedor;

implementation

uses
  ActividadSophie,
  PModule,
  DetalleContadoPago
  {$IFDEF USE_RAVE_REPORTS}
  , DMRR
  {$ENDIF}
  {$IFDEF USE_FAST_REPORTS}
  , DMFR
  {$ENDIF}
  ;

{$R *.dfm}

{ TFrmAnticipoProveedor }

procedure TFrmAnticipoProveedor.Agregar;
begin
  //Verificando
  Limpiar;
  //Asignando Valores por Defecto
  CmbCodigoMoneda.Value := Parametro.ObtenerValor('codigo_moneda');
  CmbCodigoZona.Value := Parametro.ObtenerValor('codigo_zona');
  CmbCodigoProveedor.SetFocus;
end;

procedure TFrmAnticipoProveedor.Asignar;
begin
  with Clase do
  begin
    TxtNumero.Text := GetNumero;
    CmbCodigoProveedor.KeyValue := GetCodigoProveedor;
    CmbCodigoZona.KeyValue := GetCodigoZona;
    CmbCodigoMoneda.KeyValue := GetCodigoMoneda;
    DatFechaIngreso.Date := GetFechaIngreso;
    DatFechaRecepcion.Date := GetFechaRecepcion;
    DatFechaContable.Date := GetFechaContable;
    DatFechaLibros.Date := GetFechaLibros;
    TxtConcepto.Text := GetConcepto;
    TxtObservaciones.Text := GetObservaciones;
    TxtMontoNeto.Value := GetMontoNeto;
    CmbCodigoFormaPago.KeyValue := GetCodigoFormaPago;
    TxtNumeroFormaPago.Text := GetNumeroFormaPago;
    CmbCodigoCuentaBancaria.KeyValue := GetCodigoCuentaBancaria;
    TxtNumeroDocumentoBancario.Text := GetNumeroDocumentoBancario;
    DatFechaOperacionBancaria.Date := GetFechaOperacionBancaria;
    DS.DataSet := Data;
  end;
end;

procedure TFrmAnticipoProveedor.BtnBuscarProveedorClick(Sender: TObject);
begin
  Buscador(Self,Proveedor,'codigo','Buscar Proveedor',nil,DataProveedor);
  CmbCodigoProveedor.KeyValue := DataProveedor.FieldValues['codigo'];
  CmbCodigoProveedorExit(Sender);
end;

procedure TFrmAnticipoProveedor.BtnBuscarCuentaBancariaClick(Sender: TObject);
begin
  Buscador(Self,CuentaBancaria,'codigo','Buscar Cuenta Bancaria',nil,DataCuentaBancaria);
  CmbCodigoCuentaBancaria.KeyValue := DataCuentaBancaria.FieldValues['codigo'];
end;

procedure TFrmAnticipoProveedor.BtnBuscarFormaPagoClick(Sender: TObject);
begin
  Buscador(Self,FormaPago,'codigo','Buscar Forma de Pago',nil,DataFormaPago);
  CmbCodigoFormaPago.KeyValue := DataFormaPago.FieldValues['codigo'];
end;

procedure TFrmAnticipoProveedor.BtnBuscarMonedaClick(Sender: TObject);
begin
  Buscador(Self,Moneda,'codigo','Buscar Moneda',nil,DataMoneda);
  CmbCodigoMoneda.KeyValue := DataMoneda.FieldValues['codigo'];
end;

procedure TFrmAnticipoProveedor.BtnBuscarZonaClick(Sender: TObject);
begin
  Buscador(Self,Zona,'codigo','Buscar Zona',nil,DataZona);
  CmbCodigoZona.KeyValue := DataZona.FieldValues['codigo'];
end;

procedure TFrmAnticipoProveedor.Buscar;
begin
  if (Buscador(Self,BuscadorAnticipoProveedor,'numero','Buscar Anticipo de Proveedor',nil,DataBuscadorAnticipoProveedor)) then
  begin
    Clase.SetNumero(DataBuscadorAnticipoProveedor.FieldValues['numero']);
    Clase.BuscarNumero;
    Asignar;
  end;
end;

procedure TFrmAnticipoProveedor.Cancelar;
begin
  if (Agregando) then
  begin
    Limpiar;
  end
  else
  begin
    Clase.SetNumero(TxtNumero.Text);
    Clase.BuscarNumero;
    Asignar;
  end;
end;

procedure TFrmAnticipoProveedor.CmbCodigoProveedorChange(Sender: TObject);
begin
  inherited;
  SB.Panels[0].Text := DataProveedor.FieldValues['razon_social'];
end;

procedure TFrmAnticipoProveedor.CmbCodigoProveedorExit(Sender: TObject);
begin
  if (Length(Trim(CmbCodigoProveedor.Text)) > 0) then
  begin
    Proveedor.SetCodigo(CmbCodigoProveedor.Value);
    Proveedor.BuscarCodigo;
    CmbCodigoZona.Value := Proveedor.GetCodigoZona;
  end;
end;

procedure TFrmAnticipoProveedor.Editar;
begin
  if (Length(Trim(TxtNumero.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
end;

procedure TFrmAnticipoProveedor.Eliminar;
var
  sw: Boolean;
begin
  //Se Valida Que El Anticipo de Proveedor Pueda Ser Anulado
  sw := False;
  if (Clase.GetIdPago <> -1) then //No Puede Anularse
  begin
    PagoPagado.SetIdPago(Clase.GetIdPago);
    if (PagoPagado.BuscarIdPago) then
    begin
      sw := True;
    end;
  end;
  //Consultando Si Puede Eliminarse El Anticipo de Proveedor
  if (sw = False) then
  begin
    //Se Busca El Anticipo!
    Clase.SetNumero(TxtNumero.Text);
    Clase.BuscarNumero;
    //Verificando Si Se Elimina La Operacion Bancaria
    if (Clase.GetIdOperacionBancaria <> -1) then
    begin
      if (MessageBox(Self.Handle, 'Este Anticipo Tiene una Operaci�n Bancaria Asociada y Ser� Eliminada!!!'+#13+#10+''+#13+#10+'�Esta Seguro que Desea Continuar?', PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_YESNO or MB_TOPMOST or MB_TASKMODAL) = mrYes) then
      begin
        OperacionBancaria.SetIdOperacionBancaria(Clase.GetIdOperacionBancaria);
        OperacionBancaria.BuscarIdOperacionBancaria;
        OperacionBancaria.Eliminar;
        ///////////////////////////////
        // Procesando Eliminacion!!! //
        ///////////////////////////////
        //Se Eliminan Los Detalles Del Pago
        CuentaxPagar.EliminarIdPago(Clase.GetIdPago);
        //Registrando El Documento Anulado
        with DocumentoAnulado do
        begin
          SetDesde('AP');
          SetNumeroDocumento(Clase.GetNumero);
          SetSerie('');
          SetNumeroControl('');
          SetFechaDocumento(Clase.GetFechaIngreso);
          SetCodigoCliente('');
          SetCodigoProveedor(Clase.GetCodigoProveedor);
          SetCodigoZona(Clase.GetCodigoZona);
          SetSubTotal(Clase.GetMontoNeto);
          SetImpuesto(0);
          SetTotal(Clase.GetMontoNeto);
          SetPIva(0);
          SetLoginUsuario(P.User.GetLoginUsuario);
          SetFecha(Date);
          SetHora(Time);
          Insertar;
        end;
        Clase.Eliminar;
        MessageBox(Self.Handle, 'El Anticipo de Proveedor Se Ha Eliminado Con Exito!!!',PChar(MsgTituloInformacion), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
        Refrescar;
        Limpiar;
      end
      else
      begin
        Abort;
      end;
    end
    else
    begin
      ///////////////////////////////
      // Procesando Eliminacion!!! //
      ///////////////////////////////
      //Se Eliminan Los Detalles De la Cobranza
      CuentaxPagar.EliminarIdPago(Clase.GetIdPago);
      //Registrando El Documento Anulado
      with DocumentoAnulado do
      begin
        SetDesde('AP');
        SetNumeroDocumento(Clase.GetNumero);
        SetSerie('');
        SetNumeroControl('');
        SetFechaDocumento(Clase.GetFechaIngreso);
        SetCodigoCliente('');
        SetCodigoProveedor(Clase.GetCodigoProveedor);
        SetCodigoZona(Clase.GetCodigoZona);
        SetSubTotal(Clase.GetMontoNeto);
        SetImpuesto(0);
        SetTotal(Clase.GetMontoNeto);
        SetPIva(0);
        SetLoginUsuario(P.User.GetLoginUsuario);
        SetFecha(Date);
        SetHora(Time);
        Insertar;
      end;
      Clase.Eliminar;
      MessageBox(Self.Handle, 'El Anticipo de Proveedor Se Ha Eliminado Con Exito!!!',PChar(MsgTituloInformacion), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      Refrescar;
      Limpiar;
    end;
  end
  else
  begin
    MessageBox(Self.Handle, 'El Anticipo de Proveedor No Puede Ser Eliminado!!!'+#13+#10+''+#13+#10+'Para Poder Eliminar Un Anticipo de Cliente Se Debe Hacer Desde La Ultima Liquidaci�n Generada Asociada A Este Anticipo!', PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_APPLMODAL);
    Abort;
  end;
end;

procedure TFrmAnticipoProveedor.FormCreate(Sender: TObject);
begin
  inherited;
  ProcAgregar := Agregar;
  ProcEditar := Editar;
  ProcEliminar := Eliminar;
  ProcGuardar := Guardar;
  ProcCancelar := Cancelar;
  ProcRefrescar := Refrescar;
  ProcImprimir := Imprimir;
  ProcBuscar := Buscar;
  FrmActividadSophie.Show;
  FrmActividadSophie.Actividad('Cargando N�cleo...');
  Clase := TAnticipoProveedor.Create;
  //Inicializando DataSets Secundarios
  BuscadorAnticipoProveedor := TBusquedaAnticipoProveedor.Create;
  Proveedor := TProveedor.Create;
  Moneda := TMoneda.Create;
  Zona := TZona.Create;
  FormaPago := TFormaPago.Create;
  OperacionBancaria := TOperacionBancaria.Create;
  CuentaBancaria := TCuentaBancaria.Create;
  Reporte := TReporte.Create;
  Parametro := TParametro.Create;
  DocumentoAnulado := TDocumentoAnulado.Create;
  CuentaxPagar := TCuentaxPagar.Create;
  PagoPagado := TPagoPagado.Create;
  FrmActividadSophie.Actividad('Cargando Proveedores...');
  DataProveedor := Proveedor.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Monedas...');
  DataMoneda := Moneda.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Zonas...');
  DataZona := Zona.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Forma de Pagos...');
  DataFormaPago := FormaPago.ObtenerComboPago;
  FrmActividadSophie.Actividad('Cargando Cuentas Bancarias...');
  DataCuentaBancaria := CuentaBancaria.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Resto del N�cleo...');
  DataBuscadorAnticipoProveedor := BuscadorAnticipoProveedor.ObtenerLista;
  DataProveedor.First;
  DataZona.First;
  DataMoneda.First;
  DataFormaPago.First;
  DataCuentaBancaria.First;
  DataBuscadorAnticipoProveedor.First;
  DSProveedor.DataSet := DataProveedor;
  DSZona.DataSet := DataZona;
  DSMoneda.DataSet := DataMoneda;
  DSFormaPago.DataSet := DataFormaPago;
  DSCuentaBancaria.DataSet := DataCuentaBancaria;
  ConfigurarRxDBLookupCombo(CmbCodigoProveedor,DSProveedor,'codigo','codigo;razon_social');
  ConfigurarRxDBLookupCombo(CmbCodigoZona,DSZona,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoMoneda,DSMoneda,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoFormaPago,DSFormaPago,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoCuentaBancaria,DSCuentaBancaria,'codigo','codigo;entidad');
  DS.DataSet := Data;
  GridDetalle.DataSource := DS;
  FrmActividadSophie.Close;
  TabDetalle.TabVisible := False;
  PageDetalles.ActivePage := TabConcepto;
  //Buscar;
end;

procedure TFrmAnticipoProveedor.Guardar;
begin
  Obtener;
  if (Agregando) then
  begin
    //Agregando...
    //Guardando Encabezado
    Clase.Insertar;
    TxtNumero.Text := Clase.GetNumero;
    //Guardando Cuenta x Pagar
    GuardarCuentaxPagar;
    //Guardando Operaciones Bancarias
    GuardarOperacionesBancarias;
    Application.ProcessMessages;
    MessageBox(Self.Handle, 'El Anticipo a Proveedor Ha Sido Agregado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    //Se Imprime El Anticipo
    MessageBox(Self.Handle, 'Presione "Aceptar" Para Proceder A Imprimir El Anticipo a Proveedor!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    with Reporte do
    begin
      AgregarParametro('NumeroAnticipoProveedor',Clase.GetNumero);
      AgregarParametro('MontoLetra',NumeroALetras(Clase.GetMontoNeto));
      begin
        ImprimeReporte('AnticipoProveedor',trDocumento);
      end;
    end;
  end
  else
  begin
    //Editando...
    Clase.Modificar;
    //Guardando Cuenta x Pagar
    GuardarCuentaxPagar;
    MessageBox(Self.Handle, 'El Anticipo a Proveedor Ha Sido Editado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end;
end;

procedure TFrmAnticipoProveedor.GuardarOperacionesBancarias;
begin
  if (DataFormaPago['operacion_bancaria']) then
  begin
    with OperacionBancaria do
    begin
      SetCodigoCuenta(CmbCodigoCuentaBancaria.Text);
      SetCodigoTipoOperacion(DataFormaPago['codigo_tipo_operacion_bancaria']);
      SetNumero(TxtNumeroDocumentoBancario.Text);
      SetFecha(DatFechaOperacionBancaria.Date);
      SetFechaContable(DatFechaOperacionBancaria.Date);
      SetMonto(Clase.GetMontoNeto);
      SetConcepto('Anticipo a Proveedor Generado N� ' + Clase.GetNumero);
      SetObservaciones('');
      SetDiferido(False);
      SetConciliado(False);
      SetFechaConciliacion(Date);
      SetBeneficiario('');
      SetLoginUsuario(P.User.GetLoginUsuario);
      Insertar;
    end;
    //Actualizando IdOperacionBancaria en Anticipo
    with Clase do
    begin
      SetIdOperacionBancaria(OperacionBancaria.GetIdOperacionBancaria);
      Modificar;
    end;
  end;
end;

procedure TFrmAnticipoProveedor.GuardarCuentaxPagar;
begin
  if (Editando) then
  begin
    CuentaxPagar.EliminarIdPago(Clase.GetIdPago);
  end;
  with CuentaxPagar do //Se Inserta la CuentaxPagar
  begin
    SetDesde('AP');
    SetNumeroDocumento(Clase.GetNumero);
    SetCodigoMoneda(Clase.GetCodigoMoneda);
    SetCodigoProveedor(Clase.GetCodigoProveedor);
    SetCodigoZona(Clase.GetCodigoZona);
    SetFechaIngreso(Clase.GetFechaIngreso);
    SetFechaVencIngreso(Clase.GetFechaIngreso);
    SetFechaRecepcion(Clase.GetFechaRecepcion);
    SetFechaVencRecepcion(Clase.GetFechaRecepcion);
    SetFechaContable(Clase.GetFechaContable);
    SetFechaPago(clase.GetFechaIngreso);
    SetFechaLibros(Clase.GetFechaLibros);
    SetNumeroCuota(-1);
    SetTotalCuotas(-1);
    SetOrdenPago(1);
    SetMontoNeto(Clase.GetMontoNeto);
    SetImpuesto(0);
    SetTotal(Clase.GetMontoNeto);
    SetPIva(0);
    SetIdPago(Clase.GetIdPago);
    SetIdContadoPago(Clase.GetIdContadoPago);
    Insertar;
  end;
end;

procedure TFrmAnticipoProveedor.Imprimir;
begin
  if (Length(Trim(TxtNumero.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
  with Reporte do
  begin
    AgregarParametro('NumeroAnticipoProveedor',Clase.GetNumero);
    AgregarParametro('MontoLetra',NumeroALetras(Clase.GetMontoNeto));
    EmiteReporte('AnticipoProveedor',trDocumento);
  end;
end;

procedure TFrmAnticipoProveedor.Limpiar;
begin
  TxtNumero.Text := '';
  CmbCodigoProveedor.KeyValue := '';
  CmbCodigoZona.KeyValue := '';
  CmbCodigoMoneda.KeyValue := '';
  DatFechaIngreso.Date := Date;
  DatFechaRecepcion.Date := Date;
  DatFechaContable.Date := Date;
  DatFechaLibros.Date := Date;
  TxtConcepto.Text := '';
  TxtObservaciones.Text := '';
  TxtMontoNeto.Value := 0;
  CmbCodigoFormaPago.KeyValue := '';
  TxtNumeroFormaPago.Text := '';
  CmbCodigoCuentaBancaria.KeyValue := '';
  TxtNumeroDocumentoBancario.Text := '';
  DatFechaOperacionBancaria.Date := Date;
  //Status Bar
  SB.Panels[0].Text := '';
end;

procedure TFrmAnticipoProveedor.Mover;
begin

end;

procedure TFrmAnticipoProveedor.Obtener;
begin
  with Clase do
  begin
    SetNumero(TxtNumero.Text);
    SetCodigoProveedor(CmbCodigoProveedor.KeyValue);
    SetCodigoZona(CmbCodigoZona.KeyValue);
    SetCodigoMoneda(CmbCodigoMoneda.KeyValue);
    SetFechaIngreso(DatFechaIngreso.Date);
    SetFechaRecepcion(DatFechaRecepcion.Date);
    SetFechaContable(DatFechaContable.Date);
    SetFechaLibros(DatFechaLibros.Date);
    SetConcepto(TxtConcepto.Text);
    SetObservaciones(TxtObservaciones.Text);
    SetMontoNeto(TxtMontoNeto.Value);
    SetLoginUsuario(P.User.GetLoginUsuario);
    SetCodigoFormaPago(CmbCodigoFormaPago.Text);
    SetNumeroFormaPago(TxtNumeroFormaPago.Text);
    SetCodigoCuentaBancaria(CmbCodigoCuentaBancaria.Text);
    SetNumeroDocumentoBancario(TxtNumeroDocumentoBancario.Text);
    SetFechaOperacionBancaria(DatFechaOperacionBancaria.Date);
    if (Agregando) then
    begin
      SetTiempoIngreso(Now);
      SetIdAnticipoProveedor(-1);
      SetIdPago(-1);
      SetIdContadoPago(-1);
      SetIdOperacionBancaria(-1);
    end;
  end;
end;

procedure TFrmAnticipoProveedor.Refrescar;
begin
  //Se Actualiza El DataBuscador
  DataBuscadorAnticipoProveedor := BuscadorAnticipoProveedor.ObtenerLista;
  //Se Actualizan Los demas Componentes
  DataProveedor := Proveedor.ObtenerCombo;
  DataMoneda := Moneda.ObtenerCombo;
  DataZona := Zona.ObtenerCombo;
  DataFormaPago := FormaPago.ObtenerComboPago;
  DataCuentaBancaria := CuentaBancaria.ObtenerCombo;
  DataProveedor.First;
  DataZona.First;
  DataMoneda.First;
  DataFormaPago.First;
  DataCuentaBancaria.First;
  DataBuscadorAnticipoProveedor.First;
  DSProveedor.DataSet := DataProveedor;
  DSZona.DataSet := DataZona;
  DSMoneda.DataSet := DataMoneda;
  DSFormaPago.DataSet := DataFormaPago;
  DSCuentaBancaria.DataSet := DataCuentaBancaria;
end;

procedure TFrmAnticipoProveedor.TBtnEliminarClick(Sender: TObject);
begin
  if (Length(Trim(TxtNumero.Text)) > 0) then
  begin
    inherited;
  end
  else
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
end;

function TFrmAnticipoProveedor.Validar: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  //Validando Proveedor
  if (Length(Trim(CmbCodigoProveedor.Text)) = 0) then
  begin
    ListaErrores.Add('El Proveedor No Puede Estar En Blanco!!!');
  end;
  //Validando Fechas
  if ((DatFechaIngreso.Date > DatFechaRecepcion.Date) or (DatFechaIngreso.Date > DatFechaContable.Date) or (DatFechaIngreso.Date > DatFechaLibros.Date)) then
  begin
    ListaErrores.Add('La Fecha de Ingreso Debe Ser Menor a las Fechas de Recepcion, Contable y Libros Respectivamente!!!');
  end;
  //Validando Concepto
  if (Length(Trim(TxtConcepto.Text)) = 0) then
  begin
    ListaErrores.Add('El Concepto No Puede Estar En Blanco!!!');
  end;
  if (TxtMontoNeto.Value <= 0) then
  begin
    ListaErrores.Add('El Monto Neto Debe Ser Positivo!!!');
  end;
  //Validando Operacion Bancaria
  if DataFormaPago.FieldValues['operacion_bancaria']  then
  begin
    //Validando Cuenta Bancaria
    if (Length(Trim(CmbCodigoCuentaBancaria.Text)) = 0) then
    begin
      ListaErrores.Add('La Cuenta Bancaria No Puede Estar En Blanco!!!');
    end;
    //Validando Numero Documento
    if (Length(Trim(TxtNumeroDocumentoBancario.Text)) = 0) then
    begin
      ListaErrores.Add('El Numero de Documento No Puede Estar En Blanco!!!');
    end;
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
