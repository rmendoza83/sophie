inherited FrmEditarDetalleRadioContratoDistribucionFacturacion: TFrmEditarDetalleRadioContratoDistribucionFacturacion
  Caption = 'Sophie - Editar Distribuci'#243'n Factura'
  ClientHeight = 256
  ExplicitHeight = 280
  PixelsPerInch = 96
  TextHeight = 13
  object LblFechaInicial: TLabel [0]
    Left = 39
    Top = 64
    Width = 73
    Height = 13
    Alignment = taRightJustify
    Caption = 'Fecha Inicial:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object LblFechaFinal: TLabel [1]
    Left = 47
    Top = 88
    Width = 65
    Height = 13
    Alignment = taRightJustify
    Caption = 'Fecha Final:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object LblTotal: TLabel [2]
    Left = 83
    Top = 40
    Width = 29
    Height = 13
    Alignment = taRightJustify
    Caption = 'Total'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label1: TLabel [3]
    Left = 76
    Top = 16
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'Fecha:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object LblObservaciones: TLabel [4]
    Left = 26
    Top = 112
    Width = 86
    Height = 13
    Alignment = taRightJustify
    Caption = 'Observaciones:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  inherited PanelBotones: TPanel
    Top = 216
    TabOrder = 5
  end
  object DatFechaInicial: TDateEdit
    Left = 119
    Top = 61
    Width = 120
    Height = 21
    NumGlyphs = 2
    TabOrder = 2
  end
  object DatFechaFinal: TDateEdit
    Left = 119
    Top = 85
    Width = 120
    Height = 21
    NumGlyphs = 2
    TabOrder = 3
  end
  object TxtTotal: TCurrencyEdit
    Left = 119
    Top = 37
    Width = 120
    Height = 21
    Margins.Left = 4
    Margins.Top = 1
    AutoSelect = False
    DecimalPlaces = 4
    DisplayFormat = '#,##0.0000'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
  end
  object DatFecha: TDateEdit
    Left = 119
    Top = 13
    Width = 120
    Height = 21
    NumGlyphs = 2
    TabOrder = 0
  end
  object TxtObservaciones: TMemo
    Left = 119
    Top = 109
    Width = 217
    Height = 100
    Lines.Strings = (
      '')
    TabOrder = 4
  end
end
