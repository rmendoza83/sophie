unit Producto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Generico, XPMan, ImgList, DB, Grids, DBGrids, StdCtrls, Buttons,
  ExtCtrls, ComCtrls, ToolWin, DBCtrls, RxLookup, Mask, rxToolEdit, rxCurrEdit,
  ClsProducto, ClsDivision, ClsLinea, ClsFamilia, ClsClase, ClsManufactura,
  ClsArancel, ClsUnidad, Utils, DBClient, ImageList, DMBD;

type
  TFrmProducto = class(TFrmGenerico)
    TxtCodigo: TEdit;
    LblCodigo: TLabel;
    LblReferencia: TLabel;
    TxtReferencia: TEdit;
    GrpCostos: TGroupBox;
    GrpIVA: TGroupBox;
    GrpClasificaciones: TGroupBox;
    TxtPeso: TCurrencyEdit;
    TxtVolumen: TCurrencyEdit;
    LblDescripcion: TLabel;
    TxtDescripcion: TEdit;
    LblUbicacion: TLabel;
    TxtUbicacion: TEdit;
    LblPeso: TLabel;
    LblVolumen: TLabel;
    LblDivision: TLabel;
    CmbCodigoDivision: TRxDBLookupCombo;
    BtnBuscarDivision: TBitBtn;
    TxtDescripcionDivision: TDBText;
    LblLinea: TLabel;
    CmbCodigoLinea: TRxDBLookupCombo;
    BtnBuscarLinea: TBitBtn;
    TxtDescripcionLinea: TDBText;
    LblFamilia: TLabel;
    CmbCodigoFamilia: TRxDBLookupCombo;
    BtnBuscarFamilia: TBitBtn;
    TxtDescripcionFamilia: TDBText;
    LblManufactura: TLabel;
    CmbCodigoClase: TRxDBLookupCombo;
    BtnBuscarClase: TBitBtn;
    TxtDescripcionClase: TDBText;
    LblClase: TLabel;
    CmbCodigoManufactura: TRxDBLookupCombo;
    BtnBuscarManufactura: TBitBtn;
    TxtDescripcionManufactura: TDBText;
    LblCodigoArancel: TLabel;
    CmbCodigoArancel: TRxDBLookupCombo;
    BtnBuscarArancel: TBitBtn;
    TxtDescripcionArancel: TDBText;
    LblUnidad: TLabel;
    CmbCodigoUnidad: TRxDBLookupCombo;
    BtnBuscarUnidad: TBitBtn;
    TxtDescripcionUnidad: TDBText;
    LblCostoAnterior: TLabel;
    TxtCostoAnterior: TCurrencyEdit;
    TxtCostoActual: TCurrencyEdit;
    LblCostoActual: TLabel;
    TxtCostoFOB: TCurrencyEdit;
    LblCostoFOB: TLabel;
    TxtCostoAnteriorUS: TCurrencyEdit;
    LblCostoAnteriorUS: TLabel;
    TxtCostoActualUS: TCurrencyEdit;
    LblCostoActualUS: TLabel;
    ChkInventario: TCheckBox;
    LblFechaIngreso: TLabel;
    LblFechaUltimaCompra: TLabel;
    TxtUniqueID: TLabel;
    LblPIVAVenta: TLabel;
    TxtPIVAVenta: TCurrencyEdit;
    LblPIVACompra: TLabel;
    TxtPIVACompra: TCurrencyEdit;
    TxtPrecio: TCurrencyEdit;
    LblPrecio: TLabel;
    LblPDescuento: TLabel;
    TxtPDescuento: TCurrencyEdit;
    TxtFechaIngreso: TLabel;
    TxtFechaUltimaCompra: TLabel;
    DSDivision: TDataSource;
    DSLinea: TDataSource;
    DSFamilia: TDataSource;
    DSClase: TDataSource;
    DSManufactura: TDataSource;
    DSArancel: TDataSource;
    DSUnidad: TDataSource;
    procedure GrdBusquedaCellClick(Column: TColumn);
    procedure FormCreate(Sender: TObject);
    procedure BtnBuscarUnidadClick(Sender: TObject);
    procedure BtnBuscarArancelClick(Sender: TObject);
    procedure BtnBuscarManufacturaClick(Sender: TObject);
    procedure BtnBuscarClaseClick(Sender: TObject);
    procedure BtnBuscarFamiliaClick(Sender: TObject);
    procedure BtnBuscarLineaClick(Sender: TObject);
    procedure BtnBuscarDivisionClick(Sender: TObject);
  private
    { Private declarations }
    Clase: TProducto;
    Division: TDivision;
    Linea: TLinea;
    Familia: TFamilia;
    ClaseClase: TClase;
    Manufactura: TManufactura;
    Arancel: TArancel;
    Unidad: TUnidad;
    Data: TClientDataSet;
    DataDivision: TClientDataSet;
    DataLinea: TClientDataSet;
    DataFamilia: TClientDataSet;
    DataClase: TClientDataSet;
    DataManufactura: TClientDataSet;
    DataArancel: TClientDataSet;
    DataUnidad: TClientDataSet;
    MaxRegistros: Integer;
    procedure Agregar;
    procedure Editar;
    procedure Eliminar;
    procedure Guardar;
    procedure Cancelar;
    procedure Refrescar;
    procedure Imprimir;
    procedure Buscar;
    procedure Limpiar;
    procedure Asignar;
    procedure Obtener;
    procedure Mover;
  protected
    function Validar: Boolean; override;
  public
    { Public declarations }
  end;

var
  FrmProducto: TFrmProducto;

implementation

uses
  GenericoBuscador;

{$R *.dfm}

{ TFrmProducto }

procedure TFrmProducto.Agregar;
begin
  //Verificando
  Limpiar;
  //Asignando Valores por Defecto
  TxtPIVAVenta.Value := Parametro.ObtenerValor('p_iva');
  TxtPIVACompra.Value := Parametro.ObtenerValor('p_iva');
  TxtCodigo.SetFocus
end;

procedure TFrmProducto.Asignar;
begin
  with Clase do
  begin
    TxtCodigo.Text := GetCodigo;
    TxtReferencia.Text := GetReferencia;
    TxtUniqueID.Caption := IntToStr(GetUniqueId);
    TxtDescripcion.Text := GetDescripcion;
    TxtPrecio.Value := GetPrecio;
    TxtPDescuento.Value := GetPDescuento;
    TxtPIVAVenta.Value := GetPIvaVenta;
    TxtPIVACompra.Value := GetPIvaCompra;
    CmbCodigoDivision.KeyValue := GetCodigoDivision;
    CmbCodigoLinea.KeyValue := GetCodigoLinea;
    CmbCodigoFamilia.KeyValue := GetCodigoFamilia;
    CmbCodigoClase.KeyValue := GetCodigoClase;
    CmbCodigoManufactura.KeyValue := GetCodigoManufactura;
    CmbCodigoArancel.KeyValue := GetCodigoArancel;
    CmbCodigoUnidad.KeyValue := GetCodigoUnidad;
    TxtUbicacion.Text := GetUbicacion;
    ChkInventario.Checked := GetInventario;
    TxtCostoAnterior.Value := GetCostoAnterior;
    TxtCostoActual.Value := GetCostoActual;
    TxtCostoFOB.Value := GetCostoFob;
    TxtCostoAnteriorUS.Value := GetUsCostoAnterior;
    TxtCostoActualUS.Value := GetUsCostoActual;
    TxtPeso.Value := GetPeso;
    TxtVolumen.Value := GetVolumen;
    TxtFechaIngreso.Caption := FormatDateTime('dd/mm/yyyy',GetFechaIngreso);
    TxtFechaUltimaCompra.Caption := FormatDateTime('dd/mm/yyyy', GetFechaUCompra);
    DataDivision.Locate('codigo',CmbCodigoDivision.KeyValue,[loCaseInsensitive]);
    DataLinea.Locate('codigo',CmbCodigoLinea.KeyValue,[loCaseInsensitive]);
    DataFamilia.Locate('codigo',CmbCodigoFamilia.KeyValue,[loCaseInsensitive]);
    DataClase.Locate('codigo',CmbCodigoClase.KeyValue,[loCaseInsensitive]);
    DataManufactura.Locate('codigo',CmbCodigoManufactura.KeyValue,[loCaseInsensitive]);
    DataArancel.Locate('codigo',CmbCodigoArancel.KeyValue,[loCaseInsensitive]);
    DataUnidad.Locate('codigo',CmbCodigoUnidad.KeyValue,[loCaseInsensitive]);
  end;
end;

procedure TFrmProducto.BtnBuscarArancelClick(Sender: TObject);
begin
  Buscador(Self,Arancel,'codigo','Busqueda de Aranceles',nil,DataArancel);
  CmbCodigoArancel.KeyValue := DataArancel.FieldValues['codigo'];
end;

procedure TFrmProducto.BtnBuscarClaseClick(Sender: TObject);
begin
  Buscador(Self,ClaseClase,'codigo','Busqueda de Clases',nil,DataClase);
  CmbCodigoClase.KeyValue := DataClase.FieldValues['codigo'];
end;

procedure TFrmProducto.BtnBuscarDivisionClick(Sender: TObject);
begin
  Buscador(Self,Division,'codigo','Busqueda de Divisiones',nil,DataDivision);
  CmbCodigoDivision.KeyValue := DataDivision.FieldValues['codigo'];
end;

procedure TFrmProducto.BtnBuscarFamiliaClick(Sender: TObject);
begin
  Buscador(Self,Familia,'codigo','Busqueda de Familias',nil,DataFamilia);
  CmbCodigoFamilia.KeyValue := DataFamilia.FieldValues['codigo'];
end;

procedure TFrmProducto.BtnBuscarLineaClick(Sender: TObject);
begin
  Buscador(Self,Linea,'codigo','Busqueda de Lineas',nil,DataLinea);
  CmbCodigoLinea.KeyValue := DataLinea.FieldValues['codigo'];
end;

procedure TFrmProducto.BtnBuscarManufacturaClick(Sender: TObject);
begin
  Buscador(Self,Manufactura,'codigo','Busqueda de Manufacturas',nil,DataManufactura);
  CmbCodigoManufactura.KeyValue := DataManufactura.FieldValues['codigo'];
end;

procedure TFrmProducto.BtnBuscarUnidadClick(Sender: TObject);
begin
  Buscador(Self,Unidad,'codigo','Busqueda de Unidades',nil,DataUnidad);
  CmbCodigoUnidad.KeyValue := DataUnidad.FieldValues['codigo'];
end;

procedure TFrmProducto.Buscar;
begin
  Screen.Cursor := crSQLWait;
  if (Length(Trim(TxtCriterio.Text)) > 0) then
  begin
    Data := Clase.ObtenerListaCriterio(Trim(TxtCriterio.Text));
  end
  else
  begin
    Data := Clase.ObtenerLista;
  end;
  Data.First;
  DS.DataSet := Data;
  GrdBusqueda.Refresh;
  Screen.Cursor := crDefault;
end;

procedure TFrmProducto.Cancelar;
begin
  TxtCodigo.Enabled := True;
end;

procedure TFrmProducto.Editar;
begin
  //Se Busca el Elemento
  Clase.SetCodigo(Data.FieldValues['codigo']);
  Clase.Buscar;
  Asignar;
  TxtCodigo.Enabled := False;
end;

procedure TFrmProducto.Eliminar;
begin
  //Se Busca el Elemento
  Clase.SetCodigo(Data.FieldValues['codigo']);
  Clase.Buscar;
  Clase.Eliminar;
  MessageBox(Self.Handle, 'El Producto Se Ha Eliminado Con Exito!!!',PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  Refrescar;
  Limpiar;
end;

procedure TFrmProducto.FormCreate(Sender: TObject);
begin
  inherited;
  ProcAgregar := Agregar;
  ProcEditar := Editar;
  ProcEliminar := Eliminar;
  ProcGuardar := Guardar;
  ProcCancelar := Cancelar;
  ProcRefrescar := Refrescar;
  ProcImprimir := Imprimir;
  ProcBuscar := Buscar;
  Clase := TProducto.Create;
  //Inicializando DataSets Secundarios
  Division := TDivision.Create;
  Linea := TLinea.Create;
  Familia := TFamilia.Create;
  ClaseClase := TClase.Create;
  Manufactura := TManufactura.Create;
  Arancel := TArancel.Create;
  Unidad := TUnidad.Create;
  DataDivision := Division.ObtenerLista;
  DataLinea := Linea.ObtenerLista;
  DataFamilia := Familia.ObtenerLista;
  DataClase := ClaseClase.ObtenerLista;
  DataManufactura := Manufactura.ObtenerLista;
  DataArancel := Arancel.ObtenerLista;
  DataUnidad := Unidad.ObtenerLista;
  DataDivision.First;
  DataLinea.First;
  DataFamilia.First;
  DataClase.First;
  DataManufactura.First;
  DataArancel.First;
  DataUnidad.First;
  DSDivision.DataSet := DataDivision;
  DSLinea.DataSet := DataLinea;
  DSFamilia.DataSet := DataFamilia;
  DSClase.DataSet := DataClase;
  DSManufactura.DataSet := DataManufactura;
  DSArancel.DataSet := DataArancel;
  DSUnidad.DataSet := DataUnidad;   
  TxtDescripcionDivision.DataSource := DSDivision;
  TxtDescripcionDivision.DataField := 'descripcion';
  CmbCodigoDivision.LookupSource := DSDivision;
  CmbCodigoDivision.LookupField := 'codigo';
  CmbCodigoDivision.LookupDisplay := 'codigo;descripcion';
  TxtDescripcionLinea.DataSource := DSLinea;
  TxtDescripcionLinea.DataField := 'descripcion';
  CmbCodigoLinea.LookupSource := DSLinea;
  CmbCodigoLinea.LookupField := 'codigo';
  CmbCodigoLinea.LookupDisplay := 'codigo;descripcion';
  TxtDescripcionFamilia.DataSource := DSFamilia;
  TxtDescripcionFamilia.DataField := 'descripcion';
  CmbCodigoFamilia.LookupSource := DSFamilia;
  CmbCodigoFamilia.LookupField := 'codigo';
  CmbCodigoFamilia.LookupDisplay := 'codigo;descripcion';
  TxtDescripcionClase.DataSource := DSClase;
  TxtDescripcionClase.DataField := 'descripcion';
  CmbCodigoClase.LookupSource := DSClase;
  CmbCodigoClase.LookupField := 'codigo';
  CmbCodigoClase.LookupDisplay := 'codigo;descripcion';
  TxtDescripcionManufactura.DataSource := DSManufactura;
  TxtDescripcionManufactura.DataField := 'descripcion';
  CmbCodigoManufactura.LookupSource := DSManufactura;
  CmbCodigoManufactura.LookupField := 'codigo';
  CmbCodigoManufactura.LookupDisplay := 'codigo;descripcion';
  TxtDescripcionArancel.DataSource := DSArancel;
  TxtDescripcionArancel.DataField := 'descripcion';
  CmbCodigoArancel.LookupSource := DSArancel;
  CmbCodigoArancel.LookupField := 'codigo';
  CmbCodigoArancel.LookupDisplay := 'codigo;descripcion';
  TxtDescripcionUnidad.DataSource := DSUnidad;
  TxtDescripcionUnidad.DataField := 'descripcion';
  CmbCodigoUnidad.LookupSource := DSUnidad;
  CmbCodigoUnidad.LookupField := 'codigo';
  CmbCodigoUnidad.LookupDisplay := 'codigo;descripcion';
  Buscar;
  MaxRegistros := Parametro.ObtenerValor(MAX_REGISTROS_REFRESCAR);
end;

procedure TFrmProducto.GrdBusquedaCellClick(Column: TColumn);
begin
  Mover;
end;

procedure TFrmProducto.Guardar;
begin
  if (Agregando) then
  begin
    //Agregando...
    //Se Busca el Codigo
    Clase.SetCodigo(TxtCodigo.Text);
    if (Clase.BuscarCodigo) then
    begin
      MessageBox(Self.Handle, 'El C�digo Ya Existe!!!', PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      TxtCodigo.SetFocus;
      Abort;
    end
    else
    begin
      Obtener;
      Clase.Insertar;
      MessageBox(Self.Handle, 'El Producto Ha Sido Agregado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    end;
  end
  else
  begin
    //Editando...
    Obtener;
    Clase.Modificar;
    MessageBox(Self.Handle, 'El Producto Ha Sido Editado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    TxtCodigo.Enabled := True;
  end;
  if Data.RecordCount <= MaxRegistros then
  begin
    Refrescar;
  end;
  Limpiar;
end;

procedure TFrmProducto.Imprimir;
begin

end;

procedure TFrmProducto.Limpiar;
begin
  TxtCodigo.Text := '';
  TxtReferencia.Text := '';
  TxtUniqueID.Caption := '';
  TxtDescripcion.Text := '';
  TxtPrecio.Text := '';
  TxtPDescuento.Text := '';
  TxtPIVAVenta.Text := '';
  TxtPIVACompra.Text := '';
  CmbCodigoDivision.KeyValue := '';
  CmbCodigoLinea.KeyValue := '';
  CmbCodigoFamilia.KeyValue := '';
  CmbCodigoClase.KeyValue := '';
  CmbCodigoManufactura.KeyValue := '';
  CmbCodigoArancel.KeyValue := '';
  CmbCodigoUnidad.KeyValue := '';
  TxtUbicacion.Text := '';
  TxtCostoAnterior.Text := '';
  TxtCostoActual.Text := '';
  TxtCostoFOB.Text := '';
  TxtCostoAnteriorUS.Text := '';
  TxtCostoActualUS.Text := '';
  TxtPeso.Text := '';
  TxtVolumen.Text := '';
  TxtFechaIngreso.Caption := '';
  TxtFechaUltimaCompra.Caption := '';
  ChkInventario.Checked := False;
end;

procedure TFrmProducto.Mover;
var
  DSTemp: TDataSet;
begin
  if (DS.DataSet.RecordCount > 0) then
  begin
    DSTemp := DS.DataSet;
    with Clase do
    begin
      SetCodigo(DSTemp.FieldValues['codigo']);
      SetReferencia(DSTemp.FieldValues['referencia']);
      SetDescripcion(DSTemp.FieldValues['descripcion']);
      SetCodigoDivision(DSTemp.FieldValues['codigo_division']);
      SetCodigoLinea(DSTemp.FieldValues['codigo_linea']);
      SetCodigoFamilia(DSTemp.FieldValues['codigo_familia']);
      SetCodigoClase(DSTemp.FieldValues['codigo_clase']);
      SetCodigoManufactura(DSTemp.FieldValues['codigo_manufactura']);
      SetCodigoArancel(DSTemp.FieldValues['codigo_arancel']);
      SetCodigoUnidad(DSTemp.FieldValues['codigo_unidad']);
      SetFechaIngreso(DSTemp.FieldValues['fecha_ingreso']);
      SetPeso(DSTemp.FieldValues['peso']);
      SetVolumen(DSTemp.FieldValues['volumen']);
      SetPIvaVenta(DSTemp.FieldValues['p_iva_venta']);
      SetPIvaCompra(DSTemp.FieldValues['p_iva_compra']);
      SetPrecio(DSTemp.FieldValues['precio']);
      SetPDescuento(DSTemp.FieldValues['p_descuento']);
      SetCostoAnterior(DSTemp.FieldValues['costo_anterior']);
      SetCostoActual(DSTemp.FieldValues['costo_actual']);
      SetFechaUCompra(DSTemp.FieldValues['fecha_u_compra']);
      SetUsCostoAnterior(DSTemp.FieldValues['us_costo_anterior']);
      SetUsCostoActual(DSTemp.FieldValues['us_costo_actual']);
      SetCostoFob(DSTemp.FieldValues['costo_fob']);
      SetUbicacion(DSTemp.FieldValues['ubicacion']);
      SetInventario(DSTemp.FieldValues['inventario']);
      SetUniqueId(DSTemp.FieldValues['unique_id']);
    end;
    Asignar;
  end
  else
  begin
    Limpiar;
  end;
end;

procedure TFrmProducto.Obtener;
begin
  with Clase do
  begin
    SetCodigo(TxtCodigo.Text);
    SetReferencia(TxtReferencia.Text);
    SetDescripcion(TxtDescripcion.Text);
    SetCodigoDivision(CmbCodigoDivision.Text);
    SetCodigoLinea(CmbCodigoLinea.Text);
    SetCodigoFamilia(CmbCodigoFamilia.Text);
    SetCodigoClase(CmbCodigoClase.Text);
    SetCodigoManufactura(CmbCodigoManufactura.Text);
    SetCodigoArancel(CmbCodigoArancel.Text);
    SetCodigoUnidad(CmbCodigoUnidad.Text);
    //SetFechaIngreso();
    SetPeso(TxtPeso.Value);
    SetVolumen(TxtVolumen.Value);
    SetPIvaVenta(TxtPIVAVenta.Value);
    SetPIvaCompra(TxtPIVACompra.Value);
    SetPrecio(TxtPrecio.Value);
    SetPDescuento(TxtPDescuento.Value);
    SetCostoAnterior(TxtCostoAnterior.Value);
    SetCostoActual(TxtCostoActual.Value);
    //SetFechaUCompra();
    SetUsCostoAnterior(TxtCostoAnteriorUS.Value);
    SetUsCostoActual(TxtCostoActualUS.Value);
    SetCostoFob(TxtCostoFOB.Value);
    SetUbicacion(TxtUbicacion.Text);
    SetInventario(ChkInventario.Checked);
    SetUniqueId(StrToIntDef(TxtUniqueId.Caption,0));
  end;
end;

procedure TFrmProducto.Refrescar;
begin
  TxtCriterio.Text := '';
  Buscar;
  //Se Actualizan Los demas Componentes
  DataDivision := Division.ObtenerLista;
  DataLinea := Linea.ObtenerLista;
  DataFamilia := Familia.ObtenerLista;
  DataClase := ClaseClase.ObtenerLista;
  DataManufactura := Manufactura.ObtenerLista;
  DataArancel := Arancel.ObtenerLista;
  DataUnidad := Unidad.ObtenerLista;
  DataDivision.First;
  DataLinea.First;
  DataFamilia.First;
  DataClase.First;
  DataManufactura.First;
  DataArancel.First;
  DataUnidad.First;
  DSDivision.DataSet := DataDivision;
  DSLinea.DataSet := DataLinea;
  DSFamilia.DataSet := DataFamilia;
  DSClase.DataSet := DataClase;
  DSManufactura.DataSet := DataManufactura;
  DSArancel.DataSet := DataArancel;
  DSUnidad.DataSet := DataUnidad;
end;

function TFrmProducto.Validar: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  (* Validando Informacion del Producto *)
  //Validando Codigo
  if (Length(Trim(TxtCodigo.Text)) = 0) then
  begin
    ListaErrores.Add('El C�digo No Puede Estar En Blanco!!!');
  end;
  //Validando Referencia
  if (Length(Trim(TxtReferencia.Text)) = 0) then
  begin
    ListaErrores.Add('La Referencia No Puede Estar En Blanco!!!');
  end;
  //Validando Descripcion
  if (Length(Trim(TxtDescripcion.Text)) = 0) then
  begin
    ListaErrores.Add('La Descripci�n No Puede Estar En Blanco!!!');
  end;
  //Validando C�digo de la Division
  if (Length(Trim(CmbCodigoDivision.Text)) = 0) then
  begin
    ListaErrores.Add('El C�digo de Divisi�n No Puede Estar En Blanco!!!');
  end;
  //Validando C�digo de la L�nea
  if (Length(Trim(CmbCodigoLinea.Text)) = 0) then
  begin
    ListaErrores.Add('El C�digo de la L�nea No Puede Estar En Blanco!!!');
  end;
  //Validando C�digo de la Familia
  if (Length(Trim(CmbCodigoFamilia.Text)) = 0) then
  begin
    ListaErrores.Add('El C�digo de la Familia No Puede Estar En Blanco!!!');
  end;
  //Validando C�digo de la Clase
  if (Length(Trim(CmbCodigoClase.Text)) = 0) then
  begin
    ListaErrores.Add('El C�digo de la Clase No Puede Estar En Blanco!!!');
  end;
  //Validando C�digo de la Manufactura
  if (Length(Trim(CmbCodigoManufactura.Text)) = 0) then
  begin
    ListaErrores.Add('El C�digo de la Manufactura No Puede Estar En Blanco!!!');
  end;
  //Validando C�digo de la Unidad
  if (Length(Trim(CmbCodigoUnidad.Text)) = 0) then
  begin
    ListaErrores.Add('El C�digo de la Unidad No Puede Estar En Blanco!!!');
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
