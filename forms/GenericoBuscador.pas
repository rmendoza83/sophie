unit GenericoBuscador;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, DB,
  ZAbstractRODataset, ZDataset, ZConnection, ClsGenericoBD, Utils,
  ZAbstractConnection;

type
  TFrmGenericoBuscador = class(TForm)
    PanelBusqueda: TPanel;
    PanelBotones: TPanel;
    LblCriterio: TLabel;
    TxtCriterio: TEdit;
    BtnCerrar: TBitBtn;
    GridBusqueda: TDBGrid;
    DS: TDataSource;
    ZCnn: TZConnection;
    ZQry: TZReadOnlyQuery;
    TimerFiltro: TTimer;
    procedure TxtCriterioChange(Sender: TObject);
    procedure TimerFiltroTimer(Sender: TObject);
    procedure BtnCerrarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure GridBusquedaDblClick(Sender: TObject);
    procedure GridBusquedaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TxtCriterioKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ZQryFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure DSStateChange(Sender: TObject);
  private
    { Private declarations }
    Aceptado: Boolean;
    ResultEdit: TEdit;
    Clase: TGenericoBD;
    CampoClave: string;
    ValorClave: Variant;
    DSet: TDataSet;
    Condicion: string;
    ArrayResult: Boolean;
    ArrayValorClave: TArrayVariant;
    HayRegistros: Boolean;
    function CampoValido(Indice: Integer): Boolean;
    procedure Filtrar;
    procedure AsignarValorClave;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent; AClase: TGenericoBD; ACampoClave: string; ATitulo: string; AEdit: TEdit; ADataset: TDataSet; ACondicion: string = ''); reintroduce; overload;
    constructor Create(AOwner: TComponent; AClase: TGenericoBD; ACampoClave: string; ATitulo: string; AEdit: TEdit; ADataset: TDataSet; AArrayResult: Boolean; ACondicion: string = ''); reintroduce; overload;
    function GetValorClave: Variant;
    function GetArrayValorClave: TArrayVariant;
    function GetHayRegistros: Boolean;
  end;

var
  FrmGenericoBuscador: TFrmGenericoBuscador;

function Buscador(AOwner: TComponent; AClase: TGenericoBD; ACampoClave: string; ATitulo: string; AEdit: TEdit; ADataset: TDataSet; ACondicion: string = ''): Boolean; overload;
function Buscador(AOwner: TComponent; AClase: TGenericoBD; ACampoClave: string; ATitulo: string; AEdit: TEdit; ADataset: TDataSet; AArrayResult: Boolean; var AArrayValorClave: TArrayVariant; ACondicion: string = ''): Boolean; overload;

implementation

uses
  DMBD;

function Buscador(AOwner: TComponent; AClase: TGenericoBD; ACampoClave: string; ATitulo: string; AEdit: TEdit; ADataset: TDataSet; ACondicion: string = ''): Boolean;
begin
  FrmGenericoBuscador := TFrmGenericoBuscador.Create(AOwner,AClase,ACampoClave,ATitulo,AEdit,ADataset,ACondicion);
  if FrmGenericoBuscador.GetHayRegistros then
  begin
    FrmGenericoBuscador.ShowModal;
    Result := FrmGenericoBuscador.Aceptado;
    FreeAndNil(FrmGenericoBuscador);
  end
  else
  begin
    Result := False;
    FreeAndNil(FrmGenericoBuscador);
    MessageBox(Application.Handle, 'No Hay Registros Disponibles para Realizar una Busqueda!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end;
end;

function Buscador(AOwner: TComponent; AClase: TGenericoBD; ACampoClave: string; ATitulo: string; AEdit: TEdit; ADataset: TDataSet; AArrayResult: Boolean; var AArrayValorClave: TArrayVariant; ACondicion: string = ''): Boolean;
begin
  FrmGenericoBuscador := TFrmGenericoBuscador.Create(AOwner,AClase,ACampoClave,ATitulo,AEdit,ADataset,True,ACondicion);
  if FrmGenericoBuscador.GetHayRegistros then
  begin
    FrmGenericoBuscador.ShowModal;
    Result := FrmGenericoBuscador.Aceptado;
    AArrayValorClave := FrmGenericoBuscador.GetArrayValorClave;
    FreeAndNil(FrmGenericoBuscador);
  end
  else
  begin
    Result := False;
    FreeAndNil(FrmGenericoBuscador);
    MessageBox(Application.Handle, 'No Hay Registros Disponibles para Realizar una Busqueda!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end;
end;

{$R *.dfm}

procedure TFrmGenericoBuscador.AsignarValorClave;
var
  Aux: TArrayString;
  i: Integer;
begin
  if (ArrayResult) then
  begin
    SetLength(ArrayValorClave,0);
    Aux := Explode(';',CampoClave);
    for i := Low(Aux) to High(Aux) do
    begin
      SetLength(ArrayValorClave,Length(ArrayValorClave) + 1);
      ArrayValorClave[Length(ArrayValorClave) - 1] := ZQry.FieldValues[Aux[i]];
    end;
  end
  else
  begin
    ValorClave := ZQry.FieldValues[CampoClave];
  end;
  Aceptado := True;
  Close;
end;

procedure TFrmGenericoBuscador.BtnCerrarClick(Sender: TObject);
begin
  ModalResult := mrNone;
  Close;
end;

function TFrmGenericoBuscador.CampoValido(Indice: Integer): Boolean;
begin
  Result := False;
  case ZQry.FieldDefs[Indice].DataType of
    ftString, ftWideString, ftMemo, ftFixedChar,
    ftGuid: Result := True;
  end;
end;

constructor TFrmGenericoBuscador.Create(AOwner: TComponent;
  AClase: TGenericoBD; ACampoClave, ATitulo: String; AEdit: TEdit;
  ADataset: TDataSet; ACondicion: string = '');
begin
  inherited Create(AOwner);
  Aceptado := False;
  Clase := AClase;
  CampoClave := ACampoClave;
  Caption := 'Sophie - ' + ATitulo;
  ResultEdit := AEdit;
  DSet := ADataset;
  Condicion := ACondicion;
  ArrayResult := False;
end;

constructor TFrmGenericoBuscador.Create(AOwner: TComponent;
  AClase: TGenericoBD; ACampoClave, ATitulo: String; AEdit: TEdit;
  ADataset: TDataSet; AArrayResult: Boolean; ACondicion: string = '');
begin
  Create(AOwner,AClase,ACampoClave,ATitulo,AEdit,ADataset,ACondicion);
  ArrayResult := AArrayResult;
end;

procedure TFrmGenericoBuscador.DSStateChange(Sender: TObject);
var
  i: Integer;
begin
  if (Assigned(Ds.DataSet)) then
    begin
      if (DS.DataSet.State = dsBrowse) then
      begin
        with GridBusqueda do
        begin
          for i := 0 to Columns.Count - 1 do
          begin
            Columns[i].Title.Caption := NombreCampoBDToNombreCapital(Columns[i].Title.Caption);
          end;
        end;
      end;
    end;
end;

procedure TFrmGenericoBuscador.Filtrar;
begin
  Screen.Cursor := crSQLWait;
  ZQry.Filtered := False;
  if (Length(Trim(TxtCriterio.Text)) > 0) then
  begin
    ZQry.Filtered := True;
    ZQry.First;
  end;
  Screen.Cursor := crDefault;
  TimerFiltro.Enabled := False;
end;

procedure TFrmGenericoBuscador.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if Aceptado then
  begin
    ModalResult := mrOk;
    //Actualizando Dataset de Origen
    if (DSet <> nil) then
    begin
      with DSet do
      begin
        Locate(CampoClave,ValorClave,[loCaseInsensitive]);
        EnableControls;
      end;
    end;
  end;
  if (ResultEdit <> nil) then
  begin
    ResultEdit.Text := ValorClave;
    ResultEdit.SetFocus;
    ResultEdit.SelectAll;
  end;
  ZQry.Close;
end;

procedure TFrmGenericoBuscador.FormShow(Sender: TObject);
begin
  GridBusqueda.DataSource := DS;
end;

function TFrmGenericoBuscador.GetValorClave: Variant;
begin
  Result := ValorClave;
end;

function TFrmGenericoBuscador.GetArrayValorClave: TArrayVariant;
begin
  Result := ArrayValorClave;
end;

function TFrmGenericoBuscador.GetHayRegistros: Boolean;
begin
  Result := HayRegistros;
end;

procedure TFrmGenericoBuscador.GridBusquedaDblClick(Sender: TObject);
begin
  AsignarValorClave;
end;

procedure TFrmGenericoBuscador.GridBusquedaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  case Key of
    VK_RETURN:
    begin
      AsignarValorClave;
    end;
    VK_ESCAPE:
    begin
      ModalResult := mrNone;
      Close;
    end;
  end;
end;

procedure TFrmGenericoBuscador.TimerFiltroTimer(Sender: TObject);
begin
  Filtrar;
  TimerFiltro.Enabled := False;
end;

procedure TFrmGenericoBuscador.TxtCriterioChange(Sender: TObject);
begin
  TimerFiltro.Enabled := False;
  TimerFiltro.Enabled := True;
end;

procedure TFrmGenericoBuscador.TxtCriterioKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  case Key of
    VK_DOWN:
      GridBusqueda.SetFocus;
    VK_RETURN, VK_TAB:
      begin
        Filtrar;
        GridBusqueda.SetFocus;
      end;
    VK_ESCAPE: Close;
  end;
end;

procedure TFrmGenericoBuscador.ZQryFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
var
  i: Integer;
begin
  Accept := False;
  if (Length(Trim(TxtCriterio.Text)) > 0) then
  begin
    for i := 0 to DataSet.FieldCount - 1 do
    begin
      if (CampoValido(i)) then
      begin
        if (Pos(UpperCase(TxtCriterio.Text),UpperCase(DataSet.Fields[i].AsString)) > 0) then
        begin
          Accept := True;
          Break;
        end;
      end;
    end;
  end;
end;

procedure TFrmGenericoBuscador.FormCreate(Sender: TObject);
begin
  HayRegistros := False;
  Screen.Cursor := crHourGlass;
  //Configurando Conexion
  with ZCnn do
  begin
    Database := BD.GetBaseDatos;
    HostName := BD.GetHost;
    Password := BD.GetPassword;
    Port := BD.GetPuerto;
    Protocol := BD.GetProtocolo;
    User := BD.GetUsuario;
    Properties := BD.ZCnn.Properties;
    Connect;
  end;
  //Configurando Query
  with ZQry do
  begin
    //Aplicando Condicion Especial (Sentencia WHERE SQL)
    if (Length(Trim(Condicion)) > 0) then
    begin
      SQL.Text := 'SELECT ' + Implode(',',Clase.GetCamposCriterio) + ' FROM ' + Clase.GetTabla + ' WHERE ' + Condicion + ' ORDER BY 1';
    end
    else
    begin
      SQL.Text := 'SELECT ' + Implode(',',Clase.GetCamposCriterio) + ' FROM ' + Clase.GetTabla + ' ORDER BY 1';
    end;
    Filter := '';
    FilterOptions := [foCaseInsensitive];
    Filtered := False;
    Open;
    If ZQry.RecordCount > 0 then
    begin
      HayRegistros := True;
    end
    else
    begin
      ZQry.Close;
    end;
  end;
  Screen.Cursor := crDefault;
end;

procedure TFrmGenericoBuscador.FormDestroy(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  ZQry.Close;
  ZCnn.Disconnect;
  Screen.Cursor := crDefault;
end;

end.
