object FrmSophieSoporteTecnico: TFrmSophieSoporteTecnico
  Left = 0
  Top = 0
  BorderStyle = bsSizeToolWin
  Caption = 'Sophie - Contacto al Soporte Tecnico...'
  ClientHeight = 366
  ClientWidth = 384
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object LblTitulo: TLabel
    Left = 8
    Top = 8
    Width = 371
    Height = 39
    Caption = 
      'Por favor, complete el siguiente formulario para poder prestarle' +
      ' '#13#10'un mejor servicio (Recuerde que es indispensable tener conexi' +
      #243'n '#13#10'a Internet):'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object LblNombreRegistro: TLabel
    Left = 18
    Top = 64
    Width = 65
    Height = 13
    Alignment = taRightJustify
    Caption = 'Registrado a:'
  end
  object TxtNombreRegistro: TLabel
    Left = 89
    Top = 64
    Width = 3
    Height = 13
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object LblNombre: TLabel
    Left = 42
    Top = 96
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nombre:'
  end
  object LblEmail: TLabel
    Left = 55
    Top = 128
    Width = 28
    Height = 13
    Alignment = taRightJustify
    Caption = 'Email:'
  end
  object LblTelefonos: TLabel
    Left = 32
    Top = 160
    Width = 51
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tel'#233'fonos:'
  end
  object LblObservaciones: TLabel
    Left = 8
    Top = 192
    Width = 75
    Height = 13
    Alignment = taRightJustify
    Caption = 'Observaciones:'
  end
  object BtnAceptar: TBitBtn
    Left = 8
    Top = 333
    Width = 75
    Height = 25
    Caption = '&Aceptar'
    Default = True
    TabOrder = 4
    OnClick = BtnAceptarClick
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333333333333333330000333333333333333333333333F33333333333
      00003333344333333333333333388F3333333333000033334224333333333333
      338338F3333333330000333422224333333333333833338F3333333300003342
      222224333333333383333338F3333333000034222A22224333333338F338F333
      8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
      33333338F83338F338F33333000033A33333A222433333338333338F338F3333
      0000333333333A222433333333333338F338F33300003333333333A222433333
      333333338F338F33000033333333333A222433333333333338F338F300003333
      33333333A222433333333333338F338F00003333333333333A22433333333333
      3338F38F000033333333333333A223333333333333338F830000333333333333
      333A333333333333333338330000333333333333333333333333333333333333
      0000}
    NumGlyphs = 2
  end
  object BtnCancelar: TBitBtn
    Left = 301
    Top = 333
    Width = 75
    Height = 25
    Cancel = True
    Caption = '&Cancelar'
    TabOrder = 5
    OnClick = BtnCancelarClick
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      333333333333333333333333000033338833333333333333333F333333333333
      0000333911833333983333333388F333333F3333000033391118333911833333
      38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
      911118111118333338F3338F833338F3000033333911111111833333338F3338
      3333F8330000333333911111183333333338F333333F83330000333333311111
      8333333333338F3333383333000033333339111183333333333338F333833333
      00003333339111118333333333333833338F3333000033333911181118333333
      33338333338F333300003333911183911183333333383338F338F33300003333
      9118333911183333338F33838F338F33000033333913333391113333338FF833
      38F338F300003333333333333919333333388333338FFF830000333333333333
      3333333333333333333888330000333333333333333333333333333333333333
      0000}
    NumGlyphs = 2
  end
  object TxtNombre: TEdit
    Left = 89
    Top = 93
    Width = 287
    Height = 21
    TabOrder = 0
  end
  object TxtEmail: TEdit
    Left = 89
    Top = 125
    Width = 287
    Height = 21
    TabOrder = 1
  end
  object TxtTelefonos: TEdit
    Left = 89
    Top = 157
    Width = 287
    Height = 21
    TabOrder = 2
  end
  object TxtObservaciones: TMemo
    Left = 89
    Top = 189
    Width = 287
    Height = 138
    TabOrder = 3
  end
  object MXP: TmxProtector
    CodeKey = 'Ignore'
    ProtectionTypes = [stDayTrial, stRegister]
    Options = [poAutoInit, poCheckSytemTime]
    RegistryRootKey = rkLocalMachine
    Expiration = 41456.773032013890000000
    MaxStartNumber = 0
    MaxDayNumber = 60
    UserName = 'YUGLYS MARCANO'
    Version = '1.32'
    OnCheckRegistration = MXPCheckRegistration
    OnDayTrial = MXPDayTrial
    Left = 176
    Top = 328
    UniqueCodeID = 
      '7A4330453731354C3C273E3D3B3C2224212A56395454232934282E255F2F2915' +
      '11161115665B'
    UniqueID = 
      '21015772657169150175796473236B187510696E64076B0B4E011F7662677E1E' +
      '05690F267024'
  end
end
