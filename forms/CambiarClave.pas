unit CambiarClave;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ClsUser, XPMan;

type
  TFrmCambiarClave = class(TForm)
    LblTitulo: TLabel;
    LblActual: TLabel;
    LblNueva: TLabel;
    LblNueva2: TLabel;
    TxtActual: TEdit;
    TxtNueva: TEdit;
    TxtNueva2: TEdit;
    BtnCancelar: TBitBtn;
    BtnAceptar: TBitBtn;
    TemaXP: TXPManifest;
    procedure BtnAceptarClick(Sender: TObject);
    procedure BtnCancelarClick(Sender: TObject);
  private
    { Private declarations }
    User: TUser;
    function ValidarCambio: Boolean;
    procedure CambiarClave;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent; ALoginUsuario: String); reintroduce;
  end;

var
  FrmCambiarClave: TFrmCambiarClave;

implementation

uses
  Utils;

{$R *.dfm}

procedure TFrmCambiarClave.BtnAceptarClick(Sender: TObject);
begin
  if (ValidarCambio) then
  begin
    if (MessageBox(Self.Handle, '�Esta Seguro de Cambiar su Contrase�a?', PChar(MsgTituloInformacion), MB_ICONQUESTION or MB_YESNO or MB_TOPMOST or MB_TASKMODAL) = mrYes) then
    begin
      CambiarClave;
      MessageBox(Self.Handle, 'La Contrase�a Se Ha Cambiado Correctamente!!!', PChar(MsgTituloInformacion), MB_ICONQUESTION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      Close;
    end;
  end;
end;

procedure TFrmCambiarClave.BtnCancelarClick(Sender: TObject);
begin
  Close;
end;

procedure TFrmCambiarClave.CambiarClave;
begin
  User.SetPasswordUsuario(TxtNueva.Text);
  User.Modificar;
end;

constructor TFrmCambiarClave.Create(AOwner: TComponent;
  ALoginUsuario: String);
begin
  inherited Create(AOwner);
  User := TUser.Create;
  User.SetLoginUsuario(ALoginUsuario);
  User.Buscar;
  Caption := Caption + ' [' + User.GetLoginUsuario + ']';
end;

function TFrmCambiarClave.ValidarCambio: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  (* Validando Cambio de Contrase�a *)
  //Validando Contrase�a Actual
  if (Length(Trim(TxtActual.Text)) = 0) then
  begin
    ListaErrores.Add('La Contrase�a Actual No Puede Estar En Blanco!!!');
  end;
  //Validando Contrase�a Nueva
  if (Length(Trim(TxtNueva.Text)) = 0) then
  begin
    ListaErrores.Add('La Contrase�a Nueva No Puede Estar En Blanco!!!');
  end;
  //Validando Contrase�a Nueva 2
  if (Length(Trim(TxtNueva2.Text)) = 0) then
  begin
    ListaErrores.Add('La Repetici�n de la Contrase�a No Puede Estar En Blanco!!!');
  end;
  //Validando Contrase�a Actual sea Valida
  if (MD5String(TxtActual.Text) <> User.GetPasswordUsuario) then
  begin
    ListaErrores.Add('La Contrase�a Actual No Es V�lida!!');
  end;
  //Validando Igualdad de la Contrase�a Nueva
  if (TxtNueva.Text <> TxtNueva2.Text) then
  begin
    ListaErrores.Add('La Contrase�a Nueva No Coincide Con La Repetici�n!!!');
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
