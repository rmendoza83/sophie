unit AcercaDe;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Buttons, mxProtector, XPMan;

type
  TFrmAcercaDe = class(TForm)
    Image1: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    GrpDatosLicencia: TGroupBox;
    Shape1: TShape;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    LblTipoLicencia: TLabel;
    LblRegistradoA: TLabel;
    LblSerial: TLabel;
    Label11: TLabel;
    LblTiempo: TLabel;
    Label8: TLabel;
    BtnAceptar: TBitBtn;
    GroupBox1: TGroupBox;
    Label9: TLabel;
    PanelAyudaRegistro: TPanel;
    MXP: TmxProtector;
    TemaXP: TXPManifest;
    procedure MXPCheckRegistration(Sender: TObject; var UserName,
      SerialNumber: string; var Registered: Boolean);
    procedure MXPDayTrial(Sender: TObject; DaysRemained: Integer);
    procedure PanelAyudaRegistroDblClick(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmAcercaDe: TFrmAcercaDe;

implementation

{$R *.dfm}

uses
  PModule,
  Clipbrd;

procedure TFrmAcercaDe.BtnAceptarClick(Sender: TObject);
begin
  Close;
end;

procedure TFrmAcercaDe.MXPCheckRegistration(Sender: TObject; var UserName,
  SerialNumber: string; var Registered: Boolean);
begin
  if (Registered) then
  begin
    LblTipoLicencia.Caption := 'Full Version';
    LblRegistradoA.Caption := UserName;
    LblSerial.Caption := SerialNumber;
  end
  else
  begin
    LblTipoLicencia.Caption := 'Demo Version';
    LblRegistradoA.Caption := 'Sin Registro';
    LblSerial.Caption := 'Sin Registro';
  end;
end;

procedure TFrmAcercaDe.MXPDayTrial(Sender: TObject; DaysRemained: Integer);
begin
  if (MXP.IsRegistered) then
  begin
    LblTiempo.Caption := 'N/A';
  end
  else
  begin
    LblTiempo.Caption := IntToStr(DaysRemained) + ' Dias...';
  end;
end;

procedure TFrmAcercaDe.PanelAyudaRegistroDblClick(Sender: TObject);
begin
  Clipboard.AsText := P.GetSophieValidationPath;
end;

end.
