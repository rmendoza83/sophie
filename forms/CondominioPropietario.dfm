inherited FrmCondominioPropietario: TFrmCondominioPropietario
  Caption = 'Sophie - Propietarios'
  ClientHeight = 422
  ExplicitHeight = 450
  PixelsPerInch = 96
  TextHeight = 13
  inherited Pag: TPageControl
    Height = 386
    ExplicitHeight = 386
    inherited TabBusqueda: TTabSheet
      ExplicitHeight = 358
      inherited GrdBusqueda: TDBGrid
        Height = 320
      end
    end
    inherited TabDatos: TTabSheet
      ExplicitHeight = 358
      object LblCodigo: TLabel
        Left = 33
        Top = 19
        Width = 41
        Height = 13
        Alignment = taRightJustify
        Caption = 'C'#243'digo:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblRif: TLabel
        Left = 1
        Top = 43
        Width = 73
        Height = 13
        Alignment = taRightJustify
        Caption = 'C'#233'dula o RIF:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblRazonSocial: TLabel
        Left = 0
        Top = 92
        Width = 74
        Height = 13
        Alignment = taRightJustify
        Caption = 'Raz'#243'n Social:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblTelefonos: TLabel
        Left = 16
        Top = 116
        Width = 58
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tel'#233'fonos:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblFechaNacimiento: TLabel
        Left = 145
        Top = 67
        Width = 119
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fecha de Nacimiento:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblEmail: TLabel
        Left = 41
        Top = 140
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Email:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblNit: TLabel
        Left = 244
        Top = 43
        Width = 22
        Height = 13
        Alignment = taRightJustify
        Caption = 'NIT:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblSexo: TLabel
        Left = 43
        Top = 67
        Width = 31
        Height = 13
        Alignment = taRightJustify
        Caption = 'Sexo:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblDireccionDomicilio: TLabel
        Left = 14
        Top = 283
        Width = 126
        Height = 13
        Alignment = taRightJustify
        Caption = 'Direcci'#243'n de Domicilio:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblDireccionFiscal: TLabel
        Left = 15
        Top = 217
        Width = 89
        Height = 13
        Alignment = taRightJustify
        Caption = 'Direcci'#243'n Fiscal:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblFax: TLabel
        Left = 243
        Top = 116
        Width = 23
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fax:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblWebsite: TLabel
        Left = 243
        Top = 140
        Width = 49
        Height = 13
        Alignment = taRightJustify
        Caption = 'Website:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblFechaIngreso: TLabel
        Left = 13
        Top = 167
        Width = 100
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fecha de Ingreso:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblContacto: TLabel
        Left = 20
        Top = 193
        Width = 54
        Height = 13
        Alignment = taRightJustify
        Caption = 'Contacto:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object TxtEdad: TLabel
        Left = 390
        Top = 67
        Width = 5
        Height = 13
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clHotLight
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object TxtCodigo: TEdit
        Left = 84
        Top = 16
        Width = 144
        Height = 21
        TabOrder = 0
      end
      object CmbPrefijoRif: TComboBox
        Left = 84
        Top = 40
        Width = 34
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 1
        Items.Strings = (
          'V'
          'E'
          'J'
          'G')
      end
      object TxtRif: TEdit
        Left = 124
        Top = 40
        Width = 104
        Height = 21
        MaxLength = 10
        TabOrder = 2
      end
      object TxtRazonSocial: TEdit
        Left = 84
        Top = 89
        Width = 417
        Height = 21
        TabOrder = 3
      end
      object TxtTelefonos: TEdit
        Left = 84
        Top = 113
        Width = 144
        Height = 21
        TabOrder = 4
      end
      object DatFechaNacimiento: TDateEdit
        Left = 278
        Top = 64
        Width = 106
        Height = 21
        DefaultToday = True
        DialogTitle = 'Seleccione Fecha'
        NumGlyphs = 2
        YearDigits = dyFour
        TabOrder = 5
        Text = '15/11/2011'
        OnChange = DatFechaNacimientoChange
      end
      object TxtEmail: TEdit
        Left = 84
        Top = 137
        Width = 146
        Height = 21
        TabOrder = 6
      end
      object TxtNit: TEdit
        Left = 278
        Top = 37
        Width = 106
        Height = 21
        TabOrder = 7
      end
      object CmbSexo: TComboBox
        Left = 84
        Top = 64
        Width = 46
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 8
        Items.Strings = (
          'M'
          'F')
      end
      object TxtDireccionDomicilio2: TEdit
        Left = 16
        Top = 323
        Width = 488
        Height = 21
        TabOrder = 9
      end
      object TxtDireccionDomicilio1: TEdit
        Left = 16
        Top = 299
        Width = 488
        Height = 21
        TabOrder = 10
      end
      object TxtDireccionFiscal1: TEdit
        Left = 16
        Top = 233
        Width = 487
        Height = 21
        TabOrder = 11
      end
      object TxtDireccionFiscal2: TEdit
        Left = 16
        Top = 257
        Width = 487
        Height = 21
        TabOrder = 12
      end
      object TxtFax: TEdit
        Left = 276
        Top = 113
        Width = 169
        Height = 21
        TabOrder = 13
      end
      object TxtWebsite: TEdit
        Left = 302
        Top = 137
        Width = 146
        Height = 21
        TabOrder = 14
      end
      object DatFechaIngreso: TDateEdit
        Left = 124
        Top = 164
        Width = 108
        Height = 21
        DefaultToday = True
        DialogTitle = 'Seleccione Fecha'
        NumGlyphs = 2
        YearDigits = dyFour
        TabOrder = 15
        Text = '15/11/2011'
      end
      object TxtContacto: TEdit
        Left = 84
        Top = 190
        Width = 196
        Height = 21
        TabOrder = 16
      end
    end
  end
end
