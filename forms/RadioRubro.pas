unit RadioRubro;

{$I sophie.inc}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Generico, XPMan, ImgList, DB, Grids, DBGrids, StdCtrls, Buttons,
  ExtCtrls, ComCtrls, ToolWin, ClsRadioRubro, DBClient, Utils
  {$IFDEF DELPHI10}, ImageList{$ENDIF};

type
  TFrmRadioRubro = class(TFrmGenerico)
    LblCodigo: TLabel;
    TxtCodigo: TEdit;
    LblDescripcion: TLabel;
    TxtDescripcion: TEdit;
    procedure GrdBusquedaCellClick(Column: TColumn);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    Clase: TRadioRubro;
    Data: TClientDataSet;
    procedure Agregar;
    procedure Editar;
    procedure Eliminar;
    procedure Guardar;
    procedure Cancelar;
    procedure Refrescar;
    procedure Imprimir;
    procedure Buscar;
    procedure Limpiar;
    procedure Asignar;
    procedure Obtener;
    procedure Mover;
  protected
    function Validar: Boolean; override;
  public
    { Public declarations }
  end;

var
  FrmRadioRubro: TFrmRadioRubro;

implementation

{$R *.dfm}

procedure TFrmRadioRubro.Agregar;
begin
  //Verificando
  Limpiar;
  TxtCodigo.SetFocus;
end;

procedure TFrmRadioRubro.Asignar;
begin
  with Clase do
  begin
    TxtCodigo.Text := GetCodigo;
    TxtDescripcion.Text := GetDescripcion;
  end;
end;

procedure TFrmRadioRubro.Buscar;
begin
  Screen.Cursor := crSQLWait;
  if (Length(Trim(TxtCriterio.Text)) > 0) then
  begin
    Data := Clase.ObtenerListaCriterio(Trim(TxtCriterio.Text));
  end
  else
  begin
    Data := Clase.ObtenerLista;
  end;
  Data.First;
  DS.DataSet := Data;
  GrdBusqueda.Refresh;
  Screen.Cursor := crDefault;
end;

procedure TFrmRadioRubro.Cancelar;
begin
  TxtCodigo.Enabled := True;
end;

procedure TFrmRadioRubro.Editar;
begin
  //Se Busca el Elemento
  Clase.SetCodigo(Data.FieldValues['codigo']);
  Clase.Buscar;
  Asignar;
  TxtCodigo.Enabled := False;
  TxtDescripcion.SetFocus;
end;

procedure TFrmRadioRubro.Eliminar;
begin
  //Se Busca el Elemento
  Clase.SetCodigo(Data.FieldValues['codigo']);
  Clase.Buscar;
  Clase.Eliminar;
  MessageBox(Self.Handle, 'El Rubro Ha Sido Eliminado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  Refrescar;
  Limpiar;
end;

procedure TFrmRadioRubro.FormCreate(Sender: TObject);
begin
  ProcAgregar := Agregar;
  ProcEditar := Editar;
  ProcEliminar := Eliminar;
  ProcGuardar := Guardar;
  ProcCancelar := Cancelar;
  ProcRefrescar := Refrescar;
  ProcImprimir := Imprimir;
  ProcBuscar := Buscar;
  Clase := TRadioRubro.Create;
  Buscar;
end;

procedure TFrmRadioRubro.GrdBusquedaCellClick(Column: TColumn);
begin
  Mover;
end;

procedure TFrmRadioRubro.Guardar;
begin
  if (Agregando) then
  begin
    //Agregando...
    //Se Busca el Codigo
    Clase.SetCodigo(TxtCodigo.Text);
    if (Clase.Buscar) then
    begin
      MessageBox(Self.Handle, 'El C�digo Ya Existe!!!', PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      TxtCodigo.SetFocus;
      Abort;
    end
    else
    begin
      Obtener;
      Clase.Insertar;
      MessageBox(Self.Handle, 'El Rubro  Ha Sido Agregado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    end;
  end
  else
  begin
    //Editando...
    Obtener;
    Clase.Modificar;
    MessageBox(Self.Handle, 'El Rubro Ha Sido Editado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    TxtCodigo.Enabled := True;
  end;
  Refrescar;
  Limpiar;
end;

procedure TFrmRadioRubro.Imprimir;
begin

end;

procedure TFrmRadioRubro.Limpiar;
begin
  TxtCodigo.Text := '';
  TxtDescripcion.Text := '';
end;

procedure TFrmRadioRubro.Mover;
var
  DSTemp: TDataSet;
begin
  if (DS.DataSet.RecordCount > 0) then
  begin
    DSTemp := DS.DataSet;
    with Clase do
    begin
      SetCodigo(DSTemp.FieldValues['codigo']);
      SetDescripcion(DSTemp.FieldValues['descripcion']);
    end;
    Asignar;
  end
  else
  begin
    Limpiar;
  end;
end;

procedure TFrmRadioRubro.Obtener;
begin
  with Clase do
  begin
    SetCodigo(TxtCodigo.Text);
    SetDescripcion(TxtDescripcion.Text);
  end;
end;

procedure TFrmRadioRubro.Refrescar;
begin
  TxtCriterio.Text := '';
  Buscar;
end;

function TFrmRadioRubro.Validar: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  //Validando Codigo
  if (Length(Trim(TxtCodigo.Text)) = 0) then
  begin
    ListaErrores.Add('El C�digo No Puede Estar En Blanco!!!');
  end;
  //Validando Descripci�n
  if (Length(Trim(TxtDescripcion.Text)) = 0) then
  begin
    ListaErrores.Add('La Descripci�n No Puede Estar En Blanco!!!');
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
