unit RegistrarLicencia;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, XPMan, StdCtrls, Buttons, mxProtector, Utils;

type
  TFrmRegistrarLicencia = class(TForm)
    LblTitulo: TLabel;
    LblUsuario: TLabel;
    LblSerial: TLabel;
    TxtUsuario: TEdit;
    TxtSerial: TEdit;
    BtnCancelar: TBitBtn;
    BtnAceptar: TBitBtn;
    TemaXP: TXPManifest;
    MXP: TmxProtector;
    procedure MXPInvalidSerialNumber(Sender: TObject);
    procedure MXPGetSerialNumber(Sender: TObject; var UserName,
      SerialNumber: string);
    procedure BtnAceptarClick(Sender: TObject);
    procedure BtnCancelarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmRegistrarLicencia: TFrmRegistrarLicencia;

implementation

{$R *.dfm}

procedure TFrmRegistrarLicencia.BtnAceptarClick(Sender: TObject);
begin
  if (Length(Trim(TxtUsuario.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Debe Indicar el Usuario!!!', PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_APPLMODAL);
    TxtUsuario.SetFocus;
  end
  else
  begin
    if (Length(Trim(TxtSerial.Text)) = 0) then
    begin
      MessageBox(Self.Handle, 'Debe Indicar el Serial!!!', PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_APPLMODAL);
      TxtSerial.SetFocus;    
    end
    else
    begin
      if (MessageBox(Self.Handle, PChar('�Los Datos Estan Correctos?'), PChar(MsgTituloInformacion), MB_ICONQUESTION or MB_YESNO or MB_TOPMOST or MB_APPLMODAL) = mrYes) then
      begin
        with MXP do
        begin
          Registration;
          if (IsRegistered) then
          begin
            MessageBox(Self.Handle, PChar('Sophie Ha Sido Registrado Satisfactoriamente!!!'+#13+#10+''+#13+#10+'Gracias por Preferirnos...'), PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_APPLMODAL);
            Close;
          end;
        end;
      end;
    end;
  end;
end;

procedure TFrmRegistrarLicencia.BtnCancelarClick(Sender: TObject);
begin
  Close;
end;

procedure TFrmRegistrarLicencia.MXPGetSerialNumber(Sender: TObject;
  var UserName, SerialNumber: string);
begin
  UserName := TxtUsuario.Text;
  SerialNumber := TxtSerial.Text;
end;

procedure TFrmRegistrarLicencia.MXPInvalidSerialNumber(Sender: TObject);
begin
  MessageBox(Self.Handle, PChar('Serial Especificado Inv�lido!!!'), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_APPLMODAL);
  TxtSerial.SetFocus;
  TxtSerial.SelectAll;
end;

end.
