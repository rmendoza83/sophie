unit AgregarDetalleVendedor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GenericoAgregarDetalle, StdCtrls, DB, Buttons, ExtCtrls, Mask,
  rxToolEdit, rxCurrEdit, GenericoBuscador, DBClient, Utils,
  ClsTipoComision;

type
  TFrmAgregarDetalleVendedor = class(TFrmGenericoAgregarDetalle)
    TxtDescripcionProducto: TEdit;
    TxtPorcentaje: TCurrencyEdit;
    LblPorcentaje: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure BtnBuscarClick(Sender: TObject);
    procedure TxtCodigoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    TipoComision: TTipoComision;
  protected
    function Buscar: Boolean; override;
    procedure Agregar; override;
    procedure Limpiar; override;
  public
    { Public declarations }
  end;

var
  FrmAgregarDetalleVendedor: TFrmAgregarDetalleVendedor;

implementation

{$R *.dfm}

procedure TFrmAgregarDetalleVendedor.Agregar;
begin
  with Data do
  begin
    //Verificando si existe el codigo en el Grid
    if (Locate('codigo',TxtCodigo.Text,[loCaseInsensitive])) then
    begin
      MessageBox(Self.Handle, Cadena('El Codigo Ya Existe!!!'), PChar(MsgTituloError), MB_ICONSTOP or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      ModalResult := mrNone;
    end
    else
    begin
      Append;
      FieldValues['codigo'] := TipoComision.GetCodigo;
      FieldValues['descripcion'] := TipoComision.GetDescripcion;
      FieldValues['porcentaje'] := TipoComision.GetPorcentaje;
      Post;
    end;
  end;
end;

procedure TFrmAgregarDetalleVendedor.BtnBuscarClick(Sender: TObject);
begin
  inherited;
  Buscador(Self,TipoComision,'codigo','Buscar Tipo Comisi�n',TxtCodigo,nil);
  if (Length(Trim(TxtCodigo.Text)) > 0) then
  begin
    Buscar;
  end;
end;

function TFrmAgregarDetalleVendedor.Buscar: Boolean;
begin
  TipoComision.SetCodigo(TxtCodigo.Text);
  Result := TipoComision.Buscar;
  if (Result) then
  begin
    TxtDescripcionProducto.Text := TipoComision.GetDescripcion;
    TxtPorcentaje.Value := TipoComision.GetPorcentaje;
  end
  else
  begin
    MessageBox(Self.Handle, Cadena('El Codigo No Existe!!!'), PChar(MsgTituloError), MB_ICONSTOP or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Limpiar;
  end;
end;

procedure TFrmAgregarDetalleVendedor.FormCreate(Sender: TObject);
begin
  inherited;
  ComponenteFoco := nil;
  TipoComision := TTipoComision.Create;
end;

procedure TFrmAgregarDetalleVendedor.Limpiar;
begin
  inherited;
  TxtDescripcionProducto.Text := '';
  TxtPorcentaje.Value := 0;
end;

procedure TFrmAgregarDetalleVendedor.TxtCodigoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  case Key of
    VK_RETURN:
      Agregar;
  end;
end;

end.
