unit RaveStatus;

interface

uses
  Windows, Messages, Graphics, Controls, Forms, Dialogs, StdCtrls, Buttons,
  SysUtils, Classes, RpSystem, RpDefine, XPMan;

type
  TFormRaveStatus = class(TForm)
    StatusLabel: TLabel;
    TemaXP: TXPManifest;
    BtnCancelar: TBitBtn;
    procedure BtnCancelarClick(Sender: TObject);
  private
  public
    ReportSystem: TRvSystem;
    FormClosed: boolean;
  end;

var
  FormRaveStatus: TFormRaveStatus;

implementation

{$R *.dfm}

procedure TFormRaveStatus.BtnCancelarClick(Sender: TObject);
begin
  with Sender as TButton do
  begin
    if ModalResult = mrCancel then ReportSystem.BaseReport.Abort
    else FormClosed := true;
  end;
end;

end.
