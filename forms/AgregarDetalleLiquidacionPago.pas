unit AgregarDetalleLiquidacionPago;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GenericoAgregarDetalle, StdCtrls, Buttons, ExtCtrls,
  ClsBusquedaDocumentoCuentaxPagar, ClsAnticipoProveedor,
  ClsFacturacionProveedor, ClsNotaDebitoProveedor, ClsNotaCreditoProveedor,
  ClsDetLiquidacionPago, ClsCuentaxPagar, ClsTipoRetencion, ClsProveedor,
  ClsDetRetencionIslr, ClsDetRetencionIva, GenericoBuscador, DBClient,
  Utils, DB, Mask, rxToolEdit, rxCurrEdit, RxLookup;

type
  TFrmAgregarDetalleLiquidacionPago = class(TFrmGenericoAgregarDetalle)
    LblNumero: TLabel;
    LblProveedor: TLabel;
    LblDesde: TLabel;
    LblDescripcionDesde: TLabel;
    LblTotalRetencion: TLabel;
    LblMontoBase: TLabel;
    LblRazonSocialProveedor: TLabel;
    LblAbono: TLabel;
    LblNroCuota: TLabel;
    LblMontoTotalAbono: TLabel;
    GrpRetencionesIslr: TGroupBox;
    CmbCodigoRetencion1: TRxDBLookupCombo;
    BtnRetencion1: TBitBtn;
    TxtMontoRetencion1: TCurrencyEdit;
    CmbCodigoRetencion2: TRxDBLookupCombo;
    BtnRetencion2: TBitBtn;
    TxtMontoRetencion2: TCurrencyEdit;
    CmbCodigoRetencion3: TRxDBLookupCombo;
    BtnRetencion3: TBitBtn;
    TxtMontoRetencion3: TCurrencyEdit;
    CmbCodigoRetencion4: TRxDBLookupCombo;
    BtnRetencion4: TBitBtn;
    TxtMontoRetencion4: TCurrencyEdit;
    CmbCodigoRetencion5: TRxDBLookupCombo;
    BtnRetencion5: TBitBtn;
    TxtMontoRetencion5: TCurrencyEdit;
    BtnBorrarRetencion1: TBitBtn;
    BtnBorrarRetencion2: TBitBtn;
    BtnBorrarRetencion3: TBitBtn;
    BtnBorrarRetencion4: TBitBtn;
    BtnBorrarRetencion5: TBitBtn;
    TxtDesde: TEdit;
    TxtNumero: TEdit;
    TxtCodigoProveedor: TEdit;
    TxtTotalRetencion: TCurrencyEdit;
    GrpRetencionIva: TGroupBox;
    CmbCodigoRetencionIva: TRxDBLookupCombo;
    BtnRetencionIva: TBitBtn;
    TxtMontoRetencionIva: TCurrencyEdit;
    BtnBorrarRetencionIva: TBitBtn;
    TxtAbono: TCurrencyEdit;
    TxtNroCuota: TEdit;
    PanelNoAplicaISLR: TPanel;
    PanelNoAplicaIVA: TPanel;
    DSTipoRetencion1: TDataSource;
    DSTipoRetencion2: TDataSource;
    DSTipoRetencion3: TDataSource;
    DSTipoRetencion4: TDataSource;
    DSTipoRetencion5: TDataSource;
    DSTipoRetencionIva: TDataSource;
    procedure TxtCodigoExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtnBuscarClick(Sender: TObject);
  private
    { Private declarations }
    ArrayVariant: TArrayVariant;
    DetLiquidacionPago: TDetLiquidacionPago;
    CuentaxPagar: TCuentaxPagar;
    TipoRetencion: TTipoRetencion;
    AnticipoProveedor: TAnticipoProveedor;
    FacturaProveedor: TFacturacionProveedor;
    NotaDebitoProveedor: TNotaDebitoProveedor;
    NotaCreditoProveedor: TNotaCreditoProveedor;
    Proveedor: TProveedor;
    DetRetencionISLR: TDetRetencionIslr;
    DetRetencionIVA: TDetRetencionIva;
    DataTipoRetencion1: TClientDataSet;
    DataTipoRetencion2: TClientDataSet;
    DataTipoRetencion3: TClientDataSet;
    DataTipoRetencion4: TClientDataSet;
    DataTipoRetencion5: TClientDataSet;
    DataTipoRetencionIva: TClientDataSet;
    procedure CalcularTotalRetencion;
  protected
    { Protected declarations }
    function Buscar: Boolean; override;
    procedure Agregar; override;
    procedure Limpiar; override;
    function Validar: Boolean;
  public
    { Public declarations }
    CodigoProveedor: string;
    BusquedaDocumentoCuentaxPagar: TBusquedaDocumentoCuentaxPagar;
  end;

var
  FrmAgregarDetalleLiquidacionPago: TFrmAgregarDetalleLiquidacionPago;

implementation

uses
  Math,
  Types, TipoRetencion;

{$R *.dfm}

{ TFrmAgregarDetalleLiquidacionPago }

procedure TFrmAgregarDetalleLiquidacionPago.Agregar;
begin
  if (Validar) then
  begin
    with Data do
    begin
      CuentaxPagar.SetDesde(BusquedaDocumentoCuentaxPagar.GetDesde);
      CuentaxPagar.SetNumeroDocumento(BusquedaDocumentoCuentaxPagar.GetNumeroDocumento);
      CuentaxPagar.SetNumeroCuota(BusquedaDocumentoCuentaxPagar.GetNumeroCuota);
      CuentaxPagar.BuscarPago;
      Append;
      FieldValues['desde'] := CuentaxPagar.GetDesde;
      FieldValues['numero_documento'] := CuentaxPagar.GetNumeroDocumento;
      if (BusquedaDocumentoCuentaxPagar.GetDesde = 'AP') then
      begin //Anticipo de Proveedor
        AnticipoProveedor.SetNumero(BusquedaDocumentoCuentaxPagar.GetNumeroDocumento);
        AnticipoProveedor.BuscarNumero;
        FieldValues['monto_base'] := AnticipoProveedor.GetMontoNeto;
        FieldValues['p_iva'] := 0;
        FieldValues['impuesto'] := 0;
        FieldValues['total'] := AnticipoProveedor.GetMontoNeto;
      end
      else
      begin
        if (BusquedaDocumentoCuentaxPagar.GetDesde = 'NDP') then
        begin //Nota Debito Proveedor
          NotaDebitoProveedor.SetNumero(BusquedaDocumentoCuentaxPagar.GetNumeroDocumento);
          NotaDebitoProveedor.BuscarNumero;
          FieldValues['monto_base'] := NotaDebitoProveedor.GetMontoNeto;
          FieldValues['p_iva'] := NotaDebitoProveedor.GetPIva;
          FieldValues['impuesto'] := NotaDebitoProveedor.GetImpuesto;
          FieldValues['total'] := NotaDebitoProveedor.GetTotal;
        end
        else
        begin
          if (BusquedaDocumentoCuentaxPagar.GetDesde = 'NCP') then
          begin //Nota Credito Proveedor
            NotaCreditoProveedor.SetNumero(BusquedaDocumentoCuentaxPagar.GetNumeroDocumento);
            NotaCreditoProveedor.BuscarNumero;
            FieldValues['monto_base'] := NotaCreditoProveedor.GetMontoNeto;
            FieldValues['p_iva'] := NotaCreditoProveedor.GetPIva;
            FieldValues['impuesto'] := NotaCreditoProveedor.GetImpuesto;
            FieldValues['total'] := NotaCreditoProveedor.GetTotal;
          end
          else
          begin //Factura de Cliente
            FacturaProveedor.SetNumero(BusquedaDocumentoCuentaxPagar.GetNumeroDocumento);
            FacturaProveedor.BuscarNumero;
            FieldValues['monto_base'] := FacturaProveedor.GetMontoNeto;
            FieldValues['p_iva'] := FacturaProveedor.GetPIva;
            FieldValues['impuesto'] := FacturaProveedor.GetImpuesto;
            FieldValues['total'] := FacturaProveedor.GetTotal;
          end;
        end;
      end;
      FieldValues['id_pago'] := CuentaxPagar.GetIdPago;
      FieldValues['orden_pago'] := CuentaxPagar.GetOrdenPago;
      FieldValues['numero_cuota'] := CuentaxPagar.GetNumeroCuota;
      FieldValues['abonado'] := TxtAbono.Value;
      FieldValues['codigo_retencion_iva'] := CmbCodigoRetencionIva.Text;
      FieldValues['monto_retencion_iva'] := TxtMontoRetencionIva.Value;
      if (TxtMontoRetencionIva.Value > 0) then
      begin
        FieldValues['descripcion_retencion_iva'] := DataTipoRetencionIva.FieldValues['descripcion'];
      end
      else
      begin
        FieldValues['descripcion_retencion_iva'] := '';
      end;
      FieldValues['codigo_retencion_1'] := CmbCodigoRetencion1.Text;
      FieldValues['monto_retencion_1'] := TxtMontoRetencion1.Value;
      if (TxtMontoRetencion1.Value > 0) then
      begin
        FieldValues['descripcion_retencion_1'] := DataTipoRetencion1.FieldValues['descripcion'];
      end
      else
      begin
        FieldValues['descripcion_retencion_1'] := '';
      end;
      FieldValues['codigo_retencion_2'] := CmbCodigoRetencion2.Text;
      FieldValues['monto_retencion_2'] := TxtMontoRetencion2.Value;
      if (TxtMontoRetencion2.Value > 0) then
      begin
        FieldValues['descripcion_retencion_2'] := DataTipoRetencion2.FieldValues['descripcion'];
      end
      else
      begin
        FieldValues['descripcion_retencion_2'] := '';
      end;
      FieldValues['codigo_retencion_3'] := CmbCodigoRetencion3.Text;
      FieldValues['monto_retencion_3'] := TxtMontoRetencion3.Value;
      if (TxtMontoRetencion3.Value > 0) then
      begin
        FieldValues['descripcion_retencion_3'] := DataTipoRetencion3.FieldValues['descripcion'];
      end
      else
      begin
        FieldValues['descripcion_retencion_3'] := '';
      end;
      FieldValues['codigo_retencion_4'] := CmbCodigoRetencion4.Text;
      FieldValues['monto_retencion_4'] := TxtMontoRetencion4.Value;
      if (TxtMontoRetencion4.Value > 0) then
      begin
        FieldValues['descripcion_retencion_4'] := DataTipoRetencion4.FieldValues['descripcion'];
      end
      else
      begin
        FieldValues['descripcion_retencion_4'] := '';
      end;
      FieldValues['codigo_retencion_5'] := CmbCodigoRetencion5.Text;
      FieldValues['monto_retencion_5'] := TxtMontoRetencion5.Value;
      if (TxtMontoRetencion5.Value > 0) then
      begin
        FieldValues['descripcion_retencion_5'] := DataTipoRetencion5.FieldValues['descripcion'];
      end
      else
      begin
        FieldValues['descripcion_retencion_5'] := '';
      end;
      Post;
    end;
    inherited;
  end
  else
  begin
    ModalResult := mrNone;
  end;
end;

procedure TFrmAgregarDetalleLiquidacionPago.BtnBuscarClick(Sender: TObject);
var
  Condicion: string;
begin
  inherited;
  Condicion := '"codigo_proveedor" = ' + QuotedStr(CodigoProveedor);
  if (Buscador(Self,BusquedaDocumentoCuentaxPagar,'desde;numero_documento;codigo_proveedor;numero_cuota','Buscar Documento',nil,nil,True,ArrayVariant,Condicion)) then
  begin
    Buscar;
  end
  else
  begin
    SetLength(ArrayVariant,0);
  end;
end;

function TFrmAgregarDetalleLiquidacionPago.Buscar: Boolean;
begin
  if (Length(ArrayVariant) > 0) then
  begin
    BusquedaDocumentoCuentaxPagar.SetDesde(ArrayVariant[0]);
    BusquedaDocumentoCuentaxPagar.SetNumeroDocumento(ArrayVariant[1]);
    BusquedaDocumentoCuentaxPagar.SetCodigoProveedor(ArrayVariant[2]);
    BusquedaDocumentoCuentaxPagar.SetNumeroCuota(ArrayVariant[3]);
    Result := BusquedaDocumentoCuentaxPagar.Buscar;
    if (Result) then
    begin
      TxtDesde.Text := BusquedaDocumentoCuentaxPagar.GetDesde;
      if (BusquedaDocumentoCuentaxPagar.GetDesde = 'AP') then
      begin
        LblDescripcionDesde.Caption := 'Anticipo a Proveedor';
        AnticipoProveedor.SetNumero(BusquedaDocumentoCuentaxPagar.GetNumeroDocumento);
        AnticipoProveedor.BuscarNumero;
      end
      else
      begin
        if (BusquedaDocumentoCuentaxPagar.GetDesde = 'NDP') then
        begin
          LblDescripcionDesde.Caption := 'Nota de D�bito de Proveedor';
          NotaDebitoProveedor.SetNumero(BusquedaDocumentoCuentaxPagar.GetNumeroDocumento);
          NotaDebitoProveedor.BuscarNumero;
        end
        else
        begin
          if (BusquedaDocumentoCuentaxPagar.GetDesde = 'NCP') then
          begin
            LblDescripcionDesde.Caption := 'Nota de Cr�dito a Proveedor';
            NotaCreditoProveedor.SetNumero(BusquedaDocumentoCuentaxPagar.GetNumeroDocumento);
            NotaCreditoProveedor.BuscarNumero;
          end
          else
          begin
            LblDescripcionDesde.Caption := 'Factura de Proveedor';
            FacturaProveedor.SetNumero(BusquedaDocumentoCuentaxPagar.GetNumeroDocumento);
            FacturaProveedor.BuscarNumero;
          end;
        end;
      end;
      TxtNumero.Text := BusquedaDocumentoCuentaxPagar.GetNumeroDocumento;
      TxtCodigoProveedor.Text := BusquedaDocumentoCuentaxPagar.GetCodigoProveedor;
      Proveedor.SetCodigo(BusquedaDocumentoCuentaxPagar.GetCodigoProveedor);
      Proveedor.BuscarCodigo;
      LblRazonSocialProveedor.Caption := Proveedor.GetRazonSocial;
      LblMontoBase.Caption := 'Monto Documento: ' + FormatFloat('#,##0.0000',BusquedaDocumentoCuentaxPagar.GetMontoNetoDocumento);
      LblMontoTotalAbono.Caption := 'Monto a Pagar: ' + FormatFloat('#,##0.0000',BusquedaDocumentoCuentaxPagar.GetTotal);
      if (BusquedaDocumentoCuentaxPagar.GetNumeroCuota = -1) then
      begin
        TxtNroCuota.Text := 'No Aplica';
      end
      else
      begin
        TxtNroCuota.Text := IntToStr(BusquedaDocumentoCuentaxPagar.GetNumeroCuota) + '/' + IntToStr(BusquedaDocumentoCuentaxPagar.GetTotalCuotas);
      end;
      TxtAbono.Value := BusquedaDocumentoCuentaxPagar.GetTotal;
      //Verificando Tipo de Documento
      if (BusquedaDocumentoCuentaxPagar.GetDesde = 'AP') then
      begin
        PanelNoAplicaISLR.Visible := True;
        PanelNoAplicaIVA.Visible := True;
      end
      else
      begin
        PanelNoAplicaISLR.Visible := False;
        PanelNoAplicaIVA.Visible := False;
      end;
      //Limpiando Datos de la Retencion
      CmbCodigoRetencion1.Value := '';
      TxtMontoRetencion1.Value := 0;
      CmbCodigoRetencion2.Value := '';
      TxtMontoRetencion2.Value := 0;
      CmbCodigoRetencion3.Value := '';
      TxtMontoRetencion3.Value := 0;
      CmbCodigoRetencion4.Value := '';
      TxtMontoRetencion4.Value := 0;
      CmbCodigoRetencion5.Value := '';
      TxtMontoRetencion5.Value := 0;
      //Verificando Retenciones ISLR
      if (DetLiquidacionPago.VerificarRetencionesISLR(BusquedaDocumentoCuentaxPagar.GetIdPago)) then
      begin
        PanelNoAplicaISLR.Visible := True;
      end
      else
      begin
        //Se Ubica La Retencion En Caso de Haber Sido Realizada
        DetRetencionISLR.SetDesde(BusquedaDocumentoCuentaxPagar.GetDesde);
        DetRetencionISLR.SetNumeroDocumento(BusquedaDocumentoCuentaxPagar.GetNumeroDocumento);
        if (DetRetencionISLR.BuscarRetencionDocumento) then
        begin
          PanelNoAplicaISLR.Visible := False;
          //Moviendo Datos de la Retencion
          CmbCodigoRetencion1.Value := DetRetencionISLR.GetCodigoRetencion1;
          TxtMontoRetencion1.Value := DetRetencionISLR.GetMontoRetencion1;
          CmbCodigoRetencion2.Value := DetRetencionISLR.GetCodigoRetencion2;
          TxtMontoRetencion2.Value := DetRetencionISLR.GetMontoRetencion2;
          CmbCodigoRetencion3.Value := DetRetencionISLR.GetCodigoRetencion3;
          TxtMontoRetencion3.Value := DetRetencionISLR.GetMontoRetencion3;
          CmbCodigoRetencion4.Value := DetRetencionISLR.GetCodigoRetencion4;
          TxtMontoRetencion4.Value := DetRetencionISLR.GetMontoRetencion4;
          CmbCodigoRetencion5.Value := DetRetencionISLR.GetCodigoRetencion5;
          TxtMontoRetencion5.Value := DetRetencionISLR.GetMontoRetencion5;
        end
        else
        begin
          PanelNoAplicaISLR.Visible := True;
        end;
      end;
      //Limpiando Datos de la Retencion
      CmbCodigoRetencionIva.Value := '';
      TxtMontoRetencionIva.Value := 0;
      //Verificando Retenciones IVA
      if (DetLiquidacionPago.VerificarRetencionesIVA(BusquedaDocumentoCuentaxPagar.GetIdPago)) then
      begin
        PanelNoAplicaIVA.Visible := True;
      end
      else
      begin
        //Se Ubica La Retencion En Caso de Haber Sido Realizada
        DetRetencionIVA.SetDesde(BusquedaDocumentoCuentaxPagar.GetDesde);
        DetRetencionIVA.SetNumeroDocumento(BusquedaDocumentoCuentaxPagar.GetNumeroDocumento);
        if (DetRetencionIVA.BuscarRetencionDocumento) then
        begin
          PanelNoAplicaIVA.Visible := False;
          //Moviendo Datos de la Retencion
          CmbCodigoRetencionIva.Value := DetRetencionIVA.GetCodigoRetencion;
          TxtMontoRetencionIva.Value := DetRetencionIVA.GetMontoRetencion;
        end
        else
        begin
          PanelNoAplicaIVA.Visible := True;
        end;
      end;
      CalcularTotalRetencion;
      TxtAbono.SetFocus;
    end
    else
    begin
      MessageBox(Self.Handle, Cadena('El Documento No Existe!!!'), PChar(MsgTituloError), MB_ICONSTOP or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      GrpRetencionesIslr.Enabled := False;
      GrpRetencionIva.Enabled := False;
      Limpiar;
    end;
  end
  else
  begin
    Result := False;
  end;
end;

procedure TFrmAgregarDetalleLiquidacionPago.CalcularTotalRetencion;
begin
  TxtTotalRetencion.Value := TxtMontoRetencion1.Value +
                             TxtMontoRetencion2.Value +
                             TxtMontoRetencion3.Value +
                             TxtMontoRetencion4.Value +
                             TxtMontoRetencion5.Value +
                             TxtMontoRetencionIva.Value;
end;

procedure TFrmAgregarDetalleLiquidacionPago.FormCreate(Sender: TObject);
begin
  inherited;
  ComponenteFoco := nil;
  BusquedaDocumentoCuentaxPagar := TBusquedaDocumentoCuentaxPagar.Create;
  DetLiquidacionPago := TDetLiquidacionPago.Create;
  CuentaxPagar := TCuentaxPagar.Create;
  TipoRetencion := TTipoRetencion.Create;
  AnticipoProveedor := TAnticipoProveedor.Create;
  FacturaProveedor := TFacturacionProveedor.Create;
  NotaDebitoProveedor := TNotaDebitoProveedor.Create;
  NotaCreditoProveedor := TNotaCreditoProveedor.Create;
  Proveedor := TProveedor.Create;
  DetRetencionISLR := TDetRetencionIslr.Create;
  DetRetencionIVA := TDetRetencionIva.Create;
  DataTipoRetencion1 := TipoRetencion.ObtenerComboISLR;
  DataTipoRetencion2 := TipoRetencion.ObtenerComboISLR;
  DataTipoRetencion3 := TipoRetencion.ObtenerComboISLR;
  DataTipoRetencion4 := TipoRetencion.ObtenerComboISLR;
  DataTipoRetencion5 := TipoRetencion.ObtenerComboISLR;
  DataTipoRetencionIva := TipoRetencion.ObtenerComboIVA;
  DataTipoRetencion1.First;
  DataTipoRetencion2.First;
  DataTipoRetencion3.First;
  DataTipoRetencion4.First;
  DataTipoRetencion5.First;
  DataTipoRetencionIva.First;
  DSTipoRetencion1.DataSet := DataTipoRetencion1;
  DSTipoRetencion2.DataSet := DataTipoRetencion2;
  DSTipoRetencion3.DataSet := DataTipoRetencion3;
  DSTipoRetencion4.DataSet := DataTipoRetencion4;
  DSTipoRetencion5.DataSet := DataTipoRetencion5;
  DSTipoRetencionIva.DataSet := DataTipoRetencionIva;
  ConfigurarRxDBLookupCombo(CmbCodigoRetencion1,DSTipoRetencion1,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoRetencion2,DSTipoRetencion2,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoRetencion3,DSTipoRetencion3,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoRetencion4,DSTipoRetencion4,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoRetencion5,DSTipoRetencion5,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoRetencionIva,DSTipoRetencionIva,'codigo','codigo;descripcion');
end;

procedure TFrmAgregarDetalleLiquidacionPago.Limpiar;
begin
  inherited;
  TxtDesde.Text := '';
  LblDescripcionDesde.Caption := '';
  TxtNumero.Text := '';
  LblMontoBase.Caption := '';
  TxtCodigoProveedor.Text := '';
  LblRazonSocialProveedor.Caption := '';
  CmbCodigoRetencion1.Value := '';
  TxtMontoRetencion1.Text := '';
  CmbCodigoRetencion2.Value := '';
  TxtMontoRetencion2.Text := '';
  CmbCodigoRetencion3.Value := '';
  TxtMontoRetencion3.Text := '';
  CmbCodigoRetencion4.Value := '';
  TxtMontoRetencion4.Text := '';
  CmbCodigoRetencion5.Value := '';
  TxtMontoRetencion5.Text := '';
end;

procedure TFrmAgregarDetalleLiquidacionPago.TxtCodigoExit(Sender: TObject);
begin
  Exit;
end;

function TFrmAgregarDetalleLiquidacionPago.Validar: Boolean;
var
  ListaErrores: TStringList;
  sw: Boolean;
begin
  ListaErrores := TStringList.Create;
  //Validando Existencia del Documento a Retener
  sw := false;
  with Data do
  begin
    First;
    while (not Eof) do
    begin
      if ((FieldValues['desde'] = BusquedaDocumentoCuentaxPagar.GetDesde) and
          (FieldValues['numero_documento'] = BusquedaDocumentoCuentaxPagar.GetNumeroDocumento) and
          (FieldValues['numero_cuota'] = BusquedaDocumentoCuentaxPagar.GetNumeroCuota)) then
      begin
        sw := True;
        Break;
      end;      
      Next;
    end;    
  end;
  if (sw) then
  begin
    ListaErrores.Add('El Documento Ya Se Encuentra En La Lista!!!');
  end;
  //Validando Monto de Retencion
  if (TxtTotalRetencion.Value < 0) then
  begin
    ListaErrores.Add('El Total A Retener Debe Ser Mayor o Igual A Cero!!!');
  end;
  //Validando Monto del Abono
  if (CompareValue(TxtAbono.Value,BusquedaDocumentoCuentaxPagar.GetTotal) = GreaterThanValue) then
  begin
    ListaErrores.Add('La Porci�n a Considerar No Debe Ser Mayor al Monto del Documento!!!');
  end;  
  //Validando Codigo de Retencion 1
  if ((Length(Trim(CmbCodigoRetencion1.Text)) > 0) and (TxtMontoRetencion1.Value <= 0)) then
  begin
    ListaErrores.Add('El Monto de Retencion 1 Debe Ser Mayor A Cero!!!');
  end;
  //Validando Codigo de Retencion 2
  if ((Length(Trim(CmbCodigoRetencion2.Text)) > 0) and (TxtMontoRetencion2.Value <= 0)) then
  begin
    ListaErrores.Add('El Monto de Retencion 2 Debe Ser Mayor A Cero!!!');
  end;
  //Validando Repeticion Codigo Retencion 2
  if ((Length(Trim(CmbCodigoRetencion2.Text)) > 0) and (CmbCodigoRetencion1.Text = CmbCodigoRetencion2.Text)) then
  begin
    ListaErrores.Add('La Retencion 2 Ya Fue Utilizada!!!');
  end;
  //Validando Codigo de Retencion 3
  if ((Length(Trim(CmbCodigoRetencion3.Text)) > 0) and (TxtMontoRetencion3.Value <= 0)) then
  begin
    ListaErrores.Add('El Monto de Retencion 3 Debe Ser Mayor A Cero!!!');
  end;
  //Validando Repeticion Codigo Retencion 3
  if ((Length(Trim(CmbCodigoRetencion3.Text)) > 0) and ((CmbCodigoRetencion1.Text = CmbCodigoRetencion3.Text) or (CmbCodigoRetencion2.Text = CmbCodigoRetencion3.Text))) then
  begin
    ListaErrores.Add('La Retencion 3 Ya Fue Utilizada!!!');
  end;
  //Validando Codigo de Retencion 4
  if ((Length(Trim(CmbCodigoRetencion4.Text)) > 0) and (TxtMontoRetencion4.Value <= 0)) then
  begin
    ListaErrores.Add('El Monto de Retencion 4 Debe Ser Mayor A Cero!!!');
  end;
  //Validando Repeticion Codigo Retencion 4
  if ((Length(Trim(CmbCodigoRetencion4.Text)) > 0) and ((CmbCodigoRetencion1.Text = CmbCodigoRetencion4.Text) or (CmbCodigoRetencion2.Text = CmbCodigoRetencion4.Text) or (CmbCodigoRetencion3.Text = CmbCodigoRetencion4.Text))) then
  begin
    ListaErrores.Add('La Retencion 4 Ya Fue Utilizada!!!');
  end;
  //Validando Codigo de Retencion 5
  if ((Length(Trim(CmbCodigoRetencion5.Text)) > 0) and (TxtMontoRetencion5.Value <= 0)) then
  begin
    ListaErrores.Add('El Monto de Retencion 5 Debe Ser Mayor A Cero!!!');
  end;
  //Validando Repeticion Codigo Retencion 5
  if ((Length(Trim(CmbCodigoRetencion5.Text)) > 0) and ((CmbCodigoRetencion1.Text = CmbCodigoRetencion5.Text) or (CmbCodigoRetencion2.Text = CmbCodigoRetencion5.Text) or (CmbCodigoRetencion3.Text = CmbCodigoRetencion5.Text) or (CmbCodigoRetencion4.Text = CmbCodigoRetencion5.Text))) then
  begin
    ListaErrores.Add('La Retencion 5 Ya Fue Utilizada!!!');
  end;
  //Validando Codigo de Retencion IVA
  if ((Length(Trim(CmbCodigoRetencionIva.Text)) > 0) and (TxtMontoRetencionIva.Value <= 0)) then
  begin
    ListaErrores.Add('El Monto de Retencion de IVA Debe Ser Mayor A Cero!!!');
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
