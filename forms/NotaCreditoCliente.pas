unit NotaCreditoCliente;

{$I sophie.inc}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GenericoDetalle, DB, DBClient, ImgList, XPMan, ComCtrls, Grids,
  DBGrids, ExtCtrls, ToolWin, Mask, rxToolEdit, StdCtrls, Buttons, RxLookup,
  rxCurrEdit, Utils, ClsCliente, ClsNotaCreditoCliente, ClsDetNotaCreditoCliente,
  ClsVendedor, ClsEstacion, ClsZona, ClsMoneda, ClsProducto, ClsBusquedaNotaCreditoCliente,
  ClsDetMovimientoInventario, ClsCuentaxCobrar, GenericoBuscador, ClsReporte,
  ClsParametro, ClsCobranzaCobrada, ClsDocumentoLegal, ClsDocumentoAnulado
  {$IFDEF DELPHI10}, ImageList{$ENDIF};

type
  TFrmNotaCreditoCliente = class(TFrmGenericoDetalle)
    TxtNumero: TEdit;
    LblNumero: TLabel;
    LblCodigoCliente: TLabel;
    CmbCodigoCliente: TRxDBLookupCombo;
    LblCodigoVendedor: TLabel;
    CmbCodigoVendedor: TRxDBLookupCombo;
    LblCodigoEstacion: TLabel;
    CmbCodigoEstacion: TRxDBLookupCombo;
    LblCodigoZona: TLabel;
    CmbCodigoZona: TRxDBLookupCombo;
    LblNumeroFacturaAsociada: TLabel;
    LblMoneda: TLabel;
    CmbCodigoMoneda: TRxDBLookupCombo;
    BtnBuscarCliente: TBitBtn;
    BtnBuscarVendedor: TBitBtn;
    BtnBuscarEstacion: TBitBtn;
    BtnBuscarZona: TBitBtn;
    BtnBuscarMoneda: TBitBtn;
    LblFechaIngreso: TLabel;
    DatFechaIngreso: TDateEdit;
    LblRecepcion: TLabel;
    DatFechaRecepcion: TDateEdit;
    LblFechaContable: TLabel;
    DatFechaContable: TDateEdit;
    LblFechaLibros: TLabel;
    DatFechaLibros: TDateEdit;
    TxtNumeroFacturaAsociada: TEdit;
    TabConcepto: TTabSheet;
    TxtConcepto: TRichEdit;
    TabSheet1: TTabSheet;
    TxtObservaciones: TRichEdit;
    DSCliente: TDataSource;
    DSVendedor: TDataSource;
    DSEstacion: TDataSource;
    DSZona: TDataSource;
    DSMoneda: TDataSource;
    LblMontoNeto: TLabel;
    TxtMontoNeto: TCurrencyEdit;
    LblMontoDescuento: TLabel;
    TxtMontoDescuento: TCurrencyEdit;
    TxtPDescuento: TLabel;
    LblSubTotal: TLabel;
    TxtSubTotal: TCurrencyEdit;
    LblImpuesto: TLabel;
    TxtImpuesto: TCurrencyEdit;
    TxtPIVA: TLabel;
    LblTotal: TLabel;
    TxtTotal: TCurrencyEdit;
    Datacodigo_producto: TStringField;
    Datareferencia: TStringField;
    Datadescripcion: TStringField;
    Datacantidad: TFloatField;
    Dataprecio: TFloatField;
    Datap_descuento: TFloatField;
    Datamonto_descuento: TFloatField;
    Dataimpuesto: TFloatField;
    Datap_iva: TFloatField;
    Datatotal: TFloatField;
    Dataid_facturacion_cliente: TIntegerField;
    procedure TxtNumeroFacturaAsociadaExit(Sender: TObject);
    procedure CmbCodigoClienteExit(Sender: TObject);
    procedure TxtMontoNetoExit(Sender: TObject);
    procedure TxtPIVADblClick(Sender: TObject);
    procedure TxtPDescuentoDblClick(Sender: TObject);
    procedure LblMontoDescuentoDblClick(Sender: TObject);
    procedure LblMontoNetoDblClick(Sender: TObject);
    procedure CmbCodigoClienteChange(Sender: TObject);
    procedure LblImpuestoDblClick(Sender: TObject);
    procedure DataAfterPost(DataSet: TDataSet);
    procedure TBtnEliminarClick(Sender: TObject);
    procedure BtnBuscarMonedaClick(Sender: TObject);
    procedure BtnBuscarZonaClick(Sender: TObject);
    procedure BtnBuscarEstacionClick(Sender: TObject);
    procedure BtnBuscarVendedorClick(Sender: TObject);
    procedure BtnBuscarClienteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    Clase: TNotaCreditoCliente;
    DetNotaCreditoCliente: TDetnotaCreditoCliente;
    MovimientoInventario: TDetMovimientoInventario;
    CuentaxCobrar: TCuentaxCobrar;
    //CobranzaCobrada: TCobranzaCobrada;
    BuscadorNotaCreditoCliente: TBusquedaNotaCreditoCliente;
    Cliente: TCliente;
    Vendedor: TVendedor;
    Estacion: TEstacion;
    Zona: TZona;
    Moneda: TMoneda;
    Producto: TProducto;
    Reporte: TReporte;
    Parametro: TParametro;
    DocumentoLegal: TDocumentoLegal;
    DocumentoAnulado: TDocumentoAnulado;
    DataBuscadorNotaCreditoCliente: TClientDataSet;
    DataCliente: TClientDataSet;
    DataVendedor: TClientDataSet;
    DataEstacion: TClientDataSet;
    DataZona: TClientDataSet;
    DataMoneda: TClientDataSet;
    procedure GuardarCobranzaCobrada;
    procedure Agregar;
    procedure Editar;
    procedure Eliminar;
    procedure Guardar;
    procedure Cancelar;
    procedure Refrescar;
    procedure Imprimir;
    procedure Buscar;
    procedure Limpiar;
    procedure Asignar;
    procedure Obtener;
    procedure Mover;
    procedure GuardarDetalles;
    //procedure GuardarCobranzaCobrada;
    procedure GuardarCuentaxCobrar;
    procedure GuardarMovimientoInventario;
    procedure CalcularMontos;
    procedure CalcularMontosCabecera;
  protected
    { Protected declarations }
    function Validar: Boolean; override;
    procedure Detalle; override;
  public
    { Public declarations }
  end;

var
  FrmNotaCreditoCliente: TFrmNotaCreditoCliente;

implementation

uses
  ActividadSophie,
  PModule,
  ClsGenericoBD,
  AgregarDetalleNotaCreditoCliente,
  DetalleContadoCobranza,
  SolicitarMonto,
  ClsFacturacionCliente,
  ClsDetFacturacionCliente,
  DocumentoLegal
  {$IFDEF USE_RAVE_REPORTS}
  , DMRR
  {$ENDIF}
  {$IFDEF USE_FAST_REPORTS}
  , DMFR
  {$ENDIF}
  ;

{$R *.dfm}

{ TFrmNotaCreditoCliente }

procedure TFrmNotaCreditoCliente.Agregar;
begin
  //Verificando
  Limpiar;
  //Asignando Valores por Defecto
  TxtPIVA.Caption := FormatFloat('#,##0.0000%',Parametro.ObtenerValor('p_iva'));
  Clase.SetPIva(Parametro.ObtenerValor('p_iva'));
  CmbCodigoVendedor.Value := Parametro.ObtenerValor('codigo_vendedor');
  CmbCodigoEstacion.Value := Parametro.ObtenerValor('codigo_estacion_salida');
  CmbCodigoZona.Value := Parametro.ObtenerValor('codigo_zona');
  CmbCodigoMoneda.Value := Parametro.ObtenerValor('codigo_moneda');
  CmbCodigoCliente.SetFocus;
end;

procedure TFrmNotaCreditoCliente.Asignar;
begin
  if (Clase.GetFormaLibre) then
  begin
    TabDetalle.TabVisible := False;
    PageDetalles.ActivePage := TabConcepto;
    TxtMontoNeto.ReadOnly := False;
  end
  else
  begin
    TabDetalle.TabVisible := True;
    PageDetalles.ActivePage := TabDetalle;
    TxtMontoNeto.ReadOnly := True;
  end;
  with Clase do
  begin
    TxtNumero.Text := GetNumero;
    TxtNumeroFacturaAsociada.Text := GetNumeroFacturaAsociada;
    CmbCodigoCliente.KeyValue := GetCodigoCliente;
    CmbCodigoVendedor.KeyValue := GetCodigoVendedor;
    CmbCodigoEstacion.KeyValue := GetCodigoEstacion;
    CmbCodigoZona.KeyValue := GetCodigoZona;
    CmbCodigoMoneda.KeyValue := GetCodigoMoneda;
    DatFechaIngreso.Date := GetFechaIngreso;
    DatFechaRecepcion.Date := GetFechaRecepcion;
    DatFechaContable.Date := GetFechaContable;
    DatFechaLibros.Date := GetFechaLibros;
    TxtConcepto.Text := GetConcepto;
    TxtObservaciones.Text := GetObservaciones;
    TxtMontoNeto.Value := GetMontoNeto;
    TxtMontoDescuento.Value := GetMontoDescuento;
    TxtSubTotal.Value := GetSubTotal;
    TxtImpuesto.Value := GetImpuesto;
    TxtTotal.Value := GetTotal;
    TxtPDescuento.Caption := FormatFloat('###0.0000%',GetPDescuento);
    TxtPIVA.Caption := FormatFloat('###0.0000%',GetPIva);
    Data.CloneCursor(DetNotaCreditoCliente.ObtenerIdNotaCreditoCliente(GetIdNotaCreditoCliente),False,True);
    DS.DataSet := Data;
  end;
end;

procedure TFrmNotaCreditoCliente.BtnBuscarClienteClick(Sender: TObject);
begin
  Buscador(Self,Cliente,'codigo','Buscar Cliente',nil,DataCliente);
  CmbCodigoCliente.KeyValue := DataCliente.FieldValues['codigo'];
  CmbCodigoClienteExit(Sender);
end;

procedure TFrmNotaCreditoCliente.BtnBuscarEstacionClick(Sender: TObject);
begin
  Buscador(Self,Estacion,'codigo','Buscar Estacion',nil,DataEstacion);
  CmbCodigoEstacion.KeyValue := DataEstacion.FieldValues['codigo'];
end;

procedure TFrmNotaCreditoCliente.BtnBuscarMonedaClick(Sender: TObject);
begin
  Buscador(Self,Moneda,'codigo','Buscar Moneda',nil,DataMoneda);
  CmbCodigoMoneda.KeyValue := DataMoneda.FieldValues['codigo'];
end;

procedure TFrmNotaCreditoCliente.BtnBuscarVendedorClick(Sender: TObject);
begin
  Buscador(Self,Vendedor,'codigo','Buscar Vendedor',nil,DataVendedor);
  CmbCodigoVendedor.KeyValue := DataVendedor.FieldValues['codigo'];
end;

procedure TFrmNotaCreditoCliente.BtnBuscarZonaClick(Sender: TObject);
begin
  Buscador(Self,Zona,'codigo','Buscar Zona',nil,DataZona);
  CmbCodigoZona.KeyValue := DataZona.FieldValues['codigo'];
end;

procedure TFrmNotaCreditoCliente.Buscar;
begin
  if (Buscador(Self,BuscadorNotaCreditoCliente,'numero','Buscar Nota Credito',nil,DataBuscadorNotaCreditoCliente)) then
  begin
    Clase.SetNumero(DataBuscadorNotaCreditoCliente.FieldValues['numero']);
    Clase.BuscarNumero;
    Asignar;
  end;
end;

procedure TFrmNotaCreditoCliente.CalcularMontos;
var
  Neto, Descuento, Impuesto: Currency;
begin
  Neto := 0;
  Descuento := 0;
  Impuesto := 0;
  with Data do
  begin
    DisableControls;
    First;
    while (not Eof) do
    begin
      Neto := Neto + (FieldValues['precio'] * FieldValues['cantidad']);
      Descuento := Descuento + FieldValues['monto_descuento'];
      Impuesto := Impuesto + FieldValues['impuesto'];
      Next;
    end;
    EnableControls;
  end;
  //Asignando Montos
  TxtMontoNeto.Value := Neto;
  //Determinando si hay un Descuento Manual
  if ((TxtMontoDescuento.Value > 0) and (StrToFloat(StringReplace(TxtPDescuento.Caption,'%','',[rfReplaceAll])) > 0) and (Descuento = 0)) then
  begin
    Descuento := TxtMontoDescuento.Value;
  end;
  TxtMontoDescuento.Value := Descuento;
  TxtSubTotal.Value := Neto - Descuento;
  TxtImpuesto.Value := Impuesto;
  TxtTotal.Value := (Neto - Descuento) + Impuesto;
end;

procedure TFrmNotaCreditoCliente.CalcularMontosCabecera;
begin
  if (Clase.GetFormaLibre) then
  begin
    TxtSubTotal.Value := TxtMontoNeto.Value - TxtMontoDescuento.Value;
    TxtTotal.Value := TxtSubTotal.Value + TxtImpuesto.Value;
  end;
end;

procedure TFrmNotaCreditoCliente.Cancelar;
begin
  if (Agregando) then
  begin
    Limpiar;
  end
  else
  begin
    Clase.SetNumero(TxtNumero.Text);
    Clase.BuscarNumero;
    Asignar;
  end;
end;

procedure TFrmNotaCreditoCliente.CmbCodigoClienteChange(Sender: TObject);
begin
  inherited;
  SB.Panels[0].Text := DataCliente.FieldValues['razon_social'];
end;

procedure TFrmNotaCreditoCliente.CmbCodigoClienteExit(Sender: TObject);
begin
  if (Length(Trim(CmbCodigoCliente.Text)) > 0) then
  begin
    if (Agregando) then
    begin
      Cliente.SetCodigo(CmbCodigoCliente.Value);
      Cliente.BuscarCodigo;
      CmbCodigoVendedor.Value := Cliente.GetCodigoVendedor;
      CmbCodigoZona.Value := Cliente.GetCodigoZona;
    end;
  end;
end;

procedure TFrmNotaCreditoCliente.DataAfterPost(DataSet: TDataSet);
begin
  inherited;
  CalcularMontos;
end;

procedure TFrmNotaCreditoCliente.Detalle;
begin
  inherited;
  FrmAgregarDetalleNotaCreditoCliente := TFrmAgregarDetalleNotaCreditoCliente.CrearDetalle(Self,Data);
  FrmAgregarDetalleNotaCreditoCliente.ShowModal;
  FreeAndNil(FrmAgregarDetalleNotaCreditoCliente);
end;

procedure TFrmNotaCreditoCliente.Editar;
begin
  if (Length(Trim(TxtNumero.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
end;

procedure TFrmNotaCreditoCliente.Eliminar;
begin
  //Se Busca La Nota de Credito!
  Clase.SetNumero(TxtNumero.Text);
  Clase.BuscarNumero;
  //Se Registra El Documento Anulado y Eliminan Registros del Documento Legal
  DocumentoLegal.SetDesde('NCC');
  DocumentoLegal.SetNumeroDocumento(Clase.GetNumero);
  if (DocumentoLegal.Buscar) then
  begin
    //Registrando El Documento Anulado
    with DocumentoAnulado do
    begin
      SetDesde('NCC');
      SetNumeroDocumento(Clase.GetNumero);
      SetSerie(DocumentoLegal.GetSerie);
      SetNumeroControl(DocumentoLegal.GetNumeroControl);
      SetFechaDocumento(Clase.GetFechaIngreso);
      SetCodigoCliente(Clase.GetCodigoCliente);
      SetCodigoProveedor('');
      SetCodigoZona(Clase.GetCodigoZona);
      SetSubTotal(Clase.GetSubTotal);
      SetImpuesto(Clase.GetImpuesto);
      SetTotal(Clase.GetTotal);
      SetPIva(Clase.GetPIva);
      SetLoginUsuario(P.User.GetLoginUsuario);
      SetFecha(Date);
      SetHora(Time);
      Insertar;
    end;
    //Borrando Documento Legal
    DocumentoLegal.Eliminar;
  end;
  //Se Eliminan Los Detalles
  DetNotaCreditoCliente.EliminarIdNotaCreditoCliente(Clase.GetIdNotaCreditoCliente);
  //Se Eliminan Los Movimientos de Inventario
  MovimientoInventario.EliminarIdMovimientoInventario(Clase.GetIdMovimientoInventario);
  //Se Eliminan Los Detalles De la Cobranza
  CuentaxCobrar.EliminarIdCobranza(Clase.GetIdCobranza);
  Clase.Eliminar;
  MessageBox(Self.Handle, 'La Nota de Cr�dito de Cliente Se Ha Eliminado Con Exito!!!',PChar(MsgTituloInformacion), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  Refrescar;
  Limpiar;
end;

procedure TFrmNotaCreditoCliente.FormCreate(Sender: TObject);
begin
  inherited;
  ProcAgregar := Agregar;
  ProcEditar := Editar;
  ProcEliminar := Eliminar;
  ProcGuardar := Guardar;
  ProcCancelar := Cancelar;
  ProcRefrescar := Refrescar;
  ProcImprimir := Imprimir;
  ProcBuscar := Buscar;
  FrmActividadSophie.Show;
  FrmActividadSophie.Actividad('Cargando N�cleo...');
  Clase := TNotaCreditoCliente.Create;
  //Inicializando DataSets Secundarios
  BuscadorNotaCreditoCliente := TBusquedaNotaCreditoCliente.Create;
  Cliente := TCliente.Create;
  Vendedor := TVendedor.Create;
  Estacion := TEstacion.Create;
  Zona := TZona.Create;
  Moneda := TMoneda.Create;
  Producto := TProducto.Create;
  Reporte := TReporte.Create;
  Parametro := TParametro.Create;
  DocumentoLegal := TDocumentoLegal.Create;
  DocumentoAnulado := TDocumentoAnulado.Create;
  DetNotaCreditoCliente := TDetNotaCreditoCliente.Create;
  MovimientoInventario := TDetMovimientoInventario.Create;
  CuentaxCobrar := TCuentaxCobrar.Create;
  //CobranzaCobrada := TCobranzaCobrada.Create;
  FrmActividadSophie.Actividad('Cargando Clientes...');
  DataCliente := Cliente.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Vendedores...');
  DataVendedor := Vendedor.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Estaciones...');
  DataEstacion := Estacion.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Zonas...');
  DataZona := Zona.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Monedas...');
  DataMoneda := Moneda.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Resto del N�cleo...');
  DataBuscadorNotaCreditoCliente := BuscadorNotaCreditoCliente.ObtenerLista;
  DataCliente.First;
  DataVendedor.First;
  DataEstacion.First;
  DataZona.First;
  DataMoneda.First;
  DataBuscadorNotaCreditoCliente.First;
  DSCliente.DataSet := DataCliente;
  DSVendedor.DataSet := DataVendedor;
  DSEstacion.DataSet := DataEstacion;
  DSZona.DataSet := DataZona;
  DSMoneda.DataSet := DataMoneda;
  ConfigurarRxDBLookupCombo(CmbCodigoCliente,DSCliente,'codigo','codigo;razon_social');
  ConfigurarRxDBLookupCombo(CmbCodigoVendedor,DSVendedor,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoEstacion,DSEstacion,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoZona,DSZona,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoMoneda,DSMoneda,'codigo','codigo;descripcion');
  DS.DataSet := Data;
  GridDetalle.DataSource := DS;
  FrmActividadSophie.Close;
  //Buscar;
end;

procedure TFrmNotaCreditoCliente.Guardar;
var
  SwDocumentoLegal: Boolean;
begin
  if (Clase.GetFormaLibre) then
  begin
    CalcularMontosCabecera;
    Data.EmptyDataSet;
  end
  else
  begin
    CalcularMontos;
  end;
  Obtener;
  if (Agregando) then
  begin
    //Agregando...
    //Guardando Encabezado
    Clase.Insertar;
    TxtNumero.Text := Clase.GetNumero;
    //Solicitando Datos Legales
    FrmDocumentoLegal := TFrmDocumentoLegal.CreateInsert(Self,'NCC',Clase.GetNumero,'','');
    FrmDocumentoLegal.ShowModal;
    if FrmDocumentoLegal.Aceptado then
    begin //Se Guardan Los Datos Legales
      DocumentoLegal.SetDesde(FrmDocumentoLegal.Desde);
      DocumentoLegal.SetNumeroDocumento(Clase.GetNumero);
      DocumentoLegal.SetSerie(FrmDocumentoLegal.Serie);
      DocumentoLegal.SetNumeroControl(FrmDocumentoLegal.NumeroDocumento);
      DocumentoLegal.SetLoginUsuario(P.User.GetLoginUsuario);
      DocumentoLegal.SetFecha(Date);
      DocumentoLegal.SetHora(Time);
      DocumentoLegal.Insertar;
    end;
    FreeAndNil(FrmDocumentoLegal);
    Application.ProcessMessages;
    //Guardando Detalles
    GuardarDetalles;
    //Guardando Cobranza Cobrada
//    GuardarCobranzaCobrada;
    //Guardando Cuenta x Cobrar
    GuardarCuentaxCobrar;
    //Guardando Movimiento de Inventario
    GuardarMovimientoInventario;
    MessageBox(Self.Handle, 'La Nota de Cr�dito Ha Sido Agregada Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    //Se Imprime la Factura
    MessageBox(Self.Handle, 'Presione "Aceptar" Para Proceder A Imprimir La Nota de Cr�dito de Cliente!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    with Reporte do
    begin
      AgregarParametro('NumeroNotaCreditoCliente',Clase.GetNumero);
      AgregarParametro('MontoLetra',NumeroALetras(Clase.GetTotal));
      if (Clase.GetFormaLibre) then
      begin
        ImprimeReporte('NotaCreditoClienteFormaLibre',trDocumento);
      end
      else
      begin
        ImprimeReporte('NotaCreditoCliente',trDocumento);
      end;
    end;
  end
  else
  begin
    //Editando...
    Clase.Modificar;
    //Solicitando Datos Legales
    DocumentoLegal.SetDesde('NCC');
    DocumentoLegal.SetNumeroDocumento(Clase.GetNumero);
    SwDocumentoLegal := DocumentoLegal.Buscar;
    if (SwDocumentoLegal) then
    begin
      FrmDocumentoLegal := TFrmDocumentoLegal.CreateEdit(Self,'NCC',Clase.GetNumero,DocumentoLegal.GetSerie,DocumentoLegal.GetNumeroControl);
    end
    else
    begin
      FrmDocumentoLegal := TFrmDocumentoLegal.CreateInsert(Self,'NCC',Clase.GetNumero,'','');
    end;
    FrmDocumentoLegal.ShowModal;
    if FrmDocumentoLegal.Aceptado then
    begin //Se Guardan Los Datos Legales
      DocumentoLegal.SetSerie(FrmDocumentoLegal.Serie);
      DocumentoLegal.SetNumeroControl(FrmDocumentoLegal.NumeroControl);
      DocumentoLegal.SetLoginUsuario(P.User.GetLoginUsuario);
      DocumentoLegal.SetFecha(Date);
      DocumentoLegal.SetHora(Time);
      if (SwDocumentoLegal) then
      begin
        DocumentoLegal.Modificar;
      end
      else
      begin
        DocumentoLegal.Insertar;
      end;
    end;
    FreeAndNil(FrmDocumentoLegal);
    //Actualizando Detalles (Eliminando Anteriores y Cargando los Nuevos)
    DetNotaCreditoCliente.EliminarIdNotaCreditoCliente(Clase.GetIdNotaCreditoCliente);
    //Guardando Detalles
    GuardarDetalles;
    //Guardando Cobranza Cobrada
//    GuardarCobranzaCobrada;
    //Guardando Cuenta x Cobrar
    GuardarCuentaxCobrar;
    //Guardando Movimiento de Inventario
    GuardarMovimientoInventario;
    MessageBox(Self.Handle, 'La Nota de Cr�dito de Cliente Ha Sido Editada Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end;
end;

procedure TFrmNotaCreditoCliente.GuardarCobranzaCobrada;
begin
{  if (Editando) then
  begin
    CuentaxCobrar.EliminarIdCobranza(Clase.GetIdCobranza);
  end;
  with CuentaxCobrar do //Se Inserta la CuentaxCobrar
  begin
    SetDesde('AC');
    SetNumeroDocumento(Clase.GetNumero);
    SetCodigoMoneda(Clase.GetCodigoMoneda);
    SetCodigoCliente(Clase.GetCodigoCliente);
    SetCodigoVendedor('');
    SetCodigoZona(Clase.GetCodigoZona);
    SetFechaIngreso(Clase.GetFechaIngreso);
    SetFechaVencIngreso(Clase.GetFechaIngreso);
    SetFechaRecepcion(Clase.GetFechaRecepcion);
    SetFechaVencRecepcion(Clase.GetFechaRecepcion);
    SetFechaContable(Clase.GetFechaContable);
    SetFechaCobranza(clase.GetFechaIngreso);
    SetFechaLibros(Clase.GetFechaLibros);
    SetNumeroCuota(-1);
    SetTotalCuotas(-1);
    SetOrdenCobranza(1);
    SetMontoNeto(Clase.GetMontoNeto);
    SetImpuesto(0);
    SetTotal(Clase.GetMontoNeto);
    SetPIva(0);
    SetIdCobranza(Clase.GetIdCobranza);
    SetIdContadoCobranza(Clase.GetIdContadoCobranza);
    Insertar;
  end;}
end;

procedure TFrmNotaCreditoCliente.GuardarCuentaxCobrar;
begin
  if (Editando) then
  begin
    CuentaxCobrar.EliminarIdCobranza(Clase.GetIdCobranza);
  end;
  with CuentaxCobrar do //Se Inserta la CuentaxCobrar
  begin
    SetDesde('NCC');
    SetNumeroDocumento(Clase.GetNumero);
    SetCodigoMoneda(Clase.GetCodigoMoneda);
    SetCodigoCliente(Clase.GetCodigoCliente);
    SetCodigoVendedor(Clase.GetCodigoVendedor);
    SetCodigoZona(Clase.GetCodigoZona);
    SetFechaIngreso(Clase.GetFechaIngreso);
    SetFechaVencIngreso(Clase.GetFechaIngreso);
    SetFechaRecepcion(Clase.GetFechaRecepcion);
    SetFechaVencRecepcion(Clase.GetFechaRecepcion);
    SetFechaContable(Clase.GetFechaContable);
    SetFechaCobranza(Clase.GetFechaIngreso);
    SetFechaLibros(Clase.GetFechaLibros);
    SetNumeroCuota(-1);
    SetTotalCuotas(-1);
    SetOrdenCobranza(1);
    SetMontoNeto(Clase.GetMontoNeto);
    SetImpuesto(Clase.GetImpuesto);
    SetTotal(Clase.GetTotal);
    SetPIva(Clase.GetPIva);
    SetIdCobranza(Clase.GetIdCobranza);
    SetIdContadoCobranza(-1);
    Insertar;
  end;
end;

procedure TFrmNotaCreditoCliente.GuardarDetalles;
begin
  with Data do
  begin
    DisableControls;
    First;
    while (not Eof) do
    begin
      with DetNotaCreditoCliente do
      begin
        SetCodigoProducto(FieldValues['codigo_producto']);
        SetReferencia(FieldValues['referencia']);
        SetDescripcion(FieldValues['descripcion']);
        SetCantidad(FieldValues['cantidad']);
        SetPrecio(FieldValues['precio']);
        SetPDescuento(FieldValues['p_descuento']);
        SetMontoDescuento(FieldValues['monto_descuento']);
        SetImpuesto(FieldValues['impuesto']);
        SetPIva(FieldValues['p_iva']);
        SetTotal(FieldValues['total']);
        SetIdNotaCreditoCliente(Clase.GetIdNotaCreditoCliente);
        Insertar;
      end;
      Next;
    end;
    EnableControls;
  end;
end;

procedure TFrmNotaCreditoCliente.GuardarMovimientoInventario;
begin
  if (Editando)then
  begin
    MovimientoInventario.EliminarIdMovimientoInventario(Clase.GetIdMovimientoInventario);
  end;
  with Data do
  begin
    DisableControls;
    First;
    while (not Eof) do
    begin
      Producto.SetCodigo(FieldValues['codigo_producto']);
      Producto.BuscarCodigo;
      if (Producto.GetInventario) then
      begin
        with MovimientoInventario do
        begin
          SetDesde('NCC');
          SetNumeroDocumento(Clase.GetNumero);
          SetCodigoProducto(Producto.GetCodigo);
          SetCantidad(FieldValues['cantidad']);
          SetEstacionSalida(Clase.GetCodigoEstacion);
          SetEstacionEntrada('');
          SetCodigoZona(Clase.GetCodigoZona);
          SetPrecio(FieldValues['precio']);
          SetPDescuento(FieldValues['p_descuento']);
          SetMontoDescuento(FieldValues['monto_descuento']);
          SetImpuesto(FieldValues['impuesto']);
          SetPIva(FieldValues['p_iva']);
          SetTotal(FieldValues['total']);
          SetCosto(Producto.GetCostoActual);
          SetUsCosto(Producto.GetUsCostoActual);
          SetCostoFob(Producto.GetCostoFob);
          SetFecha(Clase.GetFechaIngreso);
          SetCodigoCliente(Clase.GetCodigoCliente);
          SetCodigoProveedor('');
          SetIdMovimientoInventario(Clase.GetIdMovimientoInventario);
          Insertar;
        end;
      end;
      Next;
    end;
    EnableControls;
  end;
end;

function TFrmNotaCreditoCliente. Validar: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  //Validando Cliente
  if (Length(Trim(CmbCodigoCliente.Text)) = 0) then
  begin
    ListaErrores.Add('El Cliente No Puede Estar En Blanco!!!');
  end;
  //Validando Vendedor
  if (Length(Trim(CmbCodigoVendedor.Text)) = 0) then
  begin
    ListaErrores.Add('El Vendedor No Puede Estar En Blanco!!!');
  end;
  //Validando Monto
  if (TxtTotal.Value <= 0) then
  begin
    ListaErrores.Add('El Total Debe Ser Positivo!!!');
  end;
  //Validando Fechas
  if ((DatFechaIngreso.Date > DatFechaRecepcion.Date) or (DatFechaIngreso.Date > DatFechaContable.Date) or (DatFechaIngreso.Date > DatFechaLibros.Date)) then
  begin
    ListaErrores.Add('La Fecha de Ingreso Debe Ser Menor a las Fechas de Recepcion, Contable y Libros Respectivamente!!!');
  end;
  //Validando Detalle o Forma Libre
  if (Clase.GetFormaLibre) then
  begin
    if (Length(Trim(TxtConcepto.Text)) = 0) then
    begin
      ListaErrores.Add('El Concepto No Puede Estar En Blanco!!!');
    end;
    if (TxtMontoNeto.Value <= 0) then
    begin
      ListaErrores.Add('El Monto Neto Debe Ser Positivo!!!');
    end;
    if (TxtMontoDescuento.Value < 0) then
    begin
      ListaErrores.Add('El Monto de Descuento No Debe Ser Negativo!!!');
    end;
    if (TxtImpuesto.Value < 0) then
    begin
      ListaErrores.Add('El Impuesto No Debe Ser Negativo!!!');
    end;
  end
  else
  begin
    if (Data.RecordCount = 0) then
    begin
      ListaErrores.Add('El Detalle De La Nota de Cr�dito No Puede Estar Vacio!!!');
    end;
  end;
  //Validando Monto de Impuesto
  if (TxtImpuesto.Value = 0) then
  begin
    if (MessageBox(Self.Handle, '�El Valor del Impuesto esta en 0 Aun Asi Desea Continuar?', PChar(MsgTituloInformacion), MB_ICONWARNING or MB_YESNO or MB_TOPMOST or MB_TASKMODAL) = mrNo) then
    begin
      TxtImpuesto.SetFocus;
      Abort;
    end;
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

       
procedure TFrmNotaCreditoCliente.Imprimir;
begin
  if (Length(Trim(TxtNumero.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
  with Reporte do
  begin
    AgregarParametro('NumeroNotaCreditoCliente',Clase.GetNumero);
    AgregarParametro('MontoLetra',NumeroALetras(Clase.GetTotal));
    if (Clase.GetFormaLibre) then
    begin
      EmiteReporte('NotaCreditoClienteFormaLibre',trDocumento);
    end
    else
    begin
      EmiteReporte('NotaCreditoCliente',trDocumento);
    end;
  end;
end;

procedure TFrmNotaCreditoCliente.LblImpuestoDblClick(Sender: TObject);
var
  Aux: Currency;
begin
  if (Clase.GetFormaLibre) then
  begin
    Aux := 0;
    Solicitar(Self,'Impuesto',TxtImpuesto.Value,Aux);
    if (Aux >= 0) then
    begin
      TxtImpuesto.Value := Aux;
      CalcularMontosCabecera;
    end;
  end
  else
  begin
    MessageBox(Self.Handle, 'La Nota de Cr�dito No Es de Forma "Concepto Libre"', PChar(MsgTituloError), MB_ICONSTOP or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end;
end;

procedure TFrmNotaCreditoCliente.LblMontoDescuentoDblClick(Sender: TObject);
var
  Aux: Currency;
begin
  Aux := 0;
  Solicitar(Self,'Monto Descuento',TxtMontoDescuento.Value,Aux);
  if (Aux >= 0) then
  begin
    TxtMontoDescuento.Value := Aux;
    TxtPDescuento.Caption := FormatFloat('#,##0.0000%',Aux * 100 / TxtMontoNeto.Value);
    if (Clase.GetFormaLibre) then
    begin
      CalcularMontosCabecera;
    end
    else
    begin
      CalcularMontos;
    end;    
  end;
end;

procedure TFrmNotaCreditoCliente.LblMontoNetoDblClick(Sender: TObject);
begin
  inherited;
  if (Data.IsEmpty) then
  begin
    if (MessageBox(Self.Handle, '�Desea Activar Forma Concepto Libre?', PChar(MsgTituloInformacion), MB_ICONQUESTION or MB_YESNO or MB_TOPMOST or MB_TASKMODAL) = mrYes) then
    begin
      Clase.SetFormaLibre(True);
      TabDetalle.TabVisible := False;
      PageDetalles.ActivePage := TabConcepto;
      TxtMontoNeto.ReadOnly := False;
    end
    else
    begin
      Clase.SetFormaLibre(False);
      TabDetalle.TabVisible := True;
      PageDetalles.ActivePage := TabDetalle;
      TxtMontoNeto.ReadOnly := True;
      TxtMontoNeto.Value := 0;
      TxtMontoDescuento.Value := 0;
      TxtPDescuento.Caption := FormatFloat('#,##0.0000%',0);
      TxtSubTotal.Value := 0;
      TxtImpuesto.Value := 0;
      TxtTotal.Value := 0;
    end;
  end;
end;

procedure TFrmNotaCreditoCliente.Limpiar;
begin
  TxtNumero.Text := '';
  TxtNumeroFacturaAsociada.Text :='';
  CmbCodigoCliente.KeyValue := '';
  CmbCodigoVendedor.KeyValue := '';
  CmbCodigoEstacion.KeyValue := '';
  CmbCodigoZona.KeyValue := '';
  CmbCodigoMoneda.KeyValue := '';
  DatFechaIngreso.Date := Date;
  DatFechaRecepcion.Date := Date;
  DatFechaContable.Date := Date;
  DatFechaLibros.Date := Date;
  TxtConcepto.Text := '';
  TxtObservaciones.Text := '';
  TxtMontoNeto.Value := 0;
  TxtMontoDescuento.Value := 0;
  TxtSubTotal.Value := 0;
  TxtImpuesto.Value := 0;
  TxtTotal.Value := 0;
  TxtPDescuento.Caption := FormatFloat('###0.0000%',0);
  TxtPIVA.Caption := FormatFloat('###0.0000%',0);
  //Status Bar
  SB.Panels[0].Text := '';
  Data.EmptyDataSet;
  //Formato Concepto Libre
  TxtMontoNeto.ReadOnly := True;
  TabDetalle.TabVisible := True;
  PageDetalles.ActivePage := TabDetalle;
  Clase.SetFormaLibre(False);
end;

procedure TFrmNotaCreditoCliente.Mover;
begin

end;

procedure TFrmNotaCreditoCliente.Obtener;
begin
  with Clase do
  begin
    SetNumero(TxtNumero.Text);
    SetNumeroFacturaAsociada(TxtNumeroFacturaAsociada.Text);
    SetCodigoCliente(CmbCodigoCliente.KeyValue);
    SetCodigoVendedor(CmbCodigoVendedor.KeyValue);
    SetCodigoEstacion(CmbCodigoEstacion.KeyValue);
    SetCodigoZona(CmbCodigoZona.KeyValue);
    SetCodigoMoneda(CmbCodigoMoneda.KeyValue);
    SetFechaIngreso(DatFechaIngreso.Date);
    SetFechaRecepcion(DatFechaRecepcion.Date);
    SetFechaContable(DatFechaContable.Date);
    SetFechaLibros(DatFechaLibros.Date);
    SetConcepto(TxtConcepto.Text);
    SetObservaciones(TxtObservaciones.Text);
    SetMontoNeto(TxtMontoNeto.Value);
    SetMontoDescuento(TxtMontoDescuento.Value);
    SetSubTotal(TxtSubTotal.Value);
    SetImpuesto(TxtImpuesto.Value);
    SetTotal(TxtTotal.Value);
    SetPDescuento(FormatearFloat(StringReplace(TxtPDescuento.Caption,'%','',[rfReplaceAll])));
    SetPIva(FormatearFloat(StringReplace(TxtPIVA.Caption,'%','',[rfReplaceAll])));
    SetTasaDolar(0); //***//
    SetLoginUsuario(P.User.GetLoginUsuario);
    if (Agregando) then
    begin
      SetTiempoIngreso(Now);
      SetIdNotaCreditoCliente(-1);
      SetIdCobranza(-1);
      SetIdMovimientoInventario(-1);
      //SetIdContadoCobranza(-1);
    end;
  end;
end;

procedure TFrmNotaCreditoCliente.Refrescar;
begin
  //Se Actualizan Los demas Componentes
  DataCliente := Cliente.ObtenerCombo;
  DataVendedor := Vendedor.ObtenerCombo;
  DataEstacion := Estacion.ObtenerCombo;
  DataZona := Zona.ObtenerCombo;
  DataMoneda := Moneda.ObtenerCombo;
  DataBuscadorNotaCreditoCliente := BuscadorNotaCreditoCliente.ObtenerLista;
  DataCliente.First;
  DataVendedor.First;
  DataEstacion.First;
  DataZona.First;
  DataMoneda.First;
  DataBuscadorNotaCreditoCliente.First;
  DSCliente.DataSet := DataCliente;
  DSVendedor.DataSet := DataVendedor;
  DSEstacion.DataSet := DataEstacion;
  DSZona.DataSet := DataZona;
  DSMoneda.DataSet := DataMoneda;
end;

procedure TFrmNotaCreditoCliente.TBtnEliminarClick(Sender: TObject);
begin
  if (Length(Trim(TxtNumero.Text)) > 0) then
  begin
    inherited;
  end
  else
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
end;

procedure TFrmNotaCreditoCliente.TxtMontoNetoExit(Sender: TObject);
begin
  CalcularMontosCabecera;
end;

procedure TFrmNotaCreditoCliente.TxtNumeroFacturaAsociadaExit(Sender: TObject);
var
  FacturacionCliente: TFacturacionCliente;
  DetFacturacionCliente: TDetFacturacionCliente;
  DataDetFacturacionCliente: TClientDataSet;
begin
  if (Agregando) then
  begin
    Clase.SetNumeroFacturaAsociada(Trim(TxtNumeroFacturaAsociada.Text));
    if (not Clase.BuscarNumeroFacturaAsociada) then
    begin
      if (Length(Trim(TxtNumeroFacturaAsociada.Text)) > 0) then
      begin
        FacturacionCliente := TFacturacionCliente.Create;
        FacturacionCliente.SetNumero(Trim(TxtNumeroFacturaAsociada.Text));
        if (FacturacionCliente.BuscarNumero) then
        begin
          with Clase do
          begin
            SetNumero('');
            SetNumeroFacturaAsociada(TxtNumeroFacturaAsociada.Text);
            SetCodigoCliente(FacturacionCliente.GetCodigoCliente);
            SetCodigoVendedor(FacturacionCliente.GetCodigoVendedor);
            SetCodigoEstacion(Parametro.ObtenerValor('codigo_estacion_entrada'));
            SetCodigoZona(FacturacionCliente.GetCodigoZona);
            SetCodigoMoneda(FacturacionCliente.GetCodigoMoneda);
            SetFechaIngreso(Date);
            SetFechaRecepcion(Date);
            SetFechaContable(Date);
            SetFechaLibros(Date);
            SetConcepto(FacturacionCliente.GetConcepto);
            SetObservaciones(FacturacionCliente.GetObservaciones);
            SetMontoNeto(FacturacionCliente.GetMontoNeto);
            SetMontoDescuento(FacturacionCliente.GetMontoDescuento);
            SetSubTotal(FacturacionCliente.GetSubTotal);
            SetImpuesto(FacturacionCliente.GetImpuesto);
            SetTotal(FacturacionCliente.GetTotal);
            SetPDescuento(FacturacionCliente.GetPDescuento);
            SetPIva(FacturacionCliente.GetPIva);
            SetTasaDolar(0); //***//
            SetFormaLibre(FacturacionCliente.GetFormaLibre);
            SetLoginUsuario(P.User.GetLoginUsuario);
            if (Agregando) then
            begin
              SetTiempoIngreso(Now);
              SetIdNotaCreditoCliente(-1);
              SetIdCobranza(-1);
              SetIdMovimientoInventario(-1);
              //SetIdContadoCobranza(-1);
            end;
            Asignar;
            DetFacturacionCliente := TDetFacturacionCliente.Create;
            DataDetFacturacionCliente := DetFacturacionCliente.ObtenerIdFacturacionCliente(FacturacionCliente.GetIdFacturacionCliente);
            with DataDetFacturacionCliente do
            begin
              First;
              while (not Eof) do
              begin
                Self.Data.Insert;
                Self.Data.FieldValues['codigo_producto'] := FieldValues['codigo_producto'];
                Self.Data.FieldValues['referencia'] := FieldValues['referencia'];
                Self.Data.FieldValues['descripcion'] := FieldValues['descripcion'];
                Self.Data.FieldValues['cantidad'] := FieldValues['cantidad'];
                Self.Data.FieldValues['precio'] := FieldValues['precio'];
                Self.Data.FieldValues['p_descuento'] := FieldValues['p_descuento'];
                Self.Data.FieldValues['monto_descuento'] := FieldValues['monto_descuento'];
                Self.Data.FieldValues['impuesto'] := FieldValues['impuesto'];
                Self.Data.FieldValues['p_iva'] := FieldValues['p_iva'];
                Self.Data.FieldValues['total'] := FieldValues['total'];
                Self.Data.FieldValues['id_nota_credito_cliente'] := -1;
                Self.Data.Post;
                Next;
              end;
            end;
            DS.DataSet := Data;
          end;
        end;
      end;
    end
    else
    begin
      MessageBox(Self.Handle, 'El N�mero de Factura ya esta Asociado una Nota de Cr�dito ', PChar(MsgTituloInformacion), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      Limpiar;
    end;
  end;
end;

procedure TFrmNotaCreditoCliente.TxtPDescuentoDblClick(Sender: TObject);
var
  Aux: Currency;
begin
  Aux := 0;
  Solicitar(Self,'(%) Descuento',StrToFloat(StringReplace(TxtPDescuento.Caption,'%','',[rfReplaceAll])),Aux);
  if (Aux >= 0) then
  begin
    TxtPDescuento.Caption := FormatFloat('#,##0.0000%',Aux);
    TxtMontoDescuento.Value := Aux * TxtMontoNeto.Value / 100;
    if (Clase.GetFormaLibre) then
    begin
      CalcularMontosCabecera;
    end
    else
    begin
      CalcularMontos;
    end;
  end;
end;

procedure TFrmNotaCreditoCliente.TxtPIVADblClick(Sender: TObject);
var
  Aux: Currency;
  Exentos: Boolean;
  BMark: TBookmark;
begin
  if (Clase.GetFormaLibre) then //Forma Concepto Libre
  begin
    Aux := 0;
    Solicitar(Self,'(%) I.V.A.',StrToFloat(StringReplace(TxtPIVA.Caption,'%','',[rfReplaceAll])),Aux);
    if (Aux >= 0) then
    begin
      TxtPIVA.Caption := FormatFloat('#,##0.0000%',Aux);
      TxtImpuesto.Value := Aux * TxtMontoNeto.Value / 100;
      CalcularMontosCabecera;
    end;
  end
  else
  begin
    Aux := 0;
    Solicitar(Self,'(%) I.V.A.',StrToFloat(StringReplace(TxtPIVA.Caption,'%','',[rfReplaceAll])),Aux);
    if (Aux >= 0) then
    begin
      TxtPIVA.Caption := FormatFloat('#,##0.0000%',Aux);
      if (not Data.IsEmpty) then
      begin
        if (MessageBox(Self.Handle, '�Desea Actualizar El Porcentaje De Impuesto En Todos Los Renglones?', PChar(MsgTituloInformacion), MB_ICONQUESTION or MB_YESNO or MB_TOPMOST or MB_TASKMODAL) = mrYes) then
        begin
          if (MessageBox(Self.Handle, '�Incluir Renglones Exentos de I.V.A.?', PChar(MsgTituloInformacion), MB_ICONQUESTION or MB_YESNO or MB_TOPMOST or MB_TASKMODAL) = mrYes) then
          begin
            Exentos := True;
          end
          else
          begin
            Exentos := False;
          end;
          //Actualizando PIVA en Renglones
          with Data do
          begin
            DisableControls;
            First;
            while (not Eof) do
            begin
              if ((FieldValues['impuesto'] > 0) or (Exentos)) then
              begin
                //Utilizando Punteros de los Registros (Bookmarks)
                //Debido a una deficiencia en la edicion de ClientDataSets
                BMark := GetBookmark;
                Edit;
                FieldValues['p_iva'] := Aux;
                FieldValues['impuesto'] := (FieldValues['precio'] * FieldValues['cantidad']) * (FieldValues['p_iva'] / 100);
                Post;
                GotoBookmark(BMark);
              end;
              Next;
            end;
            EnableControls;
          end;
          MessageBox(Self.Handle, 'Renglones Actualizados Correctamente', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
        end;
      end;
      CalcularMontos;
    end;
  end;
end;

end.
