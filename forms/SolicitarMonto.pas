unit SolicitarMonto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, rxToolEdit, rxCurrEdit, Buttons;

type
  TFrmSolicitarMonto = class(TForm)
    BtnAceptar: TBitBtn;
    BitBtn2: TBitBtn;
    LblTitulo: TLabel;
    TxtMonto: TCurrencyEdit;
    procedure FormActivate(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
  private
    { Private declarations }
    Monto: Currency;
  public
    { Public declarations }
    Aceptado: Boolean;
    constructor Crear(Owner: TComponent; Parametro: string; ValorDefecto: Currency);
    function GetMonto: Currency;
  end;

var
  FrmSolicitarMonto: TFrmSolicitarMonto;

procedure Solicitar(AOwner: TComponent; Parametro: string; ValorDefecto: Currency; var Valor: Currency);

implementation

{$R *.dfm}

{ TFrmSolicitarMonto }

procedure TFrmSolicitarMonto.BtnAceptarClick(Sender: TObject);
begin
  Aceptado := True;
  Monto := TxtMonto.Value; 
end;

constructor TFrmSolicitarMonto.Crear(Owner: TComponent; Parametro: string;
  ValorDefecto: Currency);
begin
  inherited Create(Owner);
  Caption := 'Sophie - Solicitar "' + Parametro + '"';
  LblTitulo.Caption := 'Indique el Monto para "' + Parametro + '"';
  TxtMonto.Value := ValorDefecto;
  Aceptado := False;
end;

procedure TFrmSolicitarMonto.FormActivate(Sender: TObject);
begin
  TxtMonto.SetFocus;
end;

function TFrmSolicitarMonto.GetMonto: Currency;
begin
  Result := Monto;
end;

procedure Solicitar(AOwner: TComponent; Parametro: string; ValorDefecto: Currency;
  var Valor: Currency);
begin
  FrmSolicitarMonto := TFrmSolicitarMonto.Crear(AOwner,Parametro,ValorDefecto);
  FrmSolicitarMonto.ShowModal;
  if (FrmSolicitarMonto.Aceptado) then
  begin
    Valor := FrmSolicitarMonto.GetMonto;
  end;
  FreeAndNil(FrmSolicitarMonto);  
end;

end.
