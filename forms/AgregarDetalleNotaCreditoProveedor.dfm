inherited FrmAgregarDetalleNotaCreditoProveedor: TFrmAgregarDetalleNotaCreditoProveedor
  Caption = 'Sophie - Agregar Detalle....'
  ClientHeight = 146
  ClientWidth = 349
  OnCreate = FormCreate
  ExplicitWidth = 355
  ExplicitHeight = 170
  PixelsPerInch = 96
  TextHeight = 13
  inherited LblCodigo: TLabel
    Top = 13
    ExplicitTop = 13
  end
  object LblCantidad: TLabel [1]
    Left = 11
    Top = 64
    Width = 47
    Height = 13
    Caption = 'Cantidad:'
  end
  inherited TxtCodigo: TEdit
    Top = 10
    ExplicitTop = 10
  end
  inherited PanelBotones: TPanel
    Top = 106
    Width = 349
    TabOrder = 4
  end
  inherited BtnBuscar: TBitBtn
    Top = 10
    TabOrder = 1
    OnClick = BtnBuscarClick
    ExplicitTop = 10
  end
  object TxtDescripcionProducto: TEdit
    Left = 64
    Top = 35
    Width = 249
    Height = 21
    Enabled = False
    TabOrder = 2
    OnKeyDown = TxtCodigoKeyDown
  end
  object TxtCantidad: TCurrencyEdit
    Left = 64
    Top = 60
    Width = 75
    Height = 21
    Margins.Left = 4
    Margins.Top = 1
    DecimalPlaces = 4
    DisplayFormat = '#,##0.0000'
    TabOrder = 3
    Value = 1.000000000000000000
    OnKeyDown = TxtCantidadKeyDown
  end
end
