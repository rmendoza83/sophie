unit RadioPrograma;

{$I sophie.inc}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Generico, XPMan, ImgList, DB, Grids, DBGrids, StdCtrls, Buttons,
  ExtCtrls, ComCtrls, ToolWin, RxLookup, rxToolEdit, DBCtrls, Mask, rxCurrEdit,
  DBClient, Utils, ClsRadioPrograma, ClsRadioEmisora
  {$IFDEF DELPHI10}, ImageList{$ENDIF};

const
  DiasLV = 0; //Lunes a Viernes
  DiasLS = 1; //Lunes a Sabados
  DiasS  = 2; //Sabados
  DiasD  = 3; //Domingos

type
  TFrmRadioPrograma = class(TFrmGenerico)
    LblCodigo: TLabel;
    LblDescripcion: TLabel;
    LblHoraInicio: TLabel;
    LblTipoVenta: TLabel;
    LblLocutor: TLabel;
    LblTarifaDia: TLabel;
    TxtCodigo: TEdit;
    TxtDescripcion: TEdit;
    TxtTipoVenta: TEdit;
    TxtLocutor: TEdit;
    LblCodigoEmisora: TLabel;
    CmbCodigoEmisora: TRxDBLookupCombo;
    BtnBuscarCliente: TBitBtn;
    TxtRazonSocialEmisora: TDBText;
    DTHoraFin: TDateTimePicker;
    LblHoraFin: TLabel;
    DTHoraInicio: TDateTimePicker;
    DSEmisora: TDataSource;
    LblDias: TLabel;
    CmbDias: TComboBox;
    LblTarifaNacional: TLabel;
    LblTarifaSegundo: TLabel;
    TxtTarifaNacional: TCurrencyEdit;
    TxtTarifaDia: TCurrencyEdit;
    TxtTarifaSegundo: TCurrencyEdit;
    ChkActivo: TCheckBox;
    procedure LblTarifaNacionalMouseLeave(Sender: TObject);
    procedure LblTarifaNacionalMouseEnter(Sender: TObject);
    procedure LblTarifaNacionalDblClick(Sender: TObject);
    procedure GrdBusquedaCellClick(Column: TColumn);
    procedure BtnBuscarClienteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    Clase: TRadioPrograma;
    Data: TClientDataSet;
    Emisora: TRadioEmisora;
    DataEmisora: TClientDataSet;
    procedure Agregar;
    procedure Editar;
    procedure Eliminar;
    procedure Guardar;
    procedure Cancelar;
    procedure Refrescar;
    procedure Imprimir;
    procedure Buscar;
    procedure Limpiar;
    procedure Asignar;
    procedure Obtener;
    procedure Mover;
    function ObtenerCasoTarifa: Shortint;
    procedure CalcularTarifas;
  protected
    function Validar: Boolean; override;
  public
    { Public declarations }
  end;

var
  FrmRadioPrograma: TFrmRadioPrograma;

implementation

uses
  GenericoBuscador;

{$R *.dfm}

procedure TFrmRadioPrograma.Agregar;
begin
  //Verificando
  Limpiar;
  TxtCodigo.SetFocus;
  ChkActivo.Checked := True;
end;

procedure TFrmRadioPrograma.Asignar;
begin
  with Clase do
  begin
    TxtCodigo.Text := GetCodigo;
    ChkActivo.Checked := GetActivo;
    CmbCodigoEmisora.KeyValue := GetCodigoEmisora;
    TxtDescripcion.Text := GetDescripcion;
    DTHoraInicio.DateTime := GetHoraInicial;
    DTHoraFin.DateTime := GetHoraFinal;
    TxtTipoVenta.Text := GetTipoVenta;
    TxtLocutor.Text := GetLocutor;
    CmbDias.ItemIndex := CmbDias.Items.IndexOf(GetDias);
    TxtTarifaNacional.Value := GetTarifaNacional;
    TxtTarifaDia.Value := GetTarifaDia;
    TxtTarifaSegundo.Value := GetTarifaSegundo;
    DataEmisora.Locate('codigo',CmbCodigoEmisora.KeyValue,[loCaseInsensitive]);
  end;  
end;

procedure TFrmRadioPrograma.BtnBuscarClienteClick(Sender: TObject);
begin
  Buscador(Self,Emisora,'codigo','Busqueda de Emisora',nil,DataEmisora);
  CmbCodigoEmisora.KeyValue := DataEmisora.FieldValues['codigo'];
end;

procedure TFrmRadioPrograma.Buscar;
begin
  Screen.Cursor := crSQLWait;
  if (Length(Trim(TxtCriterio.Text)) > 0) then
  begin
    Data := Clase.ObtenerListaCriterio(Trim(TxtCriterio.Text));
  end
  else
  begin
    Data := Clase.ObtenerLista;
  end;
  Data.First;
  DS.DataSet := Data;
  GrdBusqueda.Refresh;
  Screen.Cursor := crDefault;
end;

procedure TFrmRadioPrograma.CalcularTarifas;
begin
  case ObtenerCasoTarifa of
    DiasLV:
      begin
        TxtTarifaDia.Value := TxtTarifaNacional.Value / 22;
        TxtTarifaSegundo.Value := TxtTarifaDia.Value / 20;
      end;
    DiasLS:
      begin
        TxtTarifaDia.Value := TxtTarifaNacional.Value / 26;
        TxtTarifaSegundo.Value := TxtTarifaDia.Value / 20;
      end;
    DiasS:
      begin
        TxtTarifaDia.Value := TxtTarifaNacional.Value / 0;
        TxtTarifaSegundo.Value := 0;
      end;
    DiasD:
      begin
        TxtTarifaDia.Value := TxtTarifaNacional.Value / 4;
        TxtTarifaSegundo.Value := 0;
      end;
  end;
end;

procedure TFrmRadioPrograma.Cancelar;
begin
  TxtCodigo.Enabled := True;
end;

procedure TFrmRadioPrograma.Editar;
begin
  //Se Busca el Elemento
  Clase.SetCodigo(Data.FieldValues['codigo']);
  Clase.Buscar;
  Asignar;
  TxtCodigo.Enabled := False;
  CmbCodigoEmisora.SetFocus;
end;

procedure TFrmRadioPrograma.Eliminar;
begin
  //Se Busca el Elemento
  Clase.SetCodigo(Data.FieldValues['codigo']);
  Clase.Buscar;
  Clase.Eliminar;
  MessageBox(Self.Handle, 'El Programa Ha Sido Eliminado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  Refrescar;
  Limpiar;
end;

procedure TFrmRadioPrograma.FormCreate(Sender: TObject);
begin
  ProcAgregar := Agregar;
  ProcEditar := Editar;
  ProcEliminar := Eliminar;
  ProcGuardar := Guardar;
  ProcCancelar := Cancelar;
  ProcRefrescar := Refrescar;
  ProcImprimir := Imprimir;
  ProcBuscar := Buscar;
  //Inicializando DataSets Secundarios
  Emisora := TRadioEmisora.Create;
  DataEmisora := Emisora.ObtenerLista;
  DataEmisora.First;
  DSEmisora.DataSet:= DataEmisora;
  TxtRazonSocialEmisora.DataSource:= DSEmisora;
  TxtRazonSocialEmisora.DataField:= 'razon_social';
  ConfigurarRxDBLookupCombo(CmbCodigoEmisora,DSEmisora,'codigo','codigo;rif;razon_social');
  Clase := TRadioPrograma.Create;
  Buscar;
end;

procedure TFrmRadioPrograma.GrdBusquedaCellClick(Column: TColumn);
begin
  Mover;
end;

procedure TFrmRadioPrograma.Guardar;
begin
  if (Agregando) then
  begin
    //Agregando...
    //Se Busca el Codigo
    Clase.SetCodigo(TxtCodigo.Text);
    if (Clase.Buscar) then
    begin
      MessageBox(Self.Handle, 'El C�digo Ya Existe!!!', PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      TxtCodigo.SetFocus;
      Abort;
    end
    else
    begin
      Obtener;
      Clase.Insertar;
      MessageBox(Self.Handle, 'El Programa Ha Sido Agregado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    end;
  end
  else
  begin
    //Editando...
    Obtener;
    Clase.Modificar;
    MessageBox(Self.Handle, 'El Programa Ha Sido Editado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    TxtCodigo.Enabled := True;
  end;
  Refrescar;
  Limpiar;
end;

procedure TFrmRadioPrograma.Imprimir;
begin

end;

procedure TFrmRadioPrograma.LblTarifaNacionalDblClick(Sender: TObject);
begin
  if (MessageBox(Self.Handle, PChar('�Desea Calcular Las Tarifas Autom�ticamente?'), PChar(MsgTituloInformacion), MB_ICONQUESTION or MB_YESNO or MB_TOPMOST or MB_APPLMODAL) = mrYes) then
  begin
    CalcularTarifas;
  end;
end;

procedure TFrmRadioPrograma.LblTarifaNacionalMouseEnter(Sender: TObject);
begin
  LblTarifaNacional.Font.Color := clRed;
end;

procedure TFrmRadioPrograma.LblTarifaNacionalMouseLeave(Sender: TObject);
begin
  LblTarifaNacional.Font.Color := clWindowText;
end;

procedure TFrmRadioPrograma.Limpiar;
begin
  TxtCodigo.Text := '';
  ChkActivo.Checked := False;
  CmbCodigoEmisora.KeyValue := '';
  TxtDescripcion.Text := '';
  DTHoraInicio.DateTime := Time;
  DTHoraFin.DateTime := Time;
  TxtTipoVenta.Text := '';
  TxtLocutor.Text := '';
  CmbDias.Text := '';
  TxtTarifaNacional.Text := '';
  TxtTarifaDia.Text := '';
  TxtTarifaSegundo.Text := '';
end;

procedure TFrmRadioPrograma.Mover;
var
  DSTemp: TDataSet;
begin
  if (DS.DataSet.RecordCount > 0) then
  begin
    DSTemp := DS.DataSet;
    with Clase do
    begin
      SetCodigo(DSTemp.FieldValues['codigo']);
      SetActivo(DSTemp.FieldValues['activo']);
      SetDescripcion(DSTemp.FieldValues['descripcion']);
      SetCodigoEmisora(DSTemp.FieldValues['codigo_emisora']);
      SetHoraInicial(DSTemp.FieldValues['hora_inicial']);
      SetHoraFinal(DSTemp.FieldValues['hora_final']);
      SetTipoVenta(DSTemp.FieldValues['tipo_venta']);
      SetLocutor(DSTemp.FieldValues['locutor']);
      SetDias(DSTemp.FieldValues['dias']);
      SetTarifaNacional(DSTemp.FieldValues['tarifa_nacional']);
      SetTarifaDia(DSTemp.FieldValues['tarifa_dia']);
      SetTarifaSegundo(DSTemp.FieldValues['tarifa_segundo']);
    end;
    Asignar;
  end
  else
  begin
    Limpiar;
  end;
end;

procedure TFrmRadioPrograma.Obtener;
begin
  with Clase do
  begin
    SetCodigo(TxtCodigo.Text);
    SetActivo(ChkActivo.Checked);
    SetCodigoEmisora(CmbCodigoEmisora.Text);
    SetDescripcion(TxtDescripcion.Text);
    SetHoraInicial(DTHoraInicio.DateTime);
    SetHoraFinal(DTHoraFin.DateTime);
    SetTipoVenta(TxtTipoVenta.Text);
    SetLocutor(TxtLocutor.Text);
    SetDias(CmbDias.Text);
    SetTarifaNacional(TxtTarifaNacional.Value);
    SetTarifaDia(TxtTarifaDia.Value);
    SetTarifaSegundo(TxtTarifaSegundo.Value);
  end;
end;

function TFrmRadioPrograma.ObtenerCasoTarifa: Shortint;
begin
  Result := -1;
  if (CmbDias.Text = 'LUNES A VIERNES') then
  begin
    Result := DiasLV;
    Exit;
  end;
  if (CmbDias.Text = 'LUNES A SABADOS') then
  begin
    Result := DiasLS;
    Exit;
  end;
  if (CmbDias.Text = 'SABADOS') then
  begin
    Result := DiasS;
    Exit;
  end;
  if (CmbDias.Text = 'DOMINGOS') then
  begin
    Result := DiasD;
    Exit;
  end;
end;

procedure TFrmRadioPrograma.Refrescar;
begin
  TxtCriterio.Text := '';
  Buscar;
  //Se Actualizan Los demas Componentes
  DataEmisora := Emisora.ObtenerLista;
  DataEmisora.First;
  DSEmisora.DataSet:= DataEmisora;
end;

function TFrmRadioPrograma.Validar: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  //Validando Codigo
  if (Length(Trim(TxtCodigo.Text)) = 0) then
  begin
    ListaErrores.Add('El C�digo No Puede Estar En Blanco!!!');
  end;
  //Validando La Emisora
  if (Length(Trim(CmbCodigoEmisora.Text)) = 0) then
  begin
    ListaErrores.Add('El C�digo de Emisora No Puede Estar En Blanco!!!');
  end;
  //Validando La Descripci�n
  if (Length(Trim(TxtDescripcion.Text)) = 0) then
  begin
    ListaErrores.Add('La Descripci�n No Puede Estar En Blanco!!!');
  end;
  //Validando Hora Inicio - Hora Fin
  if (DTHoraFin.DateTime < DTHoraInicio.DateTime) then
  begin
    ListaErrores.Add('La Hora de Inicio del Programa Debe Ser Menor Que La Hora Final!!!');
  end;
  //Validando Tipo de Venta
  if (Length(Trim(TxtTipoVenta.Text)) = 0) then
  begin
    ListaErrores.Add('El Tipo de Venta No Puede Estar En Blanco!!!');
  end;
  //Validando Locutor
  if (Length(Trim(TxtLocutor.Text)) = 0) then
  begin
    ListaErrores.Add('El Locutor No Puede Estar En Blanco!!!');
  end;
  //Validando Tipo de Venta
  if (Length(Trim(CmbDias.Text)) = 0) then
  begin
    ListaErrores.Add('Debe Asiqnar los Dias para el Para el Programa!!!');
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
