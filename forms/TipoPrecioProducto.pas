unit TipoPrecioProducto;

{$I sophie.inc}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Generico, XPMan, ImgList, DB, Grids, DBGrids, StdCtrls, Buttons,
  ExtCtrls, ComCtrls, ToolWin, ClsTipoPrecioProducto, DBClient, Utils,
  ClsProducto, ClsProductoPrecio, Mask, rxToolEdit, rxCurrEdit
  {$IFDEF DELPHI10}, ImageList{$ENDIF};

type
  TFrmTipoPrecioProducto = class(TFrmGenerico)
    LblCodigo: TLabel;
    LblDescripcion: TLabel;
    TxtCodigo: TEdit;
    TxtDescripcion: TEdit;
    LblPCalculoAutomatico: TLabel;
    TxtPCalculoAutomatico: TCurrencyEdit;
    TxtCalculoAutomatico: TCheckBox;
    LblCalculoAutomatico: TLabel;
    LblMontoFijo: TLabel;
    TxtMontoFijo: TCurrencyEdit;
    TxtCalculoMontoFijo: TCheckBox;
    LblCalculoMontoFijo: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure GrdBusquedaCellClick(Column: TColumn);
    procedure TxtCalculoAutomaticoClick(Sender: TObject);
    procedure TxtCalculoMontoFijoClick(Sender: TObject);
  private
    { Private declarations }
    Clase: TTipoPrecioProducto;
    Data: TClientDataSet;
    Producto: TProducto;
    ProductoPrecio: TProductoPrecio;
    DataProducto: TClientDataSet;
    procedure Agregar;
    procedure Editar;
    procedure Eliminar;
    procedure Guardar;
    procedure Cancelar;
    procedure Refrescar;
    procedure Imprimir;
    procedure Buscar;
    procedure Limpiar;
    procedure Asignar;
    procedure Obtener;
    procedure Mover;
  protected
    function Validar: Boolean; override;
  public
    { Public declarations }
  end;

var
  FrmTipoPrecioProducto: TFrmTipoPrecioProducto;

implementation

{$R *.dfm}

procedure TFrmTipoPrecioProducto.FormCreate(Sender: TObject);
begin
  inherited;
  ProcAgregar := Agregar;
  ProcEditar := Editar;
  ProcEliminar := Eliminar;
  ProcGuardar := Guardar;
  ProcCancelar := Cancelar;
  ProcRefrescar := Refrescar;
  ProcImprimir := Imprimir;
  ProcBuscar := Buscar;
  Clase := TTipoPrecioProducto.Create;
  Producto := TProducto.Create;
  ProductoPrecio := TProductoPrecio.Create;
  Buscar;
end;

procedure TFrmTipoPrecioProducto.GrdBusquedaCellClick(Column: TColumn);
begin
  Mover;
end;

procedure TFrmTipoPrecioProducto.Agregar;
begin
  //Verificando
  Limpiar;
  TxtCodigo.SetFocus;
end;

procedure TFrmTipoPrecioProducto.Asignar;
begin
  with Clase do
  begin
    TxtCodigo.Text := GetCodigo;
    TxtDescripcion.Text := GetDescripcion;
    TxtCalculoAutomatico.Checked := GetCalculoAutomatico;
    TxtPCalculoAutomatico.Value := GetPCalculoAutomatico;
    TxtCalculoMontoFijo.Checked := GetCalculoMontoFijo;
    TxtMontoFijo.Value := GetMontoFijo;
    TxtCalculoAutomaticoClick(nil);
    TxtCalculoMontoFijoClick(nil);
  end;
end;

procedure TFrmTipoPrecioProducto.Buscar;
begin
  Screen.Cursor := crSQLWait;
  if (Length(Trim(TxtCriterio.Text)) > 0) then
  begin
    Data := Clase.ObtenerListaCriterio(Trim(TxtCriterio.Text));
  end
  else
  begin
    Data := Clase.ObtenerLista;
  end;
  Data.First;
  DS.DataSet := Data;
  GrdBusqueda.Refresh;
  Screen.Cursor := crDefault;
end;

procedure TFrmTipoPrecioProducto.Cancelar;
begin
  TxtCodigo.Enabled := True;
end;

procedure TFrmTipoPrecioProducto.Editar;
begin
  //Se Busca el Elemento
  Clase.SetCodigo(Data.FieldValues['codigo']);
  Clase.Buscar;
  Asignar;
  TxtCodigo.Enabled := False;
  TxtDescripcion.SetFocus;
end;

procedure TFrmTipoPrecioProducto.Eliminar;
begin
  //Se Busca el Elemento
  Clase.SetCodigo(Data.FieldValues['codigo']);
  Clase.Buscar;
  Clase.Eliminar;
  //Eliminando Tipos de Precios del Tipo de Precio de Producto
  ProductoPrecio.EliminarCodigoTipoPrecioProducto(Clase.GetCodigo);
  MessageBox(Self.Handle, 'El Tipo de Precio de Producto Ha Sido Eliminado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  Refrescar;
  Limpiar;
end;

procedure TFrmTipoPrecioProducto.Guardar;
begin
  if (Agregando) then
  begin
    //Agregando...
    //Se Busca el Codigo
    Clase.SetCodigo(TxtCodigo.Text);
    if (Clase.Buscar) then
    begin
      MessageBox(Self.Handle, 'El C�digo Ya Existe!!!', PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      TxtCodigo.SetFocus;
      Abort;
    end
    else
    begin
      Obtener;
      Clase.Insertar;
      MessageBox(Self.Handle, 'El Tipo de Precio de Producto Ha Sido Agregado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    end;
    //Actualizando Tabla de Tipos de Precios
    MessageBox(Self.Handle, 'Atenci�n! Se actualizaran los datos internos para los Tipos de Precio de Todos los Productos del Sistema!!! Por favor, espere...', PChar(MsgTituloAdvertencia), MB_ICONEXCLAMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    DataProducto := Producto.ObtenerLista;
    with DataProducto do
    begin
      First;
      ProductoPrecio.SetCodigoTipoPrecioProducto(Clase.GetCodigo);
      ProductoPrecio.SetDescripcion(Clase.GetDescripcion);
      while (not Eof) do
      begin
        ProductoPrecio.SetCodigoProducto(FieldValues['codigo']);
        ProductoPrecio.SetPrecio(FieldValues['precio']);
        ProductoPrecio.Insertar;
        Next;
      end;
    end;
    MessageBox(Self.Handle, 'Los Datos Internos Se Actualizaron Correctamente!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end
  else
  begin
    //Editando...
    Obtener;
    Clase.Modificar;
    MessageBox(Self.Handle, 'El Tipo de Precio de Producto Ha Sido Editado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    TxtCodigo.Enabled := True;
  end;
  Refrescar;
  Limpiar;
end;

procedure TFrmTipoPrecioProducto.Imprimir;
begin

end;

procedure TFrmTipoPrecioProducto.Limpiar;
begin
  TxtCodigo.Text := '';
  TxtDescripcion.Text := '';
  TxtCalculoAutomatico.Checked := False;
  TxtPCalculoAutomatico.Text := '';
  TxtCalculoMontoFijo.Checked := False;
  TxtMontoFijo.Text := '';
end;

procedure TFrmTipoPrecioProducto.Mover;
var
  DSTemp: TDataSet;
begin
  if (DS.DataSet.RecordCount > 0) then
  begin
    DSTemp := DS.DataSet;
    with Clase do
    begin
      SetCodigo(DSTemp.FieldValues['codigo']);
      SetDescripcion(DSTemp.FieldValues['descripcion']);
      SetCalculoAutomatico(DSTemp.FieldValues['calculo_automatico']);
      SetPCalculoAutomatico(DSTemp.FieldValues['p_calculo_automatico']);
      SetCalculoMontoFijo(DSTemp.FieldValues['calculo_monto_fijo']);
      SetMontoFijo(DSTemp.FieldValues['monto_fijo']);
    end;
    Asignar;
  end
  else
  begin
    Limpiar;
  end;
end;

procedure TFrmTipoPrecioProducto.Obtener;
begin
  with Clase do
  begin
    SetCodigo(TxtCodigo.Text);
    SetDescripcion(TxtDescripcion.Text);
    SetCalculoAutomatico(TxtCalculoAutomatico.Checked);
    SetPCalculoAutomatico(TxtPCalculoAutomatico.Value);
    SetCalculoMontoFijo(TxtCalculoMontoFijo.Checked);
    SetMontoFijo(TxtMontoFijo.Value);
  end;
end;

procedure TFrmTipoPrecioProducto.Refrescar;
begin
  TxtCriterio.Text := '';
  Buscar;
end;

procedure TFrmTipoPrecioProducto.TxtCalculoAutomaticoClick(Sender: TObject);
begin
  inherited;
  LblPCalculoAutomatico.Enabled := TxtCalculoAutomatico.Checked;
  TxtPCalculoAutomatico.Enabled := TxtCalculoAutomatico.Checked;
  if (not TxtCalculoAutomatico.Checked) then
  begin
    TxtPCalculoAutomatico.Value := 0;
  end;  
end;

procedure TFrmTipoPrecioProducto.TxtCalculoMontoFijoClick(Sender: TObject);
begin
  inherited;
  LblMontoFijo.Enabled := TxtCalculoMontoFijo.Checked;
  TxtMontoFijo.Enabled := TxtCalculoMontoFijo.Checked;
  if (not TxtCalculoMontoFijo.Checked) then
  begin
    TxtMontoFijo.Value := 0;
  end;
end;

function TFrmTipoPrecioProducto.Validar: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  (* Validando Informacion del Concepto *)
  //Validando Codigo
  if (Length(Trim(TxtCodigo.Text)) = 0) then
  begin
    ListaErrores.Add('El C�digo No Puede Estar En Blanco!!!');
  end;
  //Validando Descripcion
  if (Length(Trim(TxtDescripcion.Text)) = 0) then
  begin
    ListaErrores.Add('La Descripci�n No Puede Estar En Blanco!!!');
  end;
  //Validando Calculo por %
  if (TxtCalculoAutomatico.Checked) then
  begin
    if (TxtPCalculoAutomatico.Value = 0) then
    begin
      ListaErrores.Add('El Porcentaje de C�lculo Automatico por Porcentaje debe ser distinto de Cero (0.0000%)!!!');
    end;
  end;
  //Validando Calculo por Monto Fijo
  if (TxtCalculoMontoFijo.Checked) then
  begin
    if (TxtMontoFijo.Value = 0) then
    begin
      ListaErrores.Add('El Monto Fijo de C�lculo Automatico por Monto ser distinto de Cero (0.0000)!!!');
    end;
  end;
  //Validando que no existan Ambos Metodos de Calculo
  if ((TxtCalculoAutomatico.Checked) and (TxtCalculoMontoFijo.Checked)) then
  begin
    ListaErrores.Add('No Pueden Especificarse Ambos M�todos de C�lculo Automatico!!!');
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
