unit OperacionBancaria;

{$I sophie.inc}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Generico, XPMan, ImgList, DB, Grids, DBGrids, StdCtrls, Buttons,
  ExtCtrls, ComCtrls, ToolWin, Mask, rxToolEdit, RxLookup, DBCtrls, rxCurrEdit,
  ClsOperacionBancaria, DBClient, Utils, ClsCuentaBancaria,
  ClsTipoOperacionBancaria, ClsReporte, ClsParametro
  {$IFDEF DELPHI10}, ImageList{$ENDIF};

type
  TFrmOperacionBancaria = class(TFrmGenerico)
    LblCodigoCuenta: TLabel;
    LblNumero: TLabel;
    LblMonto: TLabel;
    LblConcepto: TLabel;
    TxtNumero: TEdit;
    TxtConcepto: TEdit;
    DSCuentaBancaria: TDataSource;
    CmbCodigoCuenta: TRxDBLookupCombo;
    BtnBuscarCodigoCuenta: TBitBtn;
    TxtNumeroCuenta: TDBText;
    LblCodigoTipoOperacionBancaria: TLabel;
    CmbCodigoTipoOperacionBancaria: TRxDBLookupCombo;
    BtnBuscarCodigoTipoOperacionBancaria: TBitBtn;
    TxtDescripcionTipoOperacionBancaria: TDBText;
    LblFecha: TLabel;
    DatFecha: TDateEdit;
    LblFechaContable: TLabel;
    DatFechaContable: TDateEdit;
    LblObservaciones: TLabel;
    TxtObservaciones: TEdit;
    LblBeneficiario: TLabel;
    TxtBeneficiario: TEdit;
    TxtMonto: TCurrencyEdit;
    DatFechaConciliacion: TDateEdit;
    LblFechaConciliacion: TLabel;
    LblDiferido: TLabel;
    LblConciliado: TLabel;
    ChkDiferido: TCheckBox;
    ChkConciliado: TCheckBox;
    DSTipoOperacionBancaria: TDataSource;
    procedure BtnBuscarCodigoTipoOperacionBancariaClick(Sender: TObject);
    procedure BtnBuscarCodigoCuentaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure GrdBusquedaCellClick(Column: TColumn);
  private
    { Private declarations }
    Clase: TOperacionBancaria;
    CuentaBancaria: TCuentaBancaria;
    TipoOperacionBancaria: TTipoOperacionBancaria;
    Reporte: TReporte;
    Parametro: TParametro;
    Data: TClientDataSet;
    DataCuentaBancaria: TClientDataSet;
    DataTipoOperacionBancaria: TClientDataSet;
    procedure Agregar;
    procedure Editar;
    procedure Eliminar;
    procedure Guardar;
    procedure Cancelar;
    procedure Refrescar;
    procedure Imprimir;
    procedure Buscar;
    procedure Limpiar;
    procedure Asignar;
    procedure Obtener;
    procedure Mover;
  protected
    function Validar: Boolean; override;
  public
    { Public declarations }
  end;

var
  FrmOperacionBancaria: TFrmOperacionBancaria;

implementation

uses
  GenericoBuscador,
  PModule
  {$IFDEF USE_RAVE_REPORTS}
  , DMRR
  {$ENDIF}
  {$IFDEF USE_FAST_REPORTS}
  , DMFR
  {$ENDIF}
  ;

{$R *.dfm}

procedure TFrmOperacionBancaria.FormCreate(Sender: TObject);
begin
  ProcAgregar := Agregar;
  ProcEditar := Editar;
  ProcEliminar := Eliminar;
  ProcGuardar := Guardar;
  ProcCancelar := Cancelar;
  ProcRefrescar := Refrescar;
  ProcImprimir := Imprimir;
  ProcBuscar := Buscar;
  Clase := TOperacionBancaria.Create;
  //Inicializando DataSets Secundarios
  CuentaBancaria := TCuentaBancaria.Create;
  TipoOperacionBancaria := TTipoOperacionBancaria.Create;
  DataCuentaBancaria := CuentaBancaria.ObtenerCombo;
  DataTipoOperacionBancaria := TipoOperacionBancaria.ObtenerCombo;
  Reporte := TReporte.Create;
  Parametro := TParametro.Create;
  DataCuentaBancaria.First;
  DataTipoOperacionBancaria.First;
  DSCuentaBancaria.DataSet := DataCuentaBancaria;
  DSTipoOperacionBancaria.DataSet := DataTipoOperacionBancaria;
  TxtNumeroCuenta.DataSource := DSCuentaBancaria;
  TxtNumeroCuenta.DataField := 'numero';
  CmbCodigoCuenta.LookupSource := DSCuentaBancaria;
  CmbCodigoCuenta.LookupField := 'codigo';
  CmbCodigoCuenta.LookupDisplay := 'codigo;numero;entidad';
  TxtDescripcionTipoOperacionBancaria.DataSource := DSTipoOperacionBancaria;
  TxtDescripcionTipoOperacionBancaria.DataField := 'descripcion';
  CmbCodigoTipoOperacionBancaria.LookupSource := DSTipoOperacionBancaria;
  CmbCodigoTipoOperacionBancaria.LookupField := 'codigo';
  CmbCodigoTipoOperacionBancaria.LookupDisplay := 'codigo;descripcion';
  Buscar;
end;

procedure TFrmOperacionBancaria.GrdBusquedaCellClick(Column: TColumn);
begin
  Mover;
end;

procedure TFrmOperacionBancaria.Agregar;
begin
  //Verificando
  Limpiar;
  CmbCodigoCuenta.SetFocus;
end;

procedure TFrmOperacionBancaria.Asignar;
begin
  with Clase do
  begin
    CmbCodigoCuenta.KeyValue := GetCodigoCuenta;
    CmbCodigoTipoOperacionBancaria.KeyValue := GetCodigoTipoOperacion;
    TxtNumero.Text := GetNumero;
    DatFecha.Date := GetFecha;
    DatFechaContable.Date := GetFechaContable;
    TxtMonto.Value := GetMonto;
    TxtConcepto.Text := GetConcepto;
    TxtObservaciones.Text := GetObservaciones;
    TxtBeneficiario.Text := GetBeneficiario;
    ChkDiferido.Checked := GetDiferido;
    ChkConciliado.Checked := GetDiferido;
    DatFechaConciliacion.Date := GetFechaConciliacion;
  end;
end;

procedure TFrmOperacionBancaria.BtnBuscarCodigoCuentaClick(Sender: TObject);
begin
  Buscador(Self,CuentaBancaria,'codigo','Busqueda de Cuenta Bancaria',nil,DataCuentaBancaria);
  CmbCodigoCuenta.KeyValue := DataCuentaBancaria.FieldValues['codigo'];
end;

procedure TFrmOperacionBancaria.BtnBuscarCodigoTipoOperacionBancariaClick(
  Sender: TObject);
begin
  Buscador(Self,TipoOperacionBancaria,'codigo','Busqueda de Tipo de Operaci�n Bancaria',nil,DataTipoOperacionBancaria);
  CmbCodigoTipoOperacionBancaria.KeyValue := DataTipoOperacionBancaria.FieldValues['codigo'];
end;

procedure TFrmOperacionBancaria.Buscar;
begin
  Screen.Cursor := crSQLWait;
  if (Length(Trim(TxtCriterio.Text)) > 0) then
  begin
    Data := Clase.ObtenerListaCriterio(Trim(TxtCriterio.Text));
  end
  else
  begin
    Data := Clase.ObtenerLista;
  end;
  Data.First;
  DS.DataSet := Data;
  GrdBusqueda.Refresh;
  Screen.Cursor := crDefault;
end;

procedure TFrmOperacionBancaria.Cancelar;
begin
  CmbCodigoCuenta.Enabled := True;
  CmbCodigoTipoOperacionBancaria.Enabled := True;
  TxtNumero.Enabled := True
end;

procedure TFrmOperacionBancaria.Editar;
begin
  //Se Busca el Elemento
  Clase.SetCodigoCuenta(Data.FieldValues['codigo_cuenta']);
  Clase.SetCodigoTipoOperacion(Data.FieldValues['codigo_tipo_operacion_bancaria']);
  Clase.SetNumero(Data.FieldValues['numero']);
  Clase.Buscar;
  Asignar;
  CmbCodigoCuenta.Enabled := False;
  CmbCodigoTipoOperacionBancaria.Enabled := False;
  TxtNumero.Enabled := False;
  DatFecha.SetFocus;
end;

procedure TFrmOperacionBancaria.Eliminar;
begin
  //Se Busca el Elemento
  Clase.SetCodigoCuenta(Data.FieldValues['codigo_cuenta']);
  Clase.SetCodigoTipoOperacion(Data.FieldValues['codigo_tipo_operacion_bancaria']);
  Clase.SetNumero(Data.FieldValues['numero']);
  Clase.Buscar;
  Clase.Eliminar;
  MessageBox(Self.Handle, 'La Operaci�n Bancaria Ha Sido Eliminada Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  Refrescar;
  Limpiar;
end;

procedure TFrmOperacionBancaria.Guardar;
begin
  if (Agregando) then
  begin
    //Agregando...
    //Se Busca la Operacion Bancaria
    Clase.SetCodigoCuenta(CmbCodigoCuenta.KeyValue);
    Clase.SetCodigoTipoOperacion(CmbCodigoTipoOperacionBancaria.KeyValue);
    Clase.SetNumero(TxtNumero.Text);
    if (Clase.Buscar) then
    begin
      MessageBox(Self.Handle, 'La Operaci�n Bancaria Ya Existe!!!', PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      CmbCodigoCuenta.SetFocus;
      Abort;
    end
    else
    begin
      Obtener;
      Clase.Insertar;
      MessageBox(Self.Handle, 'La Operaci�n Bancaria Ha Sido Agregada Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    end;
  end
  else
  begin
    //Editando...
    Obtener;
    Clase.Modificar;
    MessageBox(Self.Handle, 'La Operaci�n Bancaria Ha Sido Editada Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    CmbCodigoCuenta.Enabled := False;
    CmbCodigoTipoOperacionBancaria.Enabled := False;
    TxtNumero.Enabled := False;
  end;
  Refrescar;
  Limpiar;
end;

procedure TFrmOperacionBancaria.Imprimir;
begin
  TipoOperacionBancaria.SetCodigo(Clase.GetCodigoTipoOperacion);
  TipoOperacionBancaria.Buscar;
  if TipoOperacionBancaria.GetEmiteCheque then
  begin
    with Reporte do
    begin
      AgregarParametro('Monto',TxtMonto.Text);
      AgregarParametro('Beneficiario',Clase.GetBeneficiario);
      AgregarParametro('MontoLetra',NumeroALetras(Clase.GetMonto));
      AgregarParametro('FechaCheque',DatFecha.Text);
      EmiteReporte('Cheque',trGeneral);
    end;
  end;
end;

procedure TFrmOperacionBancaria.Limpiar;
begin
  CmbCodigoCuenta.KeyValue := '';
  CmbCodigoTipoOperacionBancaria.KeyValue := '';
  TxtNumero.Text := '';
  DatFecha.Date := Date;
  DatFechaContable.Date := Date;
  TxtMonto.Text := '';
  TxtConcepto.Text := '';
  TxtObservaciones.Text := '';
  TxtBeneficiario.Text := '';
  ChkDiferido.Checked := False;
  ChkConciliado.Checked := False;
  DatFechaConciliacion.Date := Date;
end;

procedure TFrmOperacionBancaria.Mover;
var
  DSTemp: TDataSet;
begin
  if (DS.DataSet.RecordCount > 0) then
  begin
    DSTemp := DS.DataSet;
    with Clase do
    begin
      SetCodigoCuenta(DSTemp.FieldValues['codigo_cuenta']);
      SetCodigoTipoOperacion(DSTemp.FieldValues['codigo_tipo_operacion_bancaria']);
      SetNumero(DSTemp.FieldValues['numero']);
      SetFecha(DSTemp.FieldValues['fecha']);
      SetFechaContable(DSTemp.FieldValues['fecha_contable']);
      SetMonto(DSTemp.FieldValues['monto']);
      SetConcepto(DSTemp.FieldValues['concepto']);
      SetObservaciones(DSTemp.FieldValues['observaciones']);
      SetDiferido(DSTemp.FieldValues['diferido']);
      SetConciliado(DSTemp.FieldValues['conciliado']);
      SetFechaConciliacion(DSTemp.FieldValues['fecha_conciliacion']);
      SetBeneficiario(DSTemp.FieldValues['beneficiario']);
      SetLoginUsuario(DSTemp.FieldValues['login_usuario']);
    end;
    Asignar;
  end
  else
  begin
    Limpiar;
  end;
end;

procedure TFrmOperacionBancaria.Obtener;
begin
  with Clase do
  begin
    SetCodigoCuenta(CmbCodigoCuenta.KeyValue);
    SetCodigoTipoOperacion(CmbCodigoTipoOperacionBancaria.KeyValue);
    SetNumero(TxtNumero.Text);
    SetFecha(DatFecha.Date);
    SetFechaContable(DatFechaContable.Date);
    SetMonto(TxtMonto.Value);
    SetConcepto(TxtConcepto.Text);
    SetObservaciones(TxtObservaciones.Text);
    SetDiferido(ChkDiferido.Checked);
    SetConciliado(ChkConciliado.Checked);
    SetFechaConciliacion(DatFechaConciliacion.Date);
    SetBeneficiario(TxtBeneficiario.Text);
    if Agregando then
    begin
      SetLoginUsuario(P.User.GetLoginUsuario);
    end;    
  end;
end;

procedure TFrmOperacionBancaria.Refrescar;
begin
  TxtCriterio.Text := '';
  Buscar;
  //Se Actualizan Los demas Componentes
  DataCuentaBancaria := CuentaBancaria.ObtenerCombo;
  DataTipoOperacionBancaria := TipoOperacionBancaria.ObtenerCombo;
  DataCuentaBancaria.First;
  DataTipoOperacionBancaria.First;
  DSCuentaBancaria.DataSet := DataCuentaBancaria;
  DSTipoOperacionBancaria.DataSet := DataTipoOperacionBancaria;
end;

function TFrmOperacionBancaria.Validar: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  (* Validando Informacion de la Cuenta Bancaria *)
  //Validando Codigo Cuenta
  if (Length(Trim(CmbCodigoCuenta.Text)) = 0) then
  begin
    ListaErrores.Add('El C�digo de la Cuenta Bancaria No Puede Estar En Blanco!!!');
  end;
  //Validando Codigo Tipo Operaci�n Bancaria
  if (Length(Trim(CmbCodigoTipoOperacionBancaria.Text)) = 0) then
  begin
    ListaErrores.Add('El C�digo del Tipo de Operaci�n Bancaria No Puede Estar En Blanco!!!');
  end;
  //Validando Numero
  if (Length(Trim(TxtNumero.Text)) = 0) then
  begin
    ListaErrores.Add('El N�mero de la Operaci�n No Puede Estar En Blanco!!!');
  end;
  //Validando Concepto
  if (Length(Trim(TxtConcepto.Text)) = 0) then
  begin
    ListaErrores.Add('El Concepto No Puede Estar En Blanco!!!');
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
