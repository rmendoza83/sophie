unit AgregarDetalleRadioContrato;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GenericoAgregarDetalle, StdCtrls, Buttons, ExtCtrls, Mask,
  rxToolEdit, rxCurrEdit, RxLookup, ClsRadioPrograma, ClsRadioContratoPrograma,
  ClsRadioEspacio, ClsRadioEmisora, GenericoBuscador, DBClient, Utils, DB;

type
  TFrmAgregarDetalleRadioContrato = class(TFrmGenericoAgregarDetalle)
    CmbCodigoEspacio: TRxDBLookupCombo;
    BtnBuscarEspacio: TBitBtn;
    LblCodigoEspacio: TLabel;
    LblCunasDiarias: TLabel;
    TxtCunasDiarias: TCurrencyEdit;
    LblTotalCunas: TLabel;
    TxtTotalCunas: TCurrencyEdit;
    LblDuracion: TLabel;
    TxtDuracion: TCurrencyEdit;
    TxtDescripcionPrograma: TEdit;
    TxtDescripcionEspacio: TEdit;
    DSRadioPrograma: TDataSource;
    DSRadioEspacio: TDataSource;
    procedure CmbCodigoEspacioChange(Sender: TObject);
    procedure BtnBuscarEspacioClick(Sender: TObject);
    procedure BtnBuscarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    RadioContratoPrograma: TRadioContratoPrograma;
    RadioEspacio: TRadioEspacio;
    RadioPrograma: TRadioPrograma;
    RadioEmisora: TRadioEmisora;
    DataRadioPrograma: TClientDataSet;
    DataRadioEspacio: TClientDataSet;
  protected
    { Protected declarations }
    function Buscar: Boolean; override;
    procedure Agregar; override;
    procedure Limpiar; override;
    function Validar: Boolean;
  public
    { Public declarations }
  end;

var
  FrmAgregarDetalleRadioContrato: TFrmAgregarDetalleRadioContrato;

implementation

uses ClsGenericoBD;

{$R *.dfm}

procedure TFrmAgregarDetalleRadioContrato.Agregar;
begin
  if (Validar) then
  begin
    with Data do
    begin
      Append;
      //Ubicando Programa
      RadioPrograma.SetCodigo(TxtCodigo.Text);
      RadioPrograma.Buscar;
      //Ubicando Emisora
      RadioEmisora.SetCodigo(RadioPrograma.GetCodigoEmisora);
      RadioEmisora.Buscar;
      //Ubicando Espacio
      RadioEspacio.SetCodigo(CmbCodigoEspacio.Text);
      RadioEspacio.Buscar;
      FieldValues['codigo_emisora'] := RadioEmisora.GetCodigo;
      FieldValues['nombre_emisora'] := RadioEmisora.GetRazonSocial;
      FieldValues['codigo_espacio'] := RadioEspacio.GetCodigo;
      FieldValues['nombre_espacio'] := RadioEspacio.GetDescripcion;
      FieldValues['codigo_programa'] := RadioPrograma.GetCodigo;
      FieldValues['nombre_programa'] := RadioPrograma.GetDescripcion;
      FieldValues['cunas_diarias'] := TxtCunasDiarias.Value;
      FieldValues['total_cunas'] := TxtTotalCunas.Value;
      FieldValues['duracion'] := TxtDuracion.Value;
      FieldValues['monto_agencia'] := 0;
      FieldValues['porcentaje'] := 0;
      FieldValues['monto_emisora'] := 0;
      FieldValues['comision'] := 0;
      FieldValues['monto_real'] := 0;
      //Calculando Tarifa, Costo Bruto 
      //Calculando Tarifa y Costo Bruto
      if (RadioPrograma.GetTarifaSegundo > 0) then
      begin
        FieldValues['tarifa'] := RadioPrograma.GetTarifaSegundo;
        FieldValues['costo_bruto'] := Round(RadioPrograma.GetTarifaSegundo * TxtDuracion.Value * TxtTotalCunas.Value);
      end
      else
      begin
        FieldValues['tarifa'] := RadioPrograma.GetTarifaDia;
        FieldValues['costo_bruto'] := Round(RadioPrograma.GetTarifaDia * TxtTotalCunas.Value);
      end;
      FieldValues['p_descuento'] := 0;
      //Porcentaje de Descuento, Monto Agencia, Monto Emisora, Monto Real, Porcentaje y Comision
      //Se Asignaran en el Principal (FrmRadioContrato - DataBeforePost) 
      Post;
    end;
    inherited;
  end
  else
  begin
    ModalResult := mrNone;
  end;
end;

procedure TFrmAgregarDetalleRadioContrato.BtnBuscarClick(Sender: TObject);
begin
  inherited;
  Buscador(Self,RadioPrograma,'codigo','Buscar Programa',TxtCodigo,nil);
  if (Length(Trim(TxtCodigo.Text)) > 0) then
  begin
    ComponenteFoco.SetFocus;
  end;
end;

procedure TFrmAgregarDetalleRadioContrato.BtnBuscarEspacioClick(
  Sender: TObject);
begin
  Buscador(Self,RadioEspacio,'codigo','Buscar Espacio',nil,DataRadioEspacio);
  CmbCodigoEspacio.KeyValue := DataRadioEspacio.FieldValues['codigo'];
  TxtDescripcionEspacio.Text := DataRadioEspacio.FieldValues['descripcion'];
end;

function TFrmAgregarDetalleRadioContrato.Buscar: Boolean;
begin
  RadioPrograma.SetCodigo(TxtCodigo.Text);
  Result := RadioPrograma.Buscar;
  if (Result) then
  begin
    TxtDescripcionPrograma.Text := RadioPrograma.GetDescripcion;
  end
  else
  begin
    MessageBox(Self.Handle, Cadena('El Codigo No Existe!!!'), PChar(MsgTituloError), MB_ICONSTOP or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Limpiar;
  end;
end;

procedure TFrmAgregarDetalleRadioContrato.CmbCodigoEspacioChange(
  Sender: TObject);
begin
  TxtDescripcionEspacio.Text := DataRadioEspacio.FieldValues['descripcion'];
end;

procedure TFrmAgregarDetalleRadioContrato.FormCreate(Sender: TObject);
begin
  inherited;
  ComponenteFoco := CmbCodigoEspacio;
  RadioContratoPrograma := TRadioContratoPrograma.Create;
  RadioEspacio := TRadioEspacio.Create;
  RadioPrograma := TRadioPrograma.Create;
  RadioEmisora := TRadioEmisora.Create;
  DataRadioPrograma := RadioPrograma.ObtenerComboActivo;
  DataRadioEspacio := RadioEspacio.ObtenerLista;
  DataRadioPrograma.First;
  DataRadioEspacio.First;
  DSRadioPrograma.DataSet := DataRadioPrograma;
  DSRadioEspacio.DataSet := DataRadioEspacio;
  ConfigurarRxDBLookupCombo(CmbCodigoEspacio,DSRadioEspacio,'codigo','codigo;descripcion');
end;

procedure TFrmAgregarDetalleRadioContrato.Limpiar;
begin
  inherited;
  TxtDescripcionPrograma.Text := '';
  CmbCodigoEspacio.KeyValue := '';
  TxtDescripcionEspacio.Text := '';
  TxtCunasDiarias.Value := 0;
  TxtTotalCunas.Value := 0;
  TxtDuracion.Value := 0;
end;

function TFrmAgregarDetalleRadioContrato.Validar: Boolean;
var
  ListaErrores: TStringList;
  sw: Boolean;
begin
  ListaErrores := TStringList.Create;
  //Validando Existencia del Programa
  sw := false;
  with Data do
  begin
    First;
    while (not Eof) do
    begin
      if (FieldValues['codigo_programa'] = TxtCodigo.Text) then
      begin
        sw := True;
        Break;
      end;      
      Next;
    end;    
  end;
  if (sw) then
  begin
    ListaErrores.Add('El Programa Ya Se Encuentra En La Lista!!!');
  end;
  //Validando Cu�as Diarias
  if (TxtCunasDiarias.Value <= 0) then
  begin
    RadioEspacio.SetCodigo(CmbCodigoEspacio.Text);
    RadioEspacio.Buscar;
    if (RadioEspacio.GetRotativo) then
    begin
      ListaErrores.Add('El Espacio Seleccionado Es Rotativo!!! La Cantidad de Cu�as Diarias Debe Ser Mayor A Cero!!!');
    end;
  end;
  //Validando Total Cu�as
  if (TxtTotalCunas.Value <= 0) then
  begin
    ListaErrores.Add('El Total de Cu�as Debe Ser Mayor A Cero!!!');
  end;
  //Validando Duracion
  if (TxtDuracion.Value <= 0) then
  begin
    ListaErrores.Add('La Duraci�n de La Cu�a Debe Ser Mayor A Cero!!!');
  end;
  //Validando Espacio
  if (Length(Trim(CmbCodigoEspacio.Text)) = 0) then
  begin
    ListaErrores.Add('El Espacio No Puede Estar En Blanco!!!');
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
