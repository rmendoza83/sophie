inherited FrmRadioAnunciante: TFrmRadioAnunciante
  Caption = 'Sophie - Anunciantes [Radio]'
  ExplicitWidth = 598
  ExplicitHeight = 400
  PixelsPerInch = 96
  TextHeight = 13
  inherited Pag: TPageControl
    ActivePage = TabBusqueda
    inherited TabBusqueda: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 24
      ExplicitWidth = 584
      ExplicitHeight = 304
      inherited GrdBusqueda: TDBGrid
        OnCellClick = GrdBusquedaCellClick
        Columns = <
          item
            Expanded = False
            FieldName = 'codigo'
            Title.Caption = 'C'#243'digo'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'descripcion'
            Title.Caption = 'Descripci'#243'n'
            Width = 300
            Visible = True
          end>
      end
    end
    inherited TabDatos: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 24
      ExplicitWidth = 584
      ExplicitHeight = 304
      object LblDescripcion: TLabel
        Left = 45
        Top = 35
        Width = 68
        Height = 13
        Alignment = taRightJustify
        Caption = 'Descripci'#243'n:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
      end
      object LblCodigo: TLabel
        Left = 72
        Top = 11
        Width = 41
        Height = 13
        Alignment = taRightJustify
        Caption = 'C'#243'digo:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clMaroon
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
      end
      object TxtCodigo: TEdit
        Left = 119
        Top = 8
        Width = 150
        Height = 21
        TabOrder = 0
      end
      object TxtDescripcion: TEdit
        Left = 119
        Top = 32
        Width = 278
        Height = 21
        TabOrder = 1
      end
    end
  end
end
