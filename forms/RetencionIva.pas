unit RetencionIva;

{$I sophie.inc}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GenericoDetalle, DB, DBClient, ImgList, XPMan, ComCtrls, Grids,
  DBGrids, ExtCtrls, ToolWin, StdCtrls, Buttons, rxCurrEdit, Mask, rxToolEdit,
  RxLookup, Utils, ClsRetencionIva, ClsNotaCreditoProveedor,
  ClsNotaDebitoProveedor, ClsFacturacionProveedor, ClsTipoRetencion,
  ClsProveedor, ClsDetRetencionIva, ClsZona, ClsBusquedaRetencionIva,
  GenericoBuscador, ClsReporte, ClsParametro{$IFDEF DELPHI10}, ImageList{$ENDIF};

type
  TFrmRetencionIva = class(TFrmGenericoDetalle)
    LblNumero: TLabel;
    LblCodigoProveedor: TLabel;
    LblCodigoZona: TLabel;
    LblFechaIngreso: TLabel;
    LblTotal: TLabel;
    LblFechaContable: TLabel;
    TxtNumero: TEdit;
    CmbCodigoProveedor: TRxDBLookupCombo;
    DatFechaIngreso: TDateEdit;
    CmbCodigoZona: TRxDBLookupCombo;
    TxtTotal: TCurrencyEdit;
    BtnBuscarProveedor: TBitBtn;
    BtnBuscarZona: TBitBtn;
    DatFechaContable: TDateEdit;
    Datadesde: TStringField;
    Datanumero_documento: TStringField;
    Datacodigo_retencion_1: TStringField;
    Datamonto_retencion_1: TFloatField;
    DSProveedor: TDataSource;
    DSZona: TDataSource;
    Datamonto_base: TFloatField;
    Datap_iva: TFloatField;
    Dataimpuesto: TFloatField;
    Datadescripcion_retencion: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure DataAfterPost(DataSet: TDataSet);
    procedure GridDetalleKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtnBuscarProveedorClick(Sender: TObject);
    procedure CmbCodigoProveedorExit(Sender: TObject);
    procedure CmbCodigoProveedorChange(Sender: TObject);
    procedure BtnBuscarZonaClick(Sender: TObject);
    procedure TBtnEliminarClick(Sender: TObject);
  private
    { Private declarations }
    Clase: TRetencionIva;
    DetRetencionIva: TDetRetencionIva;
    BuscadorRetencionIva: TBusquedaRetencionIva;
    Proveedor: TProveedor;
    Zona: TZona;
    NotaCreditoProveedor: TNotaCreditoProveedor;
    NotaDebitoProveedor: TNotaDebitoProveedor;
    FacturaProveedor: TFacturacionProveedor;
    TipoRetencion: TTipoRetencion;
    Reporte: TReporte;
    Parametro: TParametro;
    DataBuscadorRetencionIva: TClientDataSet;
    DataProveedor: TClientDataSet;
    DataZona: TClientDataSet;
    procedure Agregar;
    procedure Editar;
    procedure Eliminar;
    procedure Guardar;
    procedure Cancelar;
    procedure Refrescar;
    procedure Imprimir;
    procedure Buscar;
    procedure Limpiar;
    procedure Asignar;
    procedure Obtener;
    procedure Mover;
    procedure GuardarDetalles;
    procedure CalcularMontos;
  protected
    { Protected declarations }
    function Validar: Boolean; override;
    procedure Detalle; override;
  public
    { Public declarations }
  end;

var
  FrmRetencionIva: TFrmRetencionIva;

implementation

uses
  ActividadSophie,
  PModule,
  ClsGenericoBD,
  AgregarDetalleRetencionIva
  {$IFDEF USE_RAVE_REPORTS}
  , DMRR
  {$ENDIF}
  {$IFDEF USE_FAST_REPORTS}
  , DMFR
  {$ENDIF}
  ;

{$R *.dfm}

{ TFrmRetencionIva }

procedure TFrmRetencionIva.Agregar;
begin
  //Verificando
  Limpiar;
  //Asignando Valores por Defecto
  CmbCodigoZona.Value := Parametro.ObtenerValor('codigo_zona');
  CmbCodigoProveedor.SetFocus;
end;

procedure TFrmRetencionIva.Asignar;
var
  DataDetalle: TClientDataSet;
  Campo: string;
  i: Integer;

  function BuscarDescripcionRetencion(varCodigo: string): string;
  begin
    Result := '';
    if (Length(Trim(varCodigo)) > 0) then
    begin
      TipoRetencion.SetCodigo(varCodigo);
      TipoRetencion.Buscar;
      Result := TipoRetencion.GetDescripcion;
    end;       
  end;

begin
  with Clase do
  begin
    TxtNumero.Text := GetNumero;
    //NumeroPedido
    CmbCodigoProveedor.KeyValue := GetCodigoProveedor;
    CmbCodigoZona.KeyValue := GetCodigoZona;
    DatFechaIngreso.Date := GetFechaIngreso;
    DatFechaContable.Date := GetFechaContable;
    TxtTotal.Value := GetTotal;
    //Cargando DataSet Secundario "Data"
    DataDetalle := TClientDataSet.Create(Self);
    DataDetalle.CloneCursor(DetRetencionIva.ObtenerNumero(GetNumero),False,True);
    Data.EmptyDataSet;
    DataDetalle.First;
    while (not DataDetalle.Eof) do
    begin
      Data.Insert;
      //Asignando Campos Similares
      for i := 0 to DataDetalle.FieldCount - 1 do
      begin
        Campo := DataDetalle.FieldDefs[i].Name;
        if (Data.FieldDefs.IndexOf(Campo) <> -1) then
        begin
          Data.FieldValues[Campo] := DataDetalle.FieldValues[Campo];
        end;        
      end;
      //Asignando Campos Faltantes
      if (DataDetalle.FieldValues['desde'] = 'NCP') then
      begin //Nota de Credito de Proveedor
        NotaCreditoProveedor.SetNumero(DataDetalle.FieldValues['numero_documento']);
        NotaCreditoProveedor.BuscarNumero;
        Data.FieldValues['monto_base'] := NotaCreditoProveedor.GetMontoNeto;
        Data.FieldValues['p_iva'] := NotaCreditoProveedor.GetPIva;
        Data.FieldValues['impuesto'] := NotaCreditoProveedor.GetImpuesto;
      end
      else
      begin
        if (DataDetalle.FieldValues['desde'] = 'NDP') then
        begin //Nota Debito de Proveedor
          NotaDebitoProveedor.SetNumero(DataDetalle.FieldValues['numero_documento']);
          NotaDebitoProveedor.BuscarNumero;
          Data.FieldValues['monto_base'] := NotaDebitoProveedor.GetMontoNeto;
          Data.FieldValues['p_iva'] := NotaDebitoProveedor.GetPIva;
          Data.FieldValues['impuesto'] := NotaDebitoProveedor.GetImpuesto;
        end
        else
        begin //Factura de Proveedor
          FacturaProveedor.SetNumero(DataDetalle.FieldValues['numero_documento']);
          FacturaProveedor.BuscarNumero;
          Data.FieldValues['monto_base'] := FacturaProveedor.GetMontoNeto;
          Data.FieldValues['p_iva'] := FacturaProveedor.GetPIva;
          Data.FieldValues['impuesto'] := FacturaProveedor.GetImpuesto;
        end;
      end;
      Data.FieldValues['descripcion_retencion'] := BuscarDescripcionRetencion(DataDetalle.FieldValues['codigo_retencion']);
      Data.Post;
      DataDetalle.Next;
    end;
    DS.DataSet := Data;
  end;
end;

procedure TFrmRetencionIva.BtnBuscarProveedorClick(Sender: TObject);
begin
  Buscador(Self,Proveedor,'codigo','Buscar Proveedor',nil,DataProveedor);
  CmbCodigoProveedor.KeyValue := DataProveedor.FieldValues['codigo'];
  CmbCodigoProveedorExit(Sender);
end;

procedure TFrmRetencionIva.BtnBuscarZonaClick(Sender: TObject);
begin
  Buscador(Self,Zona,'codigo','Buscar Zona',nil,DataZona);
  CmbCodigoZona.KeyValue := DataZona.FieldValues['codigo'];
end;

procedure TFrmRetencionIva.Buscar;
begin
  if (Buscador(Self,BuscadorRetencionIva,'numero','Buscar Retenciones',nil,DataBuscadorRetencionIva)) then
  begin
    Clase.SetNumero(DataBuscadorRetencionIva.FieldValues['numero']);
    Clase.BuscarNumero;
    Asignar;
  end;
end;

procedure TFrmRetencionIva.CalcularMontos;
var
  Neto: Currency;
begin
  Neto := 0;
  with Data do
  begin
    DisableControls;
    First;
    while (not Eof) do
    begin
      Neto := Neto + FieldValues['monto_retencion'];
      Next;
    end;
    EnableControls;
  end;
  //Asignando Monto
  TxtTotal.Value := Neto;
end;

procedure TFrmRetencionIva.Cancelar;
begin
  if (Agregando) then
  begin
    Limpiar;
  end
  else
  begin
    Clase.SetNumero(TxtNumero.Text);
    Clase.BuscarNumero;
    Asignar;
  end;
end;

procedure TFrmRetencionIva.CmbCodigoProveedorChange(Sender: TObject);
begin
  SB.Panels[0].Text := DataProveedor.FieldValues['razon_social'];
end;

procedure TFrmRetencionIva.CmbCodigoProveedorExit(Sender: TObject);
begin
  if (Length(Trim(CmbCodigoProveedor.Text)) > 0) then
  begin
    if (Agregando) then
    begin
      Proveedor.SetCodigo(CmbCodigoProveedor.Value);
      Proveedor.BuscarCodigo;
      CmbCodigoZona.Value := Proveedor.GetCodigoZona;
    end;
  end;
end;

procedure TFrmRetencionIva.DataAfterPost(DataSet: TDataSet);
begin
  CalcularMontos;
end;

procedure TFrmRetencionIva.Detalle;
begin
  inherited;
  FrmAgregarDetalleRetencionIva := TFrmAgregarDetalleRetencionIva.CrearDetalle(Self,Data);
  FrmAgregarDetalleRetencionIva.CodigoProveedor := CmbCodigoProveedor.Text;
  FrmAgregarDetalleRetencionIva.ShowModal;
  FreeAndNil(FrmAgregarDetalleRetencionIva);
end;

procedure TFrmRetencionIva.Editar;
begin
  if (Length(Trim(TxtNumero.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
end;

procedure TFrmRetencionIva.Eliminar;
begin
  //Se Busca La Declaracion de Retenciones
  Clase.SetNumero(TxtNumero.Text);
  Clase.BuscarNumero;
  //Se Eliminan Los Detalles
  DetRetencionIva.EliminarNumero(Clase.GetNumero);
  Clase.Eliminar;
  MessageBox(Self.Handle, 'Las Retenciones IVA Se Han Eliminado Con Exito!!!',PChar(MsgTituloInformacion), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  Refrescar;
  Limpiar;
end;

procedure TFrmRetencionIva.FormCreate(Sender: TObject);
begin
  inherited;
  ProcAgregar := Agregar;
  ProcEditar := Editar;
  ProcEliminar := Eliminar;
  ProcGuardar := Guardar;
  ProcCancelar := Cancelar;
  ProcRefrescar := Refrescar;
  ProcImprimir := Imprimir;
  ProcBuscar := Buscar;
  FrmActividadSophie.Show;
  FrmActividadSophie.Actividad('Cargando N�cleo...');
  Clase := TRetencionIva.Create;
  //Inicializando DataSets Secundarios
  BuscadorRetencionIva := TBusquedaRetencionIva.Create;
  Proveedor := TProveedor.Create;
  Zona := TZona.Create;
  NotaCreditoProveedor := TNotaCreditoProveedor.Create;
  NotaDebitoProveedor := TNotaDebitoProveedor.Create;
  FacturaProveedor := TFacturacionProveedor.Create;
  TipoRetencion := TTipoRetencion.Create;
  Reporte := TReporte.Create;
  Parametro := TParametro.Create;
  DetRetencionIva := TDetRetencionIva.Create;
  FrmActividadSophie.Actividad('Cargando Proveedores...');
  DataProveedor := Proveedor.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Zonas...');
  DataZona := Zona.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Resto del N�cleo...');
  DataBuscadorRetencionIva := BuscadorRetencionIva.ObtenerLista;
  DataProveedor.First;
  DataZona.First;
  DataBuscadorRetencionIva.First;
  DSProveedor.DataSet := DataProveedor;
  DSZona.DataSet := DataZona;
  ConfigurarRxDBLookupCombo(CmbCodigoProveedor,DSProveedor,'codigo','codigo;razon_social');
  ConfigurarRxDBLookupCombo(CmbCodigoZona,DSZona,'codigo','codigo;descripcion');
  DS.DataSet := Data;
  GridDetalle.DataSource := DS;
  FrmActividadSophie.Close;
  //Buscar;
end;

procedure TFrmRetencionIva.GridDetalleKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Agregando or Editando) then
  begin
    if (((Key = VK_INSERT) or (Key = VK_DELETE)) and (Length(Trim(CmbCodigoProveedor.Text)) = 0)) then
    begin
      MessageBox(Self.Handle, 'Seleccione Primero un Proveedor!!!', PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      CmbCodigoProveedor.SetFocus;
      CmbCodigoProveedor.DropDown;
      Abort;
    end;
  end;
  //Llamando a Evento Padre
  inherited;
end;

procedure TFrmRetencionIva.Guardar;
begin
  CalcularMontos;
  Obtener;
  if (Agregando) then
  begin
    //Agregando...
    //Guardando Encabezado
    Clase.Insertar;
    TxtNumero.Text := Clase.GetNumero;
    Application.ProcessMessages;
    //Guardando Detalles
    GuardarDetalles;
    MessageBox(Self.Handle, 'Las Retenciones IVA Han Sido Agregadas Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    //Se Imprime El Comprobante
    MessageBox(Self.Handle, 'Presione "Aceptar" Para Proceder A Imprimir El Comprobante de Retenci�n!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    with Reporte do
    begin
      AgregarParametro('NumeroRetencion',Clase.GetNumero);
      AgregarParametro('MontoLetra',NumeroALetras(Clase.GetTotal));
      EmiteReporte('RetencionIva',trGeneral);
    end;
  end
  else
  begin
    //Editando...
    Clase.Modificar;
    //Actualizando Detalles (Eliminando Anteriores y Cargando los Nuevos)
    DetRetencionIva.EliminarNumero(Clase.GetNumero);
    //Guardando Detalles
    GuardarDetalles;
    MessageBox(Self.Handle, 'Las Retenciones IVA Han Sido Editadas Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end;
  //Actualizando Dataset de Busqueda
  DataBuscadorRetencionIva := BuscadorRetencionIva.ObtenerLista;
end;

procedure TFrmRetencionIva.GuardarDetalles;
begin
  with Data do
  begin
    DisableControls;
    First;
    while (not Eof) do
    begin
      with DetRetencionIva do
      begin
        SetNumero(Clase.GetNumero);
        SetDesde(FieldValues['desde']);
        SetNumeroDocumento(FieldValues['numero_documento']);
        SetCodigoRetencion(FieldValues['codigo_retencion']);
        SetMontoRetencion(FieldValues['monto_retencion']);
        Insertar;
      end;
      Next;
    end;
    EnableControls;
  end;
end;

procedure TFrmRetencionIva.Imprimir;
begin
  if (Length(Trim(TxtNumero.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
  with Reporte do
  begin
    AgregarParametro('NumeroRetencion',Clase.GetNumero);
    AgregarParametro('MontoLetra',NumeroALetras(Clase.GetTotal));
    EmiteReporte('RetencionIva',trGeneral);
  end;
end;

procedure TFrmRetencionIva.Limpiar;
begin
  TxtNumero.Text := '';
  CmbCodigoProveedor.KeyValue := '';
  CmbCodigoZona.KeyValue := '';
  DatFechaIngreso.Date := Date;
  DatFechaContable.Date := Date;
  TxtTotal.Value := 0;
  //Status Bar
  SB.Panels[0].Text := '';
  Data.EmptyDataSet;
end;

procedure TFrmRetencionIva.Mover;
begin

end;

procedure TFrmRetencionIva.Obtener;
begin
  with Clase do
  begin
    SetNumero(TxtNumero.Text);
    SetCodigoProveedor(CmbCodigoProveedor.KeyValue);
    SetCodigoZona(CmbCodigoZona.KeyValue);
    SetFechaIngreso(DatFechaIngreso.Date);
    SetFechaContable(DatFechaContable.Date);
    SetTotal(TxtTotal.Value);
  end;
end;

procedure TFrmRetencionIva.Refrescar;
begin
  //Actualizando Dataset de Busqueda
  DataBuscadorRetencionIva := BuscadorRetencionIva.ObtenerLista;
  //Se Actualizan Los demas Componentes
  DataProveedor := Proveedor.ObtenerCombo;
  DataZona := Zona.ObtenerCombo;
  DataProveedor.First;
  DataZona.First;
  DataBuscadorRetencionIva.First;
  DSProveedor.DataSet := DataProveedor;
  DSZona.DataSet := DataZona;
end;

procedure TFrmRetencionIva.TBtnEliminarClick(Sender: TObject);
begin
  if (Length(Trim(TxtNumero.Text)) > 0) then
  begin
    inherited;
  end
  else
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
end;

function TFrmRetencionIva.Validar: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  //Validando Proveedor
  if (Length(Trim(CmbCodigoProveedor.Text)) = 0) then
  begin
    ListaErrores.Add('El Proveedor No Puede Estar En Blanco!!!');
  end;
  //Validando Monto
  if (TxtTotal.Value <= 0) then
  begin
    ListaErrores.Add('El Total Debe Ser Positivo!!!');
  end;
  //Validando Fechas
  if (DatFechaIngreso.Date > DatFechaContable.Date) then
  begin
    ListaErrores.Add('La Fecha de Ingreso Debe Ser Menor a la Fecha Contable!!!');
  end;
  //Validando Detalle
  if (Data.RecordCount = 0) then
  begin
    ListaErrores.Add('El Detalle De Las Retenciones No Puede Estar Vacio!!!');
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
