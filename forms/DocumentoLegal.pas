unit DocumentoLegal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Utils;

type
  TFrmDocumentoLegal = class(TForm)
    LblSerie: TLabel;
    TxtSerie: TEdit;
    LblNumeroControl: TLabel;
    TxtNumeroControl: TEdit;
    LblDesde: TLabel;
    TxtDesde: TLabel;
    LblNumeroDocumento: TLabel;
    TxtNumeroDocumento: TLabel;
    PanelBotones: TPanel;
    Aceptar: TBitBtn;
    BtnCerrar: TBitBtn;
    procedure BtnCerrarClick(Sender: TObject);
    procedure AceptarClick(Sender: TObject);
  private
    { Private declarations }
    constructor Create(AOwner: TComponent; varDesde, varNumeroDocumento, varSerie, varNumeroControl: string); reintroduce;
    procedure Mostrar;
  public
    { Public declarations }
    Desde: string;
    NumeroDocumento: string;
    Serie: string;
    NumeroControl: string;
    Agregar: Boolean;
    Editar: Boolean;
    Aceptado: Boolean;
    constructor CreateEdit(AOwner: TComponent; varDesde, varNumeroDocumento, varSerie, varNumeroControl: string);
    constructor CreateInsert(AOwner: TComponent; varDesde, varNumeroDocumento, varSerie, varNumeroControl: string);
    function Validar: Boolean;    
  end;

var
  FrmDocumentoLegal: TFrmDocumentoLegal;

implementation

{$R *.dfm}

{ TFrmDocumentoLegal }

procedure TFrmDocumentoLegal.AceptarClick(Sender: TObject);
begin
  if (Validar) then
  begin
    if (MessageBox(Self.Handle, PChar('�Esta Seguro?'), PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_YESNO or MB_TOPMOST or MB_APPLMODAL) = mrYes) then
    begin
      Serie := TxtSerie.Text;
      NumeroControl := TxtNumeroControl.Text;
      Aceptado := True;
      Close;
    end;
  end;
end;

procedure TFrmDocumentoLegal.BtnCerrarClick(Sender: TObject);
begin
  Aceptado := False;
end;

constructor TFrmDocumentoLegal.Create(AOwner: TComponent; varDesde,
  varNumeroDocumento, varSerie, varNumeroControl: string);
begin
  inherited Create(AOwner);
  Desde := varDesde;
  NumeroDocumento := varNumeroDocumento;
  Serie := varSerie;
  NumeroControl := varNumeroControl;
  Agregar := False;
  Editar := False;
  Aceptado := False;
  Mostrar;
end;

constructor TFrmDocumentoLegal.CreateEdit(AOwner: TComponent; varDesde,
  varNumeroDocumento, varSerie, varNumeroControl: string);
begin
  Create(AOwner,varDesde,varNumeroDocumento,varSerie,varNumeroControl);
  Agregar := False;
  Editar := True;
end;

constructor TFrmDocumentoLegal.CreateInsert(AOwner: TComponent; varDesde,
  varNumeroDocumento, varSerie, varNumeroControl: string);
begin
  Create(AOwner,varDesde,varNumeroDocumento,varSerie,varNumeroControl);
  Agregar := True;
  Editar := False;
end;

procedure TFrmDocumentoLegal.Mostrar;
begin
  TxtDesde.Caption := Desde;
  TxtNumeroDocumento.Caption := NumeroDocumento;
  TxtSerie.Text := Serie;
  TxtNumeroControl.Text := NumeroControl;
end;

function TFrmDocumentoLegal.Validar: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  //Validando Serie
  if (Length(Trim(TxtSerie.Text)) = 0) then
  begin
    ListaErrores.Add('La Serie No Puede Estar En Blanco!!!');
  end;
  //Validando Numero de Control
  if (Length(Trim(TxtNumeroControl.Text)) = 0) then
  begin
    ListaErrores.Add('El Numero de Control No Puede Estar En Blanco!!!');
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
