unit NotaDebitoCliente;

{$I sophie.inc}
 
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GenericoDetalle, DB, DBClient, ImgList, XPMan, ComCtrls, Grids,
  DBGrids, ExtCtrls, ToolWin, rxCurrEdit, Mask, rxToolEdit, StdCtrls, Buttons,
  RxLookup, Utils, ClsNotaDebitoCliente, ClsCliente, ClsVendedor, ClsZona,
  ClsEsquemaPago, ClsMoneda, ClsBusquedaNotaDebitoCliente, ClsCuentaxCobrar,
  GenericoBuscador, ClsReporte, ClsParametro, ClsCobranzaCobrada,
  ClsDocumentoLegal, ClsDocumentoAnulado{$IFDEF DELPHI10}, ImageList{$ENDIF};

type
  TFrmNotaDebitoCliente = class(TFrmGenericoDetalle)
    TabConcepto: TTabSheet;
    TabObservaciones: TTabSheet;
    TxtConcepto: TRichEdit;
    TxtObservaciones: TRichEdit;
    LblNumero: TLabel;
    TxtNumero: TEdit;
    LblCodigoCliente: TLabel;
    CmbCodigoCliente: TRxDBLookupCombo;
    BtnBuscarCliente: TBitBtn;
    LblCodigoVendedor: TLabel;
    CmbCodigoVendedor: TRxDBLookupCombo;
    BtnBuscarVendedor: TBitBtn;
    LblCodigoZona: TLabel;
    CmbCodigoZona: TRxDBLookupCombo;
    BtnBuscarZona: TBitBtn;
    LblEsquemaPago: TLabel;
    CmbCodigoEsquemaPago: TRxDBLookupCombo;
    BtnBuscarEsquemaPago: TBitBtn;
    LblMoneda: TLabel;
    CmbCodigoMoneda: TRxDBLookupCombo;
    BtnBuscarMoneda: TBitBtn;
    LblFechaIngreso: TLabel;
    DatFechaIngreso: TDateEdit;
    LblRecepcion: TLabel;
    DatFechaRecepcion: TDateEdit;
    LblFechaContable: TLabel;
    DatFechaContable: TDateEdit;
    LblFechaLibros: TLabel;
    DatFechaLibros: TDateEdit;
    LblMontoNeto: TLabel;
    TxtMontoNeto: TCurrencyEdit;
    LblMontoDescuento: TLabel;
    TxtMontoDescuento: TCurrencyEdit;
    TxtPDescuento: TLabel;
    LblSubTotal: TLabel;
    TxtSubTotal: TCurrencyEdit;
    LblImpuesto: TLabel;
    TxtImpuesto: TCurrencyEdit;
    TxtPIVA: TLabel;
    LblTotal: TLabel;
    TxtTotal: TCurrencyEdit;
    DSCliente: TDataSource;
    DSVendedor: TDataSource;
    DSZona: TDataSource;
    DSEsquemaPago: TDataSource;
    DSMoneda: TDataSource;
    procedure TxtMontoNetoExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TxtPIVADblClick(Sender: TObject);
    procedure LblImpuestoDblClick(Sender: TObject);
    procedure TxtPDescuentoDblClick(Sender: TObject);
    procedure LblMontoDescuentoDblClick(Sender: TObject);
    procedure BtnBuscarMonedaClick(Sender: TObject);
    procedure BtnBuscarEsquemaPagoClick(Sender: TObject);
    procedure BtnBuscarZonaClick(Sender: TObject);
    procedure BtnBuscarVendedorClick(Sender: TObject);
    procedure BtnBuscarClienteClick(Sender: TObject);
    procedure CmbCodigoClienteChange(Sender: TObject);
    procedure CmbCodigoClienteExit(Sender: TObject);
    procedure TBtnEliminarClick(Sender: TObject);
  private
    { Private declarations }
    Clase: TNotaDebitoCliente;
    CuentaxCobrar: TCuentaxCobrar;
    CobranzaCobrada: TCobranzaCobrada;
    BuscadorNotaDebitoCliente: TBusquedaNotaDebitoCliente;
    Cliente: TCliente;
    Vendedor: TVendedor;
    Zona: TZona;
    EsquemaPago: TEsquemaPago;
    Moneda: TMoneda;
    Reporte: TReporte;
    Parametro: TParametro;
    DocumentoLegal: TDocumentoLegal;
    DocumentoAnulado: TDocumentoAnulado;
    DataBuscadorNotaDebitoCliente: TClientDataSet;
    DataCliente: TClientDataSet;
    DataVendedor: TClientDataSet;
    DataZona: TClientDataSet;
    DataEsquemaPago: TClientDataSet;
    DataMoneda: TClientDataSet;
    procedure Agregar;
    procedure Editar;
    procedure Eliminar;
    procedure Guardar;
    procedure Cancelar;
    procedure Refrescar;
    procedure Imprimir;
    procedure Buscar;
    procedure Limpiar;
    procedure Asignar;
    procedure Obtener;
    procedure Mover;
    procedure GuardarCuentaxCobrar;
    procedure CalcularMontosCabecera;
  protected
    { Protected declarations }
    function Validar: Boolean; override;
  public
    { Public declarations }
  end;

var
  FrmNotaDebitoCliente: TFrmNotaDebitoCliente;

implementation

uses
  ActividadSophie,
  PModule,
  ClsGenericoBD,
  DetalleContadoCobranza,
  SolicitarMonto,
  ClsFacturacionCliente,
  DocumentoLegal
  {$IFDEF USE_RAVE_REPORTS}
  , DMRR
  {$ENDIF}
  {$IFDEF USE_FAST_REPORTS}
  , DMFR
  {$ENDIF}
  ;

{$R *.dfm}

{ TFrmNotaDebitoCliente }

procedure TFrmNotaDebitoCliente.Agregar;
begin
  //Verificando
  Limpiar;
  //Asignando Valores por Defecto
  TxtPIVA.Caption := FormatFloat('#,##0.0000%',Parametro.ObtenerValor('p_iva'));
  Clase.SetPIva(Parametro.ObtenerValor('p_iva'));
  CmbCodigoZona.Value := Parametro.ObtenerValor('codigo_zona');
  CmbCodigoEsquemaPago.Value := Parametro.ObtenerValor('codigo_esquema_pago');
  CmbCodigoMoneda.Value := Parametro.ObtenerValor('codigo_moneda');
  CmbCodigoCliente.SetFocus;
end;

procedure TFrmNotaDebitoCliente.Asignar;
begin
  TabDetalle.TabVisible := False;
  PageDetalles.ActivePage := TabConcepto;
  TxtMontoNeto.ReadOnly := False;
  with Clase do
  begin
    TxtNumero.Text := GetNumero;
    CmbCodigoCliente.KeyValue := GetCodigoCliente;
    CmbCodigoVendedor.KeyValue := GetCodigoVendedor;
    CmbCodigoZona.KeyValue := GetCodigoZona;
    CmbCodigoEsquemaPago.KeyValue := GetCodigoEsquemaPago;
    CmbCodigoMoneda.KeyValue := GetCodigoMoneda;
    DatFechaIngreso.Date := GetFechaIngreso;
    DatFechaRecepcion.Date := GetFechaRecepcion;
    DatFechaContable.Date := GetFechaContable;
    DatFechaLibros.Date := GetFechaLibros;
    TxtConcepto.Text := GetConcepto;
    TxtObservaciones.Text := GetObservaciones;
    TxtMontoNeto.Value := GetMontoNeto;
    TxtMontoDescuento.Value := GetMontoDescuento;
    TxtSubTotal.Value := GetSubTotal;
    TxtImpuesto.Value := GetImpuesto;
    TxtTotal.Value := GetTotal;
    TxtPDescuento.Caption := FormatFloat('###0.0000%',GetPDescuento);
    TxtPIVA.Caption := FormatFloat('###0.0000%',GetPIva);
  end;
end;

procedure TFrmNotaDebitoCliente.BtnBuscarClienteClick(Sender: TObject);
begin
  Buscador(Self,Cliente,'codigo','Buscar Cliente',nil,DataCliente);
  CmbCodigoCliente.KeyValue := DataCliente.FieldValues['codigo'];
  CmbCodigoClienteExit(Sender);
end;

procedure TFrmNotaDebitoCliente.BtnBuscarEsquemaPagoClick(Sender: TObject);
begin
  Buscador(Self,EsquemaPago,'codigo','Buscar Esquema de Pago',nil,DataEsquemaPago);
  CmbCodigoEsquemaPago.KeyValue := DataEsquemaPago.FieldValues['codigo'];
end;

procedure TFrmNotaDebitoCliente.BtnBuscarMonedaClick(Sender: TObject);
begin
  Buscador(Self,Moneda,'codigo','Buscar Moneda',nil,DataMoneda);
  CmbCodigoMoneda.KeyValue := DataMoneda.FieldValues['codigo'];
end;

procedure TFrmNotaDebitoCliente.BtnBuscarVendedorClick(Sender: TObject);
begin
  Buscador(Self,Vendedor,'codigo','Buscar Vendedor',nil,DataVendedor);
  CmbCodigoVendedor.KeyValue := DataVendedor.FieldValues['codigo'];
end;

procedure TFrmNotaDebitoCliente.BtnBuscarZonaClick(Sender: TObject);
begin
  Buscador(Self,Zona,'codigo','Buscar Zona',nil,DataZona);
  CmbCodigoZona.KeyValue := DataZona.FieldValues['codigo'];
end;

procedure TFrmNotaDebitoCliente.Buscar;
begin
  if (Buscador(Self,BuscadorNotaDebitoCliente,'numero','Buscar Nota de D�bito',nil,DataBuscadorNotaDebitoCliente)) then
  begin
    Clase.SetNumero(DataBuscadorNotaDebitoCliente.FieldValues['numero']);
    Clase.BuscarNumero;
    Asignar;
  end;
end;

procedure TFrmNotaDebitoCliente.CalcularMontosCabecera;
begin
  TxtSubTotal.Value := TxtMontoNeto.Value - TxtMontoDescuento.Value;
  TxtTotal.Value := TxtSubTotal.Value + TxtImpuesto.Value;
end;

procedure TFrmNotaDebitoCliente.Cancelar;
begin
  if (Agregando) then
  begin
    Limpiar;
  end
  else
  begin
    Clase.SetNumero(TxtNumero.Text);
    Clase.BuscarNumero;
    Asignar;
  end;
end;

procedure TFrmNotaDebitoCliente.CmbCodigoClienteChange(Sender: TObject);
begin
  inherited;
  SB.Panels[0].Text := DataCliente.FieldValues['razon_social'];
end;

procedure TFrmNotaDebitoCliente.CmbCodigoClienteExit(Sender: TObject);
begin
  if (Length(Trim(CmbCodigoCliente.Text)) > 0) then
  begin
    if (Agregando) then
    begin
      Cliente.SetCodigo(CmbCodigoCliente.Value);
      Cliente.BuscarCodigo;
      CmbCodigoVendedor.Value := Cliente.GetCodigoVendedor;
      CmbCodigoZona.Value := Cliente.GetCodigoZona;
      CmbCodigoEsquemaPago.Value := Cliente.GetCodigoEsquemaPago;
    end;
  end;
end;

procedure TFrmNotaDebitoCliente.Editar;
begin
  if (Length(Trim(TxtNumero.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
end;

procedure TFrmNotaDebitoCliente.Eliminar;
begin
  //Se Busca La Nota de Debito!
  Clase.SetNumero(TxtNumero.Text);
  Clase.BuscarNumero;
  //Se Eliminan Los Detalles De la Cobranza
  CuentaxCobrar.EliminarIdCobranza(Clase.GetIdCobranza);
  //Se Registra El Documento Anulado y Eliminan Registros del Documento Legal
  DocumentoLegal.SetDesde('NDC');
  DocumentoLegal.SetNumeroDocumento(Clase.GetNumero);
  if (DocumentoLegal.Buscar) then
  begin
    //Registrando El Documento Anulado
    with DocumentoAnulado do
    begin
      SetDesde('NDC');
      SetNumeroDocumento(Clase.GetNumero);
      SetSerie(DocumentoLegal.GetSerie);
      SetNumeroControl(DocumentoLegal.GetNumeroControl);
      SetFechaDocumento(Clase.GetFechaIngreso);
      SetCodigoCliente(Clase.GetCodigoCliente);
      SetCodigoProveedor('');
      SetCodigoZona(Clase.GetCodigoZona);
      SetSubTotal(Clase.GetSubTotal);
      SetImpuesto(Clase.GetImpuesto);
      SetTotal(Clase.GetTotal);
      SetPIva(Clase.GetPIva);
      SetLoginUsuario(P.User.GetLoginUsuario);
      SetFecha(Date);
      SetHora(Time);
      Insertar;
    end;
    //Borrando Documento Legal
    DocumentoLegal.Eliminar;
  end;
  Clase.Eliminar;
  MessageBox(Self.Handle, 'La Nota de D�bito Se Ha Eliminado Con Exito!!!',PChar(MsgTituloInformacion), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  Refrescar;
  Limpiar;
end;

procedure TFrmNotaDebitoCliente.FormCreate(Sender: TObject);
begin
  inherited;
  ProcAgregar := Agregar;
  ProcEditar := Editar;
  ProcEliminar := Eliminar;
  ProcGuardar := Guardar;
  ProcCancelar := Cancelar;
  ProcRefrescar := Refrescar;
  ProcImprimir := Imprimir;
  ProcBuscar := Buscar;
  FrmActividadSophie.Show;
  FrmActividadSophie.Actividad('Cargando N�cleo...');
  Clase := TNotaDebitoCliente.Create;
  //Inicializando DataSets Secundarios
  BuscadorNotaDebitoCliente := TBusquedaNotaDebitoCliente.Create;
  Cliente := TCliente.Create;
  Vendedor := TVendedor.Create;
  Zona := TZona.Create;
  EsquemaPago := TEsquemaPago.Create;
  Moneda := TMoneda.Create;
//  Producto := TProducto.Create;
  Reporte := TReporte.Create;
  Parametro := TParametro.Create;
  DocumentoLegal := TDocumentoLegal.Create;
  DocumentoAnulado := TDocumentoAnulado.Create;
  CuentaxCobrar := TCuentaxCobrar.Create;
  CobranzaCobrada := TCobranzaCobrada.Create;
  FrmActividadSophie.Actividad('Cargando Clientes...');
  DataCliente := Cliente.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Vendedores...');
  DataVendedor := Vendedor.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Zonas...');
  DataZona := Zona.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Esquemas de Pago...');
  DataEsquemaPago := EsquemaPago.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Monedas...');
  DataMoneda := Moneda.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Resto del N�cleo...');
  DataBuscadorNotaDebitoCliente := BuscadorNotaDebitoCliente.ObtenerLista;
  DataCliente.First;
  DataVendedor.First;
  DataZona.First;
  DataEsquemaPago.First;
  DataMoneda.First;
  DataBuscadorNotaDebitoCliente.First;
  DSCliente.DataSet := DataCliente;
  DSVendedor.DataSet := DataVendedor;
  DSZona.DataSet := DataZona;
  DSEsquemaPago.DataSet := DataEsquemaPago;
  DSMoneda.DataSet := DataMoneda;
  ConfigurarRxDBLookupCombo(CmbCodigoCliente,DSCliente,'codigo','codigo;razon_social');
  ConfigurarRxDBLookupCombo(CmbCodigoVendedor,DSVendedor,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoZona,DSZona,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoEsquemaPago,DSEsquemaPago,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoMoneda,DSMoneda,'codigo','codigo;descripcion');
  DS.DataSet := Data;
  GridDetalle.DataSource := DS;
  FrmActividadSophie.Close;
  //Ocultado la pesta�a de Detalles
  TabDetalle.TabVisible := False;
  //Desactivando el ReadOnly
  TxtMontoNeto.ReadOnly := False;
  TxtImpuesto.ReadOnly := False;
end;

procedure TFrmNotaDebitoCliente.Guardar;
var
  SwDocumentoLegal: Boolean;
begin
  CalcularMontosCabecera;
  //Data.EmptyDataSet;
  Obtener;
  if (Agregando) then
  begin
    //Agregando...
    //Guardando Encabezado
    Clase.Insertar;
    TxtNumero.Text := Clase.GetNumero;
    //Solicitando Datos Legales
    FrmDocumentoLegal := TFrmDocumentoLegal.CreateInsert(Self,'NDC',Clase.GetNumero,'','');
    FrmDocumentoLegal.ShowModal;
    if FrmDocumentoLegal.Aceptado then
    begin //Se Guardan Los Datos Legales
      DocumentoLegal.SetDesde(FrmDocumentoLegal.Desde);
      DocumentoLegal.SetNumeroDocumento(Clase.GetNumero);
      DocumentoLegal.SetSerie(FrmDocumentoLegal.Serie);
      DocumentoLegal.SetNumeroControl(FrmDocumentoLegal.NumeroDocumento);
      DocumentoLegal.SetLoginUsuario(P.User.GetLoginUsuario);
      DocumentoLegal.SetFecha(Date);
      DocumentoLegal.SetHora(Time);
      DocumentoLegal.Insertar;
    end;
    FreeAndNil(FrmDocumentoLegal);
    Application.ProcessMessages;
    //Guardando Cuenta x Cobrar
    GuardarCuentaxCobrar;
    MessageBox(Self.Handle, 'La Nota de D�bito Ha Sido Agregada Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    //Se Imprime la Factura
    MessageBox(Self.Handle, 'Presione "Aceptar" Para Proceder A Imprimir La Nota de D�bito de Cliente!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    with Reporte do
    begin
      AgregarParametro('NumeroNotaDebitoCliente',Clase.GetNumero);
      AgregarParametro('MontoLetra',NumeroALetras(Clase.GetTotal));
      begin
        ImprimeReporte('NotaDebitoClienteFormaLibre',trDocumento);
      end;
    end;
  end
  else
  begin
    //Editando...
    Clase.Modificar;
    //Solicitando Datos Legales
    DocumentoLegal.SetDesde('NDC');
    DocumentoLegal.SetNumeroDocumento(Clase.GetNumero);
    SwDocumentoLegal := DocumentoLegal.Buscar;
    if (SwDocumentoLegal) then
    begin
      FrmDocumentoLegal := TFrmDocumentoLegal.CreateEdit(Self,'NDC',Clase.GetNumero,DocumentoLegal.GetSerie,DocumentoLegal.GetNumeroControl);
    end
    else
    begin
      FrmDocumentoLegal := TFrmDocumentoLegal.CreateInsert(Self,'NDC',Clase.GetNumero,'','');
    end;
    FrmDocumentoLegal.ShowModal;
    if FrmDocumentoLegal.Aceptado then
    begin //Se Guardan Los Datos Legales
      DocumentoLegal.SetSerie(FrmDocumentoLegal.Serie);
      DocumentoLegal.SetNumeroControl(FrmDocumentoLegal.NumeroControl);
      DocumentoLegal.SetLoginUsuario(P.User.GetLoginUsuario);
      DocumentoLegal.SetFecha(Date);
      DocumentoLegal.SetHora(Time);
      if (SwDocumentoLegal) then
      begin
        DocumentoLegal.Modificar;
      end
      else
      begin
        DocumentoLegal.Insertar;
      end;
    end;
    FreeAndNil(FrmDocumentoLegal);
    //Guardando Cuenta x Cobrar
    GuardarCuentaxCobrar;
    MessageBox(Self.Handle, 'La Nota de D�bito de Cliente Ha Sido Editada Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end;
  PageDetalles.ActivePage := TabConcepto;
end;

procedure TFrmNotaDebitoCliente.GuardarCuentaxCobrar;
begin
  if (Editando) then
  begin
    CuentaxCobrar.EliminarIdCobranza(Clase.GetIdCobranza);
  end;
  with CuentaxCobrar do //Se Inserta la CuentaxCobrar
  begin
    SetDesde('NDC');
    SetNumeroDocumento(Clase.GetNumero);
    SetCodigoMoneda(Clase.GetCodigoMoneda);
    SetCodigoCliente(Clase.GetCodigoCliente);
    SetCodigoVendedor(Clase.GetCodigoVendedor);
    SetCodigoZona(Clase.GetCodigoZona);
    SetFechaIngreso(Clase.GetFechaIngreso);
    SetFechaVencIngreso(Clase.GetFechaIngreso);
    SetFechaRecepcion(Clase.GetFechaRecepcion);
    SetFechaVencRecepcion(Clase.GetFechaRecepcion);
    SetFechaContable(Clase.GetFechaContable);
    SetFechaCobranza(Clase.GetFechaIngreso);
    SetFechaLibros(Clase.GetFechaLibros);
    SetNumeroCuota(-1);
    SetTotalCuotas(-1);
    SetOrdenCobranza(1);
    SetMontoNeto(Clase.GetMontoNeto);
    SetImpuesto(Clase.GetImpuesto);
    SetTotal(Clase.GetTotal);
    SetPIva(Clase.GetPIva);
    SetIdCobranza(Clase.GetIdCobranza);
    SetIdContadoCobranza(Clase.GetIdContadoCobranza);
    Insertar;
  end;
end;

procedure TFrmNotaDebitoCliente.Imprimir;
begin
  if (Length(Trim(TxtNumero.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
  with Reporte do
  begin
    AgregarParametro('NumeroNotaDebitoCliente',Clase.GetNumero);
    AgregarParametro('MontoLetra',NumeroALetras(Clase.GetTotal));
    begin
      EmiteReporte('NotaDebitoClienteFormaLibre',trDocumento);
    end
  end;
end;

procedure TFrmNotaDebitoCliente.LblImpuestoDblClick(Sender: TObject);
var
  Aux: Currency;
begin
  Aux := 0;
  Solicitar(Self,'Impuesto',TxtImpuesto.Value,Aux);
  if (Aux >= 0) then
  begin
    TxtImpuesto.Value := Aux;
    CalcularMontosCabecera;
  end;
end;

procedure TFrmNotaDebitoCliente.LblMontoDescuentoDblClick(Sender: TObject);
var
  Aux: Currency;
begin
  Aux := 0;
  Solicitar(Self,'Monto Descuento',TxtMontoDescuento.Value,Aux);
  if (Aux >= 0) then
  begin
    TxtMontoDescuento.Value := Aux;
    TxtPDescuento.Caption := FormatFloat('#,##0.0000%',Aux * 100 / TxtMontoNeto.Value);
    CalcularMontosCabecera;
  end;
end;

procedure TFrmNotaDebitoCliente.Limpiar;
begin
  TxtNumero.Text := '';
  CmbCodigoCliente.KeyValue := '';
  CmbCodigoVendedor.KeyValue := '';
  CmbCodigoZona.KeyValue := '';
  CmbCodigoEsquemaPago.KeyValue := '';
  CmbCodigoMoneda.KeyValue := '';
  DatFechaIngreso.Date := Date;
  DatFechaRecepcion.Date := Date;
  DatFechaContable.Date := Date;
  DatFechaLibros.Date := Date;
  TxtConcepto.Text := '';
  TxtObservaciones.Text := '';
  TxtMontoNeto.Value := 0;
  TxtMontoDescuento.Value := 0;
  TxtSubTotal.Value := 0;
  TxtImpuesto.Value := 0;
  TxtTotal.Value := 0;
  TxtPDescuento.Caption := FormatFloat('###0.0000%',0);
  TxtPIVA.Caption := FormatFloat('###0.0000%',0);
  //Status Bar
  SB.Panels[0].Text := '';
  //Data.EmptyDataSet;
  PageDetalles.ActivePage := TabConcepto;
end;

procedure TFrmNotaDebitoCliente.Mover;
begin

end;

procedure TFrmNotaDebitoCliente.Obtener;
begin
  with Clase do
  begin
    SetNumero(TxtNumero.Text);
    SetCodigoCliente(CmbCodigoCliente.KeyValue);
    SetCodigoVendedor(CmbCodigoVendedor.KeyValue);
    SetCodigoZona(CmbCodigoZona.KeyValue);
    SetCodigoEsquemaPago(CmbCodigoEsquemaPago.KeyValue);
    SetCodigoMoneda(CmbCodigoMoneda.KeyValue);
    SetFechaIngreso(DatFechaIngreso.Date);
    SetFechaRecepcion(DatFechaRecepcion.Date);
    SetFechaContable(DatFechaContable.Date);
    SetFechaLibros(DatFechaLibros.Date);
    SetConcepto(TxtConcepto.Text);
    SetObservaciones(TxtObservaciones.Text);
    SetMontoNeto(TxtMontoNeto.Value);
    SetMontoDescuento(TxtMontoDescuento.Value);
    SetSubTotal(TxtSubTotal.Value);
    SetImpuesto(TxtImpuesto.Value);
    SetTotal(TxtTotal.Value);
    SetPDescuento(FormatearFloat(StringReplace(TxtPDescuento.Caption,'%','',[rfReplaceAll])));
    SetPIva(FormatearFloat(StringReplace(TxtPIVA.Caption,'%','',[rfReplaceAll])));
    SetTasaDolar(0); //***//
    SetLoginUsuario(P.User.GetLoginUsuario);
    if (Agregando) then
    begin
      SetTiempoIngreso(Now);
      SetIdNotaDebitoCliente(-1);
      SetIdCobranza(-1);
      SetIdContadoCobranza(-1);
    end;
  end;
end;

procedure TFrmNotaDebitoCliente.Refrescar;
begin
  //Se Actualizan Los demas Componentes
  DataCliente := Cliente.ObtenerCombo;
  DataVendedor := Vendedor.ObtenerCombo;
  DataZona := Zona.ObtenerCombo;
  DataEsquemaPago := EsquemaPago.ObtenerCombo;
  DataMoneda := Moneda.ObtenerCombo;
  DataBuscadorNotaDebitoCliente := BuscadorNotaDebitoCliente.ObtenerLista;
  DataCliente.First;
  DataVendedor.First;
  DataZona.First;
  DataEsquemaPago.First;
  DataMoneda.First;
  DataBuscadorNotaDebitoCliente.First;
  DSCliente.DataSet := DataCliente;
  DSVendedor.DataSet := DataVendedor;
  DSZona.DataSet := DataZona;
  DSEsquemaPago.DataSet := DataEsquemaPago;
  DSMoneda.DataSet := DataMoneda;
end;

procedure TFrmNotaDebitoCliente.TBtnEliminarClick(Sender: TObject);
begin
  if (Length(Trim(TxtNumero.Text)) > 0) then
  begin
    inherited;
  end
  else
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
end;

procedure TFrmNotaDebitoCliente.TxtMontoNetoExit(Sender: TObject);
begin
  CalcularMontosCabecera;
end;

procedure TFrmNotaDebitoCliente.TxtPDescuentoDblClick(Sender: TObject);
var
  Aux: Currency;
begin
  Aux := 0;
  Solicitar(Self,'(%) Descuento',StrToFloat(StringReplace(TxtPDescuento.Caption,'%','',[rfReplaceAll])),Aux);
  if (Aux >= 0) then
  begin
    TxtPDescuento.Caption := FormatFloat('#,##0.0000%',Aux);
    TxtMontoDescuento.Value := Aux * TxtMontoNeto.Value / 100;
    CalcularMontosCabecera;
  end;
end;

procedure TFrmNotaDebitoCliente.TxtPIVADblClick(Sender: TObject);
var
  Aux: Currency;
begin
  Aux := 0;
  Solicitar(Self,'(%) I.V.A.',StrToFloat(StringReplace(TxtPIVA.Caption,'%','',[rfReplaceAll])),Aux);
  if (Aux >= 0) then
  begin
    TxtPIVA.Caption := FormatFloat('#,##0.0000%',Aux);
    TxtImpuesto.Value := Aux * TxtMontoNeto.Value / 100;
    CalcularMontosCabecera;
  end;
end;

function TFrmNotaDebitoCliente.Validar: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  //Validando Cliente
  if (Length(Trim(CmbCodigoCliente.Text)) = 0) then
  begin
    ListaErrores.Add('El Cliente No Puede Estar En Blanco!!!');
  end;
  //Validando Vendedor
  if (Length(Trim(CmbCodigoVendedor.Text)) = 0) then
  begin
    ListaErrores.Add('El Vendedor No Puede Estar En Blanco!!!');
  end;
  //Validando Monto
  if (TxtTotal.Value <= 0) then
  begin
    ListaErrores.Add('El Total Debe Ser Positivo!!!');
  end;
  //Validando Fechas
  if ((DatFechaIngreso.Date > DatFechaRecepcion.Date) or (DatFechaIngreso.Date > DatFechaContable.Date) or (DatFechaIngreso.Date > DatFechaLibros.Date)) then
  begin
    ListaErrores.Add('La Fecha de Ingreso Debe Ser Menor a las Fechas de Recepcion, Contable y Libros Respectivamente!!!');
  end;
  //Validando Detalle o Forma Libre
  if (Length(Trim(TxtConcepto.Text)) = 0) then
  begin
    ListaErrores.Add('El Concepto No Puede Estar En Blanco!!!');
  end;
  if (TxtMontoNeto.Value <= 0) then
  begin
    ListaErrores.Add('El Monto Neto Debe Ser Positivo!!!');
  end;
  if (TxtMontoDescuento.Value < 0) then
  begin
    ListaErrores.Add('El Monto de Descuento No Debe Ser Negativo!!!');
  end;
  if (TxtImpuesto.Value < 0) then
  begin
    ListaErrores.Add('El Impuesto No Debe Ser Negativo!!!');
  end;
  //Validando Monto de Impuesto
  if (TxtImpuesto.Value = 0) then
  begin
    if (MessageBox(Self.Handle, '�El Valor del Impuesto esta en 0 Aun Asi Desea Continuar?', PChar(MsgTituloInformacion), MB_ICONWARNING or MB_YESNO or MB_TOPMOST or MB_TASKMODAL) = mrNo) then
    begin
      TxtImpuesto.SetFocus;
      Abort;
    end
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
