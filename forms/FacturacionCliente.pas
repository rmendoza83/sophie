unit FacturacionCliente;

{$I sophie.inc}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GenericoDetalle, DB, ImgList, XPMan, Grids, DBGrids, ComCtrls,
  ExtCtrls, ToolWin, StdCtrls, rxCurrEdit, Mask, rxToolEdit, RxLookup, Buttons,
  DBClient, Utils, ClsFacturacionCliente, ClsCliente, ClsVendedor, ClsEstacion,
  ClsZona, ClsEsquemaPago, ClsMoneda, ClsDetFacturacionCliente, ClsProducto,
  ClsBusquedaFacturacionCliente, ClsDetMovimientoInventario, ClsCuentaxCobrar,
  GenericoBuscador, ClsReporte, ClsParametro, ClsCobranzaCobrada,
  ClsDocumentoLegal, ClsDocumentoAnulado, ClsDetalleContadoCobranza, Menus
  {$IFDEF DELPHI10}, ImageList{$ENDIF};

type
  TFrmFacturacionCliente = class(TFrmGenericoDetalle)
    LblNumero: TLabel;
    LblCodigoCliente: TLabel;
    LblCodigoVendedor: TLabel;
    LblCodigoEstacion: TLabel;
    LblCodigoZona: TLabel;
    LblEsquemaPago: TLabel;
    LblMoneda: TLabel;
    TxtNumero: TEdit;
    CmbCodigoCliente: TRxDBLookupCombo;
    DatFechaIngreso: TDateEdit;
    TxtMontoNeto: TCurrencyEdit;
    CmbCodigoVendedor: TRxDBLookupCombo;
    CmbCodigoEstacion: TRxDBLookupCombo;
    DatFechaRecepcion: TDateEdit;
    DatFechaContable: TDateEdit;
    DatFechaLibros: TDateEdit;
    CmbCodigoZona: TRxDBLookupCombo;
    CmbCodigoEsquemaPago: TRxDBLookupCombo;
    CmbCodigoMoneda: TRxDBLookupCombo;
    LblFechaIngreso: TLabel;
    LblRecepcion: TLabel;
    LblFechaContable: TLabel;
    LblFechaLibros: TLabel;
    TxtMontoDescuento: TCurrencyEdit;
    TxtSubTotal: TCurrencyEdit;
    TxtImpuesto: TCurrencyEdit;
    TxtTotal: TCurrencyEdit;
    LblMontoNeto: TLabel;
    LblMontoDescuento: TLabel;
    LblSubTotal: TLabel;
    LblImpuesto: TLabel;
    LblTotal: TLabel;
    TabConcepto: TTabSheet;
    TabObservaciones: TTabSheet;
    DSCliente: TDataSource;
    DSVendedor: TDataSource;
    DSEstacion: TDataSource;
    DSZona: TDataSource;
    DSEsquemaPago: TDataSource;
    DSMoneda: TDataSource;
    BtnBuscarCliente: TBitBtn;
    BtnBuscarVendedor: TBitBtn;
    BtnBuscarEstacion: TBitBtn;
    BtnBuscarZona: TBitBtn;
    BtnBuscarEsquemaPago: TBitBtn;
    BtnBuscarMoneda: TBitBtn;
    TxtPDescuento: TLabel;
    TxtPIVA: TLabel;
    TxtObservaciones: TRichEdit;
    TxtConcepto: TRichEdit;
    Datacodigo_producto: TStringField;
    Datareferencia: TStringField;
    Datadescripcion: TStringField;
    Datacantidad: TFloatField;
    Dataprecio: TFloatField;
    Datap_descuento: TFloatField;
    Datamonto_descuento: TFloatField;
    Dataimpuesto: TFloatField;
    Datap_iva: TFloatField;
    Datatotal: TFloatField;
    Dataid_facturacion_cliente: TIntegerField;
    TBtnS4: TToolButton;
    TBtnImportar: TToolButton;
    PMImportar: TPopupMenu;
    PMIN1: TMenuItem;
    PMIPedido: TMenuItem;
    PMITallerServicio: TMenuItem;
    procedure CmbCodigoClienteExit(Sender: TObject);
    procedure TxtMontoNetoExit(Sender: TObject);
    procedure TxtPIVADblClick(Sender: TObject);
    procedure TxtPDescuentoDblClick(Sender: TObject);
    procedure LblMontoDescuentoDblClick(Sender: TObject);
    procedure LblMontoNetoDblClick(Sender: TObject);
    procedure CmbCodigoClienteChange(Sender: TObject);
    procedure LblImpuestoDblClick(Sender: TObject);
    procedure DataAfterPost(DataSet: TDataSet);
    procedure TBtnEliminarClick(Sender: TObject);
    procedure BtnBuscarMonedaClick(Sender: TObject);
    procedure BtnBuscarEsquemaPagoClick(Sender: TObject);
    procedure BtnBuscarZonaClick(Sender: TObject);
    procedure BtnBuscarEstacionClick(Sender: TObject);
    procedure BtnBuscarVendedorClick(Sender: TObject);
    procedure BtnBuscarClienteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PMITallerServicioClick(Sender: TObject);
  private
    { Private declarations }
    Clase: TFacturacionCliente;
    DetFacturacionCliente: TDetFacturacionCliente;
    MovimientoInventario: TDetMovimientoInventario;
    CuentaxCobrar: TCuentaxCobrar;
    CobranzaCobrada: TCobranzaCobrada;
    BuscadorFacturacionCliente: TBusquedaFacturacionCliente;
    Cliente: TCliente;
    Vendedor: TVendedor;
    Estacion: TEstacion;
    Zona: TZona;
    EsquemaPago: TEsquemaPago;
    Moneda: TMoneda;
    Producto: TProducto;
    Reporte: TReporte;
    Parametro: TParametro;
    DocumentoLegal: TDocumentoLegal;
    DocumentoAnulado: TDocumentoAnulado;
    DetalleContadoCobranza: TDetalleContadoCobranza;
    DataBuscadorFacturacionCliente: TClientDataSet;
    DataCliente: TClientDataSet;
    DataVendedor: TClientDataSet;
    DataEstacion: TClientDataSet;
    DataZona: TClientDataSet;
    DataEsquemaPago: TClientDataSet;
    DataMoneda: TClientDataSet;
    SwImportado: Boolean;
    SwTallerServicio: Boolean;
    NumeroTallerServicio: string;
    procedure Agregar;
    procedure Editar;
    procedure Eliminar;
    procedure Guardar;
    procedure Cancelar;
    procedure Refrescar;
    procedure Imprimir;
    procedure Buscar;
    procedure Limpiar;
    procedure Asignar;
    procedure Obtener;
    procedure Mover;
    procedure GuardarDetalles;
    procedure GuardarCobranzaCobrada;
    procedure GuardarCuentaxCobrar;
    procedure GuardarMovimientoInventario;
    procedure CalcularMontos;
    procedure CalcularMontosCabecera;
  protected
    { Protected declarations }
    function Validar: Boolean; override;
    procedure Detalle; override;
  public
    { Public declarations }
  end;

var
  FrmFacturacionCliente: TFrmFacturacionCliente;

implementation

uses
  ActividadSophie,
  PModule,
  AgregarDetalleFacturacionCliente,
  DetalleContadoCobranza,
  SolicitarMonto,
  DocumentoLegal,
  ClsTallerBusquedaServicio,
  ClsTallerServicio,
  ClsTallerDetServicio
  {$IFDEF USE_RAVE_REPORTS}
  , DMRR
  {$ENDIF}
  {$IFDEF USE_FAST_REPORTS}
  , DMFR
  {$ENDIF}
  ;

{$R *.dfm}

procedure TFrmFacturacionCliente.Agregar;
begin
  //Verificando
  Limpiar;
  //Asignando Valores por Defecto
  TxtPIVA.Caption := FormatFloat('#,##0.0000%',Parametro.ObtenerValor('p_iva'));
  Clase.SetPIva(Parametro.ObtenerValor('p_iva'));
  CmbCodigoEstacion.Value := Parametro.ObtenerValor('codigo_estacion_salida');
  CmbCodigoZona.Value := Parametro.ObtenerValor('codigo_zona');
  CmbCodigoEsquemaPago.Value := Parametro.ObtenerValor('codigo_esquema_pago');
  CmbCodigoMoneda.Value := Parametro.ObtenerValor('codigo_moneda');
  CmbCodigoCliente.SetFocus;
  TBtnImportar.Enabled := True;
end;

procedure TFrmFacturacionCliente.Asignar;
begin
  if (Clase.GetFormaLibre) then
  begin
    TabDetalle.TabVisible := False;
    PageDetalles.ActivePage := TabConcepto;
    TxtMontoNeto.ReadOnly := False;
  end
  else
  begin
    TabDetalle.TabVisible := True;
    PageDetalles.ActivePage := TabDetalle;
    TxtMontoNeto.ReadOnly := True;
  end;
  with Clase do
  begin
    TxtNumero.Text := GetNumero;
    //NumeroPedido
    CmbCodigoCliente.KeyValue := GetCodigoCliente;
    CmbCodigoVendedor.KeyValue := GetCodigoVendedor;
    CmbCodigoEstacion.KeyValue := GetCodigoEstacion;
    CmbCodigoZona.KeyValue := GetCodigoZona;
    CmbCodigoEsquemaPago.KeyValue := GetCodigoEsquemaPago;
    CmbCodigoMoneda.KeyValue := GetCodigoMoneda;
    DatFechaIngreso.Date := GetFechaIngreso;
    DatFechaRecepcion.Date := GetFechaRecepcion;
    DatFechaContable.Date := GetFechaContable;
    DatFechaLibros.Date := GetFechaLibros;
    TxtConcepto.Text := GetConcepto;
    TxtObservaciones.Text := GetObservaciones;
    TxtMontoNeto.Value := GetMontoNeto;
    TxtMontoDescuento.Value := GetMontoDescuento;
    TxtSubTotal.Value := GetSubTotal;
    TxtImpuesto.Value := GetImpuesto;
    TxtTotal.Value := GetTotal;
    TxtPDescuento.Caption := FormatFloat('###0.0000%',GetPDescuento);
    TxtPIVA.Caption := FormatFloat('###0.0000%',GetPIva);
    Data.CloneCursor(DetFacturacionCliente.ObtenerIdFacturacionCliente(GetIdFacturacionCliente),False,True);
    DS.DataSet := Data;
  end;
end;

procedure TFrmFacturacionCliente.BtnBuscarClienteClick(Sender: TObject);
begin
  Buscador(Self,Cliente,'codigo','Buscar Cliente',nil,DataCliente);
  CmbCodigoCliente.KeyValue := DataCliente.FieldValues['codigo'];
  CmbCodigoClienteExit(Sender);
end;

procedure TFrmFacturacionCliente.BtnBuscarEsquemaPagoClick(Sender: TObject);
begin
  Buscador(Self,EsquemaPago,'codigo','Buscar Esquema de Pago',nil,DataEsquemaPago);
  CmbCodigoEsquemaPago.KeyValue := DataEsquemaPago.FieldValues['codigo'];
end;

procedure TFrmFacturacionCliente.BtnBuscarEstacionClick(Sender: TObject);
begin
  Buscador(Self,Estacion,'codigo','Buscar Estacion',nil,DataEstacion);
  CmbCodigoEstacion.KeyValue := DataEstacion.FieldValues['codigo'];
end;

procedure TFrmFacturacionCliente.BtnBuscarMonedaClick(Sender: TObject);
begin
  Buscador(Self,Moneda,'codigo','Buscar Moneda',nil,DataMoneda);
  CmbCodigoMoneda.KeyValue := DataMoneda.FieldValues['codigo'];
end;

procedure TFrmFacturacionCliente.BtnBuscarVendedorClick(Sender: TObject);
begin
  Buscador(Self,Vendedor,'codigo','Buscar Vendedor',nil,DataVendedor);
  CmbCodigoVendedor.KeyValue := DataVendedor.FieldValues['codigo'];
end;

procedure TFrmFacturacionCliente.BtnBuscarZonaClick(Sender: TObject);
begin
  Buscador(Self,Zona,'codigo','Buscar Zona',nil,DataZona);
  CmbCodigoZona.KeyValue := DataZona.FieldValues['codigo'];
end;

procedure TFrmFacturacionCliente.Buscar;
begin
  if (Buscador(Self,BuscadorFacturacionCliente,'numero','Buscar Factura',nil,DataBuscadorFacturacionCliente)) then
  begin
    Clase.SetNumero(DataBuscadorFacturacionCliente.FieldValues['numero']);
    Clase.BuscarNumero;
    Asignar;
  end;
end;

procedure TFrmFacturacionCliente.CalcularMontos;
var
  Neto, Descuento, Impuesto: Currency;
begin
  Neto := 0;
  Descuento := 0;
  Impuesto := 0;
  with Data do
  begin
    DisableControls;
    First;
    while (not Eof) do
    begin
      Neto := Neto + (FieldValues['precio'] * FieldValues['cantidad']);
      Descuento := Descuento + FieldValues['monto_descuento'];
      Impuesto := Impuesto + FieldValues['impuesto'];
      Next;
    end;
    EnableControls;
  end;
  //Asignando Montos
  TxtMontoNeto.Value := Neto;
  //Determinando si hay un Descuento Manual
  if ((TxtMontoDescuento.Value > 0) and (StrToFloat(StringReplace(TxtPDescuento.Caption,'%','',[rfReplaceAll])) > 0) and (Descuento = 0)) then
  begin
    Descuento := TxtMontoDescuento.Value;
  end;
  TxtMontoDescuento.Value := Descuento;
  TxtSubTotal.Value := Neto - Descuento;
  TxtImpuesto.Value := Impuesto;
  TxtTotal.Value := (Neto - Descuento) + Impuesto;
end;

procedure TFrmFacturacionCliente.CalcularMontosCabecera;
begin
  if (Clase.GetFormaLibre) then
  begin
    TxtSubTotal.Value := TxtMontoNeto.Value - TxtMontoDescuento.Value;
    TxtTotal.Value := TxtSubTotal.Value + TxtImpuesto.Value;
  end;
end;

procedure TFrmFacturacionCliente.Cancelar;
begin
  if (Agregando) then
  begin
    Limpiar;
    TBtnImportar.Enabled := False;
  end
  else
  begin
    Clase.SetNumero(TxtNumero.Text);
    Clase.BuscarNumero;
    Asignar;
  end;
end;

procedure TFrmFacturacionCliente.CmbCodigoClienteChange(Sender: TObject);
begin
  SB.Panels[0].Text := DataCliente.FieldValues['razon_social'];
end;

procedure TFrmFacturacionCliente.CmbCodigoClienteExit(Sender: TObject);
begin
  if (Length(Trim(CmbCodigoCliente.Text)) > 0) then
  begin
    if (Agregando) then
    begin
      Cliente.SetCodigo(CmbCodigoCliente.Value);
      Cliente.BuscarCodigo;
      CmbCodigoVendedor.Value := Cliente.GetCodigoVendedor;
      CmbCodigoZona.Value := Cliente.GetCodigoZona;
      CmbCodigoEsquemaPago.Value := Cliente.GetCodigoEsquemaPago;
    end;
  end;
end;

procedure TFrmFacturacionCliente.Editar;
var
  sw: Boolean;
begin
  if (Length(Trim(TxtNumero.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end
  else
  begin
    //Se Valida Que La Factura Pueda Ser Editada
    sw := True;
    EsquemaPago.SetCodigo(Clase.GetCodigoEsquemaPago);
    EsquemaPago.Buscar;
    if ((Clase.GetIdContadoCobranza = -1) and (EsquemaPago.GetCredito)) then //Puede Editarse
    begin
      CobranzaCobrada.SetIdCobranza(Clase.GetIdCobranza);
      if (CobranzaCobrada.BuscarIdCobranza) then
      begin
        sw := False;
      end;
    end;
    if not (sw) then
    begin
      MessageBox(Self.Handle, 'La Factura Presenta Liquidaciones Asociadas y No Puede Ser Editada!!!',PChar(MsgTituloAdvertencia), MB_ICONEXCLAMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      Abort;
    end;
  end;
end;

procedure TFrmFacturacionCliente.Eliminar;
var
  sw: Boolean;
  TallerServicio: TTallerServicio;
begin
  //Verificando si el Modulo de Radio esta activado
  Parametro.SetNombreParametro('modulo_radio_activado');
  if (Parametro.Buscar) then
  begin
    if (Boolean(Parametro.ObtenerValor('modulo_radio_activado'))) then
    begin
      MessageBox(Self.Handle,'Las Facturas No Pueden Ser Eliminadas!!! '+#13+#10+'Para ello utilice el Modulo de Liquidaci�n de Contratos!!!'+#13+#10+''+#13+#10+'Para M�s Informaci�n Consulte Con Servicio T�cnico!!!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      Abort;
    end;
  end;
  //Se Valida Que La Factura Pueda Ser Anulada
  sw := True;
  EsquemaPago.SetCodigo(Clase.GetCodigoEsquemaPago);
  EsquemaPago.Buscar;
  if ((Clase.GetIdContadoCobranza = -1) and (EsquemaPago.GetCredito)) then //Puede Anularse
  begin
    CobranzaCobrada.SetIdCobranza(Clase.GetIdCobranza);
    if (CobranzaCobrada.BuscarIdCobranza) then
    begin
      sw := False;
    end;
  end;
  if (sw) then
  begin
    //Se Busca La Factura!
    Clase.SetNumero(TxtNumero.Text);
    Clase.BuscarNumero;
    //Se Eliminan Los Detalles
    DetFacturacionCliente.EliminarIdFacturacionCliente(Clase.GetIdFacturacionCliente);
    //Se Eliminan Los Movimientos de Inventario
    MovimientoInventario.EliminarIdMovimientoInventario(Clase.GetIdMovimientoInventario);
    //Se Eliminan Los Detalles De la Cobranza
    CuentaxCobrar.EliminarIdCobranza(Clase.GetIdCobranza);
    //Se Registra El Documento Anulado y Eliminan Registros del Documento Legal
    DocumentoLegal.SetDesde('FC');
    DocumentoLegal.SetNumeroDocumento(Clase.GetNumero);
    if (DocumentoLegal.Buscar) then
    begin
      //Registrando El Documento Anulado
      with DocumentoAnulado do
      begin
        SetDesde('FC');
        SetNumeroDocumento(Clase.GetNumero);
        SetSerie(DocumentoLegal.GetSerie);
        SetNumeroControl(DocumentoLegal.GetNumeroControl);
        SetFechaDocumento(Clase.GetFechaIngreso);
        SetCodigoCliente(Clase.GetCodigoCliente);
        SetCodigoProveedor('');
        SetCodigoZona(Clase.GetCodigoZona);
        SetSubTotal(Clase.GetSubTotal);
        SetImpuesto(Clase.GetImpuesto);
        SetTotal(Clase.GetTotal);
        SetPIva(Clase.GetPIva);
        SetLoginUsuario(P.User.GetLoginUsuario);
        SetFecha(Date);
        SetHora(Time);
        Insertar;
      end;
      //Borrando Documento Legal
      DocumentoLegal.Eliminar;
    end;
    Clase.Eliminar;
    //Verificando Si Existen Servicios de Taller Asociados
    TallerServicio := TTallerServicio.Create;
    TallerServicio.SetIdFacturacionCliente(Clase.GetIdFacturacionCliente);
    if (TallerServicio.BuscarIdFacturacionCliente) then
    begin
      TallerServicio.SetIdFacturacionCliente(-1);
      TallerServicio.Modificar;
    end;
    TallerServicio.Destroy;
    MessageBox(Self.Handle, 'La Factura Se Ha Eliminado Con Exito!!!',PChar(MsgTituloInformacion), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Refrescar;
    Limpiar;
  end
  else
  begin
    MessageBox(Self.Handle, 'La Factura Presenta Liquidaciones Asociadas y No Puede Ser Eliminada!!!',PChar(MsgTituloAdvertencia), MB_ICONEXCLAMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end;
end;

procedure TFrmFacturacionCliente.FormCreate(Sender: TObject);
begin
  inherited;
  ProcAgregar := Agregar;
  ProcEditar := Editar;
  ProcEliminar := Eliminar;
  ProcGuardar := Guardar;
  ProcCancelar := Cancelar;
  ProcRefrescar := Refrescar;
  ProcImprimir := Imprimir;
  ProcBuscar := Buscar;
  FrmActividadSophie.Show;
  FrmActividadSophie.Actividad('Cargando N�cleo...');
  Clase := TFacturacionCliente.Create;
  //Inicializando DataSets Secundarios
  BuscadorFacturacionCliente := TBusquedaFacturacionCliente.Create;
  Cliente := TCliente.Create;
  Vendedor := TVendedor.Create;
  Estacion := TEstacion.Create;
  Zona := TZona.Create;
  EsquemaPago := TEsquemaPago.Create;
  Moneda := TMoneda.Create;
  Producto := TProducto.Create;
  Reporte := TReporte.Create;
  Parametro := TParametro.Create;
  DocumentoLegal := TDocumentoLegal.Create;
  DocumentoAnulado := TDocumentoAnulado.Create;
  DetFacturacionCliente := TDetFacturacionCliente.Create;
  MovimientoInventario := TDetMovimientoInventario.Create;
  CuentaxCobrar := TCuentaxCobrar.Create;
  CobranzaCobrada := TCobranzaCobrada.Create;
  DetalleContadoCobranza := TDetalleContadoCobranza.Create;
  FrmActividadSophie.Actividad('Cargando Clientes...');
  DataCliente := Cliente.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Vendedores...');
  DataVendedor := Vendedor.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Estaciones...');
  DataEstacion := Estacion.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Zonas...');
  DataZona := Zona.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Esquemas de Pago...');
  DataEsquemaPago := EsquemaPago.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Monedas...');
  DataMoneda := Moneda.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Resto del N�cleo...');
  DataBuscadorFacturacionCliente := BuscadorFacturacionCliente.ObtenerLista;
  DataCliente.First;
  DataVendedor.First;
  DataEstacion.First;
  DataZona.First;
  DataEsquemaPago.First;
  DataMoneda.First;
  DataBuscadorFacturacionCliente.First;
  DSCliente.DataSet := DataCliente;
  DSVendedor.DataSet := DataVendedor;
  DSEstacion.DataSet := DataEstacion;
  DSZona.DataSet := DataZona;
  DSEsquemaPago.DataSet := DataEsquemaPago;
  DSMoneda.DataSet := DataMoneda;
  ConfigurarRxDBLookupCombo(CmbCodigoCliente,DSCliente,'codigo','codigo;razon_social');
  ConfigurarRxDBLookupCombo(CmbCodigoVendedor,DSVendedor,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoEstacion,DSEstacion,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoZona,DSZona,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoEsquemaPago,DSEsquemaPago,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoMoneda,DSMoneda,'codigo','codigo;descripcion');
  DS.DataSet := Data;
  GridDetalle.DataSource := DS;
  FrmActividadSophie.Close;
  TBtnImportar.Enabled := False;
  //Activando Importar
  Parametro.SetNombreParametro('modulo_taller_activado');
  if (Parametro.Buscar) then
  begin
    if (Parametro.ObtenerValorAsBoolean) then
    begin
      TBtnS4.Visible := True;
      TBtnImportar.Visible := True;
    end;
  end;
  //Buscar;
end;

procedure TFrmFacturacionCliente.Guardar;
var
  SwDocumentoLegal: Boolean;
  TallerServicio: TTallerServicio;
begin
  if (Clase.GetFormaLibre) then
  begin
    CalcularMontosCabecera;
    Data.EmptyDataSet;
  end
  else
  begin
    CalcularMontos;
  end;
  Obtener;
  //Caso de Esquema de Pago de Contado
  EsquemaPago.SetCodigo(Clase.GetCodigoEsquemaPago);
  EsquemaPago.Buscar;
  if (not EsquemaPago.GetCredito) then
  begin
    FrmDetalleContadoCobranza := TFrmDetalleContadoCobranza.Crear(Self,Agregando,Editando,Clase.GetIdContadoCobranza,Clase.GetTotal,'FC',Clase.GetCodigoCliente);
    FrmDetalleContadoCobranza.ShowModal;
    if (not FrmDetalleContadoCobranza.Aceptado) then
    begin
      FreeAndNil(FrmDetalleContadoCobranza);
      Abort;
    end;
    Clase.SetIdContadoCobranza(FrmDetalleContadoCobranza.GetIdContadoCobranza);
    FreeAndNil(FrmDetalleContadoCobranza);
  end
  else //Caso de Esquema de Pago a Credito
  begin
    if (EsquemaPago.GetInicial) then //Caso de Creditos con Inicial
    begin
      FrmDetalleContadoCobranza := TFrmDetalleContadoCobranza.Crear(Self,Agregando,Editando,Clase.GetIdContadoCobranza,(Clase.GetTotal * (EsquemaPago.GetPInicial / 100)) + Clase.GetImpuesto,'FC',Clase.GetCodigoCliente);
      FrmDetalleContadoCobranza.ShowModal;
      if (not FrmDetalleContadoCobranza.Aceptado) then
      begin
        FreeAndNil(FrmDetalleContadoCobranza);
        Abort;
      end;
      Clase.SetIdContadoCobranza(FrmDetalleContadoCobranza.GetIdContadoCobranza);
      FreeAndNil(FrmDetalleContadoCobranza);
    end;
    //Si Proviene de un Esquema de Pago de Contado
    if (Clase.GetIdContadoCobranza > 0) then
    begin
      //Se Eliminan Los Registros de Detalle Contado Cobranza
      DetalleContadoCobranza.EliminarIdContadoCobranza(Clase.GetIdContadoCobranza);
      //Se Eliminan Los Registros de Cobranza Cobrada
      CobranzaCobrada.EliminarIdCobranza(Clase.GetIdCobranza);
      Clase.SetIdContadoCobranza(-1);
    end;
  end;
  if (Agregando) then
  begin
    //Agregando...
    //Guardando Encabezado
    Clase.Insertar;
    TxtNumero.Text := Clase.GetNumero;
    //Solicitando Datos Legales
    FrmDocumentoLegal := TFrmDocumentoLegal.CreateInsert(Self,'FC',Clase.GetNumero,'','');
    FrmDocumentoLegal.ShowModal;
    if FrmDocumentoLegal.Aceptado then
    begin //Se Guardan Los Datos Legales
      DocumentoLegal.SetDesde(FrmDocumentoLegal.Desde);
      DocumentoLegal.SetNumeroDocumento(Clase.GetNumero);
      DocumentoLegal.SetSerie(FrmDocumentoLegal.Serie);
      DocumentoLegal.SetNumeroControl(FrmDocumentoLegal.NumeroControl);
      DocumentoLegal.SetLoginUsuario(P.User.GetLoginUsuario);
      DocumentoLegal.SetFecha(Date);
      DocumentoLegal.SetHora(Time);
      DocumentoLegal.Insertar;
    end;
    FreeAndNil(FrmDocumentoLegal);
    Application.ProcessMessages;
    //Guardando Detalles
    GuardarDetalles;
    //Guardando Cobranza Cobrada
    GuardarCobranzaCobrada;
    //Guardando Cuenta x Cobrar
    GuardarCuentaxCobrar;
    //Guardando Movimiento de Inventario
    GuardarMovimientoInventario;
    //Verificando Si La Factura Fue Importada para asociar los Documentos
    if (SwImportado) then
    begin
      //Importado Desde Taller Servicio
      if (SwTallerServicio) then
      begin
        TallerServicio := TTallerServicio.Create;
        TallerServicio.SetNumero(NumeroTallerServicio);
        TallerServicio.BuscarNumero;
        TallerServicio.SetIdFacturacionCliente(Clase.GetIdFacturacionCliente);
        TallerServicio.Modificar;
        TallerServicio.Destroy;
      end
      else
      begin
        //Importado Desde...
      end;      
    end;
    MessageBox(Self.Handle, 'La Factura Ha Sido Agregada Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    //Se Imprime la Factura
    MessageBox(Self.Handle, 'Presione "Aceptar" Para Proceder A Imprimir La Factura!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    //Verificando Modulo de Radio
    Parametro.SetNombreParametro('modulo_radio_activado');
    if (Parametro.Buscar) then
    begin
      if (Parametro.ObtenerValorAsBoolean) then
      begin //Se Imprime En Formato de Radio
        with Reporte do
        begin
          AgregarParametro('NumeroFactura',Clase.GetNumero);
          AgregarParametro('MontoLetra',NumeroALetras(Clase.GetTotal));
          EmiteReporte('RadioFacturacionCliente',trDocumento);
        end;
      end
      else
      begin
        with Reporte do
        begin
          AgregarParametro('NumeroFactura',Clase.GetNumero);
          AgregarParametro('MontoLetra',NumeroALetras(Clase.GetTotal));
          if (Clase.GetFormaLibre) then
          begin
            ImprimeReporte('FacturacionClienteFormaLibre',trDocumento);
          end
          else
          begin
            ImprimeReporte('FacturacionCliente',trDocumento);
          end;
        end;
      end;
    end
    else
    begin //Se Imprime En Formato Estandar del Sistema
      with Reporte do
      begin
        AgregarParametro('NumeroFactura',Clase.GetNumero);
        AgregarParametro('MontoLetra',NumeroALetras(Clase.GetTotal));
        if (Clase.GetFormaLibre) then
        begin
          ImprimeReporte('FacturacionClienteFormaLibre',trDocumento);
        end
        else
        begin
          ImprimeReporte('FacturacionCliente',trDocumento);
        end;
      end;
    end;
    TBtnImportar.Enabled := False;
  end
  else
  begin
    //Editando...
    Clase.Modificar;
    //Solicitando Datos Legales
    DocumentoLegal.SetDesde('FC');
    DocumentoLegal.SetNumeroDocumento(Clase.GetNumero);
    SwDocumentoLegal := DocumentoLegal.Buscar;
    if (SwDocumentoLegal) then
    begin
      FrmDocumentoLegal := TFrmDocumentoLegal.CreateEdit(Self,'FC',Clase.GetNumero,DocumentoLegal.GetSerie,DocumentoLegal.GetNumeroControl);
    end
    else
    begin
      FrmDocumentoLegal := TFrmDocumentoLegal.CreateInsert(Self,'FC',Clase.GetNumero,'','');
    end;
    FrmDocumentoLegal.ShowModal;
    if FrmDocumentoLegal.Aceptado then
    begin //Se Guardan Los Datos Legales
      DocumentoLegal.SetSerie(FrmDocumentoLegal.Serie);
      DocumentoLegal.SetNumeroControl(FrmDocumentoLegal.NumeroControl);
      DocumentoLegal.SetLoginUsuario(P.User.GetLoginUsuario);
      DocumentoLegal.SetFecha(Date);
      DocumentoLegal.SetHora(Time);
      if (SwDocumentoLegal) then
      begin
        DocumentoLegal.Modificar;
      end
      else
      begin
        DocumentoLegal.Insertar;
      end;
    end;
    FreeAndNil(FrmDocumentoLegal);
    //Actualizando Detalles (Eliminando Anteriores y Cargando los Nuevos)
    DetFacturacionCliente.EliminarIdFacturacionCliente(Clase.GetIdFacturacionCliente);
    //Guardando Detalles
    GuardarDetalles;
    //Guardando Cobranza Cobrada
    GuardarCobranzaCobrada;
    //Guardando Cuenta x Cobrar
    GuardarCuentaxCobrar;
    //Guardando Movimiento de Inventario
    GuardarMovimientoInventario;
    MessageBox(Self.Handle, 'La Factura Ha Sido Editada Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end;
end;

procedure TFrmFacturacionCliente.GuardarCobranzaCobrada;
begin
  //Caso de Esquema de Pago de Contado
  EsquemaPago.SetCodigo(Clase.GetCodigoEsquemaPago);
  EsquemaPago.Buscar;
  if (not EsquemaPago.GetCredito) then
  begin
    if (Editando) then
    begin
      CobranzaCobrada.EliminarIdCobranza(Clase.GetIdCobranza);
    end;
    with CobranzaCobrada do //Se Inserta la CobranzaCobrada
    begin
      SetDesde('FC');
      SetNumeroDocumento(Clase.GetNumero);
      SetCodigoMoneda(Clase.GetCodigoMoneda);
      SetCodigoCliente(Clase.GetCodigoCliente);
      SetCodigoVendedor(Clase.GetCodigoVendedor);
      SetCodigoZona(Clase.GetCodigoZona);
      SetFechaIngreso(Clase.GetFechaIngreso);
      SetFechaVencIngreso(Clase.GetFechaIngreso);
      SetFechaRecepcion(Clase.GetFechaRecepcion);
      SetFechaVencRecepcion(Clase.GetFechaRecepcion);
      SetFechaContable(Clase.GetFechaContable);
      SetFechaCobranza(clase.GetFechaIngreso);
      SetFechaLibros(Clase.GetFechaLibros);
      SetNumeroCuota(-1);
      SetTotalCuotas(-1);
      SetOrdenCobranza(1);
      SetMontoNeto(Clase.GetMontoNeto);
      SetImpuesto(Clase.GetImpuesto);
      SetTotal(Clase.GetTotal);
      SetPIva(Clase.GetPIva);
      SetIdCobranza(Clase.GetIdCobranza);
      SetIdContadoCobranza(Clase.GetIdContadoCobranza);
      Insertar;
    end;
  end
  else //Caso de Esquema de Pago a Credito
  begin
    if (EsquemaPago.GetInicial) then //Caso de Creditos con Inicial
    begin
      if (Editando) then
      begin
        CobranzaCobrada.EliminarIdCobranza(Clase.GetIdCobranza);
      end;
      with CobranzaCobrada do //Se Inserta la CobranzaCobrada
      begin
        SetDesde('FC');
        SetNumeroDocumento(Clase.GetNumero);
        SetCodigoMoneda(Clase.GetCodigoMoneda);
        SetCodigoCliente(Clase.GetCodigoCliente);
        SetCodigoVendedor(Clase.GetCodigoVendedor);
        SetCodigoZona(Clase.GetCodigoZona);
        SetFechaIngreso(Clase.GetFechaIngreso);
        SetFechaVencIngreso(Clase.GetFechaIngreso);
        SetFechaRecepcion(Clase.GetFechaRecepcion);
        SetFechaVencRecepcion(Clase.GetFechaRecepcion);
        SetFechaContable(Clase.GetFechaContable);
        SetFechaCobranza(clase.GetFechaIngreso);
        SetFechaLibros(Clase.GetFechaLibros);
        SetNumeroCuota(0);
        if ((EsquemaPago.GetCuotas) and (EsquemaPago.GetCantidadCuotas > 0)) then
        begin
          SetTotalCuotas(EsquemaPago.GetCantidadCuotas);
        end
        else
        begin
          if ((EsquemaPago.GetDias) and (EsquemaPago.GetCantidadDias > 0)) then
          begin
            SetTotalCuotas(1);
          end
          else
          begin
            SetTotalCuotas(-1); (* NO SE CONFIGURO CORRECTAMENTE EL ESQUEMA DE PAGO DE CONTADO*)
          end;
        end;
        SetOrdenCobranza(1);
        SetMontoNeto(Clase.GetTotal * (EsquemaPago.GetPInicial / 100));
        SetImpuesto(0);
        SetTotal(Clase.GetMontoNeto);
        SetPIva(Clase.GetPIva);
        SetIdCobranza(Clase.GetIdCobranza);
        SetIdContadoCobranza(Clase.GetIdContadoCobranza);
        Insertar;
      end;
    end;
  end;
end;

procedure TFrmFacturacionCliente.GuardarCuentaxCobrar;
var
  FechaIngreso, FechaRecepcion: TDate;
  MontoCuota: Currency;
  MontoTotal: Currency;
  MontoBase: Currency;
  MontoInicial: Currency;
  Impuesto: Currency;
  i: Integer;
begin
  if (Editando) then
  begin
    CuentaxCobrar.EliminarIdCobranza(Clase.GetIdCobranza);
  end;
  EsquemaPago.SetCodigo(Clase.GetCodigoEsquemaPago);
  EsquemaPago.Buscar;
  if (EsquemaPago.GetCredito) then
  begin
    if (EsquemaPago.GetInicial) then //Credito con Inicial
    begin
      if (EsquemaPago.GetCuotas) then //Credito con  Inicial + Cuotas
      begin
        with CuentaxCobrar do //Se Inserta la CuentaxCobrar
        begin
          FechaIngreso := Clase.GetFechaIngreso;
          FechaRecepcion := Clase.GetFechaRecepcion;
          //MONTO INICIAL = (PORCION INICIAL = % SUB TOTAL) + IMPUESTO
          MontoInicial := (Clase.GetSubTotal * (EsquemaPago.GetPInicial / 100)) + Clase.GetImpuesto;
          MontoCuota := (Clase.GetTotal - MontoInicial) / EsquemaPago.GetCantidadCuotas;
          MontoBase := MontoCuota;
          Impuesto := 0;
          for i := 1 to EsquemaPago.GetCantidadCuotas do
          begin
            SetDesde('FC');
            SetNumeroDocumento(Clase.GetNumero);
            SetCodigoMoneda(Clase.GetCodigoMoneda);
            SetCodigoCliente(Clase.GetCodigoCliente);
            SetCodigoVendedor(Clase.GetCodigoVendedor);
            SetCodigoZona(Clase.GetCodigoZona);
            SetFechaIngreso(Clase.GetFechaIngreso);
            SetFechaVencIngreso(FechaIngreso + EsquemaPago.GetDiasCuota);
            SetFechaRecepcion(Clase.GetFechaRecepcion);
            SetFechaVencRecepcion(FechaRecepcion + EsquemaPago.GetDiasCuota);
            SetFechaContable(Clase.GetFechaContable);
            SetFechaCobranza(clase.GetFechaIngreso);
            SetFechaLibros(Clase.GetFechaLibros);
            SetNumeroCuota(i);
            SetTotalCuotas(EsquemaPago.GetCantidadCuotas);
            SetOrdenCobranza(1);
            SetMontoNeto(MontoBase);
            SetImpuesto(Impuesto);
            SetTotal(MontoCuota);
            SetPIva(Clase.GetPIva);
            SetIdCobranza(Clase.GetIdCobranza);
            SetIdContadoCobranza(Clase.GetIdContadoCobranza);
            Insertar;
            FechaIngreso := FechaIngreso + EsquemaPago.GetDiasCuota;
            FechaRecepcion := FechaRecepcion + EsquemaPago.GetDiasCuota;
          end;
        end;
      end
      else //Credito con Inicial + Dias
      begin
        MontoInicial := (Clase.GetSubTotal * (EsquemaPago.GetPInicial / 100)) + Clase.GetImpuesto;
        MontoTotal := Clase.GetTotal - MontoInicial;
        MontoBase := MontoTotal;
        Impuesto := 0;
        with CuentaxCobrar do //Se Inserta la CuentaxCobrar
        begin
          SetDesde('FC');
          SetNumeroDocumento(Clase.GetNumero);
          SetCodigoMoneda(Clase.GetCodigoMoneda);
          SetCodigoCliente(Clase.GetCodigoCliente);
          SetCodigoVendedor(Clase.GetCodigoVendedor);
          SetCodigoZona(Clase.GetCodigoZona);
          SetFechaIngreso(Clase.GetFechaIngreso);
          SetFechaVencIngreso(Clase.GetFechaIngreso + EsquemaPago.GetCantidadDias);
          SetFechaRecepcion(Clase.GetFechaRecepcion);
          SetFechaVencRecepcion(Clase.GetFechaRecepcion + EsquemaPago.GetCantidadDias);
          SetFechaContable(Clase.GetFechaContable);
          SetFechaCobranza(Clase.GetFechaIngreso);
          SetFechaLibros(Clase.GetFechaLibros);
          SetNumeroCuota(-1);
          SetTotalCuotas(-1);
          SetOrdenCobranza(1);
          SetMontoNeto(MontoBase);
          SetImpuesto(Impuesto);
          SetTotal(MontoTotal);
          SetPIva(Clase.GetPIva);
          SetIdCobranza(Clase.GetIdCobranza);
          SetIdContadoCobranza(Clase.GetIdContadoCobranza);
          Insertar;
        end;
      end;
    end
    else
    begin
      if (EsquemaPago.GetCuotas) then //Credito con Cuotas
      begin
        with CuentaxCobrar do //Se Inserta la CuentaxCobrar
        begin
          FechaIngreso := Clase.GetFechaIngreso;
          FechaRecepcion := Clase.GetFechaRecepcion;
          MontoCuota := Clase.GetTotal / EsquemaPago.GetCantidadCuotas;
          MontoBase := Clase.GetSubTotal / EsquemaPago.GetCantidadCuotas;
          Impuesto := Clase.GetImpuesto / EsquemaPago.GetCantidadCuotas;
          for i := 1 to EsquemaPago.GetCantidadCuotas do //Agregando Cuotas Fijas
          begin
            SetDesde('FC');
            SetNumeroDocumento(Clase.GetNumero);
            SetCodigoMoneda(Clase.GetCodigoMoneda);
            SetCodigoCliente(Clase.GetCodigoCliente);
            SetCodigoVendedor(Clase.GetCodigoVendedor);
            SetCodigoZona(Clase.GetCodigoZona);
            SetFechaIngreso(Clase.GetFechaIngreso);
            SetFechaVencIngreso(FechaIngreso + EsquemaPago.GetDiasCuota);
            SetFechaRecepcion(Clase.GetFechaRecepcion);
            SetFechaVencRecepcion(FechaRecepcion + EsquemaPago.GetDiasCuota);
            SetFechaContable(Clase.GetFechaContable);
            SetFechaCobranza(Clase.GetFechaIngreso);
            SetFechaLibros(Clase.GetFechaLibros);
            SetNumeroCuota(i);
            SetTotalCuotas(EsquemaPago.GetCantidadCuotas);
            SetOrdenCobranza(1);
            SetMontoNeto(MontoBase);
            SetImpuesto(Impuesto);
            SetTotal(MontoCuota);
            SetPIva(Clase.GetPIva);
            SetIdCobranza(Clase.GetIdCobranza);
            SetIdContadoCobranza(Clase.GetIdContadoCobranza);
            Insertar;
            FechaIngreso := FechaIngreso + EsquemaPago.GetDiasCuota;
            FechaRecepcion := FechaRecepcion + EsquemaPago.GetDiasCuota;
          end;
        end;
      end
      else //Credito por Dias
      begin
        with CuentaxCobrar do //Se Inserta la CuentaxCobrar
        begin
          SetDesde('FC');
          SetNumeroDocumento(Clase.GetNumero);
          SetCodigoMoneda(Clase.GetCodigoMoneda);
          SetCodigoCliente(Clase.GetCodigoCliente);
          SetCodigoVendedor(Clase.GetCodigoVendedor);
          SetCodigoZona(Clase.GetCodigoZona);
          SetFechaIngreso(Clase.GetFechaIngreso);
          SetFechaVencIngreso(Clase.GetFechaIngreso + EsquemaPago.GetCantidadDias);
          SetFechaRecepcion(Clase.GetFechaRecepcion);
          SetFechaVencRecepcion(Clase.GetFechaRecepcion + EsquemaPago.GetCantidadDias);
          SetFechaContable(Clase.GetFechaContable);
          SetFechaCobranza(Clase.GetFechaIngreso);
          SetFechaLibros(Clase.GetFechaLibros);
          SetNumeroCuota(-1);
          SetTotalCuotas(-1);
          SetOrdenCobranza(1);
          SetMontoNeto(Clase.GetMontoNeto);
          SetImpuesto(Clase.GetImpuesto);
          SetTotal(Clase.GetTotal);
          SetPIva(Clase.GetPIva);
          SetIdCobranza(Clase.GetIdCobranza);
          SetIdContadoCobranza(Clase.GetIdContadoCobranza);
          Insertar;
        end;
      end;
    end;
  end;
end;

procedure TFrmFacturacionCliente.GuardarDetalles;
begin
  with Data do
  begin
    DisableControls;
    First;
    while (not Eof) do
    begin
      with DetFacturacionCliente do
      begin
        SetCodigoProducto(FieldValues['codigo_producto']);
        SetReferencia(FieldValues['referencia']);
        SetDescripcion(FieldValues['descripcion']);
        SetCantidad(FieldValues['cantidad']);
        SetPrecio(FieldValues['precio']);
        SetPDescuento(FieldValues['p_descuento']);
        SetMontoDescuento(FieldValues['monto_descuento']);
        SetImpuesto(FieldValues['impuesto']);
        SetPIva(FieldValues['p_iva']);
        SetTotal(FieldValues['total']);
        SetIdFacturacionCliente(Clase.GetIdFacturacionCliente);
        Insertar;
      end;
      Next;
    end;
    EnableControls;
  end;
end;

procedure TFrmFacturacionCliente.GuardarMovimientoInventario;
begin
  if (Editando) then
  begin
    MovimientoInventario.EliminarIdMovimientoInventario(Clase.GetIdMovimientoInventario);
  end;
  with Data do
  begin
    DisableControls;
    First;
    while (not Eof) do
    begin
      Producto.SetCodigo(FieldValues['codigo_producto']);
      Producto.BuscarCodigo;
      if (Producto.GetInventario) then
      begin
        with MovimientoInventario do
        begin
          SetDesde('FC');
          SetNumeroDocumento(Clase.GetNumero);
          SetCodigoProducto(Producto.GetCodigo);
          SetCantidad(FieldValues['cantidad']);
          SetEstacionSalida(Clase.GetCodigoEstacion);
          SetEstacionEntrada('');
          SetCodigoZona(Clase.GetCodigoZona);
          SetPrecio(FieldValues['precio']);
          SetPDescuento(FieldValues['p_descuento']);
          SetMontoDescuento(FieldValues['monto_descuento']);
          SetImpuesto(FieldValues['impuesto']);
          SetPIva(FieldValues['p_iva']);
          SetTotal(FieldValues['total']);
          SetCosto(Producto.GetCostoActual);
          SetUsCosto(Producto.GetUsCostoActual);
          SetCostoFob(Producto.GetCostoFob);
          SetFecha(Clase.GetFechaIngreso);
          SetCodigoCliente(Clase.GetCodigoCliente);
          SetCodigoProveedor('');
          SetIdMovimientoInventario(Clase.GetIdMovimientoInventario);
          Insertar;
        end;
      end;
      Next;
    end;
    EnableControls;
  end;
end;

procedure TFrmFacturacionCliente.Imprimir;
var
  SwDocumentoLegal: Boolean;
begin
  if (Length(Trim(TxtNumero.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
  //Verificando Modulo de Radio
  Parametro.SetNombreParametro('modulo_radio_activado');
  if (Parametro.Buscar) then
  begin
    if (Parametro.ObtenerValorAsBoolean) then
    begin //Se Imprime En Formato de Radio
      //Solicitando Datos Legales
      DocumentoLegal.SetDesde('FC');
      DocumentoLegal.SetNumeroDocumento(Clase.GetNumero);
      SwDocumentoLegal := DocumentoLegal.Buscar;
      if (SwDocumentoLegal) then
      begin
        FrmDocumentoLegal := TFrmDocumentoLegal.CreateEdit(Self,'FC',Clase.GetNumero,DocumentoLegal.GetSerie,DocumentoLegal.GetNumeroControl);
      end
      else
      begin
        FrmDocumentoLegal := TFrmDocumentoLegal.CreateInsert(Self,'FC',Clase.GetNumero,'','');
      end;
      FrmDocumentoLegal.ShowModal;
      if FrmDocumentoLegal.Aceptado then
      begin //Se Guardan Los Datos Legales
        DocumentoLegal.SetSerie(FrmDocumentoLegal.Serie);
        DocumentoLegal.SetNumeroControl(FrmDocumentoLegal.NumeroControl);
        DocumentoLegal.SetLoginUsuario(P.User.GetLoginUsuario);
        DocumentoLegal.SetFecha(Date);
        DocumentoLegal.SetHora(Time);
        if (SwDocumentoLegal) then
        begin
          DocumentoLegal.Modificar;
        end
        else
        begin
          DocumentoLegal.Insertar;
        end;
      end;
      FreeAndNil(FrmDocumentoLegal);
      //Se Imprime
      with Reporte do
      begin
        AgregarParametro('NumeroFactura',Clase.GetNumero);
        AgregarParametro('MontoLetra',NumeroALetras(Clase.GetTotal));
        EmiteReporte('RadioFacturacionCliente',trDocumento);
      end;
    end
    else
    begin
      with Reporte do
      begin
        AgregarParametro('NumeroFactura',Clase.GetNumero);
        AgregarParametro('MontoLetra',NumeroALetras(Clase.GetTotal));
        if (Clase.GetFormaLibre) then
        begin
          EmiteReporte('FacturacionClienteFormaLibre',trDocumento);
        end
        else
        begin
          EmiteReporte('FacturacionCliente',trDocumento);
        end;
      end;
    end;
  end
  else
  begin //Se Imprime En Formato Estandar del Sistema
    with Reporte do
    begin
      AgregarParametro('NumeroFactura',Clase.GetNumero);
      AgregarParametro('MontoLetra',NumeroALetras(Clase.GetTotal));
      if (Clase.GetFormaLibre) then
      begin
        EmiteReporte('FacturacionClienteFormaLibre',trDocumento);
      end
      else
      begin
        EmiteReporte('FacturacionCliente',trDocumento);
      end;
    end;
  end;
end;

procedure TFrmFacturacionCliente.LblImpuestoDblClick(Sender: TObject);
var
  Aux: Currency;
begin
  if (Clase.GetFormaLibre) then
  begin
    Aux := 0;
    Solicitar(Self,'Impuesto',TxtImpuesto.Value,Aux);
    if (Aux >= 0) then
    begin
      TxtImpuesto.Value := Aux;
      CalcularMontosCabecera;
    end;
  end
  else
  begin
    MessageBox(Self.Handle, 'La Factura No Es de Forma "Concepto Libre"', PChar(MsgTituloError), MB_ICONSTOP or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end;
end;

procedure TFrmFacturacionCliente.LblMontoDescuentoDblClick(Sender: TObject);
var
  Aux: Currency;
begin
  Aux := 0;
  Solicitar(Self,'Monto Descuento',TxtMontoDescuento.Value,Aux);
  if (Aux >= 0) then
  begin
    TxtMontoDescuento.Value := Aux;
    TxtPDescuento.Caption := FormatFloat('#,##0.0000%',Aux * 100 / TxtMontoNeto.Value);
    if (Clase.GetFormaLibre) then
    begin
      CalcularMontosCabecera;
    end
    else
    begin
      CalcularMontos;
    end;
  end;
end;

procedure TFrmFacturacionCliente.LblMontoNetoDblClick(Sender: TObject);
begin
  if (Data.IsEmpty) then
  begin
    if (MessageBox(Self.Handle, '�Desea Activar Forma Concepto Libre?', PChar(MsgTituloInformacion), MB_ICONQUESTION or MB_YESNO or MB_TOPMOST or MB_TASKMODAL) = mrYes) then
    begin
      Clase.SetFormaLibre(True);
      TabDetalle.TabVisible := False;
      PageDetalles.ActivePage := TabConcepto;
      TxtMontoNeto.ReadOnly := False;
    end
    else
    begin
      Clase.SetFormaLibre(False);
      TabDetalle.TabVisible := True;
      PageDetalles.ActivePage := TabDetalle;
      TxtMontoNeto.ReadOnly := True;
      TxtMontoNeto.Value := 0;
      TxtMontoDescuento.Value := 0;
      TxtPDescuento.Caption := FormatFloat('#,##0.0000%',0);
      TxtSubTotal.Value := 0;
      TxtImpuesto.Value := 0;
      TxtTotal.Value := 0;
    end;
  end;
end;

procedure TFrmFacturacionCliente.Limpiar;
begin
  TxtNumero.Text := '';
  //NumeroPedido
  CmbCodigoCliente.KeyValue := '';
  CmbCodigoVendedor.KeyValue := '';
  CmbCodigoEstacion.KeyValue := '';
  CmbCodigoZona.KeyValue := '';
  CmbCodigoEsquemaPago.KeyValue := '';
  CmbCodigoMoneda.KeyValue := '';
  DatFechaIngreso.Date := Date;
  DatFechaRecepcion.Date := Date;
  DatFechaContable.Date := Date;
  DatFechaLibros.Date := Date;
  TxtConcepto.Text := '';
  TxtObservaciones.Text := '';
  TxtMontoNeto.Value := 0;
  TxtMontoDescuento.Value := 0;
  TxtSubTotal.Value := 0;
  TxtImpuesto.Value := 0;
  TxtTotal.Value := 0;
  TxtPDescuento.Caption := FormatFloat('###0.0000%',0);
  TxtPIVA.Caption := FormatFloat('###0.0000%',0);
  //Status Bar
  SB.Panels[0].Text := '';
  Data.EmptyDataSet;
  //Formato Concepto Libre
  TxtMontoNeto.ReadOnly := True;
  TabDetalle.TabVisible := True;
  PageDetalles.ActivePage := TabDetalle;
  Clase.SetFormaLibre(False);
end;

procedure TFrmFacturacionCliente.Mover;
begin

end;

procedure TFrmFacturacionCliente.Obtener;
begin
  with Clase do
  begin
    SetNumero(TxtNumero.Text);
    SetNumeroPedido('Indefinido');
    SetCodigoCliente(CmbCodigoCliente.KeyValue);
    SetCodigoVendedor(CmbCodigoVendedor.KeyValue);
    SetCodigoEstacion(CmbCodigoEstacion.KeyValue);
    SetCodigoZona(CmbCodigoZona.KeyValue);
    SetCodigoEsquemaPago(CmbCodigoEsquemaPago.KeyValue);
    SetCodigoMoneda(CmbCodigoMoneda.KeyValue);
    SetFechaIngreso(DatFechaIngreso.Date);
    SetFechaRecepcion(DatFechaRecepcion.Date);
    SetFechaContable(DatFechaContable.Date);
    SetFechaLibros(DatFechaLibros.Date);
    SetConcepto(TxtConcepto.Text);
    SetObservaciones(TxtObservaciones.Text);
    SetMontoNeto(TxtMontoNeto.Value);
    SetMontoDescuento(TxtMontoDescuento.Value);
    SetSubTotal(TxtSubTotal.Value);
    SetImpuesto(TxtImpuesto.Value);
    SetTotal(TxtTotal.Value);
    SetPDescuento(FormatearFloat(StringReplace(TxtPDescuento.Caption,'%','',[rfReplaceAll])));
    SetPIva(FormatearFloat(StringReplace(TxtPIVA.Caption,'%','',[rfReplaceAll])));
    SetTasaDolar(0); //***//
    SetLoginUsuario(P.User.GetLoginUsuario);
    if (Agregando) then
    begin
      SetTiempoIngreso(Now);
      SetIdFacturacionCliente(-1);
      SetIdCobranza(-1);
      SetIdMovimientoInventario(-1);
      SetIdContadoCobranza(-1);
    end;
  end;
end;

procedure TFrmFacturacionCliente.PMITallerServicioClick(Sender: TObject);
var
  TallerBusquedaServicio: TTallerBusquedaServicio;
  TallerServicio: TTallerServicio;
  TallerDetServicio: TTallerDetServicio;
  DataTallerBuscadorServicio: TClientDataSet;
  DataTallerDetServicio: TClientDataSet;
begin
  SwImportado := False;
  SwTallerServicio := False;
  TallerBusquedaServicio := TTallerBusquedaServicio.Create;
  TallerServicio := TTallerServicio.Create;
  TallerDetServicio := TTallerDetServicio.Create;
  DataTallerBuscadorServicio := TallerBusquedaServicio.ObtenerLista;
  if (Buscador(Self,TallerBusquedaServicio,'numero','Buscar Servicio',nil,DataTallerBuscadorServicio,'id_facturacion_cliente = -1')) then
  begin
    if (MessageBox(Self.Handle, '�Esta Seguro Que Desea Importar Este Servicio?', PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_YESNO or MB_TOPMOST or MB_TASKMODAL) = mrYes) then
    begin
      SwImportado := True; SwTallerServicio := True;
      NumeroTallerServicio := DataTallerBuscadorServicio.FieldValues['numero'];
      //Generando Factura desde Servicio de Taller
      TallerServicio.SetNumero(NumeroTallerServicio);
      TallerServicio.BuscarNumero;
      DataTallerDetServicio := TallerDetServicio.ObtenerIdTallerServicio(TallerServicio.GetIdTallerServicio);
      //Importando...
      with TallerServicio do
      begin
        if (GetFormaLibre) then
        begin
          Clase.SetFormaLibre(True);
          TabDetalle.TabVisible := False;
          PageDetalles.ActivePage := TabConcepto;
          TxtMontoNeto.ReadOnly := False;
        end
        else
        begin
          Clase.SetFormaLibre(False);
          TabDetalle.TabVisible := True;
          PageDetalles.ActivePage := TabDetalle;
          TxtMontoNeto.ReadOnly := True;
        end;
        CmbCodigoCliente.KeyValue := GetCodigoCliente;
        CmbCodigoVendedor.KeyValue := GetCodigoVendedor;
        CmbCodigoEstacion.KeyValue := Parametro.ObtenerValor('codigo_estacion_salida');
        CmbCodigoZona.KeyValue := GetCodigoZona;
        CmbCodigoEsquemaPago.KeyValue := Parametro.ObtenerValor('codigo_esquema_pago');
        CmbCodigoMoneda.KeyValue := Parametro.ObtenerValor('codigo_moneda');
        DatFechaIngreso.Date := Date;
        DatFechaRecepcion.Date := Date;
        DatFechaContable.Date := Date;
        DatFechaLibros.Date := Date;
        TxtConcepto.Text := GetConcepto;
        TxtObservaciones.Text := GetObservaciones;
        TxtMontoNeto.Value := GetMontoNeto;
        TxtMontoDescuento.Value := GetMontoDescuento;
        TxtSubTotal.Value := GetSubTotal;
        TxtImpuesto.Value := GetImpuesto;
        TxtTotal.Value := GetTotal;
        TxtPDescuento.Caption := FormatFloat('###0.0000%',GetPDescuento);
        TxtPIVA.Caption := FormatFloat('###0.0000%',GetPIva);        
      end;
      //Importando Detalles
      with Data do
      begin
        EmptyDataSet;
        DisableControls;
        DataTallerDetServicio.First;
        while (not DataTallerDetServicio.Eof) do
        begin
          if (DataTallerDetServicio.FieldValues['facturable']) then
          begin
            Append;
            FieldValues['codigo_producto'] := DataTallerDetServicio.FieldValues['codigo_producto'];
            FieldValues['referencia'] := DataTallerDetServicio.FieldValues['referencia'];
            FieldValues['descripcion'] := DataTallerDetServicio.FieldValues['descripcion'];
            FieldValues['cantidad'] := DataTallerDetServicio.FieldValues['cantidad'];
            FieldValues['precio'] := DataTallerDetServicio.FieldValues['precio'];
            FieldValues['p_descuento'] := DataTallerDetServicio.FieldValues['p_descuento'];
            FieldValues['monto_descuento'] := DataTallerDetServicio.FieldValues['monto_descuento'];
            FieldValues['impuesto'] := DataTallerDetServicio.FieldValues['impuesto'];
            FieldValues['p_iva'] := DataTallerDetServicio.FieldValues['p_iva'];
            FieldValues['total'] := DataTallerDetServicio.FieldValues['total'];
            Post;
          end;
          DataTallerDetServicio.Next;                  
        end;
        First;
        EnableControls;
      end;
      if (TallerServicio.GetFormaLibre) then
      begin
        CalcularMontosCabecera;
      end
      else
      begin
        CalcularMontos;
      end;
    end;
  end;
  TallerDetServicio.Destroy;
  TallerServicio.Destroy;
  TallerBusquedaServicio.Destroy;
end;

procedure TFrmFacturacionCliente.Refrescar;
begin
  //Se Actualiza El DataBuscador
  DataBuscadorFacturacionCliente := BuscadorFacturacionCliente.ObtenerLista;
  //Se Actualizan Los demas Componentes
  DataCliente := Cliente.ObtenerCombo;
  DataVendedor := Vendedor.ObtenerCombo;
  DataEstacion := Estacion.ObtenerCombo;
  DataZona := Zona.ObtenerCombo;
  DataEsquemaPago := EsquemaPago.ObtenerCombo;
  DataMoneda := Moneda.ObtenerCombo;
  DataCliente.First;
  DataVendedor.First;
  DataEstacion.First;
  DataZona.First;
  DataEsquemaPago.First;
  DataMoneda.First;
  DataBuscadorFacturacionCliente.First;
  DSCliente.DataSet := DataCliente;
  DSVendedor.DataSet := DataVendedor;
  DSEstacion.DataSet := DataEstacion;
  DSZona.DataSet := DataZona;
  DSEsquemaPago.DataSet := DataEsquemaPago;
  DSMoneda.DataSet := DataMoneda;
end;

procedure TFrmFacturacionCliente.TBtnEliminarClick(Sender: TObject);
begin
  if (Length(Trim(TxtNumero.Text)) > 0) then
  begin
    inherited;
  end
  else
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
end;

procedure TFrmFacturacionCliente.TxtMontoNetoExit(Sender: TObject);
begin
  CalcularMontosCabecera;
end;

procedure TFrmFacturacionCliente.TxtPDescuentoDblClick(Sender: TObject);
var
  Aux: Currency;
begin
  Aux := 0;
  Solicitar(Self,'(%) Descuento',StrToFloat(StringReplace(TxtPDescuento.Caption,'%','',[rfReplaceAll])),Aux);
  if (Aux >= 0) then
  begin
    TxtPDescuento.Caption := FormatFloat('#,##0.0000%',Aux);
    TxtMontoDescuento.Value := Aux * TxtMontoNeto.Value / 100;
    if (Clase.GetFormaLibre) then
    begin
      CalcularMontosCabecera;
    end
    else
    begin
      CalcularMontos;
    end;
  end;
end;

procedure TFrmFacturacionCliente.TxtPIVADblClick(Sender: TObject);
var
  Aux: Currency;
  Exentos: Boolean;
  BMark: TBookmark;
begin
  if (Clase.GetFormaLibre) then //Forma Concepto Libre
  begin
    Aux := 0;
    Solicitar(Self,'(%) I.V.A.',StrToFloat(StringReplace(TxtPIVA.Caption,'%','',[rfReplaceAll])),Aux);
    if (Aux >= 0) then
    begin
      TxtPIVA.Caption := FormatFloat('#,##0.0000%',Aux);
      TxtImpuesto.Value := Aux * TxtMontoNeto.Value / 100;
      CalcularMontosCabecera;
    end;
  end
  else
  begin
    Aux := 0;
    Solicitar(Self,'(%) I.V.A.',StrToFloat(StringReplace(TxtPIVA.Caption,'%','',[rfReplaceAll])),Aux);
    if (Aux >= 0) then
    begin
      TxtPIVA.Caption := FormatFloat('#,##0.0000%',Aux);
      if (not Data.IsEmpty) then
      begin
        if (MessageBox(Self.Handle, '�Desea Actualizar El Porcentaje De Impuesto En Todos Los Renglones?', PChar(MsgTituloInformacion), MB_ICONQUESTION or MB_YESNO or MB_TOPMOST or MB_TASKMODAL) = mrYes) then
        begin
          if (MessageBox(Self.Handle, '�Incluir Renglones Exentos de I.V.A.?', PChar(MsgTituloInformacion), MB_ICONQUESTION or MB_YESNO or MB_TOPMOST or MB_TASKMODAL) = mrYes) then
          begin
            Exentos := True;
          end
          else
          begin
            Exentos := False;
          end;
          //Actualizando PIVA en Renglones
          with Data do
          begin
            DisableControls;
            First;
            while (not Eof) do
            begin
              if ((FieldValues['impuesto'] > 0) or (Exentos)) then
              begin
                //Utilizando Punteros de los Registros (Bookmarks)
                //Debido a una deficiencia en la edicion de ClientDataSets
                BMark := GetBookmark;
                Edit;
                FieldValues['p_iva'] := Aux;
                FieldValues['impuesto'] := (FieldValues['precio'] * FieldValues['cantidad']) * (FieldValues['p_iva'] / 100);
                Post;
                GotoBookmark(BMark);
              end;
              Next;
            end;
            EnableControls;
          end;
          MessageBox(Self.Handle, 'Renglones Actualizados Correctamente', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
        end;
      end;
      CalcularMontos;
    end;
  end;
end;

function TFrmFacturacionCliente.Validar: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  //Validando Cliente
  if (Length(Trim(CmbCodigoCliente.Text)) = 0) then
  begin
    ListaErrores.Add('El Cliente No Puede Estar En Blanco!!!');
  end;
  //Validando Vendedor
  if (Length(Trim(CmbCodigoVendedor.Text)) = 0) then
  begin
    ListaErrores.Add('El Vendedor No Puede Estar En Blanco!!!');
  end;
  //Validando Monto
  if (TxtTotal.Value <= 0) then
  begin
    ListaErrores.Add('El Total Debe Ser Positivo!!!');
  end;
  //Validando Fechas
  if ((DatFechaIngreso.Date > DatFechaRecepcion.Date) or (DatFechaIngreso.Date > DatFechaContable.Date) or (DatFechaIngreso.Date > DatFechaLibros.Date)) then
  begin
    ListaErrores.Add('La Fecha de Ingreso Debe Ser Menor a las Fechas de Recepcion, Contable y Libros Respectivamente!!!');
  end;
  //Validando Detalle o Forma Libre
  if (Clase.GetFormaLibre) then
  begin
    if (Length(Trim(TxtConcepto.Text)) = 0) then
    begin
      ListaErrores.Add('El Concepto No Puede Estar En Blanco!!!');
    end;
    if (TxtMontoNeto.Value <= 0) then
    begin
      ListaErrores.Add('El Monto Neto Debe Ser Positivo!!!');
    end;
    if (TxtMontoDescuento.Value < 0) then
    begin
      ListaErrores.Add('El Monto de Descuento No Debe Ser Negativo!!!');
    end;
    if (TxtImpuesto.Value < 0) then
    begin
      ListaErrores.Add('El Impuesto No Debe Ser Negativo!!!');
    end;
  end
  else
  begin
    if (Data.RecordCount = 0) then
    begin
      ListaErrores.Add('El Detalle De La Factura No Puede Estar Vacio!!!');
    end;
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

procedure TFrmFacturacionCliente.DataAfterPost(DataSet: TDataSet);
begin
  CalcularMontos;
end;

procedure TFrmFacturacionCliente.Detalle;
begin
  inherited;
  FrmAgregarDetalleFacturacionCliente := TFrmAgregarDetalleFacturacionCliente.CrearDetalle(Self,Data);
  FrmAgregarDetalleFacturacionCliente.ShowModal;
  FreeAndNil(FrmAgregarDetalleFacturacionCliente);
end;

end.
