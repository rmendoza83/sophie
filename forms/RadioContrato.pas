unit RadioContrato;

{$I sophie.inc}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GenericoDetalle, DB, DBClient, ImgList, XPMan, ComCtrls, Grids,
  DBGrids, ExtCtrls, ToolWin, rxCurrEdit, StdCtrls, Buttons, Mask, rxToolEdit,
  RxLookup, Utils, ClsRadioContrato, ClsRadioContratoPrograma,
  ClsRadioContratoDistribucionFactura, ClsCliente, ClsRadioAnunciante,
  ClsVendedor, ClsRadioRubro, ClsZona, ClsBusquedaRadioContrato,
  ClsRadioContratoFacturaGenerada, ClsRadioContratoFacturaPendiente,
  ClsRadioPrograma, ClsRadioEmisora, ClsRadioEspacio, GenericoBuscador,
  ClsReporte, ClsParametro{$IFDEF DELPHI10}, ImageList{$ENDIF};

type
  TFrmRadioContrato = class(TFrmGenericoDetalle)
    TabDistribucionFacturacion: TTabSheet;
    LblNumero: TLabel;
    LblCodigoCliente: TLabel;
    LblCodigoZona: TLabel;
    LblFechaInicial: TLabel;
    LblFechaOrden: TLabel;
    LblNumeroOrden: TLabel;
    LblCodigoRubro: TLabel;
    LblTotal: TLabel;
    LblFechaFinal: TLabel;
    TxtNumero: TEdit;
    CmbCodigoCliente: TRxDBLookupCombo;
    DatFechaInicial: TDateEdit;
    DatFechaFinal: TDateEdit;
    CmbCodigoZona: TRxDBLookupCombo;
    BtnBuscarCliente: TBitBtn;
    BtnBuscarZona: TBitBtn;
    DatFechaOrden: TDateEdit;
    TxtNumeroOrden: TEdit;
    CmbCodigoRubro: TRxDBLookupCombo;
    BtnBuscarRubro: TBitBtn;
    TxtTotal: TCurrencyEdit;
    LblCodigoAnunciante: TLabel;
    CmbCodigoAnunciante: TRxDBLookupCombo;
    BtnBuscarAnunciante: TBitBtn;
    LblCodigoAgencia: TLabel;
    CmbCodigoAgencia: TRxDBLookupCombo;
    BtnBuscarAgencia: TBitBtn;
    LblCodigoVendedor: TLabel;
    CmbCodigoVendedor: TRxDBLookupCombo;
    BtnBuscarVendedor: TBitBtn;
    LblObservaciones: TLabel;
    TxtObservaciones: TEdit;
    LblProducto: TLabel;
    TxtProducto: TEdit;
    GrpOpciones: TGroupBox;
    ChkEfectivo: TCheckBox;
    ChkVentaNacional: TCheckBox;
    ChkIntercambio: TCheckBox;
    GridDistribucionFacturacion: TDBGrid;
    ChkCancelado: TCheckBox;
    DatFechaCancelado: TDateEdit;
    LblNroCuotas: TLabel;
    TxtNroFacturas: TCurrencyEdit;
    Datacodigo_emisora: TStringField;
    Datanombre_emisora: TStringField;
    Datacodigo_espacio: TStringField;
    Datanombre_espacio: TStringField;
    Datacodigo_programa: TStringField;
    Datanombre_programa: TStringField;
    Datacunas_diarias: TIntegerField;
    Datatotal_cunas: TIntegerField;
    Dataduracion: TIntegerField;
    Datamonto_agencia: TFloatField;
    Dataporcentaje: TFloatField;
    Datamonto_emisora: TFloatField;
    Datacomison: TFloatField;
    Datamonto_real: TFloatField;
    Datatarifa: TFloatField;
    Datacosto_bruto: TFloatField;
    Datap_descuento: TFloatField;
    DataDistribucionFacturacion: TClientDataSet;
    DSDistribucionFacturacion: TDataSource;
    DataDistribucionFacturacionitem: TIntegerField;
    DataDistribucionFacturaciontotal_items: TIntegerField;
    DataDistribucionFacturacionfecha_ingreso: TDateField;
    DataDistribucionFacturaciontotal: TFloatField;
    DataDistribucionFacturacionfecha_inicial: TDateField;
    DataDistribucionFacturacionfecha_final: TDateField;
    DataDistribucionFacturacionobservaciones: TStringField;
    DataDistribucionFacturacionid_facturacion_cliente: TIntegerField;
    DataDistribucionFacturacionnumero_documento: TStringField;
    DataDistribucionFacturacionfacturado: TBooleanField;
    DSCliente: TDataSource;
    DSAnunciante: TDataSource;
    DSAgencia: TDataSource;
    DSVendedor: TDataSource;
    DSRubro: TDataSource;
    DSZona: TDataSource;
    BtnGenerarCuotas: TBitBtn;
    procedure LblCodigoRubroDblClick(Sender: TObject);
    procedure LblCodigoRubroMouseEnter(Sender: TObject);
    procedure LblCodigoRubroMouseLeave(Sender: TObject);
    procedure LblCodigoAnuncianteDblClick(Sender: TObject);
    procedure LblCodigoAnuncianteMouseLeave(Sender: TObject);
    procedure LblCodigoAnuncianteMouseEnter(Sender: TObject);
    procedure GridDistribucionFacturacionDblClick(Sender: TObject);
    procedure GridDistribucionFacturacionKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtnGenerarCuotasClick(Sender: TObject);
    procedure BtnBuscarZonaClick(Sender: TObject);
    procedure BtnBuscarRubroClick(Sender: TObject);
    procedure BtnBuscarVendedorClick(Sender: TObject);
    procedure BtnBuscarAgenciaClick(Sender: TObject);
    procedure BtnBuscarAnuncianteClick(Sender: TObject);
    procedure CmbCodigoVendedorExit(Sender: TObject);
    procedure CmbCodigoAnuncianteChange(Sender: TObject);
    procedure DataBeforePost(DataSet: TDataSet);
    procedure CmbCodigoClienteChange(Sender: TObject);
    procedure CmbCodigoClienteExit(Sender: TObject);
    procedure BtnBuscarClienteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    Clase: TRadioContrato;
    RadioContratoPrograma: TRadioContratoPrograma;
    RadioContratoFacturaGenerada: TRadioContratoFacturaGenerada;
    RadioContratoFacturaPendiente: TRadioContratoFacturaPendiente;
    RadioContratoDistribucionFactura: TRadioContratoDistribucionFactura;
    BuscadorRadioContrato: TBusquedaRadioContrato;
    Cliente: TCliente;
    Anunciante: TRadioAnunciante;
    Agencia: TCliente;
    Vendedor: TVendedor;
    Rubro: TRadioRubro;
    Zona: TZona;
    RadioPrograma: TRadioPrograma;
    RadioEmisora: TRadioEmisora;
    RadioEspacio: TRadioEspacio;
    Reporte: TReporte;
    Parametro: TParametro;
    DataBuscadorRadioContrato: TClientDataSet;
    DataCliente: TClientDataSet;
    DataAnunciante: TClientDataSet;
    DataAgencia: TClientDataSet;
    DataVendedor: TClientDataSet;
    DataRubro: TClientDataSet;
    DataZona: TClientDataSet;
    procedure Agregar;
    procedure Editar;
    procedure Eliminar;
    procedure Guardar;
    procedure Cancelar;
    procedure Refrescar;
    procedure Imprimir;
    procedure Buscar;
    procedure Limpiar;
    procedure Asignar;
    procedure Obtener;
    procedure Mover;
    procedure GuardarDetalles;
  protected
    { Protected declarations }
    function Validar: Boolean; override;
    procedure Detalle; override;
    procedure DetalleDistribucionFacturacion;
  public
    { Public declarations }
  end;

var
  FrmRadioContrato: TFrmRadioContrato;

implementation

uses
  ActividadSophie,
  PModule,
  ClsGenericoBD,
  AgregarDetalleRadioContrato,
  EditarDetalleRadioContratoDistribucionFacturacion,
  Math,
  Types
  {$IFDEF USE_RAVE_REPORTS}
  , DMRR
  {$ENDIF}
  {$IFDEF USE_FAST_REPORTS}
  , DMFR
  {$ENDIF}
  ;

{$R *.dfm}

procedure TFrmRadioContrato.Agregar;
begin
  //Verificando
  Limpiar;
  //Asignando Valores por Defecto
  CmbCodigoZona.Value := Parametro.ObtenerValor('codigo_zona');
  CmbCodigoCliente.SetFocus;
end;

procedure TFrmRadioContrato.Asignar;
var
  DataDetalle: TClientDataSet;
  Campo: string;
  i: Integer;
begin
  with Clase do
  begin
    TxtNumero.Text := GetNumero;
    CmbCodigoCliente.KeyValue := GetCodigoCliente;
    CmbCodigoAnunciante.KeyValue := GetCodigoAnunciante;
    CmbCodigoAgencia.KeyValue := GetCodigoAgencia;
    CmbCodigoVendedor.KeyValue := GetCodigoVendedor;
    CmbCodigoRubro.KeyValue := GetCodigoRubro;
    DatFechaInicial.Date := GetFechaInicial;
    DatFechaFinal.Date := GetFechaFinal;
    CmbCodigoZona.KeyValue := GetCodigoZona;
    TxtNumeroOrden.Text := GetNumeroOrden;
    DatFechaOrden.Date := GetFechaOrden;
    TxtNroFacturas.Value := GetNroFacturas;
    TxtTotal.Value := GetTotal;
    ChkIntercambio.Checked := GetIntercambio;
    ChkEfectivo.Checked := GetEfectivo;
    ChkVentaNacional.Checked := GetVentaNacional;
    ChkCancelado.Checked := GetCancelado;
    DatFechaCancelado.Date := GetFechaCancelado;
    TxtObservaciones.Text := GetObservaciones;
    TxtProducto.Text := GetProducto;
    //Cargando DataSet Secundario "Data"
    DataDetalle := TClientDataSet.Create(Self);
    DataDetalle.CloneCursor(RadioContratoPrograma.ObtenerIdRadioContrato(GetIdRadioContrato),False,True);
    Data.EmptyDataSet;
    DataDetalle.First;
    while (not DataDetalle.Eof) do
    begin
      Data.Append;
      //Asignando Campos Similares
      for i := 0 to DataDetalle.FieldCount - 1 do
      begin
        Campo := DataDetalle.FieldDefs[i].Name;
        if (Data.FieldDefs.IndexOf(Campo) <> -1) then
        begin
          Data.FieldValues[Campo] := DataDetalle.FieldValues[Campo];
        end;        
      end;
      //Se Ubica El Programa
      RadioPrograma.SetCodigo(DataDetalle.FieldValues['codigo_programa']);
      RadioPrograma.Buscar;
      //Se Ubica La Emisora
      RadioEmisora.SetCodigo(RadioPrograma.GetCodigoEmisora);
      RadioEmisora.Buscar;
      //Se Ubica El Espacio
      RadioEspacio.SetCodigo(DataDetalle.FieldValues['codigo_espacio']);
      RadioEspacio.Buscar;
      Data.FieldValues['codigo_emisora'] := RadioEmisora.GetCodigo;
      Data.FieldValues['nombre_emisora'] := RadioEmisora.GetRazonSocial;
      Data.FieldValues['nombre_programa'] := RadioPrograma.GetDescripcion;
      Data.FieldValues['nombre_espacio'] := RadioEspacio.GetDescripcion;
      Data.Post;
      DataDetalle.Next;
    end;
    DS.DataSet := Data;
    DataDetalle.Free;
    //Cargando DataSet Secundario "DataDistribucionFacturacion"
    DataDetalle := TClientDataSet.Create(Self);
    DataDetalle.CloneCursor(RadioContratoDistribucionFactura.ObtenerIdRadioContrato(GetIdRadioContrato),False,True);
    DataDistribucionFacturacion.EmptyDataSet;
    DataDetalle.First;
    while (not DataDetalle.Eof) do
    begin
      DataDistribucionFacturacion.Append;
      //Asignando Campos Similares
      for i := 0 to DataDetalle.FieldCount - 1 do
      begin
        Campo := DataDetalle.FieldDefs[i].Name;
        if (DataDistribucionFacturacion.FieldDefs.IndexOf(Campo) <> -1) then
        begin
          DataDistribucionFacturacion.FieldValues[Campo] := DataDetalle.FieldValues[Campo];
        end;        
      end;
      DataDistribucionFacturacion.Post;
      DataDetalle.Next;
    end;
    DSDistribucionFacturacion.DataSet := DataDistribucionFacturacion;
  end;
end;

procedure TFrmRadioContrato.BtnBuscarAgenciaClick(Sender: TObject);
begin
  Buscador(Self,Agencia,'codigo','Buscar Agencia',nil,DataAgencia);
  CmbCodigoAgencia.KeyValue := DataAgencia.FieldValues['codigo'];
end;

procedure TFrmRadioContrato.BtnBuscarAnuncianteClick(Sender: TObject);
begin
  Buscador(Self,Anunciante,'codigo','Buscar Anunciante',nil,DataAnunciante);
  CmbCodigoAnunciante.KeyValue := DataAnunciante.FieldValues['codigo'];
end;

procedure TFrmRadioContrato.BtnBuscarClienteClick(Sender: TObject);
begin
  Buscador(Self,Cliente,'codigo','Buscar Cliente',nil,DataCliente);
  CmbCodigoCliente.KeyValue := DataCliente.FieldValues['codigo'];
  CmbCodigoClienteExit(Sender);
end;

procedure TFrmRadioContrato.BtnBuscarRubroClick(Sender: TObject);
begin
  Buscador(Self,Rubro,'codigo','Buscar Rubro',nil,DataRubro);
  CmbCodigoRubro.KeyValue := DataRubro.FieldValues['codigo'];
end;

procedure TFrmRadioContrato.BtnBuscarVendedorClick(Sender: TObject);
begin
  Buscador(Self,Vendedor,'codigo','Buscar Vendedor',nil,DataVendedor);
  CmbCodigoVendedor.KeyValue := DataVendedor.FieldValues['codigo'];
end;

procedure TFrmRadioContrato.BtnBuscarZonaClick(Sender: TObject);
begin
  Buscador(Self,Zona,'codigo','Buscar Zona',nil,DataZona);
  CmbCodigoZona.KeyValue := DataZona.FieldValues['codigo'];
end;

procedure TFrmRadioContrato.BtnGenerarCuotasClick(Sender: TObject);
var
  i: Integer;
  FechaInicio: TDate;
  FechaFin: TDate;
  Dias: Integer;
  CuotasAprox: Integer;
begin
  if (Agregando) then
  begin
    //Revisando Nro de Cuotas Segun Rango del Contrato
    Dias := Trunc(DatFechaFinal.Date) - Trunc(DatFechaInicial.Date);
    CuotasAprox := Dias Div 30;
    if ((Dias Mod 30) > 0) then
    begin
      CuotasAprox := CuotasAprox + 1;
    end;
    if (TxtNroFacturas.Value <> CuotasAprox) then //Se Lanza La Advertencia de la Cantidad de Cuotas Calculadas
    begin
      MessageBox(Self.Handle, PChar('La Cantidad De Cuotas Indicada Difiere De La Cantidad De Cuotas Sugeridas Segun'+#13+#10+'La Duraci�n Del Contrato.'+#13+#10+'Cantidad de Dias: ' +IntToStr(Dias)+#13+#10+'Cantidad de Cuotas Aproximadas: '+IntToStr(CuotasAprox)+' Cuotas'+#13+#10+''+#13+#10+'Por Favor Revise Estos Parametros Antes de Generar La Distribuci�n Autom�tica de Facturaci�n'), PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_APPLMODAL);
    end;
    if (TxtNroFacturas.Value > 0) then
    begin
      if (MessageBox(Self.Handle, PChar('�Esta Seguro Que Desea Generar La Distribuci�n de Facturaci�n?'+#13+#10+''+#13+#10+'Esta Acci�n Eliminara Cualquier Distribuci�n Generada con Anterioridad!!!'), PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_YESNO or MB_TOPMOST or MB_APPLMODAL) = mrYes) then
      begin
        with DataDistribucionFacturacion do
        begin
          DataDistribucionFacturacion.EmptyDataSet;
          FechaInicio := DatFechaInicial.Date;
          FechaFin := FechaInicio;
          for i := 0 to (Trunc(TxtNroFacturas.Value) - 1) do
          begin
            //Si es la Ultima Cuota
            if (i = (Trunc(TxtNroFacturas.Value) - 1)) then
            begin
              FechaInicio := FechaFin + 1;
              FechaFin := DatFechaFinal.Date;
            end
            else
            begin
              FechaFin := FechaInicio + 30;
            end;
            //Generando Pre-Factura
            Append ;
            FieldValues['item'] := i + 1;
            FieldValues['total_items'] := Trunc(TxtNroFacturas.Value);
            FieldValues['fecha_ingreso'] := Date;
            FieldValues['total'] := (TxtTotal.Value / TxtNroFacturas.Value);
            FieldValues['fecha_inicial'] := FechaInicio;
            FieldValues['fecha_final'] := FechaFin;
            FieldValues['observaciones'] := '';
            FieldValues['id_facturacion_cliente'] := -1;
            FieldValues['numero_documento'] := '';
            FieldValues['facturado'] := False;
            Post;
            FechaInicio := FechaFin + 1;
          end;
        end;
        PageDetalles.ActivePage := TabDistribucionFacturacion;
      end;
    end
    else
    begin
      MessageBox(Self.Handle, PChar('El Nro de Cuotas Debe Ser Mayor a Cero!!!'), PChar(MsgTituloError), MB_ICONSTOP or MB_OK or MB_TOPMOST or MB_APPLMODAL);
      TxtNroFacturas.SetFocus;
      TxtNroFacturas.SelectAll;
    end;
  end
  else
  begin
    MessageBox(Self.Handle, PChar('Solo Se Puede Generar La Distribucion En Contratos Nuevos!!!'), PChar(MsgTituloError), MB_ICONSTOP or MB_OK or MB_TOPMOST or MB_APPLMODAL);
  end;
end;

procedure TFrmRadioContrato.Buscar;
begin
  if (Buscador(Self,BuscadorRadioContrato,'numero','Buscar Contrato',nil,DataBuscadorRadioContrato)) then
  begin
    Clase.SetNumero(DataBuscadorRadioContrato.FieldValues['numero']);
    Clase.BuscarNumero;
    Asignar;
  end;
end;

procedure TFrmRadioContrato.Cancelar;
begin
  if (Agregando) then
  begin
    Limpiar;
  end
  else
  begin
    Clase.SetNumero(TxtNumero.Text);
    Clase.BuscarNumero;
    Asignar;
  end;
end;

procedure TFrmRadioContrato.CmbCodigoAnuncianteChange(Sender: TObject);
begin
  if (Length(Trim(CmbCodigoAnunciante.Text)) > 0) then
  begin
    TxtProducto.Text := DataAnunciante.FieldValues['descripcion'];
  end;
end;

procedure TFrmRadioContrato.CmbCodigoClienteChange(Sender: TObject);
begin
  SB.Panels[0].Text := DataCliente.FieldValues['razon_social'];
  CmbCodigoAgencia.KeyValue := CmbCodigoCliente.Text;
end;

procedure TFrmRadioContrato.CmbCodigoClienteExit(Sender: TObject);
begin
  if (Length(Trim(CmbCodigoCliente.Text)) > 0) then
  begin
    if (Agregando) then
    begin
      Cliente.SetCodigo(CmbCodigoCliente.Value);
      Cliente.BuscarCodigo;
      CmbCodigoZona.Value := Cliente.GetCodigoZona;
    end;
  end;
end;

procedure TFrmRadioContrato.CmbCodigoVendedorExit(Sender: TObject);
begin
  if (Length(Trim(CmbCodigoVendedor.Text)) > 0) then
  begin
    if (Agregando) then
    begin
      Vendedor.SetCodigo(CmbCodigoVendedor.Value);
      Vendedor.Buscar;
      CmbCodigoZona.Value := Vendedor.GetCodigoZona;
    end;
  end;
end;

procedure TFrmRadioContrato.DataBeforePost(DataSet: TDataSet);
begin
  //Calculando Campos Automaticos
  with DataSet do
  begin
    FieldValues['monto_agencia'] := TxtTotal.Value;
    FieldValues['porcentaje'] := 0;
    FieldValues['monto_emisora'] := TxtTotal.Value;
    FieldValues['comision'] := 0;
    FieldValues['monto_real'] := TxtTotal.Value;
    //Calculando Porcentaje de Descuento
    if (FieldValues['costo_bruto'] > 0) then
    begin
      FieldValues['p_descuento'] := (FieldValues['costo_bruto'] - TxtTotal.Value) / (100 * FieldValues['costo_bruto']);
    end
    else
    begin
      FieldValues['p_descuento'] := 0;
    end;
  end;
end;

procedure TFrmRadioContrato.Detalle;
begin
  inherited;
  FrmAgregarDetalleRadioContrato := TFrmAgregarDetalleRadioContrato.CrearDetalle(Self,Data);
  //FrmAgregarDetalleRadioContrato.CodigoCliente := CmbCodigoCliente.Text;
  FrmAgregarDetalleRadioContrato.ShowModal;
  FreeAndNil(FrmAgregarDetalleRadioContrato);
end;

procedure TFrmRadioContrato.DetalleDistribucionFacturacion;
begin
  with DataDistribucionFacturacion do
  begin
    if (RecordCount > 0) then
    begin
      if (FieldValues['facturado'] = False) then
      begin
        FrmEditarDetalleRadioContratoDistribucionFacturacion := TFrmEditarDetalleRadioContratoDistribucionFacturacion.CrearDetalle(Self,DataDistribucionFacturacion);
        FrmEditarDetalleRadioContratoDistribucionFacturacion.ShowModal;
        FreeAndNil(FrmEditarDetalleRadioContratoDistribucionFacturacion);
      end
      else
      begin
        MessageBox(Self.Handle, PChar('No Se Pueden Editar Los Items Ya Facturados!!!'), PChar(MsgTituloError), MB_ICONSTOP or MB_OK or MB_TOPMOST or MB_APPLMODAL);
      end;
    end;
  end;
end;

procedure TFrmRadioContrato.Editar;
begin
  if (Length(Trim(TxtNumero.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
end;

procedure TFrmRadioContrato.Eliminar;
begin
  //Se Busca El Contrato
  Clase.SetNumero(TxtNumero.Text);
  Clase.BuscarNumero;
  //Se Eliminan Los Detalles (Detalle de los Programas)
  RadioContratoPrograma.EliminarIdRadioContrato(Clase.GetIdRadioContrato);
  //Se Eliminan Las Facturas Pendientes y Generadas
  RadioContratoFacturaPendiente.EliminarIdRadioContrato(Clase.GetIdRadioContrato);
  RadioContratoFacturaGenerada.EliminarIdRadioContrato(Clase.GetIdRadioContrato);
  Clase.Eliminar;
  MessageBox(Self.Handle, 'El Contrato Se Ha Eliminado Con Exito!!!',PChar(MsgTituloInformacion), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  Refrescar;
  Limpiar;
end;

procedure TFrmRadioContrato.FormCreate(Sender: TObject);
begin
  inherited;
  ProcAgregar := Agregar;
  ProcEditar := Editar;
  ProcEliminar := Eliminar;
  ProcGuardar := Guardar;
  ProcCancelar := Cancelar;
  ProcRefrescar := Refrescar;
  ProcImprimir := Imprimir;
  ProcBuscar := Buscar;
  FrmActividadSophie.Show;
  FrmActividadSophie.Actividad('Cargando N�cleo...');
  Clase := TRadioContrato.Create;
  //Inicializando DataSets Secundarios
  BuscadorRadioContrato := TBusquedaRadioContrato.Create;
  Cliente := TCliente.Create;
  Anunciante := TRadioAnunciante.Create;
  Agencia := TCliente.Create;
  Vendedor := TVendedor.Create;
  Rubro := TRadioRubro.Create;
  Zona := TZona.Create;
  RadioPrograma := TRadioPrograma.Create;
  RadioEmisora := TRadioEmisora.Create;
  RadioEspacio := TRadioEspacio.Create;
  Reporte := TReporte.Create;
  Parametro := TParametro.Create;
  RadioContratoPrograma := TRadioContratoPrograma.Create;
  RadioContratoFacturaGenerada := TRadioContratoFacturaGenerada.Create;
  RadioContratoFacturaPendiente := TRadioContratoFacturaPendiente.Create;
  RadioContratoDistribucionFactura := TRadioContratoDistribucionFactura.Create;
  FrmActividadSophie.Actividad('Cargando Clientes...');
  DataCliente := Cliente.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Anunciantes...');
  DataAnunciante := Anunciante.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Agencias...');
  DataAgencia := Agencia.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Vendedores...');
  DataVendedor := Vendedor.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Rubros...');
  DataRubro := Rubro.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Zonas...');
  DataZona := Zona.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Resto del N�cleo...');
  DataBuscadorRadioContrato := BuscadorRadioContrato.ObtenerLista;
  DataCliente.First;
  DataAnunciante.First;
  DataAgencia.First;
  DataVendedor.First;
  DataRubro.First;
  DataZona.First;
  DataBuscadorRadioContrato.First;
  DSCliente.DataSet := DataCliente;
  DSAnunciante.DataSet := DataAnunciante;
  DSAgencia.DataSet := DataAgencia;
  DSVendedor.DataSet := DataVendedor;
  DSRubro.DataSet := DataRubro;
  DSZona.DataSet := DataZona;
  ConfigurarRxDBLookupCombo(CmbCodigoCliente,DSCliente,'codigo','codigo;razon_social');
  ConfigurarRxDBLookupCombo(CmbCodigoAnunciante,DSAnunciante,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoAgencia,DSAgencia,'codigo','codigo;razon_social');
  ConfigurarRxDBLookupCombo(CmbCodigoVendedor,DSVendedor,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoRubro,DSRubro,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoZona,DSZona,'codigo','codigo;descripcion');
  DS.DataSet := Data;
  GridDetalle.DataSource := DS;
  DSDistribucionFacturacion.DataSet := DataDistribucionFacturacion;
  GridDistribucionFacturacion.DataSource := DSDistribucionFacturacion;
  FrmActividadSophie.Close;
  //Buscar;
end;

procedure TFrmRadioContrato.GridDistribucionFacturacionDblClick(
  Sender: TObject);
begin
  DetalleDistribucionFacturacion;
end;

procedure TFrmRadioContrato.GridDistribucionFacturacionKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  case Key of
    VK_RETURN:
      begin
        GridDistribucionFacturacionDblClick(Sender);
      end;  
  end;
end;

procedure TFrmRadioContrato.Guardar;
begin
  Obtener;
  if (Agregando) then
  begin
    //Agregando...
    //Guardando Encabezado
    Clase.Insertar;
    TxtNumero.Text := Clase.GetNumero;
    Application.ProcessMessages;
    //Guardando Detalles
    GuardarDetalles;
    MessageBox(Self.Handle, 'El Contrato Ha Sido Agregado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    //Solicitando Impresion del Contrato y Documentos Asociados
    if (MessageBox(Self.Handle, '�Desea Imprimir El Contrato?', PChar(MsgTituloInformacion), MB_ICONQUESTION or MB_YESNO or MB_TOPMOST or MB_APPLMODAL) = mrYes) then
    begin
      with Reporte do
      begin
        AgregarParametro('Numero',Clase.GetNumero);
        AgregarParametro('MontoLetra',NumeroALetras(Clase.GetTotal));
        EmiteReporte('RadioContrato',trDocumento);
      end;
    end;
  end
  else
  begin
    //Editando...
    Clase.Modificar;
    //Actualizando Detalles (Eliminando Anteriores y Cargando los Nuevos)
    //Detalles de la Programacion
    RadioContratoPrograma.EliminarIdRadioContrato(Clase.GetIdRadioContrato);
    //Detalles de la Distribucion de Facturacion
    RadioContratoFacturaPendiente.EliminarIdRadioContrato(Clase.GetIdRadioContrato);
    //Guardando Detalles
    GuardarDetalles;
    MessageBox(Self.Handle, 'El Contrato Ha Sido Editado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end;
  //Actualizando Dataset de Busqueda
  DataBuscadorRadioContrato := BuscadorRadioContrato.ObtenerLista;
end;

procedure TFrmRadioContrato.GuardarDetalles;
begin
  //Recalculando Tarifa, Costo Bruto, Porcentaje de Descuento, Etc.
  with Data do
  begin
    First;
    while (not Eof) do
    begin
      Edit; Post; Next;
    end;    
  end;
  //Guardando Detalle del Programa
  with Data do
  begin
    DisableControls;
    First;
    while (not Eof) do
    begin
      //Guardando Detalle de la Cobranza
      with RadioContratoPrograma do
      begin
        SetIdRadioContrato(Clase.GetIdRadioContrato);
        SetCodigoPrograma(FieldValues['codigo_programa']);
        SetCodigoEspacio(FieldValues['codigo_espacio']);
        SetCunasDiarias(FieldValues['cunas_diarias']);
        SetTotalCunas(FieldValues['total_cunas']);
        SetDuracion(FieldValues['duracion']);
        SetMontoAgencia(FieldValues['monto_agencia']);
        SetPorcentaje(FieldValues['porcentaje']);
        SetMontoEmisora(FieldValues['monto_emisora']);
        SetComision(FieldValues['comision']);
        SetMontoReal(FieldValues['monto_real']);
        SetTarifa(FieldValues['tarifa']);
        SetCostoBruto(FieldValues['costo_bruto']);
        SetPDescuento(FieldValues['p_descuento']);
        Insertar;
      end;
      Next;
    end;
    EnableControls;
  end;
  //Guardando Detalle de la Distribucion de Facturacion
  with DataDistribucionFacturacion do
  begin
    DisableControls;
    First;
    while (not Eof) do
    begin
      //Guardando Facturas Pendientes Por Facturar
      with RadioContratoFacturaPendiente do
      begin
        SetIdRadioContrato(Clase.GetIdRadioContrato);
        SetItem(FieldValues['item']);
        SetTotalItems(FieldValues['total_items']);
        SetFechaIngreso(FieldValues['fecha_ingreso']);
        SetTotal(FieldValues['total']);
        SetFechaInicial(FieldValues['fecha_inicial']);
        SetFechaFinal(FieldValues['fecha_final']);
        SetObservaciones(FieldValues['observaciones']);
        Insertar;
      end;
      Next;
    end;
    EnableControls;
  end;
end;

procedure TFrmRadioContrato.Imprimir;
begin
  if (Length(Trim(TxtNumero.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
  with Reporte do
  begin
    AgregarParametro('Numero',Clase.GetNumero);
    AgregarParametro('MontoLetra',NumeroALetras(Clase.GetTotal));
    EmiteReporte('RadioContrato',trDocumento);
  end;
end;

procedure TFrmRadioContrato.LblCodigoAnuncianteDblClick(Sender: TObject);
var
  Descripcion: string;
begin
  if (MessageBox(Self.Handle, PChar('�Desea Generar Un Anunciante Nuevo?'), PChar(MsgTituloInformacion), MB_ICONQUESTION or MB_YESNO or MB_TOPMOST or MB_APPLMODAL) = mrYes) then
  begin
    //Generando Un Nuevo Anunciante
    if (InputQuery('Sophie - Agregar Anunciante','Indique la Descripci�n del Anunciante',Descripcion)) then
    begin
      //Verificando Que no Exista El Anunciante
      Anunciante.SetDescripcion(Descripcion);
      if (not Anunciante.BuscarDescripcion) then
      begin
        //Agregando El Anunciante
        Anunciante.SetCodigo(Anunciante.ObtenerConsecutivoCodigo);
        Anunciante.Insertar;
      end;
      //Actualizando DataSet Anunciante
      DataAnunciante := Anunciante.ObtenerLista;
      DSAnunciante.DataSet := DataAnunciante;
      CmbCodigoAnunciante.KeyValue := Anunciante.GetCodigo;
    end;
  end;
end;

procedure TFrmRadioContrato.LblCodigoAnuncianteMouseEnter(Sender: TObject);
begin
  LblCodigoAnunciante.Font.Color := clRed;
end;

procedure TFrmRadioContrato.LblCodigoAnuncianteMouseLeave(Sender: TObject);
begin
  LblCodigoAnunciante.Font.Color := clBlack;
end;

procedure TFrmRadioContrato.LblCodigoRubroDblClick(Sender: TObject);
var
  Descripcion: string;
begin
  if (MessageBox(Self.Handle, PChar('�Desea Generar Un Rubro Nuevo?'), PChar(MsgTituloInformacion), MB_ICONQUESTION or MB_YESNO or MB_TOPMOST or MB_APPLMODAL) = mrYes) then
  begin
    //Generando Un Nuevo Rubro
    if (InputQuery('Sophie - Agregar Rubro','Indique la Descripci�n del Rubro',Descripcion)) then
    begin
      //Verificando Que no Exista El Rubro
      Rubro.SetDescripcion(Descripcion);
      if (not Rubro.BuscarDescripcion) then
      begin
        //Agregando El Rubro
        Rubro.SetCodigo(Rubro.ObtenerConsecutivoCodigo);
        Rubro.Insertar;
      end;
      //Actualizando DataSet Rubro
      DataRubro := Rubro.ObtenerLista;
      DSRubro.DataSet := DataRubro;
      CmbCodigoRubro.KeyValue := Rubro.GetCodigo;
    end;
  end;
end;

procedure TFrmRadioContrato.LblCodigoRubroMouseEnter(Sender: TObject);
begin
  LblCodigoRubro.Font.Color := clRed;
end;

procedure TFrmRadioContrato.LblCodigoRubroMouseLeave(Sender: TObject);
begin
  LblCodigoRubro.Font.Color := clBlack;
end;

procedure TFrmRadioContrato.Limpiar;
begin
  TxtNumero.Text := '';
  CmbCodigoCliente.KeyValue := '';
  CmbCodigoAnunciante.KeyValue := '';
  CmbCodigoAgencia.KeyValue := '';
  CmbCodigoVendedor.KeyValue := '';
  CmbCodigoRubro.KeyValue := '';
  DatFechaInicial.Date := Date;
  DatFechaFinal.Date := Date;
  CmbCodigoZona.KeyValue := '';
  TxtNumeroOrden.Text := '';
  DatFechaOrden.Date := Date;
  TxtNroFacturas.Value := 0;
  TxtTotal.Value := 0;
  ChkIntercambio.Checked := False;
  ChkEfectivo.Checked := False;
  ChkVentaNacional.Checked := False;
  ChkCancelado.Checked := False;
  DatFechaCancelado.Date := Date;
  TxtObservaciones.Text := '';
  TxtProducto.Text := '';
  //Status Bar
  SB.Panels[0].Text := '';
  Data.EmptyDataSet;
  DataDistribucionFacturacion.EmptyDataSet;
end;

procedure TFrmRadioContrato.Mover;
begin

end;

procedure TFrmRadioContrato.Obtener;
begin
  with Clase do
  begin
    SetNumero(TxtNumero.Text);
    SetCodigoCliente(CmbCodigoCliente.Text);
    SetCodigoAnunciante(CmbCodigoAnunciante.Text);
    SetCodigoAgencia(CmbCodigoAgencia.Text);
    SetCodigoVendedor(CmbCodigoVendedor.Text);
    SetCodigoZona(CmbCodigoZona.Text);
    SetFechaInicial(DatFechaInicial.Date);
    SetFechaFinal(DatFechaFinal.Date);
    SetCodigoRubro(CmbCodigoRubro.Text);
    SetFechaIngreso(Date);
    SetTotal(TxtTotal.Value);
    SetVentaNacional(ChkVentaNacional.Checked);
    SetNumeroOrden(TxtNumeroOrden.Text);
    SetFechaOrden(DatFechaOrden.Date);
    SetCancelado(ChkCancelado.Checked);
    SetFechaCancelado(DatFechaCancelado.Date);
    SetObservaciones(TxtObservaciones.Text);
    SetProducto(TxtProducto.Text);
    SetNroFacturas(Trunc(TxtNroFacturas.Value));
    SetEfectivo(ChkEfectivo.Checked);
    SetIntercambio(ChkIntercambio.Checked);
    if (Agregando) then
    begin
      SetIdRadioContrato(-1);
    end;
  end;
end;

procedure TFrmRadioContrato.Refrescar;
begin
  //Actualizando Dataset de Busqueda
  DataBuscadorRadioContrato := BuscadorRadioContrato.ObtenerLista;
  //Se Actualizan Los demas Componentes
  DataCliente := Cliente.ObtenerCombo;
  DataAnunciante := Anunciante.ObtenerCombo;
  DataAgencia := Agencia.ObtenerCombo;
  DataVendedor := Vendedor.ObtenerCombo;
  DataRubro := Rubro.ObtenerCombo;
  DataZona := Zona.ObtenerCombo;
  DataCliente.First;
  DataAnunciante.First;
  DataAgencia.First;
  DataVendedor.First;
  DataRubro.First;
  DataZona.First;
  DataBuscadorRadioContrato.First;
  DSCliente.DataSet := DataCliente;
  DSAnunciante.DataSet := DataAnunciante;
  DSAgencia.DataSet := DataAgencia;
  DSVendedor.DataSet := DataVendedor;
  DSRubro.DataSet := DataRubro;
  DSZona.DataSet := DataZona;
end;

function TFrmRadioContrato.Validar: Boolean;
var
  ListaErrores: TStringList;
  Acum: Double;
begin
  ListaErrores := TStringList.Create;
  //Validando Cliente
  if (Length(Trim(CmbCodigoCliente.Text)) = 0) then
  begin
    ListaErrores.Add('El Cliente No Puede Estar En Blanco!!!');
  end;
  //Validando Anunciante
  if (Length(Trim(CmbCodigoAnunciante.Text)) = 0) then
  begin
    ListaErrores.Add('El Anunciante No Puede Estar En Blanco!!!');
  end;
  //Validando Agencia
  if (Length(Trim(CmbCodigoAgencia.Text)) = 0) then
  begin
    ListaErrores.Add('El Agencia No Puede Estar En Blanco!!!');
  end;
  //Validando Vendedor
  if (Length(Trim(CmbCodigoVendedor.Text)) = 0) then
  begin
    ListaErrores.Add('El Vendedor No Puede Estar En Blanco!!!');
  end;
  //Validando Rubro
  if (Length(Trim(CmbCodigoRubro.Text)) = 0) then
  begin
    ListaErrores.Add('El Rubro No Puede Estar En Blanco!!!');
  end;
  //Validando Fecha Inicial y Final
  if (DatFechaInicial.Date >= DatFechaFinal.Date) then
  begin
    ListaErrores.Add('La Fecha Inicial del Contrato debe ser Menor a La Fecha Final!!!');
  end;
  //Validando Zona
  if (Length(Trim(CmbCodigoZona.Text)) = 0) then
  begin
    ListaErrores.Add('La Zona No Puede Estar En Blanco!!!');
  end;
  //Validando Numero de Orden
  if (Length(Trim(TxtNumeroOrden.Text)) = 0) then
  begin
    ListaErrores.Add('El Numero de Orden No Puede Estar En Blanco!!!');
  end;
  //Validando Producto
  if (Length(Trim(TxtProducto.Text)) = 0) then
  begin
    ListaErrores.Add('El Producto No Puede Estar En Blanco!!!');
  end;
  //Validando Nro de Facturas
  if (TxtNroFacturas.Value <= 0) then
  begin
    ListaErrores.Add('El Numero de Cuotas Debe Ser Mayor a Cero!!!');
  end;
  //Validando Monto
  if (TxtTotal.Value <= 0) then
  begin
    ListaErrores.Add('El Valor del Contrato Debe Ser Mayor a Cero!!!');
  end;
  //Validando Detalle (Programaci�n del Contrato)
  if (Data.RecordCount = 0) then
  begin
    ListaErrores.Add('El Detalle De Programaci�n Del Contrato No Puede Estar Vacio!!!');
  end;
  //Validando Distribuci�n de Facturaci�n
  if (DataDistribucionFacturacion.RecordCount = 0) then
  begin
    ListaErrores.Add('La Distribuci�n de Facturaci�n Del Contrato No Puede Estar Vacio!!!');
  end;
  //Validando Sumatoria de Distribuci�n de Facturaci�n
  Acum := 0;
  with DataDistribucionFacturacion do
  begin
    First;
    while (not Eof) do
    begin
      Acum := Acum + FieldValues['total'];
      Next;
    end;
  end;
  if (Acum <> TxtTotal.Value) then
  begin
    ListaErrores.Add('La Distribuci�n de Facturaci�n Del Contrato No Cuadra!!!');
  end;  
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
