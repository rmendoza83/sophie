unit EsquemaPago;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Generico, XPMan, ImgList, DB, Grids, DBGrids, StdCtrls, Buttons,
  ExtCtrls, ComCtrls, ToolWin, Mask, rxToolEdit, rxCurrEdit, ClsEsquemaPago,
  DBClient, Utils;

type
  TFrmEsquemaPago = class(TFrmGenerico)
    LblCodigo: TLabel;
    LblDescripcion: TLabel;
    TxtCodigo: TEdit;
    TxtDescripcion: TEdit;
    Label1: TLabel;
    TxtCredito: TCheckBox;
    Label2: TLabel;
    TxtInicial: TCheckBox;
    Label3: TLabel;
    TxtP_Inicial: TCurrencyEdit;
    Label4: TLabel;
    TxtCuotas: TCheckBox;
    TxtDiasCuota: TCurrencyEdit;
    Label5: TLabel;
    Label6: TLabel;
    TxtCantidadCuota: TCurrencyEdit;
    Label7: TLabel;
    TxtDias: TCheckBox;
    Label8: TLabel;
    TxtCantidadDias: TCurrencyEdit;
    Label9: TLabel;
    TxtInteres: TCheckBox;
    Label10: TLabel;
    TxtTasaInteres: TCurrencyEdit;
    Label11: TLabel;
    TxtTasaOrdinal: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure GrdBusquedaCellClick(Column: TColumn);
  private
    { Private declarations }
    Clase: TEsquemaPago;
    Data: TClientDataSet;
    procedure Agregar;
    procedure Editar;
    procedure Eliminar;
    procedure Guardar;
    procedure Cancelar;
    procedure Refrescar;
    procedure Imprimir;
    procedure Buscar;
    procedure Limpiar;
    procedure Asignar;
    procedure Obtener;
    procedure Mover;
  protected
    function Validar: Boolean; override;
  public
    { Public declarations }
  end;

var
  FrmEsquemaPago: TFrmEsquemaPago;

implementation

{$R *.dfm}

procedure TFrmEsquemaPago.FormCreate(Sender: TObject);
begin
  ProcAgregar := Agregar;
  ProcEditar := Editar;
  ProcEliminar := Eliminar;
  ProcGuardar := Guardar;
  ProcCancelar := Cancelar;
  ProcRefrescar := Refrescar;
  ProcImprimir := Imprimir;
  ProcBuscar := Buscar;
  Clase := TEsquemaPago.Create;
  Buscar;
end;

procedure TFrmEsquemaPago.GrdBusquedaCellClick(Column: TColumn);
begin
  Mover;
end;

procedure TFrmEsquemaPago.Agregar;
begin
  //Verificando
  Limpiar;
  TxtCodigo.SetFocus;
end;

procedure TFrmEsquemaPago.Asignar;
begin
  with Clase do
  begin
    TxtCodigo.Text := GetCodigo;
    TxtDescripcion.Text := GetDescripcion;
    TxtCredito.Checked := GetCredito;
    TxtInicial.Checked := GetInicial;
    TxtP_Inicial.Value := GetPInicial;
    TxtCuotas.Checked := GetCuotas;
    TxtDiasCuota.Value := GetDiasCuota;
    TxtCantidadCuota.Value := GetCantidadCuotas;
    TxtDias.Checked := GetDias;
    TxtCantidadDias.Value := GetCantidadDias;
    TxtInteres.Checked := GetInteres;
    TxtTasaInteres.Value := GetTasaInteres;
    TxtTasaOrdinal.Checked := GetTasaOrdinal;
  end;
end;

procedure TFrmEsquemaPago.Buscar;
begin
  Screen.Cursor := crSQLWait;
  if (Length(Trim(TxtCriterio.Text)) > 0) then
  begin
    Data := Clase.ObtenerListaCriterio(Trim(TxtCriterio.Text));
  end
  else
  begin
    Data := Clase.ObtenerLista;
  end;
  Data.First;
  DS.DataSet := Data;
  GrdBusqueda.Refresh;
  Screen.Cursor := crDefault;
end;

procedure TFrmEsquemaPago.Cancelar;
begin
  TxtCodigo.Enabled := True;
end;

procedure TFrmEsquemaPago.Editar;
begin
  //Se Busca el Elemento
  Clase.SetCodigo(Data.FieldValues['codigo']);
  Clase.Buscar;
  Asignar;
  TxtCodigo.Enabled := False;
  TxtDescripcion.SetFocus;
end;

procedure TFrmEsquemaPago.Eliminar;
begin
  //Se Busca el Elemento
  Clase.SetCodigo(Data.FieldValues['codigo']);
  Clase.Buscar;
  Clase.Eliminar;
  MessageBox(Self.Handle, 'El Esquema de Pago Ha Sido Eliminado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  Refrescar;
  Limpiar;
end;

procedure TFrmEsquemaPago.Guardar;
begin
  if (Agregando) then
  begin
    //Agregando...
    //Se Busca el Codigo
    Clase.SetCodigo(TxtCodigo.Text);
    if (Clase.Buscar) then
    begin
      MessageBox(Self.Handle, 'El C�digo Ya Existe!!!', PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      TxtCodigo.SetFocus;
      Abort;
    end
    else
    begin
      Obtener;
      Clase.Insertar;
      MessageBox(Self.Handle, 'El Esquema de Pago  Ha Sido Agregado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    end;
  end
  else
  begin
    //Editando...
    Obtener;
    Clase.Modificar;
    MessageBox(Self.Handle, 'El Esquema de Pago  Ha Sido Editado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    TxtCodigo.Enabled := True;
  end;
  Refrescar;
  Limpiar;
end;

procedure TFrmEsquemaPago.Imprimir;
begin

end;

procedure TFrmEsquemaPago.Limpiar;
begin
  TxtCodigo.Text := '';
  TxtDescripcion.Text := '';
  TxtCredito.Checked := False;
  TxtInicial.Checked := False;
  TxtP_Inicial.Value := 0.00;
  TxtCuotas.Checked := False;
  TxtDiasCuota.Value := 0;
  TxtCantidadCuota.Value := 0;
  TxtDias.Checked := False;
  TxtCantidadDias.Value := 0;
  TxtInteres.Checked := False;
  TxtTasaInteres.Value := 0.00;
  TxtTasaOrdinal.Checked := False;
end;

procedure TFrmEsquemaPago.Mover;
var
  DSTemp: TDataSet;
begin
  if (DS.DataSet.RecordCount > 0) then
  begin
    DSTemp := DS.DataSet;
    with Clase do
    begin
      SetCodigo(DSTemp.FieldValues['codigo']);
      SetDescripcion(DSTemp.FieldValues['descripcion']);
      SetCredito(DSTemp.FieldValues['credito']);
      SetInicial(DSTemp.FieldValues['inicial']);
      SetPInicial(DSTemp.FieldValues['p_inicial']);
      SetCuotas(DSTemp.FieldValues['cuotas']);
      SetDiasCuota(DSTemp.FieldValues['dias_cuota']);
      SetCantidadCuotas(DSTemp.FieldValues['cantidad_cuotas']);
      SetDias(DSTemp.FieldValues['dias']);
      SetCantidadDias(DSTemp.FieldValues['cantidad_dias']);
      SetInteres(DSTemp.FieldValues['interes']);
      SetTasaInteres(DSTemp.FieldValues['tasa_interes']);
      SetTasaOrdinal(DSTemp.FieldValues['tasa_ordinal']);
    end;
    Asignar;
  end
  else
  begin
    Limpiar;
  end;
end;

procedure TFrmEsquemaPago.Obtener;
begin
  with Clase do
  begin
    SetCodigo(TxtCodigo.Text);
    SetDescripcion(TxtDescripcion.Text);
    SetCredito(TxtCredito.Checked);
    SetInicial(TxtInicial.Checked);
    SetPInicial(TxtP_Inicial.Value);
    SetCuotas(TxtCuotas.Checked);
    SetDiasCuota(Trunc(TxtDiasCuota.Value));
    SetCantidadCuotas(Trunc(TxtCantidadCuota.Value));
    SetDias(TxtDias.Checked);
    SetCantidadDias(Trunc(TxtCantidadDias.Value));
    SetInteres(TxtInteres.Checked);
    SetTasaInteres(Trunc(TxtTasaInteres.Value));
    SetTasaOrdinal(TxtTasaOrdinal.Checked);
  end;
end;


procedure TFrmEsquemaPago.Refrescar;
begin
  TxtCriterio.Text := '';
  Buscar;
end;

function TFrmEsquemaPago.Validar: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  (* Validando Informacion del Esquema de Pago *)
  //Validando Codigo
  if (Length(Trim(TxtCodigo.Text)) = 0) then
  begin
    ListaErrores.Add('El C�digo No Puede Estar En Blanco!!!');
  end;
  //Validando Descripcion
  if (Length(Trim(TxtDescripcion.Text)) = 0) then
  begin
    ListaErrores.Add('La Descripci�n No Puede Estar En Blanco!!!');
  end;
  //Validando Inicial
  if (TxtInicial.Checked) then
  begin
    if (TxtP_Inicial.Value) = 0 then
      begin
        ListaErrores.Add('El Porcentaje Inicial debe Ser Mayor que 0 !!!');
      end;
    if (TxtP_Inicial.Value) < 0 then
      begin
        ListaErrores.Add('El Porcentaje Inicial debe Ser Positivo !!!');
      end;
  end;
  //Validando Cuotas
  if (TxtCuotas.Checked) then
  begin
    if (TxtDiasCuota.Value) = 0 then
      begin
        ListaErrores.Add('Los D�as de Las Cuotas deben Ser Mayor que 0 !!!');
      end;
    if (TxtDiasCuota.Value) < 0 then
      begin
        ListaErrores.Add('Los D�as de Las Cuotas deben Ser Positivos !!!');
      end;
    if (TxtCantidadCuota.Value) = 0 then
      begin
        ListaErrores.Add('La Cantidad de Cuotas debe Ser Mayor que 0 !!!');
      end;
    if (TxtCantidadCuota.Value) < 0 then
      begin
        ListaErrores.Add('La Cantidad de Cuotas debe Ser Positivo !!!');
      end;
  end;
  // Validando Dias
  if (TxtDias.Checked) then
  begin
    if (TxtCantidadDias.Value) = 0 then
      begin
        ListaErrores.Add('La Cantidad de D�as debe Ser Mayor que 0 !!!');
      end;
    if (TxtCantidadDias.Value) < 0 then
      begin
        ListaErrores.Add('La Cantidad de D�as debe Ser Positivo !!!');
      end;
  end;
  // Validando Intereses
  if (TxtInteres.Checked) then
  begin
    if (TxtTasaInteres.Value) = 0 then
      begin
        ListaErrores.Add('La Tasa de Inter�s debe Ser Mayor que 0 !!!');
      end;
    if (TxtTasaInteres.Value) < 0 then
      begin
        ListaErrores.Add('La Tasa de Inter�s debe Ser Positivo !!!');
      end;
  end;
    (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;
end.
