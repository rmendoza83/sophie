unit FormaPago;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Generico, XPMan, ImgList, DB, Grids, DBGrids, StdCtrls, Buttons,
  ExtCtrls, ComCtrls, ToolWin, ClsFormaPago, DBClient, Utils, DBCtrls, RxLookup,
  ClsTipoOperacionBancaria;

type
  TFrmFormaPago = class(TFrmGenerico)
    LblCodigo: TLabel;
    LblDescripcion: TLabel;
    TxtCodigo: TEdit;
    TxtDescripcion: TEdit;
    Label1: TLabel;
    TxtCodigoBanco: TCheckBox;
    Label2: TLabel;
    TxtCheque: TCheckBox;
    TxtAnticipo: TCheckBox;
    Label3: TLabel;
    TxtNc: TCheckBox;
    Label4: TLabel;
    TxtNd: TCheckBox;
    Label5: TLabel;
    TxtCobranza: TCheckBox;
    Label6: TLabel;
    TxtPago: TCheckBox;
    Label7: TLabel;
    LblOperacionBancaria: TLabel;
    ChkCodigoTipoOperacionBancaria: TCheckBox;
    LblCodigoTipoOperacionBancaria: TLabel;
    CmbCodigoTipoOperacionBancaria: TRxDBLookupCombo;
    TxtCodigoTipoOperacionBancaria: TDBText;
    DSTipoOperacionBancaria: TDataSource;
    procedure ChkCodigoTipoOperacionBancariaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure GrdBusquedaCellClick(Column: TColumn);
  private
    { Private declarations }
    Clase: TFormaPago;
    TipoOperacionBancaria: TTipoOperacionBancaria;
    Data: TClientDataSet;
    DataTipoOperacionBancaria: TClientDataSet;
    procedure Agregar;
    procedure Editar;
    procedure Eliminar;
    procedure Guardar;
    procedure Cancelar;
    procedure Refrescar;
    procedure Imprimir;
    procedure Buscar;
    procedure Limpiar;
    procedure Asignar;
    procedure Obtener;
    procedure Mover;
  protected
    function Validar: Boolean; override;
  public
    { Public declarations }
  end;

var
  FrmFormaPago: TFrmFormaPago;

implementation

uses ClsGenericoBD;

{$R *.dfm}

procedure TFrmFormaPago.FormCreate(Sender: TObject);
begin
  ProcAgregar := Agregar;
  ProcEditar := Editar;
  ProcEliminar := Eliminar;
  ProcGuardar := Guardar;
  ProcCancelar := Cancelar;
  ProcRefrescar := Refrescar;
  ProcImprimir := Imprimir;
  ProcBuscar := Buscar;
  Clase := TFormaPago.Create;
  //Inicializando DataSets Secundarios
  TipoOperacionBancaria := TTipoOperacionBancaria.Create;
  DataTipoOperacionBancaria := TipoOperacionBancaria.ObtenerLista;
  DataTipoOperacionBancaria.First;
  DSTipoOperacionBancaria.DataSet := DataTipoOperacionBancaria;
  TxtCodigoTipoOperacionBancaria.DataSource := DSTipoOperacionBancaria;
  TxtCodigoTipoOperacionBancaria.DataField := 'descripcion';
  ConfigurarRxDBLookupCombo(CmbCodigoTipoOperacionBancaria,DSTipoOperacionBancaria,'codigo','codigo;descripcion');
  Buscar;
end;

procedure TFrmFormaPago.GrdBusquedaCellClick(Column: TColumn);
begin
  Mover;
end;

procedure TFrmFormaPago.Agregar;
begin
  //Verificando
  Limpiar;
  TxtCodigo.SetFocus;
end;

procedure TFrmFormaPago.Asignar;
begin
  with Clase do
  begin
    TxtCodigo.Text := GetCodigo;
    TxtDescripcion.Text := GetDescripcion;
    TxtCodigoBanco.Checked := GetCodigoBanco;
    TxtCheque.Checked := GetCheque;
    TxtNc.Checked := GetNc;
    TxtNd.Checked := GetNd;
    TxtCobranza.Checked := GetCobranza;
    TxtPago.Checked := GetPago;
    TxtAnticipo.Checked := GetAnticipo;
    ChkCodigoTipoOperacionBancaria.Checked := GetOperacionBancaria;
    CmbCodigoTipoOperacionBancaria.KeyValue := GetCodigoTipoOperacionBancaria;
    ChkCodigoTipoOperacionBancariaClick(nil);
  end;
end;

procedure TFrmFormaPago.Buscar;
begin
  Screen.Cursor := crSQLWait;
  if (Length(Trim(TxtCriterio.Text)) > 0) then
  begin
    Data := Clase.ObtenerListaCriterio(Trim(TxtCriterio.Text));
  end
  else
  begin
    Data := Clase.ObtenerLista;
  end;
  Data.First;
  DS.DataSet := Data;
  GrdBusqueda.Refresh;
  Screen.Cursor := crDefault;
end;

procedure TFrmFormaPago.Cancelar;
begin
  TxtCodigo.Enabled := True;
end;

procedure TFrmFormaPago.ChkCodigoTipoOperacionBancariaClick(Sender: TObject);
begin
  LblCodigoTipoOperacionBancaria.Enabled := ChkCodigoTipoOperacionBancaria.Checked;
  CmbCodigoTipoOperacionBancaria.Enabled := ChkCodigoTipoOperacionBancaria.Checked;
  TxtCodigoTipoOperacionBancaria.Enabled := ChkCodigoTipoOperacionBancaria.Checked;
end;

procedure TFrmFormaPago.Editar;
begin
  //Se Busca el Elemento
  Clase.SetCodigo(Data.FieldValues['codigo']);
  Clase.Buscar;
  Asignar;
  TxtCodigo.Enabled := False;
  TxtDescripcion.SetFocus;
end;

procedure TFrmFormaPago.Eliminar;
begin
  //Se Busca el Elemento
  Clase.SetCodigo(Data.FieldValues['codigo']);
  Clase.Buscar;
  Clase.Eliminar;
  MessageBox(Self.Handle, 'La Forma de Pago Ha Sido Eliminada Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  Refrescar;
  Limpiar;
end;

procedure TFrmFormaPago.Guardar;
begin
  if (Agregando) then
  begin
    //Agregando...
    //Se Busca el Codigo
    Clase.SetCodigo(TxtCodigo.Text);
    if (Clase.Buscar) then
    begin
      MessageBox(Self.Handle, 'El C�digo Ya Existe!!!', PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      TxtCodigo.SetFocus;
      Abort;
    end
    else
    begin
      Obtener;
      Clase.Insertar;
      MessageBox(Self.Handle, 'La Forma de Pago Ha Sido Agregada Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    end;
  end
  else
  begin
    //Editando...
    Obtener;
    Clase.Modificar;
    MessageBox(Self.Handle, 'La Forma de Pago Ha Sido Editada Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    TxtCodigo.Enabled := True;
  end;
  Refrescar;
  Limpiar;
end;

procedure TFrmFormaPago.Imprimir;
begin

end;

procedure TFrmFormaPago.Limpiar;
begin
  TxtCodigo.Text := '';
  TxtDescripcion.Text := '';
  TxtCodigoBanco.Checked := False;
  TxtCheque.Checked := False;
  TxtNc.Checked := False;
  TxtNd.Checked := False;
  TxtCobranza.Checked := False;
  TxtPago.Checked := False;
  TxtAnticipo.Checked := False;
  ChkCodigoTipoOperacionBancaria.Checked := False;
  ChkCodigoTipoOperacionBancariaClick(nil);
end;

procedure TFrmFormaPago.Mover;
var
  DSTemp: TDataSet;
begin
  if (DS.DataSet.RecordCount > 0) then
  begin
    DSTemp := DS.DataSet;
    with Clase do
    begin
      SetCodigo(DSTemp.FieldValues['codigo']);
      SetDescripcion(DSTemp.FieldValues['descripcion']);
      SetCodigoBanco(DSTemp.FieldValues['codigo_banco']);
      SetCheque(DSTemp.FieldValues['cheque']);
      SetNc(DSTemp.FieldValues['nc']);
      SetNd(DSTemp.FieldValues['nd']);
      SetCobranza(DSTemp.FieldValues['cobranza']);
      SetPago(DSTemp.FieldValues['pago']);
      SetAnticipo(DSTemp.FieldValues['anticipo']);
      SetOperacionBancaria(DSTemp.FieldValues['operacion_bancaria']);
      SetCodigoTipoOperacionBancaria(DSTemp.FieldValues['codigo_tipo_operacion_bancaria']);
    end;
    Asignar;
  end
  else
  begin
    Limpiar;
  end;
end;

procedure TFrmFormaPago.Obtener;
begin
  with Clase do
  begin
    SetCodigo(TxtCodigo.Text);
    SetDescripcion(TxtDescripcion.Text);
    SetCodigoBanco(TxtCodigoBanco.Checked);
    SetCheque(TxtCheque.Checked);
    SetNc(TxtNc.Checked);
    SetNd(TxtNd.Checked);
    SetCobranza(TxtCobranza.Checked);
    SetPago(TxtPago.Checked);
    SetOperacionBancaria(ChkCodigoTipoOperacionBancaria.Checked);
    SetCodigoTipoOperacionBancaria(CmbCodigoTipoOperacionBancaria.Value);
  end;
end;

procedure TFrmFormaPago.Refrescar;
begin
  TxtCriterio.Text := '';
  Buscar;
end;

function TFrmFormaPago.Validar: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  (* Validando Informacion de la Forma de Pago *)
  //Validando Codigo
  if (Length(Trim(TxtCodigo.Text)) = 0) then
  begin
    ListaErrores.Add('El C�digo No Puede Estar En Blanco!!!');
  end;
  //Validando Descripcion
  if (Length(Trim(TxtDescripcion.Text)) = 0) then
  begin
    ListaErrores.Add('La Descripci�n No Puede Estar En Blanco!!!');
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
