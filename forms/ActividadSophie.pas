unit ActividadSophie;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls;

type
  TFrmActividadSophie = class(TForm)
    PanelActividad: TPanel;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Actividad(S: string);
    procedure Fin;
  end;

var
  FrmActividadSophie: TFrmActividadSophie;

implementation

{$R *.dfm}

{ TFrmActividadSophie }

procedure TFrmActividadSophie.Actividad(S: string);
begin
  PanelActividad.Caption := S;
  Application.ProcessMessages;
end;

procedure TFrmActividadSophie.Fin;
begin
  PanelActividad.Caption := '';
  Close;
end;

initialization

  FrmActividadSophie := TFrmActividadSophie.Create(Application);

end.
