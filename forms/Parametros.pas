unit Parametros;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, ComCtrls, Utils, Grids, DB, DBGrids,
  DBClient, ClsParametro;

type
  TFrmParametros = class(TForm)
    PageParametros: TPageControl;
    PanelBotones: TPanel;
    BtnCerrar: TBitBtn;
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure BtnCerrarClick(Sender: TObject);
  private
    { Private declarations }
    Parametro: TParametro;
    AData: array of TClientDataSet;
    ADS: array of TDataSource;
    AGrid: array of TDBGrid;
    ATabs: array of TTabSheet;
    procedure ActualizarData;
  public
    { Public declarations }
    procedure DobleClickGrid(Sender: TObject);
  end;

var
  FrmParametros: TFrmParametros;

implementation

uses
  ParametroDetalle, ClsGenericoBD;

{$R *.dfm}

procedure TFrmParametros.ActualizarData;
var
  i, j: Integer;
  DataCategorias: TClientDataSet;
begin
  DataCategorias := Parametro.ObtenerCategorias;
  DataCategorias.First;
  for i := 0 to PageParametros.PageCount - 1 do
  begin
    AData[i] := Parametro.ObtenerParametros(DataCategorias.FieldValues['categoria']);
    ADS[i].DataSet := AData[i];
    //Ajustando Caracteristicas del Grid
    for j := 0 to AGrid[i].Columns.Count - 1 do
    begin
      AGrid[i].Columns[j].Width := 180;
    end;
    DataCategorias.Next;
  end;
end;

procedure TFrmParametros.BtnCerrarClick(Sender: TObject);
begin
  Close;
end;

procedure TFrmParametros.DobleClickGrid(Sender: TObject);
var
  idx: Integer;
begin
  idx := PageParametros.ActivePageIndex;
  FrmParametroDetalle := TFrmParametroDetalle.CrearEditando(Self,AData[idx].FieldValues['nombre_parametro'],ATabs[idx].Caption);
  FrmParametroDetalle.ShowModal;
  FreeAndNil(FrmParametroDetalle);
  ActualizarData;
end;

procedure TFrmParametros.FormCreate(Sender: TObject);
var
  i, j, n: Integer;
  DataCategorias: TClientDataSet;
begin
  //Instanciando Clase Parametro
  Parametro := TParametro.Create;
  //Obteniendo Nro de Categorias para la Generacion del Formulario de Parametros
  n := Parametro.ObtenerTotalCategorias;
  DataCategorias := Parametro.ObtenerCategorias;
  DataCategorias.First;
  //Asignando Dimension de los Arreglos Dinamicos
  SetLength(AData,n);
  SetLength(ADS,n);
  SetLength(AGrid,n);
  SetLength(ATabs,n);
  //Generando Dinamicamente Formulario de Parametros
  for i := 0 to n - 1 do
  begin
    //Creando Tab en el Page
    ATabs[i] := TTabSheet.Create(Self);
    ATabs[i].PageControl := PageParametros;
    ATabs[i].Caption := DataCategorias.FieldValues['categoria'];
    //Creando el DataSet
    AData[i] := Parametro.ObtenerParametros(DataCategorias.FieldValues['categoria']);
    //Creando el DataSource
    ADS[i] := TDataSource.Create(Self);
    ADS[i].DataSet := AData[i];
    //Creando el DBGrid
    AGrid[i] := TDBGrid.Create(Self);
    AGrid[i].Parent := ATabs[i];
    AGrid[i].Align := alClient;
    AGrid[i].DataSource := ADS[i];
    AGrid[i].ReadOnly := True;
    AGrid[i].OnDblClick := DobleClickGrid;
    //Ajustando Caracteristicas del Grid
    for j := 0 to AGrid[i].Columns.Count - 1 do
    begin
      AGrid[i].Columns[j].Width := 180;
    end;
    //Siguiente Categoria
    DataCategorias.Next;
  end;
end;

procedure TFrmParametros.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  idx: Integer;
begin
  idx := PageParametros.ActivePageIndex;
  case Key of
    VK_INSERT:
    begin
      FrmParametroDetalle := TFrmParametroDetalle.CrearAgregando(Self,ATabs[idx].Caption);
      FrmParametroDetalle.ShowModal;
      FreeAndNil(FrmParametroDetalle);
      ActualizarData;
    end;
    VK_DELETE:
    begin
      if (MessageBox(Self.Handle, '�Esta Seguro?', PChar(MsgTituloInformacion), MB_ICONQUESTION or MB_YESNO or MB_TOPMOST or MB_TASKMODAL) = mrYes) then
      begin
        with Parametro do
        begin
          SetNombreParametro(AData[idx].FieldValues['nombre_parametro']);
          Eliminar;
          ActualizarData;
        end;      
      end;
    end;
  end;
end;

end.
