unit AgregarDetalleRetencionIslr;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GenericoAgregarDetalle, StdCtrls, Buttons, ExtCtrls,
  ClsBusquedaDocumentoRetencionIslr, ClsAnticipoProveedor,
  ClsFacturacionProveedor, ClsNotaDebitoProveedor, ClsDetRetencionIslr,
  ClsTipoRetencion, ClsProveedor, GenericoBuscador, DBClient, Utils, Mask,
  rxToolEdit, rxCurrEdit, DBCtrls, RxLookup, DB;

type
  TFrmAgregarDetalleRetencionIslr = class(TFrmGenericoAgregarDetalle)
    LblNumero: TLabel;
    LblProveedor: TLabel;
    TxtDesde: TEdit;
    LblDesde: TLabel;
    LblDescripcionDesde: TLabel;
    TxtNumero: TEdit;
    TxtCodigoProveedor: TEdit;
    GrpRetenciones: TGroupBox;
    LblTotalRetencion: TLabel;
    CmbCodigoRetencion1: TRxDBLookupCombo;
    BtnRetencion1: TBitBtn;
    TxtMontoRetencion1: TCurrencyEdit;
    CmbCodigoRetencion2: TRxDBLookupCombo;
    BtnRetencion2: TBitBtn;
    TxtMontoRetencion2: TCurrencyEdit;
    CmbCodigoRetencion3: TRxDBLookupCombo;
    BtnRetencion3: TBitBtn;
    TxtMontoRetencion3: TCurrencyEdit;
    CmbCodigoRetencion4: TRxDBLookupCombo;
    BtnRetencion4: TBitBtn;
    TxtMontoRetencion4: TCurrencyEdit;
    CmbCodigoRetencion5: TRxDBLookupCombo;
    BtnRetencion5: TBitBtn;
    TxtMontoRetencion5: TCurrencyEdit;
    TxtTotalRetencion: TCurrencyEdit;
    LblMontoBase: TLabel;
    DSTipoRetencion1: TDataSource;
    DSTipoRetencion2: TDataSource;
    DSTipoRetencion3: TDataSource;
    DSTipoRetencion4: TDataSource;
    DSTipoRetencion5: TDataSource;
    LblRazonSocialProveedor: TLabel;
    BtnBorrarRetencion1: TBitBtn;
    BtnBorrarRetencion2: TBitBtn;
    BtnBorrarRetencion3: TBitBtn;
    BtnBorrarRetencion4: TBitBtn;
    BtnBorrarRetencion5: TBitBtn;
    procedure BtnBorrarRetencion5Click(Sender: TObject);
    procedure BtnBorrarRetencion4Click(Sender: TObject);
    procedure BtnBorrarRetencion3Click(Sender: TObject);
    procedure BtnBorrarRetencion2Click(Sender: TObject);
    procedure BtnBorrarRetencion1Click(Sender: TObject);
    procedure CmbCodigoRetencion5CloseUp(Sender: TObject);
    procedure CmbCodigoRetencion4CloseUp(Sender: TObject);
    procedure CmbCodigoRetencion3CloseUp(Sender: TObject);
    procedure CmbCodigoRetencion2CloseUp(Sender: TObject);
    procedure CmbCodigoRetencion1CloseUp(Sender: TObject);
    procedure TxtMontoRetencion5Change(Sender: TObject);
    procedure TxtMontoRetencion4Change(Sender: TObject);
    procedure TxtMontoRetencion3Change(Sender: TObject);
    procedure TxtMontoRetencion2Change(Sender: TObject);
    procedure TxtMontoRetencion1Change(Sender: TObject);
    procedure BtnRetencion5Click(Sender: TObject);
    procedure BtnRetencion4Click(Sender: TObject);
    procedure BtnRetencion3Click(Sender: TObject);
    procedure BtnRetencion2Click(Sender: TObject);
    procedure BtnRetencion1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtnBuscarClick(Sender: TObject);
  private
    { Private declarations }
    ArrayVariant: TArrayVariant;
    DetRetencionIslr: TDetRetencionIslr;
    TipoRetencion: TTipoRetencion;
    AnticipoProveedor: TAnticipoProveedor;
    FacturaProveedor: TFacturacionProveedor;
    NotaDebitoProveedor: TNotaDebitoProveedor;
    Proveedor: TProveedor;
    DataTipoRetencion1: TClientDataSet;
    DataTipoRetencion2: TClientDataSet;
    DataTipoRetencion3: TClientDataSet;
    DataTipoRetencion4: TClientDataSet;
    DataTipoRetencion5: TClientDataSet;
    procedure CalcularRetencion(Ds: TClientDataSet; CurrencyEdit: TCurrencyEdit);
    procedure CalcularTotalRetencion;
  protected
    { Protected declarations }
    function Buscar: Boolean; override;
    procedure Agregar; override;
    procedure Limpiar; override;
    function Validar: Boolean;
  public
    { Public declarations }
    CodigoProveedor: string;
    BusquedaDocumentoRetencionIslr: TBusquedaDocumentoRetencionIslr;
  end;

var
  FrmAgregarDetalleRetencionIslr: TFrmAgregarDetalleRetencionIslr;

implementation

{$R *.dfm}

procedure TFrmAgregarDetalleRetencionIslr.Agregar;
begin
  if (Validar) then
  begin
    with Data do
    begin
      Append;
      FieldValues['desde'] := BusquedaDocumentoRetencionIslr.GetDesde;
      FieldValues['numero_documento'] := BusquedaDocumentoRetencionIslr.GetNumero;
      if (BusquedaDocumentoRetencionIslr.GetDesde = 'AP') then
      begin //Anticipo a Proveedor
        AnticipoProveedor.SetNumero(BusquedaDocumentoRetencionIslr.GetNumero);
        AnticipoProveedor.BuscarNumero;
        FieldValues['monto_base'] := AnticipoProveedor.GetMontoNeto;
        FieldValues['p_iva'] := 0;
        FieldValues['impuesto'] := 0;
      end
      else
      begin
        if (BusquedaDocumentoRetencionIslr.GetDesde = 'NDP') then
        begin //Nota Debito Proveedor
          NotaDebitoProveedor.SetNumero(BusquedaDocumentoRetencionIslr.GetNumero);
          NotaDebitoProveedor.BuscarNumero;
          FieldValues['monto_base'] := NotaDebitoProveedor.GetMontoNeto;
          FieldValues['p_iva'] := NotaDebitoProveedor.GetPIva;
          FieldValues['impuesto'] := NotaDebitoProveedor.GetImpuesto;
        end
        else
        begin //Factura Proveedor
          FacturaProveedor.SetNumero(BusquedaDocumentoRetencionIslr.GetNumero);
          FacturaProveedor.BuscarNumero;
          FieldValues['monto_base'] := FacturaProveedor.GetMontoNeto;
          FieldValues['p_iva'] := FacturaProveedor.GetPIva;
          FieldValues['impuesto'] := FacturaProveedor.GetImpuesto;
        end;
      end;
      FieldValues['codigo_retencion_1'] := CmbCodigoRetencion1.Text;
      FieldValues['monto_retencion_1'] := TxtMontoRetencion1.Value;
      if (TxtMontoRetencion1.Value > 0) then
      begin
        FieldValues['descripcion_retencion_1'] := DataTipoRetencion1.FieldValues['descripcion'];
      end
      else
      begin
        FieldValues['descripcion_retencion_1'] := '';
      end;
      FieldValues['codigo_retencion_2'] := CmbCodigoRetencion2.Text;
      FieldValues['monto_retencion_2'] := TxtMontoRetencion2.Value;
      if (TxtMontoRetencion2.Value > 0) then
      begin
        FieldValues['descripcion_retencion_2'] := DataTipoRetencion2.FieldValues['descripcion'];
      end
      else
      begin
        FieldValues['descripcion_retencion_2'] := '';
      end;
      FieldValues['codigo_retencion_3'] := CmbCodigoRetencion3.Text;
      FieldValues['monto_retencion_3'] := TxtMontoRetencion3.Value;
      if (TxtMontoRetencion3.Value > 0) then
      begin
        FieldValues['descripcion_retencion_3'] := DataTipoRetencion3.FieldValues['descripcion'];
      end
      else
      begin
        FieldValues['descripcion_retencion_3'] := '';
      end;
      FieldValues['codigo_retencion_4'] := CmbCodigoRetencion4.Text;
      FieldValues['monto_retencion_4'] := TxtMontoRetencion4.Value;
      if (TxtMontoRetencion4.Value > 0) then
      begin
        FieldValues['descripcion_retencion_4'] := DataTipoRetencion4.FieldValues['descripcion'];
      end
      else
      begin
        FieldValues['descripcion_retencion_4'] := '';
      end;
      FieldValues['codigo_retencion_5'] := CmbCodigoRetencion5.Text;
      FieldValues['monto_retencion_5'] := TxtMontoRetencion5.Value;
      if (TxtMontoRetencion5.Value > 0) then
      begin
        FieldValues['descripcion_retencion_5'] := DataTipoRetencion5.FieldValues['descripcion'];
      end
      else
      begin
        FieldValues['descripcion_retencion_5'] := '';
      end;
      Post;
    end;
    inherited;
  end
  else
  begin
    ModalResult := mrNone;
  end;
end;

procedure TFrmAgregarDetalleRetencionIslr.BtnBorrarRetencion1Click(
  Sender: TObject);
begin
  CmbCodigoRetencion1.KeyValue := '';
  TxtMontoRetencion1.Text := '';
end;

procedure TFrmAgregarDetalleRetencionIslr.BtnBorrarRetencion2Click(
  Sender: TObject);
begin
  CmbCodigoRetencion2.KeyValue := '';
  TxtMontoRetencion2.Text := '';
end;

procedure TFrmAgregarDetalleRetencionIslr.BtnBorrarRetencion3Click(
  Sender: TObject);
begin
  CmbCodigoRetencion3.KeyValue := '';
  TxtMontoRetencion3.Text := '';
end;

procedure TFrmAgregarDetalleRetencionIslr.BtnBorrarRetencion4Click(
  Sender: TObject);
begin
  CmbCodigoRetencion4.KeyValue := '';
  TxtMontoRetencion4.Text := '';
end;

procedure TFrmAgregarDetalleRetencionIslr.BtnBorrarRetencion5Click(
  Sender: TObject);
begin
  CmbCodigoRetencion5.KeyValue := '';
  TxtMontoRetencion5.Text := '';
end;

procedure TFrmAgregarDetalleRetencionIslr.BtnBuscarClick(Sender: TObject);
var
  Condicion: string;
begin
  inherited;
  Condicion := '"codigo_proveedor" = ' + QuotedStr(CodigoProveedor);
  if (Buscador(Self,BusquedaDocumentoRetencionIslr,'desde;numero;codigo_proveedor','Buscar Documento',nil,nil,True,ArrayVariant,Condicion)) then
  begin
    Buscar;
  end
  else
  begin
    SetLength(ArrayVariant,0);
  end;
end;

procedure TFrmAgregarDetalleRetencionIslr.BtnRetencion1Click(Sender: TObject);
begin
  if (Length(Trim(TxtDesde.Text)) > 0) then
  begin
    if (Buscador(Self,TipoRetencion,'codigo','Buscar Tipo de Retención',nil,DataTipoRetencion1,'retencion_iva = false')) then
    begin
      CmbCodigoRetencion1.KeyValue := DataTipoRetencion1.FieldValues['codigo'];
      CalcularRetencion(DataTipoRetencion1,TxtMontoRetencion1);
    end
    else
    begin
      if (Length(Trim(CmbCodigoRetencion1.Text)) = 0) then
      begin
        TxtMontoRetencion1.Text := '';
      end;      
    end;
  end;
end;

procedure TFrmAgregarDetalleRetencionIslr.BtnRetencion2Click(Sender: TObject);
begin
  if (Length(Trim(TxtDesde.Text)) > 0) then
  begin
    if (Buscador(Self,TipoRetencion,'codigo','Buscar Tipo de Retención',nil,DataTipoRetencion2,'retencion_iva = false')) then
    begin
      CmbCodigoRetencion2.KeyValue := DataTipoRetencion2.FieldValues['codigo'];
      CalcularRetencion(DataTipoRetencion2,TxtMontoRetencion2);
    end
    else
    begin
      if (Length(Trim(CmbCodigoRetencion2.Text)) = 0) then
      begin
        TxtMontoRetencion2.Text := '';
      end;      
    end;
  end;
end;

procedure TFrmAgregarDetalleRetencionIslr.BtnRetencion3Click(Sender: TObject);
begin
  if (Length(Trim(TxtDesde.Text)) > 0) then
  begin
    if (Buscador(Self,TipoRetencion,'codigo','Buscar Tipo de Retención',nil,DataTipoRetencion3,'retencion_iva = false')) then
    begin
      CmbCodigoRetencion3.KeyValue := DataTipoRetencion3.FieldValues['codigo'];
      CalcularRetencion(DataTipoRetencion3,TxtMontoRetencion3);
    end
    else
    begin
      if (Length(Trim(CmbCodigoRetencion3.Text)) = 0) then
      begin
        TxtMontoRetencion3.Text := '';
      end;
    end;
  end;
end;

procedure TFrmAgregarDetalleRetencionIslr.BtnRetencion4Click(Sender: TObject);
begin
  if (Length(Trim(TxtDesde.Text)) > 0) then
  begin
    if (Buscador(Self,TipoRetencion,'codigo','Buscar Tipo de Retención',nil,DataTipoRetencion4,'retencion_iva = false')) then
    begin
      CmbCodigoRetencion4.KeyValue := DataTipoRetencion4.FieldValues['codigo'];
      CalcularRetencion(DataTipoRetencion4,TxtMontoRetencion4);
    end
    else
    begin
      if (Length(Trim(CmbCodigoRetencion4.Text)) = 0) then
      begin
        TxtMontoRetencion4.Text := '';
      end;
    end;
  end;
end;

procedure TFrmAgregarDetalleRetencionIslr.BtnRetencion5Click(Sender: TObject);
begin
  if (Length(Trim(TxtDesde.Text)) > 0) then
  begin
    if (Buscador(Self,TipoRetencion,'codigo','Buscar Tipo de Retención',nil,DataTipoRetencion5,'retencion_iva = false')) then
    begin
      CmbCodigoRetencion5.KeyValue := DataTipoRetencion5.FieldValues['codigo'];
      CalcularRetencion(DataTipoRetencion5,TxtMontoRetencion5);
    end
    else
    begin
      if (Length(Trim(CmbCodigoRetencion5.Text)) = 0) then
      begin
        TxtMontoRetencion5.Text := '';
      end;      
    end;
  end;
end;

function TFrmAgregarDetalleRetencionIslr.Buscar: Boolean;
begin
  if (Length(ArrayVariant) > 0) then
  begin
    BusquedaDocumentoRetencionIslr.SetDesde(ArrayVariant[0]);
    BusquedaDocumentoRetencionIslr.SetNumero(ArrayVariant[1]);
    BusquedaDocumentoRetencionIslr.SetCodigoProveedor(ArrayVariant[2]);
    Result := BusquedaDocumentoRetencionIslr.Buscar;
    if (Result) then
    begin
      TxtDesde.Text := BusquedaDocumentoRetencionIslr.GetDesde;
      if (BusquedaDocumentoRetencionIslr.GetDesde = 'AP') then
      begin
        LblDescripcionDesde.Caption := 'Anticipo a Proveedor';
        AnticipoProveedor.SetNumero(BusquedaDocumentoRetencionIslr.GetNumero);
        AnticipoProveedor.BuscarNumero;
      end
      else
      begin
        if (BusquedaDocumentoRetencionIslr.GetDesde = 'NDP') then
        begin
          LblDescripcionDesde.Caption := 'Nota de Débito de Proveedor';
          NotaDebitoProveedor.SetNumero(BusquedaDocumentoRetencionIslr.GetNumero);
          NotaDebitoProveedor.BuscarNumero;
        end
        else
        begin
          LblDescripcionDesde.Caption := 'Factura de Proveedor';
          FacturaProveedor.SetNumero(BusquedaDocumentoRetencionIslr.GetNumero);
          FacturaProveedor.BuscarNumero;
        end;
      end;
      TxtNumero.Text := BusquedaDocumentoRetencionIslr.GetNumero;
      TxtCodigoProveedor.Text := BusquedaDocumentoRetencionIslr.GetCodigoProveedor;
      Proveedor.SetCodigo(BusquedaDocumentoRetencionIslr.GetCodigoProveedor);
      Proveedor.BuscarCodigo;
      LblRazonSocialProveedor.Caption := Proveedor.GetRazonSocial;
      LblMontoBase.Caption := 'Monto Base: ' + FormatFloat('#,##0.0000',BusquedaDocumentoRetencionIslr.GetMontoNeto);
      CmbCodigoRetencion1.SetFocus;      
    end
    else
    begin
      MessageBox(Self.Handle, Cadena('El Documento No Existe!!!'), PChar(MsgTituloError), MB_ICONSTOP or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      Limpiar;
    end;
  end
  else
  begin
    Result := False;
  end;
end;

procedure TFrmAgregarDetalleRetencionIslr.CalcularRetencion(Ds: TClientDataSet;
  CurrencyEdit: TCurrencyEdit);
begin
  TipoRetencion.SetCodigo(Ds.FieldValues['codigo']);
  TipoRetencion.Buscar;
  CurrencyEdit.Value := ((BusquedaDocumentoRetencionIslr.GetMontoNeto * TipoRetencion.GetTarifa) / TipoRetencion.GetBase) - TipoRetencion.GetSustraendo;
  CurrencyEdit.SetFocus;
end;

procedure TFrmAgregarDetalleRetencionIslr.CalcularTotalRetencion;
begin
  TxtTotalRetencion.Value := TxtMontoRetencion1.Value +
                             TxtMontoRetencion2.Value +
                             TxtMontoRetencion3.Value +
                             TxtMontoRetencion4.Value +
                             TxtMontoRetencion5.Value;
end;

procedure TFrmAgregarDetalleRetencionIslr.CmbCodigoRetencion1CloseUp(
  Sender: TObject);
begin
  if (Length(Trim(CmbCodigoRetencion1.Value)) > 0) then
  begin
    CalcularRetencion(DataTipoRetencion1,TxtMontoRetencion1);
  end
  else
  begin
    TxtMontoRetencion1.Text := '';
  end;
end;

procedure TFrmAgregarDetalleRetencionIslr.CmbCodigoRetencion2CloseUp(
  Sender: TObject);
begin
  if (Length(Trim(CmbCodigoRetencion2.Value)) > 0) then
  begin
    CalcularRetencion(DataTipoRetencion2,TxtMontoRetencion2);
  end
  else
  begin
    TxtMontoRetencion2.Text := '';
  end;
end;

procedure TFrmAgregarDetalleRetencionIslr.CmbCodigoRetencion3CloseUp(
  Sender: TObject);
begin
  if (Length(Trim(CmbCodigoRetencion3.Value)) > 0) then
  begin
    CalcularRetencion(DataTipoRetencion3,TxtMontoRetencion3);
  end
  else
  begin
    TxtMontoRetencion3.Text := '';
  end;
end;

procedure TFrmAgregarDetalleRetencionIslr.CmbCodigoRetencion4CloseUp(
  Sender: TObject);
begin
  if (Length(Trim(CmbCodigoRetencion4.Value)) > 0) then
  begin
    CalcularRetencion(DataTipoRetencion4,TxtMontoRetencion4);
  end
  else
  begin
    TxtMontoRetencion4.Text := '';
  end;
end;

procedure TFrmAgregarDetalleRetencionIslr.CmbCodigoRetencion5CloseUp(
  Sender: TObject);
begin
  if (Length(Trim(CmbCodigoRetencion5.Value)) > 0) then
  begin
    CalcularRetencion(DataTipoRetencion5,TxtMontoRetencion5);
  end
  else
  begin
    TxtMontoRetencion5.Text := '';
  end;
end;

procedure TFrmAgregarDetalleRetencionIslr.FormCreate(Sender: TObject);
begin
  inherited;
  ComponenteFoco := nil;
  BusquedaDocumentoRetencionIslr := TBusquedaDocumentoRetencionIslr.Create;
  DetRetencionIslr := TDetRetencionIslr.Create;
  TipoRetencion := TTipoRetencion.Create;
  AnticipoProveedor := TAnticipoProveedor.Create;
  FacturaProveedor := TFacturacionProveedor.Create;
  NotaDebitoProveedor := TNotaDebitoProveedor.Create;
  Proveedor := TProveedor.Create;
  DataTipoRetencion1 := TipoRetencion.ObtenerComboISLR;
  DataTipoRetencion2 := TipoRetencion.ObtenerComboISLR;
  DataTipoRetencion3 := TipoRetencion.ObtenerComboISLR;
  DataTipoRetencion4 := TipoRetencion.ObtenerComboISLR;
  DataTipoRetencion5 := TipoRetencion.ObtenerComboISLR;
  DataTipoRetencion1.First;
  DataTipoRetencion2.First;
  DataTipoRetencion3.First;
  DataTipoRetencion4.First;
  DataTipoRetencion5.First;
  DSTipoRetencion1.DataSet := DataTipoRetencion1;
  DSTipoRetencion2.DataSet := DataTipoRetencion2;
  DSTipoRetencion3.DataSet := DataTipoRetencion3;
  DSTipoRetencion4.DataSet := DataTipoRetencion4;
  DSTipoRetencion5.DataSet := DataTipoRetencion5;
  ConfigurarRxDBLookupCombo(CmbCodigoRetencion1,DSTipoRetencion1,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoRetencion2,DSTipoRetencion2,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoRetencion3,DSTipoRetencion3,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoRetencion4,DSTipoRetencion4,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoRetencion5,DSTipoRetencion5,'codigo','codigo;descripcion');
end;

procedure TFrmAgregarDetalleRetencionIslr.Limpiar;
begin
  inherited;
  TxtDesde.Text := '';
  LblDescripcionDesde.Caption := '';
  TxtNumero.Text := '';
  LblMontoBase.Caption := '';
  TxtCodigoProveedor.Text := '';
  LblRazonSocialProveedor.Caption := '';
  CmbCodigoRetencion1.Value := '';
  TxtMontoRetencion1.Text := '';
  CmbCodigoRetencion2.Value := '';
  TxtMontoRetencion2.Text := '';
  CmbCodigoRetencion3.Value := '';
  TxtMontoRetencion3.Text := '';
  CmbCodigoRetencion4.Value := '';
  TxtMontoRetencion4.Text := '';
  CmbCodigoRetencion5.Value := '';
  TxtMontoRetencion5.Text := '';
end;

procedure TFrmAgregarDetalleRetencionIslr.TxtMontoRetencion1Change(
  Sender: TObject);
begin
  CalcularTotalRetencion;
end;

procedure TFrmAgregarDetalleRetencionIslr.TxtMontoRetencion2Change(
  Sender: TObject);
begin
  CalcularTotalRetencion;
end;

procedure TFrmAgregarDetalleRetencionIslr.TxtMontoRetencion3Change(
  Sender: TObject);
begin
  CalcularTotalRetencion;
end;

procedure TFrmAgregarDetalleRetencionIslr.TxtMontoRetencion4Change(
  Sender: TObject);
begin
  CalcularTotalRetencion;
end;

procedure TFrmAgregarDetalleRetencionIslr.TxtMontoRetencion5Change(
  Sender: TObject);
begin
  CalcularTotalRetencion;
end;

function TFrmAgregarDetalleRetencionIslr.Validar: Boolean;
var
  ListaErrores: TStringList;
  sw: Boolean;
begin
  ListaErrores := TStringList.Create;
  //Validando Existencia del Documento a Retener
  sw := false;
  with Data do
  begin
    First;
    while (not Eof) do
    begin
      if ((FieldValues['desde'] = TxtDesde.Text) and (FieldValues['numero_documento'] = TxtNumero.Text)) then
      begin
        sw := True;
        Break;
      end;      
      Next;
    end;    
  end;
  if (sw) then
  begin
    ListaErrores.Add('El Documento Ya Se Encuentra En La Lista!!!');
  end;
  //Validando Monto de Retencion
  if (TxtTotalRetencion.Value = 0) then
  begin
    ListaErrores.Add('El Total A Retener Debe Ser Mayor A Cero!!!');
  end;
  //Validando Codigo de Retencion 1
  if ((Length(Trim(CmbCodigoRetencion1.Text)) > 0) and (TxtMontoRetencion1.Value = 0)) then
  begin
    ListaErrores.Add('El Monto de Retencion 1 Debe Ser Mayor A Cero!!!');
  end;
  //Validando Codigo de Retencion 2
  if ((Length(Trim(CmbCodigoRetencion2.Text)) > 0) and (TxtMontoRetencion2.Value = 0)) then
  begin
    ListaErrores.Add('El Monto de Retencion 2 Debe Ser Mayor A Cero!!!');
  end;
  //Validando Repeticion Codigo Retencion 2
  if ((Length(Trim(CmbCodigoRetencion2.Text)) > 0) and (CmbCodigoRetencion1.Text = CmbCodigoRetencion2.Text)) then
  begin
    ListaErrores.Add('La Retencion 2 Ya Fue Utilizada!!!');
  end;  
  //Validando Codigo de Retencion 3
  if ((Length(Trim(CmbCodigoRetencion3.Text)) > 0) and (TxtMontoRetencion3.Value = 0)) then
  begin
    ListaErrores.Add('El Monto de Retencion 3 Debe Ser Mayor A Cero!!!');
  end;
  //Validando Repeticion Codigo Retencion 3
  if ((Length(Trim(CmbCodigoRetencion3.Text)) > 0) and ((CmbCodigoRetencion1.Text = CmbCodigoRetencion3.Text) or (CmbCodigoRetencion2.Text = CmbCodigoRetencion3.Text))) then
  begin
    ListaErrores.Add('La Retencion 3 Ya Fue Utilizada!!!');
  end;
  //Validando Codigo de Retencion 4
  if ((Length(Trim(CmbCodigoRetencion4.Text)) > 0) and (TxtMontoRetencion4.Value = 0)) then
  begin
    ListaErrores.Add('El Monto de Retencion 4 Debe Ser Mayor A Cero!!!');
  end;
  //Validando Repeticion Codigo Retencion 4
  if ((Length(Trim(CmbCodigoRetencion4.Text)) > 0) and ((CmbCodigoRetencion1.Text = CmbCodigoRetencion4.Text) or (CmbCodigoRetencion2.Text = CmbCodigoRetencion4.Text) or (CmbCodigoRetencion3.Text = CmbCodigoRetencion4.Text))) then
  begin
    ListaErrores.Add('La Retencion 4 Ya Fue Utilizada!!!');
  end;
  //Validando Codigo de Retencion 5
  if ((Length(Trim(CmbCodigoRetencion5.Text)) > 0) and (TxtMontoRetencion5.Value = 0)) then
  begin
    ListaErrores.Add('El Monto de Retencion 5 Debe Ser Mayor A Cero!!!');
  end;
  //Validando Repeticion Codigo Retencion 5
  if ((Length(Trim(CmbCodigoRetencion5.Text)) > 0) and ((CmbCodigoRetencion1.Text = CmbCodigoRetencion5.Text) or (CmbCodigoRetencion2.Text = CmbCodigoRetencion5.Text) or (CmbCodigoRetencion3.Text = CmbCodigoRetencion5.Text) or (CmbCodigoRetencion4.Text = CmbCodigoRetencion5.Text))) then
  begin
    ListaErrores.Add('La Retencion 5 Ya Fue Utilizada!!!');
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
