unit CondominioAgregarDetalleRecibo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GenericoAgregarDetalle, StdCtrls, Buttons, ExtCtrls, Mask, DB,
  rxToolEdit, rxCurrEdit, ClsCondominioInmueble, ClsCondominioGasto,
  GenericoBuscador, DBClient, Utils;

type
  TFrmCondominioAgregarDetalleRecibo = class(TFrmGenericoAgregarDetalle)
    LblMonto: TLabel;
    TxtMonto: TCurrencyEdit;
    TxtDescripcionGasto: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure TxtCodigoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtnBuscarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TxtMontoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TxtCodigoExit(Sender: TObject);
  private
    { Private declarations }
    CondominioInmueble: TCondominioInmueble;
    CondominioGasto: TCondominioGasto;
  protected
    function Buscar: Boolean; override;
    procedure Agregar; override;
    procedure Limpiar; override;
  public
    { Public declarations }
    CodigoConjuntoAdministracion: string;
    CodigoInmueble: string;
  end;

var
  FrmCondominioAgregarDetalleRecibo: TFrmCondominioAgregarDetalleRecibo;

implementation

{$R *.dfm}

{ TFrmCondominioAgregarDetalleRecibo }

procedure TFrmCondominioAgregarDetalleRecibo.Agregar;
var
  DataInmueble: TClientDataSet;
  AuxFiltro: string;
  AuxFiltrado: Boolean;
  AuxMonto: Currency;
begin
  DataInmueble := nil;
  if (CondominioGasto.GetUsarAlicuotaInmueble) then //Gasto Extraordinario Comun (Calcular)
  begin
    //Guardando Parametros del Filtro Actual
    AuxFiltro := Data.Filter;
    AuxFiltrado := Data.Filtered;
    Data.Filtered := False;
    DataInmueble := CondominioInmueble.ObtenerConjuntoAdministracion(CodigoConjuntoAdministracion);
    DataInmueble.First;
    while (not DataInmueble.Eof) do
    begin
      //Verificando si existe el codigo en el Grid
      Data.First;
      if (Data.Locate('codigo_inmueble;codigo_gasto',VarArrayOf([VarToStr(DataInmueble.FieldValues['codigo']),TxtCodigo.Text]),[loCaseInsensitive])) then
      begin
        Data.Edit;
      end
      else
      begin
        Data.Append;
        Data.FieldValues['codigo_inmueble'] := VarToStr(DataInmueble.FieldValues['codigo']);
        Data.FieldValues['codigo_gasto'] := CondominioGasto.GetCodigo;
        Data.FieldValues['descripcion'] := CondominioGasto.GetDescripcion;
        Data.FieldValues['extraordinario'] := CondominioGasto.GetExtraordinario;
      end;
      //OJO, RECORDAR CONSIDERAR LAS DISTINTAS ALICUOTAS QUE PUEDE MANEJAR EL INMUEBLE
      Data.FieldValues['monto'] :=  TxtMonto.Value * (DataInmueble.FieldValues['p_alicuota'] / 100);
      Data.Post;
      DataInmueble.Next;
    end;       
    //Recolocando Filtro 
    if (AuxFiltrado) then
    begin
      Data.Filter := AuxFiltro;
      Data.Filtered := True;
    end;
  end
  else //Gasto Particular (Solo para el Inmueble Seleccionado)
  begin
    //Verificando Calculo Particular del Monto
    if (CondominioGasto.GetUsarAlicuotaParticular) then
    begin
      AuxMonto := TxtMonto.Value * (CondominioGasto.GetPAlicuota / 100);    
    end
    else //Es Monto Fijo. Se Usara El Introducido por el Usuario ya que El Sistema Automaticamente Carga El Especificado en el Gasto
    begin
      AuxMonto := TxtMonto.Value;
    end;    
    with Data do
    begin
      //Verificando si existe el codigo en el Grid
      if (Locate('codigo_inmueble;codigo_gasto',VarArrayOf([VarToStr(DataInmueble.FieldValues['codigo']),TxtCodigo.Text]),[loCaseInsensitive])) then
      begin
        Edit;
        FieldValues['monto'] := TxtMonto.Value; // *
      end
      else
      begin
        Append;
        FieldValues['codigo_conjunto_administracion'] := CodigoConjuntoAdministracion;
        FieldValues['codigo_gasto'] := CondominioGasto.GetCodigo;
        FieldValues['descripcion'] := CondominioGasto.GetDescripcion;
        FieldValues['monto'] := TxtMonto.Value; // *
        FieldValues['extraordinario'] := CondominioGasto.GetExtraordinario;
      end;
      Post;
    end;  
  end;
  inherited;
end;

procedure TFrmCondominioAgregarDetalleRecibo.BtnBuscarClick(Sender: TObject);
begin
  inherited;
  Buscador(Self,CondominioGasto,'codigo','Buscar Gasto',TxtCodigo,nil,'("codigo_conjunto_administracion" = ' + QuotedStr(CodigoConjuntoAdministracion) + ') AND ("extraordinario" = TRUE)');
  if (Length(Trim(TxtCodigo.Text)) > 0) then
  begin
    ComponenteFoco.SetFocus;
  end;
end;

function TFrmCondominioAgregarDetalleRecibo.Buscar: Boolean;
begin
  CondominioGasto.SetCodigoConjuntoAdministracion(CodigoConjuntoAdministracion);
  CondominioGasto.SetCodigo(TxtCodigo.Text);
  Result := CondominioGasto.Buscar;
  if (Result) then
  begin
    TxtDescripcionGasto.Text := CondominioGasto.GetDescripcion;
  end
  else
  begin
    MessageBox(Self.Handle, Cadena('El Codigo No Existe!!!'), PChar(MsgTituloError), MB_ICONSTOP or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Limpiar;
  end;
end;

procedure TFrmCondominioAgregarDetalleRecibo.FormCreate(Sender: TObject);
begin
  ComponenteFoco := (TxtMonto as TWinControl);
  CondominioInmueble := TCondominioInmueble.Create;
  CondominioGasto := TCondominioGasto.Create;
end;

procedure TFrmCondominioAgregarDetalleRecibo.FormShow(Sender: TObject);
begin
  inherited;
  CondominioInmueble.SetCodigoConjuntoAdministracion(CodigoConjuntoAdministracion);
  CondominioInmueble.SetCodigo(CodigoInmueble);
  CondominioInmueble.Buscar;
end;

procedure TFrmCondominioAgregarDetalleRecibo.Limpiar;
begin
  inherited;
  TxtDescripcionGasto.Text := '';
  TxtMonto.Value := 0;
end;

procedure TFrmCondominioAgregarDetalleRecibo.TxtCodigoExit(Sender: TObject);
begin
  inherited;
  if (not CondominioGasto.GetUsarAlicuotaInmueble) then
  begin
    if (CondominioGasto.GetMontoFijo) then
    begin
      TxtMonto.Value := CondominioGasto.GetMonto;
    end;
  end;
end;

procedure TFrmCondominioAgregarDetalleRecibo.TxtCodigoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  case Key of
    VK_RETURN:
      Agregar;
  end;
end;

procedure TFrmCondominioAgregarDetalleRecibo.TxtMontoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  case Key of
    VK_RETURN:
      Agregar;
  end;
end;

end.
