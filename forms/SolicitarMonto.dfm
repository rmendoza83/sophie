object FrmSolicitarMonto: TFrmSolicitarMonto
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsToolWindow
  Caption = 'Sophie - Solicitar ...'
  ClientHeight = 94
  ClientWidth = 294
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object LblTitulo: TLabel
    Left = 8
    Top = 8
    Width = 278
    Height = 17
    Alignment = taCenter
    AutoSize = False
    Caption = 'Titulo'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object BtnAceptar: TBitBtn
    Left = 8
    Top = 56
    Width = 75
    Height = 25
    Caption = '&Aceptar'
    Kind = bkOK
    NumGlyphs = 2
    TabOrder = 0
    OnClick = BtnAceptarClick
  end
  object BitBtn2: TBitBtn
    Left = 211
    Top = 56
    Width = 75
    Height = 25
    Caption = '&Cancelar'
    Kind = bkCancel
    NumGlyphs = 2
    TabOrder = 1
  end
  object TxtMonto: TCurrencyEdit
    Left = 8
    Top = 29
    Width = 278
    Height = 22
    Margins.Left = 4
    Margins.Top = 1
    DecimalPlaces = 4
    DisplayFormat = '#,##0.0000'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    HideSelection = False
    ParentFont = False
    TabOrder = 2
  end
end
