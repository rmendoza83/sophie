unit TipoComision;

{$I sophie.inc}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Generico, XPMan, ImgList, DB, Grids, DBGrids, StdCtrls, Buttons,
  ExtCtrls, ComCtrls, ToolWin, RxLookup, Mask, rxToolEdit, rxCurrEdit, RXCtrls,
  ClsDivision, ClsLinea, ClsFamilia, ClsClase, ClsManufactura, DBClient, ClsTipoComision,
  Utils, DBCtrls{$IFDEF DELPHI10}, ImageList{$ENDIF};

type
  TFrmTipoComision = class(TFrmGenerico)
    LblCodigo: TLabel;
    TxtCodigo: TEdit;
    LblDescripcion: TLabel;
    TxtDescripcion: TEdit;
    LblPorcentaje: TLabel;
    GrpCondicionComision: TGroupBox;
    LblDivision: TLabel;
    LblLinea: TLabel;
    LblFamilia: TLabel;
    LblClase: TLabel;
    LblManufactura: TLabel;
    CmbLinea: TRxDBLookupCombo;
    CmbFamilia: TRxDBLookupCombo;
    CmbClase: TRxDBLookupCombo;
    CmbManufactura: TRxDBLookupCombo;
    TxtComision: TCurrencyEdit;
    LblClasificacionDivision: TRxLabel;
    DSDivision: TDataSource;
    DSLinea: TDataSource;
    DSFamilia: TDataSource;
    DSClase: TDataSource;
    DSManufactura: TDataSource;
    CmbDivision: TRxDBLookupCombo;
    TxtDivision: TDBText;
    TxtLinea: TDBText;
    TxtFamilia: TDBText;
    TxtClase: TDBText;
    TxtManufactura: TDBText;
    LblComisionCantidad: TLabel;
    LblMontoCantidad: TLabel;
    TxtMontoCantidad: TCurrencyEdit;
    ChkComisionCantidad: TCheckBox;
    procedure LblClasificacionDivisionDblClick(Sender: TObject);
    procedure GrdBusquedaCellClick(Column: TColumn);
    procedure FormCreate(Sender: TObject);
    procedure ChkComisionCantidadClick(Sender: TObject);
    procedure LblDivisionDblClick(Sender: TObject);
    procedure LblLineaDblClick(Sender: TObject);
    procedure LblFamiliaDblClick(Sender: TObject);
    procedure LblClaseDblClick(Sender: TObject);
    procedure LblManufacturaDblClick(Sender: TObject);
  private
    { Private declarations }
    Clase: TTipoComision;
    Division: TDivision;
    Linea: TLinea;
    Familia: TFamilia;
    ClaseClase: TClase;
    Manufactura: TManufactura;
    Data: TClientDataSet;
    DataDivision: TClientDataSet;
    DataLinea: TClientDataSet;
    DataFamilia: TClientDataSet;
    DataClase: TClientDataSet;
    DataManufactura: TClientDataSet;
    procedure Agregar;
    procedure Editar;
    procedure Eliminar;
    procedure Guardar;
    procedure Cancelar;
    procedure Refrescar;
    procedure Imprimir;
    procedure Buscar;
    procedure Limpiar;
    procedure Asignar;
    procedure Obtener;
    procedure Mover;
  protected
    function Validar: Boolean; override;
  public
    { Public declarations }
  end;

var
  FrmTipoComision: TFrmTipoComision;

implementation

uses ClsGenericoBD;

{$R *.dfm}

{ TFrmTipoComision }

procedure TFrmTipoComision.Agregar;
begin
  //Verificando
  Limpiar;
  TxtCodigo.SetFocus
end;

procedure TFrmTipoComision.Asignar;
begin
  with Clase do
  begin
    TxtCodigo.Text := GetCodigo;
    TxtDescripcion.Text := GetDescripcion;
    TxtComision.Value := GetPorcentaje;
    CmbDivision.KeyValue := GetCodigoDivision;
    CmbLinea.KeyValue := GetCodigoLinea;
    CmbFamilia.KeyValue := GetCodigoFamilia;
    CmbClase.KeyValue := GetCodigoClase;
    CmbManufactura.KeyValue := GetCodigoManufactura;
    ChkComisionCantidad.Checked := GetComisionCantidad;
    TxtMontoCantidad.Value := GetMontoCantidad;
    ChkComisionCantidadClick(nil);
  end;
end;

procedure TFrmTipoComision.Buscar;
begin
  Screen.Cursor := crSQLWait;
  if (Length(Trim(TxtCriterio.Text)) > 0) then
  begin
    Data := Clase.ObtenerListaCriterio(Trim(TxtCriterio.Text));
  end
  else
  begin
    Data := Clase.ObtenerLista;
  end;
  Data.First;
  DS.DataSet := Data;
  GrdBusqueda.Refresh;
  Screen.Cursor := crDefault;
end;

procedure TFrmTipoComision.Cancelar;
begin
  TxtCodigo.Enabled := True;
end;

procedure TFrmTipoComision.ChkComisionCantidadClick(Sender: TObject);
begin
  LblMontoCantidad.Enabled := ChkComisionCantidad.Checked;
  TxtMontoCantidad.Enabled := ChkComisionCantidad.Checked;
  if (ChkComisionCantidad.Checked) then
  begin
    TxtComision.Value := 0;
  end;  
end;

procedure TFrmTipoComision.Editar;
begin
  //Se Busca el Elemento
  Clase.SetCodigo(Data.FieldValues['codigo']);
  Clase.Buscar;
  Asignar;
  TxtCodigo.Enabled := False;
end;

procedure TFrmTipoComision.Eliminar;
begin
  //Se Busca el Elemento
  Clase.SetCodigo(Data.FieldValues['codigo']);
  Clase.Buscar;
  Clase.Eliminar;
  MessageBox(Self.Handle, 'El Tipo de Comision Se Ha Eliminado Con Exito!!!',PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  Refrescar;
  Limpiar;
end;

procedure TFrmTipoComision.FormCreate(Sender: TObject);
begin
  inherited;
  ProcAgregar := Agregar;
  ProcEditar := Editar;
  ProcEliminar := Eliminar;
  ProcGuardar := Guardar;
  ProcCancelar := Cancelar;
  ProcRefrescar := Refrescar;
  ProcImprimir := Imprimir;
  ProcBuscar := Buscar;
  Clase := TTipoComision.Create;
  //Inicializando DataSets Secundarios
  Division := TDivision.Create;
  Linea := TLinea.Create;
  Familia := TFamilia.Create;
  ClaseClase := TClase.Create;
  Manufactura := TManufactura.Create;
  DataDivision := Division.ObtenerLista;
  DataLinea := Linea.ObtenerLista;
  DataFamilia := Familia.ObtenerLista;
  DataClase := ClaseClase.ObtenerLista;
  DataManufactura := Manufactura.ObtenerLista;
  DataDivision.First;
  DataLinea.First;
  DataFamilia.First;
  DataClase.First;
  DataManufactura.First;
  DSDivision.DataSet:= DataDivision;
  DSLinea.DataSet:= DataLinea;
  DSFamilia.DataSet:= DataFamilia;
  DSClase.DataSet:= DataClase;
  DSManufactura.DataSet:= DataManufactura;
  ConfigurarRxDBLookupCombo(CmbDivision,DSDivision,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbLinea,DSLinea,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbFamilia,DSFamilia,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbClase,DSClase,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbManufactura,DSManufactura,'codigo','codigo;descripcion');
  TxtDivision.DataSource:= DSDivision;
  TxtDivision.DataField:= 'descripcion';
  TxtLinea.DataSource:= DSLinea;
  TxtLinea.DataField:= 'descripcion';
  TxtFamilia.DataSource:= DSFamilia;
  TxtFamilia.DataField:= 'descripcion';
  TxtClase.DataSource:= DSClase;
  TxtClase.DataField:= 'descripcion';
  TxtManufactura.DataSource:= DSManufactura;
  TxtManufactura.DataField:= 'descripcion';
  Buscar;
end;

procedure TFrmTipoComision.GrdBusquedaCellClick(Column: TColumn);
begin
  Mover;
end;

procedure TFrmTipoComision.Guardar;
begin
  if (Agregando) then
  begin
    //Agregando...
    //Se Busca el Codigo
    Clase.SetCodigo(TxtCodigo.Text);
    if (Clase.Buscar) then
    begin
      MessageBox(Self.Handle, 'El C�digo Ya Existe!!!', PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      TxtCodigo.SetFocus;
      Abort;
    end
    else
    begin
      Obtener;
      Clase.Insertar;
      MessageBox(Self.Handle, 'El Tipo de Comision Ha Sido Agregado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    end;
  end
  else
  begin
    //Editando...
    Obtener;
    Clase.Modificar;
    MessageBox(Self.Handle, 'El Tipo de Comision Ha Sido Editado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    TxtCodigo.Enabled := True;
  end;
  Refrescar;
  Limpiar;
end;

procedure TFrmTipoComision.Imprimir;
begin

end;

procedure TFrmTipoComision.LblClaseDblClick(Sender: TObject);
begin
  inherited;
  CmbClase.Value := '';
end;

procedure TFrmTipoComision.LblClasificacionDivisionDblClick(Sender: TObject);
begin
  CmbDivision.KeyValue := '';
  CmbLinea.KeyValue := '';
  CmbFamilia.KeyValue := '';
  CmbClase.KeyValue := '';
  CmbManufactura.KeyValue := '';
end;

procedure TFrmTipoComision.LblDivisionDblClick(Sender: TObject);
begin
  inherited;
  CmbDivision.Value := '';
end;

procedure TFrmTipoComision.LblFamiliaDblClick(Sender: TObject);
begin
  inherited;
  CmbFamilia.Value := '';
end;

procedure TFrmTipoComision.LblLineaDblClick(Sender: TObject);
begin
  inherited;
  CmbLinea.Value := '';  
end;

procedure TFrmTipoComision.LblManufacturaDblClick(Sender: TObject);
begin
  inherited;
  CmbManufactura.Value := '';
end;

procedure TFrmTipoComision.Limpiar;
begin
  TxtCodigo.Text := '';
  TxtDescripcion.Text := '';
  TxtComision.Value := 0;
  CmbDivision.KeyValue := '';
  CmbLinea.KeyValue := '';
  CmbFamilia.KeyValue := '';
  CmbClase.KeyValue := '';
  CmbManufactura.KeyValue := '';
  ChkComisionCantidad.Checked := False;
  TxtMontoCantidad.Value := 0;
end;

procedure TFrmTipoComision.Mover;
var
  DSTemp: TDataSet;
begin
  if (DS.DataSet.RecordCount > 0) then
  begin
    DSTemp := DS.DataSet;
    with Clase do
    begin
      SetCodigo(DSTemp.FieldValues['codigo']);
      SetDescripcion(DSTemp.FieldValues['descripcion']);
      SetPorcentaje(DSTemp.FieldValues['porcentaje']);
      SetCodigoDivision(DSTemp.FieldValues['codigo_division']);
      SetCodigoLinea(DSTemp.FieldValues['codigo_linea']);
      SetCodigoFamilia(DSTemp.FieldValues['codigo_familia']);
      SetCodigoClase(DSTemp.FieldValues['codigo_clase']);
      SetCodigoManufactura(DSTemp.FieldValues['codigo_manufactura']);
      SetComisionCantidad(DSTemp.FieldValues['comision_cantidad']);
      SetMontoCantidad(DSTemp.FieldValues['monto_cantidad']);
    end;
    Asignar;
  end
  else
  begin
    Limpiar;
  end;
end;

procedure TFrmTipoComision.Obtener;
begin
  with Clase do
  begin
    SetCodigo(TxtCodigo.Text);
    SetDescripcion(TxtDescripcion.Text);
    SetPorcentaje(TxtComision.Value);
    SetCodigoDivision(CmbDivision.Text);
    SetCodigoLinea(CmbLinea.Text);
    SetCodigoFamilia(CmbFamilia.Text);
    SetCodigoClase(CmbClase.Text);
    SetCodigoManufactura(CmbManufactura.Text);
    SetComisionCantidad(ChkComisionCantidad.Checked);
    SetMontoCantidad(TxtMontoCantidad.Value);
  end;
end;

procedure TFrmTipoComision.Refrescar;
begin
  TxtCriterio.Text := '';
  Buscar;
  //Se Actualizan Los demas Componentes
  DataDivision := Division.ObtenerLista;
  DataLinea := Linea.ObtenerLista;
  DataFamilia := Familia.ObtenerLista;
  DataClase := ClaseClase.ObtenerLista;
  DataManufactura := Manufactura.ObtenerLista;
  DataDivision.First;
  DataLinea.First;
  DataFamilia.First;
  DataClase.First;
  DataManufactura.First;
  DSDivision.DataSet:= DataDivision;
  DSLinea.DataSet:= DataLinea;
  DSFamilia.DataSet:= DataFamilia;
  DSClase.DataSet:= DataClase;
  DSManufactura.DataSet:= DataManufactura;
end;

function TFrmTipoComision.Validar: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  (* Validando Informacion del Tipo de Comision *)
  //Validando Codigo
  if (Length(Trim(TxtCodigo.Text)) = 0) then
  begin
    ListaErrores.Add('El C�digo No Puede Estar En Blanco!!!');
  end;
  //Validando Descripcion
  if (Length(Trim(TxtDescripcion.Text)) = 0) then
  begin
    ListaErrores.Add('La Descripci�n No Puede Estar En Blanco!!!');
  end;
  //Validando (%) Comision
  if (not ChkComisionCantidad.Checked) then
  begin
    if (TxtComision.Value <= 0) then
    begin
      ListaErrores.Add('El Porcentaje (%) de Comisi�n No Puede Ser Menor o Igual a Cero!!!');
    end;
  end;
  //Validando Comision por Cantidades
  if (ChkComisionCantidad.Checked) then
  begin
    if (TxtMontoCantidad.Value <= 0) then
    begin
      ListaErrores.Add('El Monto por Cantidades No Puede Ser Menor o Igual a Cero!!!');
    end;
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
