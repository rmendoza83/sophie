unit CuentaBancaria;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Generico, XPMan, ImgList, DB, Grids, DBGrids, StdCtrls, Buttons,
  ExtCtrls, ComCtrls, ToolWin, RxLookup, DBCtrls, Mask, rxToolEdit, rxCurrEdit,
  ClsCuentaBancaria, DBClient, Utils, ClsZona;

type
  TFrmCuentaBancaria = class(TFrmGenerico)
    LblEntidad: TLabel;
    LblCodigo: TLabel;
    LblControlChequera: TLabel;
    TxtCodigo: TEdit;
    TxtEntidad: TEdit;
    ChkControlChequera: TCheckBox;
    LblAgencia: TLabel;
    TxtAgencia: TEdit;
    LblTipo: TLabel;
    TxtTipo: TEdit;
    LblNumero: TLabel;
    TxtNumero: TEdit;
    LblFechaApertura: TLabel;
    LblTelefonos: TLabel;
    TxtTelefonos: TEdit;
    LblFax: TLabel;
    TxtFax: TEdit;
    LblEmail: TLabel;
    TxtEmail: TEdit;
    LblWebsite: TLabel;
    TxtWebsite: TEdit;
    LblCodigoZona: TLabel;
    GrpControlChequera: TGroupBox;
    LblNumChequeInicial: TLabel;
    LblNumChequeFinal: TLabel;
    TxtNombreZona: TDBText;
    CmbCodigoZona: TRxDBLookupCombo;
    BtnBuscarZona: TBitBtn;
    DatFechaApertura: TDateEdit;
    LblContacto: TLabel;
    TxtContacto: TEdit;
    TxtNumChequeInicial: TCurrencyEdit;
    TxtNumChequeFinal: TCurrencyEdit;
    DSZona: TDataSource;
    procedure ChkControlChequeraClick(Sender: TObject);
    procedure BtnBuscarZonaClick(Sender: TObject);
    procedure GrdBusquedaCellClick(Column: TColumn);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    Clase: TCuentaBancaria;
    Zona: TZona;
    Data: TClientDataSet;
    DataZona: TClientDataSet;
    procedure Agregar;
    procedure Editar;
    procedure Eliminar;
    procedure Guardar;
    procedure Cancelar;
    procedure Refrescar;
    procedure Imprimir;
    procedure Buscar;
    procedure Limpiar;
    procedure Asignar;
    procedure Obtener;
    procedure Mover;
  protected
    function Validar: Boolean; override;
  public
    { Public declarations }
  end;

var
  FrmCuentaBancaria: TFrmCuentaBancaria;

implementation

uses
  GenericoBuscador;

{$R *.dfm}

procedure TFrmCuentaBancaria.FormCreate(Sender: TObject);
begin
  ProcAgregar := Agregar;
  ProcEditar := Editar;
  ProcEliminar := Eliminar;
  ProcGuardar := Guardar;
  ProcCancelar := Cancelar;
  ProcRefrescar := Refrescar;
  ProcImprimir := Imprimir;
  ProcBuscar := Buscar;
  Clase := TCuentaBancaria.Create;
  //Inicializando DataSets Secundarios
  Zona := TZona.Create;
  DataZona := Zona.ObtenerLista;
  DataZona.First;
  DSZona.DataSet := DataZona;
  TxtNombreZona.DataSource:= DSZona;
  TxtNombreZona.DataField:= 'descripcion';
  CmbCodigoZona.LookupSource:= DSZona;
  CmbCodigoZona.LookupField := 'codigo';
  CmbCodigoZona.LookupDisplay := 'codigo;descripcion';
  Buscar;
end;

procedure TFrmCuentaBancaria.GrdBusquedaCellClick(Column: TColumn);
begin
  Mover;
end;

procedure TFrmCuentaBancaria.Agregar;
begin
  //Verificando
  Limpiar;
  TxtCodigo.SetFocus;
end;

procedure TFrmCuentaBancaria.Asignar;
begin
  with Clase do
  begin
    TxtCodigo.Text := GetCodigo;
    TxtEntidad.Text := GetEntidad;
    TxtAgencia.Text := GetAgencia;
    TxtTipo.Text := GetTipo;
    TxtNumero.Text := GetNumero;
    DatFechaApertura.Date := GetFechaApertura;
    TxtContacto.Text := GetContacto;
    TxtTelefonos.Text := GetTelefonos;
    TxtFax.Text := GetFax;
    TxtEmail.Text := GetEmail;
    TxtWebsite.Text := GetWebSite;
    CmbCodigoZona.KeyValue := GetCodigoZona;
    ChkControlChequera.Checked := GetControlChequera;
    TxtNumChequeInicial.Value := GetNumChequeInicial;
    TxtNumChequeFinal.Value := GetNumChequeFinal;
  end;
end;

procedure TFrmCuentaBancaria.BtnBuscarZonaClick(Sender: TObject);
begin
  Buscador(Self,Zona,'codigo','Busqueda de Zona',nil,DataZona);
  CmbCodigoZona.KeyValue := DataZona.FieldValues['codigo'];
end;

procedure TFrmCuentaBancaria.Buscar;
begin
  Screen.Cursor := crSQLWait;
  if (Length(Trim(TxtCriterio.Text)) > 0) then
  begin
    Data := Clase.ObtenerListaCriterio(Trim(TxtCriterio.Text));
  end
  else
  begin
    Data := Clase.ObtenerLista;
  end;
  Data.First;
  DS.DataSet := Data;
  GrdBusqueda.Refresh;
  Screen.Cursor := crDefault;
end;

procedure TFrmCuentaBancaria.Cancelar;
begin
  TxtCodigo.Enabled := True;
end;

procedure TFrmCuentaBancaria.ChkControlChequeraClick(Sender: TObject);
begin
  GrpControlChequera.Enabled := ChkControlChequera.Checked;
end;

procedure TFrmCuentaBancaria.Editar;
begin
  //Se Busca el Elemento
  Clase.SetCodigo(Data.FieldValues['codigo']);
  Clase.Buscar;
  Asignar;
  TxtCodigo.Enabled := False;
  TxtEntidad.SetFocus;
end;

procedure TFrmCuentaBancaria.Eliminar;
begin
  //Se Busca el Elemento
  Clase.SetCodigo(Data.FieldValues['codigo']);
  Clase.Buscar;
  Clase.Eliminar;
  MessageBox(Self.Handle, 'La Cuenta Bancaria Ha Sido Eliminada Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  Refrescar;
  Limpiar;
end;

procedure TFrmCuentaBancaria.Guardar;
begin
  if (Agregando) then
  begin
    //Agregando...
    //Se Busca el Codigo
    Clase.SetCodigo(TxtCodigo.Text);
    if (Clase.Buscar) then
    begin
      MessageBox(Self.Handle, 'El C�digo Ya Existe!!!', PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      TxtCodigo.SetFocus;
      Abort;
    end
    else
    begin
      Obtener;
      Clase.Insertar;
      MessageBox(Self.Handle, 'La Cuenta Bancaria Ha Sido Agregada Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    end;
  end
  else
  begin
    //Editando...
    Obtener;
    Clase.Modificar;
    MessageBox(Self.Handle, 'La Cuenta Bancaria Ha Sido Editada Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    TxtCodigo.Enabled := True;
  end;
  Refrescar;
  Limpiar;
end;

procedure TFrmCuentaBancaria.Imprimir;
begin

end;

procedure TFrmCuentaBancaria.Limpiar;
begin
  TxtCodigo.Text := '';
  TxtEntidad.Text := '';
  TxtAgencia.Text := '';
  TxtTipo.Text := '';
  TxtNumero.Text := '';
  DatFechaApertura.Date := Date;
  TxtContacto.Text := '';
  TxtTelefonos.Text := '';
  TxtFax.Text := '';
  TxtEmail.Text := '';
  TxtWebsite.Text := '';
  CmbCodigoZona.KeyValue := '';
  ChkControlChequera.Checked := False;
  TxtNumChequeInicial.Text := '';
  TxtNumChequeFinal.Text := '';
end;

procedure TFrmCuentaBancaria.Mover;
var
  DSTemp: TDataSet;
begin
  if (DS.DataSet.RecordCount > 0) then
  begin
    DSTemp := DS.DataSet;
    with Clase do
    begin
      SetCodigo(DSTemp.FieldValues['codigo']);
      SetEntidad(DSTemp.FieldValues['entidad']);
      SetAgencia(DSTemp.FieldValues['agencia']);
      SetTipo(DSTemp.FieldValues['tipo']);
      SetNumero(DSTemp.FieldValues['numero']);
      SetFechaApertura(DSTemp.FieldValues['fecha_apertura']);
      SetContacto(DSTemp.FieldValues['contacto']);
      SetTelefonos(DSTemp.FieldValues['telefonos']);
      SetFax(DSTemp.FieldValues['fax']);
      SetEmail(DSTemp.FieldValues['email']);
      SetWebSite(DSTemp.FieldValues['website']);
      SetCodigoZona(DSTemp.FieldValues['codigo_zona']);
      SetControlChequera(DSTemp.FieldValues['control_chequera']);
      SetNumChequeInicial(DSTemp.FieldValues['num_cheque_inicial']);
      SetNumChequeFinal(DSTemp.FieldValues['num_cheque_final']);
    end;
    Asignar;
  end
  else
  begin
    Limpiar;
  end;
end;

procedure TFrmCuentaBancaria.Obtener;
begin
  with Clase do
  begin
    SetCodigo(TxtCodigo.Text);
    SetEntidad(TxtEntidad.Text);
    SetAgencia(TxtAgencia.Text);
    SetTipo(TxtTipo.Text);
    SetNumero(TxtNumero.Text);
    SetFechaApertura(DatFechaApertura.Date);
    SetContacto(TxtContacto.Text);
    SetTelefonos(TxtTelefonos.Text);
    SetFax(TxtFax.Text);
    SetEmail(TxtEmail.Text);
    SetWebSite(TxtWebsite.Text);
    SetCodigoZona(CmbCodigoZona.KeyValue);
    SetControlChequera(ChkControlChequera.Checked);
    SetNumChequeInicial(Trunc(TxtNumChequeInicial.Value));
    SetNumChequeFinal(Trunc(TxtNumChequeFinal.Value));
  end;
end;

procedure TFrmCuentaBancaria.Refrescar;
begin
  TxtCriterio.Text := '';
  Buscar;
  //Se Actualizan Los demas Componentes
  DataZona := Zona.ObtenerLista;
  DataZona.First;
  DSZona.DataSet := DataZona;
end;

function TFrmCuentaBancaria.Validar: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  (* Validando Informacion de la Cuenta Bancaria *)
  //Validando Codigo
  if (Length(Trim(TxtCodigo.Text)) = 0) then
  begin
    ListaErrores.Add('El C�digo No Puede Estar En Blanco!!!');
  end;
  //Validando Entidad
  if (Length(Trim(TxtEntidad.Text)) = 0) then
  begin
    ListaErrores.Add('La Entidad No Puede Estar En Blanco!!!');
  end;
  //Validando Agencia
  if (Length(Trim(TxtAgencia.Text)) = 0) then
  begin
    ListaErrores.Add('La Agencia No Puede Estar En Blanco!!!');
  end;
  //Validando Tipo
  if (Length(Trim(TxtTipo.Text)) = 0) then
  begin
    ListaErrores.Add('El Tipo de la Cuenta No Puede Estar En Blanco!!!');
  end;
  //Validando Numero
  if (Length(Trim(TxtNumero.Text)) = 0) then
  begin
    ListaErrores.Add('El N�mero de la Cuenta No Puede Estar En Blanco!!!');
  end;
  //Validando Fecha de Apertura
  if (Length(Trim(DatFechaApertura.Text)) = 0) then
  begin
    ListaErrores.Add('La Fecha de Apertura No Puede Estar En Blanco!!!');
  end;
  //Validando Contacto
  if (Length(Trim(TxtContacto.Text)) = 0) then
  begin
    ListaErrores.Add('La Persona Contacto No Puede Estar En Blanco!!!');
  end;
  //Validando Telefonos
  if (Length(Trim(TxtTelefonos.Text)) = 0) then
  begin
    ListaErrores.Add('Los Tel�fonos No Pueden Estar En Blanco!!!');
  end;
  //Validando Zona
  if (Length(Trim(CmbCodigoZona.Text)) = 0) then
  begin
    ListaErrores.Add('La Zona No Puede Estar En Blanco!!!');
  end;
  //Validando Control de Chequeras
  if (ChkControlChequera.Checked) then
  begin
    //Validando Numero del Cheque Inicial
    if (Length(Trim(TxtNumChequeInicial.Text)) = 0) then
    begin
      ListaErrores.Add('El N�mero del Cheque Inicial No Puede Estar En Blanco!!!');
    end;
    //Validando Numero del Cheque Final
    if (Length(Trim(TxtNumChequeFinal.Text)) = 0) then
    begin
      ListaErrores.Add('El N�mero del Cheque Final No Puede Estar En Blanco!!!');
    end;
    //Validando Numero del Cheque Inicial sea Menor que el Numero del Cheque Final
    if (TxtNumChequeInicial.Value >= TxtNumChequeFinal.Value) then
    begin
      ListaErrores.Add('El N�mero del Cheque Inicial Debe Ser Menor Que El Numero del Cheque Final En El Control de Chequeras!!!');
    end;
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
