unit AgregarDetalleLiquidacionCobranza;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GenericoAgregarDetalle, StdCtrls, Buttons, ExtCtrls,
  ClsBusquedaDocumentoCuentaxCobrar, ClsAnticipoCliente, ClsFacturacionCliente,
  ClsNotaDebitoCliente, ClsNotaCreditoCliente, ClsDetLiquidacionCobranza,
  ClsCuentaxCobrar, ClsTipoRetencion, ClsCliente, GenericoBuscador, DBClient,
  Utils, DB, Mask, rxToolEdit, rxCurrEdit, RxLookup;

type
  TFrmAgregarDetalleLiquidacionCobranza = class(TFrmGenericoAgregarDetalle)
    LblNumero: TLabel;
    LblCliente: TLabel;
    LblDesde: TLabel;
    LblDescripcionDesde: TLabel;
    LblTotalRetencion: TLabel;
    LblMontoBase: TLabel;
    LblRazonSocialCliente: TLabel;
    TxtDesde: TEdit;
    TxtNumero: TEdit;
    TxtCodigoCliente: TEdit;
    GrpRetencionesIslr: TGroupBox;
    CmbCodigoRetencion1: TRxDBLookupCombo;
    BtnRetencion1: TBitBtn;
    TxtMontoRetencion1: TCurrencyEdit;
    CmbCodigoRetencion2: TRxDBLookupCombo;
    BtnRetencion2: TBitBtn;
    TxtMontoRetencion2: TCurrencyEdit;
    CmbCodigoRetencion3: TRxDBLookupCombo;
    BtnRetencion3: TBitBtn;
    TxtMontoRetencion3: TCurrencyEdit;
    CmbCodigoRetencion4: TRxDBLookupCombo;
    BtnRetencion4: TBitBtn;
    TxtMontoRetencion4: TCurrencyEdit;
    CmbCodigoRetencion5: TRxDBLookupCombo;
    BtnRetencion5: TBitBtn;
    TxtMontoRetencion5: TCurrencyEdit;
    BtnBorrarRetencion1: TBitBtn;
    BtnBorrarRetencion2: TBitBtn;
    BtnBorrarRetencion3: TBitBtn;
    BtnBorrarRetencion4: TBitBtn;
    BtnBorrarRetencion5: TBitBtn;
    TxtTotalRetencion: TCurrencyEdit;
    DSTipoRetencion1: TDataSource;
    DSTipoRetencion2: TDataSource;
    DSTipoRetencion3: TDataSource;
    DSTipoRetencion4: TDataSource;
    DSTipoRetencion5: TDataSource;
    GrpRetencionIva: TGroupBox;
    CmbCodigoRetencionIva: TRxDBLookupCombo;
    BtnRetencionIva: TBitBtn;
    TxtMontoRetencionIva: TCurrencyEdit;
    BtnBorrarRetencionIva: TBitBtn;
    LblAbono: TLabel;
    TxtAbono: TCurrencyEdit;
    DSTipoRetencionIva: TDataSource;
    LblNroCuota: TLabel;
    LblMontoTotalAbono: TLabel;
    TxtNroCuota: TEdit;
    PanelNoAplicaISLR: TPanel;
    PanelNoAplicaIVA: TPanel;
    procedure TxtMontoRetencionIvaChange(Sender: TObject);
    procedure TxtCodigoExit(Sender: TObject);
    procedure TxtMontoRetencion5Change(Sender: TObject);
    procedure TxtMontoRetencion4Change(Sender: TObject);
    procedure TxtMontoRetencion3Change(Sender: TObject);
    procedure TxtMontoRetencion2Change(Sender: TObject);
    procedure TxtMontoRetencion1Change(Sender: TObject);
    procedure CmbCodigoRetencionIvaCloseUp(Sender: TObject);
    procedure CmbCodigoRetencion5CloseUp(Sender: TObject);
    procedure CmbCodigoRetencion4CloseUp(Sender: TObject);
    procedure CmbCodigoRetencion3CloseUp(Sender: TObject);
    procedure CmbCodigoRetencion2CloseUp(Sender: TObject);
    procedure CmbCodigoRetencion1CloseUp(Sender: TObject);
    procedure BtnBorrarRetencionIvaClick(Sender: TObject);
    procedure BtnBorrarRetencion5Click(Sender: TObject);
    procedure BtnBorrarRetencion4Click(Sender: TObject);
    procedure BtnBorrarRetencion3Click(Sender: TObject);
    procedure BtnBorrarRetencion2Click(Sender: TObject);
    procedure BtnBorrarRetencion1Click(Sender: TObject);
    procedure BtnRetencionIvaClick(Sender: TObject);
    procedure BtnRetencion5Click(Sender: TObject);
    procedure BtnRetencion4Click(Sender: TObject);
    procedure BtnRetencion3Click(Sender: TObject);
    procedure BtnRetencion2Click(Sender: TObject);
    procedure BtnRetencion1Click(Sender: TObject);
    procedure BtnBuscarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    ArrayVariant: TArrayVariant;
    DetLiquidacionCobraza: TDetLiquidacionCobranza;
    CuentaxCobrar: TCuentaxCobrar;
    TipoRetencion: TTipoRetencion;
    AnticipoCliente: TAnticipoCliente;
    FacturaCliente: TFacturacionCliente;
    NotaDebitoCliente: TNotaDebitoCliente;
    NotaCreditoCliente: TNotaCreditoCliente;
    Cliente: TCliente;
    DataTipoRetencion1: TClientDataSet;
    DataTipoRetencion2: TClientDataSet;
    DataTipoRetencion3: TClientDataSet;
    DataTipoRetencion4: TClientDataSet;
    DataTipoRetencion5: TClientDataSet;
    DataTipoRetencionIva: TClientDataSet;
    procedure CalcularRetencion(Ds: TClientDataSet; CurrencyEdit: TCurrencyEdit);
    procedure CalcularRetencionIva(Ds: TClientDataSet; CurrencyEdit: TCurrencyEdit);
    procedure CalcularTotalRetencion;
  protected
    { Protected declarations }
    function Buscar: Boolean; override;
    procedure Agregar; override;
    procedure Limpiar; override;
    function Validar: Boolean;
  public
    { Public declarations }
    CodigoCliente: string;
    BusquedaDocumentoCuentaxCobrar: TBusquedaDocumentoCuentaxCobrar;
  end;

var
  FrmAgregarDetalleLiquidacionCobranza: TFrmAgregarDetalleLiquidacionCobranza;

implementation

uses
  Math,
  Types;

{$R *.dfm}

{ TFrmAgregarDetalleLiquidacionCobranza }

procedure TFrmAgregarDetalleLiquidacionCobranza.Agregar;
begin
  if (Validar) then
  begin
    with Data do
    begin
      CuentaxCobrar.SetDesde(BusquedaDocumentoCuentaxCobrar.GetDesde);
      CuentaxCobrar.SetNumeroDocumento(BusquedaDocumentoCuentaxCobrar.GetNumeroDocumento);
      CuentaxCobrar.SetNumeroCuota(BusquedaDocumentoCuentaxCobrar.GetNumeroCuota);
      CuentaxCobrar.BuscarCobranza;
      Append;
      FieldValues['desde'] := CuentaxCobrar.GetDesde;
      FieldValues['numero_documento'] := CuentaxCobrar.GetNumeroDocumento;
      if (BusquedaDocumentoCuentaxCobrar.GetDesde = 'AC') then
      begin //Anticipo de Cliente
        AnticipoCliente.SetNumero(BusquedaDocumentoCuentaxCobrar.GetNumeroDocumento);
        AnticipoCliente.BuscarNumero;
        FieldValues['monto_base'] := AnticipoCliente.GetMontoNeto;
        FieldValues['p_iva'] := 0;
        FieldValues['impuesto'] := 0;
        FieldValues['total'] := AnticipoCliente.GetMontoNeto;
      end
      else
      begin
        if (BusquedaDocumentoCuentaxCobrar.GetDesde = 'NDC') then
        begin //Nota Debito Cliente
          NotaDebitoCliente.SetNumero(BusquedaDocumentoCuentaxCobrar.GetNumeroDocumento);
          NotaDebitoCliente.BuscarNumero;
          FieldValues['monto_base'] := NotaDebitoCliente.GetMontoNeto;
          FieldValues['p_iva'] := NotaDebitoCliente.GetPIva;
          FieldValues['impuesto'] := NotaDebitoCliente.GetImpuesto;
          FieldValues['total'] := NotaDebitoCliente.GetTotal;
        end
        else
        begin
          if (BusquedaDocumentoCuentaxCobrar.GetDesde = 'NCC') then
          begin //Nota Credito Cliente
            NotaCreditoCliente.SetNumero(BusquedaDocumentoCuentaxCobrar.GetNumeroDocumento);
            NotaCreditoCliente.BuscarNumero;
            FieldValues['monto_base'] := NotaCreditoCliente.GetMontoNeto;
            FieldValues['p_iva'] := NotaCreditoCliente.GetPIva;
            FieldValues['impuesto'] := NotaCreditoCliente.GetImpuesto;
            FieldValues['total'] := NotaCreditoCliente.GetTotal;
          end
          else
          begin //Factura de Cliente
            FacturaCliente.SetNumero(BusquedaDocumentoCuentaxCobrar.GetNumeroDocumento);
            FacturaCliente.BuscarNumero;
            FieldValues['monto_base'] := FacturaCliente.GetMontoNeto;
            FieldValues['p_iva'] := FacturaCliente.GetPIva;
            FieldValues['impuesto'] := FacturaCliente.GetImpuesto;
            FieldValues['total'] := FacturaCliente.GetTotal;
          end;
        end;
      end;
      FieldValues['id_cobranza'] := CuentaxCobrar.GetIdCobranza;
      FieldValues['orden_cobranza'] := CuentaxCobrar.GetOrdenCobranza;
      FieldValues['numero_cuota'] := CuentaxCobrar.GetNumeroCuota;
      FieldValues['abonado'] := TxtAbono.Value;
      FieldValues['codigo_retencion_iva'] := CmbCodigoRetencionIva.Text;
      FieldValues['monto_retencion_iva'] := TxtMontoRetencionIva.Value;
      if (TxtMontoRetencionIva.Value > 0) then
      begin
        FieldValues['descripcion_retencion_iva'] := DataTipoRetencionIva.FieldValues['descripcion'];
      end
      else
      begin
        FieldValues['descripcion_retencion_iva'] := '';
      end;
      FieldValues['codigo_retencion_1'] := CmbCodigoRetencion1.Text;
      FieldValues['monto_retencion_1'] := TxtMontoRetencion1.Value;
      if (TxtMontoRetencion1.Value > 0) then
      begin
        FieldValues['descripcion_retencion_1'] := DataTipoRetencion1.FieldValues['descripcion'];
      end
      else
      begin
        FieldValues['descripcion_retencion_1'] := '';
      end;
      FieldValues['codigo_retencion_2'] := CmbCodigoRetencion2.Text;
      FieldValues['monto_retencion_2'] := TxtMontoRetencion2.Value;
      if (TxtMontoRetencion2.Value > 0) then
      begin
        FieldValues['descripcion_retencion_2'] := DataTipoRetencion2.FieldValues['descripcion'];
      end
      else
      begin
        FieldValues['descripcion_retencion_2'] := '';
      end;
      FieldValues['codigo_retencion_3'] := CmbCodigoRetencion3.Text;
      FieldValues['monto_retencion_3'] := TxtMontoRetencion3.Value;
      if (TxtMontoRetencion3.Value > 0) then
      begin
        FieldValues['descripcion_retencion_3'] := DataTipoRetencion3.FieldValues['descripcion'];
      end
      else
      begin
        FieldValues['descripcion_retencion_3'] := '';
      end;
      FieldValues['codigo_retencion_4'] := CmbCodigoRetencion4.Text;
      FieldValues['monto_retencion_4'] := TxtMontoRetencion4.Value;
      if (TxtMontoRetencion4.Value > 0) then
      begin
        FieldValues['descripcion_retencion_4'] := DataTipoRetencion4.FieldValues['descripcion'];
      end
      else
      begin
        FieldValues['descripcion_retencion_4'] := '';
      end;
      FieldValues['codigo_retencion_5'] := CmbCodigoRetencion5.Text;
      FieldValues['monto_retencion_5'] := TxtMontoRetencion5.Value;
      if (TxtMontoRetencion5.Value > 0) then
      begin
        FieldValues['descripcion_retencion_5'] := DataTipoRetencion5.FieldValues['descripcion'];
      end
      else
      begin
        FieldValues['descripcion_retencion_5'] := '';
      end;
      Post;
    end;
    inherited;
  end
  else
  begin
    ModalResult := mrNone;
  end;
end;

procedure TFrmAgregarDetalleLiquidacionCobranza.BtnBorrarRetencion1Click(
  Sender: TObject);
begin
  CmbCodigoRetencion1.KeyValue := '';
  TxtMontoRetencion1.Text := '';
end;

procedure TFrmAgregarDetalleLiquidacionCobranza.BtnBorrarRetencion2Click(
  Sender: TObject);
begin
  CmbCodigoRetencion2.KeyValue := '';
  TxtMontoRetencion2.Text := '';
end;

procedure TFrmAgregarDetalleLiquidacionCobranza.BtnBorrarRetencion3Click(
  Sender: TObject);
begin
  CmbCodigoRetencion2.KeyValue := '';
  TxtMontoRetencion2.Text := '';
end;

procedure TFrmAgregarDetalleLiquidacionCobranza.BtnBorrarRetencion4Click(
  Sender: TObject);
begin
  CmbCodigoRetencion4.KeyValue := '';
  TxtMontoRetencion4.Text := '';
end;

procedure TFrmAgregarDetalleLiquidacionCobranza.BtnBorrarRetencion5Click(
  Sender: TObject);
begin
  CmbCodigoRetencion5.KeyValue := '';
  TxtMontoRetencion5.Text := '';
end;

procedure TFrmAgregarDetalleLiquidacionCobranza.BtnBorrarRetencionIvaClick(
  Sender: TObject);
begin
  CmbCodigoRetencionIva.KeyValue := '';
  TxtMontoRetencionIva.Text := '';
end;

procedure TFrmAgregarDetalleLiquidacionCobranza.BtnBuscarClick(Sender: TObject);
var
  Condicion: string;
begin
  inherited;
  Condicion := '"codigo_cliente" = ' + QuotedStr(CodigoCliente);
  if (Buscador(Self,BusquedaDocumentoCuentaxCobrar,'desde;numero_documento;codigo_cliente;numero_cuota','Buscar Documento',nil,nil,True,ArrayVariant,Condicion)) then
  begin
    Buscar;
  end
  else
  begin
    SetLength(ArrayVariant,0);
  end;
end;

procedure TFrmAgregarDetalleLiquidacionCobranza.BtnRetencion1Click(
  Sender: TObject);
begin
  if (Length(Trim(TxtDesde.Text)) > 0) then
  begin
    if (Buscador(Self,TipoRetencion,'codigo','Buscar Tipo de Retenci�n',nil,DataTipoRetencion1,'retencion_iva = false')) then
    begin
      CmbCodigoRetencion1.KeyValue := DataTipoRetencion1.FieldValues['codigo'];
      CalcularRetencion(DataTipoRetencion1,TxtMontoRetencion1);
    end
    else
    begin
      if (Length(Trim(CmbCodigoRetencion1.Text)) = 0) then
      begin
        TxtMontoRetencion1.Text := '';
      end;      
    end;
  end;
end;

procedure TFrmAgregarDetalleLiquidacionCobranza.BtnRetencion2Click(
  Sender: TObject);
begin
  if (Length(Trim(TxtDesde.Text)) > 0) then
  begin
    if (Buscador(Self,TipoRetencion,'codigo','Buscar Tipo de Retenci�n',nil,DataTipoRetencion2,'retencion_iva = false')) then
    begin
      CmbCodigoRetencion2.KeyValue := DataTipoRetencion2.FieldValues['codigo'];
      CalcularRetencion(DataTipoRetencion2,TxtMontoRetencion2);
    end
    else
    begin
      if (Length(Trim(CmbCodigoRetencion2.Text)) = 0) then
      begin
        TxtMontoRetencion2.Text := '';
      end;      
    end;
  end;
end;

procedure TFrmAgregarDetalleLiquidacionCobranza.BtnRetencion3Click(
  Sender: TObject);
begin
  if (Length(Trim(TxtDesde.Text)) > 0) then
  begin
    if (Buscador(Self,TipoRetencion,'codigo','Buscar Tipo de Retenci�n',nil,DataTipoRetencion3,'retencion_iva = false')) then
    begin
      CmbCodigoRetencion3.KeyValue := DataTipoRetencion3.FieldValues['codigo'];
      CalcularRetencion(DataTipoRetencion3,TxtMontoRetencion3);
    end
    else
    begin
      if (Length(Trim(CmbCodigoRetencion3.Text)) = 0) then
      begin
        TxtMontoRetencion3.Text := '';
      end;
    end;
  end;
end;

procedure TFrmAgregarDetalleLiquidacionCobranza.BtnRetencion4Click(
  Sender: TObject);
begin
  if (Length(Trim(TxtDesde.Text)) > 0) then
  begin
    if (Buscador(Self,TipoRetencion,'codigo','Buscar Tipo de Retenci�n',nil,DataTipoRetencion4,'retencion_iva = false')) then
    begin
      CmbCodigoRetencion4.KeyValue := DataTipoRetencion4.FieldValues['codigo'];
      CalcularRetencion(DataTipoRetencion4,TxtMontoRetencion4);
    end
    else
    begin
      if (Length(Trim(CmbCodigoRetencion4.Text)) = 0) then
      begin
        TxtMontoRetencion4.Text := '';
      end;
    end;
  end;
end;

procedure TFrmAgregarDetalleLiquidacionCobranza.BtnRetencion5Click(
  Sender: TObject);
begin
  inherited;
  if (Length(Trim(TxtDesde.Text)) > 0) then
  begin
    if (Buscador(Self,TipoRetencion,'codigo','Buscar Tipo de Retenci�n',nil,DataTipoRetencion5,'retencion_iva = false')) then
    begin
      CmbCodigoRetencion5.KeyValue := DataTipoRetencion5.FieldValues['codigo'];
      CalcularRetencion(DataTipoRetencion5,TxtMontoRetencion5);
    end
    else
    begin
      if (Length(Trim(CmbCodigoRetencion5.Text)) = 0) then
      begin
        TxtMontoRetencion5.Text := '';
      end;
    end;
  end;
end;

procedure TFrmAgregarDetalleLiquidacionCobranza.BtnRetencionIvaClick(
  Sender: TObject);
begin
  if (Length(Trim(TxtDesde.Text)) > 0) then
  begin
    if (Buscador(Self,TipoRetencion,'codigo','Buscar Tipo de Retenci�n',nil,DataTipoRetencionIva,'retencion_iva = true')) then
    begin
      CmbCodigoRetencionIva.KeyValue := DataTipoRetencionIva.FieldValues['codigo'];
      CalcularRetencionIva(DataTipoRetencionIva,TxtMontoRetencionIva);
    end
    else
    begin
      if (Length(Trim(CmbCodigoRetencionIva.Text)) = 0) then
      begin
        TxtMontoRetencionIva.Text := '';
      end;      
    end;
  end;
end;

function TFrmAgregarDetalleLiquidacionCobranza.Buscar: Boolean;
begin
  if (Length(ArrayVariant) > 0) then
  begin
    BusquedaDocumentoCuentaxCobrar.SetDesde(ArrayVariant[0]);
    BusquedaDocumentoCuentaxCobrar.SetNumeroDocumento(ArrayVariant[1]);
    BusquedaDocumentoCuentaxCobrar.SetCodigoCliente(ArrayVariant[2]);
    BusquedaDocumentoCuentaxCobrar.SetNumeroCuota(ArrayVariant[3]);
    Result := BusquedaDocumentoCuentaxCobrar.Buscar;
    if (Result) then
    begin
      TxtDesde.Text := BusquedaDocumentoCuentaxCobrar.GetDesde;
      if (BusquedaDocumentoCuentaxCobrar.GetDesde = 'AC') then
      begin
        LblDescripcionDesde.Caption := 'Anticipo de Cliente';
        AnticipoCliente.SetNumero(BusquedaDocumentoCuentaxCobrar.GetNumeroDocumento);
        AnticipoCliente.BuscarNumero;
      end
      else
      begin
        if (BusquedaDocumentoCuentaxCobrar.GetDesde = 'NDC') then
        begin
          LblDescripcionDesde.Caption := 'Nota de D�bito a Cliente';
          NotaDebitoCliente.SetNumero(BusquedaDocumentoCuentaxCobrar.GetNumeroDocumento);
          NotaDebitoCliente.BuscarNumero;
        end
        else
        begin
          if (BusquedaDocumentoCuentaxCobrar.GetDesde = 'NCC') then
          begin
            LblDescripcionDesde.Caption := 'Nota de Cr�dito de Cliente';
            NotaCreditoCliente.SetNumero(BusquedaDocumentoCuentaxCobrar.GetNumeroDocumento);
            NotaCreditoCliente.BuscarNumero;
          end
          else
          begin
            LblDescripcionDesde.Caption := 'Factura de Cliente';
            FacturaCliente.SetNumero(BusquedaDocumentoCuentaxCobrar.GetNumeroDocumento);
            FacturaCliente.BuscarNumero;
          end;
        end;
      end;
      TxtNumero.Text := BusquedaDocumentoCuentaxCobrar.GetNumeroDocumento;
      TxtCodigoCliente.Text := BusquedaDocumentoCuentaxCobrar.GetCodigoCliente;
      Cliente.SetCodigo(BusquedaDocumentoCuentaxCobrar.GetCodigoCliente);
      Cliente.BuscarCodigo;
      LblRazonSocialCliente.Caption := Cliente.GetRazonSocial;
      LblMontoBase.Caption := 'Monto Documento: ' + FormatFloat('#,##0.0000',BusquedaDocumentoCuentaxCobrar.GetMontoNetoDocumento);
      LblMontoTotalAbono.Caption := 'Monto a Pagar: ' + FormatFloat('#,##0.0000',BusquedaDocumentoCuentaxCobrar.GetTotal);
      if (BusquedaDocumentoCuentaxCobrar.GetNumeroCuota = -1) then
      begin
        TxtNroCuota.Text := 'No Aplica';
      end
      else
      begin
        TxtNroCuota.Text := IntToStr(BusquedaDocumentoCuentaxCobrar.GetNumeroCuota) + '/' + IntToStr(BusquedaDocumentoCuentaxCobrar.GetTotalCuotas);
      end;
      TxtAbono.Value := BusquedaDocumentoCuentaxCobrar.GetTotal;
      //Verificando Tipo de Documento
      if (BusquedaDocumentoCuentaxCobrar.GetDesde = 'AC') then
      begin
        GrpRetencionesIslr.Enabled := False;
        GrpRetencionIva.Enabled := False;
        PanelNoAplicaISLR.Visible := True;
        PanelNoAplicaIVA.Visible := True;
      end
      else
      begin
        GrpRetencionesIslr.Enabled := True;
        GrpRetencionIva.Enabled := True;
        PanelNoAplicaISLR.Visible := False;
        PanelNoAplicaIVA.Visible := False;
      end;
      //Verificando Retenciones Anteriores
      //Verificando Retenciones ISLR
      if (DetLiquidacionCobraza.VerificarRetencionesISLR(BusquedaDocumentoCuentaxCobrar.GetIdCobranza)) then
      begin
        GrpRetencionesIslr.Enabled := False;
        PanelNoAplicaISLR.Visible := True;
      end;
      //Verificando Retenciones IVA
      if (DetLiquidacionCobraza.VerificarRetencionesIVA(BusquedaDocumentoCuentaxCobrar.GetIdCobranza)) then
      begin
        GrpRetencionIva.Enabled := False;
        PanelNoAplicaIVA.Visible := True;
      end;
      TxtAbono.SetFocus;
    end
    else
    begin
      MessageBox(Self.Handle, Cadena('El Documento No Existe!!!'), PChar(MsgTituloError), MB_ICONSTOP or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      GrpRetencionesIslr.Enabled := False;
      GrpRetencionIva.Enabled := False;
      Limpiar;
    end;
  end
  else
  begin
    Result := False;
  end;
end;

procedure TFrmAgregarDetalleLiquidacionCobranza.CalcularRetencion(
  Ds: TClientDataSet; CurrencyEdit: TCurrencyEdit);
begin
  TipoRetencion.SetCodigo(Ds.FieldValues['codigo']);
  TipoRetencion.Buscar;
  CurrencyEdit.Value := ((BusquedaDocumentoCuentaxCobrar.GetMontoNetoDocumento * TipoRetencion.GetTarifa) / TipoRetencion.GetBase) - TipoRetencion.GetSustraendo;
  CurrencyEdit.SetFocus;
end;

procedure TFrmAgregarDetalleLiquidacionCobranza.CalcularRetencionIva(
  Ds: TClientDataSet; CurrencyEdit: TCurrencyEdit);
begin
  TipoRetencion.SetCodigo(Ds.FieldValues['codigo']);
  TipoRetencion.Buscar;
  CurrencyEdit.Value := ((BusquedaDocumentoCuentaxCobrar.GetImpuestoDocumento * TipoRetencion.GetTarifa) / TipoRetencion.GetBase) - TipoRetencion.GetSustraendo;
  CurrencyEdit.SetFocus;
end;

procedure TFrmAgregarDetalleLiquidacionCobranza.CalcularTotalRetencion;
begin
  TxtTotalRetencion.Value := TxtMontoRetencion1.Value +
                             TxtMontoRetencion2.Value +
                             TxtMontoRetencion3.Value +
                             TxtMontoRetencion4.Value +
                             TxtMontoRetencion5.Value +
                             TxtMontoRetencionIva.Value;
end;

procedure TFrmAgregarDetalleLiquidacionCobranza.CmbCodigoRetencion1CloseUp(
  Sender: TObject);
begin
  if (Length(Trim(CmbCodigoRetencion1.Value)) > 0) then
  begin
    CalcularRetencion(DataTipoRetencion1,TxtMontoRetencion1);
  end
  else
  begin
    TxtMontoRetencion1.Text := '';
  end;
end;

procedure TFrmAgregarDetalleLiquidacionCobranza.CmbCodigoRetencion2CloseUp(
  Sender: TObject);
begin
  if (Length(Trim(CmbCodigoRetencion2.Value)) > 0) then
  begin
    CalcularRetencion(DataTipoRetencion2,TxtMontoRetencion2);
  end
  else
  begin
    TxtMontoRetencion2.Text := '';
  end;
end;

procedure TFrmAgregarDetalleLiquidacionCobranza.CmbCodigoRetencion3CloseUp(
  Sender: TObject);
begin
  if (Length(Trim(CmbCodigoRetencion3.Value)) > 0) then
  begin
    CalcularRetencion(DataTipoRetencion3,TxtMontoRetencion3);
  end
  else
  begin
    TxtMontoRetencion3.Text := '';
  end;
end;

procedure TFrmAgregarDetalleLiquidacionCobranza.CmbCodigoRetencion4CloseUp(
  Sender: TObject);
begin
  if (Length(Trim(CmbCodigoRetencion4.Value)) > 0) then
  begin
    CalcularRetencion(DataTipoRetencion4,TxtMontoRetencion4);
  end
  else
  begin
    TxtMontoRetencion4.Text := '';
  end;
end;

procedure TFrmAgregarDetalleLiquidacionCobranza.CmbCodigoRetencion5CloseUp(
  Sender: TObject);
begin
  if (Length(Trim(CmbCodigoRetencion5.Value)) > 0) then
  begin
    CalcularRetencion(DataTipoRetencion5,TxtMontoRetencion5);
  end
  else
  begin
    TxtMontoRetencion5.Text := '';
  end;
end;

procedure TFrmAgregarDetalleLiquidacionCobranza.CmbCodigoRetencionIvaCloseUp(
  Sender: TObject);
begin
  if (Length(Trim(CmbCodigoRetencionIva.Value)) > 0) then
  begin
    CalcularRetencionIva(DataTipoRetencionIva,TxtMontoRetencionIva);
  end
  else
  begin
    TxtMontoRetencionIva.Text := '';
  end;
end;

procedure TFrmAgregarDetalleLiquidacionCobranza.FormCreate(Sender: TObject);
begin
  inherited;
  ComponenteFoco := nil;
  BusquedaDocumentoCuentaxCobrar := TBusquedaDocumentoCuentaxCobrar.Create;
  DetLiquidacionCobraza := TDetLiquidacionCobranza.Create;
  CuentaxCobrar := TCuentaxCobrar.Create;
  TipoRetencion := TTipoRetencion.Create;
  AnticipoCliente := TAnticipoCliente.Create;
  FacturaCliente := TFacturacionCliente.Create;
  NotaDebitoCliente := TNotaDebitoCliente.Create;
  NotaCreditoCliente := TNotaCreditoCliente.Create;
  Cliente := TCliente.Create;
  DataTipoRetencion1 := TipoRetencion.ObtenerComboISLR;
  DataTipoRetencion2 := TipoRetencion.ObtenerComboISLR;
  DataTipoRetencion3 := TipoRetencion.ObtenerComboISLR;
  DataTipoRetencion4 := TipoRetencion.ObtenerComboISLR;
  DataTipoRetencion5 := TipoRetencion.ObtenerComboISLR;
  DataTipoRetencionIva := TipoRetencion.ObtenerComboIVA;
  DataTipoRetencion1.First;
  DataTipoRetencion2.First;
  DataTipoRetencion3.First;
  DataTipoRetencion4.First;
  DataTipoRetencion5.First;
  DataTipoRetencionIva.First;
  DSTipoRetencion1.DataSet := DataTipoRetencion1;
  DSTipoRetencion2.DataSet := DataTipoRetencion2;
  DSTipoRetencion3.DataSet := DataTipoRetencion3;
  DSTipoRetencion4.DataSet := DataTipoRetencion4;
  DSTipoRetencion5.DataSet := DataTipoRetencion5;
  DSTipoRetencionIva.DataSet := DataTipoRetencionIva;
  ConfigurarRxDBLookupCombo(CmbCodigoRetencion1,DSTipoRetencion1,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoRetencion2,DSTipoRetencion2,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoRetencion3,DSTipoRetencion3,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoRetencion4,DSTipoRetencion4,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoRetencion5,DSTipoRetencion5,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoRetencionIva,DSTipoRetencionIva,'codigo','codigo;descripcion');
end;

procedure TFrmAgregarDetalleLiquidacionCobranza.Limpiar;
begin
  inherited;
  TxtDesde.Text := '';
  LblDescripcionDesde.Caption := '';
  TxtNumero.Text := '';
  LblMontoBase.Caption := '';
  TxtCodigoCliente.Text := '';
  LblRazonSocialCliente.Caption := '';
  CmbCodigoRetencion1.Value := '';
  TxtMontoRetencion1.Text := '';
  CmbCodigoRetencion2.Value := '';
  TxtMontoRetencion2.Text := '';
  CmbCodigoRetencion3.Value := '';
  TxtMontoRetencion3.Text := '';
  CmbCodigoRetencion4.Value := '';
  TxtMontoRetencion4.Text := '';
  CmbCodigoRetencion5.Value := '';
  TxtMontoRetencion5.Text := '';
end;

procedure TFrmAgregarDetalleLiquidacionCobranza.TxtCodigoExit(Sender: TObject);
begin
  Exit;
end;

procedure TFrmAgregarDetalleLiquidacionCobranza.TxtMontoRetencion1Change(
  Sender: TObject);
begin
  CalcularTotalRetencion;
end;

procedure TFrmAgregarDetalleLiquidacionCobranza.TxtMontoRetencion2Change(
  Sender: TObject);
begin
  CalcularTotalRetencion;
end;

procedure TFrmAgregarDetalleLiquidacionCobranza.TxtMontoRetencion3Change(
  Sender: TObject);
begin
  CalcularTotalRetencion;
end;

procedure TFrmAgregarDetalleLiquidacionCobranza.TxtMontoRetencion4Change(
  Sender: TObject);
begin
  CalcularTotalRetencion;
end;

procedure TFrmAgregarDetalleLiquidacionCobranza.TxtMontoRetencion5Change(
  Sender: TObject);
begin
  CalcularTotalRetencion;
end;

procedure TFrmAgregarDetalleLiquidacionCobranza.TxtMontoRetencionIvaChange(
  Sender: TObject);
begin
  CalcularTotalRetencion;
end;

function TFrmAgregarDetalleLiquidacionCobranza.Validar: Boolean;
var
  ListaErrores: TStringList;
  sw: Boolean;
begin
  ListaErrores := TStringList.Create;
  //Validando Existencia del Documento a Retener
  sw := false;
  with Data do
  begin
    First;
    while (not Eof) do
    begin
      if ((FieldValues['desde'] = BusquedaDocumentoCuentaxCobrar.GetDesde) and
          (FieldValues['numero_documento'] = BusquedaDocumentoCuentaxCobrar.GetNumeroDocumento) and
          (FieldValues['numero_cuota'] = BusquedaDocumentoCuentaxCobrar.GetNumeroCuota)) then
      begin
        sw := True;
        Break;
      end;      
      Next;
    end;    
  end;
  if (sw) then
  begin
    ListaErrores.Add('El Documento Ya Se Encuentra En La Lista!!!');
  end;
  //Validando Monto de Retencion
  if (TxtTotalRetencion.Value < 0) then
  begin
    ListaErrores.Add('El Total A Retener Debe Ser Mayor o Igual A Cero!!!');
  end;
  //Validando Monto del Abono
  if (CompareValue(TxtAbono.Value,BusquedaDocumentoCuentaxCobrar.GetTotal) = GreaterThanValue) then
  begin
    ListaErrores.Add('La Porci�n a Considerar No Debe Ser Mayor al Monto del Documento!!!');
  end;  
  //Validando Codigo de Retencion 1
  if ((Length(Trim(CmbCodigoRetencion1.Text)) > 0) and (TxtMontoRetencion1.Value <= 0)) then
  begin
    ListaErrores.Add('El Monto de Retencion 1 Debe Ser Mayor A Cero!!!');
  end;
  //Validando Codigo de Retencion 2
  if ((Length(Trim(CmbCodigoRetencion2.Text)) > 0) and (TxtMontoRetencion2.Value <= 0)) then
  begin
    ListaErrores.Add('El Monto de Retencion 2 Debe Ser Mayor A Cero!!!');
  end;
  //Validando Repeticion Codigo Retencion 2
  if ((Length(Trim(CmbCodigoRetencion2.Text)) > 0) and (CmbCodigoRetencion1.Text = CmbCodigoRetencion2.Text)) then
  begin
    ListaErrores.Add('La Retencion 2 Ya Fue Utilizada!!!');
  end;
  //Validando Codigo de Retencion 3
  if ((Length(Trim(CmbCodigoRetencion3.Text)) > 0) and (TxtMontoRetencion3.Value <= 0)) then
  begin
    ListaErrores.Add('El Monto de Retencion 3 Debe Ser Mayor A Cero!!!');
  end;
  //Validando Repeticion Codigo Retencion 3
  if ((Length(Trim(CmbCodigoRetencion3.Text)) > 0) and ((CmbCodigoRetencion1.Text = CmbCodigoRetencion3.Text) or (CmbCodigoRetencion2.Text = CmbCodigoRetencion3.Text))) then
  begin
    ListaErrores.Add('La Retencion 3 Ya Fue Utilizada!!!');
  end;
  //Validando Codigo de Retencion 4
  if ((Length(Trim(CmbCodigoRetencion4.Text)) > 0) and (TxtMontoRetencion4.Value <= 0)) then
  begin
    ListaErrores.Add('El Monto de Retencion 4 Debe Ser Mayor A Cero!!!');
  end;
  //Validando Repeticion Codigo Retencion 4
  if ((Length(Trim(CmbCodigoRetencion4.Text)) > 0) and ((CmbCodigoRetencion1.Text = CmbCodigoRetencion4.Text) or (CmbCodigoRetencion2.Text = CmbCodigoRetencion4.Text) or (CmbCodigoRetencion3.Text = CmbCodigoRetencion4.Text))) then
  begin
    ListaErrores.Add('La Retencion 4 Ya Fue Utilizada!!!');
  end;
  //Validando Codigo de Retencion 5
  if ((Length(Trim(CmbCodigoRetencion5.Text)) > 0) and (TxtMontoRetencion5.Value <= 0)) then
  begin
    ListaErrores.Add('El Monto de Retencion 5 Debe Ser Mayor A Cero!!!');
  end;
  //Validando Repeticion Codigo Retencion 5
  if ((Length(Trim(CmbCodigoRetencion5.Text)) > 0) and ((CmbCodigoRetencion1.Text = CmbCodigoRetencion5.Text) or (CmbCodigoRetencion2.Text = CmbCodigoRetencion5.Text) or (CmbCodigoRetencion3.Text = CmbCodigoRetencion5.Text) or (CmbCodigoRetencion4.Text = CmbCodigoRetencion5.Text))) then
  begin
    ListaErrores.Add('La Retencion 5 Ya Fue Utilizada!!!');
  end;
  //Validando Codigo de Retencion IVA
  if ((Length(Trim(CmbCodigoRetencionIva.Text)) > 0) and (TxtMontoRetencionIva.Value <= 0)) then
  begin
    ListaErrores.Add('El Monto de Retencion de IVA Debe Ser Mayor A Cero!!!');
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
