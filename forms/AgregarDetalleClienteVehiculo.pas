unit AgregarDetalleClienteVehiculo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GenericoAgregarDetalle, StdCtrls, Buttons, ExtCtrls, Mask,
  rxToolEdit, rxCurrEdit, DB, Utils;

type
  TFrmAgregarDetalleClienteVehiculo = class(TFrmGenericoAgregarDetalle)
    LblPlaca: TLabel;
    TxtPlaca: TEdit;
    TxtAnno: TCurrencyEdit;
    TxtKilometraje: TCurrencyEdit;
    LblMarca: TLabel;
    LblModelo: TLabel;
    LblAnno: TLabel;
    LblColor: TLabel;
    LblKilometraje: TLabel;
    TxtObservaciones: TMemo;
    TxtMarca: TEdit;
    TxtModelo: TEdit;
    TxtColor: TEdit;
    LblObservaciones: TLabel;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Private declarations }
    function Buscar: Boolean; override;
    procedure Agregar; override;
    procedure Limpiar; override;
  public
    { Public declarations }
  end;

var
  FrmAgregarDetalleClienteVehiculo: TFrmAgregarDetalleClienteVehiculo;

implementation

{$R *.dfm}

procedure TFrmAgregarDetalleClienteVehiculo.Agregar;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  (* Validando Informacion del Vehiculo *)
  //Validando Placa
  if (Length(Trim(TxtPlaca.Text)) = 0) then
  begin
    ListaErrores.Add('La Placa del Vehiculo No Puede Estar En Blanco!!!');
  end;
  //Validando Marca
  if (Length(Trim(TxtMarca.Text)) = 0) then
  begin
    ListaErrores.Add('La Marca del Vehiculo No Puede Estar En Blanco!!!');
  end;
  //Validando Modelo
  if (Length(Trim(TxtModelo.Text)) = 0) then
  begin
    ListaErrores.Add('El Modelo del Vehiculo No Puede Estar En Blanco!!!');
  end;
  //Validando Kilometraje
  if (Length(Trim(TxtKilometraje.Text)) = 0) and (TxtKilometraje.Value = 0) then
  begin
    ListaErrores.Add('El Kilometraje del Vehiculo Debe Ser Mayor a Cero (0 KM)!!!');
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    TxtPlaca.SetFocus;
  end
  else
  begin
    with Data do
    begin
      //Verificando si existe el codigo en el Grid
      if (Locate('placa',TxtPlaca.Text,[loCaseInsensitive])) then
      begin
        MessageBox(Self.Handle, Cadena('La Placa del Vehiculo Ya Fue Agregada!!!'), PChar(MsgTituloError), MB_ICONSTOP or MB_OK or MB_TOPMOST or MB_TASKMODAL);
        TxtPlaca.SetFocus;
        TxtPlaca.SelectAll;
      end
      else
      begin
        Append;
        FieldValues['placa'] := TxtPlaca.Text;
        FieldValues['marca'] := TxtMarca.Text;
        FieldValues['modelo'] := TxtModelo.Text;
        FieldValues['anno'] := TxtAnno.Value;
        FieldValues['color'] := TxtColor.Text;
        FieldValues['kilometraje'] := TxtKilometraje.Value;
        FieldValues['observaciones'] := TxtObservaciones.Text;
        Post;
      end;
    end;
    inherited;
  end;
end;

function TFrmAgregarDetalleClienteVehiculo.Buscar: Boolean;
begin
  Result := False;
end;

procedure TFrmAgregarDetalleClienteVehiculo.FormShow(Sender: TObject);
begin
  inherited;
  //Reasignando Formatos de Decimales
  with TxtAnno do
  begin
    DisplayFormat := '#,##0';
    DecimalPlaces := 0;
  end;
  with TxtKilometraje do
  begin
    DisplayFormat := '#,##0';
    DecimalPlaces := 0;
  end;
  TxtPlaca.SetFocus;
end;

procedure TFrmAgregarDetalleClienteVehiculo.Limpiar;
begin
  inherited;
  TxtPlaca.Text := '';
  TxtMarca.Text := '';
  TxtModelo.Text := '';
  TxtAnno.Text := '';
  TxtColor.Text := '';
  TxtKilometraje.Text := '';
  TxtObservaciones.Text := '';
end;

end.
