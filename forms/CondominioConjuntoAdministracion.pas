unit CondominioConjuntoAdministracion;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Generico, XPMan, ImgList, DB, Grids, DBGrids, StdCtrls, Buttons,
  ExtCtrls, ComCtrls, ToolWin, ClsCondominioConjuntoAdministracion, DBClient,
  Utils;

type
  TFrmCondominioConjuntoAdministracion = class(TFrmGenerico)
    LblCodigo: TLabel;
    TxtCodigo: TEdit;
    LblDescripcion: TLabel;
    TxtDescripcion: TEdit;
    TxtDireccionUbicacion: TMemo;
    LblPersonasContacto: TLabel;
    TxtPersonasContacto: TEdit;
    LblObservaciones: TLabel;
    TxtObservaciones: TMemo;
    LblDireccionUbicacion: TLabel;
    procedure GrdBusquedaCellClick(Column: TColumn);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    Clase: TCondominioConjuntoAdministracion;
    Data: TClientDataSet;
    procedure Agregar;
    procedure Editar;
    procedure Eliminar;
    procedure Guardar;
    procedure Cancelar;
    procedure Refrescar;
    procedure Imprimir;
    procedure Buscar;
    procedure Limpiar;
    procedure Asignar;
    procedure Obtener;
    procedure Mover;
  protected
    function Validar: Boolean; override;
  public
    { Public declarations }
  end;

var
  FrmCondominioConjuntoAdministracion: TFrmCondominioConjuntoAdministracion;

implementation

{$R *.dfm}

procedure TFrmCondominioConjuntoAdministracion.FormCreate(Sender: TObject);
begin
  ProcAgregar := Agregar;
  ProcEditar := Editar;
  ProcEliminar := Eliminar;
  ProcGuardar := Guardar;
  ProcCancelar := Cancelar;
  ProcRefrescar := Refrescar;
  ProcImprimir := Imprimir;
  ProcBuscar := Buscar;
  Clase := TCondominioConjuntoAdministracion.Create;
  Buscar;
end;

procedure TFrmCondominioConjuntoAdministracion.GrdBusquedaCellClick(Column: TColumn);
begin
  Mover;
end;

procedure TFrmCondominioConjuntoAdministracion.Agregar;
begin
  //Verificando
  Limpiar;
  TxtCodigo.SetFocus;
end;

procedure TFrmCondominioConjuntoAdministracion.Asignar;
begin
  with Clase do
  begin
    TxtCodigo.Text := GetCodigo;
    TxtDescripcion.Text := GetDescripcion;
    TxtDireccionUbicacion.Text := GetDireccionUbicacion;
    TxtPersonasContacto.Text := GetPersonasContacto;
    TxtObservaciones.Text := GetObservaciones
  end;
end;

procedure TFrmCondominioConjuntoAdministracion.Buscar;
begin
  Screen.Cursor := crSQLWait;
  if (Length(Trim(TxtCriterio.Text)) > 0) then
  begin
    Data := Clase.ObtenerListaCriterio(Trim(TxtCriterio.Text));
  end
  else
  begin
    Data := Clase.ObtenerLista;
  end;
  Data.First;
  DS.DataSet := Data;
  GrdBusqueda.Refresh;
  Screen.Cursor := crDefault;
end;

procedure TFrmCondominioConjuntoAdministracion.Cancelar;
begin
  TxtCodigo.Enabled := True;
end;

procedure TFrmCondominioConjuntoAdministracion.Editar;
begin
  //Se Busca el Elemento
  Clase.SetCodigo(Data.FieldValues['codigo']);
  Clase.Buscar;
  Asignar;
  TxtCodigo.Enabled := False;
  TxtDescripcion.SetFocus;
end;

procedure TFrmCondominioConjuntoAdministracion.Eliminar;
begin
  //Se Busca el Elemento
  Clase.SetCodigo(Data.FieldValues['codigo']);
  Clase.Buscar;
  Clase.Eliminar;
  MessageBox(Self.Handle, 'El Conjunto de Administración Ha Sido Eliminado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  Refrescar;
  Limpiar;
end;

procedure TFrmCondominioConjuntoAdministracion.Guardar;
begin
  if (Agregando) then
  begin
    //Agregando...
    //Se Busca el Codigo
    Clase.SetCodigo(TxtCodigo.Text);
    if (Clase.Buscar) then
    begin
      MessageBox(Self.Handle, 'El Código Ya Existe!!!', PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      TxtCodigo.SetFocus;
      Abort;
    end
    else
    begin
      Obtener;
      Clase.Insertar;
      MessageBox(Self.Handle, 'El Conjunto de Administración Ha Sido Agregado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    end;
  end
  else
  begin
    //Editando...
    Obtener;
    Clase.Modificar;
    MessageBox(Self.Handle, 'El Conjunto de Administración Ha Sido Editado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    TxtCodigo.Enabled := True;
  end;
  Refrescar;
  Limpiar;
end;

procedure TFrmCondominioConjuntoAdministracion.Imprimir;
begin

end;

procedure TFrmCondominioConjuntoAdministracion.Limpiar;
begin
  TxtCodigo.Text := '';
  TxtDescripcion.Text := '';
  TxtDireccionUbicacion.Text := '';
  TxtPersonasContacto.Text := '';
  TxtObservaciones.Text := '';
end;

procedure TFrmCondominioConjuntoAdministracion.Mover;
var
  DSTemp: TDataSet;
begin
  if (DS.DataSet.RecordCount > 0) then
  begin
    DSTemp := DS.DataSet;
    with Clase do
    begin
      SetCodigo(DSTemp.FieldValues['codigo']);
      SetDescripcion(DSTemp.FieldValues['descripcion']);
      SetDireccionUbicacion(DSTemp.FieldValues['direccion_ubicacion']);
      SetPersonasContacto(DSTemp.FieldValues['personas_contacto']);
      SetObservaciones(DSTemp.FieldValues['observaciones']);
    end;
    Asignar;
  end
  else
  begin
    Limpiar;
  end;
end;

procedure TFrmCondominioConjuntoAdministracion.Obtener;
begin
  with Clase do
  begin
    SetCodigo(TxtCodigo.Text);
    SetDescripcion(TxtDescripcion.Text);
    SetDireccionUbicacion(TxtDireccionUbicacion.Text);
    SetPersonasContacto(TxtPersonasContacto.Text);
    SetObservaciones(TxtObservaciones.Text);
  end;
end;

procedure TFrmCondominioConjuntoAdministracion.Refrescar;
begin
  TxtCriterio.Text := '';
  Buscar;
end;

function TFrmCondominioConjuntoAdministracion.Validar: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  (* Validando Informacion del Concepto *)
  //Validando Codigo
  if (Length(Trim(TxtCodigo.Text)) = 0) then
  begin
    ListaErrores.Add('El Código No Puede Estar En Blanco!!!');
  end;
  //Validando Descripcion
  if (Length(Trim(TxtDescripcion.Text)) = 0) then
  begin
    ListaErrores.Add('La Descripción No Puede Estar En Blanco!!!');
  end;
  //Validando Direccion de Ubicacion
  if (Length(Trim(TxtDireccionUbicacion.Text)) = 0) then
  begin
    ListaErrores.Add('La Dirección de Ubicación No Puede Estar En Blanco!!!');
  end;
  //Validando Personas Contacto
  if (Length(Trim(TxtPersonasContacto.Text)) = 0) then
  begin
    ListaErrores.Add('Las Personas Contacto No Pueden Estar En Blanco!!!');
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
