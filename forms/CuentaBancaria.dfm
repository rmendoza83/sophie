inherited FrmCuentaBancaria: TFrmCuentaBancaria
  Caption = 'Sophie - Cuentas Bancarias'
  ExplicitWidth = 598
  ExplicitHeight = 400
  PixelsPerInch = 96
  TextHeight = 13
  inherited Pag: TPageControl
    ActivePage = TabBusqueda
    inherited TabBusqueda: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 24
      ExplicitWidth = 584
      ExplicitHeight = 304
      inherited GrdBusqueda: TDBGrid
        OnCellClick = GrdBusquedaCellClick
      end
    end
    inherited TabDatos: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 24
      ExplicitWidth = 584
      ExplicitHeight = 304
      object LblEntidad: TLabel
        Left = 68
        Top = 35
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = 'Entidad:'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object LblCodigo: TLabel
        Left = 72
        Top = 11
        Width = 41
        Height = 13
        Alignment = taRightJustify
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object LblControlChequera: TLabel
        Left = 8
        Top = 203
        Width = 107
        Height = 13
        Alignment = taRightJustify
        Caption = 'Control Chequeras:'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object LblAgencia: TLabel
        Left = 65
        Top = 59
        Width = 48
        Height = 13
        Alignment = taRightJustify
        Caption = 'Agencia:'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object LblTipo: TLabel
        Left = 318
        Top = 59
        Width = 27
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo:'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object LblNumero: TLabel
        Left = 66
        Top = 83
        Width = 47
        Height = 13
        Alignment = taRightJustify
        Caption = 'N'#250'mero:'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object LblFechaApertura: TLabel
        Left = 23
        Top = 107
        Width = 90
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fecha Apertura:'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object LblTelefonos: TLabel
        Left = 55
        Top = 131
        Width = 58
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tel'#233'fonos:'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object LblFax: TLabel
        Left = 322
        Top = 131
        Width = 23
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fax:'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object LblEmail: TLabel
        Left = 80
        Top = 155
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Email:'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object LblWebsite: TLabel
        Left = 296
        Top = 155
        Width = 49
        Height = 13
        Alignment = taRightJustify
        Caption = 'Website:'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object LblCodigoZona: TLabel
        Left = 82
        Top = 180
        Width = 31
        Height = 13
        Alignment = taRightJustify
        Caption = 'Zona:'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object TxtNombreZona: TDBText
        Left = 299
        Top = 180
        Width = 77
        Height = 13
        AutoSize = True
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LblContacto: TLabel
        Left = 291
        Top = 107
        Width = 54
        Height = 13
        Alignment = taRightJustify
        Caption = 'Contacto:'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object TxtCodigo: TEdit
        Left = 119
        Top = 8
        Width = 150
        Height = 21
        TabOrder = 0
      end
      object TxtEntidad: TEdit
        Left = 119
        Top = 32
        Width = 150
        Height = 21
        TabOrder = 1
      end
      object ChkControlChequera: TCheckBox
        Left = 119
        Top = 203
        Width = 97
        Height = 17
        TabOrder = 13
        OnClick = ChkControlChequeraClick
      end
      object TxtAgencia: TEdit
        Left = 119
        Top = 56
        Width = 150
        Height = 21
        TabOrder = 2
      end
      object TxtTipo: TEdit
        Left = 351
        Top = 56
        Width = 150
        Height = 21
        TabOrder = 3
      end
      object TxtNumero: TEdit
        Left = 119
        Top = 80
        Width = 150
        Height = 21
        TabOrder = 4
      end
      object TxtTelefonos: TEdit
        Left = 119
        Top = 128
        Width = 150
        Height = 21
        TabOrder = 7
      end
      object TxtFax: TEdit
        Left = 351
        Top = 128
        Width = 150
        Height = 21
        TabOrder = 8
      end
      object TxtEmail: TEdit
        Left = 119
        Top = 152
        Width = 150
        Height = 21
        TabOrder = 9
      end
      object TxtWebsite: TEdit
        Left = 351
        Top = 152
        Width = 150
        Height = 21
        TabOrder = 10
      end
      object GrpControlChequera: TGroupBox
        Left = 32
        Top = 222
        Width = 469
        Height = 51
        Caption = 'Control de Chequera - Secuencia:'
        Enabled = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 14
        object LblNumChequeInicial: TLabel
          Left = 46
          Top = 22
          Width = 37
          Height = 13
          Alignment = taRightJustify
          Caption = 'Inicial:'
          Color = clBtnFace
          ParentColor = False
        end
        object LblNumChequeFinal: TLabel
          Left = 279
          Top = 22
          Width = 29
          Height = 13
          Alignment = taRightJustify
          Caption = 'Final:'
          Color = clBtnFace
          ParentColor = False
        end
        object TxtNumChequeInicial: TCurrencyEdit
          Left = 87
          Top = 19
          Width = 150
          Height = 21
          Margins.Left = 4
          Margins.Top = 1
          DecimalPlaces = 0
          DisplayFormat = '#,##0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object TxtNumChequeFinal: TCurrencyEdit
          Left = 312
          Top = 19
          Width = 150
          Height = 21
          Margins.Left = 4
          Margins.Top = 1
          DecimalPlaces = 0
          DisplayFormat = '#,##0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
      end
      object CmbCodigoZona: TRxDBLookupCombo
        Left = 119
        Top = 177
        Width = 150
        Height = 21
        DropDownCount = 8
        TabOrder = 11
      end
      object BtnBuscarZona: TBitBtn
        Left = 270
        Top = 177
        Width = 23
        Height = 22
        TabOrder = 12
        OnClick = BtnBuscarZonaClick
        Glyph.Data = {
          36060000424D3606000000000000360400002800000020000000100000000100
          08000000000000020000850E0000850E00000001000000000000000000003300
          00006600000099000000CC000000FF0000000033000033330000663300009933
          0000CC330000FF33000000660000336600006666000099660000CC660000FF66
          000000990000339900006699000099990000CC990000FF99000000CC000033CC
          000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
          0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
          330000333300333333006633330099333300CC333300FF333300006633003366
          33006666330099663300CC663300FF6633000099330033993300669933009999
          3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
          330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
          66006600660099006600CC006600FF0066000033660033336600663366009933
          6600CC336600FF33660000666600336666006666660099666600CC666600FF66
          660000996600339966006699660099996600CC996600FF99660000CC660033CC
          660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
          6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
          990000339900333399006633990099339900CC339900FF339900006699003366
          99006666990099669900CC669900FF6699000099990033999900669999009999
          9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
          990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
          CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
          CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
          CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
          CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
          CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
          FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
          FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
          FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
          FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
          000000808000800000008000800080800000C0C0C00080808000191919004C4C
          4C00B2B2B200E5E5E5005A1E1E00783C3C0096646400C8969600FFC8C800465F
          82005591B9006EB9D7008CD2E600B4E6F000D8E9EC0099A8AC00646F7100E2EF
          F100C56A31000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000EEEEEEEEEEEE
          EEEE0909EE0909EE0909EEEEEEEEEEEEEEEE8181EE8181EE8181EEEEEEEEEEEE
          EEEE1009EE1009EE1009EEEEEEEEEEEEEEEEE281EEE281EEE281EEEEEEEEEEEE
          EEB4D8EEEEEEEEEEEEEEEEEEEEEEEEEEEEE281EEEEEEEEEEEEEEEEEEEEEEEEEE
          B46C6CD8EEEEEEEEEEEEEEEEEEEEEEEEE2818181EEEEEEEEEEEEEEEEEEEEEEEE
          B46CD86CD8EEEEEEEEEEEEEEEEEEEEEEE281818181EEEEEEEEEEEEEEEEEEEEEE
          B46C6CD86CD8EEEEEEEEEEEEEEEEEEEEE28181818181EEEEEEEEEEEEEEEEEED7
          5E6C6C6CB46CD8EEEEEEEEEEEEEEEED781818181E28181EEEEEEEEEEEEEEEED7
          89896CB4B4B46CD8EEEEEEEEEEEEEED7ACAC81E2E2E28181EEEEEEEEEEEED789
          89D7D7B4C7C7C76CEEEEEEEEEEEED7ACACD7D7E2ACACAC81EEEEEEEEEED78989
          D7D7D7D76C6C6CEEEEEEEEEEEED7ACACD7D7D7D7818181EEEEEEEEEED78989D7
          D7D75E5EEEEEEEEEEEEEEEEED7ACACD7D7D78181EEEEEEEEEEEEEED78989D7D7
          D75EEEEEEEEEEEEEEEEEEED7ACACD7D7D781EEEEEEEEEEEEEEEED78989D7D7D7
          5EB4EEEEEEEEEEEEEEEED7ACACD7D7D781E2EEEEEEEEEEEEEEEE5E89D7D7D75E
          B4EEEEEEEEEEEEEEEEEE81ACD7D7D781E2EEEEEEEEEEEEEEEEEEEE5ED7D75EEE
          EEEEEEEEEEEEEEEEEEEEEE81D7D781EEEEEEEEEEEEEEEEEEEEEEEEEE5E5EEEEE
          EEEEEEEEEEEEEEEEEEEEEEEE8181EEEEEEEEEEEEEEEEEEEEEEEE}
        NumGlyphs = 2
      end
      object DatFechaApertura: TDateEdit
        Left = 119
        Top = 104
        Width = 150
        Height = 21
        DefaultToday = True
        DialogTitle = 'Seleccione Fecha'
        NumGlyphs = 2
        YearDigits = dyFour
        TabOrder = 5
        Text = '15/11/2011'
      end
      object TxtContacto: TEdit
        Left = 351
        Top = 104
        Width = 150
        Height = 21
        TabOrder = 6
      end
    end
  end
  object DSZona: TDataSource
    Left = 8
    Top = 296
  end
end
