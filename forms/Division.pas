unit Division;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Generico, XPMan, ImgList, DB, Grids, DBGrids, StdCtrls, Buttons,
  ExtCtrls, ComCtrls, ToolWin, ClsDivision, DBClient, Utils;

type
  TFrmDivision = class(TFrmGenerico)
    LblCodigo: TLabel;
    TxtCodigo: TEdit;
    LblDescripcion: TLabel;
    TxtDescripcion: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure GrdBusquedaCellClick(Column: TColumn);
  private
    { Private declarations }
    Clase: TDivision;
    Data: TClientDataSet;
    procedure Agregar;
    procedure Editar;
    procedure Eliminar;
    procedure Guardar;
    procedure Cancelar;
    procedure Refrescar;
    procedure Imprimir;
    procedure Buscar;
    procedure Limpiar;
    procedure Asignar;
    procedure Obtener;
    procedure Mover;
  protected
    function Validar: Boolean; override;
  public
    { Public declarations }
  end;

var
  FrmDivision: TFrmDivision;

implementation

{$R *.dfm}

procedure TFrmDivision.FormCreate(Sender: TObject);
begin
  ProcAgregar := Agregar;
  ProcEditar := Editar;
  ProcEliminar := Eliminar;
  ProcGuardar := Guardar;
  ProcCancelar := Cancelar;
  ProcRefrescar := Refrescar;
  ProcImprimir := Imprimir;
  ProcBuscar := Buscar;
  Clase := TDivision.Create;
  Buscar;
end;

procedure TFrmDivision.GrdBusquedaCellClick(Column: TColumn);
begin
  Mover;
end;

procedure TFrmDivision.Agregar;
begin
  //Verificando
  Limpiar;
  TxtCodigo.SetFocus;
end;

procedure TFrmDivision.Asignar;
begin
  with Clase do
  begin
    TxtCodigo.Text := GetCodigo;
    TxtDescripcion.Text := GetDescripcion;
  end;
end;

procedure TFrmDivision.Buscar;
begin
  Screen.Cursor := crSQLWait;
  if (Length(Trim(TxtCriterio.Text)) > 0) then
  begin
    Data := Clase.ObtenerListaCriterio(Trim(TxtCriterio.Text));
  end
  else
  begin
    Data := Clase.ObtenerLista;
  end;
  Data.First;
  DS.DataSet := Data;
  GrdBusqueda.Refresh;
  Screen.Cursor := crDefault;
end;

procedure TFrmDivision.Cancelar;
begin
  TxtCodigo.Enabled := True;
end;

procedure TFrmDivision.Editar;
begin
  //Se Busca el Elemento
  Clase.SetCodigo(Data.FieldValues['codigo']);
  Clase.Buscar;
  Asignar;
  TxtCodigo.Enabled := False;
  TxtDescripcion.SetFocus;
end;

procedure TFrmDivision.Eliminar;
begin
  //Se Busca el Elemento
  Clase.SetCodigo(Data.FieldValues['codigo']);
  Clase.Buscar;
  Clase.Eliminar;
  MessageBox(Self.Handle, 'La Division Ha Sido Eliminada Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  Refrescar;
  Limpiar;
end;

procedure TFrmDivision.Guardar;
begin
  if (Agregando) then
  begin
    //Agregando...
    //Se Busca el Codigo
    Clase.SetCodigo(TxtCodigo.Text);
    if (Clase.Buscar) then
    begin
      MessageBox(Self.Handle, 'El C�digo Ya Existe!!!', PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      TxtCodigo.SetFocus;
      Abort;
    end
    else
    begin
      Obtener;
      Clase.Insertar;
      MessageBox(Self.Handle, 'La Division  Ha Sido Agregada Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    end;
  end
  else
  begin
    //Editando...
    Obtener;
    Clase.Modificar;
    MessageBox(Self.Handle, 'La Division Ha Sido Editada Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    TxtCodigo.Enabled := True;
  end;
  Refrescar;
  Limpiar;
end;

procedure TFrmDivision.Imprimir;
begin

end;

procedure TFrmDivision.Limpiar;
begin
  TxtCodigo.Text := '';
  TxtDescripcion.Text := '';
end;

procedure TFrmDivision.Mover;
var
  DSTemp: TDataSet;
begin
  if (DS.DataSet.RecordCount > 0) then
  begin
    DSTemp := DS.DataSet;
    with Clase do
    begin
      SetCodigo(DSTemp.FieldValues['codigo']);
      SetDescripcion(DSTemp.FieldValues['descripcion']);
    end;
    Asignar;
  end
  else
  begin
    Limpiar;
  end;
end;

procedure TFrmDivision.Obtener;
begin
  with Clase do
  begin
    SetCodigo(TxtCodigo.Text);
    SetDescripcion(TxtDescripcion.Text);
  end;
end;

procedure TFrmDivision.Refrescar;
begin
  TxtCriterio.Text := '';
  Buscar;
end;

function TFrmDivision.Validar: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  (* Validando Informacion del Concepto *)
  //Validando Codigo
  if (Length(Trim(TxtCodigo.Text)) = 0) then
  begin
    ListaErrores.Add('El C�digo No Puede Estar En Blanco!!!');
  end;
  //Validando Descripcion
  if (Length(Trim(TxtDescripcion.Text)) = 0) then
  begin
    ListaErrores.Add('La Descripci�n No Puede Estar En Blanco!!!');
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
