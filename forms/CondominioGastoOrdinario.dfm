inherited FrmCondominioGastoOrdinario: TFrmCondominioGastoOrdinario
  Caption = 'Sophie - Gastos Ordinarios'
  ExplicitWidth = 640
  ExplicitHeight = 476
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelMaestro: TPanel
    Height = 77
    Enabled = True
    ExplicitHeight = 77
    object GrpConjuntoAdministracion: TGroupBox
      Left = 16
      Top = 6
      Width = 601
      Height = 57
      Caption = 'Seleccione Conjunto de Administraci'#243'n: '
      TabOrder = 0
      object LblCodigoConjuntoAdministracion: TLabel
        Left = 10
        Top = 22
        Width = 159
        Height = 13
        Alignment = taRightJustify
        Caption = 'Conjunto de Administraci'#243'n:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object TxtCodigoConjuntoAdministracion: TDBText
        Left = 331
        Top = 22
        Width = 162
        Height = 13
        AutoSize = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object CmbCodigoConjuntoAdministracion: TRxDBLookupCombo
        Left = 175
        Top = 19
        Width = 150
        Height = 21
        DropDownCount = 20
        DropDownWidth = 400
        TabOrder = 0
        OnChange = CmbCodigoConjuntoAdministracionChange
      end
    end
  end
  inherited PageDetalles: TPageControl
    Top = 113
    Height = 314
    ExplicitTop = 113
    ExplicitHeight = 314
    inherited TabDetalle: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 24
      ExplicitWidth = 626
      ExplicitHeight = 286
      inherited GridDetalle: TDBGrid
        Height = 286
        ParentFont = False
        TitleFont.Style = [fsBold]
        Columns = <
          item
            Expanded = False
            FieldName = 'codigo_gasto'
            Title.Caption = 'C'#243'digo Gasto'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'descripcion'
            Title.Caption = 'Descripci'#243'n'
            Width = 200
            Visible = True
          end
          item
            Alignment = taRightJustify
            Expanded = False
            FieldName = 'monto'
            Title.Caption = 'Monto'
            Width = 100
            Visible = True
          end>
      end
    end
  end
  inherited Data: TClientDataSet
    object Datacodigo_conjunto_administracion: TStringField
      FieldName = 'codigo_conjunto_administracion'
      Size = 10
    end
    object Datacodigo_gasto: TStringField
      FieldName = 'codigo_gasto'
      Size = 10
    end
    object Datadescripcion: TStringField
      FieldName = 'descripcion'
      Size = 40
    end
    object Datamonto: TFloatField
      FieldName = 'monto'
      DisplayFormat = '#,##0.0000'
      Precision = 4
    end
  end
  object DSConjuntoAdministracion: TDataSource
    Left = 592
    Top = 388
  end
end
