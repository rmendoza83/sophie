inherited FrmAgregarDetalleVendedor: TFrmAgregarDetalleVendedor
  Caption = 'Sophie - Agregar Detalle Tipo Comisiones..'
  ClientHeight = 136
  ClientWidth = 348
  OnCreate = FormCreate
  ExplicitWidth = 354
  ExplicitHeight = 160
  PixelsPerInch = 96
  TextHeight = 13
  inherited LblCodigo: TLabel
    Left = 27
    ExplicitLeft = 27
  end
  object LblPorcentaje: TLabel [1]
    Left = 8
    Top = 64
    Width = 56
    Height = 13
    Caption = 'Porcentaje:'
  end
  inherited TxtCodigo: TEdit
    Left = 70
    ParentFont = False
    ExplicitLeft = 70
  end
  inherited PanelBotones: TPanel
    Top = 96
    Width = 348
    ExplicitTop = 96
    ExplicitWidth = 348
  end
  inherited BtnBuscar: TBitBtn
    Left = 319
    OnClick = BtnBuscarClick
    ExplicitLeft = 319
  end
  object TxtDescripcionProducto: TEdit
    Left = 70
    Top = 35
    Width = 249
    Height = 21
    Enabled = False
    TabOrder = 3
    OnKeyDown = TxtCodigoKeyDown
  end
  object TxtPorcentaje: TCurrencyEdit
    Left = 70
    Top = 61
    Width = 75
    Height = 21
    Margins.Left = 4
    Margins.Top = 1
    DecimalPlaces = 4
    DisplayFormat = '#,##0.0000%'
    Enabled = False
    TabOrder = 4
    Value = 1.000000000000000000
  end
end
