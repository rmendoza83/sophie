inherited FrmCondominioConjuntoAdministracion: TFrmCondominioConjuntoAdministracion
  Caption = 'Sophie - Conjuntos de Administraci'#243'n'
  PixelsPerInch = 96
  TextHeight = 13
  inherited Pag: TPageControl
    inherited TabDatos: TTabSheet
      object LblCodigo: TLabel
        Left = 72
        Top = 11
        Width = 41
        Height = 13
        Alignment = taRightJustify
        Caption = 'C'#243'digo:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblDescripcion: TLabel
        Left = 45
        Top = 35
        Width = 68
        Height = 13
        Alignment = taRightJustify
        Caption = 'Descripci'#243'n:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblDireccionUbicacion: TLabel
        Left = 1
        Top = 59
        Width = 112
        Height = 13
        Alignment = taRightJustify
        Caption = 'Direccion Ubicaci'#243'n:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblPersonasContacto: TLabel
        Left = 4
        Top = 151
        Width = 109
        Height = 13
        Alignment = taRightJustify
        Caption = 'Personas Contacto:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblObservaciones: TLabel
        Left = 27
        Top = 175
        Width = 86
        Height = 13
        Alignment = taRightJustify
        Caption = 'Observaciones:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object TxtCodigo: TEdit
        Left = 119
        Top = 8
        Width = 150
        Height = 21
        TabOrder = 0
      end
      object TxtDescripcion: TEdit
        Left = 119
        Top = 32
        Width = 300
        Height = 21
        TabOrder = 1
      end
      object TxtDireccionUbicacion: TMemo
        Left = 119
        Top = 56
        Width = 300
        Height = 89
        MaxLength = 255
        TabOrder = 2
      end
      object TxtPersonasContacto: TEdit
        Left = 119
        Top = 148
        Width = 300
        Height = 21
        MaxLength = 255
        TabOrder = 3
      end
      object TxtObservaciones: TMemo
        Left = 119
        Top = 172
        Width = 300
        Height = 89
        TabOrder = 4
      end
    end
  end
end
