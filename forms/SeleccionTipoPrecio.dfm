object FrmSeleccionTipoPrecio: TFrmSeleccionTipoPrecio
  Left = 0
  Top = 0
  BorderStyle = bsToolWindow
  Caption = 'Sophie - Seleccione Tipo de Precio...'
  ClientHeight = 216
  ClientWidth = 294
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object LblTitulo: TLabel
    Left = 8
    Top = 8
    Width = 156
    Height = 13
    Caption = 'Tipo de Precio [PRODUCTO]:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
end
