unit SeleccionTipoPrecio;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ClsProducto, ClsProductoPrecio, DBClient;

type
  TArrayButtons = array of TButton;

  TFrmSeleccionTipoPrecio = class(TForm)
    LblTitulo: TLabel;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    CodigoProducto: string;
    ArrayButtons: TArrayButtons;
    Producto: TProducto;
    ProductoPrecio: TProductoPrecio;
    DataProductoPrecio: TClientDataSet;
  public
    { Public declarations }
    CodigoTipoPrecioProductoSeleccionado: string;
    constructor Crear(AOwner: TComponent; ACodigoProducto: string);
    procedure ClickBoton(Sender: TObject); 
  end;

var
  FrmSeleccionTipoPrecio: TFrmSeleccionTipoPrecio;

implementation

{$R *.dfm}

{ TFrmSeleccionTipoPrecio }

procedure TFrmSeleccionTipoPrecio.ClickBoton(Sender: TObject);
begin
  CodigoTipoPrecioProductoSeleccionado := ArrayButtons[(Sender as TButton).Tag].Hint;
  Close;
end;

constructor TFrmSeleccionTipoPrecio.Crear(AOwner: TComponent;
  ACodigoProducto: string);
var
  i: Integer;
begin
  inherited Create(AOwner);
  CodigoProducto := ACodigoProducto;
  SetLength(ArrayButtons,0);
  Producto := TProducto.Create;
  ProductoPrecio := TProductoPrecio.Create;
  //Generando Botones Dinamicos para el Tipo de Productos
  Producto.SetCodigo(ACodigoProducto);
  Producto.BuscarCodigo;
  DataProductoPrecio := ProductoPrecio.ObtenerCodigoProducto(ACodigoProducto);
  SetLength(ArrayButtons,DataProductoPrecio.RecordCount);
  DataProductoPrecio.First;
  for i := 0 to DataProductoPrecio.RecordCount - 1 do
  begin
    ArrayButtons[i] := TButton.Create(Self);
    with ArrayButtons[i] do
    begin
      Caption := DataProductoPrecio.FieldValues['descripcion'] + ' - ' + FormatFloat('#,##0.00',DataProductoPrecio.FieldValues['precio']);
      Hint := DataProductoPrecio.FieldValues['codigo_tipo_precio_producto'];
      ShowHint := False;
      Parent := Self;
      Height := 45;
      Width := 278;
      Left := 8;
      Top := 27 + (52 * i);
      OnClick := ClickBoton;
      Tag := i;
    end;
    DataProductoPrecio.Next;
  end;
  LblTitulo.Caption := 'Tipo de Precio [' + Producto.GetCodigo + ']:';
  Height := 27 + (52 * DataProductoPrecio.RecordCount) + 27;
end;

procedure TFrmSeleccionTipoPrecio.FormShow(Sender: TObject);
begin
  if (Length(ArrayButtons) > 0) then
  begin
    ArrayButtons[0].SetFocus;
  end;
end;

end.
