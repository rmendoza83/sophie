unit AgregarDetalleMovimientoInventario;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GenericoAgregarDetalle, DB, StdCtrls, Buttons, ExtCtrls, Mask,
  rxToolEdit, rxCurrEdit, ClsProducto, ClsDetMovimientoInventario,
  GenericoBuscador, DBClient, Utils;

type
  TFrmAgregarDetalleMovimientoInventario = class(TFrmGenericoAgregarDetalle)
    LblCantidad: TLabel;
    TxtCantidad: TCurrencyEdit;
    TxtDescripcionProducto: TEdit;
    procedure BtnBuscarClick(Sender: TObject);
    procedure TxtCantidadKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    Producto: TProducto;
  protected
    function Buscar: Boolean; override;
    procedure Agregar; override;
    procedure Limpiar; override;
  public
    { Public declarations }
  end;

var
  FrmAgregarDetalleMovimientoInventario: TFrmAgregarDetalleMovimientoInventario;

implementation

{$R *.dfm}

procedure TFrmAgregarDetalleMovimientoInventario.Agregar;
begin
  with Data do
  begin
    //Verificando si existe el codigo en el Grid
    if (Locate('codigo_producto',TxtCodigo.Text,[loCaseInsensitive])) then
    begin
      Edit;
      FieldValues['cantidad'] := FieldValues['cantidad'] + TxtCantidad.Value;
    end
    else
    begin
      Append;
      FieldValues['codigo_producto'] := Producto.GetCodigo;
      FieldValues['referencia'] := Producto.GetReferencia;
      FieldValues['descripcion'] := Producto.GetDescripcion;
      FieldValues['cantidad'] := TxtCantidad.Value;
      FieldValues['precio'] := Producto.GetPrecio;
      FieldValues['p_descuento'] := Producto.GetPDescuento;
      FieldValues['monto_descuento'] := (Producto.GetPrecio * TxtCantidad.Value * Producto.GetPDescuento) / 100;
      FieldValues['impuesto'] := Producto.GetPrecio - (Producto.GetPrecio / (1 + (Producto.GetPIvaVenta / 100)));
      FieldValues['p_iva'] := Producto.GetPIvaVenta;
      FieldValues['total'] := Producto.GetPrecio * TxtCantidad.Value;
      FieldValues['id_movimiento_inventario'] := -1;
    end;
    Post;
  end;
  inherited;
end;

procedure TFrmAgregarDetalleMovimientoInventario.BtnBuscarClick(Sender: TObject);
begin
  inherited;
  Buscador(Self,Producto,'codigo','Buscar Producto',TxtCodigo,nil);
  if (Length(Trim(TxtCodigo.Text)) > 0) then
  begin
    ComponenteFoco.SetFocus;
  end;
end;

function TFrmAgregarDetalleMovimientoInventario.Buscar: Boolean;
begin
  Producto.SetCodigo(TxtCodigo.Text);
  Result := Producto.BuscarCodigo;
  if (Result) then
  begin
    TxtDescripcionProducto.Text := Producto.GetDescripcion;
  end
  else
  begin
    MessageBox(Self.Handle, Cadena('El Codigo No Existe!!!'), PChar(MsgTituloError), MB_ICONSTOP or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Limpiar;
  end;
end;

procedure TFrmAgregarDetalleMovimientoInventario.FormCreate(Sender: TObject);
begin
  inherited;
  ComponenteFoco := (TxtCantidad as TWinControl);
  Producto := TProducto.Create;
end;

procedure TFrmAgregarDetalleMovimientoInventario.Limpiar;
begin
  inherited;
  TxtDescripcionProducto.Text := '';
  TxtCantidad.Value := 1;
end;

procedure TFrmAgregarDetalleMovimientoInventario.TxtCantidadKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  case Key of
    VK_RETURN:
      Agregar;    
  end;
end;

end.
