inherited FrmEsquemaPago: TFrmEsquemaPago
  Caption = 'Sophie - Esquemas de Pagos'
  ExplicitWidth = 598
  ExplicitHeight = 400
  PixelsPerInch = 96
  TextHeight = 13
  inherited Pag: TPageControl
    ActivePage = TabBusqueda
    OnChanging = nil
    inherited TabBusqueda: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 24
      ExplicitWidth = 584
      ExplicitHeight = 304
      inherited GrdBusqueda: TDBGrid
        OnCellClick = GrdBusquedaCellClick
      end
    end
    inherited TabDatos: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 24
      ExplicitWidth = 584
      ExplicitHeight = 304
      object LblCodigo: TLabel
        Left = 72
        Top = 11
        Width = 41
        Height = 13
        Alignment = taRightJustify
        Caption = 'C'#243'digo:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblDescripcion: TLabel
        Left = 45
        Top = 35
        Width = 68
        Height = 13
        Alignment = taRightJustify
        Caption = 'Descripci'#243'n:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label1: TLabel
        Left = 69
        Top = 60
        Width = 44
        Height = 13
        Alignment = taRightJustify
        Caption = 'Cr'#233'dito:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 76
        Top = 83
        Width = 37
        Height = 13
        Alignment = taRightJustify
        Caption = 'Inicial:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 10
        Top = 108
        Width = 102
        Height = 13
        Alignment = taRightJustify
        Caption = 'Porcentaje Inicial:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label4: TLabel
        Left = 71
        Top = 133
        Width = 42
        Height = 13
        Alignment = taRightJustify
        Caption = 'Cuotas:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label5: TLabel
        Left = 43
        Top = 161
        Width = 69
        Height = 13
        Alignment = taRightJustify
        Caption = 'D'#237'as Cuotas:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 17
        Top = 188
        Width = 95
        Height = 13
        Alignment = taRightJustify
        Caption = 'Cantidad Cuotas:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label7: TLabel
        Left = 86
        Top = 218
        Width = 27
        Height = 13
        Alignment = taRightJustify
        Caption = 'D'#237'as:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label8: TLabel
        Left = 156
        Top = 218
        Width = 80
        Height = 13
        Alignment = taRightJustify
        Caption = 'Cantidad D'#237'as:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label9: TLabel
        Left = 68
        Top = 243
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = 'Inter'#233's:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label10: TLabel
        Left = 144
        Top = 243
        Width = 92
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tasa de Inter'#233's:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label11: TLabel
        Left = 40
        Top = 268
        Width = 73
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tasa Ordinal:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object TxtCodigo: TEdit
        Left = 119
        Top = 8
        Width = 150
        Height = 21
        TabOrder = 0
      end
      object TxtDescripcion: TEdit
        Left = 119
        Top = 32
        Width = 152
        Height = 21
        TabOrder = 1
      end
      object TxtCredito: TCheckBox
        Left = 119
        Top = 59
        Width = 97
        Height = 17
        TabOrder = 2
      end
      object TxtInicial: TCheckBox
        Left = 119
        Top = 82
        Width = 97
        Height = 17
        TabOrder = 3
      end
      object TxtP_Inicial: TCurrencyEdit
        Left = 119
        Top = 105
        Width = 81
        Height = 21
        Margins.Left = 4
        Margins.Top = 1
        DisplayFormat = '0.00;'
        TabOrder = 4
      end
      object TxtCuotas: TCheckBox
        Left = 119
        Top = 132
        Width = 97
        Height = 18
        TabOrder = 5
      end
      object TxtDiasCuota: TCurrencyEdit
        Left = 119
        Top = 158
        Width = 36
        Height = 21
        Margins.Left = 4
        Margins.Top = 1
        DecimalPlaces = 0
        DisplayFormat = '0'
        TabOrder = 6
      end
      object TxtCantidadCuota: TCurrencyEdit
        Left = 119
        Top = 185
        Width = 36
        Height = 21
        Margins.Left = 4
        Margins.Top = 1
        DecimalPlaces = 0
        DisplayFormat = '0'
        TabOrder = 7
      end
      object TxtDias: TCheckBox
        Left = 119
        Top = 217
        Width = 15
        Height = 18
        TabOrder = 8
      end
      object TxtCantidadDias: TCurrencyEdit
        Left = 243
        Top = 215
        Width = 36
        Height = 21
        Margins.Left = 4
        Margins.Top = 1
        DecimalPlaces = 0
        DisplayFormat = '0'
        TabOrder = 9
      end
      object TxtInteres: TCheckBox
        Left = 119
        Top = 242
        Width = 15
        Height = 18
        TabOrder = 10
      end
      object TxtTasaInteres: TCurrencyEdit
        Left = 243
        Top = 240
        Width = 81
        Height = 21
        Margins.Left = 4
        Margins.Top = 1
        DisplayFormat = '0.00;'
        TabOrder = 11
      end
      object TxtTasaOrdinal: TCheckBox
        Left = 119
        Top = 267
        Width = 16
        Height = 18
        TabOrder = 12
      end
    end
  end
end
