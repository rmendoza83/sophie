inherited FrmAgregarDetalleClienteCuentaBancaria: TFrmAgregarDetalleClienteCuentaBancaria
  ClientHeight = 256
  ExplicitWidth = 350
  ExplicitHeight = 280
  PixelsPerInch = 96
  TextHeight = 13
  inherited LblCodigo: TLabel
    Visible = False
  end
  object LblNumeroCuenta: TLabel [1]
    Left = 2
    Top = 30
    Width = 90
    Height = 13
    Alignment = taRightJustify
    Caption = 'Numero Cuenta:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object LblEntidad: TLabel [2]
    Left = 47
    Top = 54
    Width = 45
    Height = 13
    Alignment = taRightJustify
    Caption = 'Entidad:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object LblTipo: TLabel [3]
    Left = 65
    Top = 78
    Width = 27
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object LblEmail: TLabel [4]
    Left = 59
    Top = 102
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'Email:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object LblObservaciones: TLabel [5]
    Left = 6
    Top = 126
    Width = 86
    Height = 13
    Alignment = taRightJustify
    Caption = 'Observaciones:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  inherited TxtCodigo: TEdit
    Visible = False
  end
  inherited PanelBotones: TPanel
    Top = 216
    TabOrder = 7
    ExplicitTop = 216
  end
  inherited BtnBuscar: TBitBtn
    TabOrder = 1
    Visible = False
  end
  object TxtNumeroCuenta: TEdit
    Left = 98
    Top = 27
    Width = 150
    Height = 21
    MaxLength = 20
    TabOrder = 2
  end
  object TxtObservaciones: TMemo
    Left = 98
    Top = 123
    Width = 238
    Height = 89
    MaxLength = 256
    ScrollBars = ssVertical
    TabOrder = 6
  end
  object TxtEntidad: TEdit
    Left = 98
    Top = 51
    Width = 200
    Height = 21
    MaxLength = 40
    TabOrder = 3
  end
  object TxtTipo: TEdit
    Left = 98
    Top = 75
    Width = 100
    Height = 21
    MaxLength = 20
    TabOrder = 4
  end
  object TxtEmail: TEdit
    Left = 98
    Top = 99
    Width = 150
    Height = 21
    MaxLength = 20
    TabOrder = 5
  end
end
