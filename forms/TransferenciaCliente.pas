unit TransferenciaCliente;

{$I sophie.inc}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GenericoDetalle, DB, DBClient, ImgList, XPMan, ComCtrls, Grids,
  DBGrids, ExtCtrls, ToolWin, StdCtrls, Buttons, rxCurrEdit, Mask, rxToolEdit,
  RxLookup, ClsTransferenciaCliente, ClsDetTransferenciaCliente, ClsCliente,
  ClsClienteCuentaBancaria, ClsZona, ClsMoneda, ClsReporte, ClsParametro, Utils,
  GenericoBuscador, ClsBusquedaTransferenciaCliente
  {$IFDEF DELPHI10}, ImageList{$ENDIF};

type
  TFrmTransferenciaCliente = class(TFrmGenericoDetalle)
    LblNumero: TLabel;
    LblCodigoCliente: TLabel;
    LblCodigoZona: TLabel;
    LblMoneda: TLabel;
    LblFechaIngreso: TLabel;
    LblFechaContable: TLabel;
    LblTotal: TLabel;
    TxtNumero: TEdit;
    CmbCodigoCliente: TRxDBLookupCombo;
    DatFechaIngreso: TDateEdit;
    DatFechaContable: TDateEdit;
    CmbCodigoZona: TRxDBLookupCombo;
    CmbCodigoMoneda: TRxDBLookupCombo;
    TxtTotal: TCurrencyEdit;
    BtnBuscarCliente: TBitBtn;
    BtnBuscarZona: TBitBtn;
    BtnBuscarMoneda: TBitBtn;
    TabConcepto: TTabSheet;
    TabObservaciones: TTabSheet;
    TxtConcepto: TRichEdit;
    TxtObservaciones: TRichEdit;
    Datacodigo_cliente: TStringField;
    Datanumero_cuenta_cliente: TStringField;
    Datacodigo_cuenta: TStringField;
    Datacodigo_tipo_operacion_bancaria: TStringField;
    Datanumero: TStringField;
    Datafecha: TDateField;
    Datamonto: TFloatField;
    Dataid_operacion_bancaria: TIntegerField;
    Dataid_transferencia_cliente: TIntegerField;
    DSCliente: TDataSource;
    DSMoneda: TDataSource;
    DSZona: TDataSource;
    procedure FormCreate(Sender: TObject);
    procedure CmbCodigoClienteExit(Sender: TObject);
    procedure CmbCodigoClienteChange(Sender: TObject);
    procedure DataAfterPost(DataSet: TDataSet);
    procedure TBtnEliminarClick(Sender: TObject);
    procedure BtnBuscarMonedaClick(Sender: TObject);
    procedure BtnBuscarZonaClick(Sender: TObject);
    procedure BtnBuscarClienteClick(Sender: TObject);
  private
    { Private declarations }
    Clase: TTransferenciaCliente;
    DetTransferenciaCliente: TDetTransferenciaCliente;
    BuscadorTransferenciaCliente: TBusquedaTransferenciaCliente;
    Cliente: TCliente;
    ClienteCuentaBancaria: TClienteCuentaBancaria;
    Zona: TZona;
    Moneda: TMoneda;
    Reporte: TReporte;
    Parametro: TParametro;
    DataBuscadorTransferenciaCliente: TClientDataSet;
    DataCliente: TClientDataSet;
    DataZona: TClientDataSet;
    DataMoneda: TClientDataSet;
    procedure Agregar;
    procedure Editar;
    procedure Eliminar;
    procedure Guardar;
    procedure Cancelar;
    procedure Refrescar;
    procedure Imprimir;
    procedure Buscar;
    procedure Limpiar;
    procedure Asignar;
    procedure Obtener;
    procedure Mover;
    procedure GuardarDetalles;
    procedure CalcularMontos;
  protected
    { Protected declarations }
    function Validar: Boolean; override;
    procedure Detalle; override;
  public
    { Public declarations }
  end;

var
  FrmTransferenciaCliente: TFrmTransferenciaCliente;

implementation

uses
  ActividadSophie,
  PModule,
  AgregarDetalleTransferenciaCliente
  {$IFDEF USE_RAVE_REPORTS}
  , DMRR
  {$ENDIF}
  {$IFDEF USE_FAST_REPORTS}
  , DMFR
  {$ENDIF}
  ;

{$R *.dfm}

{ TFrmTransferenciaCliente }

procedure TFrmTransferenciaCliente.Agregar;
begin
  //Verificando
  Limpiar;
  //Asignando Valores por Defecto
  CmbCodigoZona.Value := Parametro.ObtenerValor('codigo_zona');
  CmbCodigoMoneda.Value := Parametro.ObtenerValor('codigo_moneda');
  CmbCodigoCliente.SetFocus;
end;

procedure TFrmTransferenciaCliente.Asignar;
begin
  with Clase do
  begin
    TxtNumero.Text := GetNumero;
    CmbCodigoCliente.KeyValue := GetCodigoCliente;
    CmbCodigoZona.KeyValue := GetCodigoZona;
    CmbCodigoMoneda.KeyValue := GetCodigoMoneda;
    DatFechaIngreso.Date := GetFechaIngreso;
    DatFechaContable.Date := GetFechaContable;
    TxtConcepto.Text := GetConcepto;
    TxtObservaciones.Text := GetObservaciones;
    TxtTotal.Value := GetTotal;
    Data.CloneCursor(DetTransferenciaCliente.ObtenerIdTransferenciaCliente(GetIdTransferenciaCliente),False,True);
    DS.DataSet := Data;
  end;
end;

procedure TFrmTransferenciaCliente.BtnBuscarClienteClick(Sender: TObject);
begin
  Buscador(Self,Cliente,'codigo','Buscar Cliente',nil,DataCliente);
  CmbCodigoCliente.KeyValue := DataCliente.FieldValues['codigo'];
  CmbCodigoClienteExit(Sender);
end;

procedure TFrmTransferenciaCliente.BtnBuscarMonedaClick(Sender: TObject);
begin
  Buscador(Self,Moneda,'codigo','Buscar Moneda',nil,DataMoneda);
  CmbCodigoMoneda.KeyValue := DataMoneda.FieldValues['codigo'];
end;

procedure TFrmTransferenciaCliente.BtnBuscarZonaClick(Sender: TObject);
begin
  Buscador(Self,Zona,'codigo','Buscar Zona',nil,DataZona);
  CmbCodigoZona.KeyValue := DataZona.FieldValues['codigo'];
end;

procedure TFrmTransferenciaCliente.Buscar;
begin
  if (Buscador(Self,BuscadorTransferenciaCliente,'numero','Buscar Transferencia',nil,DataBuscadorTransferenciaCliente)) then
  begin
    Clase.SetNumero(DataBuscadorTransferenciaCliente.FieldValues['numero']);
    Clase.BuscarNumero;
    Asignar;
  end;
end;

procedure TFrmTransferenciaCliente.CalcularMontos;
var
  Total: Currency;
begin
  Total := 0;
  with Data do
  begin
    DisableControls;
    First;
    while (not Eof) do
    begin
      Total := Total + FieldValues['monto'];
      Next;
    end;
    EnableControls;
  end;
  TxtTotal.Value := Total;
end;

procedure TFrmTransferenciaCliente.Cancelar;
begin
  if (Agregando) then
  begin
    Limpiar;
  end
  else
  begin
    Clase.SetNumero(TxtNumero.Text);
    Clase.BuscarNumero;
    Asignar;
  end;
end;

procedure TFrmTransferenciaCliente.CmbCodigoClienteChange(Sender: TObject);
begin
  SB.Panels[0].Text := DataCliente.FieldValues['razon_social'];
end;

procedure TFrmTransferenciaCliente.CmbCodigoClienteExit(Sender: TObject);
begin
  if (Length(Trim(CmbCodigoCliente.Text)) > 0) then
  begin
    if (Agregando) then
    begin
      Cliente.SetCodigo(CmbCodigoCliente.Value);
      Cliente.BuscarCodigo;
      CmbCodigoZona.Value := Cliente.GetCodigoZona;
    end;
  end;
end;

procedure TFrmTransferenciaCliente.DataAfterPost(DataSet: TDataSet);
begin
  CalcularMontos;
end;

procedure TFrmTransferenciaCliente.Detalle;
begin
  inherited;
  FrmAgregarDetalleTransferenciaCliente := TFrmAgregarDetalleTransferenciaCliente.CrearDetalle(Self,Data,CmbCodigoCliente.KeyValue);
  FrmAgregarDetalleTransferenciaCliente.ShowModal;
  FreeAndNil(FrmAgregarDetalleTransferenciaCliente);
end;

procedure TFrmTransferenciaCliente.Editar;
begin
  if (Length(Trim(TxtNumero.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
end;

procedure TFrmTransferenciaCliente.Eliminar;
begin
  //Se Busca La Transferencia!
  Clase.SetNumero(TxtNumero.Text);
  Clase.BuscarNumero;
  //Se Eliminar Los Movimientos Bancarios
  
  //Se Eliminan Los Detalles
  DetTransferenciaCliente.EliminarIdTransferenciaCliente(Clase.GetIdTransferenciaCliente);
  Clase.Eliminar;
  MessageBox(Self.Handle, 'La Transferencia a Cliente Se Ha Eliminado Con Exito!!!',PChar(MsgTituloInformacion), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  Refrescar;
  Limpiar;
end;

procedure TFrmTransferenciaCliente.FormCreate(Sender: TObject);
begin
  inherited;
  ProcAgregar := Agregar;
  ProcEditar := Editar;
  ProcEliminar := Eliminar;
  ProcGuardar := Guardar;
  ProcCancelar := Cancelar;
  ProcRefrescar := Refrescar;
  ProcImprimir := Imprimir;
  ProcBuscar := Buscar;
  FrmActividadSophie.Show;
  FrmActividadSophie.Actividad('Cargando N�cleo...');
  Clase := TTransferenciaCliente.Create;
  //Inicializando DataSets Secundarios
  BuscadorTransferenciaCliente := TBusquedaTransferenciaCliente.Create;
  Cliente := TCliente.Create;
  ClienteCuentaBancaria := TClienteCuentaBancaria.Create;
  Zona := TZona.Create;
  Moneda := TMoneda.Create;
  Reporte := TReporte.Create;
  Parametro := TParametro.Create;
  DetTransferenciaCliente := TDetTransferenciaCliente.Create;
  FrmActividadSophie.Actividad('Cargando Clientes...');
  DataCliente := Cliente.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Zonas...');
  DataZona := Zona.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Monedas...');
  DataMoneda := Moneda.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Resto del N�cleo...');
  DataBuscadorTransferenciaCliente := BuscadorTransferenciaCliente.ObtenerLista;
  DataCliente.First;
  DataZona.First;
  DataMoneda.First;
  DataBuscadorTransferenciaCliente.First;
  DSCliente.DataSet := DataCliente;
  DSZona.DataSet := DataZona;
  DSMoneda.DataSet := DataMoneda;
  ConfigurarRxDBLookupCombo(CmbCodigoCliente,DSCliente,'codigo','codigo;razon_social');
  ConfigurarRxDBLookupCombo(CmbCodigoZona,DSZona,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoMoneda,DSMoneda,'codigo','codigo;descripcion');
  DS.DataSet := Data;
  GridDetalle.DataSource := DS;
  FrmActividadSophie.Close;
end;

procedure TFrmTransferenciaCliente.Guardar;
var
  SwImpresoraTicket: Boolean;
begin
  CalcularMontos;
  Obtener;
  if (Agregando) then
  begin
    //Agregando...
    //Guardando Encabezado
    Clase.Insertar;
    TxtNumero.Text := Clase.GetNumero;
    //Guardando Detalles
    GuardarDetalles;
    //Guardando Operaciones Bancarias
    //GuardarOperacionesBancarias;
    MessageBox(Self.Handle, 'La Transferencia a Cliente Ha Sido Agregada Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    //Se Imprime la Factura
    MessageBox(Self.Handle, 'Presione "Aceptar" Para Proceder A Imprimir La Transferencia!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    //Verificando Impresora de Ticket
    Parametro.SetNombreParametro('usar_impresora_ticket');
    SwImpresoraTicket := False;
    if (Parametro.Buscar) then
    begin
      if (Parametro.ObtenerValorAsBoolean) then
      begin
        SwImpresoraTicket := True;
      end;
    end;
    if (SwImpresoraTicket) then
    begin
      with Reporte do
      begin
        AgregarParametro('Numero',Clase.GetNumero);
        AgregarParametro('MontoLetra',NumeroALetras(Clase.GetTotal));
        ImprimeReporte('TransferenciaClienteTicket',trDocumento);
      end;
    end
    else
    begin
      with Reporte do
      begin
        AgregarParametro('Numero',Clase.GetNumero);
        AgregarParametro('MontoLetra',NumeroALetras(Clase.GetTotal));
        ImprimeReporte('TransferenciaCliente',trDocumento);
      end;
    end;
  end
  else
  begin
    //Editando...
    Clase.Modificar;
    //Actualizando Detalles (Eliminando Anteriores y Cargando los Nuevos)
    DetTransferenciaCliente.EliminarIdTransferenciaCliente(Clase.GetIdTransferenciaCliente);
    //Guardando Detalles
    GuardarDetalles;
    MessageBox(Self.Handle, 'La Transferencia a Cliente Ha Sido Editada Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end;
end;

procedure TFrmTransferenciaCliente.GuardarDetalles;
begin
  with Data do
  begin
    DisableControls;
    First;
    while (not Eof) do
    begin
      with DetTransferenciaCliente do
      begin
        SetCodigoCliente(FieldValues['codigo_cliente']);
        SetNumeroCuentaCliente(FieldValues['numero_cuenta_cliente']);
        SetCodigoCuenta(FieldValues['codigo_cuenta']);
        SetCodigoTipoOperacion(FieldValues['codigo_tipo_operacion_bancaria']);
        SetNumero(FieldValues['numero']);
        SetFecha(FieldValues['fecha']);
        SetMonto(FieldValues['monto']);
        SetIdOperacionBancaria(FieldValues['id_operacion_bancaria']);
        SetIdTransferenciaCliente(Clase.GetIdTransferenciaCliente);
        Insertar;
      end;
      Next;
    end;
    EnableControls;
  end;
end;

procedure TFrmTransferenciaCliente.Imprimir;
var
  SwImpresoraTicket: Boolean;
begin
  if (Length(Trim(TxtNumero.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
  Parametro.SetNombreParametro('usar_impresora_ticket');
  SwImpresoraTicket := False;
  if (Parametro.Buscar) then
  begin
    if (Parametro.ObtenerValorAsBoolean) then
    begin
      SwImpresoraTicket := True;
    end;
  end;
  if (SwImpresoraTicket) then
  begin
    with Reporte do
    begin
      AgregarParametro('Numero',Clase.GetNumero);
      AgregarParametro('MontoLetra',NumeroALetras(Clase.GetTotal));
      ImprimeReporte('TransferenciaClienteTicket',trDocumento);
    end;
  end
  else
  begin
    with Reporte do
    begin
      AgregarParametro('Numero',Clase.GetNumero);
      AgregarParametro('MontoLetra',NumeroALetras(Clase.GetTotal));
      ImprimeReporte('TransferenciaCliente',trDocumento);
    end;
  end;
end;

procedure TFrmTransferenciaCliente.Limpiar;
begin
  TxtNumero.Text := '';
  //NumeroPedido
  CmbCodigoCliente.KeyValue := '';
  CmbCodigoZona.KeyValue := '';
  CmbCodigoMoneda.KeyValue := '';
  DatFechaIngreso.Date := Date;
  DatFechaContable.Date := Date;
  TxtConcepto.Text := '';
  TxtObservaciones.Text := '';
  TxtTotal.Value := 0;
  //Status Bar
  SB.Panels[0].Text := '';
  Data.EmptyDataSet;
  TabDetalle.TabVisible := True;
  PageDetalles.ActivePage := TabDetalle;
end;

procedure TFrmTransferenciaCliente.Mover;
begin

end;

procedure TFrmTransferenciaCliente.Obtener;
begin
  with Clase do
  begin
    SetNumero(TxtNumero.Text);
    SetCodigoCliente(CmbCodigoCliente.KeyValue);
    SetCodigoZona(CmbCodigoZona.KeyValue);
    SetCodigoMoneda(CmbCodigoMoneda.KeyValue);
    SetFechaIngreso(DatFechaIngreso.Date);
    SetFechaContable(DatFechaContable.Date);
    SetConcepto(TxtConcepto.Text);
    SetObservaciones(TxtObservaciones.Text);
    SetTotal(TxtTotal.Value);
    SetLoginUsuario(P.User.GetLoginUsuario);
    if (Agregando) then
    begin
      SetTiempoIngreso(Now);
      SetIdTransferenciaCliente(-1);
    end;
  end;
end;

procedure TFrmTransferenciaCliente.Refrescar;
begin
  //Se Actualiza El DataBuscador
  DataBuscadorTransferenciaCliente := BuscadorTransferenciaCliente.ObtenerLista;
  //Se Actualizan Los demas Componentes
  DataCliente := Cliente.ObtenerCombo;
  DataZona := Zona.ObtenerCombo;
  DataMoneda := Moneda.ObtenerCombo;
  DataCliente.First;
  DataZona.First;
  DataMoneda.First;
  DataBuscadorTransferenciaCliente.First;
  DSCliente.DataSet := DataCliente;
  DSZona.DataSet := DataZona;
  DSMoneda.DataSet := DataMoneda;
end;

procedure TFrmTransferenciaCliente.TBtnEliminarClick(Sender: TObject);
begin
  if (Length(Trim(TxtNumero.Text)) > 0) then
  begin
    inherited;
  end
  else
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
end;

function TFrmTransferenciaCliente.Validar: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  //Validando Cliente
  if (Length(Trim(CmbCodigoCliente.Text)) = 0) then
  begin
    ListaErrores.Add('El Cliente No Puede Estar En Blanco!!!');
  end;
  //Validando Monto
  if (TxtTotal.Value <= 0) then
  begin
    ListaErrores.Add('El Total Debe Ser Positivo!!!');
  end;
  if (Data.RecordCount = 0) then
  begin
    ListaErrores.Add('El Detalle De La Factura No Puede Estar Vacio!!!');
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
