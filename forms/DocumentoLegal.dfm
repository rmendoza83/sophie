object FrmDocumentoLegal: TFrmDocumentoLegal
  Left = 0
  Top = 0
  BorderStyle = bsToolWindow
  Caption = 'Sophie - Documento Legal (Serie / N'#176' de Control)'
  ClientHeight = 155
  ClientWidth = 344
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object LblSerie: TLabel
    Left = 77
    Top = 60
    Width = 58
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#176' de Serie:'
  end
  object LblNumeroControl: TLabel
    Left = 66
    Top = 82
    Width = 69
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#176' de Control:'
  end
  object LblDesde: TLabel
    Left = 101
    Top = 16
    Width = 34
    Height = 13
    Alignment = taRightJustify
    Caption = 'Desde:'
  end
  object TxtDesde: TLabel
    Left = 141
    Top = 16
    Width = 3
    Height = 13
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object LblNumeroDocumento: TLabel
    Left = 47
    Top = 38
    Width = 88
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#176' de Documento:'
  end
  object TxtNumeroDocumento: TLabel
    Left = 141
    Top = 38
    Width = 3
    Height = 13
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object TxtSerie: TEdit
    Left = 141
    Top = 57
    Width = 100
    Height = 21
    TabOrder = 0
  end
  object TxtNumeroControl: TEdit
    Left = 141
    Top = 79
    Width = 100
    Height = 21
    TabOrder = 1
  end
  object PanelBotones: TPanel
    Left = 0
    Top = 115
    Width = 344
    Height = 40
    Align = alBottom
    TabOrder = 2
    object Aceptar: TBitBtn
      Left = 8
      Top = 8
      Width = 75
      Height = 25
      Caption = '&Aceptar'
      TabOrder = 0
      OnClick = AceptarClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333330000333333333333333333333333F33333333333
        00003333344333333333333333388F3333333333000033334224333333333333
        338338F3333333330000333422224333333333333833338F3333333300003342
        222224333333333383333338F3333333000034222A22224333333338F338F333
        8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
        33333338F83338F338F33333000033A33333A222433333338333338F338F3333
        0000333333333A222433333333333338F338F33300003333333333A222433333
        333333338F338F33000033333333333A222433333333333338F338F300003333
        33333333A222433333333333338F338F00003333333333333A22433333333333
        3338F38F000033333333333333A223333333333333338F830000333333333333
        333A333333333333333338330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
    object BtnCerrar: TBitBtn
      Left = 261
      Top = 8
      Width = 75
      Height = 25
      Caption = '&Cerrar'
      TabOrder = 1
      OnClick = BtnCerrarClick
      Kind = bkClose
    end
  end
end
