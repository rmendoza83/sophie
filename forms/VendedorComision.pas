unit VendedorComision;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Generico, XPMan, ImgList, DB, Grids, DBGrids, StdCtrls, Buttons,
  ExtCtrls, ComCtrls, ToolWin, RxLookup, ClsVendedorComision, ClsTipoComision,
  ClsVendedor, Utils, DBClient, DBCtrls;

type
  TFrmVendedorComision = class(TFrmGenerico)
    LblVendedor: TLabel;
    CmbCodigoVendedor: TRxDBLookupCombo;
    BtnBuscarCodigoVendedor: TBitBtn;
    LblCodigoTipoComision: TLabel;
    CmbCodigoTipoComision: TRxDBLookupCombo;
    BtnBuscarCodigoTipoComision: TBitBtn;
    TxtVendedor: TDBText;
    TxtTipoComision: TDBText;
    DSZona: TDataSource;
    DSTipoProveedor: TDataSource;
    DSVendedor: TDataSource;
    DSTipoComision: TDataSource;
    procedure BtnBuscarCodigoTipoComisionClick(Sender: TObject);
    procedure BtnBuscarCodigoVendedorClick(Sender: TObject);
    procedure GrdBusquedaCellClick(Column: TColumn);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    Clase: TVendedorComision;
    Vendedor: TVendedor;
    TipoComision: TTipoComision;
    Data: TClientDataSet;
    DataVendedor: TClientDataSet;
    DataTipoComision: TClientDataSet;
    procedure Agregar;
    procedure Editar;
    procedure Eliminar;
    procedure Guardar;
    procedure Cancelar;
    procedure Refrescar;
    procedure Imprimir;
    procedure Buscar;
    procedure Limpiar;
    procedure Asignar;
    procedure Obtener;
    procedure Mover;
  protected
    function Validar: Boolean; override;
  public
    { Public declarations }
  end;

var
  FrmVendedorComision: TFrmVendedorComision;

implementation

uses
  GenericoBuscador;

{$R *.dfm}

{ TFrmVendedorComision }

procedure TFrmVendedorComision.Agregar;
begin
  //Verificando
  Limpiar;
  CmbCodigoVendedor.SetFocus
end;

procedure TFrmVendedorComision.Asignar;
begin
  with Clase do
  begin
    CmbCodigoVendedor.KeyValue := GetCodigoVendedor;
    CmbCodigoTipoComision.KeyValue := GetCodigoTipoComision;
  end;
end;

procedure TFrmVendedorComision.BtnBuscarCodigoTipoComisionClick(
  Sender: TObject);
begin
  Buscador(Self,TipoComision,'codigo','Busqueda de Tipo Comision',nil,DataTipoComision);
  CmbCodigoTipoComision.KeyValue := DataTipoComision.FieldValues['codigo'];
end;

procedure TFrmVendedorComision.BtnBuscarCodigoVendedorClick(Sender: TObject);
begin
  Buscador(Self,Vendedor,'codigo','Busqueda de Vendedor',nil,DataVendedor);
  CmbCodigoVendedor.KeyValue := DataVendedor.FieldValues['codigo'];
end;

procedure TFrmVendedorComision.Buscar;
begin
  Screen.Cursor := crSQLWait;
  if (Length(Trim(TxtCriterio.Text)) > 0) then
  begin
    Data := Clase.ObtenerListaCriterio(Trim(TxtCriterio.Text));
  end
  else
  begin
    Data := Clase.ObtenerLista;
  end;
  Data.First;
  DS.DataSet := Data;
  GrdBusqueda.Refresh;
  Screen.Cursor := crDefault;
end;

procedure TFrmVendedorComision.Cancelar;
begin
  CmbCodigoVendedor.Enabled := True;
end;

procedure TFrmVendedorComision.Editar;
begin
  //Se Busca el Elemento
  Clase.SetCodigoVendedor(Data.FieldValues['codigo_vendedor']);
  Clase.Buscar;
  Asignar;
end;

procedure TFrmVendedorComision.Eliminar;
begin
  //Se Busca el Elemento
  Clase.SetCodigoVendedor(Data.FieldValues['codigo_vendedor']);
  Clase.Buscar;
  Clase.Eliminar;
  MessageBox(Self.Handle, 'El Registro de Comisi�n del Vendedor Se Ha Eliminado Con Exito!!!',PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  Refrescar;
  Limpiar;
end;

procedure TFrmVendedorComision.FormCreate(Sender: TObject);
begin
  inherited;
  ProcAgregar := Agregar;
  ProcEditar := Editar;
  ProcEliminar := Eliminar;
  ProcGuardar := Guardar;
  ProcCancelar := Cancelar;
  ProcRefrescar := Refrescar;
  ProcImprimir := Imprimir;
  ProcBuscar := Buscar;
  Clase := TVendedorComision.Create;
  //Inicializando DataSets Secundarios
  Vendedor := TVendedor.Create;
  TipoComision := TTipoComision.Create;
  DataVendedor := Vendedor.ObtenerLista;
  DataTipoComision := TipoComision.ObtenerLista;
  DataVendedor.First;
  DataTipoComision.First;
  DSVendedor.DataSet := DataVendedor;
  DSTipoComision.DataSet := DataTipoComision;
  ConfigurarRxDBLookupCombo(CmbCodigoVendedor,DSVendedor,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoTipoComision,DSTipoComision,'codigo','codigo;descripcion');
  TxtVendedor.DataSource := DSVendedor;
  TxtVendedor.DataField := 'descripcion';
  TxtTipoComision.DataSource := DSTipoComision;
  TxtTipoComision.DataField := 'descripcion';
  Buscar;
end;

procedure TFrmVendedorComision.GrdBusquedaCellClick(Column: TColumn);
begin
  Mover;
end;

procedure TFrmVendedorComision.Guardar;
begin
  if (Agregando) then
  begin
    //Agregando...
    //Se Busca el Codigo
    Clase.SetCodigoVendedor(CmbCodigoVendedor.Text);
    if (Clase.Buscar) then
    begin
      MessageBox(Self.Handle, 'El C�digo Ya Existe!!!', PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      CmbCodigoVendedor.SetFocus;
      Abort;
    end
    else
    begin
      Obtener;
      Clase.Insertar;
      MessageBox(Self.Handle, 'El Registro de Comisi�n Ha Sido Agregado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    end;
  end
  else
  begin
    //Editando...
    Obtener;
    Clase.Modificar;
    MessageBox(Self.Handle, 'El Registro de Comisi�n Ha Sido Editado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    CmbCodigoVendedor.Enabled := True;
  end;
  Refrescar;
  Limpiar;
end;

procedure TFrmVendedorComision.Imprimir;
begin

end;

procedure TFrmVendedorComision.Limpiar;
begin
  CmbCodigoVendedor.KeyValue := '';
  CmbCodigoTipoComision.KeyValue := '';
end;

procedure TFrmVendedorComision.Mover;
var
  DSTemp: TDataSet;
begin
  if (DS.DataSet.RecordCount > 0) then
  begin
    DSTemp := DS.DataSet;
    with Clase do
    begin
      SetCodigoVendedor(DSTemp.FieldValues['codigo_vendedor']);
      SetCodigoTipoComision(DSTemp.FieldValues['codigo_tipo_comision']);
    end;
    Asignar;
  end
  else
  begin
    Limpiar;
  end;
end;

procedure TFrmVendedorComision.Obtener;
begin
  with Clase do
  begin
    SetCodigoVendedor(CmbCodigoVendedor.Text);
    SetCodigoTipoComision(CmbCodigoTipoComision.Text);
  end;
end;

procedure TFrmVendedorComision.Refrescar;
begin
  TxtCriterio.Text := '';
  Buscar;
  //Se Actualizan Los demas Componentes
  DataVendedor := Vendedor.ObtenerLista;
  DataTipoComision := TipoComision.ObtenerLista;
  DataVendedor.First;
  DataTipoComision.First;
  DSVendedor.DataSet := DataVendedor;
  DSTipoComision.DataSet := DataTipoComision;
end;

function TFrmVendedorComision.Validar: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  (* Validando Informacion del Tipo del Codigo Vendedor *)
  //Validando Codigo
  if (Length(Trim(CmbCodigoVendedor.Text)) = 0) then
  begin
    ListaErrores.Add('El C�digo del Vendedor No Puede Estar En Blanco!!!');
  end;
  //Validando Descripcion
  if (Length(Trim(CmbCodigoTipoComision.Text)) = 0) then
  begin
    ListaErrores.Add('El C�digo del Tipo de Comisi�n No Puede Estar En Blanco !!!');
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
