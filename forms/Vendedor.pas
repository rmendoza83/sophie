unit Vendedor;

{$I sophie.inc}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Generico, XPMan, ImgList, DB, Grids, DBGrids, StdCtrls, Buttons,
  ExtCtrls, ComCtrls, ToolWin, ClsVendedor, DBClient, Utils, RxLookup, DBCtrls,
  ClsZona, GenericoDetalle, ClsTipoComision, ClsBusquedaVendedor,
  ClsVendedorComision{$IFDEF DELPHI10}, ImageList{$ENDIF};

type
  TFrmVendedor = class(TFrmGenericoDetalle)
    LblCodigo: TLabel;
    TxtCodigo: TEdit;
    LblDescripcion: TLabel;
    TxtDescripcion: TEdit;
    LblCodigoZona: TLabel;
    CmbCodigoZona: TRxDBLookupCombo;
    BtnBuscarZona: TBitBtn;
    TxtZona: TDBText;
    LblActivo: TLabel;
    ChkActivo: TCheckBox;
    DSZona: TDataSource;
    DSTipoComision: TDataSource;
    Datacodigo: TStringField;
    Datadecripcion: TStringField;
    Dataactivo: TBooleanField;
    Dataporcentaje: TFloatField;
    procedure BtnBuscarZonaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TBtnEliminarClick(Sender: TObject);
  private
    { Private declarations }
    Clase: TVendedor;
    BuscadorVendedor: TBusquedaVendedor;
    Zona: TZona;
    TipoComision: TTipoComision;
    VendedorComision: TVendedorComision;
    DataZona: TClientDataSet;
    DataTipoComision: TClientDataSet;
    DataBuscadorVendedor: TClientDataSet;
    procedure Agregar;
    procedure Editar;
    procedure Eliminar;
    procedure Guardar;
    procedure Cancelar;
    procedure Refrescar;
    procedure Imprimir;
    procedure Buscar;
    procedure Limpiar;
    procedure Asignar;
    procedure Obtener;
    procedure Mover;
    procedure GuardarDetalles;
  protected
    function Validar: Boolean; override;
    procedure Detalle; override;
  public
    { Public declarations }
  end;

var
  FrmVendedor: TFrmVendedor;

implementation

uses
  GenericoBuscador,
  AgregarDetalleVendedor,
  ClsAnticipoCliente,
  ClsFacturacionCliente,
  ClsNotaCreditoCliente,
  ClsNotaDebitoCliente;

{$R *.dfm}


{ TFrmGenericoDetalle1 }

procedure TFrmVendedor.Agregar;
begin
   //Verificando
  Limpiar;
  TxtCodigo.SetFocus;
end;

procedure TFrmVendedor.Asignar;
var
  DataDetalle: TClientDataSet;
  Campo: string;
  i: Integer;
begin
  with Clase do
  begin
    TxtCodigo.Text := GetCodigo;
    TxtDescripcion.Text := GetDescripcion;
    CmbCodigoZona.KeyValue := GetCodigoZona;
    ChkActivo.Checked := GetActivo;
    //Cargando DataSet Secundario "Data"
    DataDetalle := TClientDataSet.Create(Self);
    DataDetalle.CloneCursor(VendedorComision.ObtenerCodigoVendedor(GetCodigo),False,True);
    Data.EmptyDataSet;
    DataDetalle.First;
    while (not DataDetalle.Eof) do
    begin
      Data.Insert;
      //Asignando Campos Similares
      for i := 0 to DataDetalle.FieldCount - 1 do
      begin
        Campo := DataDetalle.FieldDefs[i].Name;
        if (Data.FieldDefs.IndexOf(Campo) <> -1) then
        begin
          Data.FieldValues[Campo] := DataDetalle.FieldValues[Campo];
        end;
      end;
      TipoComision.SetCodigo(DataDetalle.FieldValues['codigo_tipo_comision']);
      TipoComision.Buscar;
      Data.FieldValues['codigo'] := TipoComision.GetCodigo;
      Data.FieldValues['descripcion'] := TipoComision.GetDescripcion;
      Data.FieldValues['porcentaje'] := TipoComision.GetPorcentaje;
      Data.Post;
      DataDetalle.Next;
    end;
    DS.DataSet := Data;
  end;
end;

procedure TFrmVendedor.BtnBuscarZonaClick(Sender: TObject);
begin
  Buscador(Self,Zona,'codigo','Busqueda de Zona',nil,DataZona);
  CmbCodigoZona.KeyValue := DataZona.FieldValues['codigo'];
end;

procedure TFrmVendedor.Buscar;
begin
  if (Buscador(Self,BuscadorVendedor,'codigo','Buscar Vendedor',nil,DataBuscadorVendedor)) then
  begin
    Clase.SetCodigo(DataBuscadorVendedor.FieldValues['codigo']);
    Clase.Buscar;
    Asignar;
  end;
end;

procedure TFrmVendedor.Cancelar;
begin
  TxtCodigo.Enabled := True;
end;

procedure TFrmVendedor.Editar;
begin
  if (Length(Trim(TxtCodigo.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
end;

procedure TFrmVendedor.Eliminar;
var
  FacturacionCliente: TFacturacionCliente;
  NotaCreditoCliente: TNotaCreditoCliente;
  NotaDebitoCliente: TNotaDebitoCliente;
  sw: Boolean;
begin
  //Se Valida Que El Vendedor No Posee Movimientos En El Sistema
  sw := True;
  //Verificando Facturas
  FacturacionCliente := TFacturacionCliente.Create;
  FacturacionCliente.SetCodigoVendedor(Clase.GetCodigo);
  if (FacturacionCliente.BuscarCodigoVendedor) then
  begin
    sw := False;
  end
  else
  begin
    //Verificando Notas de Credito
    NotaCreditoCliente := TNotaCreditoCliente.Create;
    NotaCreditoCliente.SetCodigoVendedor(Clase.GetCodigo);
    if (NotaCreditoCliente.BuscarCodigoVendedor) then
    begin
      sw := False;
    end
    else
    begin
      //Verificando Notas de Debito
      NotaDebitoCliente := TNotaDebitoCliente.Create;
      NotaDebitoCliente.SetCodigoVendedor(Clase.GetCodigo);
      if (NotaDebitoCliente.BuscarCodigoVendedor) then
      begin
        sw := False;
      end;
    end;
  end;
  if (sw) then
  begin
    //Se Busca el Elemento
    Clase.SetCodigo(TxtCodigo.Text);
    Clase.Buscar;
    //Se Eliminan los Detalles de Comisiones
    VendedorComision.EliminarCodigoVendedor(Clase.GetCodigo);
    Clase.Eliminar;
    MessageBox(Self.Handle, 'El Vendedor Se Ha Eliminado Con Exito!!!',PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Refrescar;
    Limpiar;
  end
  else
  begin
    MessageBox(Self.Handle, 'El Vendedor Presenta Movimientos En El Sistema y No Puede Ser Eliminado!!!',PChar(MsgTituloAdvertencia), MB_ICONEXCLAMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end;
end;

procedure TFrmVendedor.FormCreate(Sender: TObject);
begin
  ProcAgregar := Agregar;
  ProcEditar := Editar;
  ProcEliminar := Eliminar;
  ProcGuardar := Guardar;
  ProcCancelar := Cancelar;
  ProcRefrescar := Refrescar;
  ProcImprimir := Imprimir;
  ProcBuscar := Buscar;
  BuscadorVendedor := TBusquedaVendedor.Create;
  Clase := TVendedor.Create;
  Zona:= TZona.Create;
  TipoComision := TTipoComision.Create;
  VendedorComision := TVendedorComision.Create;
  DataZona:= Zona.ObtenerLista;
  DataZona.First;
  DataTipoComision:= TipoComision.ObtenerLista;
  DataTipoComision.First;
  DataBuscadorVendedor := BuscadorVendedor.ObtenerLista;
  DataBuscadorVendedor.First;
  DSZona.DataSet:= DataZona;
  DSTipoComision.DataSet:= DataTipoComision;
  TxtZona.DataSource:= DSZona;
  TxtZona.DataField:= 'descripcion';
  ConfigurarRxDBLookupCombo(CmbCodigoZona,DSZona,'codigo','codigo;descripcion');
  DS.DataSet := Data;
  GridDetalle.DataSource := DS;
 // Buscar;
end;

procedure TFrmVendedor.Guardar;
begin
  Obtener;
  if (Agregando) then
  begin
    //Se Busca el Codigo
    Clase.SetCodigo(TxtCodigo.Text);
    if (Clase.Buscar) then
    begin
      MessageBox(Self.Handle, 'El C�digo Ya Existe!!!', PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      TxtCodigo.SetFocus;
      Abort;
    end
    else
    begin
    //Agregando...
    //Guardando Encabezado
    Clase.Insertar;
    TxtCodigo.Text := Clase.GetCodigo;
    Application.ProcessMessages;
    //Guardando Detalles
    GuardarDetalles;
    MessageBox(Self.Handle, 'El Vendedor Ha Sido Agregado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    end;
  end
  else
  begin
    //Editando...
    Obtener;
    Clase.Modificar;
    //Actualizando Detalles (Eliminando Anteriores y Cargando los Nuevos)
    VendedorComision.EliminarCodigoVendedor(Clase.GetCodigo);
    //Guardando Detalles
    GuardarDetalles;
    MessageBox(Self.Handle, 'El Vendedor Ha Sido Editado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    TxtCodigo.Enabled := True;
  end;
  Refrescar;
  Limpiar;
end;

procedure TFrmVendedor.Imprimir;
begin

end;

procedure TFrmVendedor.Limpiar;
begin
  TxtCodigo.Text := '';
  TxtDescripcion.Text := '';
  CmbCodigoZona.KeyValue := '';
  ChkActivo.Checked := False;
  Data.EmptyDataSet;
end;

procedure TFrmVendedor.Mover;
var
  DSTemp: TDataSet;
begin
  if (DS.DataSet.RecordCount > 0) then
  begin
    DSTemp := DS.DataSet;
    with Clase do
    begin
      SetCodigo(DSTemp.FieldValues['codigo']);
      SetDescripcion(DSTemp.FieldValues['descripcion']);
      SetCodigoZona(DSTemp.FieldValues['codigo_zona']);
      SetActivo(DSTemp.FieldValues['activo']);
    end;
    Asignar;
  end
  else
  begin
    Limpiar;
  end;
end;

procedure TFrmVendedor.GuardarDetalles;
begin
  with Data do
  begin
    DisableControls;
    First;
    VendedorComision.SetCodigoVendedor(Clase.GetCodigo);
    while (not Eof) do
    begin
      with VendedorComision do
      begin
        SetCodigoTipoComision(FieldValues['codigo']);
        Insertar;
      end;
      Next;
    end;
    EnableControls;
  end;
end;

procedure TFrmVendedor.Obtener;
begin
  with Clase do
  begin
    SetCodigo(TxtCodigo.Text);
    SetDescripcion(TxtDescripcion.Text);
    SetCodigoZona(CmbCodigoZona.Text);
    SetActivo(ChkActivo.Checked);
  end;
end;

procedure TFrmVendedor.Refrescar;
begin
  //Se Actualizan Los demas Componentes
  DataZona:= Zona.ObtenerLista;
  DataZona.First;
  DataTipoComision:= TipoComision.ObtenerLista;
  DataTipoComision.First;
  DataBuscadorVendedor := BuscadorVendedor.ObtenerLista;
  DataBuscadorVendedor.First;
  DSZona.DataSet:= DataZona;
  DSTipoComision.DataSet:= DataTipoComision;
end;

procedure TFrmVendedor.TBtnEliminarClick(Sender: TObject);
begin
  if (Length(Trim(TxtCodigo.Text)) > 0) then
  begin
    inherited;
  end
  else
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
end;

function TFrmVendedor.Validar: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  //Validando Codigo
  if (Length(Trim(TxtCodigo.Text)) = 0) then
  begin
    ListaErrores.Add('El C�digo No Puede Estar En Blanco!!!');
  end;
  //Validando Descripci�n
  if (Length(Trim(TxtDescripcion.Text)) = 0) then
  begin
    ListaErrores.Add('La Descripci�n No Puede Estar En Blanco!!!');
  end;
  //Validando Codigo Zona
  if (Length(Trim(CmbCodigoZona.Text)) = 0) then
  begin
    ListaErrores.Add('Debe Seleccionar La Zona!!!');
  end;
  //Validando Detalle
  if (Data.RecordCount = 0) then
  begin
    ListaErrores.Add('Debe Especificar Un Detalle de Comisiones!!!');
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

procedure TFrmVendedor.Detalle;
begin
  inherited;
  FrmAgregarDetalleVendedor := TFrmAgregarDetalleVendedor.CrearDetalle(Self,Data);
  FrmAgregarDetalleVendedor.ShowModal;
  FreeAndNil(FrmAgregarDetalleVendedor);
end;

end.
