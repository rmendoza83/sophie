unit AgregarDetalleRetencionIva;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GenericoAgregarDetalle, StdCtrls, Buttons, ExtCtrls,
  ClsBusquedaDocumentoRetencionIva, ClsFacturacionProveedor,
  ClsNotaDebitoProveedor, ClsNotaCreditoProveedor, ClsDetRetencionIva,
  ClsTipoRetencion, ClsProveedor, GenericoBuscador, DBClient, Utils, Mask,
  rxToolEdit, rxCurrEdit, RxLookup, DB;

type
  TFrmAgregarDetalleRetencionIva = class(TFrmGenericoAgregarDetalle)
    LblNumero: TLabel;
    LblProveedor: TLabel;
    LblDesde: TLabel;
    LblDescripcionDesde: TLabel;
    LblMontoBase: TLabel;
    LblRazonSocialProveedor: TLabel;
    TxtDesde: TEdit;
    TxtNumero: TEdit;
    TxtCodigoProveedor: TEdit;
    GrpRetenciones: TGroupBox;
    CmbCodigoRetencion: TRxDBLookupCombo;
    BtnRetencion: TBitBtn;
    TxtMontoRetencion: TCurrencyEdit;
    BtnBorrarRetencion: TBitBtn;
    DSTipoRetencion: TDataSource;
    procedure CmbCodigoRetencionCloseUp(Sender: TObject);
    procedure BtnBuscarClick(Sender: TObject);
    procedure BtnBorrarRetencionClick(Sender: TObject);
    procedure BtnRetencionClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    ArrayVariant: TArrayVariant;
    DetRetencionIva: TDetRetencionIva;
    TipoRetencion: TTipoRetencion;
    FacturaProveedor: TFacturacionProveedor;
    NotaDebitoProveedor: TNotaDebitoProveedor;
    NotaCreditoProvedor: TNotaCreditoProveedor;
    Proveedor: TProveedor;
    DataTipoRetencion: TClientDataSet;
    procedure CalcularRetencion(Ds: TClientDataSet; CurrencyEdit: TCurrencyEdit);
  protected
    { Protected declarations }
    function Buscar: Boolean; override;
    procedure Agregar; override;
    procedure Limpiar; override;
    function Validar: Boolean;
  public
    { Public declarations }
    CodigoProveedor: string;
    BusquedaDocumentoRetencionIva: TBusquedaDocumentoRetencionIva;
  end;

var
  FrmAgregarDetalleRetencionIva: TFrmAgregarDetalleRetencionIva;

implementation

{$R *.dfm}

{ TFrmAgregarDetalleRetencionIva }

procedure TFrmAgregarDetalleRetencionIva.Agregar;
begin
  if (Validar) then
  begin
    with Data do
    begin
      Append;
      FieldValues['desde'] := BusquedaDocumentoRetencionIva.GetDesde;
      FieldValues['numero_documento'] := BusquedaDocumentoRetencionIva.GetNumero;
      if (BusquedaDocumentoRetencionIva.GetDesde = 'NCP') then
      begin //Nota Credito de Proveedor
        NotaCreditoProvedor.SetNumero(BusquedaDocumentoRetencionIva.GetNumero);
        NotaCreditoProvedor.BuscarNumero;
        FieldValues['monto_base'] := NotaCreditoProvedor.GetMontoNeto;
        FieldValues['p_iva'] := NotaCreditoProvedor.GetPIva;
        FieldValues['impuesto'] := NotaCreditoProvedor.GetImpuesto;
      end
      else
      begin
        if (BusquedaDocumentoRetencionIva.GetDesde = 'NDP') then
        begin //Nota Debito Proveedor
          NotaDebitoProveedor.SetNumero(BusquedaDocumentoRetencionIva.GetNumero);
          NotaDebitoProveedor.BuscarNumero;
          FieldValues['monto_base'] := NotaDebitoProveedor.GetMontoNeto;
          FieldValues['p_iva'] := NotaDebitoProveedor.GetPIva;
          FieldValues['impuesto'] := NotaDebitoProveedor.GetImpuesto;
        end
        else
        begin //Factura Proveedor
          FacturaProveedor.SetNumero(BusquedaDocumentoRetencionIva.GetNumero);
          FacturaProveedor.BuscarNumero;
          FieldValues['monto_base'] := FacturaProveedor.GetMontoNeto;
          FieldValues['p_iva'] := FacturaProveedor.GetPIva;
          FieldValues['impuesto'] := FacturaProveedor.GetImpuesto;
        end;
      end;
      FieldValues['codigo_retencion'] := CmbCodigoRetencion.Text;
      FieldValues['monto_retencion'] := TxtMontoRetencion.Value;
      if (TxtMontoRetencion.Value > 0) then
      begin
        FieldValues['descripcion_retencion'] := DataTipoRetencion.FieldValues['descripcion'];
      end
      else
      begin
        FieldValues['descripcion_retencion'] := '';
      end;
      Post;
    end;
    inherited;
  end
  else
  begin
    ModalResult := mrNone;
  end;
end;

procedure TFrmAgregarDetalleRetencionIva.BtnBorrarRetencionClick(
  Sender: TObject);
begin
  CmbCodigoRetencion.KeyValue := '';
  TxtMontoRetencion.Text := '';
end;

procedure TFrmAgregarDetalleRetencionIva.BtnBuscarClick(Sender: TObject);
var
  Condicion: string;
begin
  inherited;
  Condicion := '"codigo_proveedor" = ' + QuotedStr(CodigoProveedor);
  if (Buscador(Self,BusquedaDocumentoRetencionIva,'desde;numero;codigo_proveedor','Buscar Documento',nil,nil,True,ArrayVariant,Condicion)) then
  begin
    Buscar;
  end
  else
  begin
    SetLength(ArrayVariant,0);
  end;
end;

procedure TFrmAgregarDetalleRetencionIva.BtnRetencionClick(Sender: TObject);
begin
  if (Length(Trim(TxtDesde.Text)) > 0) then
  begin
    if (Buscador(Self,TipoRetencion,'codigo','Buscar Tipo de Retenci�n',nil,DataTipoRetencion,'retencion_iva = true')) then
    begin
      CmbCodigoRetencion.KeyValue := DataTipoRetencion.FieldValues['codigo'];
      CalcularRetencion(DataTipoRetencion,TxtMontoRetencion);
    end
    else
    begin
      if (Length(Trim(CmbCodigoRetencion.Text)) = 0) then
      begin
        TxtMontoRetencion.Text := '';
      end;      
    end;
  end;
end;

function TFrmAgregarDetalleRetencionIva.Buscar: Boolean;
begin
  if (Length(ArrayVariant) > 0) then
  begin
    BusquedaDocumentoRetencionIva.SetDesde(ArrayVariant[0]);
    BusquedaDocumentoRetencionIva.SetNumero(ArrayVariant[1]);
    BusquedaDocumentoRetencionIva.SetCodigoProveedor(ArrayVariant[2]);
    Result := BusquedaDocumentoRetencionIva.Buscar;
    if (Result) then
    begin
      TxtDesde.Text := BusquedaDocumentoRetencionIva.GetDesde;
      if (BusquedaDocumentoRetencionIva.GetDesde = 'NCP') then
      begin
        LblDescripcionDesde.Caption := 'Nota de Cr�dito de Proveedor';
        NotaCreditoProvedor.SetNumero(BusquedaDocumentoRetencionIva.GetNumero);
        NotaCreditoProvedor.BuscarNumero;
      end
      else
      begin
        if (BusquedaDocumentoRetencionIva.GetDesde = 'NDP') then
        begin
          LblDescripcionDesde.Caption := 'Nota de D�bito de Proveedor';
          NotaDebitoProveedor.SetNumero(BusquedaDocumentoRetencionIva.GetNumero);
          NotaDebitoProveedor.BuscarNumero;
        end
        else
        begin
          LblDescripcionDesde.Caption := 'Factura de Proveedor';
          FacturaProveedor.SetNumero(BusquedaDocumentoRetencionIva.GetNumero);
          FacturaProveedor.BuscarNumero;
        end;
      end;
      TxtNumero.Text := BusquedaDocumentoRetencionIva.GetNumero;
      TxtCodigoProveedor.Text := BusquedaDocumentoRetencionIva.GetCodigoProveedor;
      Proveedor.SetCodigo(BusquedaDocumentoRetencionIva.GetCodigoProveedor);
      Proveedor.BuscarCodigo;
      LblRazonSocialProveedor.Caption := Proveedor.GetRazonSocial;
      LblMontoBase.Caption := 'Monto Base: ' + FormatFloat('#,##0.0000',BusquedaDocumentoRetencionIva.GetMontoNeto);
      CmbCodigoRetencion.SetFocus;
    end
    else
    begin
      MessageBox(Self.Handle, Cadena('El Documento No Existe!!!'), PChar(MsgTituloError), MB_ICONSTOP or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      Limpiar;
    end;
  end
  else
  begin
    Result := False;
  end;
end;

procedure TFrmAgregarDetalleRetencionIva.CalcularRetencion(Ds: TClientDataSet;
  CurrencyEdit: TCurrencyEdit);
begin
  TipoRetencion.SetCodigo(Ds.FieldValues['codigo']);
  TipoRetencion.Buscar;
  CurrencyEdit.Value := ((BusquedaDocumentoRetencionIva.GetImpuesto * TipoRetencion.GetTarifa) / TipoRetencion.GetBase) - TipoRetencion.GetSustraendo;
  CurrencyEdit.SetFocus;
end;

procedure TFrmAgregarDetalleRetencionIva.CmbCodigoRetencionCloseUp(
  Sender: TObject);
begin
  if (Length(Trim(CmbCodigoRetencion.Value)) > 0) then
  begin
    CalcularRetencion(DataTipoRetencion,TxtMontoRetencion);
  end
  else
  begin
    TxtMontoRetencion.Text := '';
  end;
end;

procedure TFrmAgregarDetalleRetencionIva.FormCreate(Sender: TObject);
begin
  inherited;
  ComponenteFoco := nil;
  BusquedaDocumentoRetencionIva := TBusquedaDocumentoRetencionIva.Create;
  DetRetencionIva := TDetRetencionIva.Create;
  TipoRetencion := TTipoRetencion.Create;
  FacturaProveedor := TFacturacionProveedor.Create;
  NotaDebitoProveedor := TNotaDebitoProveedor.Create;
  NotaCreditoProvedor := TNotaCreditoProveedor.Create;
  Proveedor := TProveedor.Create;
  DataTipoRetencion := TipoRetencion.ObtenerComboIVA;
  DataTipoRetencion.First;
  DSTipoRetencion.DataSet := DataTipoRetencion;
  ConfigurarRxDBLookupCombo(CmbCodigoRetencion,DSTipoRetencion,'codigo','codigo;descripcion');
end;

procedure TFrmAgregarDetalleRetencionIva.Limpiar;
begin
  inherited;
  TxtDesde.Text := '';
  LblDescripcionDesde.Caption := '';
  TxtNumero.Text := '';
  LblMontoBase.Caption := '';
  TxtCodigoProveedor.Text := '';
  LblRazonSocialProveedor.Caption := '';
  CmbCodigoRetencion.Value := '';
  TxtMontoRetencion.Text := '';
end;

function TFrmAgregarDetalleRetencionIva.Validar: Boolean;
var
  ListaErrores: TStringList;
  sw: Boolean;
begin
  ListaErrores := TStringList.Create;
  //Validando Existencia del Documento a Retener
  sw := false;
  with Data do
  begin
    First;
    while (not Eof) do
    begin
      if ((FieldValues['desde'] = TxtDesde.Text) and (FieldValues['numero_documento'] = TxtNumero.Text)) then
      begin
        sw := True;
        Break;
      end;
      Next;
    end;
  end;
  if (sw) then
  begin
    ListaErrores.Add('El Documento Ya Se Encuentra En La Lista!!!');
  end;
  //Validando Codigo de Retencion
  if ((Length(Trim(CmbCodigoRetencion.Text)) > 0) and (TxtMontoRetencion.Value = 0)) then
  begin
    ListaErrores.Add('El Monto de Retencion 1 Debe Ser Mayor A Cero!!!');
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
