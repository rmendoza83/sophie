unit RadioEmisora;

{$I sophie.inc}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Generico, XPMan, ImgList, DB, Grids, DBGrids, StdCtrls, Buttons,
  ExtCtrls, ComCtrls, ToolWin, Mask, rxToolEdit, Utils, ClsRadioEmisora,
  DBClient{$IFDEF DELPHI10}, ImageList{$ENDIF};

type
  TFrmRadioEmisora = class(TFrmGenerico)
    LblCodigo: TLabel;
    LblRif: TLabel;
    LblRazonSocial: TLabel;
    LblTelefonos: TLabel;
    LblDireccionFiscal: TLabel;
    LblFax: TLabel;
    LblWebsite: TLabel;
    LblFechaIngreso: TLabel;
    LblContacto: TLabel;
    Label12: TLabel;
    TxtCodigo: TEdit;
    CmbPrefijoRif: TComboBox;
    TxtRif: TEdit;
    TxtRazonSocial: TEdit;
    TxtTelefonos: TEdit;
    TxtDireccionFiscal1: TEdit;
    TxtDireccionFiscal2: TEdit;
    TxtFax: TEdit;
    TxtWebsite: TEdit;
    DatFechaIngreso: TDateEdit;
    TxtContacto: TEdit;
    procedure GrdBusquedaCellClick(Column: TColumn);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    Clase: TRadioEmisora;
    Data: TClientDataSet;
    procedure Agregar;
    procedure Editar;
    procedure Eliminar;
    procedure Guardar;
    procedure Cancelar;
    procedure Refrescar;
    procedure Imprimir;
    procedure Buscar;
    procedure Limpiar;
    procedure Asignar;
    procedure Obtener;
    procedure Mover;
  protected
    function Validar: Boolean; override;
  public
    { Public declarations }
  end;

var
  FrmRadioEmisora: TFrmRadioEmisora;

implementation

{$R *.dfm}

{ TFrmRadioEmisora }

procedure TFrmRadioEmisora.Agregar;
begin
  //Verificando
  Limpiar;
  TxtCodigo.SetFocus;
end;

procedure TFrmRadioEmisora.Asignar;
begin
  with Clase do
  begin
    TxtCodigo.Text := GetCodigo;
    CmbPrefijoRif.ItemIndex := CmbPrefijoRif.Items.IndexOf(GetPrefijoRif);
    TxtRif.Text := GetRif;
    TxtRazonSocial.Text := GetRazonSocial;
    TxtTelefonos.Text := GetTelefonos;
    TxtFax.Text := GetFax;
    TxtWebsite.Text := GetWebsite;
    DatFechaIngreso.Date := GetFechaIngreso;
    TxtContacto.Text := GetContacto;
    TxtDireccionFiscal1.Text := GetDireccionFiscal1;
    TxtDireccionFiscal2.Text := GetDireccionFiscal2;
  end;
end;

procedure TFrmRadioEmisora.Buscar;
begin
  Screen.Cursor := crSQLWait;
  if (Length(Trim(TxtCriterio.Text)) > 0) then
  begin
    Data := Clase.ObtenerListaCriterio(Trim(TxtCriterio.Text));
  end
  else
  begin
    Data := Clase.ObtenerLista;
  end;
  Data.First;
  DS.DataSet := Data;
  GrdBusqueda.Refresh;
  Screen.Cursor := crDefault;
end;

procedure TFrmRadioEmisora.Cancelar;
begin
  TxtCodigo.Enabled := True;
end;

procedure TFrmRadioEmisora.Editar;
begin
  //Se Busca el Elemento
  Clase.SetCodigo(Data.FieldValues['codigo']);
  Clase.Buscar;
  Asignar;
  TxtCodigo.Enabled := False;
  TxtRif.SetFocus;
end;

procedure TFrmRadioEmisora.Eliminar;
begin
  //Se Busca el Elemento
  Clase.SetCodigo(Data.FieldValues['codigo']);
  Clase.Buscar;
  Clase.Eliminar;
  MessageBox(Self.Handle, 'La Emisora Ha Sido Eliminada Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  Refrescar;
  Limpiar;
end;

procedure TFrmRadioEmisora.FormCreate(Sender: TObject);
begin
  ProcAgregar := Agregar;
  ProcEditar := Editar;
  ProcEliminar := Eliminar;
  ProcGuardar := Guardar;
  ProcCancelar := Cancelar;
  ProcRefrescar := Refrescar;
  ProcImprimir := Imprimir;
  ProcBuscar := Buscar;
  Clase := TRadioEmisora.Create;
  Buscar;
end;

procedure TFrmRadioEmisora.GrdBusquedaCellClick(Column: TColumn);
begin
  Mover;
end;

procedure TFrmRadioEmisora.Guardar;
begin
  if (Agregando) then
  begin
    //Agregando...
    //Se Busca el Codigo
    Clase.SetCodigo(TxtCodigo.Text);
    if (Clase.Buscar) then
    begin
      MessageBox(Self.Handle, 'El C�digo Ya Existe!!!', PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      TxtCodigo.SetFocus;
      Abort;
    end
    else
    begin
      Obtener;
      Clase.Insertar;
      MessageBox(Self.Handle, 'La Emisora  Ha Sido Agregada Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    end;
  end
  else
  begin
    //Editando...
    Obtener;
    Clase.Modificar;
    MessageBox(Self.Handle, 'La Emisora  Ha Sido Editada Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    TxtCodigo.Enabled := True;
  end;
  Refrescar;
  Limpiar;
end;

procedure TFrmRadioEmisora.Imprimir;
begin

end;

procedure TFrmRadioEmisora.Limpiar;
begin
  TxtCodigo.Text := '';
  CmbPrefijoRif.Text := '';
  TxtRif.Text := '';
  TxtRazonSocial.Text := '';
  TxtTelefonos.Text := '';
  TxtFax.Text := '';
  TxtWebsite.Text := '';
  DatFechaIngreso.Date := Now;
  TxtContacto.Text := '';
  TxtDireccionFiscal1.Text := '';
  TxtDireccionFiscal2.Text := '';
end;


procedure TFrmRadioEmisora.Mover;
var
  DSTemp: TDataSet;
begin
  if (DS.DataSet.RecordCount > 0) then
  begin
    DSTemp := DS.DataSet;
    with Clase do
    begin
      SetCodigo(DSTemp.FieldValues['codigo']);
      SetPrefijoRif(DSTemp.FieldValues['prefijo_rif']);
      SetRif(DSTemp.FieldValues['rif']);
      SetRazonSocial(DSTemp.FieldValues['razon_social']);
      SetTelefonos(DSTemp.FieldValues['telefonos']);
      SetFax(DSTemp.FieldValues['fax']);
      SetWebsite(DSTemp.FieldValues['website']);
      SetFechaIngreso(DSTemp.FieldByName('fecha_ingreso').AsDateTime);
      SetContacto(DSTemp.FieldValues['contacto']);
      SetDireccionFiscal1(DSTemp.FieldValues['direccion_fiscal_1']);
      SetDireccionFiscal2(DSTemp.FieldValues['direccion_fiscal_2']);
    end;
    Asignar;
  end
  else
  begin
    Limpiar;
  end;
end;

procedure TFrmRadioEmisora.Obtener;
begin
  with Clase do
  begin
    SetCodigo(TxtCodigo.Text);
    SetPrefijoRif(CmbPrefijoRif.Text);
    SetRif(TxtRif.Text);
    SetRazonSocial(TxtRazonSocial.Text);
    SetTelefonos(TxtTelefonos.Text);
    SetFax(TxtFax.Text);
    SetWebsite(TxtWebsite.Text);
    SetFechaIngreso(DatFechaIngreso.Date);
    SetContacto(TxtContacto.Text);
    SetDireccionFiscal1(TxtDireccionFiscal1.Text);
    SetDireccionFiscal2(TxtDireccionFiscal2.Text);
  end;
end;

procedure TFrmRadioEmisora.Refrescar;
begin
  TxtCriterio.Text := '';
  Buscar;
end;

function TFrmRadioEmisora.Validar: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  //Validando Codigo
  if (Length(Trim(TxtCodigo.Text)) = 0) then
  begin
    ListaErrores.Add('El C�digo No Puede Estar En Blanco!!!');
  end;
  //Validando Prefijo Rif
  if (Length(Trim(CmbPrefijoRif.Text)) = 0) then
  begin
    ListaErrores.Add('El Prefijo del Rif No Puede Estar En Blanco!!!');
  end;
  //Validando  Rif
  if (Length(Trim(TxtRif.Text)) = 0) then
  begin
    ListaErrores.Add('El Rif No Puede Estar En Blanco!!!');
  end;
  //Validando  Razon Social
  if (Length(Trim(TxtRazonSocial.Text)) = 0) then
  begin
    ListaErrores.Add('El Nombre de la Emisora No Puede Estar En Blanco!!!');
  end;
  //Validando Direccion
  if (Length(Trim(TxtDireccionFiscal1.Text)) = 0) then
  begin
    ListaErrores.Add('La Direcci�n Fiscal No Puede Estar En Blanco!!!');
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
