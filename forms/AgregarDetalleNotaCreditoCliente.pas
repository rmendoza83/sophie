unit AgregarDetalleNotaCreditoCliente;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GenericoAgregarDetalle, DB, StdCtrls, Buttons, ExtCtrls, Mask,
  rxToolEdit, rxCurrEdit, DBCtrls, ClsProducto, ClsDetNotaCreditoCliente,
  GenericoBuscador, DBClient, Utils, AgregarDetalleFacturacionCliente;

type
  TFrmAgregarDetalleNotaCreditoCliente = class(TFrmGenericoAgregarDetalle)
    TxtDescripcionProducto: TEdit;
    TxtCantidad: TCurrencyEdit;
    LblCantidad: TLabel;
    procedure TxtCantidadKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtnBuscarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    Producto: TProducto;
  protected
    function Buscar: Boolean; override;
    procedure Agregar; override;
    procedure Limpiar; override;
  public
    { Public declarations }
  end;

var
  FrmAgregarDetalleNotaCreditoCliente: TFrmAgregarDetalleNotaCreditoCliente;

implementation

{$R *.dfm}

procedure TFrmAgregarDetalleNotaCreditoCliente.Agregar;
begin
  with Data do
  begin
    //Verificando si existe el codigo en el Grid
    if (Locate('codigo_producto',TxtCodigo.Text,[loCaseInsensitive])) then
    begin
      Edit;
      FieldValues['cantidad'] := FieldValues['cantidad'] + TxtCantidad.Value;
      FieldValues['monto_descuento'] := (Producto.GetPrecio * FieldValues['cantidad']) * (Producto.GetPDescuento / 100);
      FieldValues['impuesto'] := (Producto.GetPrecio * FieldValues['cantidad']) * (Producto.GetPIvaVenta / 100);
      FieldValues['total'] := Producto.GetPrecio * FieldValues['cantidad'];
    end
    else
    begin
      Append;
      FieldValues['codigo_producto'] := Producto.GetCodigo;
      FieldValues['referencia'] := Producto.GetReferencia;
      FieldValues['descripcion'] := Producto.GetDescripcion;
      FieldValues['cantidad'] := TxtCantidad.Value;
      FieldValues['precio'] := Producto.GetPrecio;
      FieldValues['p_descuento'] := Producto.GetPDescuento;
      FieldValues['monto_descuento'] := (Producto.GetPrecio * TxtCantidad.Value) * (Producto.GetPDescuento / 100);
      FieldValues['impuesto'] := (Producto.GetPrecio * TxtCantidad.Value) * (Producto.GetPIvaVenta / 100);
      FieldValues['p_iva'] := Producto.GetPIvaVenta;
      FieldValues['total'] := Producto.GetPrecio * TxtCantidad.Value;
      FieldValues['id_nota_credito_cliente'] := -1;
    end;
    Post;
  end;
  inherited;
end;

procedure TFrmAgregarDetalleNotaCreditoCliente.BtnBuscarClick(Sender: TObject);
begin
  inherited;
  Buscador(Self,Producto,'codigo','Buscar Producto',TxtCodigo,nil);
  if (Length(Trim(TxtCodigo.Text)) > 0) then
  begin
    ComponenteFoco.SetFocus;
  end;
end;

function TFrmAgregarDetalleNotaCreditoCliente.Buscar: Boolean;
begin
  Producto.SetCodigo(TxtCodigo.Text);
  Result := Producto.BuscarCodigo;
  if (Result) then
  begin
    TxtDescripcionProducto.Text := Producto.GetDescripcion;
  end
  else
  begin
    MessageBox(Self.Handle, Cadena('El Codigo No Existe!!!'), PChar(MsgTituloError), MB_ICONSTOP or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Limpiar;
  end;
end;

procedure TFrmAgregarDetalleNotaCreditoCliente.FormCreate(Sender: TObject);
begin
  inherited;
  ComponenteFoco := (TxtCantidad as TWinControl);
  Producto := TProducto.Create;
end;

procedure TFrmAgregarDetalleNotaCreditoCliente.Limpiar;
begin
  inherited;
  TxtDescripcionProducto.Text := '';
  TxtCantidad.Value := 1;
end;

procedure TFrmAgregarDetalleNotaCreditoCliente.TxtCantidadKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  case Key of
    VK_RETURN:
      Agregar;    
  end;
end;

end.
