unit RaveSetup;

interface

uses
  Windows, Messages, Graphics, Controls, Forms, StdCtrls, Buttons, Dialogs, ComCtrls,
  SysUtils, Classes, RpDevice, RpDefine, RpSystem, RpRenderPrinter, XPMan;

type
  TFormRaveSetup = class(TForm)
    DestGB: TGroupBox;
    PrinterRB: TRadioButton;
    PreviewRB: TRadioButton;
    FileRB: TRadioButton;
    editFileName: TEdit;
    RangeGB: TGroupBox;
    FileNameSB: TSpeedButton;
    dlogSave: TSaveDialog;
    AllRB: TRadioButton;
    SelectionRB: TRadioButton;
    PagesRB: TRadioButton;
    FromLabel: TLabel;
    FromED: TEdit;
    ToLabel: TLabel;
    ToED: TEdit;
    SelectionED: TEdit;
    GroupBox1: TGroupBox;
    PrinterLabel: TLabel;
    GroupBox2: TGroupBox;
    CopiesED: TEdit;
    CopiesLabel: TLabel;
    CollateCK: TCheckBox;
    DuplexCK: TCheckBox;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    cboxFormat: TComboBox;
    Label1: TLabel;
    TemaXP: TXPManifest;
    BtnAceptar: TBitBtn;
    BtnCancelar: TBitBtn;
    BtnConfig: TBitBtn;
    procedure SetupBBClick(Sender: TObject);
    procedure FileNameSBClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PagesRBClick(Sender: TObject);
    procedure SelectionRBClick(Sender: TObject);
    procedure AllRBClick(Sender: TObject);
    procedure PrinterRBClick(Sender: TObject);
    procedure FileRBClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure bbtnOKClick(Sender: TObject);
    procedure editFileNameChange(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure SelectionEDKeyPress(Sender: TObject; var Key: Char);
    procedure cboxFormatChange(Sender: TObject);
  private
    procedure UpdateForm;
    procedure GetRenderList;
  public
    ReportSystem: TRvSystem;
    FilePrinter: TRvRenderPrinter;
    PreviewSetup: boolean;
  end;

var
  FormRaveSetup: TFormRaveSetup;

implementation

uses  RpRender;

{$R *.dfm}

procedure TFormRaveSetup.UpdateForm;

begin
  if (RPDev = nil) or (RPDev.Printers.Count = 0) then
  PrinterLabel.Caption := Trans('No ha sido detectada ninguna impresora')
  else begin
         RPDev.State := dsIC;
         if RPDev.Output = '' then PrinterLabel.Caption := RPDev.Device
         else PrinterLabel.Caption := Trans(Format({Trans+}'%s en %s',[RPDev.Device,RPDev.Output]));
       end;
end;

procedure TFormRaveSetup.SetupBBClick(Sender: TObject);
begin
  if RPDev <> nil then
  begin
    RPDev.PrinterSetupDialog;
    UpdateForm;
  end;
end;

procedure TFormRaveSetup.FileNameSBClick(Sender: TObject);
begin
  if dlogSave.Execute then
  begin
    editFileName.Text := dlogSave.FileName;
    cboxFormat.ItemIndex := dlogSave.FilterIndex - 1;
  end;
end;

procedure TFormRaveSetup.FormCreate(Sender: TObject);
begin
  PageControl1.ActivePage := nil;
end;

procedure TFormRaveSetup.PagesRBClick(Sender: TObject);
begin
  with Sender as TRadioButton do
  begin
    if Checked then
    begin
      SelectionED.Enabled := False;
      FromLabel.Enabled := true;
      FromED.Enabled := true;
      ToLabel.Enabled := true;
      ToED.Enabled := true;
    end;
  end;
end;

procedure TFormRaveSetup.SelectionRBClick(Sender: TObject);
begin
  with Sender as TRadioButton do
  begin
    if Checked then
    begin
      FromLabel.Enabled := False;
      FromED.Enabled := False;
      ToLabel.Enabled := False;
      ToED.Enabled := False;
      SelectionED.Enabled := true;
    end;
  end;
end;

procedure TFormRaveSetup.AllRBClick(Sender: TObject);
begin
  SelectionED.Enabled := false;
  FromLabel.Enabled := false;
  FromED.Enabled := false;
  ToLabel.Enabled := false;
  ToED.Enabled := false;
end;

procedure TFormRaveSetup.PrinterRBClick(Sender: TObject);
begin
  editFileName.Enabled := false;
  FileNameSB.Enabled := false;
  cboxFormat.Enabled := false;
  BtnAceptar.Enabled := true;
  BtnConfig.enabled := true;
//  GroupBox2.Enabled := Sender = PrinterRB;
  CopiesED.Enabled := Sender = PrinterRB;
  CollateCK.Enabled := Sender = PrinterRB;
  DuplexCK.Enabled := Sender = PrinterRB;
end;

procedure TFormRaveSetup.FileRBClick(Sender: TObject);
begin
  editFileName.Enabled := true;
  FileNameSB.Enabled := true;
  cboxFormat.Enabled := true;
//  GroupBox2.Enabled := false;
  BtnConfig.enabled := false;
  BtnAceptar.Enabled := Trim(editFileName.Text) <> '';
end;

procedure TFormRaveSetup.FormShow(Sender: TObject);
begin
  GetRenderList;
{ Enable/Disable items on setup form }
  with ReportSystem do
  begin
    UpdateForm;
    if PreviewSetup then
    begin
      PageControl1.ActivePage := TabSheet2;
    { Init Page range variables here for FilePrinter.ExecuteCustom }
      FromED.Text := '1';
      ToED.Text := IntToStr(FilePrinter.Pages);
      SelectionED.Text := {Trans-}'1-' + IntToStr(FilePrinter.Pages);
    end else
    begin { Initial setup, ask for destination }
      PageControl1.ActivePage := TabSheet1;
      if cboxFormat.ItemIndex < 0 then cboxFormat.ItemIndex := 0;
      if (RPDev = nil) or RPDev.InvalidPrinter then
      begin
        PrinterRB.Enabled := false;
        FileRB.Enabled := false;
        if not (ssAllowDestPreview in SystemSetups) then PreviewRB.Enabled := false
        else PreviewRB.Checked := true;
      end else
      begin
        if not (ssAllowDestPrinter in SystemSetups) then begin
          PrinterRB.Enabled := false;
        end; { if }
        if not (ssAllowDestPreview in SystemSetups) then begin
          PreviewRB.Enabled := false;
        end; { if }
        if not (ssAllowDestFile in SystemSetups) then begin
          FileRB.Enabled := false;
        end; { if }
        case DefaultDest of
          rdPrinter: PrinterRB.Checked := true;
          rdPreview: PreviewRB.Checked := true;
          rdFile: FileRB.Checked := true;
        end;
      end;
    end;
    if (RPDev = nil) or RPDev.InvalidPrinter then
    begin
      CollateCK.Enabled := false;
      CopiesED.Enabled := false;
      CopiesLabel.Enabled := false;
      CopiesED.Text := IntToStr(SystemPrinter.Copies);
      DuplexCK.Enabled := false;
      BtnConfig.Enabled := false;
    end else
    begin
      if not (ssAllowCollate in SystemSetups) then CollateCK.Enabled := false;
      CollateCK.Checked := ReportSystem.SystemPrinter.Collate;
      if not (ssAllowCopies in SystemSetups) then
      begin
        CopiesED.Enabled := false;
        CopiesLabel.Enabled := false;
      end; { if }
      CopiesED.Text := IntToStr(SystemPrinter.Copies);
      if not (ssAllowDuplex in SystemSetups) then DuplexCK.Enabled := false;
      DuplexCK.Checked := ReportSystem.SystemPrinter.Duplex <> dupSimplex;
      if not (ssAllowPrinterSetup in SystemSetups) then BtnConfig.Enabled := false;
    end; { else }
  end;
end;

procedure TFormRaveSetup.bbtnOKClick(Sender: TObject);
var
  I1: integer;
  ErrCode: integer;
  DefExtension: string;
begin
  with ReportSystem do
  begin
  { Gather information that was changed in setup window }
    if PreviewSetup then
    begin
    { Put code here to retrieve page range info }
      FilePrinter.FirstPage := 1;
      FilePrinter.LastPage := 9999;
      FilePrinter.Selection := '';
      if SelectionRB.Checked then FilePrinter.Selection := SelectionED.Text
      else if PagesRB.Checked then
      begin
        FilePrinter.FirstPage := StrToInt(FromED.Text);
        FilePrinter.LastPage := StrToInt(ToED.Text);
      end;

      if ssAllowCopies in SystemSetups then
      begin
        Val(CopiesED.Text,I1,ErrCode);
        if ErrCode = 0 then FilePrinter.Copies := I1;
      end;
      if ssAllowCollate in SystemSetups then FilePrinter.Collate := CollateCK.Checked;
      if (ssAllowDuplex in SystemSetups) then
      begin
        if DuplexCK.Checked then
        begin
          if FilePrinter.Duplex = dupSimplex then FilePrinter.Duplex := dupVertical;
        end else FilePrinter.Duplex := dupSimplex;
      end; 
    end else
    begin
      if PrinterRB.Checked then
      begin
        ReportDest := rdPrinter;
        OutputFileName := '';
      end else
      if PreviewRB.Checked then
      begin
        ReportDest := rdPreview;
        OutputFileName := '';
      end else
      begin
        ReportDest := rdFile;
        OutputFileName := Trim(editFileName.Text);

        if cboxFormat.ItemIndex = 0 then
        begin // Do NDR
          if Pos('.',OutputFileName) < 1 then OutputFileName := OutputFileName + {Trans-}'.ndr';
          ReportSystem.DoNativeOutput := false;
          ReportSystem.RenderObject := nil;
        end else
        if cboxFormat.ItemIndex = 1 then
        begin // Do Native Output PRN
          if Pos('.',OutputFileName) < 1 then OutputFileName := OutputFileName + {Trans-}'.prn';
          ReportSystem.DoNativeOutput := true;
          ReportSystem.RenderObject := nil;
        end else
        begin // Do Renderer
          ReportSystem.DoNativeOutput := false;
          ReportSystem.RenderObject :=
           TRPRender(cboxFormat.Items.Objects[cboxFormat.ItemIndex]);
          if Pos('.',OutputFileName) < 1 then
          begin
            with ReportSystem.RenderObject do
            begin
              DefExtension := Copy(FileExtension, 2, Length(FileExtension) - 1);
            end;
            OutputFileName := OutputFileName + DefExtension;
          end;
        end;
      end;
    end;

    if ssAllowCopies in SystemSetups then
    begin
      Val(CopiesED.Text,I1,ErrCode);
      if ErrCode = 0 then SystemPrinter.Copies := I1;
    end;
    if ssAllowCollate in SystemSetups then SystemPrinter.Collate := CollateCK.Checked;
    if (ssAllowDuplex in SystemSetups) then
    begin
      if DuplexCK.Checked then
      begin
        if SystemPrinter.Duplex = dupSimplex then SystemPrinter.Duplex := dupVertical;
      end else SystemPrinter.Duplex := dupSimplex;
    end;
  end;
end;

procedure TFormRaveSetup.GetRenderList;
const
  DefOutputStr: array[0..1] of string = ({Trans+}'Informe (*.NDR)',
                                         {Trans+}'Formato nativo impresora (*.PRN)');
begin
  cboxFormat.Items.Clear;
  if Assigned(RpRender.RenderList) then RpRender.GetRenderList(cboxFormat.Items);
  cboxFormat.Items.Insert(0,Trans(DefOutputStr[0]));
  cboxFormat.Items.Insert(1,Trans(DefOutputStr[1]));

  dlogSave.Filter := DefOutputStr[0] + {Trans-}'|*.NDR|' + DefOutputStr[1] + {Trans-}'|*.PRN';
  if RpRender.GetRenderFilter <> '' then dlogSave.Filter := dlogSave.Filter + '|' + RpRender.GetRenderFilter;
end;

procedure TFormRaveSetup.editFileNameChange(Sender: TObject);
begin
  BtnAceptar.Enabled := Trim(editFileName.Text) <> '';
end;

procedure TFormRaveSetup.FormKeyPress(Sender: TObject; var Key: Char);
var
  ValidKeys: string;
begin
  ValidKeys := {Trans-}'0123456789'#8;
  if (Sender <> editFileName) and (Pos(Key,ValidKeys) <= 0) then Key := #0;
end;

procedure TFormRaveSetup.SelectionEDKeyPress(Sender: TObject; var Key: Char);
var
  ValidKeys: string;
begin
  ValidKeys := {Trans-}'0123456789-,'#8;
  if (Sender <> editFileName) and (Pos(Key,ValidKeys) <= 0) then Key := #0;
end;

procedure TFormRaveSetup.cboxFormatChange(Sender: TObject);
begin
  dlogSave.FilterIndex := (Sender as TComboBox).ItemIndex + 1;
end;

end.
