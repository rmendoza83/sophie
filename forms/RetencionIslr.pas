unit RetencionIslr;

{$I sophie.inc}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GenericoDetalle, DB, DBClient, ImgList, XPMan, ComCtrls, Grids,
  DBGrids, ExtCtrls, ToolWin, StdCtrls, Buttons, rxCurrEdit, Mask, rxToolEdit,
  RxLookup, Utils, ClsRetencionIslr, ClsAnticipoProveedor,
  ClsNotaDebitoProveedor, ClsFacturacionProveedor, ClsTipoRetencion,
  ClsProveedor, ClsDetRetencionIslr, ClsZona, ClsBusquedaRetencionIslr,
  GenericoBuscador, ClsReporte, ClsParametro{$IFDEF DELPHI10}, ImageList{$ENDIF};

type
  TFrmRetencionIslr = class(TFrmGenericoDetalle)
    LblNumero: TLabel;
    LblCodigoProveedor: TLabel;
    LblCodigoZona: TLabel;
    LblFechaIngreso: TLabel;
    LblTotal: TLabel;
    TxtNumero: TEdit;
    CmbCodigoProveedor: TRxDBLookupCombo;
    DatFechaIngreso: TDateEdit;
    CmbCodigoZona: TRxDBLookupCombo;
    TxtTotal: TCurrencyEdit;
    BtnBuscarProveedor: TBitBtn;
    BtnBuscarZona: TBitBtn;
    LblFechaContable: TLabel;
    DatFechaContable: TDateEdit;
    DSProveedor: TDataSource;
    DSZona: TDataSource;
    Datadesde: TStringField;
    Datanumero_documento: TStringField;
    Datacodigo_retencion_1: TStringField;
    Datamonto_retencion_1: TFloatField;
    Datacodigo_retencion_2: TStringField;
    Datacodigo_retencion_3: TStringField;
    Datacodigo_retencion_4: TStringField;
    Datacodigo_retencion_5: TStringField;
    Datamonto_retencion_2: TFloatField;
    Datamonto_retencion_3: TFloatField;
    Datamonto_retencion_4: TFloatField;
    Datamonto_retencion_5: TFloatField;
    Datamonto_base: TFloatField;
    Datap_iva: TFloatField;
    Dataimpuesto: TFloatField;
    Datadescripcion_retencion_1: TStringField;
    Datadescripcion_retencion_2: TStringField;
    Datadescripcion_retencion_3: TStringField;
    Datadescripcion_retencion_4: TStringField;
    Datadescripcion_retencion_5: TStringField;
    procedure BtnBuscarZonaClick(Sender: TObject);
    procedure CmbCodigoProveedorChange(Sender: TObject);
    procedure CmbCodigoProveedorExit(Sender: TObject);
    procedure BtnBuscarProveedorClick(Sender: TObject);
    procedure GridDetalleKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DataAfterPost(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure TBtnEliminarClick(Sender: TObject);
  private
    { Private declarations }
    Clase: TRetencionIslr;
    DetRetencionIslr: TDetRetencionIslr;
    BuscadorRetencionIslr: TBusquedaRetencionIslr;
    Proveedor: TProveedor;
    Zona: TZona;
    AnticipoProveedor: TAnticipoProveedor;
    NotaDebitoProveedor: TNotaDebitoProveedor;
    FacturaProveedor: TFacturacionProveedor;
    TipoRetencion: TTipoRetencion;
    Reporte: TReporte;
    Parametro: TParametro;
    DataBuscadorRetencionIslr: TClientDataSet;
    DataProveedor: TClientDataSet;
    DataZona: TClientDataSet;
    procedure Agregar;
    procedure Editar;
    procedure Eliminar;
    procedure Guardar;
    procedure Cancelar;
    procedure Refrescar;
    procedure Imprimir;
    procedure Buscar;
    procedure Limpiar;
    procedure Asignar;
    procedure Obtener;
    procedure Mover;
    procedure GuardarDetalles;
    procedure CalcularMontos;
  protected
    { Protected declarations }
    function Validar: Boolean; override;
    procedure Detalle; override;
  public
    { Public declarations }
  end;

var
  FrmRetencionIslr: TFrmRetencionIslr;

implementation

uses
  ActividadSophie,
  PModule,
  ClsGenericoBD,
  AgregarDetalleRetencionIslr
  {$IFDEF USE_RAVE_REPORTS}
  , DMRR
  {$ENDIF}
  {$IFDEF USE_FAST_REPORTS}
  , DMFR
  {$ENDIF}
  ;

{$R *.dfm}

{ TFrmRetencionIslr }

procedure TFrmRetencionIslr.Agregar;
begin
  //Verificando
  Limpiar;
  //Asignando Valores por Defecto
  CmbCodigoZona.Value := Parametro.ObtenerValor('codigo_zona');
  CmbCodigoProveedor.SetFocus;
end;

procedure TFrmRetencionIslr.Asignar;
var
  DataDetalle: TClientDataSet;
  Campo: string;
  i: Integer;

  function BuscarDescripcionRetencion(varCodigo: string): string;
  begin
    Result := '';
    if (Length(Trim(varCodigo)) > 0) then
    begin
      TipoRetencion.SetCodigo(varCodigo);
      TipoRetencion.Buscar;
      Result := TipoRetencion.GetDescripcion;
    end;       
  end;

begin
  with Clase do
  begin
    TxtNumero.Text := GetNumero;
    //NumeroPedido
    CmbCodigoProveedor.KeyValue := GetCodigoProveedor;
    CmbCodigoZona.KeyValue := GetCodigoZona;
    DatFechaIngreso.Date := GetFechaIngreso;
    DatFechaContable.Date := GetFechaContable;
    TxtTotal.Value := GetTotal;
    //Cargando DataSet Secundario "Data"
    DataDetalle := TClientDataSet.Create(Self);
    DataDetalle.CloneCursor(DetRetencionIslr.ObtenerNumero(GetNumero),False,True);
    Data.EmptyDataSet;
    DataDetalle.First;
    while (not DataDetalle.Eof) do
    begin
      Data.Insert;
      //Asignando Campos Similares
      for i := 0 to DataDetalle.FieldCount - 1 do
      begin
        Campo := DataDetalle.FieldDefs[i].Name;
        if (Data.FieldDefs.IndexOf(Campo) <> -1) then
        begin
          Data.FieldValues[Campo] := DataDetalle.FieldValues[Campo];
        end;        
      end;
      //Asignando Campos Faltantes
      if (DataDetalle.FieldValues['desde'] = 'AP') then
      begin //Anticipo a Proveedor
        AnticipoProveedor.SetNumero(DataDetalle.FieldValues['numero_documento']);
        AnticipoProveedor.BuscarNumero;
        Data.FieldValues['monto_base'] := AnticipoProveedor.GetMontoNeto;
        Data.FieldValues['p_iva'] := 0;
        Data.FieldValues['impuesto'] := 0;
      end
      else
      begin
        if (DataDetalle.FieldValues['desde'] = 'NDP') then
        begin //Nota Debito de Proveedor
          NotaDebitoProveedor.SetNumero(DataDetalle.FieldValues['numero_documento']);
          NotaDebitoProveedor.BuscarNumero;
          Data.FieldValues['monto_base'] := NotaDebitoProveedor.GetMontoNeto;
          Data.FieldValues['p_iva'] := NotaDebitoProveedor.GetPIva;
          Data.FieldValues['impuesto'] := NotaDebitoProveedor.GetImpuesto;
        end
        else
        begin //Factura de Proveedor
          FacturaProveedor.SetNumero(DataDetalle.FieldValues['numero_documento']);
          FacturaProveedor.BuscarNumero;
          Data.FieldValues['monto_base'] := FacturaProveedor.GetMontoNeto;
          Data.FieldValues['p_iva'] := FacturaProveedor.GetPIva;
          Data.FieldValues['impuesto'] := FacturaProveedor.GetImpuesto;
        end;
      end;
      Data.FieldValues['descripcion_retencion_1'] := BuscarDescripcionRetencion(DataDetalle.FieldValues['codigo_retencion_1']);
      Data.FieldValues['descripcion_retencion_2'] := BuscarDescripcionRetencion(DataDetalle.FieldValues['codigo_retencion_2']);
      Data.FieldValues['descripcion_retencion_3'] := BuscarDescripcionRetencion(DataDetalle.FieldValues['codigo_retencion_3']);
      Data.FieldValues['descripcion_retencion_4'] := BuscarDescripcionRetencion(DataDetalle.FieldValues['codigo_retencion_4']);
      Data.FieldValues['descripcion_retencion_5'] := BuscarDescripcionRetencion(DataDetalle.FieldValues['codigo_retencion_5']);
      Data.Post;
      DataDetalle.Next;
    end;
    DS.DataSet := Data;
  end;
end;

procedure TFrmRetencionIslr.BtnBuscarProveedorClick(Sender: TObject);
begin
  Buscador(Self,Proveedor,'codigo','Buscar Proveedor',nil,DataProveedor);
  CmbCodigoProveedor.KeyValue := DataProveedor.FieldValues['codigo'];
  CmbCodigoProveedorExit(Sender);
end;

procedure TFrmRetencionIslr.BtnBuscarZonaClick(Sender: TObject);
begin
  Buscador(Self,Zona,'codigo','Buscar Zona',nil,DataZona);
  CmbCodigoZona.KeyValue := DataZona.FieldValues['codigo'];
end;

procedure TFrmRetencionIslr.Buscar;
begin
  if (Buscador(Self,BuscadorRetencionIslr,'numero','Buscar Retenciones',nil,DataBuscadorRetencionIslr)) then
  begin
    Clase.SetNumero(DataBuscadorRetencionIslr.FieldValues['numero']);
    Clase.BuscarNumero;
    Asignar;
  end;
end;

procedure TFrmRetencionIslr.CalcularMontos;
var
  Neto: Currency;
begin
  Neto := 0;
  with Data do
  begin
    DisableControls;
    First;
    while (not Eof) do
    begin
      Neto := Neto + (FieldValues['monto_retencion_1'] +
                      FieldValues['monto_retencion_2'] +
                      FieldValues['monto_retencion_3'] +
                      FieldValues['monto_retencion_4'] +
                      FieldValues['monto_retencion_5']);
      Next;
    end;
    EnableControls;
  end;
  //Asignando Monto
  TxtTotal.Value := Neto;
end;

procedure TFrmRetencionIslr.Cancelar;
begin
  if (Agregando) then
  begin
    Limpiar;
  end
  else
  begin
    Clase.SetNumero(TxtNumero.Text);
    Clase.BuscarNumero;
    Asignar;
  end;
end;

procedure TFrmRetencionIslr.CmbCodigoProveedorChange(Sender: TObject);
begin
  SB.Panels[0].Text := DataProveedor.FieldValues['razon_social'];
end;

procedure TFrmRetencionIslr.CmbCodigoProveedorExit(Sender: TObject);
begin
  if (Length(Trim(CmbCodigoProveedor.Text)) > 0) then
  begin
    if (Agregando) then
    begin
      Proveedor.SetCodigo(CmbCodigoProveedor.Value);
      Proveedor.BuscarCodigo;
      CmbCodigoZona.Value := Proveedor.GetCodigoZona;
    end;
  end;
end;

procedure TFrmRetencionIslr.DataAfterPost(DataSet: TDataSet);
begin
  CalcularMontos;
end;

procedure TFrmRetencionIslr.Detalle;
begin
  inherited;
  FrmAgregarDetalleRetencionIslr := TFrmAgregarDetalleRetencionIslr.CrearDetalle(Self,Data);
  FrmAgregarDetalleRetencionIslr.CodigoProveedor := CmbCodigoProveedor.Text;
  FrmAgregarDetalleRetencionIslr.ShowModal;
  FreeAndNil(FrmAgregarDetalleRetencionIslr);
end;

procedure TFrmRetencionIslr.Editar;
begin
  if (Length(Trim(TxtNumero.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
end;

procedure TFrmRetencionIslr.Eliminar;
begin
  //Se Busca La Declaracion de Retenciones
  Clase.SetNumero(TxtNumero.Text);
  Clase.BuscarNumero;
  //Se Eliminan Los Detalles
  DetRetencionIslr.EliminarNumero(Clase.GetNumero);
  Clase.Eliminar;
  MessageBox(Self.Handle, 'Las Retenciones ISLR Se Han Eliminado Con Exito!!!',PChar(MsgTituloInformacion), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  Refrescar;
  Limpiar;
end;

procedure TFrmRetencionIslr.FormCreate(Sender: TObject);
begin
  inherited;
  ProcAgregar := Agregar;
  ProcEditar := Editar;
  ProcEliminar := Eliminar;
  ProcGuardar := Guardar;
  ProcCancelar := Cancelar;
  ProcRefrescar := Refrescar;
  ProcImprimir := Imprimir;
  ProcBuscar := Buscar;
  FrmActividadSophie.Show;
  FrmActividadSophie.Actividad('Cargando N�cleo...');
  Clase := TRetencionIslr.Create;
  //Inicializando DataSets Secundarios
  BuscadorRetencionIslr := TBusquedaRetencionIslr.Create;
  Proveedor := TProveedor.Create;
  Zona := TZona.Create;
  AnticipoProveedor := TAnticipoProveedor.Create;
  NotaDebitoProveedor := TNotaDebitoProveedor.Create;
  FacturaProveedor := TFacturacionProveedor.Create;
  TipoRetencion := TTipoRetencion.Create;
  Reporte := TReporte.Create;
  Parametro := TParametro.Create;
  DetRetencionIslr := TDetRetencionIslr.Create;
  FrmActividadSophie.Actividad('Cargando Proveedores...');
  DataProveedor := Proveedor.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Zonas...');
  DataZona := Zona.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Resto del N�cleo...');
  DataBuscadorRetencionIslr := BuscadorRetencionIslr.ObtenerLista;
  DataProveedor.First;
  DataZona.First;
  DataBuscadorRetencionIslr.First;
  DSProveedor.DataSet := DataProveedor;
  DSZona.DataSet := DataZona;
  ConfigurarRxDBLookupCombo(CmbCodigoProveedor,DSProveedor,'codigo','codigo;razon_social');
  ConfigurarRxDBLookupCombo(CmbCodigoZona,DSZona,'codigo','codigo;descripcion');
  DS.DataSet := Data;
  GridDetalle.DataSource := DS;
  FrmActividadSophie.Close;
  //Buscar;
end;

procedure TFrmRetencionIslr.GridDetalleKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Agregando or Editando) then
  begin
    if (((Key = VK_INSERT) or (Key = VK_DELETE)) and (Length(Trim(CmbCodigoProveedor.Text)) = 0)) then
    begin
      MessageBox(Self.Handle, 'Seleccione Primero un Proveedor!!!', PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      CmbCodigoProveedor.SetFocus;
      CmbCodigoProveedor.DropDown;
      Abort;
    end;
  end;
  //Llamando a Evento Padre
  inherited;
end;

procedure TFrmRetencionIslr.Guardar;
begin
  CalcularMontos;
  Obtener;
  if (Agregando) then
  begin
    //Agregando...
    //Guardando Encabezado
    Clase.Insertar;
    TxtNumero.Text := Clase.GetNumero;
    Application.ProcessMessages;
    //Guardando Detalles
    GuardarDetalles;
    MessageBox(Self.Handle, 'Las Retenciones ISLR Han Sido Agregadas Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    //Se Imprime El Comprobante
    MessageBox(Self.Handle, 'Presione "Aceptar" Para Proceder A Imprimir El Comprobante de Retenci�n!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    with Reporte do
    begin
      AgregarParametro('NumeroRetencion',Clase.GetNumero);
      AgregarParametro('MontoLetra',NumeroALetras(Clase.GetTotal));
      EmiteReporte('RetencionISLR',trGeneral);
    end;
  end
  else
  begin
    //Editando...
    Clase.Modificar;
    //Actualizando Detalles (Eliminando Anteriores y Cargando los Nuevos)
    DetRetencionIslr.EliminarNumero(Clase.GetNumero);
    //Guardando Detalles
    GuardarDetalles;
    MessageBox(Self.Handle, 'Las Retenciones ISLR Han Sido Editadas Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end;
  //Actualizando Dataset de Busqueda
  DataBuscadorRetencionIslr := BuscadorRetencionIslr.ObtenerLista;
end;

procedure TFrmRetencionIslr.GuardarDetalles;
begin
  with Data do
  begin
    DisableControls;
    First;
    while (not Eof) do
    begin
      with DetRetencionIslr do
      begin
        SetNumero(Clase.GetNumero);
        SetDesde(FieldValues['desde']);
        SetNumeroDocumento(FieldValues['numero_documento']);
        SetCodigoRetencion1(FieldValues['codigo_retencion_1']);
        SetMontoRetencion1(FieldValues['monto_retencion_1']);
        SetCodigoRetencion2(FieldValues['codigo_retencion_2']);
        SetMontoRetencion2(FieldValues['monto_retencion_2']);
        SetCodigoRetencion3(FieldValues['codigo_retencion_3']);
        SetMontoRetencion3(FieldValues['monto_retencion_3']);
        SetCodigoRetencion4(FieldValues['codigo_retencion_4']);
        SetMontoRetencion4(FieldValues['monto_retencion_4']);
        SetCodigoRetencion5(FieldValues['codigo_retencion_5']);
        SetMontoRetencion5(FieldValues['monto_retencion_5']);
        Insertar;
      end;
      Next;
    end;
    EnableControls;
  end;
end;

procedure TFrmRetencionIslr.Imprimir;
begin
  if (Length(Trim(TxtNumero.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
  with Reporte do
  begin
    AgregarParametro('NumeroRetencion',Clase.GetNumero);
    AgregarParametro('MontoLetra',NumeroALetras(Clase.GetTotal));
    EmiteReporte('RetencionISLR',trGeneral);
  end;
end;

procedure TFrmRetencionIslr.Limpiar;
begin
  TxtNumero.Text := '';
  CmbCodigoProveedor.KeyValue := '';
  CmbCodigoZona.KeyValue := '';
  DatFechaIngreso.Date := Date;
  DatFechaContable.Date := Date;
  TxtTotal.Value := 0;
  //Status Bar
  SB.Panels[0].Text := '';
  Data.EmptyDataSet;
end;

procedure TFrmRetencionIslr.Mover;
begin

end;

procedure TFrmRetencionIslr.Obtener;
begin
  with Clase do
  begin
    SetNumero(TxtNumero.Text);
    SetCodigoProveedor(CmbCodigoProveedor.KeyValue);
    SetCodigoZona(CmbCodigoZona.KeyValue);
    SetFechaIngreso(DatFechaIngreso.Date);
    SetFechaContable(DatFechaContable.Date);
    SetTotal(TxtTotal.Value);
  end;
end;

procedure TFrmRetencionIslr.Refrescar;
begin
  //Actualizando Dataset de Busqueda
  DataBuscadorRetencionIslr := BuscadorRetencionIslr.ObtenerLista;
  //Se Actualizan Los demas Componentes
  DataProveedor := Proveedor.ObtenerCombo;
  DataZona := Zona.ObtenerCombo;
  DataProveedor.First;
  DataZona.First;
  DataBuscadorRetencionIslr.First;
  DSProveedor.DataSet := DataProveedor;
  DSZona.DataSet := DataZona;
end;

procedure TFrmRetencionIslr.TBtnEliminarClick(Sender: TObject);
begin
  if (Length(Trim(TxtNumero.Text)) > 0) then
  begin
    inherited;
  end
  else
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
end;

function TFrmRetencionIslr.Validar: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  //Validando Proveedor
  if (Length(Trim(CmbCodigoProveedor.Text)) = 0) then
  begin
    ListaErrores.Add('El Proveedor No Puede Estar En Blanco!!!');
  end;
  //Validando Monto
  if (TxtTotal.Value <= 0) then
  begin
    ListaErrores.Add('El Total Debe Ser Positivo!!!');
  end;
  //Validando Fechas
  if (DatFechaIngreso.Date > DatFechaContable.Date) then
  begin
    ListaErrores.Add('La Fecha de Ingreso Debe Ser Menor a la Fecha Contable!!!');
  end;
  //Validando Detalle
  if (Data.RecordCount = 0) then
  begin
    ListaErrores.Add('El Detalle De Las Retenciones No Puede Estar Vacio!!!');
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
