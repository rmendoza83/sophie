unit ConteoInventario;

{$I sophie.inc}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GenericoDetalle, DB, DBClient, ImgList, XPMan, ComCtrls, Grids,
  DBGrids, ExtCtrls, ToolWin, StdCtrls, Buttons, RxLookup, Mask, rxToolEdit,
  Utils, ClsConteoInventario, ClsEstacion, ClsZona, ClsEsquemaPago,
  ClsDetConteoInventario, ClsBusquedaConteoInventario, GenericoBuscador,
  ClsReporte, ClsParametro, ClsProducto, ExtDlgs{$IFDEF DELPHI10}, ImageList{$ENDIF};

type
  TControlConteo = record
    Index: Integer;
    Grid: TDBGrid;
    DSource: TDataSource;
    DSet: TClientDataSet;
  end;
  
  TArrayControlConteo = array of TControlConteo;
  
  TFrmConteoInventario = class(TFrmGenericoDetalle)
    LblNumero: TLabel;
    LblCodigoEstacion: TLabel;
    LblCodigoZona: TLabel;
    LblFechaIngreso: TLabel;
    TxtNumero: TEdit;
    DatFechaIngreso: TDateEdit;
    CmbCodigoEstacion: TRxDBLookupCombo;
    CmbCodigoZona: TRxDBLookupCombo;
    BtnBuscarEstacion: TBitBtn;
    BtnBuscarZona: TBitBtn;
    LblSubTotal: TLabel;
    CmbCantidadConteo: TComboBox;
    TabObservaciones: TTabSheet;
    TxtObservaciones: TRichEdit;
    Datareferencia: TStringField;
    Datacodigo_producto: TStringField;
    Datadescripcion: TStringField;
    Datacantidad: TFloatField;
    DSEstacion: TDataSource;
    DSZona: TDataSource;
    ODlg: TOpenTextFileDialog;
    procedure BtnBuscarZonaClick(Sender: TObject);
    procedure BtnBuscarEstacionClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CmbCantidadConteoChange(Sender: TObject);
    procedure TBtnEliminarClick(Sender: TObject);
  private
    { Private declarations }
    Clase: TConteoInventario;
    DetConteoInventario: TDetConteoInventario;
    BuscadorConteoInventario: TBusquedaConteoInventario;
    Estacion: TEstacion;
    Zona: TZona;
    Reporte: TReporte;
    Parametro: TParametro;
    Producto: TProducto;
    DataBuscadorConteoInventario: TClientDataSet;
    DataEstacion: TClientDataSet;
    DataZona: TClientDataSet;
    //Variable Especial Para el Control Visual Din�mico de los Conteos
    ArrayControlConteo: TArrayControlConteo;
    procedure Agregar;
    procedure Editar;
    procedure Eliminar;
    procedure Guardar;
    procedure Cancelar;
    procedure Refrescar;
    procedure Imprimir;
    procedure Buscar;
    procedure Limpiar;
    procedure Asignar;
    procedure Obtener;
    procedure Mover;
    procedure GuardarDetalles;
    procedure GenerarControlesVisualesConteos;
  protected
    { Protected declarations }
    function Validar: Boolean; override;
    procedure Detalle; override;
  public
    { Public declarations }
  end;

var
  FrmConteoInventario: TFrmConteoInventario;

implementation

uses
  ActividadSophie,
  PModule
  {$IFDEF USE_RAVE_REPORTS}
  , DMRR
  {$ENDIF}
  {$IFDEF USE_FAST_REPORTS}
  , DMFR
  {$ENDIF}
  ;

{$R *.dfm}

procedure TFrmConteoInventario.Agregar;
begin
  //Verificando
  Limpiar;
  //Asignando Valores por Defecto
  CmbCodigoZona.Value := Parametro.ObtenerValor('codigo_zona');
end;

procedure TFrmConteoInventario.Asignar;
var
  i: Integer;
  AuxDataSet: TClientDataSet;
begin
  with Clase do
  begin
    TxtNumero.Text := GetNumero;
    CmbCodigoZona.KeyValue := GetCodigoZona;
    CmbCodigoEstacion.KeyValue := GetCodigoEstacion;
    DatFechaIngreso.Date := GetFecha;
    CmbCantidadConteo.ItemIndex := CmbCantidadConteo.Items.IndexOf(IntToStr(GetCantidadConteo));
    //Generando Controles Visuales Dinamicos
    GenerarControlesVisualesConteos;
    TxtObservaciones.Text := GetObservaciones;
    //Asignando Datos En Los Datasets segun corresponda
    for i := 0 to GetCantidadConteo - 1 do
    begin
      AuxDataSet := DetConteoInventario.ObtenerIdConteoInventarioNumeroConteo(GetIdConteoInventario,i + 1);
      AuxDataSet.First;
      while (not AuxDataSet.Eof) do
      begin
        with ArrayControlConteo[i].DSet do
        begin
          Append;
          FieldValues['codigo_producto'] := AuxDataSet.FieldValues['codigo_producto'];
          FieldValues['referencia'] := AuxDataSet.FieldValues['referencia'];
          FieldValues['descripcion'] := AuxDataSet.FieldValues['descripcion'];
          FieldValues['cantidad'] := AuxDataSet.FieldValues['cantidad'];
          Post;
          First;
        end;
        AuxDataSet.Next;
      end;
    end;
  end;
end;

procedure TFrmConteoInventario.BtnBuscarEstacionClick(Sender: TObject);
begin
  Buscador(Self,Estacion,'codigo','Buscar Estacion',nil,DataEstacion);
  CmbCodigoEstacion.KeyValue := DataEstacion.FieldValues['codigo'];
end;

procedure TFrmConteoInventario.BtnBuscarZonaClick(Sender: TObject);
begin
  Buscador(Self,Zona,'codigo','Buscar Zona',nil,DataZona);
  CmbCodigoZona.KeyValue := DataZona.FieldValues['codigo'];
end;

procedure TFrmConteoInventario.Buscar;
begin
 if (Buscador(Self,BuscadorConteoInventario,'numero','Buscar Conteo Inventario',nil,DataBuscadorConteoInventario)) then
  begin
    Clase.SetNumero(DataBuscadorConteoInventario.FieldValues['numero']);
    Clase.BuscarNumero;
    Asignar;
  end;
end;

procedure TFrmConteoInventario.Cancelar;
begin
  if (Agregando) then
  begin
    Limpiar;
  end
  else
  begin
    Clase.SetNumero(TxtNumero.Text);
    Clase.BuscarNumero;
    Asignar;
  end;
end;

procedure TFrmConteoInventario.CmbCantidadConteoChange(Sender: TObject);
begin
  GenerarControlesVisualesConteos;
end;

procedure TFrmConteoInventario.Detalle;
var
  i, j: Integer;
  AuxDataSet: TClientDataSet;
  ArrayString1, ArrayString2: TArrayString;
  S: string;
begin
  inherited;
  if (ODlg.Execute) then
  begin
    Screen.Cursor := crHourGlass;
    i := PageDetalles.ActivePage.Tag;
    AuxDataSet := ArrayControlConteo[i].DSet;
    AuxDataSet.DisableControls;
    AuxDataSet.EmptyDataSet;
    ArrayString1 := FileToArrayString(ODlg.FileName);
    //Determinando Tipo de Archivo a Cargar
    if (LowerCase(ExtractFileExt(ODlg.FileName)) = '.csv') then
    begin
      //Caso de Archivo de Texto con Formato (Proporcionado por Sophie)
      for j := Low(ArrayString1) + 1 to High(ArrayString1) do
      begin
        S := Trim(ArrayString1[j]);
        ArrayString2 := Explode(';',S);
        Producto.SetCodigo(Trim(ArrayString2[0]));
        if (Producto.BuscarCodigo) then
        begin
          with AuxDataSet do
          begin
            Append;
            FieldValues['codigo_producto'] := Producto.GetCodigo;
            FieldValues['referencia'] := Producto.GetReferencia;
            FieldValues['descripcion'] := Producto.GetDescripcion;
            FieldValues['cantidad'] := StrToFloat(ArrayString2[3]);
            Post;
          end;
        end;
      end;
    end
    else
    begin
      //Caso de Archivo de Texto sin Formato (Listado de Codigos de Productos)
      for j := Low(ArrayString1) to High(ArrayString1) do
      begin
        S := StringReplace(Trim(ArrayString1[j]),'"','',[rfReplaceAll, rfIgnoreCase]);
        Producto.SetCodigo(S);
        if (Producto.BuscarCodigo) then
        begin
          with AuxDataSet do
          begin
            if (Locate('codigo_producto',Producto.GetCodigo,[loCaseInsensitive])) then
            begin
              Edit;
              FieldValues['cantidad'] := FieldValues['cantidad'] + 1;
            end
            else
            begin
              Append;
              FieldValues['codigo_producto'] := Producto.GetCodigo;
              FieldValues['referencia'] := Producto.GetReferencia;
              FieldValues['descripcion'] := Producto.GetDescripcion;
              FieldValues['cantidad'] := 1;
            end;
            Post;
          end;
        end;
      end;
    end;
    Screen.Cursor := crDefault;
    AuxDataSet.EnableControls;
    AuxDataSet.First;
  end;
end;

procedure TFrmConteoInventario.Editar;
begin
  if (Length(Trim(TxtNumero.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end
  else
  begin
    CmbCantidadConteo.Enabled := False;
  end;
end;

procedure TFrmConteoInventario.Eliminar;
begin
  if (Length(Trim(TxtNumero.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end
  else
  begin
    //Se Busca El Conteo
    Clase.SetNumero(TxtNumero.Text);
    Clase.BuscarNumero;
    //Se Eliminan Los Detalles
    DetConteoInventario.EliminarIdConteoInventario(Clase.GetIdConteoInventario);
    //Se Elimina El Conteo
    Clase.Eliminar;
    MessageBox(Self.Handle, 'El Conteo de Inventario Se Ha Eliminado Con Exito!!!',PChar(MsgTituloInformacion), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Refrescar;
    Limpiar;
  end;
end;

procedure TFrmConteoInventario.FormCreate(Sender: TObject);
var
  i, max: Integer;
begin
  inherited;
  ProcAgregar := Agregar;
  ProcEditar := Editar;
  ProcEliminar := Eliminar;
  ProcGuardar := Guardar;
  ProcCancelar := Cancelar;
  ProcRefrescar := Refrescar;
  ProcImprimir := Imprimir;
  ProcBuscar := Buscar;
  FrmActividadSophie.Show;
  FrmActividadSophie.Actividad('Cargando N�cleo...');
  Clase := TConteoInventario.Create;
  //Inicializando DataSets Secundarios
  BuscadorConteoInventario := TBusquedaConteoInventario.Create;
  Estacion := TEstacion.Create;
  Zona := TZona.Create;
  Producto := TProducto.Create;
  Reporte := TReporte.Create;
  Parametro := TParametro.Create;
  DetConteoInventario := TDetConteoInventario.Create;
  FrmActividadSophie.Actividad('Cargando Zonas...');
  DataZona := Zona.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Estaciones...');
  DataEstacion := Estacion.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Resto del N�cleo...');
  DataBuscadorConteoInventario := BuscadorConteoInventario.ObtenerLista;
  DataZona.First;
  DataEstacion.First;
  DataBuscadorConteoInventario.First;
  DSEstacion.DataSet := DataEstacion;
  DSZona.DataSet := DataZona;
  ConfigurarRxDBLookupCombo(CmbCodigoEstacion,DSEstacion,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoZona,DSZona,'codigo','codigo;descripcion');
  DS.DataSet := Data;
  GridDetalle.DataSource := DS;
  FrmActividadSophie.Close;
  SetLength(ArrayControlConteo,0);
  TabDetalle.TabVisible := False;
  PageDetalles.ActivePage := TabObservaciones;
  max := Parametro.ObtenerValor('maximo_conteo_inventario');
  CmbCantidadConteo.Items.Clear;
  CmbCantidadConteo.Items.Add('Seleccione...');
  for i := 0 to max - 1 do
  begin
    CmbCantidadConteo.Items.Add(IntToStr(i + 1));
  end;
  CmbCantidadConteo.ItemIndex := 0;
end;

procedure TFrmConteoInventario.GenerarControlesVisualesConteos;
var
  i, j, k, n: Integer;
  TabSheetAux: TTabSheet;
  ColumnAux: TColumn;
begin
  Screen.Cursor := crHourGlass;
  //Limpiando Memoria de los Controles Creados Anteriormente
  for i := Low(ArrayControlConteo) to High(ArrayControlConteo) do
  begin
    with ArrayControlConteo[i] do
    begin
      DSet.Close;
      DSet.Destroy;
      DSource.Destroy;
      Grid.Destroy;
    end;
  end;
  SetLength(ArrayControlConteo,0);
  //Limpiando Tabs Generados Anteriormente
  i := 0;
  while (PageDetalles.PageCount <> 2) do
  begin
    if ((PageDetalles.Pages[i].Name <> 'TabDetalle') and (PageDetalles.Pages[i].Name <> 'TabObservaciones')) then
    begin
      PageDetalles.Pages[i].Destroy;
    end
    else
    begin
      i := i + 1;
    end;
  end;
  if (CmbCantidadConteo.ItemIndex > 0) then
  begin
    n := CmbCantidadConteo.ItemIndex;  
    //Asignando Nuevo Tama�o de Controles
    SetLength(ArrayControlConteo,n);
    k := 0;
    for i := 0 to n - 1 do
    begin
      //Agregando Page
      TabSheetAux := TTabSheet.Create(PageDetalles);
      TabSheetAux.Name := 'TabConteo' + IntToStr(i + 1);
      TabSheetAux.Caption := 'Conteo ' + IntToStr(i + 1);
      TabSheetAux.PageControl := PageDetalles;
      TabSheetAux.PageIndex := i + 1;
      TabSheetAux.Tag := i;
      with ArrayControlConteo[i] do
      begin
        Grid := TDBGrid.Create(TabSheetAux);
        //Copiando Columnas desde GridDetalle
        for j := 0 to GridDetalle.Columns.Count - 1 do
        begin
          ColumnAux := Grid.Columns.Add;
          ColumnAux.Assign(GridDetalle.Columns[j]);
        end;
        Grid.Align := alClient;
        Grid.TitleFont.Assign(GridDetalle.TitleFont);
        Grid.Parent := TabSheetAux;
        Grid.ReadOnly := True;
        Grid.OnKeyDown := GridDetalle.OnKeyDown;
        DSource := TDataSource.Create(nil);
        DSet := TClientDataSet.Create(nil);
        DSet.FieldDefs.Assign(Data.FieldDefs);
        DSet.CreateDataSet;
        DSource.DataSet := DSet;
        Grid.DataSource := DSource;
      end;
      k := i;
    end;
    TabObservaciones.PageIndex := k + 2;
    PageDetalles.ActivePageIndex := 1;
    Application.ProcessMessages;
  end;
  Screen.Cursor := crDefault;
end;

procedure TFrmConteoInventario.Guardar;
begin
  Obtener;
  PageDetalles.ActivePageIndex := 1;
  if (Agregando) then
  begin
    //Agregando...
    //Guardando Encabezado
    Clase.Insertar;
    TxtNumero.Text := Clase.GetNumero;
    //Guardando Detalles
    GuardarDetalles;
    MessageBox(Self.Handle, 'El Conteo de Inventario Ha Sido Agregado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    with Reporte do
    begin
      AgregarParametro('Numero',Clase.GetNumero);
      EmiteReporte('ConteoInventario',trGeneral);
    end;
  end
  else
  begin
    //Editando...
    Clase.Modificar;
    //Actualizando Detalles (Eliminando Anteriores y Cargando los Nuevos)
    DetConteoInventario.EliminarIdConteoInventario(Clase.GetIdConteoInventario);
    //Guardando Detalles
    GuardarDetalles;
    MessageBox(Self.Handle, 'El Conteo de Inventario Ha Sido Editado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end;
end;

procedure TFrmConteoInventario.GuardarDetalles;
var
  i, conteo: Integer;
begin
  conteo := 0;
  for i := Low(ArrayControlConteo) to High(ArrayControlConteo) do
  begin
    conteo := conteo + 1;
    with ArrayControlConteo[i].DSet do
    begin
      DisableControls;
      First;
      while (not Eof) do
      begin
        DetConteoInventario.SetNumeroConteo(conteo);
        DetConteoInventario.SetCodigoProducto(FieldValues['codigo_producto']);
        DetConteoInventario.SetReferencia(FieldValues['referencia']);
        DetConteoInventario.SetDescripcion(FieldValues['descripcion']);
        DetConteoInventario.SetCantidad(FieldValues['cantidad']);
        DetConteoInventario.SetIdConteoInventario(Clase.GetIdConteoInventario);
        DetConteoInventario.Insertar;
        Next;
      end;
      First;
      EnableControls;
    end;
  end;
end;

procedure TFrmConteoInventario.Imprimir;
begin
  if (Length(Trim(TxtNumero.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
  with Reporte do
  begin
    AgregarParametro('Numero',Clase.GetNumero);
    EmiteReporte('ConteoInventario',trGeneral);
  end;
end;

procedure TFrmConteoInventario.Limpiar;
begin
  TxtNumero.Text := '';
  //NumeroPedido
  CmbCodigoZona.KeyValue := '';
  CmbCodigoEstacion.KeyValue := '';
  DatFechaIngreso.Date := Date;
  CmbCantidadConteo.ItemIndex := 0;
  CmbCantidadConteo.Enabled := True;
  TxtObservaciones.Text := '';
  GenerarControlesVisualesConteos;
  //Status Bar
  SB.Panels[0].Text := '';
  Data.EmptyDataSet;
  PageDetalles.ActivePage := TabObservaciones;
end;

procedure TFrmConteoInventario.Mover;
begin

end;

procedure TFrmConteoInventario.Obtener;
begin
  with Clase do
  begin
    SetNumero(TxtNumero.Text);
    SetCodigoZona(CmbCodigoZona.KeyValue);
    SetCodigoEstacion(CmbCodigoEstacion.KeyValue);
    SetFecha(DatFechaIngreso.Date);
    SetCantidadConteo(StrToInt(CmbCantidadConteo.Text));
    SetObservaciones(TxtObservaciones.Text);
    if (Agregando) then
    begin
      SetIdConteoInventario(-1);
    end;
  end;
end;

procedure TFrmConteoInventario.Refrescar;
begin
  //Se Actualiza El DataBuscador
  DataBuscadorConteoInventario := BuscadorConteoInventario.ObtenerLista;
  //Se Actualizan Los demas Componentes
  DataZona := Zona.ObtenerCombo;
  DataEstacion := Estacion.ObtenerCombo;
  DataZona.First;
  DataEstacion.First;
  DSZona.DataSet := DataZona;
  DSEstacion.DataSet := DataEstacion;
end;

procedure TFrmConteoInventario.TBtnEliminarClick(Sender: TObject);
begin
  if (Length(Trim(TxtNumero.Text)) > 0) then
  begin
    inherited;
  end
  else
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
end;

function TFrmConteoInventario.Validar: Boolean;
var
  ListaErrores: TStringList;
  i: Integer;
begin
  ListaErrores := TStringList.Create;
  //Validando Zona
  if (Length(Trim(CmbCodigoZona.Text)) = 0) then
  begin
    ListaErrores.Add('La Zona No Puede Estar En Blanco!!!');
  end;
  //Validando Estacion
  if (Length(Trim(CmbCodigoEstacion.Text)) = 0) then
  begin
    ListaErrores.Add('La Estaci�n No Puede Estar En Blanco!!!');
  end;
  //Validando Cantidad de Conteos
  if (CmbCantidadConteo.ItemIndex = 0) then
  begin
    ListaErrores.Add('La Cantidad de Conteos Debe Ser Mayor a Cero (0)!!!');
  end;
  //Validando Productos de Conteo
  for i := Low(ArrayControlConteo) to High(ArrayControlConteo) do
  begin
    if (ArrayControlConteo[i].DSet.RecordCount = 0) then
    begin
      ListaErrores.Add('Los Productos del Conteo ' + IntToStr(i + 1) + ' NO Fueron Cargados!!!');
    end;     
  end;    
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
