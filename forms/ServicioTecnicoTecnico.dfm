inherited FrmServicioTecnicoTecnico: TFrmServicioTecnicoTecnico
  Caption = 'Sophie - [SERVICIO TECNICO] T'#233'cnicos'
  ExplicitWidth = 640
  ExplicitHeight = 476
  PixelsPerInch = 96
  TextHeight = 13
  inherited BH: TToolBar
    ExplicitTop = -8
  end
  inherited PanelMaestro: TPanel
    Height = 205
    ExplicitTop = 30
    ExplicitHeight = 205
    object LblCodigo: TLabel
      Left = 41
      Top = 13
      Width = 41
      Height = 13
      Alignment = taRightJustify
      Caption = 'C'#243'digo:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LblRif: TLabel
      Left = 242
      Top = 13
      Width = 73
      Height = 13
      Alignment = taRightJustify
      Caption = 'C'#233'dula o RIF:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LblRazonSocial: TLabel
      Left = 8
      Top = 37
      Width = 74
      Height = 13
      Alignment = taRightJustify
      Caption = 'Raz'#243'n Social:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LblTelefonos: TLabel
      Left = 24
      Top = 61
      Width = 58
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tel'#233'fonos:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LblNit: TLabel
      Left = 485
      Top = 13
      Width = 22
      Height = 13
      Alignment = taRightJustify
      Caption = 'NIT:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LblDireccionFiscal: TLabel
      Left = 23
      Top = 133
      Width = 89
      Height = 13
      Alignment = taRightJustify
      Caption = 'Direcci'#243'n Fiscal:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LblFechaIngreso: TLabel
      Left = 242
      Top = 61
      Width = 100
      Height = 13
      Alignment = taRightJustify
      Caption = 'Fecha de Ingreso:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LblCodigoZona: TLabel
      Left = 51
      Top = 85
      Width = 31
      Height = 13
      Alignment = taRightJustify
      Caption = 'Zona:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object TxtDescripcionZona: TDBText
      Left = 270
      Top = 85
      Width = 94
      Height = 13
      AutoSize = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object TxtEdad: TLabel
      Left = 400
      Top = 37
      Width = 5
      Height = 13
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clHotLight
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LblPComisionGlobal: TLabel
      Left = 25
      Top = 109
      Width = 118
      Height = 13
      Alignment = taRightJustify
      Caption = '(%) Comisi'#243'n Global:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object TxtCodigo: TEdit
      Left = 94
      Top = 10
      Width = 142
      Height = 21
      TabOrder = 0
    end
    object CmbPrefijoRif: TComboBox
      Left = 327
      Top = 10
      Width = 32
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 1
      Items.Strings = (
        'V'
        'E'
        'J'
        'G')
    end
    object TxtRif: TEdit
      Left = 367
      Top = 10
      Width = 102
      Height = 21
      MaxLength = 10
      TabOrder = 2
    end
    object TxtRazonSocial: TEdit
      Left = 94
      Top = 34
      Width = 415
      Height = 21
      TabOrder = 3
    end
    object TxtTelefonos: TEdit
      Left = 94
      Top = 58
      Width = 142
      Height = 21
      TabOrder = 4
    end
    object TxtNit: TEdit
      Left = 519
      Top = 10
      Width = 104
      Height = 21
      TabOrder = 5
    end
    object TxtDireccionFiscal1: TEdit
      Left = 26
      Top = 149
      Width = 485
      Height = 21
      TabOrder = 6
    end
    object TxtDireccionFiscal2: TEdit
      Left = 26
      Top = 173
      Width = 485
      Height = 21
      TabOrder = 7
    end
    object DatFechaIngreso: TDateEdit
      Left = 351
      Top = 58
      Width = 108
      Height = 21
      DefaultToday = True
      DialogTitle = 'Seleccione Fecha'
      NumGlyphs = 2
      YearDigits = dyFour
      TabOrder = 8
      Text = '15/11/2011'
    end
    object CmbCodigoZona: TRxDBLookupCombo
      Left = 94
      Top = 82
      Width = 144
      Height = 21
      DropDownCount = 20
      DropDownWidth = 400
      TabOrder = 9
    end
    object BtnBuscarZona: TBitBtn
      Left = 241
      Top = 82
      Width = 23
      Height = 22
      TabOrder = 10
      OnClick = BtnBuscarZonaClick
      Glyph.Data = {
        36060000424D3606000000000000360400002800000020000000100000000100
        08000000000000020000850E0000850E00000001000000000000000000003300
        00006600000099000000CC000000FF0000000033000033330000663300009933
        0000CC330000FF33000000660000336600006666000099660000CC660000FF66
        000000990000339900006699000099990000CC990000FF99000000CC000033CC
        000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
        0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
        330000333300333333006633330099333300CC333300FF333300006633003366
        33006666330099663300CC663300FF6633000099330033993300669933009999
        3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
        330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
        66006600660099006600CC006600FF0066000033660033336600663366009933
        6600CC336600FF33660000666600336666006666660099666600CC666600FF66
        660000996600339966006699660099996600CC996600FF99660000CC660033CC
        660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
        6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
        990000339900333399006633990099339900CC339900FF339900006699003366
        99006666990099669900CC669900FF6699000099990033999900669999009999
        9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
        990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
        CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
        CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
        CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
        CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
        CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
        FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
        FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
        FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
        FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
        000000808000800000008000800080800000C0C0C00080808000191919004C4C
        4C00B2B2B200E5E5E5005A1E1E00783C3C0096646400C8969600FFC8C800465F
        82005591B9006EB9D7008CD2E600B4E6F000D8E9EC0099A8AC00646F7100E2EF
        F100C56A31000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000EEEEEEEEEEEE
        EEEE0909EE0909EE0909EEEEEEEEEEEEEEEE8181EE8181EE8181EEEEEEEEEEEE
        EEEE1009EE1009EE1009EEEEEEEEEEEEEEEEE281EEE281EEE281EEEEEEEEEEEE
        EEB4D8EEEEEEEEEEEEEEEEEEEEEEEEEEEEE281EEEEEEEEEEEEEEEEEEEEEEEEEE
        B46C6CD8EEEEEEEEEEEEEEEEEEEEEEEEE2818181EEEEEEEEEEEEEEEEEEEEEEEE
        B46CD86CD8EEEEEEEEEEEEEEEEEEEEEEE281818181EEEEEEEEEEEEEEEEEEEEEE
        B46C6CD86CD8EEEEEEEEEEEEEEEEEEEEE28181818181EEEEEEEEEEEEEEEEEED7
        5E6C6C6CB46CD8EEEEEEEEEEEEEEEED781818181E28181EEEEEEEEEEEEEEEED7
        89896CB4B4B46CD8EEEEEEEEEEEEEED7ACAC81E2E2E28181EEEEEEEEEEEED789
        89D7D7B4C7C7C76CEEEEEEEEEEEED7ACACD7D7E2ACACAC81EEEEEEEEEED78989
        D7D7D7D76C6C6CEEEEEEEEEEEED7ACACD7D7D7D7818181EEEEEEEEEED78989D7
        D7D75E5EEEEEEEEEEEEEEEEED7ACACD7D7D78181EEEEEEEEEEEEEED78989D7D7
        D75EEEEEEEEEEEEEEEEEEED7ACACD7D7D781EEEEEEEEEEEEEEEED78989D7D7D7
        5EB4EEEEEEEEEEEEEEEED7ACACD7D7D781E2EEEEEEEEEEEEEEEE5E89D7D7D75E
        B4EEEEEEEEEEEEEEEEEE81ACD7D7D781E2EEEEEEEEEEEEEEEEEEEE5ED7D75EEE
        EEEEEEEEEEEEEEEEEEEEEE81D7D781EEEEEEEEEEEEEEEEEEEEEEEEEE5E5EEEEE
        EEEEEEEEEEEEEEEEEEEEEEEE8181EEEEEEEEEEEEEEEEEEEEEEEE}
      NumGlyphs = 2
    end
    object TxtPComisionGlobal: TCurrencyEdit
      Left = 150
      Top = 106
      Width = 98
      Height = 21
      Margins.Left = 1
      Margins.Top = 1
      DecimalPlaces = 4
      DisplayFormat = '#,##0.0000%'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 11
    end
  end
  inherited PageDetalles: TPageControl
    Top = 241
    Height = 186
    ExplicitTop = 241
    ExplicitHeight = 186
    inherited TabDetalle: TTabSheet
      Caption = 'Detalle de Comisiones por Tipos de Servicios'
      ExplicitLeft = 4
      ExplicitTop = 24
      ExplicitWidth = 626
      ExplicitHeight = 158
      inherited GridDetalle: TDBGrid
        Height = 158
        ParentFont = False
        TitleFont.Style = [fsBold]
        Columns = <
          item
            Expanded = False
            FieldName = 'codigo'
            Title.Caption = 'C'#243'digo'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'descripcion'
            Title.Caption = 'Descripci'#243'n'
            Width = 200
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'p_comision'
            Title.Caption = '(%) Comisi'#243'n'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'observaciones'
            Title.Caption = 'Observaciones'
            Width = 400
            Visible = True
          end>
      end
    end
  end
  inherited Data: TClientDataSet
    Active = True
    Data = {
      980000009619E0BD010000001800000004000000000003000000980014636F64
      69676F5F7469706F5F736572766963696F010049000000010005574944544802
      0002000A000B6465736372697063696F6E010049000000010005574944544802
      00020028000A705F636F6D6973696F6E08000400000000000D6F627365727661
      63696F6E657302004900000001000557494454480200020000010000}
    object Datacodigo_tipo_servicio: TStringField
      FieldName = 'codigo_tipo_servicio'
      Size = 10
    end
    object Datadescripcion: TStringField
      FieldName = 'descripcion'
      Size = 40
    end
    object Datap_comision: TFloatField
      FieldName = 'p_comision'
    end
    object Dataobservaciones: TStringField
      FieldName = 'observaciones'
      Size = 256
    end
  end
  object DSZona: TDataSource
    Left = 592
    Top = 224
  end
end
