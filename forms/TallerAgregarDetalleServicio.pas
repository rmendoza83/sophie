unit TallerAgregarDetalleServicio;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GenericoAgregarDetalle, StdCtrls, Mask, DB, rxToolEdit, rxCurrEdit,
  Buttons, ExtCtrls, ClsProducto, ClsTallerDetServicio, GenericoBuscador,
  Utils;

type
  TFrmTallerAgregarDetalle = class(TFrmGenericoAgregarDetalle)
    LblCantidad: TLabel;
    TxtCantidad: TCurrencyEdit;
    TxtDescripcionProducto: TEdit;
    LblFacturable: TLabel;
    ChkFacturable: TCheckBox;
    TxtObservaciones: TMemo;
    LblObservaciones: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure BtnBuscarClick(Sender: TObject);
    procedure TxtCantidadKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    Producto: TProducto;
  protected
    function Buscar: Boolean; override;
    procedure Agregar; override;
    procedure Limpiar; override;
  public
    { Public declarations }
  end;

var
  FrmTallerAgregarDetalle: TFrmTallerAgregarDetalle;

implementation

uses
  SolicitarMonto;

{$R *.dfm}

procedure TFrmTallerAgregarDetalle.Agregar;
var
  PrecioProducto: Currency;
begin
  //Verificando Precio
  PrecioProducto := 0;
  if Producto.GetPrecio <= 0 then
  begin
    while (PrecioProducto = 0) do
    begin
      Solicitar(Self,'Precio del Producto',0,PrecioProducto);
    end;
    if (Data.Locate('codigo_producto',TxtCodigo.Text,[loCaseInsensitive])) then
    begin
      Data.Delete;
    end;
  end
  else
  begin
    PrecioProducto := Producto.GetPrecio;
  end;
  with Data do
  begin
    //Verificando si existe el codigo en el Grid
    if (Locate('codigo_producto',TxtCodigo.Text,[loCaseInsensitive])) then
    begin
      Edit;
      FieldValues['cantidad'] := FieldValues['cantidad'] + TxtCantidad.Value;
      FieldValues['monto_descuento'] := (PrecioProducto * FieldValues['cantidad']) * (Producto.GetPDescuento / 100);
      FieldValues['impuesto'] := (PrecioProducto * FieldValues['cantidad']) * (Producto.GetPIvaVenta / 100);
      FieldValues['total'] := PrecioProducto * FieldValues['cantidad'];
    end
    else
    begin
      Append;
      FieldValues['codigo_producto'] := Producto.GetCodigo;
      FieldValues['referencia'] := Producto.GetReferencia;
      FieldValues['descripcion'] := Producto.GetDescripcion;
      FieldValues['cantidad'] := TxtCantidad.Value;
      FieldValues['precio'] := PrecioProducto;
      FieldValues['p_descuento'] := Producto.GetPDescuento;
      FieldValues['monto_descuento'] := (PrecioProducto * TxtCantidad.Value) * (Producto.GetPDescuento / 100);
      FieldValues['impuesto'] := (PrecioProducto * TxtCantidad.Value) * (Producto.GetPIvaVenta / 100);
      FieldValues['p_iva'] := Producto.GetPIvaVenta;
      FieldValues['total'] := PrecioProducto * TxtCantidad.Value;
      FieldValues['facturable'] := ChkFacturable.Checked;
      FieldValues['observaciones'] := TxtObservaciones.Text;
      FieldValues['id_taller_servicio'] := -1;
    end;
    Post;
  end;
  inherited;
end;

procedure TFrmTallerAgregarDetalle.BtnBuscarClick(Sender: TObject);
begin
  inherited;
  Buscador(Self,Producto,'codigo','Buscar Producto',TxtCodigo,nil);
  if (Length(Trim(TxtCodigo.Text)) > 0) then
  begin
    ComponenteFoco.SetFocus;
  end;
end;

function TFrmTallerAgregarDetalle.Buscar: Boolean;
begin
  Producto.SetCodigo(TxtCodigo.Text);
  Result := Producto.BuscarCodigo;
  if (Result) then
  begin
    TxtDescripcionProducto.Text := Producto.GetDescripcion;
  end
  else
  begin
    MessageBox(Self.Handle, Cadena('El Codigo No Existe!!!'), PChar(MsgTituloError), MB_ICONSTOP or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Limpiar;
  end;
end;

procedure TFrmTallerAgregarDetalle.FormCreate(Sender: TObject);
begin
  inherited;
  ComponenteFoco := (TxtCantidad as TWinControl);
  Producto := TProducto.Create;
  ChkFacturable.Checked := False; 
end;

procedure TFrmTallerAgregarDetalle.Limpiar;
begin
  inherited;
  TxtDescripcionProducto.Text := '';
  TxtCantidad.Value := 1;
  ChkFacturable.Checked := True;
  TxtObservaciones.Text := '';
end;

procedure TFrmTallerAgregarDetalle.TxtCantidadKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  case Key of
    VK_RETURN:
      Agregar;
  end;
end;

end.
