unit SophieSoporteTecnico;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, mxProtector;

type
  TFrmSophieSoporteTecnico = class(TForm)
    LblTitulo: TLabel;
    BtnAceptar: TBitBtn;
    BtnCancelar: TBitBtn;
    LblNombreRegistro: TLabel;
    TxtNombreRegistro: TLabel;
    LblNombre: TLabel;
    LblEmail: TLabel;
    LblTelefonos: TLabel;
    LblObservaciones: TLabel;
    TxtNombre: TEdit;
    TxtEmail: TEdit;
    TxtTelefonos: TEdit;
    TxtObservaciones: TMemo;
    MXP: TmxProtector;
    procedure BtnCancelarClick(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure MXPCheckRegistration(Sender: TObject; var UserName,
      SerialNumber: string; var Registered: Boolean);
    procedure MXPDayTrial(Sender: TObject; DaysRemained: Integer);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
    RegisteredUser: string;
    DaysTrial: Integer;
    procedure EnviarEmail;
  public
    { Public declarations }
  end;

var
  FrmSophieSoporteTecnico: TFrmSophieSoporteTecnico;

implementation

uses
  Utils,
  DMIndySMTP;

{$R *.dfm}

procedure TFrmSophieSoporteTecnico.BtnAceptarClick(Sender: TObject);
begin
  if (Length(Trim(TxtNombre.Text)) = 0) or
     (Length(Trim(TxtEmail.Text)) = 0) or
     (Length(Trim(TxtTelefonos.Text)) = 0) or
     (Length(Trim(TxtObservaciones.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Debe Especificar Todos Los Campos!!!',PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end
  else
  begin
    if (MessageBox(Self.Handle, '�Esta Seguro Que Los Campos Estan Correctos?',PChar(MsgTituloInformacion), MB_ICONQUESTION or MB_YESNO or MB_TOPMOST or MB_TASKMODAL) = mrYes) then
    begin
      EnviarEmail;
    end;
  end;
end;

procedure TFrmSophieSoporteTecnico.BtnCancelarClick(Sender: TObject);
begin
  Close;
end;

procedure TFrmSophieSoporteTecnico.EnviarEmail;
var
  CorreoEnviado: Boolean;
  Body: string;
begin
  Screen.Cursor := crHourGlass;
  //Configurando Sender
  DMIndySMTPSender := TDMIndySMTPSender.Create(Self);
  try
    with DMIndySMTPSender do
    begin
      ConfigSMTPServer('smtp.gmail.com','soluciones.informatica.rye',Cadena('delphi2006'),587);
      SetBodyHTML(True);
      InitMessage;
      SetTimeout(0);
      //Configurando Correo al Soporte Tecnico
      SetFrom('soluciones.informatica.rye@gmail.com');
      SetFromName('Sophie Sistema Administrativo');
      SetSubject('Atenci�n al Usuario - ' + TxtNombreRegistro.Caption);
      AddTo('soluciones.informatica.rye@gmail.com');
      AddCCO('rmendoza83@gmail.com');
      AddCCO('eliudduno@outlook.com');
      Body := '<h2>EL CLIENTE "' + TxtNombreRegistro.Caption + '" HA REQUERIDO SOPORTE TECNICO</h2>' +
              '<p><b>Nombre:</b> ' + TxtNombre.Text + '</p>' +
              '<p><b>Email:</b> ' + TxtEmail.Text + '</p>' +
              '<p><b>Telefonos:</b> ' + TxtTelefonos.Text + '</p>' +
              '<p><b>Observaciones:</b> <p>' + TxtObservaciones.Text + '</p></p>' +
              '<br/><br/>' +
              FormatDateTime('dd/mm/yyyy hh:nn:ss',Now) +
              '<h5>Sophie - Sistema Administrativo</h5>';
      SetBody(Body);
      CorreoEnviado := SendEmail;
      //Configurando Correo al Cliente
      if (CorreoEnviado) then
      begin
        InitMessage;
        SetTimeout(0);
        SetFrom('soluciones.informatica.rye@gmail.com');
        SetFromName('Sophie Sistema Administrativo');
        SetSubject('Se Ha Recibido una Solicitud de Contacto de Soporte T�cnico...');
        //AddTo('"' + TxtNombre.Text + '" <' + TxtEmail.Text + '>');
        AddTo(TxtEmail.Text);
        Body := '<p>Usted ha realizado una solicitud de Contacto de Soporte T�cnico desde el Sistema Administrativo Sophie.</p>' +
                '<p>En poco tiempo su solicitud sera atendida.</p>' +
                '<br /><p>Gracias por utilizar el Modulo de Contacto de Soporte T�cnico.</p>' +
                '<br/><br/>' +
                '<h5>Sophie - Sistema Administrativo</h5>';
        SetBody(Body);
        SendEmail;
      end;
    end;
  finally
    DMIndySMTPSender.Free;
  end;
  Screen.Cursor := crDefault;
  if (CorreoEnviado) then
  begin
    MessageBox(Self.Handle, 'El Contacto Se Ha Realizado Correctamente. En Poco Tiempo Ser� Atendido!!!',PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Close;
  end
  else
  begin
    MessageBox(Self.Handle, 'El Contacto No Ha Podido Ser Realizado. Por Favor Verifique Conexi�n a Internet!!!',PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end;
end;

procedure TFrmSophieSoporteTecnico.FormActivate(Sender: TObject);
begin
  if (MXP.IsRegistered) then
  begin
    TxtNombreRegistro.Caption := RegisteredUser;
  end
  else
  begin
    TxtNombreRegistro.Caption := 'Demo (' + IntToStr(DaysTrial) + ' Restantes)';
  end;
end;

procedure TFrmSophieSoporteTecnico.MXPCheckRegistration(Sender: TObject;
  var UserName, SerialNumber: string; var Registered: Boolean);
begin
  if (Registered) then
  begin
    RegisteredUser := UserName;
  end
  else
  begin
    RegisteredUser := 'Demo';
  end;
end;

procedure TFrmSophieSoporteTecnico.MXPDayTrial(Sender: TObject;
  DaysRemained: Integer);
begin
  if (not MXP.IsRegistered) then
  begin
    DaysTrial := DaysRemained;  
  end
  else
  begin
    DaysTrial := 0;
  end;
end;

end.
