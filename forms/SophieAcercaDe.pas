unit SophieAcercaDe;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, XPMan, mxProtector, StdCtrls, Buttons, pngimage, frxClass;

type
  TFrmSophieAcercaDe = class(TForm)
    Image1: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Shape1: TShape;
    Label8: TLabel;
    GrpDatosLicencia: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    LblTipoLicencia: TLabel;
    LblRegistradoA: TLabel;
    LblSerial: TLabel;
    Label11: TLabel;
    LblTiempo: TLabel;
    BtnAceptar: TBitBtn;
    GroupBox1: TGroupBox;
    Label9: TLabel;
    PanelAyudaRegistro: TPanel;
    MXP: TmxProtector;
    TemaXP: TXPManifest;
    Label10: TLabel;
    Label12: TLabel;
    procedure BtnAceptarClick(Sender: TObject);
    procedure MXPCheckRegistration(Sender: TObject; var UserName,
      SerialNumber: string; var Registered: Boolean);
    procedure MXPDayTrial(Sender: TObject; DaysRemained: Integer);
    procedure PanelAyudaRegistroDblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmSophieAcercaDe: TFrmSophieAcercaDe;

implementation

uses
  PModule,
  Clipbrd;

{$R *.dfm}

procedure TFrmSophieAcercaDe.BtnAceptarClick(Sender: TObject);
begin
  Close;
end;

procedure TFrmSophieAcercaDe.FormCreate(Sender: TObject);
begin
  if (P.InValidUser) then
  begin
    PanelAyudaRegistro.Cursor := crSQLWait;
  end
  else
  begin
    PanelAyudaRegistro.Cursor := crDefault;
  end;
end;

procedure TFrmSophieAcercaDe.MXPCheckRegistration(Sender: TObject; var UserName,
  SerialNumber: string; var Registered: Boolean);
begin
  if (Registered) then
  begin
    LblTipoLicencia.Caption := 'Full Version';
    LblRegistradoA.Caption := UserName;
    LblSerial.Caption := SerialNumber;
    LblTiempo.Caption := 'N/A';
  end
  else
  begin
    LblTipoLicencia.Caption := 'Demo Version';
    LblRegistradoA.Caption := 'Sin Registro';
    LblSerial.Caption := 'Sin Registro';
  end;
end;

procedure TFrmSophieAcercaDe.MXPDayTrial(Sender: TObject;
  DaysRemained: Integer);
begin
  if (MXP.IsRegistered) then
  begin
    LblTiempo.Caption := 'N/A';
  end
  else
  begin
    LblTiempo.Caption := IntToStr(DaysRemained) + ' Dias...';
  end;
end;

procedure TFrmSophieAcercaDe.PanelAyudaRegistroDblClick(Sender: TObject);
begin
  Clipboard.AsText := P.GetSophieValidationPath;
end;

end.
