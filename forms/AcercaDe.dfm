object FrmAcercaDe: TFrmAcercaDe
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Sophie - Acerca de...'
  ClientHeight = 393
  ClientWidth = 390
  Color = clBtnFace
  DragKind = dkDock
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Image1: TImage
    Left = 16
    Top = 8
    Width = 64
    Height = 64
  end
  object Label1: TLabel
    Left = 96
    Top = 8
    Width = 32
    Height = 13
    Caption = 'Sophie'
  end
  object Label2: TLabel
    Left = 96
    Top = 27
    Width = 166
    Height = 13
    Caption = 'Sistema Administrativo Empresarial'
  end
  object Label3: TLabel
    Left = 96
    Top = 46
    Width = 161
    Height = 13
    Caption = 'Grupo Desarrolladores R&&M, C.A.'
  end
  object Label4: TLabel
    Left = 96
    Top = 65
    Width = 62
    Height = 13
    Caption = 'Versi'#243'n: 1.0.'
  end
  object Shape1: TShape
    Left = 16
    Top = 351
    Width = 361
    Height = 2
  end
  object Label8: TLabel
    Left = 16
    Top = 84
    Width = 339
    Height = 65
    Caption = 
      'Sophie, es un software para la administraci'#243'n gerencial de peque' +
      #241'as'#13#10'y medianas empresas, la copia o redistribuci'#243'n del mismo es' +
      'tan regidas'#13#10'bajos las normas explicitas de Grupo Desarrolladore' +
      's R&&M, C.A. '#13#10#13#10'Reservados Todos Los Derechos...'
  end
  object GrpDatosLicencia: TGroupBox
    Left = 16
    Top = 155
    Width = 361
    Height = 113
    Caption = 'Datos de Licencia:'
    TabOrder = 0
    object Label5: TLabel
      Left = 16
      Top = 24
      Width = 79
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tipo de Licencia:'
    end
    object Label6: TLabel
      Left = 30
      Top = 43
      Width = 65
      Height = 13
      Alignment = taRightJustify
      Caption = 'Registrado a:'
    end
    object Label7: TLabel
      Left = 65
      Top = 62
      Width = 30
      Height = 13
      Alignment = taRightJustify
      Caption = 'Serial:'
    end
    object LblTipoLicencia: TLabel
      Left = 101
      Top = 24
      Width = 35
      Height = 13
      Caption = 'xxxxx'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ShowAccelChar = False
    end
    object LblRegistradoA: TLabel
      Left = 101
      Top = 43
      Width = 35
      Height = 13
      Caption = 'xxxxx'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ShowAccelChar = False
    end
    object LblSerial: TLabel
      Left = 101
      Top = 62
      Width = 35
      Height = 13
      Caption = 'xxxxx'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ShowAccelChar = False
    end
    object Label11: TLabel
      Left = 57
      Top = 81
      Width = 38
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tiempo:'
    end
    object LblTiempo: TLabel
      Left = 101
      Top = 81
      Width = 35
      Height = 13
      Caption = 'xxxxx'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ShowAccelChar = False
    end
  end
  object BtnAceptar: TBitBtn
    Left = 302
    Top = 359
    Width = 75
    Height = 25
    Caption = '&Aceptar'
    Default = True
    TabOrder = 1
    OnClick = BtnAceptarClick
    Glyph.Data = {
      36060000424D3606000000000000360400002800000020000000100000000100
      08000000000000020000610B0000610B00000001000000000000000000003300
      00006600000099000000CC000000FF0000000033000033330000663300009933
      0000CC330000FF33000000660000336600006666000099660000CC660000FF66
      000000990000339900006699000099990000CC990000FF99000000CC000033CC
      000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
      0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
      330000333300333333006633330099333300CC333300FF333300006633003366
      33006666330099663300CC663300FF6633000099330033993300669933009999
      3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
      330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
      66006600660099006600CC006600FF0066000033660033336600663366009933
      6600CC336600FF33660000666600336666006666660099666600CC666600FF66
      660000996600339966006699660099996600CC996600FF99660000CC660033CC
      660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
      6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
      990000339900333399006633990099339900CC339900FF339900006699003366
      99006666990099669900CC669900FF6699000099990033999900669999009999
      9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
      990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
      CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
      CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
      CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
      CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
      CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
      FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
      FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
      FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
      FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
      000000808000800000008000800080800000C0C0C00080808000191919004C4C
      4C00B2B2B200E5E5E5005A1E1E00783C3C0096646400C8969600FFC8C800465F
      82005591B9006EB9D7008CD2E600B4E6F000D8E9EC0099A8AC00646F7100E2EF
      F100C56A31000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000EEEEEEEEEEEE
      EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
      EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE180C
      EEEEEEEEEEEEEEEEEEEEEEEEEEEEE2DFEEEEEEEEEEEEEEEEEEEEEEEEEE181212
      0CEEEEEEEEEEEEEEEEEEEEEEEEE28181DFEEEEEEEEEEEEEEEEEEEEEE18121212
      120CEEEEEEEEEEEEEEEEEEEEE281818181DFEEEEEEEEEEEEEEEEEE1812121212
      12120CEEEEEEEEEEEEEEEEE2818181818181DFEEEEEEEEEEEEEEEE1812120C18
      1212120CEEEEEEEEEEEEEEE28181DFE2818181DFEEEEEEEEEEEEEE18120CEEEE
      181212120CEEEEEEEEEEEEE281DFEEEEE2818181DFEEEEEEEEEEEE180CEEEEEE
      EE181212120CEEEEEEEEEEE2DFEEEEEEEEE2818181DFEEEEEEEEEEEEEEEEEEEE
      EEEE181212120CEEEEEEEEEEEEEEEEEEEEEEE2818181DFEEEEEEEEEEEEEEEEEE
      EEEEEE181212120CEEEEEEEEEEEEEEEEEEEEEEE2818181DFEEEEEEEEEEEEEEEE
      EEEEEEEE1812120CEEEEEEEEEEEEEEEEEEEEEEEEE28181DFEEEEEEEEEEEEEEEE
      EEEEEEEEEE18120CEEEEEEEEEEEEEEEEEEEEEEEEEEE281DFEEEEEEEEEEEEEEEE
      EEEEEEEEEEEE180CEEEEEEEEEEEEEEEEEEEEEEEEEEEEE2DFEEEEEEEEEEEEEEEE
      EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
      EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE}
    NumGlyphs = 2
  end
  object GroupBox1: TGroupBox
    Left = 16
    Top = 274
    Width = 361
    Height = 71
    Caption = 'Desarrolladores:'
    TabOrder = 2
    object Label9: TLabel
      Left = 16
      Top = 24
      Width = 237
      Height = 13
      Caption = 'Ing. Reinaldo Mendoza (rmendoza83@gmail.com)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
  end
  object PanelAyudaRegistro: TPanel
    Left = 16
    Top = 360
    Width = 16
    Height = 16
    Cursor = crSQLWait
    BevelOuter = bvNone
    TabOrder = 3
    OnDblClick = PanelAyudaRegistroDblClick
  end
  object MXP: TmxProtector
    CodeKey = 'Ignore'
    ProtectionTypes = [stDayTrial, stRegister]
    Options = [poAutoInit, poCheckSytemTime]
    RegistryRootKey = rkLocalMachine
    Expiration = 41456.773032013890000000
    MaxStartNumber = 0
    MaxDayNumber = 60
    UserName = 'SOLUCIONES INFORMATICAS R&E, C.A.'
    Version = '1.32'
    OnCheckRegistration = MXPCheckRegistration
    OnDayTrial = MXPDayTrial
    Left = 352
    Top = 8
    UniqueCodeID = 
      '7A4330453731354C3C273E3D3B3C2224212A56395454232934282E255F2F2915' +
      '11161115665B'
    UniqueID = 
      '21015772657169150175796473236B187510696E64076B0B4E011F7662677E1E' +
      '05690F267024'
  end
  object TemaXP: TXPManifest
    Left = 8
    Top = 8
  end
end
