unit CondominioInmueble;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Generico, XPMan, ImgList, DB, Grids, DBGrids, StdCtrls, Buttons,
  ExtCtrls, ComCtrls, ToolWin, Mask, rxToolEdit, rxCurrEdit, DBCtrls, RxLookup,
  ClsCondominioInmueble, DBClient, Utils, ClsCondominioConjuntoAdministracion,
  ClsCondominioPropietario, ClsCondominioTipoInmueble, GenericoBuscador,
  ClsParametro;

type
  TFrmCondominioInmueble = class(TFrmGenerico)
    LblCodigoConjuntoAdministracion: TLabel;
    CmbCodigoConjuntoAdministracion: TRxDBLookupCombo;
    TxtCodigoConjuntoAdministracion: TDBText;
    LblCodigo: TLabel;
    TxtCodigo: TEdit;
    LblDescripcion: TLabel;
    TxtDescripcion: TEdit;
    LblObservaciones: TLabel;
    TxtObservaciones: TMemo;
    LblCodigoTipoInmueble: TLabel;
    CmbCodigoTipoInmueble: TRxDBLookupCombo;
    TxtCodigoTipoInmueble: TDBText;
    LblPAlicuota: TLabel;
    TxtPAlicuota: TCurrencyEdit;
    DSConjuntoAdministracion: TDataSource;
    DSPropietario: TDataSource;
    LblCodigoPropietario: TLabel;
    CmbCodigoPropietario: TRxDBLookupCombo;
    TxtCodigoPropietario: TDBText;
    BtnBuscarPropietario: TBitBtn;
    DSTipoInmueble: TDataSource;
    procedure GrdBusquedaCellClick(Column: TColumn);
    procedure FormCreate(Sender: TObject);
    procedure BtnBuscarPropietarioClick(Sender: TObject);
  private
    { Private declarations }
    Clase: TCondominioInmueble;
    ConjuntoAdministracion: TCondominioConjuntoAdministracion;
    Propietario: TCondominioPropietario;
    TipoInmueble: TCondominioTipoInmueble;
    Parametro: TParametro;
    Data: TClientDataSet;
    DataConjuntoAdministracion: TClientDataSet;
    DataPropietario: TClientDataSet;
    DataTipoInmueble: TClientDataSet;
    procedure Agregar;
    procedure Editar;
    procedure Eliminar;
    procedure Guardar;
    procedure Cancelar;
    procedure Refrescar;
    procedure Imprimir;
    procedure Buscar;
    procedure Limpiar;
    procedure Asignar;
    procedure Obtener;
    procedure Mover;
  protected
    function Validar: Boolean; override;
  public
    { Public declarations }
  end;

var
  FrmCondominioInmueble: TFrmCondominioInmueble;

implementation

{$R *.dfm}

procedure TFrmCondominioInmueble.Agregar;
begin
//Verificando
  Limpiar;
  CmbCodigoConjuntoAdministracion.Value := Parametro.ObtenerValor('codigo_conjunto_administracion');
  CmbCodigoConjuntoAdministracion.SetFocus;
end;

procedure TFrmCondominioInmueble.Asignar;
begin
  with Clase do
  begin
    CmbCodigoConjuntoAdministracion.KeyValue := GetCodigoConjuntoAdministracion;
    TxtCodigo.Text := GetCodigo;
    CmbCodigoPropietario.KeyValue := GetCodigoPropietario;
    TxtDescripcion.Text := GetDescripcion;
    CmbCodigoTipoInmueble.KeyValue := GetCodigoTipoInmueble;
    TxtObservaciones.Text := GetObservaciones;
    TxtPAlicuota.Value := GetPAlicuota;
  end;
end;

procedure TFrmCondominioInmueble.BtnBuscarPropietarioClick(Sender: TObject);
begin
  Buscador(Self,Propietario,'codigo','Buscar Propietario',nil,DataPropietario);
  CmbCodigoPropietario.KeyValue := DataPropietario.FieldValues['codigo'];
end;

procedure TFrmCondominioInmueble.Buscar;
begin
  Screen.Cursor := crSQLWait;
  if (Length(Trim(TxtCriterio.Text)) > 0) then
  begin
    Data := Clase.ObtenerListaCriterio(Trim(TxtCriterio.Text));
  end
  else
  begin
    Data := Clase.ObtenerLista;
  end;
  Data.First;
  DS.DataSet := Data;
  GrdBusqueda.Refresh;
  Screen.Cursor := crDefault;
end;

procedure TFrmCondominioInmueble.Cancelar;
begin
  CmbCodigoConjuntoAdministracion.Enabled := True;
  TxtCodigo.Enabled := True;
end;

procedure TFrmCondominioInmueble.Editar;
begin
  //Se Busca el Elemento
  Clase.SetCodigoConjuntoAdministracion(Data.FieldValues['codigo_conjunto_administracion']);
  Clase.SetCodigo(Data.FieldValues['codigo']);
  Clase.Buscar;
  Asignar;
  CmbCodigoConjuntoAdministracion.Enabled := False;
  TxtCodigo.Enabled := False;
  CmbCodigoPropietario.SetFocus;
end;

procedure TFrmCondominioInmueble.Eliminar;
begin
  //Se Busca el Elemento
  Clase.SetCodigoConjuntoAdministracion(Data.FieldValues['codigo_conjunto_administracion']);
  Clase.SetCodigo(Data.FieldValues['codigo']);
  Clase.Buscar;
  Clase.Eliminar;
  MessageBox(Self.Handle, 'El Inmueble de Condominio Ha Sido Eliminado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  Refrescar;
  Limpiar;
end;

procedure TFrmCondominioInmueble.FormCreate(Sender: TObject);
begin
  inherited;
  ProcAgregar := Agregar;
  ProcEditar := Editar;
  ProcEliminar := Eliminar;
  ProcGuardar := Guardar;
  ProcCancelar := Cancelar;
  ProcRefrescar := Refrescar;
  ProcImprimir := Imprimir;
  ProcBuscar := Buscar;
  Clase := TCondominioInmueble.Create;
  //Inicializando DataSets Secundarios
  ConjuntoAdministracion := TCondominioConjuntoAdministracion.Create;
  Propietario := TCondominioPropietario.Create;
  TipoInmueble := TCondominioTipoInmueble.Create;
  Parametro := TParametro.Create; 
  DataConjuntoAdministracion := ConjuntoAdministracion.ObtenerLista;
  DataConjuntoAdministracion.First;
  DataPropietario := Propietario.ObtenerLista;
  DataPropietario.First;
  DataTipoInmueble := TipoInmueble.ObtenerLista;
  DataTipoInmueble.First;
  DSConjuntoAdministracion.DataSet := DataConjuntoAdministracion;
  DSPropietario.DataSet := DataPropietario;
  DSTipoInmueble.DataSet := DataTipoInmueble;
  TxtCodigoConjuntoAdministracion.DataSource := DSConjuntoAdministracion;
  TxtCodigoConjuntoAdministracion.DataField := 'descripcion';
  TxtCodigoPropietario.DataSource := DSPropietario;
  TxtCodigoPropietario.DataField := 'razon_social';
  TxtCodigoTipoInmueble.DataSource := DSTipoInmueble;
  TxtCodigoTipoInmueble.DataField := 'descripcion';
  ConfigurarRxDBLookupCombo(CmbCodigoConjuntoAdministracion,DSConjuntoAdministracion,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoPropietario,DSPropietario,'codigo','codigo;rif;razon_social');
  ConfigurarRxDBLookupCombo(CmbCodigoTipoInmueble,DSTipoInmueble,'codigo','codigo;descripcion');
  Buscar;
end;

procedure TFrmCondominioInmueble.GrdBusquedaCellClick(Column: TColumn);
begin
  inherited;
  Mover;
end;

procedure TFrmCondominioInmueble.Guardar;
begin
  if (Agregando) then
  begin
    //Agregando...
    //Se Busca el Codigo
    Clase.SetCodigoConjuntoAdministracion(CmbCodigoConjuntoAdministracion.KeyValue);
    Clase.SetCodigo(TxtCodigo.Text);
    if (Clase.Buscar) then
    begin
      MessageBox(Self.Handle, 'El Código Ya Existe Para El Conjunto de Administración Seleccionado!!!', PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      TxtCodigo.SetFocus;
      Abort;
    end
    else
    begin
      Obtener;
      Clase.Insertar;
      MessageBox(Self.Handle, 'El Inmueble de Condominio Ha Sido Agregado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    end;
  end
  else
  begin
    //Editando...
    Obtener;
    Clase.Modificar;
    MessageBox(Self.Handle, 'El Inmueble de Condominio Ha Sido Editado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    CmbCodigoConjuntoAdministracion.Enabled := True;    
    TxtCodigo.Enabled := True;
  end;
  Refrescar;
  Limpiar;
end;

procedure TFrmCondominioInmueble.Imprimir;
begin

end;

procedure TFrmCondominioInmueble.Limpiar;
begin
  DataConjuntoAdministracion.First;
  CmbCodigoConjuntoAdministracion.KeyValue := '';
  TxtCodigo.Text := '';
  DataPropietario.First;
  CmbCodigoPropietario.KeyValue := '';
  TxtDescripcion.Text := '';
  DataTipoInmueble.First;
  CmbCodigoTipoInmueble.KeyValue := '';
  TxtObservaciones.Text := '';
  TxtPAlicuota.Value := 0;
end;

procedure TFrmCondominioInmueble.Mover;
var
  DSTemp: TDataSet;
begin
  if (DS.DataSet.RecordCount > 0) then
  begin
    DSTemp := DS.DataSet;
    with Clase do
    begin
      SetCodigoConjuntoAdministracion(DSTemp.FieldValues['codigo_conjunto_administracion']);
      SetCodigo(DSTemp.FieldValues['codigo']);
      SetCodigoPropietario(DSTemp.FieldValues['codigo_propietario']);
      SetDescripcion(DSTemp.FieldValues['descripcion']);
      SetCodigoTipoInmueble(DSTemp.FieldValues['codigo_tipo_inmueble']);
      SetObservaciones(DSTemp.FieldValues['observaciones']);
      SetPAlicuota(DSTemp.FieldValues['p_alicuota']);
    end;
    Asignar;
  end
  else
  begin
    Limpiar;
  end;
end;

procedure TFrmCondominioInmueble.Obtener;
begin
  with Clase do
  begin
    SetCodigoConjuntoAdministracion(VarToStr(CmbCodigoConjuntoAdministracion.KeyValue));
    SetCodigo(TxtCodigo.Text);
    SetCodigoPropietario(VarToStr(CmbCodigoPropietario.KeyValue));
    SetDescripcion(TxtDescripcion.Text);
    SetCodigoTipoInmueble(VarToStr(CmbCodigoTipoInmueble.KeyValue));
    SetObservaciones(TxtObservaciones.Text);
    SetPAlicuota(TxtPAlicuota.Value);
  end;
end;

procedure TFrmCondominioInmueble.Refrescar;
begin
  TxtCriterio.Text := '';
  Buscar;
end;

function TFrmCondominioInmueble.Validar: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  (* Validando Informacion del Inmueble de Condominio *)
  //Validando Codigo Conjunto Administracion
  if (Length(Trim(CmbCodigoConjuntoAdministracion.Text)) = 0) then
  begin
    ListaErrores.Add('El Conjunto de Administración No Puede Estar En Blanco!!!');
  end;
  //Validando Codigo
  if (Length(Trim(TxtCodigo.Text)) = 0) then
  begin
    ListaErrores.Add('El Código No Puede Estar En Blanco!!!');
  end;
  //Validando Codigo Propietario
  if (Length(Trim(CmbCodigoPropietario.Text)) = 0) then
  begin
    ListaErrores.Add('El Propietario No Puede Estar En Blanco!!!');
  end;
  //Validando Descripcion
  if (Length(Trim(TxtDescripcion.Text)) = 0) then
  begin
    ListaErrores.Add('La Descripción No Puede Estar En Blanco!!!');
  end;
  //Validando Codigo Tipo de Inmueble de Condominio
  if (Length(Trim(CmbCodigoTipoInmueble.Text)) = 0) then
  begin
    ListaErrores.Add('El Tipo de Inmueble del Condominio No Puede Estar En Blanco!!!');
  end;
  //Validando (%) Alicuota
  if (TxtPAlicuota.Value <= 0) then
  begin
    ListaErrores.Add('El Porcentaje de Alicuota No Puede ser Cero (0,0000)!!!');
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
