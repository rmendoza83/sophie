unit Cliente;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Generico, XPMan, ImgList, DB, Grids, DBGrids, StdCtrls, Buttons,
  ExtCtrls, ComCtrls, ToolWin, Mask, rxToolEdit, rxCurrEdit, DBCtrls,
  ClsCliente, ClsVendedor, ClsZona, ClsEsquemaPago, RxLookup, Utils, DBClient,
  ImageList;

type
  TFrmCliente = class(TFrmGenerico)
    LblCodigo: TLabel;
    TxtCodigo: TEdit;
    LblRif: TLabel;
    CmbPrefijoRif: TComboBox;
    TxtRif: TEdit;
    LblRazonSocial: TLabel;
    TxtRazonSocial: TEdit;
    LblTelefonos: TLabel;
    TxtTelefonos: TEdit;
    LblFechaNacimiento: TLabel;
    DatFechaNacimiento: TDateEdit;
    lblEmail: TLabel;
    TxtEmail: TEdit;
    LblNit: TLabel;
    TxtNit: TEdit;
    LblSexo: TLabel;
    CmbSexo: TComboBox;
    TxtDireccionDomicilio2: TEdit;
    LblDireccionDomicilio: TLabel;
    TxtDireccionDomicilio1: TEdit;
    TxtDireccionFiscal1: TEdit;
    TxtDireccionFiscal2: TEdit;
    LblDireccionFiscal: TLabel;
    LblFax: TLabel;
    TxtFax: TEdit;
    LblWebsite: TLabel;
    TxtWebsite: TEdit;
    DatFechaIngreso: TDateEdit;
    LblFechaIngreso: TLabel;
    LblContacto: TLabel;
    TxtContacto: TEdit;
    LblVendedor: TLabel;
    LblEsquemaPago: TLabel;
    LblZona: TLabel;
    TxtVendedor: TDBText;
    TxtZona: TDBText;
    TxtEsquemaPago: TDBText;
    TxtUniqueId: TLabel;
    TxtEdad: TLabel;
    CmbVendedor: TRxDBLookupCombo;
    CmbZona: TRxDBLookupCombo;
    CmbEsquemaPago: TRxDBLookupCombo;
    BtnBuscarZona: TBitBtn;
    BtnBuscarVendedor: TBitBtn;
    BtnEsquemaPago: TBitBtn;
    DSVendedor: TDataSource;
    DSZona: TDataSource;
    DSEsquemaPago: TDataSource;
    procedure FormCreate(Sender: TObject);
    procedure BtnBuscarZonaClick(Sender: TObject);
    procedure BtnBuscarVendedorClick(Sender: TObject);
    procedure BtnEsquemaPagoClick(Sender: TObject);
    procedure GrdBusquedaCellClick(Column: TColumn);
    procedure DatFechaNacimientoChange(Sender: TObject);
  private
    { Private declarations }
    Clase: TCliente;
    Vendedor: TVendedor;
    Zona: TZona;
    EsquemaPago: TEsquemaPago;
    Data: TClientDataSet;
    DataVendedor: TClientDataSet;
    DataZona: TClientDataSet;
    DataEsquemaPago: TClientDataSet;
    procedure Agregar;
    procedure Editar;
    procedure Eliminar;
    procedure Guardar;
    procedure Cancelar;
    procedure Refrescar;
    procedure Imprimir;
    procedure Buscar;
    procedure Limpiar;
    procedure Asignar;
    procedure Obtener;
    procedure Mover;
  protected
    function Validar: Boolean; override;
  public
    { Public declarations }
  end;

var
  FrmCliente: TFrmCliente;

implementation

uses
  GenericoBuscador,
  ClsAnticipoCliente,
  ClsFacturacionCliente,
  ClsNotaCreditoCliente,
  ClsNotaDebitoCliente;

{$R *.dfm}

{ TFrmCliente }

procedure TFrmCliente.Agregar;
begin
  //Verificando
  Limpiar;
  TxtCodigo.SetFocus
end;

procedure TFrmCliente.Editar;
begin
  //Se Busca el Elemento
  Clase.SetCodigo(Data.FieldValues['codigo']);
  Clase.BuscarCodigo;
  Asignar;
  TxtCodigo.Enabled := False;
  CmbPrefijoRif.SetFocus;
end;

procedure TFrmCliente.Eliminar;
var
  AnticipoCliente: TAnticipoCliente;
  FacturacionCliente: TFacturacionCliente;
  NotaCreditoCliente: TNotaCreditoCliente;
  NotaDebitoCliente: TNotaDebitoCliente;
  sw: Boolean;
begin
  //Se Valida Que El Cliente No Posee Movimientos En El Sistema
  sw := True;
  //Verificando Anticipos
  AnticipoCliente := TAnticipoCliente.Create;
  AnticipoCliente.SetCodigoCliente(Clase.GetCodigo);
  if (AnticipoCliente.BuscarCodigoCliente) then
  begin
    sw := False;
  end
  else
  begin
    //Verificando Facturas
    FacturacionCliente := TFacturacionCliente.Create;
    FacturacionCliente.SetCodigoCliente(Clase.GetCodigo);
    if (FacturacionCliente.BuscarCodigoCliente) then
    begin
      sw := False;
    end
    else
    begin
      //Verificando Notas de Credito
      NotaCreditoCliente := TNotaCreditoCliente.Create;
      NotaCreditoCliente.SetCodigoCliente(Clase.GetCodigo);
      if (NotaCreditoCliente.BuscarCodigoCliente) then
      begin
        sw := False;
      end
      else
      begin
        //Verificando Notas de Debito
        NotaDebitoCliente := TNotaDebitoCliente.Create;
        NotaDebitoCliente.SetCodigoCliente(Clase.GetCodigo);
        if (NotaDebitoCliente.Buscar) then
        begin
          sw := False;
        end;
      end;
    end;
  end;
  if (sw) then
  begin
    //Se Busca el Elemento
    Clase.SetCodigo(Data.FieldValues['codigo']);
    Clase.BuscarCodigo;
    Clase.Eliminar;
    MessageBox(Self.Handle, 'El Cliente Se Ha Eliminado Con Exito!!!',PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Refrescar;
    Limpiar;
  end
  else
  begin
    MessageBox(Self.Handle, 'El Cliente Presenta Movimientos En El Sistema y No Puede Ser Eliminado!!!',PChar(MsgTituloAdvertencia), MB_ICONEXCLAMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end;
end;

procedure TFrmCliente.Guardar;
begin
  if (Agregando) then
  begin
    //Agregando...
    //Se Busca el Codigo
    Clase.SetCodigo(TxtCodigo.Text);
    if (Clase.BuscarCodigo) then
    begin
      MessageBox(Self.Handle, 'El C�digo Ya Existe!!!', PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      TxtCodigo.SetFocus;
      Abort;
    end
    else
    begin
      Obtener;
      Clase.Insertar;
      MessageBox(Self.Handle, 'El Cliente Ha Sido Agregado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    end;
  end
  else
  begin
    //Editando...
    Obtener;
    Clase.Modificar;
    MessageBox(Self.Handle, 'El Cliente Ha Sido Editado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    TxtCodigo.Enabled := True;
  end;
  Refrescar;
  Limpiar;
end;

procedure TFrmCliente.Cancelar;
begin
  TxtCodigo.Enabled := True;
end;

procedure TFrmCliente.Refrescar;
begin
  TxtCriterio.Text := '';
  Buscar;
  //Se Actualizan Los demas Componentes
  DataVendedor:= Vendedor.ObtenerLista;
  DataZona:= Zona.ObtenerLista;
  DataEsquemaPago:= EsquemaPago.ObtenerLista;
  DataVendedor.First;
  DataZona.First;
  DataEsquemaPago.First;
  DSVendedor.DataSet:= DataVendedor;
  DSZona.DataSet:= DataZona;
  DSEsquemaPago.DataSet:= DataEsquemaPago;
end;

procedure TFrmCliente.Imprimir;
begin

end;

procedure TFrmCliente.Buscar;
begin
  Screen.Cursor := crSQLWait;
  if (Length(Trim(TxtCriterio.Text)) > 0) then
  begin
    Data := Clase.ObtenerListaCriterio(Trim(TxtCriterio.Text));
  end
  else
  begin
    Data := Clase.ObtenerLista;
  end;
  Data.First;
  DS.DataSet := Data;
  GrdBusqueda.Refresh;
  Screen.Cursor := crDefault;
end;

procedure TFrmCliente.Limpiar;
begin
  TxtCodigo.Text := '';
  CmbPrefijoRif.Text := '';
  TxtRif.Text := '';
  TxtNit.Text := '';
  CmbSexo.Text := '';
  DatFechaNacimiento.Date := Now;
  TxtRazonSocial.Text := '';
  TxtTelefonos.Text := '';
  TxtFax.Text := '';
  TxtEmail.Text := '';
  TxtWebsite.Text := '';
  DatFechaIngreso.Date := Now;
  TxtContacto.Text := '';
  CmbVendedor.KeyValue := '';
  CmbZona.KeyValue := '';
  CmbEsquemaPago.KeyValue := '';
  TxtDireccionFiscal1.Text := '';
  TxtDireccionFiscal2.Text := '';
  TxtDireccionDomicilio1.Text := '';
  TxtDireccionDomicilio2.Text := '';
  TxtUniqueId.Caption := '';
end;

procedure TFrmCliente.Asignar;
begin
  with Clase do
  begin
    TxtCodigo.Text := GetCodigo;
    CmbPrefijoRif.ItemIndex := CmbPrefijoRif.Items.IndexOf(GetPrefijoRif);
    TxtRif.Text := GetRif;
    TxtNit.Text := GetNit;
    CmbSexo.ItemIndex := CmbSexo.Items.IndexOf(GetSexo);
    DatFechaNacimiento.Date:= GetFechaNacimiento;
    TxtRazonSocial.Text := GetRazonSocial;
    TxtTelefonos.Text := GetTelefonos;
    TxtFax.Text := GetFax;
    TxtEmail.Text := GetEmail;
    TxtWebsite.Text := GetWebsite;
    DatFechaIngreso.Date:= GetFechaIngreso;
    TxtContacto.Text :=GetContacto;
    CmbVendedor.KeyValue :=GetCodigoVendedor;
    CmbZona.KeyValue := GetCodigoZona;
    CmbEsquemaPago.KeyValue:= GetCodigoEsquemaPago;
    TxtDireccionFiscal1.Text := GetDireccionFiscal1;
    TxtDireccionFiscal2.Text := GetDireccionFiscal2;
    TxtDireccionDomicilio1.Text := GetDireccionDomicilio1;
    TxtDireccionDomicilio2.Text := GetDireccionDomicilio2;
    DataVendedor.Locate('codigo',CmbVendedor.KeyValue,[loCaseInsensitive]);
    DataZona.Locate('codigo',CmbZona.KeyValue,[loCaseInsensitive]);
    DataEsquemaPago.Locate('codigo',CmbEsquemaPago.KeyValue,[loCaseInsensitive]);
    TxtUniqueId.Caption := IntToStr(GetUniqueId);
  end;
end;

procedure TFrmCliente.BtnBuscarVendedorClick(Sender: TObject);
begin
  Buscador(Self,Vendedor,'codigo','Busqueda de Vendedor',nil,DataVendedor);
  CmbVendedor.KeyValue := DataVendedor.FieldValues['codigo'];
end;

procedure TFrmCliente.BtnBuscarZonaClick(Sender: TObject);
begin
  Buscador(Self,Zona,'codigo','Busqueda de Zona',nil,DataZona);
  CmbZona.KeyValue := DataZona.FieldValues['codigo'];
end;

procedure TFrmCliente.BtnEsquemaPagoClick(Sender: TObject);
begin
  Buscador(Self,EsquemaPago,'codigo','Busqueda de Esquema de Pago',nil,DataEsquemaPago);
  CmbEsquemaPago.KeyValue := DataEsquemaPago.FieldValues['codigo'];
end;

procedure TFrmCliente.DatFechaNacimientoChange(Sender: TObject);
var
  Edad: Integer;
begin
  Edad := YearOld(Date,DatFechaNacimiento.Date);
  Edad := Edad * (-1);
  TxtEdad.Caption := IntToStr(Edad)+' A�os';
end;


procedure TFrmCliente.FormCreate(Sender: TObject);
begin
  inherited;
  ProcAgregar := Agregar;
  ProcEditar := Editar;
  ProcEliminar := Eliminar;
  ProcGuardar := Guardar;
  ProcCancelar := Cancelar;
  ProcRefrescar := Refrescar;
  ProcImprimir := Imprimir;
  ProcBuscar := Buscar;
  Clase := TCliente.Create;
  //Inicializando DataSets Secundarios
  Vendedor:= TVendedor.Create;
  Zona:= TZona.Create;
  EsquemaPago:= TEsquemaPago.Create;
  DataVendedor:= Vendedor.ObtenerLista;
  DataZona:= Zona.ObtenerLista;
  DataEsquemaPago:= EsquemaPago.ObtenerLista;
  DataVendedor.First;
  DataZona.First;
  DataEsquemaPago.First;
  DSVendedor.DataSet:= DataVendedor;
  DSZona.DataSet:= DataZona;
  DSEsquemaPago.DataSet:= DataEsquemaPago;
  TxtVendedor.DataSource:= DSVendedor;
  TxtVendedor.DataField:= 'descripcion';
  TxtZona.DataSource:= DSZona;
  TxtZona.DataField:= 'descripcion';
  TxtEsquemaPago.DataSource:= DSEsquemaPago;
  TxtEsquemaPago.DataField:= 'descripcion';
  ConfigurarRxDBLookupCombo(CmbVendedor,DSVendedor,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbZona,DSZona,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbEsquemaPago,DSEsquemaPago,'codigo','codigo;descripcion');
  Buscar;
end;

procedure TFrmCliente.GrdBusquedaCellClick(Column: TColumn);
begin
  Mover;
end;

procedure TFrmCliente.Obtener;
begin
  with Clase do
  begin
    SetCodigo(TxtCodigo.Text);
    SetPrefijoRif(CmbPrefijoRif.Text);
    SetRif(TxtRif.Text);
    SetNit(TxtNit.Text);
    SetSexo(CmbSexo.Text[1]);
    SetFechaNacimiento(DatFechaNacimiento.Date);
    SetRazonSocial(TxtRazonSocial.Text);
    SetTelefonos(TxtTelefonos.Text);
    SetFax(TxtFax.Text);
    SetEmail(TxtEmail.Text);
    SetWebsite(TxtWebsite.Text);
    SetFechaIngreso(DatFechaIngreso.Date);
    SetContacto(TxtContacto.Text);
    SetCodigoVendedor(CmbVendedor.Text);
    SetCodigoZona(CmbZona.Text);
    SetCodigoEsquemaPago(CmbEsquemaPago.Text);
    SetDireccionFiscal1(TxtDireccionFiscal1.Text);
    SetDireccionFiscal2(TxtDireccionFiscal2.Text);
    SetDireccionDomicilio1(TxtDireccionDomicilio1.Text);
    SetDireccionDomicilio2(TxtDireccionDomicilio2.Text);
    SetUniqueId(StrToIntDef(TxtUniqueId.Caption,0));
  end;
end;

procedure TFrmCliente.Mover;
var
  DSTemp: TDataSet;
begin
  if (DS.DataSet.RecordCount > 0) then
  begin
    DSTemp := DS.DataSet;
    with Clase do
    begin
      SetCodigo(DSTemp.FieldValues['codigo']);
      SetPrefijoRif(DSTemp.FieldValues['prefijo_rif']);
      SetRif(DSTemp.FieldValues['rif']);
      SetNit(DSTemp.FieldValues['nit']);
      SetRazonSocial(DSTemp.FieldValues['razon_social']);
      SetSexo(DSTemp.FieldByName('sexo').AsString[1]);
      SetFechaNacimiento(DSTemp.FieldByName('fecha_nacimiento').AsDateTime);
      SetDireccionFiscal1(DSTemp.FieldValues['direccion_fiscal_1']);
      SetDireccionFiscal2(DSTemp.FieldValues['direccion_fiscal_2']);
      SetDireccionDomicilio1(DSTemp.FieldValues['direccion_domicilio_1']);
      SetDireccionDomicilio2(DSTemp.FieldValues['direccion_domicilio_2']);
      SetTelefonos(DSTemp.FieldValues['telefonos']);
      SetFax(DSTemp.FieldValues['fax']);
      SetEmail(DSTemp.FieldValues['email']);
      SetWebsite(DSTemp.FieldValues['website']);
      SetFechaIngreso(DSTemp.FieldByName('fecha_ingreso').AsDateTime);
      SetContacto(DSTemp.FieldValues['contacto']);
      SetCodigoVendedor(DSTemp.FieldValues['codigo_vendedor']);
      SetCodigoEsquemaPago(DSTemp.FieldValues['codigo_esquema_pago']);
      SetCodigoZona(DSTemp.FieldValues['codigo_zona']);
      SetUniqueId(DSTemp.FieldValues['unique_id']);
    end;
    Asignar;
  end
  else
  begin
    Limpiar;
  end;
end;

function TFrmCliente.Validar: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  (* Validando Informacion del Cliente *)
  //Validando Codigo
  if (Length(Trim(TxtCodigo.Text)) = 0) then
  begin
    ListaErrores.Add('El C�digo No Puede Estar En Blanco!!!');
  end;
  //Validando Prefijo
  if (Length(Trim(CmbPrefijoRif.Text)) = 0) then
  begin
    ListaErrores.Add('Debe Asignar un Prefijo Ej. "E" o "V"!!!');
  end;
  //Validando Cedula
  if (Length(Trim(TxtRif.Text)) = 0) then
  begin
    ListaErrores.Add('La C�dula No Puede Estar En Blanco!!!');
  end;
  //Validando Nit
  if (Length(Trim(TxtNit.Text)) = 0) then
  begin
    ListaErrores.Add('El Nit No Puede Estar En Blanco!!!');
  end;
  //Validando Sexo
  if (Length(Trim(CmbSexo.Text)) = 0) then
  begin
    ListaErrores.Add('El Campo Sexo No Puede Estar En Blanco!!!');
  end;
  //Validando Razon Social
  if (Length(Trim(TxtRazonSocial.Text)) = 0) then
  begin
    ListaErrores.Add('La Raz�n Social No Puede Estar En Blanco!!!');
  end;
  //Validando Telefonos
  if (Length(Trim(TxtTelefonos.Text)) = 0) then
  begin
    ListaErrores.Add('Los Tel�fonos No Puede Estar En Blanco!!!');
  end;
  //Validando Codigo Vendedor
  if (Length(Trim(CmbVendedor.Text)) = 0) then
  begin
    ListaErrores.Add('Debe Seleccionar el Vendedor!!!');
  end;
  //Validando Codigo Zona
  if (Length(Trim(CmbZona.Text)) = 0) then
  begin
    ListaErrores.Add('Debe Seleccionar La Zona!!!');
  end;
  //Validando Esquema de Pago
  if (Length(Trim(CmbEsquemaPago.Text)) = 0) then
  begin
    ListaErrores.Add('Debe Seleccionar el Esquema de Pago!!!');
  end;
  //Validando Direccion Fiscal
  if (Length(Trim(TxtDireccionFiscal1.Text)) = 0) then
  begin
    ListaErrores.Add('La Direcci�n Fiscal No Puede Estar En Blanco!!!');
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;


end.
