unit AgregarDetalleClienteCuentaBancaria;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GenericoAgregarDetalle, StdCtrls, Mask, rxToolEdit, rxCurrEdit,
  Buttons, ExtCtrls, DB, Utils;

type
  TFrmAgregarDetalleClienteCuentaBancaria = class(TFrmGenericoAgregarDetalle)
    LblNumeroCuenta: TLabel;
    LblEntidad: TLabel;
    LblTipo: TLabel;
    LblEmail: TLabel;
    LblObservaciones: TLabel;
    TxtNumeroCuenta: TEdit;
    TxtObservaciones: TMemo;
    TxtEntidad: TEdit;
    TxtTipo: TEdit;
    TxtEmail: TEdit;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Private declarations }
    function Buscar: Boolean; override;
    procedure Agregar; override;
    procedure Limpiar; override;
  public
    { Public declarations }
  end;

var
  FrmAgregarDetalleClienteCuentaBancaria: TFrmAgregarDetalleClienteCuentaBancaria;

implementation

{$R *.dfm}

procedure TFrmAgregarDetalleClienteCuentaBancaria.Agregar;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  (* Validando Informacion de la Cuenta *)
  //Validando Numero de Cuenta
  if (Length(Trim(TxtNumeroCuenta.Text)) = 0) then
  begin
    ListaErrores.Add('El Numero de Cuenta No Puede Estar En Blanco!!!');
  end;
  //Validando Entidad
  if (Length(Trim(TxtEntidad.Text)) = 0) then
  begin
    ListaErrores.Add('La Entidad Bancaria No Puede Estar En Blanco!!!');
  end;
  //Validando Tipo
  if (Length(Trim(TxtTipo.Text)) = 0) then
  begin
    ListaErrores.Add('El Tipo de Cuenta No Puede Estar En Blanco!!!');
  end;
  //Validando Email
  if (Length(Trim(TxtEmail.Text)) = 0) then
  begin
    ListaErrores.Add('El Email No Puede Estar En Blanco!!!');
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    TxtNumeroCuenta.SetFocus;
  end
  else
  begin
    with Data do
    begin
      //Verificando si existe el codigo en el Grid
      if (Locate('numero_cuenta',TxtNumeroCuenta.Text,[loCaseInsensitive])) then
      begin
        MessageBox(Self.Handle, Cadena('El Numero de Cuenta Ya Fue Agregada!!!'), PChar(MsgTituloError), MB_ICONSTOP or MB_OK or MB_TOPMOST or MB_TASKMODAL);
        TxtNumeroCuenta.SetFocus;
        TxtNumeroCuenta.SelectAll;
      end
      else
      begin
        Append;
        FieldValues['numero_cuenta'] := TxtNumeroCuenta.Text;
        FieldValues['entidad'] := TxtEntidad.Text;
        FieldValues['tipo'] := TxtTipo.Text;
        FieldValues['email'] := TxtEmail.Text;
        FieldValues['observaciones'] := TxtObservaciones.Text;
        Post;
      end;
    end;
    inherited;
  end;
end;

function TFrmAgregarDetalleClienteCuentaBancaria.Buscar: Boolean;
begin
  Result := False;
end;

procedure TFrmAgregarDetalleClienteCuentaBancaria.FormShow(Sender: TObject);
begin
  inherited;
  TxtNumeroCuenta.SetFocus;
end;

procedure TFrmAgregarDetalleClienteCuentaBancaria.Limpiar;
begin
  inherited;
  TxtNumeroCuenta.Text := '';
  TxtEntidad.Text := '';
  TxtTipo.Text := '';
  TxtEmail.Text := '';
  TxtObservaciones.Text := '';
end;

end.
