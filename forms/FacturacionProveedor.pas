unit FacturacionProveedor;

{$I sophie.inc}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GenericoDetalle, DB, DBClient, ImgList, XPMan, ComCtrls, Grids,
  DBGrids, ExtCtrls, ToolWin, StdCtrls, Buttons, rxCurrEdit, Mask, rxToolEdit,
  RxLookup, Utils, ClsFacturacionProveedor, ClsProveedor, ClsVendedor,
  ClsEstacion, ClsZona, ClsEsquemaPago, ClsMoneda, ClsDetFacturacionProveedor,
  ClsProducto, ClsBusquedaFacturacionProveedor, ClsDetMovimientoInventario,
  ClsCuentaxPagar, GenericoBuscador, ClsReporte, ClsParametro, ClsPagoPagado,
  ClsDocumentoLegal, ClsDocumentoAnulado, ClsDetalleContadoPago
  {$IFDEF DELPHI10}, ImageList{$ENDIF};

type
  TFrmFacturacionProveedor = class(TFrmGenericoDetalle)
    LblNumero: TLabel;
    LblCodigoProveedor: TLabel;
    LblCodigoEstacion: TLabel;
    LblCodigoZona: TLabel;
    LblEsquemaPago: TLabel;
    LblMoneda: TLabel;
    LblFechaIngreso: TLabel;
    LblRecepcion: TLabel;
    LblFechaContable: TLabel;
    LblFechaLibros: TLabel;
    LblMontoNeto: TLabel;
    LblMontoDescuento: TLabel;
    LblSubTotal: TLabel;
    LblImpuesto: TLabel;
    LblTotal: TLabel;
    TxtPDescuento: TLabel;
    TxtPIVA: TLabel;
    TxtNumero: TEdit;
    CmbCodigoProveedor: TRxDBLookupCombo;
    DatFechaIngreso: TDateEdit;
    TxtMontoNeto: TCurrencyEdit;
    CmbCodigoEstacion: TRxDBLookupCombo;
    DatFechaRecepcion: TDateEdit;
    DatFechaContable: TDateEdit;
    DatFechaLibros: TDateEdit;
    CmbCodigoZona: TRxDBLookupCombo;
    CmbCodigoEsquemaPago: TRxDBLookupCombo;
    CmbCodigoMoneda: TRxDBLookupCombo;
    TxtMontoDescuento: TCurrencyEdit;
    TxtSubTotal: TCurrencyEdit;
    TxtImpuesto: TCurrencyEdit;
    TxtTotal: TCurrencyEdit;
    BtnBuscarProveedor: TBitBtn;
    BtnBuscarEstacion: TBitBtn;
    BtnBuscarZona: TBitBtn;
    BtnBuscarEsquemaPago: TBitBtn;
    BtnBuscarMoneda: TBitBtn;
    DSProveedor: TDataSource;
    DSEstacion: TDataSource;
    DSZona: TDataSource;
    DSEsquemaPago: TDataSource;
    DSMoneda: TDataSource;
    TabConcepto: TTabSheet;
    TxtConcepto: TRichEdit;
    TabObservaciones: TTabSheet;
    TxtObservaciones: TRichEdit;
    Datacodigo_producto: TStringField;
    Datareferencia: TStringField;
    Datadescripcion: TStringField;
    Datacantidad: TFloatField;
    Datap_descuento: TFloatField;
    Datamonto_descuento: TFloatField;
    Dataimpuesto: TFloatField;
    Datap_iva: TFloatField;
    Datatotal: TFloatField;
    Dataid_facturacion_proveedor: TIntegerField;
    Datacantidad_pendiente: TFloatField;
    Datacosto: TFloatField;
    procedure LblMontoNetoDblClick(Sender: TObject);
    procedure TBtnEliminarClick(Sender: TObject);
    procedure DataAfterPost(DataSet: TDataSet);
    procedure LblMontoDescuentoDblClick(Sender: TObject);
    procedure LblImpuestoDblClick(Sender: TObject);
    procedure TxtPIVADblClick(Sender: TObject);
    procedure TxtPDescuentoDblClick(Sender: TObject);
    procedure TxtMontoNetoExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CmbCodigoProveedorExit(Sender: TObject);
    procedure CmbCodigoProveedorChange(Sender: TObject);
    procedure BtnBuscarMonedaClick(Sender: TObject);
    procedure BtnBuscarEsquemaPagoClick(Sender: TObject);
    procedure BtnBuscarZonaClick(Sender: TObject);
    procedure BtnBuscarEstacionClick(Sender: TObject);
    procedure BtnBuscarProveedorClick(Sender: TObject);
  private
    { Private declarations }
    Clase: TFacturacionProveedor;
    DetFacturacionProveedor: TDetFacturacionProveedor;
    MovimientoInventario: TDetMovimientoInventario;
    CuentaxPagar: TCuentaxPagar;
    PagoPagado: TPagoPagado;
    BuscadorFacturacionProveedor: TBusquedaFacturacionProveedor;
    Proveedor: TProveedor;
    Estacion: TEstacion;
    Zona: TZona;
    EsquemaPago: TEsquemaPago;
    Moneda: TMoneda;
    Producto: TProducto;
    Reporte: TReporte;
    Parametro: TParametro;
    DocumentoLegal: TDocumentoLegal;
    DocumentoAnulado: TDocumentoAnulado;
    DetalleContadoPago: TDetalleContadoPago;
    DataBuscadorFacturacionProveedor: TClientDataSet;
    DataProveedor: TClientDataSet;
    DataEstacion: TClientDataSet;
    DataZona: TClientDataSet;
    DataEsquemaPago: TClientDataSet;
    DataMoneda: TClientDataSet;
    procedure Agregar;
    procedure Editar;
    procedure Eliminar;
    procedure Guardar;
    procedure Cancelar;
    procedure Refrescar;
    procedure Imprimir;
    procedure Buscar;
    procedure Limpiar;
    procedure Asignar;
    procedure Obtener;
    procedure Mover;
    procedure GuardarDetalles;
    procedure GuardarPagoPagado;
    procedure GuardarCuentaxPagar;
    procedure GuardarMovimientoInventario;
    procedure ActualizarCostoProducto;
    procedure CalcularMontos;
    procedure CalcularMontosCabecera;
  protected
    { Protected declarations }
    function Validar: Boolean; override;
    procedure Detalle; override;
  public
    { Public declarations }
  end;

var
  FrmFacturacionProveedor: TFrmFacturacionProveedor;

implementation

uses
  ActividadSophie,
  PModule,
  ClsGenericoBD,
  AgregarDetalleFacturacionProveedor,
  DetalleContadoPago,
  SolicitarMonto,
  DocumentoLegal
  {$IFDEF USE_RAVE_REPORTS}
  , DMRR
  {$ENDIF}
  {$IFDEF USE_FAST_REPORTS}
  , DMFR
  {$ENDIF}
  ;

{$R *.dfm}

{TFrmFacturacionProveedor}

procedure TFrmFacturacionProveedor.ActualizarCostoProducto;
var
  Tasa : Currency;
begin
  Tasa := 0;
  Moneda.SetCodigo(Clase.GetCodigoMoneda);
  Moneda.Buscar;
  Tasa := Moneda.GetTasaDolar;
  with Data do
  begin
    DisableControls;
    First;
    while (not Eof) do
    begin
      Producto.SetCodigo(FieldValues['codigo_producto']);
      Producto.BuscarCodigo;
      //Actualizando Costo Anterior
      Producto.SetCostoAnterior(Producto.GetCostoActual);
      Producto.SetFechaUCompra(Clase.GetFechaIngreso);
      //Actualizando Costo Actual
      Producto.SetCostoActual(FieldValues['costo']);
      //Actualizando Costo en Dolares
      Producto.SetUsCostoAnterior(Producto.GetCostoAnterior / Tasa);
      Producto.SetUsCostoActual(Producto.GetCostoActual / Tasa);
      //Se Modifica el Producto
      Producto.Modificar;
      Next;
    end;
    EnableControls;
  end;
end;

procedure TFrmFacturacionProveedor.Agregar;
begin
  //Verificando
  Limpiar;
  //Asignando Valores por Defecto
  TxtPIVA.Caption := FormatFloat('#,##0.0000%',Parametro.ObtenerValor('p_iva'));
  Clase.SetPIva(Parametro.ObtenerValor('p_iva'));
  CmbCodigoEstacion.Value := Parametro.ObtenerValor('codigo_estacion_entrada');
  CmbCodigoZona.Value := Parametro.ObtenerValor('codigo_zona');
  CmbCodigoEsquemaPago.Value := Parametro.ObtenerValor('codigo_esquema_pago');
  CmbCodigoMoneda.Value := Parametro.ObtenerValor('codigo_moneda');
  CmbCodigoProveedor.SetFocus;
end;

procedure TFrmFacturacionProveedor.Asignar;
begin
  if (Clase.GetFormaLibre) then
  begin
    TabDetalle.TabVisible := False;
    PageDetalles.ActivePage := TabConcepto;
    TxtMontoNeto.ReadOnly := False;
  end
  else
  begin
    TabDetalle.TabVisible := True;
    PageDetalles.ActivePage := TabDetalle;
    TxtMontoNeto.ReadOnly := True;
  end;
  with Clase do
  begin
    TxtNumero.Text := GetNumero;
    //NumeroPedido
    CmbCodigoProveedor.KeyValue := GetCodigoProveedor;
    CmbCodigoEstacion.KeyValue := GetCodigoEstacion;
    CmbCodigoZona.KeyValue := GetCodigoZona;
    CmbCodigoEsquemaPago.KeyValue := GetCodigoEsquemaPago;
    CmbCodigoMoneda.KeyValue := GetCodigoMoneda;
    DatFechaIngreso.Date := GetFechaIngreso;
    DatFechaRecepcion.Date := GetFechaRecepcion;
    DatFechaContable.Date := GetFechaContable;
    DatFechaLibros.Date := GetFechaLibros;
    TxtConcepto.Text := GetConcepto;
    TxtObservaciones.Text := GetObservaciones;
    TxtMontoNeto.Value := GetMontoNeto;
    TxtMontoDescuento.Value := GetMontoDescuento;
    TxtSubTotal.Value := GetSubTotal;
    TxtImpuesto.Value := GetImpuesto;
    TxtTotal.Value := GetTotal;
    TxtPDescuento.Caption := FormatFloat('###0.0000%',GetPDescuento);
    TxtPIVA.Caption := FormatFloat('###0.0000%',GetPIva);
    Data.CloneCursor(DetFacturacionProveedor.ObtenerIdFacturacionProveedor(GetIdFacturacionProveedor),False,True);
    DS.DataSet := Data;
  end;
end;

procedure TFrmFacturacionProveedor.BtnBuscarEsquemaPagoClick(Sender: TObject);
begin
  Buscador(Self,EsquemaPago,'codigo','Buscar Esquema de Pago',nil,DataEsquemaPago);
  CmbCodigoEsquemaPago.KeyValue := DataEsquemaPago.FieldValues['codigo'];
end;

procedure TFrmFacturacionProveedor.BtnBuscarEstacionClick(Sender: TObject);
begin
  Buscador(Self,Estacion,'codigo','Buscar Estacion',nil,DataEstacion);
  CmbCodigoEstacion.KeyValue := DataEstacion.FieldValues['codigo'];
end;

procedure TFrmFacturacionProveedor.BtnBuscarMonedaClick(Sender: TObject);
begin
  Buscador(Self,Moneda,'codigo','Buscar Moneda',nil,DataMoneda);
  CmbCodigoMoneda.KeyValue := DataMoneda.FieldValues['codigo'];
end;

procedure TFrmFacturacionProveedor.BtnBuscarZonaClick(Sender: TObject);
begin
  Buscador(Self,Zona,'codigo','Buscar Zona',nil,DataZona);
  CmbCodigoZona.KeyValue := DataZona.FieldValues['codigo'];
end;

procedure TFrmFacturacionProveedor.BtnBuscarProveedorClick(Sender: TObject);
begin
  Buscador(Self,Proveedor,'codigo','Buscar Proveedor',nil,DataProveedor);
  CmbCodigoProveedor.KeyValue := DataProveedor.FieldValues['codigo'];
  CmbCodigoProveedorExit(Sender);
end;

procedure TFrmFacturacionProveedor.Buscar;
begin
  if (Buscador(Self,BuscadorFacturacionProveedor,'numero','Buscar Factura',nil,DataBuscadorFacturacionProveedor)) then
  begin
    Clase.SetNumero(DataBuscadorFacturacionProveedor.FieldValues['numero']);
    Clase.BuscarNumero;
    Asignar;
  end;
end;

procedure TFrmFacturacionProveedor.CalcularMontos;
var
  Neto, Descuento, Impuesto: Currency;
begin
  Neto := 0;
  Descuento := 0;
  Impuesto := 0;
  with Data do
  begin
    DisableControls;
    First;
    while (not Eof) do
    begin
      Neto := Neto + (FieldValues['costo'] * FieldValues['cantidad']);
      Descuento := Descuento + FieldValues['monto_descuento'];
      Impuesto := Impuesto + FieldValues['impuesto'];
      Next;
    end;
    EnableControls;
  end;
  //Asignando Montos
  TxtMontoNeto.Value := Neto;
  //Determinando si hay un Descuento Manual
  if ((TxtMontoDescuento.Value > 0) and (StrToFloat(StringReplace(TxtPDescuento.Caption,'%','',[rfReplaceAll])) > 0) and (Descuento = 0)) then
  begin
    Descuento := TxtMontoDescuento.Value;
  end;
  TxtMontoDescuento.Value := Descuento;
  TxtSubTotal.Value := Neto - Descuento;
  TxtImpuesto.Value := Impuesto;
  TxtTotal.Value := (Neto - Descuento) + Impuesto;
end;

procedure TFrmFacturacionProveedor.CalcularMontosCabecera;
begin
  if (Clase.GetFormaLibre) then
  begin
    TxtSubTotal.Value := TxtMontoNeto.Value - TxtMontoDescuento.Value;
    TxtTotal.Value := TxtSubTotal.Value + TxtImpuesto.Value;
  end;
end;

procedure TFrmFacturacionProveedor.Cancelar;
begin
  if (Agregando) then
  begin
    Limpiar;
  end
  else
  begin
    Clase.SetNumero(TxtNumero.Text);
    Clase.BuscarNumero;
    Asignar;
  end;
end;

procedure TFrmFacturacionProveedor.CmbCodigoProveedorChange(Sender: TObject);
begin
  inherited;
  SB.Panels[0].Text := DataProveedor.FieldValues['razon_social'];
end;

procedure TFrmFacturacionProveedor.CmbCodigoProveedorExit(Sender: TObject);
begin
  if (Length(Trim(CmbCodigoProveedor.Text)) > 0) then
  begin
    if (Agregando) then
    begin
      Proveedor.SetCodigo(CmbCodigoProveedor.Value);
      Proveedor.BuscarCodigo;
      CmbCodigoZona.Value := Proveedor.GetCodigoZona;
      CmbCodigoEsquemaPago.Value := Proveedor.GetCodigoEsquemaPago;
    end;
  end;
end;

procedure TFrmFacturacionProveedor.Editar;
var
  sw: Boolean;
begin
  if (Length(Trim(TxtNumero.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end
  else
  begin
    //Se Valida Que La Factura Pueda Ser Anulada
    sw := True;
    EsquemaPago.SetCodigo(Clase.GetCodigoEsquemaPago);
    EsquemaPago.Buscar;
    if ((Clase.GetIdContadoPago = -1) and (EsquemaPago.GetCredito)) then //Puede Editar
    begin
      PagoPagado.SetIdPago(Clase.GetIdPago);
      if (PagoPagado.BuscarIdPago) then
      begin
        sw := False;
      end;
    end;
    if not (sw) then
    begin
      MessageBox(Self.Handle, 'La Factura Presenta Liquidaciones Asociadas y No Puede Ser Editada!!!',PChar(MsgTituloAdvertencia), MB_ICONEXCLAMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      Abort;
    end;
  end;
end;

procedure TFrmFacturacionProveedor.Eliminar;
var
  sw: Boolean;
begin
  //Se Valida Que La Factura Pueda Ser Anulada
  sw := True;
  EsquemaPago.SetCodigo(Clase.GetCodigoEsquemaPago);
  EsquemaPago.Buscar;
  if ((Clase.GetIdContadoPago = -1) and (EsquemaPago.GetCredito)) then //Puede Anularse
  begin
    PagoPagado.SetIdPago(Clase.GetIdPago);
    if (PagoPagado.BuscarIdPago) then
    begin
      sw := False;
    end;
  end;
  if (sw) then
  begin
    //Se Busca La Factura!
    Clase.SetNumero(TxtNumero.Text);
    Clase.BuscarNumero;
    //Se Eliminan Los Detalles
    DetFacturacionProveedor.EliminarIdFacturacionProveedor(Clase.GetIdFacturacionProveedor);
    //Se Registra El Documento Anulado y Eliminan Registros del Documento Legal
    DocumentoLegal.SetDesde('FP');
    DocumentoLegal.SetNumeroDocumento(Clase.GetNumero);
    if (DocumentoLegal.Buscar) then
    begin
      //Registrando El Documento Anulado
      with DocumentoAnulado do
      begin
        SetDesde('FP');
        SetNumeroDocumento(Clase.GetNumero);
        SetSerie(DocumentoLegal.GetSerie);
        SetNumeroControl(DocumentoLegal.GetNumeroControl);
        SetFechaDocumento(Clase.GetFechaIngreso);
        SetCodigoCliente('');
        SetCodigoProveedor(Clase.GetCodigoProveedor);
        SetCodigoZona(Clase.GetCodigoZona);
        SetSubTotal(Clase.GetSubTotal);
        SetImpuesto(Clase.GetImpuesto);
        SetTotal(Clase.GetTotal);
        SetPIva(Clase.GetPIva);
        SetLoginUsuario(P.User.GetLoginUsuario);
        SetFecha(Date);
        SetHora(Time);
        Insertar;
      end;
      //Borrando Documento Legal
      DocumentoLegal.Eliminar;
    end;
    Clase.Eliminar;
    MessageBox(Self.Handle, 'La Factura Se Ha Eliminado Con Exito!!!',PChar(MsgTituloInformacion), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Refrescar;
    Limpiar;
  end
  else
  begin
    MessageBox(Self.Handle, 'La Factura Presenta Liquidaciones Asociadas y No Puede Ser Eliminada!!!',PChar(MsgTituloAdvertencia), MB_ICONEXCLAMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end;
end;

procedure TFrmFacturacionProveedor.FormCreate(Sender: TObject);
begin
  inherited;
  ProcAgregar := Agregar;
  ProcEditar := Editar;
  ProcEliminar := Eliminar;
  ProcGuardar := Guardar;
  ProcCancelar := Cancelar;
  ProcRefrescar := Refrescar;
  ProcImprimir := Imprimir;
  ProcBuscar := Buscar;
  FrmActividadSophie.Show;
  FrmActividadSophie.Actividad('Cargando N�cleo...');
  Clase := TFacturacionProveedor.Create;
  //Inicializando DataSets Secundarios
  BuscadorFacturacionProveedor := TBusquedaFacturacionProveedor.Create;
  Proveedor := TProveedor.Create;
  Estacion := TEstacion.Create;
  Zona := TZona.Create;
  EsquemaPago := TEsquemaPago.Create;
  Moneda := TMoneda.Create;
  Producto := TProducto.Create;
  Reporte := TReporte.Create;
  Parametro := TParametro.Create;
  DocumentoLegal := TDocumentoLegal.Create;
  DocumentoAnulado := TDocumentoAnulado.Create;
  DetFacturacionProveedor := TDetFacturacionProveedor.Create;
  MovimientoInventario := TDetMovimientoInventario.Create;
  CuentaxPagar := TCuentaxPagar.Create;
  PagoPagado := TPagoPagado.Create;
  DetalleContadoPago := TDetalleContadoPago.Create;
  FrmActividadSophie.Actividad('Cargando Proveedores...');
  DataProveedor := Proveedor.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Estaciones...');
  DataEstacion := Estacion.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Zonas...');
  DataZona := Zona.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Esquemas de Pago...');
  DataEsquemaPago := EsquemaPago.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Monedas...');
  DataMoneda := Moneda.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Resto del N�cleo...');
  DataBuscadorFacturacionProveedor := BuscadorFacturacionProveedor.ObtenerLista;
  DataProveedor.First;
  DataEstacion.First;
  DataZona.First;
  DataEsquemaPago.First;
  DataMoneda.First;
  DataBuscadorFacturacionProveedor.First;
  DSProveedor.DataSet := DataProveedor;
  DSEstacion.DataSet := DataEstacion;
  DSZona.DataSet := DataZona;
  DSEsquemaPago.DataSet := DataEsquemaPago;
  DSMoneda.DataSet := DataMoneda;
  ConfigurarRxDBLookupCombo(CmbCodigoProveedor,DSProveedor,'codigo','codigo;razon_social');
  ConfigurarRxDBLookupCombo(CmbCodigoEstacion,DSEstacion,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoZona,DSZona,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoEsquemaPago,DSEsquemaPago,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoMoneda,DSMoneda,'codigo','codigo;descripcion');
  DS.DataSet := Data;
  GridDetalle.DataSource := DS;
  FrmActividadSophie.Close;
  //Buscar;
end;

procedure TFrmFacturacionProveedor.Guardar;
var
  SwDocumentoLegal: Boolean;
begin
  if (Clase.GetFormaLibre) then
  begin
    CalcularMontosCabecera;
    Data.EmptyDataSet;
  end
  else
  begin
    CalcularMontos;
  end;
  Obtener;
  //Caso de Esquema de Pago de Contado
  EsquemaPago.SetCodigo(Clase.GetCodigoEsquemaPago);
  EsquemaPago.Buscar;
  if (not EsquemaPago.GetCredito) then
  begin
    FrmDetalleContadoPago := TFrmDetalleContadoPago.Crear(Self,Agregando,Editando,Clase.GetIdContadoPago,Clase.GetTotal,'FP');
    FrmDetalleContadoPago.ShowModal;
    if (not FrmDetalleContadoPago.Aceptado) then
    begin
      FreeAndNil(FrmDetalleContadoPago);
      Abort;
    end;
    Clase.SetIdContadoPago(FrmDetalleContadoPago.GetIdContadoPago);
    FreeAndNil(FrmDetalleContadoPago);
  end
  else //Caso de Esquema de Pago a Credito
  begin
    if (EsquemaPago.GetInicial) then //Caso de Creditos con Inicial
    begin
      FrmDetalleContadoPago := TFrmDetalleContadoPago.Crear(Self,Agregando,Editando,Clase.GetIdContadoPago,(Clase.GetTotal * (EsquemaPago.GetPInicial / 100)) + Clase.GetImpuesto,'FP');
      FrmDetalleContadoPago.ShowModal;
      if (not FrmDetalleContadoPago.Aceptado) then
      begin
        FreeAndNil(FrmDetalleContadoPago);
        Abort;
      end;
      Clase.SetIdContadoPago(FrmDetalleContadoPago.GetIdContadoPago);
      FreeAndNil(FrmDetalleContadoPago);
    end;
    //Si Proviene de un Esquema de Pago de Contado
    if (Clase.GetIdContadoPago > 0) then
    begin
      //Se Eliminan Los Registros de Detalle Contado Pago
      DetalleContadoPago.EliminarIdContadoPago(Clase.GetIdContadoPago);
      //Se Eliminan Los Registros de Pago Pagado
      PagoPagado.EliminarIdPago(Clase.GetIdPago);
      Clase.SetIdContadoPago(-1);
    end;
  end;
  if (Agregando) then
  begin
    //Agregando...
    //Guardando Encabezado
    Clase.Insertar;
    TxtNumero.Text := Clase.GetNumero;
    //Solicitando Datos Legales
    FrmDocumentoLegal := TFrmDocumentoLegal.CreateInsert(Self,'FP',Clase.GetNumero,'','');
    FrmDocumentoLegal.ShowModal;
    if FrmDocumentoLegal.Aceptado then
    begin //Se Guardan Los Datos Legales
      DocumentoLegal.SetDesde(FrmDocumentoLegal.Desde);
      DocumentoLegal.SetNumeroDocumento(Clase.GetNumero);
      DocumentoLegal.SetSerie(FrmDocumentoLegal.Serie);
      DocumentoLegal.SetNumeroControl(FrmDocumentoLegal.NumeroControl);
      DocumentoLegal.SetLoginUsuario(P.User.GetLoginUsuario);
      DocumentoLegal.SetFecha(Date);
      DocumentoLegal.SetHora(Time);
      DocumentoLegal.Insertar;
    end;
    FreeAndNil(FrmDocumentoLegal);
    Application.ProcessMessages;
    //Guardando Detalles
    GuardarDetalles;
    //Guardando Pago Pagado
    GuardarPagoPagado;
    //Guardando Cuenta x Pagar
    GuardarCuentaxPagar;
    //Guardando Movimiento de Inventario
    GuardarMovimientoInventario;
    //Actualizando Costo de Producto
    ActualizarCostoProducto;
    MessageBox(Self.Handle, 'La Factura Ha Sido Agregada Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    //Se Imprime la Factura
    MessageBox(Self.Handle, 'Presione "Aceptar" Para Proceder A Imprimir La Factura!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    with Reporte do
    begin
      AgregarParametro('NumeroFactura',Clase.GetNumero);
      AgregarParametro('MontoLetra',NumeroALetras(Clase.GetTotal));
      if (Clase.GetFormaLibre) then
      begin
        ImprimeReporte('FacturacionProveedorFormaLibre',trDocumento);
      end
      else
      begin
        ImprimeReporte('FacturacionProveedor',trDocumento);
      end;
    end;
  end
  else
  begin
    //Editando...
    Clase.Modificar;
    //Solicitando Datos Legales
    DocumentoLegal.SetDesde('FP');
    DocumentoLegal.SetNumeroDocumento(Clase.GetNumero);
    SwDocumentoLegal := DocumentoLegal.Buscar;
    if (SwDocumentoLegal) then
    begin
      FrmDocumentoLegal := TFrmDocumentoLegal.CreateEdit(Self,'FP',Clase.GetNumero,DocumentoLegal.GetSerie,DocumentoLegal.GetNumeroControl);
    end
    else
    begin
      FrmDocumentoLegal := TFrmDocumentoLegal.CreateInsert(Self,'FP',Clase.GetNumero,'','');
    end;
    FrmDocumentoLegal.ShowModal;
    if FrmDocumentoLegal.Aceptado then
    begin //Se Guardan Los Datos Legales
      DocumentoLegal.SetSerie(FrmDocumentoLegal.Serie);
      DocumentoLegal.SetNumeroControl(FrmDocumentoLegal.NumeroControl);
      DocumentoLegal.SetLoginUsuario(P.User.GetLoginUsuario);
      DocumentoLegal.SetFecha(Date);
      DocumentoLegal.SetHora(Time);
      if (SwDocumentoLegal) then
      begin
        DocumentoLegal.Modificar;
      end
      else
      begin
        DocumentoLegal.Insertar;
      end;
    end;
    FreeAndNil(FrmDocumentoLegal);
    //Actualizando Detalles (Eliminando Anteriores y Cargando los Nuevos)
    DetFacturacionProveedor.EliminarIdFacturacionProveedor(Clase.GetIdFacturacionProveedor);
    //Guardando Detalles
    GuardarDetalles;
    //Guardando Pago Pagado
    GuardarPagoPagado;
    //Guardando Cuenta x Pagar
    GuardarCuentaxPagar;
    //Guardando Movimiento de Inventario
    GuardarMovimientoInventario;
    MessageBox(Self.Handle, 'La Factura Ha Sido Editada Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end;
end;

procedure TFrmFacturacionProveedor.GuardarPagoPagado;
begin
  //Caso de Esquema de Pago de Contado
  EsquemaPago.SetCodigo(Clase.GetCodigoEsquemaPago);
  EsquemaPago.Buscar;
  if (not EsquemaPago.GetCredito) then
  begin
    if (Editando) then
    begin
      PagoPagado.EliminarIdPago(Clase.GetIdPago);
    end;
    with PagoPagado do //Se Inserta el Pago Pagado
    begin
      SetDesde('FP');
      SetNumeroDocumento(Clase.GetNumero);
      SetCodigoMoneda(Clase.GetCodigoMoneda);
      SetCodigoProveedor(Clase.GetCodigoProveedor);
      SetCodigoZona(Clase.GetCodigoZona);
      SetFechaIngreso(Clase.GetFechaIngreso);
      SetFechaVencIngreso(Clase.GetFechaIngreso);
      SetFechaRecepcion(Clase.GetFechaRecepcion);
      SetFechaVencRecepcion(Clase.GetFechaRecepcion);
      SetFechaContable(Clase.GetFechaContable);
      SetFechaPago(Clase.GetFechaIngreso);
      SetFechaLibros(Clase.GetFechaLibros);
      SetNumeroCuota(-1);
      SetTotalCuotas(-1);
      SetOrdenPago(1);
      SetMontoNeto(Clase.GetMontoNeto);
      SetImpuesto(Clase.GetImpuesto);
      SetTotal(Clase.GetTotal);
      SetPIva(Clase.GetPIva);
      SetIdPago(Clase.GetIdPago);
      SetIdContadoPago(Clase.GetIdContadoPago);
      Insertar;
    end;
  end
  else //Caso de Esquema de Pago a Credito
  begin
    if (EsquemaPago.GetInicial) then //Caso de Creditos con Inicial
    begin
      if (Editando) then
      begin
        PagoPagado.EliminarIdPago(Clase.GetIdPago);
      end;
      with PagoPagado do //Se Inserta el Pago Pagado
      begin
        SetDesde('FP');
        SetNumeroDocumento(Clase.GetNumero);
        SetCodigoMoneda(Clase.GetCodigoMoneda);
        SetCodigoProveedor(Clase.GetCodigoProveedor);
        SetCodigoZona(Clase.GetCodigoZona);
        SetFechaIngreso(Clase.GetFechaIngreso);
        SetFechaVencIngreso(Clase.GetFechaIngreso);
        SetFechaRecepcion(Clase.GetFechaRecepcion);
        SetFechaVencRecepcion(Clase.GetFechaRecepcion);
        SetFechaContable(Clase.GetFechaContable);
        SetFechaPago(Clase.GetFechaIngreso);
        SetFechaLibros(Clase.GetFechaLibros);
        SetNumeroCuota(0);
        if ((EsquemaPago.GetCuotas) and (EsquemaPago.GetCantidadCuotas > 0)) then
        begin
          SetTotalCuotas(EsquemaPago.GetCantidadCuotas);
        end
        else
        begin
          if ((EsquemaPago.GetDias) and (EsquemaPago.GetCantidadDias > 0)) then
          begin
            SetTotalCuotas(1);
          end
          else
          begin
            SetTotalCuotas(-1); (* NO SE CONFIGURO CORRECTAMENTE EL ESQUEMA DE PAGO DE CONTADO*)
          end;
        end;
        SetOrdenPago(1);
        SetMontoNeto(Clase.GetTotal * (EsquemaPago.GetPInicial / 100));
        SetImpuesto(0);
        SetTotal(Clase.GetMontoNeto);
        SetPIva(Clase.GetPIva);
        SetIdPago(Clase.GetIdPago);
        SetIdContadoPago(Clase.GetIdContadoPago);
        Insertar;
      end;
    end;
  end;
end;

procedure TFrmFacturacionProveedor.GuardarCuentaxPagar;
var
  FechaIngreso, FechaRecepcion: TDate;
  MontoCuota: Currency;
  MontoTotal: Currency;
  MontoBase: Currency;
  MontoInicial: Currency;
  Impuesto: Currency;
  i: Integer;
begin
  if (Editando) then
  begin
    CuentaxPagar.EliminarIdPago(Clase.GetIdPago);
  end;
  EsquemaPago.SetCodigo(Clase.GetCodigoEsquemaPago);
  EsquemaPago.Buscar;
  if (EsquemaPago.GetCredito) then
  begin
    if (EsquemaPago.GetInicial) then //Credito con Inicial
    begin
      if (EsquemaPago.GetCuotas) then //Credito con  Inicial + Cuotas
      begin
        with CuentaxPagar do //Se Inserta la CuentaxPagar
        begin
          FechaIngreso := Clase.GetFechaIngreso;
          FechaRecepcion := Clase.GetFechaRecepcion;
          //MONTO INICIAL = (PORCION INICIAL = % SUB TOTAL) + IMPUESTO
          MontoInicial := (Clase.GetSubTotal * (EsquemaPago.GetPInicial / 100)) + Clase.GetImpuesto;
          MontoCuota := (Clase.GetTotal - MontoInicial) / EsquemaPago.GetCantidadCuotas;
          MontoBase := MontoCuota;
          Impuesto := 0;
          for i := 1 to EsquemaPago.GetCantidadCuotas do
          begin
            SetDesde('FP');
            SetNumeroDocumento(Clase.GetNumero);
            SetCodigoMoneda(Clase.GetCodigoMoneda);
            SetCodigoProveedor(Clase.GetCodigoProveedor);
            SetCodigoZona(Clase.GetCodigoZona);
            SetFechaIngreso(Clase.GetFechaIngreso);
            SetFechaVencIngreso(FechaIngreso + EsquemaPago.GetDiasCuota);
            SetFechaRecepcion(Clase.GetFechaRecepcion);
            SetFechaVencRecepcion(FechaRecepcion + EsquemaPago.GetDiasCuota);
            SetFechaContable(Clase.GetFechaContable);
            SetFechaPago(Clase.GetFechaIngreso);
            SetFechaLibros(Clase.GetFechaLibros);
            SetNumeroCuota(i);
            SetTotalCuotas(EsquemaPago.GetCantidadCuotas);
            SetOrdenPago(1);
            SetMontoNeto(MontoBase);
            SetImpuesto(Impuesto);
            SetTotal(MontoCuota);
            SetPIva(Clase.GetPIva);
            SetIdPago(Clase.GetIdPago);
            SetIdContadoPago(Clase.GetIdContadoPago);
            Insertar;
            FechaIngreso := FechaIngreso + EsquemaPago.GetDiasCuota;
            FechaRecepcion := FechaRecepcion + EsquemaPago.GetDiasCuota;
          end;
        end;
      end
      else //Credito con Inicial + Dias
      begin
        MontoInicial := (Clase.GetSubTotal * (EsquemaPago.GetPInicial / 100)) + Clase.GetImpuesto;
        MontoTotal := Clase.GetTotal - MontoInicial;
        MontoBase := MontoTotal;
        Impuesto := 0;
        with CuentaxPagar do //Se Inserta la CuentaxPagar
        begin
          SetDesde('FP');
          SetNumeroDocumento(Clase.GetNumero);
          SetCodigoMoneda(Clase.GetCodigoMoneda);
          SetCodigoProveedor(Clase.GetCodigoProveedor);
          SetCodigoZona(Clase.GetCodigoZona);
          SetFechaIngreso(Clase.GetFechaIngreso);
          SetFechaVencIngreso(Clase.GetFechaIngreso + EsquemaPago.GetCantidadDias);
          SetFechaRecepcion(Clase.GetFechaRecepcion);
          SetFechaVencRecepcion(Clase.GetFechaRecepcion + EsquemaPago.GetCantidadDias);
          SetFechaContable(Clase.GetFechaContable);
          SetFechaPago(Clase.GetFechaIngreso);
          SetFechaLibros(Clase.GetFechaLibros);
          SetNumeroCuota(-1);
          SetTotalCuotas(-1);
          SetOrdenPago(1);
          SetMontoNeto(MontoBase);
          SetImpuesto(Impuesto);
          SetTotal(MontoTotal);
          SetPIva(Clase.GetPIva);
          SetIdPago(Clase.GetIdPago);
          SetIdContadoPago(Clase.GetIdContadoPago);
          Insertar;
        end;
      end;
    end
    else
    begin
      if (EsquemaPago.GetCuotas) then //Credito con Cuotas
      begin
        with CuentaxPagar do //Se Inserta la CuentaxPagar
        begin
          FechaIngreso := Clase.GetFechaIngreso;
          FechaRecepcion := Clase.GetFechaRecepcion;
          MontoCuota := Clase.GetTotal / EsquemaPago.GetCantidadCuotas;
          MontoBase := Clase.GetSubTotal / EsquemaPago.GetCantidadCuotas;
          Impuesto := Clase.GetImpuesto / EsquemaPago.GetCantidadCuotas;
          for i := 1 to EsquemaPago.GetCantidadCuotas do //Agregando Cuotas Fijas
          begin
            SetDesde('FP');
            SetNumeroDocumento(Clase.GetNumero);
            SetCodigoMoneda(Clase.GetCodigoMoneda);
            SetCodigoProveedor(Clase.GetCodigoProveedor);
            SetCodigoZona(Clase.GetCodigoZona);
            SetFechaIngreso(Clase.GetFechaIngreso);
            SetFechaVencIngreso(FechaIngreso + EsquemaPago.GetDiasCuota);
            SetFechaRecepcion(Clase.GetFechaRecepcion);
            SetFechaVencRecepcion(FechaRecepcion + EsquemaPago.GetDiasCuota);
            SetFechaContable(Clase.GetFechaContable);
            SetFechaPago(Clase.GetFechaIngreso);
            SetFechaLibros(Clase.GetFechaLibros);
            SetNumeroCuota(i);
            SetTotalCuotas(EsquemaPago.GetCantidadCuotas);
            SetOrdenPago(1);
            SetMontoNeto(MontoBase);
            SetImpuesto(Impuesto);
            SetTotal(MontoCuota);
            SetPIva(Clase.GetPIva);
            SetIdPago(Clase.GetIdPago);
            SetIdContadoPago(Clase.GetIdContadoPago);
            Insertar;
            FechaIngreso := FechaIngreso + EsquemaPago.GetDiasCuota;
            FechaRecepcion := FechaRecepcion + EsquemaPago.GetDiasCuota;
          end;
        end;
      end
      else //Credito por Dias
      begin
        with CuentaxPagar do //Se Inserta la CuentaxPagar
        begin
          SetDesde('FP');
          SetNumeroDocumento(Clase.GetNumero);
          SetCodigoMoneda(Clase.GetCodigoMoneda);
          SetCodigoProveedor(Clase.GetCodigoProveedor);
          SetCodigoZona(Clase.GetCodigoZona);
          SetFechaIngreso(Clase.GetFechaIngreso);
          SetFechaVencIngreso(Clase.GetFechaIngreso + EsquemaPago.GetCantidadDias);
          SetFechaRecepcion(Clase.GetFechaRecepcion);
          SetFechaVencRecepcion(Clase.GetFechaRecepcion + EsquemaPago.GetCantidadDias);
          SetFechaContable(Clase.GetFechaContable);
          SetFechaPago(Clase.GetFechaIngreso);
          SetFechaLibros(Clase.GetFechaLibros);
          SetNumeroCuota(-1);
          SetTotalCuotas(-1);
          SetOrdenPago(1);
          SetMontoNeto(Clase.GetMontoNeto);
          SetImpuesto(Clase.GetImpuesto);
          SetTotal(Clase.GetTotal);
          SetPIva(Clase.GetPIva);
          SetIdPago(Clase.GetIdPago);
          SetIdContadoPago(Clase.GetIdContadoPago);
          Insertar;
        end;
      end;
    end;
  end;
end;

procedure TFrmFacturacionProveedor.GuardarDetalles;
begin
  with Data do
  begin
    DisableControls;
    First;
    while (not Eof) do
    begin
      with DetFacturacionProveedor do
      begin
        SetCodigoProducto(FieldValues['codigo_producto']);
        SetReferencia(FieldValues['referencia']);
        SetDescripcion(FieldValues['descripcion']);
        SetCantidad(FieldValues['cantidad']);
        SetCantidadPendiente(FieldValues['cantidad_pendiente']);
        SetCosto(FieldValues['costo']);
        SetPDescuento(FieldValues['p_descuento']);
        SetMontoDescuento(FieldValues['monto_descuento']);
        SetImpuesto(FieldValues['impuesto']);
        SetPIva(FieldValues['p_iva']);
        SetTotal(FieldValues['total']);
        SetIdFacturacionProveedor(Clase.GetIdFacturacionProveedor);
        Insertar;
      end;
      Next;
    end;
    EnableControls;
  end;
end;

procedure TFrmFacturacionProveedor.GuardarMovimientoInventario;
begin
  if (Editando) then
  begin
    MovimientoInventario.EliminarIdMovimientoInventario(Clase.GetIdMovimientoInventario);
  end;
  with Data do
  begin
    DisableControls;
    First;
    while (not Eof) do
    begin
      Producto.SetCodigo(FieldValues['codigo_producto']);
      Producto.BuscarCodigo;
      if (Producto.GetInventario) then
      begin
        with MovimientoInventario do
        begin
          SetDesde('FP');
          SetNumeroDocumento(Clase.GetNumero);
          SetCodigoProducto(Producto.GetCodigo);
          SetCantidad(FieldValues['cantidad']);
          SetEstacionSalida('');
          SetEstacionEntrada(Clase.GetCodigoEstacion);
          SetCodigoZona(Clase.GetCodigoZona);
          SetPrecio(Producto.GetPrecio);
          SetPDescuento(FieldValues['p_descuento']);
          SetMontoDescuento(FieldValues['monto_descuento']);
          SetImpuesto(FieldValues['impuesto']);
          SetPIva(FieldValues['p_iva']);
          SetTotal(FieldValues['total']);
          SetCosto(FieldValues['costo']);
          SetUsCosto(Producto.GetUsCostoActual);
          SetCostoFob(Producto.GetCostoFob);
          SetFecha(Clase.GetFechaIngreso);
          SetCodigoCliente('');
          SetCodigoProveedor(Clase.GetCodigoProveedor);
          SetIdMovimientoInventario(Clase.GetIdMovimientoInventario);
          Insertar;
        end;
      end;
      Next;
    end;
    EnableControls;
  end;
end;

procedure TFrmFacturacionProveedor.Imprimir;
begin
  if (Length(Trim(TxtNumero.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
  with Reporte do
  begin
    AgregarParametro('NumeroFactura',Clase.GetNumero);
    AgregarParametro('MontoLetra',NumeroALetras(Clase.GetTotal));
    if (Clase.GetFormaLibre) then
    begin
      EmiteReporte('FacturacionProveedorFormaLibre',trDocumento);
    end
    else
    begin
      EmiteReporte('FacturacionProveedor',trDocumento);
    end;
  end;
end;

procedure TFrmFacturacionProveedor.LblImpuestoDblClick(Sender: TObject);
var
  Aux: Currency;
begin
  if (Clase.GetFormaLibre) then
  begin
    Aux := 0;
    Solicitar(Self,'Impuesto',TxtImpuesto.Value,Aux);
    if (Aux >= 0) then
    begin
      TxtImpuesto.Value := Aux;
      CalcularMontosCabecera;
    end;
  end
  else
  begin
    MessageBox(Self.Handle, 'La Factura No Es de Forma "Concepto Libre"', PChar(MsgTituloError), MB_ICONSTOP or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end;
end;

procedure TFrmFacturacionProveedor.LblMontoDescuentoDblClick(Sender: TObject);
var
  Aux: Currency;
begin
  Aux := 0;
  Solicitar(Self,'Monto Descuento',TxtMontoDescuento.Value,Aux);
  if (Aux >= 0) then
  begin
    TxtMontoDescuento.Value := Aux;
    TxtPDescuento.Caption := FormatFloat('#,##0.0000%',Aux * 100 / TxtMontoNeto.Value);
    if (Clase.GetFormaLibre) then
    begin
      CalcularMontosCabecera;
    end
    else
    begin
      CalcularMontos;
    end;
  end;
end;

procedure TFrmFacturacionProveedor.LblMontoNetoDblClick(Sender: TObject);
begin
  inherited;
  if (Data.IsEmpty) then
  begin
    if (MessageBox(Self.Handle, '�Desea Activar Forma Concepto Libre?', PChar(MsgTituloInformacion), MB_ICONQUESTION or MB_YESNO or MB_TOPMOST or MB_TASKMODAL) = mrYes) then
    begin
      Clase.SetFormaLibre(True);
      TabDetalle.TabVisible := False;
      PageDetalles.ActivePage := TabConcepto;
      TxtMontoNeto.ReadOnly := False;
    end
    else
    begin
      Clase.SetFormaLibre(False);
      TabDetalle.TabVisible := True;
      PageDetalles.ActivePage := TabDetalle;
      TxtMontoNeto.ReadOnly := True;
      TxtMontoNeto.Value := 0;
      TxtMontoDescuento.Value := 0;
      TxtPDescuento.Caption := FormatFloat('#,##0.0000%',0);
      TxtSubTotal.Value := 0;
      TxtImpuesto.Value := 0;
      TxtTotal.Value := 0;
    end;
  end;
end;

procedure TFrmFacturacionProveedor.Limpiar;
begin
  TxtNumero.Text := '';
  //NumeroPedido
  CmbCodigoProveedor.KeyValue := '';
  CmbCodigoEstacion.KeyValue := '';
  CmbCodigoZona.KeyValue := '';
  CmbCodigoEsquemaPago.KeyValue := '';
  CmbCodigoMoneda.KeyValue := '';
  DatFechaIngreso.Date := Date;
  DatFechaRecepcion.Date := Date;
  DatFechaContable.Date := Date;
  DatFechaLibros.Date := Date;
  TxtConcepto.Text := '';
  TxtObservaciones.Text := '';
  TxtMontoNeto.Value := 0;
  TxtMontoDescuento.Value := 0;
  TxtSubTotal.Value := 0;
  TxtImpuesto.Value := 0;
  TxtTotal.Value := 0;
  TxtPDescuento.Caption := FormatFloat('###0.0000%',0);
  TxtPIVA.Caption := FormatFloat('###0.0000%',0);
  //Status Bar
  SB.Panels[0].Text := '';
  Data.EmptyDataSet;
  //Formato Concepto Libre
  TxtMontoNeto.ReadOnly := True;
  TabDetalle.TabVisible := True;
  PageDetalles.ActivePage := TabDetalle;
  Clase.SetFormaLibre(False);
end;

procedure TFrmFacturacionProveedor.Mover;
begin

end;

procedure TFrmFacturacionProveedor.Obtener;
begin
  with Clase do
  begin
    SetNumero(TxtNumero.Text);
    SetNumeroPedido('Indefinido');
    SetCodigoProveedor(CmbCodigoProveedor.KeyValue);
    SetCodigoEstacion(CmbCodigoEstacion.KeyValue);
    SetCodigoZona(CmbCodigoZona.KeyValue);
    SetCodigoEsquemaPago(CmbCodigoEsquemaPago.KeyValue);
    SetCodigoMoneda(CmbCodigoMoneda.KeyValue);
    SetFechaIngreso(DatFechaIngreso.Date);
    SetFechaRecepcion(DatFechaRecepcion.Date);
    SetFechaContable(DatFechaContable.Date);
    SetFechaLibros(DatFechaLibros.Date);
    SetConcepto(TxtConcepto.Text);
    SetObservaciones(TxtObservaciones.Text);
    SetMontoNeto(TxtMontoNeto.Value);
    SetMontoDescuento(TxtMontoDescuento.Value);
    SetSubTotal(TxtSubTotal.Value);
    SetImpuesto(TxtImpuesto.Value);
    SetTotal(TxtTotal.Value);
    SetPDescuento(FormatearFloat(StringReplace(TxtPDescuento.Caption,'%','',[rfReplaceAll])));
    SetPIva(FormatearFloat(StringReplace(TxtPIVA.Caption,'%','',[rfReplaceAll])));
    SetTasaDolar(0); //***//
    SetLoginUsuario(P.User.GetLoginUsuario);
    if (Agregando) then
    begin
      SetTiempoIngreso(Now);
      SetIdFacturacionProveedor(-1);
      SetIdPago(-1);
      SetIdMovimientoInventario(-1);
      SetIdContadoPago(-1);
    end;
  end;
end;

procedure TFrmFacturacionProveedor.Refrescar;
begin
  //Se Actualiza El DataBuscador
  DataBuscadorFacturacionProveedor := BuscadorFacturacionProveedor.ObtenerLista;
  //Se Actualizan Los demas Componentes
  DataProveedor := Proveedor.ObtenerCombo;
  DataEstacion := Estacion.ObtenerCombo;
  DataZona := Zona.ObtenerCombo;
  DataEsquemaPago := EsquemaPago.ObtenerCombo;
  DataMoneda := Moneda.ObtenerCombo;
  DataProveedor.First;
  DataEstacion.First;
  DataZona.First;
  DataEsquemaPago.First;
  DataMoneda.First;
  DataBuscadorFacturacionProveedor.First;
  DSProveedor.DataSet := DataProveedor;
  DSEstacion.DataSet := DataEstacion;
  DSZona.DataSet := DataZona;
  DSEsquemaPago.DataSet := DataEsquemaPago;
  DSMoneda.DataSet := DataMoneda;
end;

procedure TFrmFacturacionProveedor.TBtnEliminarClick(Sender: TObject);
begin
  if (Length(Trim(TxtNumero.Text)) > 0) then
  begin
    inherited;
  end
  else
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
end;

procedure TFrmFacturacionProveedor.TxtMontoNetoExit(Sender: TObject);
begin
  CalcularMontosCabecera;
end;

procedure TFrmFacturacionProveedor.TxtPDescuentoDblClick(Sender: TObject);
var
  Aux: Currency;
begin
  Aux := 0;
  Solicitar(Self,'(%) Descuento',StrToFloat(StringReplace(TxtPDescuento.Caption,'%','',[rfReplaceAll])),Aux);
  if (Aux >= 0) then
  begin
    TxtPDescuento.Caption := FormatFloat('#,##0.0000%',Aux);
    TxtMontoDescuento.Value := Aux * TxtMontoNeto.Value / 100;
    if (Clase.GetFormaLibre) then
    begin
      CalcularMontosCabecera;
    end
    else
    begin
      CalcularMontos;
    end;
  end;
end;

procedure TFrmFacturacionProveedor.TxtPIVADblClick(Sender: TObject);
var
  Aux: Currency;
  Exentos: Boolean;
  BMark: TBookmark;
begin
  if (Clase.GetFormaLibre) then //Forma Concepto Libre
  begin
    Aux := 0;
    Solicitar(Self,'(%) I.V.A.',StrToFloat(StringReplace(TxtPIVA.Caption,'%','',[rfReplaceAll])),Aux);
    if (Aux >= 0) then
    begin
      TxtPIVA.Caption := FormatFloat('#,##0.0000%',Aux);
      TxtImpuesto.Value := Aux * TxtMontoNeto.Value / 100;
      CalcularMontosCabecera;
    end;
  end
  else
  begin
    Aux := 0;
    Solicitar(Self,'(%) I.V.A.',StrToFloat(StringReplace(TxtPIVA.Caption,'%','',[rfReplaceAll])),Aux);
    if (Aux >= 0) then
    begin
      TxtPIVA.Caption := FormatFloat('#,##0.0000%',Aux);
      if (not Data.IsEmpty) then
      begin
        if (MessageBox(Self.Handle, '�Desea Actualizar El Porcentaje De Impuesto En Todos Los Renglones?', PChar(MsgTituloInformacion), MB_ICONQUESTION or MB_YESNO or MB_TOPMOST or MB_TASKMODAL) = mrYes) then
        begin
          if (MessageBox(Self.Handle, '�Incluir Renglones Exentos de I.V.A.?', PChar(MsgTituloInformacion), MB_ICONQUESTION or MB_YESNO or MB_TOPMOST or MB_TASKMODAL) = mrYes) then
          begin
            Exentos := True;
          end
          else
          begin
            Exentos := False;
          end;
          //Actualizando PIVA en Renglones
          with Data do
          begin
            DisableControls;
            First;
            while (not Eof) do
            begin
              if ((FieldValues['impuesto'] > 0) or (Exentos)) then
              begin
                //Utilizando Punteros de los Registros (Bookmarks)
                //Debido a una deficiencia en la edicion de ClientDataSets
                BMark := GetBookmark;
                Edit;
                FieldValues['p_iva'] := Aux;
                FieldValues['impuesto'] := (FieldValues['costo'] * FieldValues['cantidad']) / (FieldValues['p_iva'] / 100);
                Post;
                GotoBookmark(BMark);
              end;
              Next;
            end;
            EnableControls;
          end;
          MessageBox(Self.Handle, 'Renglones Actualizados Correctamente', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
        end;
      end;
      CalcularMontos;
    end;
  end;
end;

function TFrmFacturacionProveedor.Validar: Boolean;
var
  ListaErrores: TStringList;
  ClaseFP : TFacturacionProveedor;
begin
  ListaErrores := TStringList.Create;
  //Validando Numero Factura
  if (Length(Trim(TxtNumero.Text)) = 0) then
  begin
    ListaErrores.Add('El N�mero de Factura No Puede Estar En Blanco!!!');
  end;
  //Validando Proveedor
  if (Length(Trim(CmbCodigoProveedor.Text)) = 0) then
  begin
    ListaErrores.Add('El Proveedor No Puede Estar En Blanco!!!');
  end;
  //Validando Monto
  if (TxtTotal.Value <= 0) then
  begin
    ListaErrores.Add('El Total Debe Ser Positivo!!!');
  end;
  //Validando Fechas
  if ((DatFechaIngreso.Date > DatFechaRecepcion.Date) or (DatFechaIngreso.Date > DatFechaContable.Date) or (DatFechaIngreso.Date > DatFechaLibros.Date)) then
  begin
    ListaErrores.Add('La Fecha de Ingreso Debe Ser Menor a las Fechas de Recepcion, Contable y Libros Respectivamente!!!');
  end;
  //Validando Detalle o Forma Libre
  if (Clase.GetFormaLibre) then
  begin
    if (Length(Trim(TxtConcepto.Text)) = 0) then
    begin
      ListaErrores.Add('El Concepto No Puede Estar En Blanco!!!');
    end;
    if (TxtMontoNeto.Value <= 0) then
    begin
      ListaErrores.Add('El Monto Neto Debe Ser Positivo!!!');
    end;
    if (TxtMontoDescuento.Value < 0) then
    begin
      ListaErrores.Add('El Monto de Descuento No Debe Ser Negativo!!!');
    end;
    if (TxtImpuesto.Value < 0) then
    begin
      ListaErrores.Add('El Impuesto No Debe Ser Negativo!!!');
    end;
  end
  else
  begin
    if (Data.RecordCount = 0) then
    begin
      ListaErrores.Add('El Detalle De La Factura No Puede Estar Vacio!!!');
    end;
  end;
  //Validando Duplicidad de Factura
  ClaseFP := TFacturacionProveedor.Create;
  ClaseFP.SetNumero(TxtNumero.Text);
  ClaseFP.SetCodigoProveedor(CmbCodigoProveedor.Text);
  if (ClaseFP.BuscarFacturaProveedor) then
  begin
    if Agregando then
    begin
      ListaErrores.Add('El N�mero de Factura Ya Se Encuentra Asignado Para Este Proveedor!!!');
    end
    else
    begin
      if (Clase.GetIdFacturacionProveedor <> ClaseFP.GetIdFacturacionProveedor)  then
      begin
        ListaErrores.Add('El N�mero de Factura Ya Se Encuentra Asignado Para Este Proveedor!!!');
      end;
    end;
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

procedure TFrmFacturacionProveedor.DataAfterPost(DataSet: TDataSet);
begin
  inherited;
  CalcularMontos;
end;

procedure TFrmFacturacionProveedor.Detalle;
begin
  inherited;
  FrmAgregarDetalleFacturacionProveedor := TFrmAgregarDetalleFacturacionProveedor.CrearDetalle(Self,Data);
  FrmAgregarDetalleFacturacionProveedor.ShowModal;
  FreeAndNil(FrmAgregarDetalleFacturacionProveedor);
end;

end.
