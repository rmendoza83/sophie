unit ServicioTecnicoTecnico;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GenericoDetalle, rxCurrEdit, StdCtrls, Buttons, RxLookup, Mask,
  rxToolEdit, DBCtrls, DB, DBClient, ImgList, XPMan, ComCtrls, Grids, DBGrids,
  ExtCtrls, ToolWin, ClsServicioTecnicoTecnico,
  ClsServicioTecnicoComisionTecnicoTipoServicio,
  ClsServicioTecnicoDetalleComisionTecnicoTipoServicio, ClsZona, ClsParametro,
  GenericoBuscador, Utils;

type
  TFrmServicioTecnicoTecnico = class(TFrmGenericoDetalle)
    LblCodigo: TLabel;
    LblRif: TLabel;
    LblRazonSocial: TLabel;
    LblTelefonos: TLabel;
    LblNit: TLabel;
    LblDireccionFiscal: TLabel;
    LblFechaIngreso: TLabel;
    LblCodigoZona: TLabel;
    TxtDescripcionZona: TDBText;
    TxtEdad: TLabel;
    LblPComisionGlobal: TLabel;
    TxtCodigo: TEdit;
    CmbPrefijoRif: TComboBox;
    TxtRif: TEdit;
    TxtRazonSocial: TEdit;
    TxtTelefonos: TEdit;
    TxtNit: TEdit;
    TxtDireccionFiscal1: TEdit;
    TxtDireccionFiscal2: TEdit;
    DatFechaIngreso: TDateEdit;
    CmbCodigoZona: TRxDBLookupCombo;
    BtnBuscarZona: TBitBtn;
    TxtPComisionGlobal: TCurrencyEdit;
    DSZona: TDataSource;
    Datacodigo_tipo_servicio: TStringField;
    Datadescripcion: TStringField;
    Datap_comision: TFloatField;
    Dataobservaciones: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure BtnBuscarZonaClick(Sender: TObject);
    procedure TBtnEliminarClick(Sender: TObject);
  private
    { Private declarations }
    Clase: TServicioTecnicoTecnico;
    ComisionTecnicoTipoServicio: TServicioTecnicoComisionTecnicoTipoServicio;
    DetalleComisionTecnicoTipoServicio: TServicioTecnicoComisionTecnicoTipoServicio;
    Parametro: TParametro;
    Zona: TZona;
    DataBuscadorTecnico: TClientDataSet;
    DataZona: TClientDataSet;
    procedure Agregar;
    procedure Editar;
    procedure Eliminar;
    procedure Guardar;
    procedure Cancelar;
    procedure Refrescar;
    procedure Imprimir;
    procedure Buscar;
    procedure Limpiar;
    procedure Asignar;
    procedure Obtener;
    procedure Mover;
    procedure GuardarDetalles;
  protected
    { Protected declarations }
    function Validar: Boolean; override;
    procedure Detalle; override;
  public
    { Public declarations }
  end;

var
  FrmServicioTecnicoTecnico: TFrmServicioTecnicoTecnico;

implementation

{$R *.dfm}

procedure TFrmServicioTecnicoTecnico.Agregar;
begin
  //Verificando
  Limpiar;
  //Asignando Valores por Defecto
  CmbCodigoZona.Value := Parametro.ObtenerValor('codigo_zona');
  TxtCodigo.SetFocus;
end;

procedure TFrmServicioTecnicoTecnico.Asignar;
begin
  with Clase do
  begin
    TxtCodigo.Text := GetCodigo;
    CmbPrefijoRif.ItemIndex := CmbPrefijoRif.Items.IndexOf(GetPrefijoRif);
    TxtRif.Text := GetRif;
    TxtNit.Text := GetNit;
    TxtRazonSocial.Text := GetRazonSocial;
    TxtTelefonos.Text := GetTelefonos;
    DatFechaIngreso.Date := GetFechaIngreso;
    CmbCodigoZona.KeyValue := GetCodigoZona;
    TxtPComisionGlobal.Value := GetPComisionGlobal;
    TxtDireccionFiscal1.Text := GetDireccionFiscal1;
    TxtDireccionFiscal2.Text := GetDireccionFiscal2;
    DataZona.Locate('codigo',CmbCodigoZona.KeyValue,[loCaseInsensitive]);
    Data.CloneCursor(DetalleComisionTecnicoTipoServicio.ObtenerCodigoTecnico(GetCodigo),False,True);
  end;
end;

procedure TFrmServicioTecnicoTecnico.BtnBuscarZonaClick(Sender: TObject);
begin
  Buscador(Self,Zona,'codigo','Buscar Zona',nil,DataZona);
  CmbCodigoZona.KeyValue := DataZona.FieldValues['codigo'];
end;

procedure TFrmServicioTecnicoTecnico.Buscar;
begin
  if (Buscador(Self,Clase,'codigo','Buscar T�cnico',nil,DataBuscadorTecnico)) then
  begin
    Clase.SetCodigo(DataBuscadorTecnico.FieldValues['codigo']);
    Clase.Buscar;
    Asignar;
  end;
end;

procedure TFrmServicioTecnicoTecnico.Cancelar;
begin
  if (Agregando) then
  begin
    Limpiar;
  end
  else
  begin
    Clase.SetCodigo(TxtCodigo.Text);
    Clase.Buscar;
    Asignar;
  end;
end;

procedure TFrmServicioTecnicoTecnico.Detalle;
begin
  inherited;

end;

procedure TFrmServicioTecnicoTecnico.Editar;
begin
  if (Length(Trim(TxtCodigo.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
end;

procedure TFrmServicioTecnicoTecnico.Eliminar;
var
  sw: Boolean;
begin
  //Se Valida Que El Tecnico Puede Ser Eliminado
  sw := True;
  //Se Verifica que el Tecnico No Tenga Comisiones Pagadas o Servicios Asignados
  (*
  if (Clase.GetIdContadoCobranza = -1) then //Puede Anularse
  begin
    CobranzaCobrada.SetIdCobranza(Clase.GetIdCobranza);
    if (CobranzaCobrada.BuscarIdCobranza) then
    begin
      sw := False;
    end;
  end;
  *)
  if (sw) then
  begin
    //Se Busca El Tecnico!
    Clase.SetCodigo(TxtCodigo.Text);
    Clase.Buscar;
    //Se Eliminan Los Detalles de las Comisiones Asignadas
    ComisionTecnicoTipoServicio.EliminarCodigoTecnico(Clase.GetCodigo);
    Clase.Eliminar;
    MessageBox(Self.Handle, 'El T�cnico Se Ha Eliminado Con Exito!!!',PChar(MsgTituloInformacion), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Refrescar;
    Limpiar;
  end
  else
  begin
    MessageBox(Self.Handle, 'El T�cnico Presenta Servicios Asociados o Comisiones Pagadas!!! No Puede Ser Eliminado!!!',PChar(MsgTituloAdvertencia), MB_ICONEXCLAMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end;
end;

procedure TFrmServicioTecnicoTecnico.FormCreate(Sender: TObject);
begin
  inherited;
  //Activando Panel Maestro
  PanelMaestro.Enabled := True;
  PageDetalles.Enabled := True;
  ProcAgregar := Agregar;
  ProcEditar := Editar;
  ProcEliminar := Eliminar;
  ProcGuardar := Guardar;
  ProcCancelar := Cancelar;
  ProcRefrescar := Refrescar;
  ProcImprimir := Imprimir;
  ProcBuscar := Buscar;
  Clase := TServicioTecnicoTecnico.Create;
  //Inicializando DataSets Secundarios
  ComisionTecnicoTipoServicio := TServicioTecnicoComisionTecnicoTipoServicio.Create;
  DetalleComisionTecnicoTipoServicio := TServicioTecnicoComisionTecnicoTipoServicio.Create;
  Zona := TZona.Create;
  Parametro := TParametro.Create;
  DataBuscadorTecnico := Clase.ObtenerLista;
  DataZona := Zona.ObtenerCombo;
  DataBuscadorTecnico.First;
  DataZona.First;
  DSZona.DataSet := DataZona;
  TxtDescripcionZona.DataSource := DSZona;
  TxtDescripcionZona.DataField := 'descripcion';
  ConfigurarRxDBLookupCombo(CmbCodigoZona,DSZona,'codigo','codigo;descripcion');
  DS.DataSet := Data;
  GridDetalle.DataSource := DS;
  //Buscar;
end;

procedure TFrmServicioTecnicoTecnico.Guardar;
begin
  Obtener;
  if (Agregando) then
  begin
    //Agregando...
    //Guardando Encabezado
    Clase.Insertar;
    //Guardando Detalles
    GuardarDetalles;
    MessageBox(Self.Handle, 'El T�cnico Ha Sido Agregado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end
  else
  begin
    //Editando...
    Clase.Modificar;
    //Actualizando Detalles (Eliminando Anteriores y Cargando los Nuevos)
    //DetalleComisionTipoServicio.EliminarCodigoTecnico(Clase.GetCodigo);
    //Guardando Detalles
    GuardarDetalles;
    MessageBox(Self.Handle, 'El T�cnico Ha Sido Editado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end;
end;

procedure TFrmServicioTecnicoTecnico.GuardarDetalles;
begin
  with Data do
  begin
    DisableControls;
    First;
    while (not Eof) do
    begin
      with ComisionTecnicoTipoServicio do
      begin
        SetCodigoTecnico(Clase.GetCodigo);
        SetCodigoTipoServicio(FieldValues['codigo_tipo_servicio']);
        SetPComision(FieldValues['p_comision']);
        SetObservaciones(FieldValues['observaciones']);
        Insertar;
      end;
      Next;
    end;
    EnableControls;
  end;
end;

procedure TFrmServicioTecnicoTecnico.Imprimir;
begin
  if (Length(Trim(TxtCodigo.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
end;

procedure TFrmServicioTecnicoTecnico.Limpiar;
begin
  TxtCodigo.Text := '';
  CmbPrefijoRif.Text := '';
  TxtRif.Text := '';
  TxtNit.Text := '';
  TxtRazonSocial.Text := '';
  TxtTelefonos.Text := '';
  DatFechaIngreso.Date := Date;
  DataZona.First;
  CmbCodigoZona.KeyValue := '';
  TxtPComisionGlobal.Text := '';
  TxtDireccionFiscal1.Text := '';
  TxtDireccionFiscal2.Text := '';
  //Status Bar
  SB.Panels[0].Text := '';
  Data.EmptyDataSet;
  TabDetalle.TabVisible := True;
  PageDetalles.ActivePage := TabDetalle;
end;

procedure TFrmServicioTecnicoTecnico.Mover;
begin

end;

procedure TFrmServicioTecnicoTecnico.Obtener;
begin
  with Clase do
  begin
    SetCodigo(TxtCodigo.Text);
    SetPrefijoRif(CmbPrefijoRif.Text);
    SetRif(TxtRif.Text);
    SetNit(TxtNit.Text);
    SetRazonSocial(TxtRazonSocial.Text);
    SetTelefonos(TxtTelefonos.Text);
    SetFechaIngreso(DatFechaIngreso.Date);
    SetCodigoZona(VarToStr(CmbCodigoZona.KeyValue));
    SetPComisionGlobal(TxtPComisionGlobal.Value);
    SetDireccionFiscal1(TxtDireccionFiscal1.Text);
    SetDireccionFiscal2(TxtDireccionFiscal2.Text);
  end;
end;

procedure TFrmServicioTecnicoTecnico.Refrescar;
begin
  //Se Actualiza El DataBuscador
  DataBuscadorTecnico := Clase.ObtenerLista;
  //Se Actualizan Los demas Componentes
  DataZona := Zona.ObtenerCombo;
  DataZona.First;
  DataBuscadorTecnico.First;
  DSZona.DataSet := DataZona;
end;

procedure TFrmServicioTecnicoTecnico.TBtnEliminarClick(Sender: TObject);
begin
  if (Length(Trim(TxtCodigo.Text)) > 0) then
  begin
    inherited;
  end
  else
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
end;

function TFrmServicioTecnicoTecnico.Validar: Boolean;
begin

end;

end.
