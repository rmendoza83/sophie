inherited FrmProducto: TFrmProducto
  Caption = 'Sophie - Productos'
  ClientHeight = 418
  ExplicitWidth = 598
  ExplicitHeight = 446
  PixelsPerInch = 96
  TextHeight = 13
  inherited Pag: TPageControl
    Height = 382
    ActivePage = TabDatos
    ExplicitHeight = 382
    inherited TabBusqueda: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 24
      ExplicitWidth = 584
      ExplicitHeight = 354
      inherited GrdBusqueda: TDBGrid
        Height = 316
      end
    end
    inherited TabDatos: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 24
      ExplicitWidth = 584
      ExplicitHeight = 354
      object LblCodigo: TLabel
        Left = 8
        Top = 11
        Width = 41
        Height = 13
        Alignment = taRightJustify
        Caption = 'C'#243'digo:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clMaroon
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblReferencia: TLabel
        Left = 221
        Top = 11
        Width = 64
        Height = 13
        Alignment = taRightJustify
        Caption = 'Referencia:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblDescripcion: TLabel
        Left = 10
        Top = 35
        Width = 68
        Height = 13
        Alignment = taRightJustify
        Caption = 'Descripci'#243'n:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblUbicacion: TLabel
        Left = 227
        Top = 328
        Width = 57
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ubicaci'#243'n:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblPeso: TLabel
        Left = 47
        Top = 224
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Peso:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblVolumen: TLabel
        Left = 25
        Top = 247
        Width = 52
        Height = 13
        Alignment = taRightJustify
        Caption = 'Vol'#250'men:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblFechaIngreso: TLabel
        Left = 6
        Top = 271
        Width = 83
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fecha Ingreso:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblFechaUltimaCompra: TLabel
        Left = 5
        Top = 295
        Width = 123
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fecha Ultima Compra:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object TxtUniqueID: TLabel
        Left = 525
        Top = 11
        Width = 52
        Height = 13
        Alignment = taRightJustify
        Caption = 'UniqueID'
        Font.Charset = ANSI_CHARSET
        Font.Color = clOlive
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblPrecio: TLabel
        Left = 10
        Top = 58
        Width = 68
        Height = 13
        Hint = 'Precio Base SIN IVA'
        Alignment = taRightJustify
        Caption = 'Precio Base:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
      end
      object LblPDescuento: TLabel
        Left = 217
        Top = 58
        Width = 89
        Height = 13
        Alignment = taRightJustify
        Caption = '(%) Descuento:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object TxtFechaIngreso: TLabel
        Left = 134
        Top = 271
        Width = 68
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fecha Ingreso'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object TxtFechaUltimaCompra: TLabel
        Left = 134
        Top = 295
        Width = 68
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ultima Compra'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object TxtCodigo: TEdit
        Left = 55
        Top = 8
        Width = 150
        Height = 21
        MaxLength = 20
        TabOrder = 0
      end
      object TxtReferencia: TEdit
        Left = 291
        Top = 8
        Width = 150
        Height = 21
        MaxLength = 20
        TabOrder = 1
      end
      object GrpCostos: TGroupBox
        Left = 0
        Top = 79
        Width = 202
        Height = 139
        Caption = 'Costos:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 8
        object LblCostoAnterior: TLabel
          Left = 38
          Top = 22
          Width = 50
          Height = 13
          Alignment = taRightJustify
          Caption = 'Anterior:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object LblCostoActual: TLabel
          Left = 49
          Top = 45
          Width = 39
          Height = 13
          Alignment = taRightJustify
          Caption = 'Actual:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object LblCostoFOB: TLabel
          Left = 29
          Top = 68
          Width = 59
          Height = 13
          Alignment = taRightJustify
          Caption = 'FOB (US$):'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object LblCostoAnteriorUS: TLabel
          Left = 3
          Top = 91
          Width = 85
          Height = 13
          Alignment = taRightJustify
          Caption = 'Anterior (US$):'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object LblCostoActualUS: TLabel
          Left = 14
          Top = 114
          Width = 74
          Height = 13
          Alignment = taRightJustify
          Caption = 'Actual (US$):'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object TxtCostoAnterior: TCurrencyEdit
          Left = 92
          Top = 19
          Width = 100
          Height = 21
          Margins.Left = 1
          Margins.Top = 1
          DecimalPlaces = 4
          DisplayFormat = '#,##0.0000'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
        end
        object TxtCostoActual: TCurrencyEdit
          Left = 92
          Top = 42
          Width = 100
          Height = 21
          Margins.Left = 1
          Margins.Top = 1
          DecimalPlaces = 4
          DisplayFormat = '#,##0.0000'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
        end
        object TxtCostoFOB: TCurrencyEdit
          Left = 92
          Top = 65
          Width = 100
          Height = 21
          Margins.Left = 1
          Margins.Top = 1
          DecimalPlaces = 4
          DisplayFormat = '#,##0.0000'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
        end
        object TxtCostoAnteriorUS: TCurrencyEdit
          Left = 92
          Top = 88
          Width = 100
          Height = 21
          Margins.Left = 1
          Margins.Top = 1
          DecimalPlaces = 4
          DisplayFormat = '#,##0.0000'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 3
        end
        object TxtCostoActualUS: TCurrencyEdit
          Left = 92
          Top = 111
          Width = 100
          Height = 21
          Margins.Left = 1
          Margins.Top = 1
          DecimalPlaces = 4
          DisplayFormat = '#,##0.0000'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 4
        end
      end
      object GrpIVA: TGroupBox
        Left = 222
        Top = 79
        Width = 359
        Height = 46
        Caption = 'I.V.A.:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 5
        object LblPIVAVenta: TLabel
          Left = 25
          Top = 20
          Width = 36
          Height = 13
          Alignment = taRightJustify
          Caption = 'Venta:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object LblPIVACompra: TLabel
          Left = 196
          Top = 20
          Width = 47
          Height = 13
          Alignment = taRightJustify
          Caption = 'Compra:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object TxtPIVAVenta: TCurrencyEdit
          Left = 65
          Top = 17
          Width = 100
          Height = 21
          Margins.Left = 1
          Margins.Top = 1
          DecimalPlaces = 4
          DisplayFormat = '#,##0.0000%'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object TxtPIVACompra: TCurrencyEdit
          Left = 247
          Top = 17
          Width = 100
          Height = 21
          Margins.Left = 1
          Margins.Top = 1
          DecimalPlaces = 4
          DisplayFormat = '#,##0.0000%'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
      end
      object GrpClasificaciones: TGroupBox
        Left = 222
        Top = 131
        Width = 359
        Height = 191
        Caption = 'Caracter'#237'sticas / Clasificaciones:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 6
        object LblDivision: TLabel
          Left = 38
          Top = 20
          Width = 47
          Height = 13
          Alignment = taRightJustify
          Caption = 'Divisi'#243'n:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object TxtDescripcionDivision: TDBText
          Left = 223
          Top = 20
          Width = 130
          Height = 13
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentColor = False
          ParentFont = False
        end
        object LblLinea: TLabel
          Left = 52
          Top = 44
          Width = 33
          Height = 13
          Alignment = taRightJustify
          Caption = 'L'#237'nea:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object TxtDescripcionLinea: TDBText
          Left = 223
          Top = 44
          Width = 97
          Height = 13
          AutoSize = True
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentColor = False
          ParentFont = False
        end
        object LblFamilia: TLabel
          Left = 42
          Top = 68
          Width = 43
          Height = 13
          Alignment = taRightJustify
          Caption = 'Familia:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object TxtDescripcionFamilia: TDBText
          Left = 223
          Top = 68
          Width = 103
          Height = 13
          AutoSize = True
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentColor = False
          ParentFont = False
        end
        object LblManufactura: TLabel
          Left = 10
          Top = 116
          Width = 75
          Height = 13
          Alignment = taRightJustify
          Caption = 'Manufactura:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object TxtDescripcionClase: TDBText
          Left = 223
          Top = 92
          Width = 97
          Height = 13
          AutoSize = True
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentColor = False
          ParentFont = False
        end
        object LblClase: TLabel
          Left = 52
          Top = 92
          Width = 33
          Height = 13
          Alignment = taRightJustify
          Caption = 'Clase:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object TxtDescripcionManufactura: TDBText
          Left = 223
          Top = 116
          Width = 131
          Height = 13
          AutoSize = True
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentColor = False
          ParentFont = False
        end
        object LblCodigoArancel: TLabel
          Left = 39
          Top = 140
          Width = 46
          Height = 13
          Alignment = taRightJustify
          Caption = 'Arancel:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object TxtDescripcionArancel: TDBText
          Left = 223
          Top = 140
          Width = 107
          Height = 13
          AutoSize = True
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentColor = False
          ParentFont = False
        end
        object LblUnidad: TLabel
          Left = 43
          Top = 164
          Width = 42
          Height = 13
          Alignment = taRightJustify
          Caption = 'Unidad:'
        end
        object TxtDescripcionUnidad: TDBText
          Left = 223
          Top = 164
          Width = 105
          Height = 13
          AutoSize = True
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentColor = False
          ParentFont = False
        end
        object CmbCodigoDivision: TRxDBLookupCombo
          Left = 91
          Top = 17
          Width = 100
          Height = 21
          DropDownCount = 20
          DropDownWidth = 400
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object BtnBuscarDivision: TBitBtn
          Left = 194
          Top = 17
          Width = 23
          Height = 22
          TabOrder = 1
          OnClick = BtnBuscarDivisionClick
          Glyph.Data = {
            36060000424D3606000000000000360400002800000020000000100000000100
            08000000000000020000850E0000850E00000001000000000000000000003300
            00006600000099000000CC000000FF0000000033000033330000663300009933
            0000CC330000FF33000000660000336600006666000099660000CC660000FF66
            000000990000339900006699000099990000CC990000FF99000000CC000033CC
            000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
            0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
            330000333300333333006633330099333300CC333300FF333300006633003366
            33006666330099663300CC663300FF6633000099330033993300669933009999
            3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
            330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
            66006600660099006600CC006600FF0066000033660033336600663366009933
            6600CC336600FF33660000666600336666006666660099666600CC666600FF66
            660000996600339966006699660099996600CC996600FF99660000CC660033CC
            660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
            6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
            990000339900333399006633990099339900CC339900FF339900006699003366
            99006666990099669900CC669900FF6699000099990033999900669999009999
            9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
            990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
            CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
            CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
            CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
            CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
            CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
            FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
            FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
            FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
            FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
            000000808000800000008000800080800000C0C0C00080808000191919004C4C
            4C00B2B2B200E5E5E5005A1E1E00783C3C0096646400C8969600FFC8C800465F
            82005591B9006EB9D7008CD2E600B4E6F000D8E9EC0099A8AC00646F7100E2EF
            F100C56A31000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000EEEEEEEEEEEE
            EEEE0909EE0909EE0909EEEEEEEEEEEEEEEE8181EE8181EE8181EEEEEEEEEEEE
            EEEE1009EE1009EE1009EEEEEEEEEEEEEEEEE281EEE281EEE281EEEEEEEEEEEE
            EEB4D8EEEEEEEEEEEEEEEEEEEEEEEEEEEEE281EEEEEEEEEEEEEEEEEEEEEEEEEE
            B46C6CD8EEEEEEEEEEEEEEEEEEEEEEEEE2818181EEEEEEEEEEEEEEEEEEEEEEEE
            B46CD86CD8EEEEEEEEEEEEEEEEEEEEEEE281818181EEEEEEEEEEEEEEEEEEEEEE
            B46C6CD86CD8EEEEEEEEEEEEEEEEEEEEE28181818181EEEEEEEEEEEEEEEEEED7
            5E6C6C6CB46CD8EEEEEEEEEEEEEEEED781818181E28181EEEEEEEEEEEEEEEED7
            89896CB4B4B46CD8EEEEEEEEEEEEEED7ACAC81E2E2E28181EEEEEEEEEEEED789
            89D7D7B4C7C7C76CEEEEEEEEEEEED7ACACD7D7E2ACACAC81EEEEEEEEEED78989
            D7D7D7D76C6C6CEEEEEEEEEEEED7ACACD7D7D7D7818181EEEEEEEEEED78989D7
            D7D75E5EEEEEEEEEEEEEEEEED7ACACD7D7D78181EEEEEEEEEEEEEED78989D7D7
            D75EEEEEEEEEEEEEEEEEEED7ACACD7D7D781EEEEEEEEEEEEEEEED78989D7D7D7
            5EB4EEEEEEEEEEEEEEEED7ACACD7D7D781E2EEEEEEEEEEEEEEEE5E89D7D7D75E
            B4EEEEEEEEEEEEEEEEEE81ACD7D7D781E2EEEEEEEEEEEEEEEEEEEE5ED7D75EEE
            EEEEEEEEEEEEEEEEEEEEEE81D7D781EEEEEEEEEEEEEEEEEEEEEEEEEE5E5EEEEE
            EEEEEEEEEEEEEEEEEEEEEEEE8181EEEEEEEEEEEEEEEEEEEEEEEE}
          NumGlyphs = 2
        end
        object CmbCodigoLinea: TRxDBLookupCombo
          Left = 91
          Top = 41
          Width = 100
          Height = 21
          DropDownCount = 20
          DropDownWidth = 400
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
        end
        object BtnBuscarLinea: TBitBtn
          Left = 194
          Top = 41
          Width = 23
          Height = 22
          TabOrder = 3
          OnClick = BtnBuscarLineaClick
          Glyph.Data = {
            36060000424D3606000000000000360400002800000020000000100000000100
            08000000000000020000850E0000850E00000001000000000000000000003300
            00006600000099000000CC000000FF0000000033000033330000663300009933
            0000CC330000FF33000000660000336600006666000099660000CC660000FF66
            000000990000339900006699000099990000CC990000FF99000000CC000033CC
            000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
            0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
            330000333300333333006633330099333300CC333300FF333300006633003366
            33006666330099663300CC663300FF6633000099330033993300669933009999
            3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
            330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
            66006600660099006600CC006600FF0066000033660033336600663366009933
            6600CC336600FF33660000666600336666006666660099666600CC666600FF66
            660000996600339966006699660099996600CC996600FF99660000CC660033CC
            660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
            6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
            990000339900333399006633990099339900CC339900FF339900006699003366
            99006666990099669900CC669900FF6699000099990033999900669999009999
            9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
            990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
            CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
            CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
            CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
            CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
            CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
            FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
            FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
            FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
            FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
            000000808000800000008000800080800000C0C0C00080808000191919004C4C
            4C00B2B2B200E5E5E5005A1E1E00783C3C0096646400C8969600FFC8C800465F
            82005591B9006EB9D7008CD2E600B4E6F000D8E9EC0099A8AC00646F7100E2EF
            F100C56A31000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000EEEEEEEEEEEE
            EEEE0909EE0909EE0909EEEEEEEEEEEEEEEE8181EE8181EE8181EEEEEEEEEEEE
            EEEE1009EE1009EE1009EEEEEEEEEEEEEEEEE281EEE281EEE281EEEEEEEEEEEE
            EEB4D8EEEEEEEEEEEEEEEEEEEEEEEEEEEEE281EEEEEEEEEEEEEEEEEEEEEEEEEE
            B46C6CD8EEEEEEEEEEEEEEEEEEEEEEEEE2818181EEEEEEEEEEEEEEEEEEEEEEEE
            B46CD86CD8EEEEEEEEEEEEEEEEEEEEEEE281818181EEEEEEEEEEEEEEEEEEEEEE
            B46C6CD86CD8EEEEEEEEEEEEEEEEEEEEE28181818181EEEEEEEEEEEEEEEEEED7
            5E6C6C6CB46CD8EEEEEEEEEEEEEEEED781818181E28181EEEEEEEEEEEEEEEED7
            89896CB4B4B46CD8EEEEEEEEEEEEEED7ACAC81E2E2E28181EEEEEEEEEEEED789
            89D7D7B4C7C7C76CEEEEEEEEEEEED7ACACD7D7E2ACACAC81EEEEEEEEEED78989
            D7D7D7D76C6C6CEEEEEEEEEEEED7ACACD7D7D7D7818181EEEEEEEEEED78989D7
            D7D75E5EEEEEEEEEEEEEEEEED7ACACD7D7D78181EEEEEEEEEEEEEED78989D7D7
            D75EEEEEEEEEEEEEEEEEEED7ACACD7D7D781EEEEEEEEEEEEEEEED78989D7D7D7
            5EB4EEEEEEEEEEEEEEEED7ACACD7D7D781E2EEEEEEEEEEEEEEEE5E89D7D7D75E
            B4EEEEEEEEEEEEEEEEEE81ACD7D7D781E2EEEEEEEEEEEEEEEEEEEE5ED7D75EEE
            EEEEEEEEEEEEEEEEEEEEEE81D7D781EEEEEEEEEEEEEEEEEEEEEEEEEE5E5EEEEE
            EEEEEEEEEEEEEEEEEEEEEEEE8181EEEEEEEEEEEEEEEEEEEEEEEE}
          NumGlyphs = 2
        end
        object CmbCodigoFamilia: TRxDBLookupCombo
          Left = 91
          Top = 65
          Width = 100
          Height = 21
          DropDownCount = 20
          DropDownWidth = 400
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 4
        end
        object BtnBuscarFamilia: TBitBtn
          Left = 194
          Top = 65
          Width = 23
          Height = 22
          TabOrder = 5
          OnClick = BtnBuscarFamiliaClick
          Glyph.Data = {
            36060000424D3606000000000000360400002800000020000000100000000100
            08000000000000020000850E0000850E00000001000000000000000000003300
            00006600000099000000CC000000FF0000000033000033330000663300009933
            0000CC330000FF33000000660000336600006666000099660000CC660000FF66
            000000990000339900006699000099990000CC990000FF99000000CC000033CC
            000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
            0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
            330000333300333333006633330099333300CC333300FF333300006633003366
            33006666330099663300CC663300FF6633000099330033993300669933009999
            3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
            330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
            66006600660099006600CC006600FF0066000033660033336600663366009933
            6600CC336600FF33660000666600336666006666660099666600CC666600FF66
            660000996600339966006699660099996600CC996600FF99660000CC660033CC
            660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
            6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
            990000339900333399006633990099339900CC339900FF339900006699003366
            99006666990099669900CC669900FF6699000099990033999900669999009999
            9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
            990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
            CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
            CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
            CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
            CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
            CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
            FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
            FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
            FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
            FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
            000000808000800000008000800080800000C0C0C00080808000191919004C4C
            4C00B2B2B200E5E5E5005A1E1E00783C3C0096646400C8969600FFC8C800465F
            82005591B9006EB9D7008CD2E600B4E6F000D8E9EC0099A8AC00646F7100E2EF
            F100C56A31000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000EEEEEEEEEEEE
            EEEE0909EE0909EE0909EEEEEEEEEEEEEEEE8181EE8181EE8181EEEEEEEEEEEE
            EEEE1009EE1009EE1009EEEEEEEEEEEEEEEEE281EEE281EEE281EEEEEEEEEEEE
            EEB4D8EEEEEEEEEEEEEEEEEEEEEEEEEEEEE281EEEEEEEEEEEEEEEEEEEEEEEEEE
            B46C6CD8EEEEEEEEEEEEEEEEEEEEEEEEE2818181EEEEEEEEEEEEEEEEEEEEEEEE
            B46CD86CD8EEEEEEEEEEEEEEEEEEEEEEE281818181EEEEEEEEEEEEEEEEEEEEEE
            B46C6CD86CD8EEEEEEEEEEEEEEEEEEEEE28181818181EEEEEEEEEEEEEEEEEED7
            5E6C6C6CB46CD8EEEEEEEEEEEEEEEED781818181E28181EEEEEEEEEEEEEEEED7
            89896CB4B4B46CD8EEEEEEEEEEEEEED7ACAC81E2E2E28181EEEEEEEEEEEED789
            89D7D7B4C7C7C76CEEEEEEEEEEEED7ACACD7D7E2ACACAC81EEEEEEEEEED78989
            D7D7D7D76C6C6CEEEEEEEEEEEED7ACACD7D7D7D7818181EEEEEEEEEED78989D7
            D7D75E5EEEEEEEEEEEEEEEEED7ACACD7D7D78181EEEEEEEEEEEEEED78989D7D7
            D75EEEEEEEEEEEEEEEEEEED7ACACD7D7D781EEEEEEEEEEEEEEEED78989D7D7D7
            5EB4EEEEEEEEEEEEEEEED7ACACD7D7D781E2EEEEEEEEEEEEEEEE5E89D7D7D75E
            B4EEEEEEEEEEEEEEEEEE81ACD7D7D781E2EEEEEEEEEEEEEEEEEEEE5ED7D75EEE
            EEEEEEEEEEEEEEEEEEEEEE81D7D781EEEEEEEEEEEEEEEEEEEEEEEEEE5E5EEEEE
            EEEEEEEEEEEEEEEEEEEEEEEE8181EEEEEEEEEEEEEEEEEEEEEEEE}
          NumGlyphs = 2
        end
        object CmbCodigoClase: TRxDBLookupCombo
          Left = 91
          Top = 89
          Width = 100
          Height = 21
          DropDownCount = 20
          DropDownWidth = 400
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 6
        end
        object BtnBuscarClase: TBitBtn
          Left = 194
          Top = 89
          Width = 23
          Height = 22
          TabOrder = 7
          OnClick = BtnBuscarClaseClick
          Glyph.Data = {
            36060000424D3606000000000000360400002800000020000000100000000100
            08000000000000020000850E0000850E00000001000000000000000000003300
            00006600000099000000CC000000FF0000000033000033330000663300009933
            0000CC330000FF33000000660000336600006666000099660000CC660000FF66
            000000990000339900006699000099990000CC990000FF99000000CC000033CC
            000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
            0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
            330000333300333333006633330099333300CC333300FF333300006633003366
            33006666330099663300CC663300FF6633000099330033993300669933009999
            3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
            330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
            66006600660099006600CC006600FF0066000033660033336600663366009933
            6600CC336600FF33660000666600336666006666660099666600CC666600FF66
            660000996600339966006699660099996600CC996600FF99660000CC660033CC
            660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
            6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
            990000339900333399006633990099339900CC339900FF339900006699003366
            99006666990099669900CC669900FF6699000099990033999900669999009999
            9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
            990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
            CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
            CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
            CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
            CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
            CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
            FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
            FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
            FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
            FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
            000000808000800000008000800080800000C0C0C00080808000191919004C4C
            4C00B2B2B200E5E5E5005A1E1E00783C3C0096646400C8969600FFC8C800465F
            82005591B9006EB9D7008CD2E600B4E6F000D8E9EC0099A8AC00646F7100E2EF
            F100C56A31000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000EEEEEEEEEEEE
            EEEE0909EE0909EE0909EEEEEEEEEEEEEEEE8181EE8181EE8181EEEEEEEEEEEE
            EEEE1009EE1009EE1009EEEEEEEEEEEEEEEEE281EEE281EEE281EEEEEEEEEEEE
            EEB4D8EEEEEEEEEEEEEEEEEEEEEEEEEEEEE281EEEEEEEEEEEEEEEEEEEEEEEEEE
            B46C6CD8EEEEEEEEEEEEEEEEEEEEEEEEE2818181EEEEEEEEEEEEEEEEEEEEEEEE
            B46CD86CD8EEEEEEEEEEEEEEEEEEEEEEE281818181EEEEEEEEEEEEEEEEEEEEEE
            B46C6CD86CD8EEEEEEEEEEEEEEEEEEEEE28181818181EEEEEEEEEEEEEEEEEED7
            5E6C6C6CB46CD8EEEEEEEEEEEEEEEED781818181E28181EEEEEEEEEEEEEEEED7
            89896CB4B4B46CD8EEEEEEEEEEEEEED7ACAC81E2E2E28181EEEEEEEEEEEED789
            89D7D7B4C7C7C76CEEEEEEEEEEEED7ACACD7D7E2ACACAC81EEEEEEEEEED78989
            D7D7D7D76C6C6CEEEEEEEEEEEED7ACACD7D7D7D7818181EEEEEEEEEED78989D7
            D7D75E5EEEEEEEEEEEEEEEEED7ACACD7D7D78181EEEEEEEEEEEEEED78989D7D7
            D75EEEEEEEEEEEEEEEEEEED7ACACD7D7D781EEEEEEEEEEEEEEEED78989D7D7D7
            5EB4EEEEEEEEEEEEEEEED7ACACD7D7D781E2EEEEEEEEEEEEEEEE5E89D7D7D75E
            B4EEEEEEEEEEEEEEEEEE81ACD7D7D781E2EEEEEEEEEEEEEEEEEEEE5ED7D75EEE
            EEEEEEEEEEEEEEEEEEEEEE81D7D781EEEEEEEEEEEEEEEEEEEEEEEEEE5E5EEEEE
            EEEEEEEEEEEEEEEEEEEEEEEE8181EEEEEEEEEEEEEEEEEEEEEEEE}
          NumGlyphs = 2
        end
        object CmbCodigoManufactura: TRxDBLookupCombo
          Left = 91
          Top = 113
          Width = 100
          Height = 21
          DropDownCount = 20
          DropDownWidth = 400
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 8
        end
        object BtnBuscarManufactura: TBitBtn
          Left = 194
          Top = 113
          Width = 23
          Height = 22
          TabOrder = 9
          OnClick = BtnBuscarManufacturaClick
          Glyph.Data = {
            36060000424D3606000000000000360400002800000020000000100000000100
            08000000000000020000850E0000850E00000001000000000000000000003300
            00006600000099000000CC000000FF0000000033000033330000663300009933
            0000CC330000FF33000000660000336600006666000099660000CC660000FF66
            000000990000339900006699000099990000CC990000FF99000000CC000033CC
            000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
            0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
            330000333300333333006633330099333300CC333300FF333300006633003366
            33006666330099663300CC663300FF6633000099330033993300669933009999
            3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
            330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
            66006600660099006600CC006600FF0066000033660033336600663366009933
            6600CC336600FF33660000666600336666006666660099666600CC666600FF66
            660000996600339966006699660099996600CC996600FF99660000CC660033CC
            660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
            6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
            990000339900333399006633990099339900CC339900FF339900006699003366
            99006666990099669900CC669900FF6699000099990033999900669999009999
            9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
            990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
            CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
            CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
            CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
            CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
            CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
            FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
            FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
            FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
            FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
            000000808000800000008000800080800000C0C0C00080808000191919004C4C
            4C00B2B2B200E5E5E5005A1E1E00783C3C0096646400C8969600FFC8C800465F
            82005591B9006EB9D7008CD2E600B4E6F000D8E9EC0099A8AC00646F7100E2EF
            F100C56A31000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000EEEEEEEEEEEE
            EEEE0909EE0909EE0909EEEEEEEEEEEEEEEE8181EE8181EE8181EEEEEEEEEEEE
            EEEE1009EE1009EE1009EEEEEEEEEEEEEEEEE281EEE281EEE281EEEEEEEEEEEE
            EEB4D8EEEEEEEEEEEEEEEEEEEEEEEEEEEEE281EEEEEEEEEEEEEEEEEEEEEEEEEE
            B46C6CD8EEEEEEEEEEEEEEEEEEEEEEEEE2818181EEEEEEEEEEEEEEEEEEEEEEEE
            B46CD86CD8EEEEEEEEEEEEEEEEEEEEEEE281818181EEEEEEEEEEEEEEEEEEEEEE
            B46C6CD86CD8EEEEEEEEEEEEEEEEEEEEE28181818181EEEEEEEEEEEEEEEEEED7
            5E6C6C6CB46CD8EEEEEEEEEEEEEEEED781818181E28181EEEEEEEEEEEEEEEED7
            89896CB4B4B46CD8EEEEEEEEEEEEEED7ACAC81E2E2E28181EEEEEEEEEEEED789
            89D7D7B4C7C7C76CEEEEEEEEEEEED7ACACD7D7E2ACACAC81EEEEEEEEEED78989
            D7D7D7D76C6C6CEEEEEEEEEEEED7ACACD7D7D7D7818181EEEEEEEEEED78989D7
            D7D75E5EEEEEEEEEEEEEEEEED7ACACD7D7D78181EEEEEEEEEEEEEED78989D7D7
            D75EEEEEEEEEEEEEEEEEEED7ACACD7D7D781EEEEEEEEEEEEEEEED78989D7D7D7
            5EB4EEEEEEEEEEEEEEEED7ACACD7D7D781E2EEEEEEEEEEEEEEEE5E89D7D7D75E
            B4EEEEEEEEEEEEEEEEEE81ACD7D7D781E2EEEEEEEEEEEEEEEEEEEE5ED7D75EEE
            EEEEEEEEEEEEEEEEEEEEEE81D7D781EEEEEEEEEEEEEEEEEEEEEEEEEE5E5EEEEE
            EEEEEEEEEEEEEEEEEEEEEEEE8181EEEEEEEEEEEEEEEEEEEEEEEE}
          NumGlyphs = 2
        end
        object CmbCodigoArancel: TRxDBLookupCombo
          Left = 91
          Top = 137
          Width = 100
          Height = 21
          DropDownCount = 20
          DropDownWidth = 400
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 10
        end
        object BtnBuscarArancel: TBitBtn
          Left = 194
          Top = 137
          Width = 23
          Height = 22
          TabOrder = 11
          OnClick = BtnBuscarArancelClick
          Glyph.Data = {
            36060000424D3606000000000000360400002800000020000000100000000100
            08000000000000020000850E0000850E00000001000000000000000000003300
            00006600000099000000CC000000FF0000000033000033330000663300009933
            0000CC330000FF33000000660000336600006666000099660000CC660000FF66
            000000990000339900006699000099990000CC990000FF99000000CC000033CC
            000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
            0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
            330000333300333333006633330099333300CC333300FF333300006633003366
            33006666330099663300CC663300FF6633000099330033993300669933009999
            3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
            330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
            66006600660099006600CC006600FF0066000033660033336600663366009933
            6600CC336600FF33660000666600336666006666660099666600CC666600FF66
            660000996600339966006699660099996600CC996600FF99660000CC660033CC
            660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
            6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
            990000339900333399006633990099339900CC339900FF339900006699003366
            99006666990099669900CC669900FF6699000099990033999900669999009999
            9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
            990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
            CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
            CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
            CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
            CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
            CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
            FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
            FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
            FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
            FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
            000000808000800000008000800080800000C0C0C00080808000191919004C4C
            4C00B2B2B200E5E5E5005A1E1E00783C3C0096646400C8969600FFC8C800465F
            82005591B9006EB9D7008CD2E600B4E6F000D8E9EC0099A8AC00646F7100E2EF
            F100C56A31000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000EEEEEEEEEEEE
            EEEE0909EE0909EE0909EEEEEEEEEEEEEEEE8181EE8181EE8181EEEEEEEEEEEE
            EEEE1009EE1009EE1009EEEEEEEEEEEEEEEEE281EEE281EEE281EEEEEEEEEEEE
            EEB4D8EEEEEEEEEEEEEEEEEEEEEEEEEEEEE281EEEEEEEEEEEEEEEEEEEEEEEEEE
            B46C6CD8EEEEEEEEEEEEEEEEEEEEEEEEE2818181EEEEEEEEEEEEEEEEEEEEEEEE
            B46CD86CD8EEEEEEEEEEEEEEEEEEEEEEE281818181EEEEEEEEEEEEEEEEEEEEEE
            B46C6CD86CD8EEEEEEEEEEEEEEEEEEEEE28181818181EEEEEEEEEEEEEEEEEED7
            5E6C6C6CB46CD8EEEEEEEEEEEEEEEED781818181E28181EEEEEEEEEEEEEEEED7
            89896CB4B4B46CD8EEEEEEEEEEEEEED7ACAC81E2E2E28181EEEEEEEEEEEED789
            89D7D7B4C7C7C76CEEEEEEEEEEEED7ACACD7D7E2ACACAC81EEEEEEEEEED78989
            D7D7D7D76C6C6CEEEEEEEEEEEED7ACACD7D7D7D7818181EEEEEEEEEED78989D7
            D7D75E5EEEEEEEEEEEEEEEEED7ACACD7D7D78181EEEEEEEEEEEEEED78989D7D7
            D75EEEEEEEEEEEEEEEEEEED7ACACD7D7D781EEEEEEEEEEEEEEEED78989D7D7D7
            5EB4EEEEEEEEEEEEEEEED7ACACD7D7D781E2EEEEEEEEEEEEEEEE5E89D7D7D75E
            B4EEEEEEEEEEEEEEEEEE81ACD7D7D781E2EEEEEEEEEEEEEEEEEEEE5ED7D75EEE
            EEEEEEEEEEEEEEEEEEEEEE81D7D781EEEEEEEEEEEEEEEEEEEEEEEEEE5E5EEEEE
            EEEEEEEEEEEEEEEEEEEEEEEE8181EEEEEEEEEEEEEEEEEEEEEEEE}
          NumGlyphs = 2
        end
        object CmbCodigoUnidad: TRxDBLookupCombo
          Left = 91
          Top = 161
          Width = 100
          Height = 21
          DropDownCount = 20
          DropDownWidth = 400
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 12
        end
        object BtnBuscarUnidad: TBitBtn
          Left = 194
          Top = 161
          Width = 23
          Height = 22
          TabOrder = 13
          OnClick = BtnBuscarUnidadClick
          Glyph.Data = {
            36060000424D3606000000000000360400002800000020000000100000000100
            08000000000000020000850E0000850E00000001000000000000000000003300
            00006600000099000000CC000000FF0000000033000033330000663300009933
            0000CC330000FF33000000660000336600006666000099660000CC660000FF66
            000000990000339900006699000099990000CC990000FF99000000CC000033CC
            000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
            0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
            330000333300333333006633330099333300CC333300FF333300006633003366
            33006666330099663300CC663300FF6633000099330033993300669933009999
            3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
            330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
            66006600660099006600CC006600FF0066000033660033336600663366009933
            6600CC336600FF33660000666600336666006666660099666600CC666600FF66
            660000996600339966006699660099996600CC996600FF99660000CC660033CC
            660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
            6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
            990000339900333399006633990099339900CC339900FF339900006699003366
            99006666990099669900CC669900FF6699000099990033999900669999009999
            9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
            990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
            CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
            CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
            CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
            CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
            CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
            FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
            FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
            FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
            FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
            000000808000800000008000800080800000C0C0C00080808000191919004C4C
            4C00B2B2B200E5E5E5005A1E1E00783C3C0096646400C8969600FFC8C800465F
            82005591B9006EB9D7008CD2E600B4E6F000D8E9EC0099A8AC00646F7100E2EF
            F100C56A31000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000EEEEEEEEEEEE
            EEEE0909EE0909EE0909EEEEEEEEEEEEEEEE8181EE8181EE8181EEEEEEEEEEEE
            EEEE1009EE1009EE1009EEEEEEEEEEEEEEEEE281EEE281EEE281EEEEEEEEEEEE
            EEB4D8EEEEEEEEEEEEEEEEEEEEEEEEEEEEE281EEEEEEEEEEEEEEEEEEEEEEEEEE
            B46C6CD8EEEEEEEEEEEEEEEEEEEEEEEEE2818181EEEEEEEEEEEEEEEEEEEEEEEE
            B46CD86CD8EEEEEEEEEEEEEEEEEEEEEEE281818181EEEEEEEEEEEEEEEEEEEEEE
            B46C6CD86CD8EEEEEEEEEEEEEEEEEEEEE28181818181EEEEEEEEEEEEEEEEEED7
            5E6C6C6CB46CD8EEEEEEEEEEEEEEEED781818181E28181EEEEEEEEEEEEEEEED7
            89896CB4B4B46CD8EEEEEEEEEEEEEED7ACAC81E2E2E28181EEEEEEEEEEEED789
            89D7D7B4C7C7C76CEEEEEEEEEEEED7ACACD7D7E2ACACAC81EEEEEEEEEED78989
            D7D7D7D76C6C6CEEEEEEEEEEEED7ACACD7D7D7D7818181EEEEEEEEEED78989D7
            D7D75E5EEEEEEEEEEEEEEEEED7ACACD7D7D78181EEEEEEEEEEEEEED78989D7D7
            D75EEEEEEEEEEEEEEEEEEED7ACACD7D7D781EEEEEEEEEEEEEEEED78989D7D7D7
            5EB4EEEEEEEEEEEEEEEED7ACACD7D7D781E2EEEEEEEEEEEEEEEE5E89D7D7D75E
            B4EEEEEEEEEEEEEEEEEE81ACD7D7D781E2EEEEEEEEEEEEEEEEEEEE5ED7D75EEE
            EEEEEEEEEEEEEEEEEEEEEE81D7D781EEEEEEEEEEEEEEEEEEEEEEEEEE5E5EEEEE
            EEEEEEEEEEEEEEEEEEEEEEEE8181EEEEEEEEEEEEEEEEEEEEEEEE}
          NumGlyphs = 2
        end
      end
      object TxtPeso: TCurrencyEdit
        Left = 84
        Top = 220
        Width = 121
        Height = 21
        Margins.Left = 4
        Margins.Top = 1
        DecimalPlaces = 4
        DisplayFormat = '#,##0.0000'
        TabOrder = 9
      end
      object TxtVolumen: TCurrencyEdit
        Left = 84
        Top = 243
        Width = 121
        Height = 21
        Margins.Left = 4
        Margins.Top = 1
        DecimalPlaces = 4
        DisplayFormat = '#,##0.0000'
        TabOrder = 10
      end
      object TxtDescripcion: TEdit
        Left = 84
        Top = 31
        Width = 492
        Height = 21
        MaxLength = 80
        TabOrder = 2
      end
      object TxtUbicacion: TEdit
        Left = 290
        Top = 325
        Width = 286
        Height = 21
        MaxLength = 40
        TabOrder = 7
      end
      object ChkInventario: TCheckBox
        Left = 3
        Top = 324
        Width = 199
        Height = 17
        Caption = 'Control de Inventario'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 11
      end
      object TxtPrecio: TCurrencyEdit
        Left = 84
        Top = 55
        Width = 121
        Height = 21
        Hint = 'Precio Base SIN IVA'
        Margins.Left = 4
        Margins.Top = 1
        DecimalPlaces = 4
        DisplayFormat = '#,##0.0000'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
      end
      object TxtPDescuento: TCurrencyEdit
        Left = 313
        Top = 55
        Width = 121
        Height = 21
        Margins.Left = 4
        Margins.Top = 1
        DecimalPlaces = 4
        DisplayFormat = '#,##0.0000'
        TabOrder = 4
      end
    end
  end
  object DSDivision: TDataSource
    Left = 224
    Top = 360
  end
  object DSLinea: TDataSource
    Left = 256
    Top = 360
  end
  object DSFamilia: TDataSource
    Left = 288
    Top = 360
  end
  object DSClase: TDataSource
    Left = 320
    Top = 360
  end
  object DSManufactura: TDataSource
    Left = 352
    Top = 360
  end
  object DSArancel: TDataSource
    Left = 384
    Top = 360
  end
  object DSUnidad: TDataSource
    Left = 416
    Top = 360
  end
end
