unit CondominioAgregarDetalleGastoOrdinario;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GenericoAgregarDetalle, Mask, rxToolEdit, rxCurrEdit, DB, StdCtrls,
  Buttons, ExtCtrls, ClsCondominioGasto, GenericoBuscador, DBClient, Utils;

type
  TFrmCondominioAgregarDetalleGastoOrdinario = class(TFrmGenericoAgregarDetalle)
    LblMonto: TLabel;
    TxtMonto: TCurrencyEdit;
    TxtDescripcionGasto: TEdit;
    procedure BtnBuscarClick(Sender: TObject);
    procedure TxtMontoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    Gasto: TCondominioGasto;
  protected
    function Buscar: Boolean; override;
    procedure Agregar; override;
    procedure Limpiar; override;
  public
    { Public declarations }
    CodigoConjuntoAdministracion: string;
  end;

var
  FrmCondominioAgregarDetalleGastoOrdinario: TFrmCondominioAgregarDetalleGastoOrdinario;

implementation

{$R *.dfm}

procedure TFrmCondominioAgregarDetalleGastoOrdinario.Agregar;
begin
  with Data do
  begin
    //Verificando si existe el codigo en el Grid
    if (Locate('codigo_gasto',TxtCodigo.Text,[loCaseInsensitive])) then
    begin
      Edit;
      FieldValues['monto'] := TxtMonto.Value;
    end
    else
    begin
      Append;
      FieldValues['codigo_conjunto_administracion'] := CodigoConjuntoAdministracion;
      FieldValues['codigo_gasto'] := Gasto.GetCodigo;
      FieldValues['descripcion'] := Gasto.GetDescripcion;
      FieldValues['monto'] := TxtMonto.Value;
    end;
    Post;
  end;
  inherited;
end;

procedure TFrmCondominioAgregarDetalleGastoOrdinario.BtnBuscarClick(
  Sender: TObject);
begin
  inherited;
  Buscador(Self,Gasto,'codigo','Buscar Gasto',TxtCodigo,nil,'("codigo_conjunto_administracion" = ' + QuotedStr(CodigoConjuntoAdministracion) + ') AND ("extraordinario" = FALSE)');
  if (Length(Trim(TxtCodigo.Text)) > 0) then
  begin
    ComponenteFoco.SetFocus;
  end;
end;

function TFrmCondominioAgregarDetalleGastoOrdinario.Buscar: Boolean;
begin
  Gasto.SetCodigoConjuntoAdministracion(CodigoConjuntoAdministracion);
  Gasto.SetCodigo(TxtCodigo.Text);
  Result := Gasto.Buscar;
  if (Result) then
  begin
    TxtDescripcionGasto.Text := Gasto.GetDescripcion;
  end
  else
  begin
    MessageBox(Self.Handle, Cadena('El Codigo No Existe!!!'), PChar(MsgTituloError), MB_ICONSTOP or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Limpiar;
  end;
end;

procedure TFrmCondominioAgregarDetalleGastoOrdinario.FormCreate(
  Sender: TObject);
begin
  inherited;
  ComponenteFoco := (TxtMonto as TWinControl);
  Gasto := TCondominioGasto.Create;
end;

procedure TFrmCondominioAgregarDetalleGastoOrdinario.Limpiar;
begin
  inherited;
  TxtDescripcionGasto.Text := '';
  TxtMonto.Value := 0;
end;

procedure TFrmCondominioAgregarDetalleGastoOrdinario.TxtMontoKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  case Key of
    VK_RETURN:
      Agregar;
  end;
end;

end.
