unit CondominioPropietario;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Generico, XPMan, ImgList, DB, Grids, DBGrids, StdCtrls, Buttons,
  ExtCtrls, ComCtrls, ToolWin, Mask, rxToolEdit, ClsCondominioPropietario,
  Utils, DBClient;

type
  TFrmCondominioPropietario = class(TFrmGenerico)
    LblCodigo: TLabel;
    LblRif: TLabel;
    LblRazonSocial: TLabel;
    LblTelefonos: TLabel;
    LblFechaNacimiento: TLabel;
    lblEmail: TLabel;
    LblNit: TLabel;
    LblSexo: TLabel;
    LblDireccionDomicilio: TLabel;
    LblDireccionFiscal: TLabel;
    LblFax: TLabel;
    LblWebsite: TLabel;
    LblFechaIngreso: TLabel;
    LblContacto: TLabel;
    TxtEdad: TLabel;
    TxtCodigo: TEdit;
    CmbPrefijoRif: TComboBox;
    TxtRif: TEdit;
    TxtRazonSocial: TEdit;
    TxtTelefonos: TEdit;
    DatFechaNacimiento: TDateEdit;
    TxtEmail: TEdit;
    TxtNit: TEdit;
    CmbSexo: TComboBox;
    TxtDireccionDomicilio2: TEdit;
    TxtDireccionDomicilio1: TEdit;
    TxtDireccionFiscal1: TEdit;
    TxtDireccionFiscal2: TEdit;
    TxtFax: TEdit;
    TxtWebsite: TEdit;
    DatFechaIngreso: TDateEdit;
    TxtContacto: TEdit;
    procedure GrdBusquedaCellClick(Column: TColumn);
    procedure FormCreate(Sender: TObject);
    procedure DatFechaNacimientoChange(Sender: TObject);
  private
    { Private declarations }
    Clase: TCondominioPropietario;
    Data: TClientDataSet;
    procedure Agregar;
    procedure Editar;
    procedure Eliminar;
    procedure Guardar;
    procedure Cancelar;
    procedure Refrescar;
    procedure Imprimir;
    procedure Buscar;
    procedure Limpiar;
    procedure Asignar;
    procedure Obtener;
    procedure Mover;
  protected
    function Validar: Boolean; override;
  public
    { Public declarations }
  end;

var
  FrmCondominioPropietario: TFrmCondominioPropietario;

implementation

{$R *.dfm}

procedure TFrmCondominioPropietario.Agregar;
begin
  //Verificando
  Limpiar;
  TxtCodigo.SetFocus
end;

procedure TFrmCondominioPropietario.Editar;
begin
  //Se Busca el Elemento
  Clase.SetCodigo(Data.FieldValues['codigo']);
  Clase.Buscar;
  Asignar;
  TxtCodigo.Enabled := False;
  CmbPrefijoRif.SetFocus;
end;

procedure TFrmCondominioPropietario.Eliminar;
begin
  //Se Busca el Elemento
  Clase.SetCodigo(Data.FieldValues['codigo']);
  Clase.Buscar;
  Clase.Eliminar;
  MessageBox(Self.Handle, 'El Propietario Se Ha Eliminado Con Exito!!!',PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  Refrescar;
  Limpiar;
end;

procedure TFrmCondominioPropietario.GrdBusquedaCellClick(Column: TColumn);
begin
  Mover;
end;

procedure TFrmCondominioPropietario.Guardar;
begin
  if (Agregando) then
  begin
    //Agregando...
    //Se Busca el Codigo
    Clase.SetCodigo(TxtCodigo.Text);
    if (Clase.BuscarCodigo) then
    begin
      MessageBox(Self.Handle, 'El C�digo Ya Existe!!!', PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      TxtCodigo.SetFocus;
      Abort;
    end
    else
    begin
      Obtener;
      Clase.Insertar;
      MessageBox(Self.Handle, 'El Propietario Ha Sido Agregado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    end;
  end
  else
  begin
    //Editando...
    Obtener;
    Clase.Modificar;
    MessageBox(Self.Handle, 'El Propietario Ha Sido Editado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    TxtCodigo.Enabled := True;
  end;
  Refrescar;
  Limpiar;
end;

procedure TFrmCondominioPropietario.Cancelar;
begin
  TxtCodigo.Enabled := True;
end;

procedure TFrmCondominioPropietario.Refrescar;
begin
  TxtCriterio.Text := '';
  Buscar;
end;

procedure TFrmCondominioPropietario.Imprimir;
begin

end;

procedure TFrmCondominioPropietario.Buscar;
begin
  Screen.Cursor := crSQLWait;
  if (Length(Trim(TxtCriterio.Text)) > 0) then
  begin
    Data := Clase.ObtenerListaCriterio(Trim(TxtCriterio.Text));
  end
  else
  begin
    Data := Clase.ObtenerLista;
  end;
  Data.First;
  DS.DataSet := Data;
  GrdBusqueda.Refresh;
  Screen.Cursor := crDefault;
end;

procedure TFrmCondominioPropietario.Limpiar;
begin
  TxtCodigo.Text := '';
  CmbPrefijoRif.Text := '';
  TxtRif.Text := '';
  TxtNit.Text := '';
  CmbSexo.Text := '';
  DatFechaNacimiento.Date := Now;
  TxtRazonSocial.Text := '';
  TxtTelefonos.Text := '';
  TxtFax.Text := '';
  TxtEmail.Text := '';
  TxtWebsite.Text := '';
  DatFechaIngreso.Date := Now;
  TxtContacto.Text := '';
  TxtDireccionFiscal1.Text := '';
  TxtDireccionFiscal2.Text := '';
  TxtDireccionDomicilio1.Text := '';
  TxtDireccionDomicilio2.Text := '';
end;

procedure TFrmCondominioPropietario.Asignar;
begin
  with Clase do
  begin
    TxtCodigo.Text := GetCodigo;
    CmbPrefijoRif.ItemIndex := CmbPrefijoRif.Items.IndexOf(GetPrefijoRif);
    TxtRif.Text := GetRif;
    TxtNit.Text := GetNit;
    CmbSexo.ItemIndex := CmbSexo.Items.IndexOf(GetSexo);
    DatFechaNacimiento.Date:= GetFechaNacimiento;
    TxtRazonSocial.Text := GetRazonSocial;
    TxtTelefonos.Text := GetTelefonos;
    TxtFax.Text := GetFax;
    TxtEmail.Text := GetEmail;
    TxtWebsite.Text := GetWebsite;
    DatFechaIngreso.Date:= GetFechaIngreso;
    TxtContacto.Text := GetContacto;
    TxtDireccionFiscal1.Text := GetDireccionFiscal1;
    TxtDireccionFiscal2.Text := GetDireccionFiscal2;
    TxtDireccionDomicilio1.Text := GetDireccionDomicilio1;
    TxtDireccionDomicilio2.Text := GetDireccionDomicilio2;
  end;
end;

procedure TFrmCondominioPropietario.DatFechaNacimientoChange(Sender: TObject);
var
  Edad: Integer;
begin
  Edad := YearOld(Date,DatFechaNacimiento.Date);
  Edad := Edad * (-1);
  TxtEdad.Caption := IntToStr(Edad) + ' A�os';
end;

procedure TFrmCondominioPropietario.FormCreate(Sender: TObject);
begin
  inherited;
  ProcAgregar := Agregar;
  ProcEditar := Editar;
  ProcEliminar := Eliminar;
  ProcGuardar := Guardar;
  ProcCancelar := Cancelar;
  ProcRefrescar := Refrescar;
  ProcImprimir := Imprimir;
  ProcBuscar := Buscar;
  Clase := TCondominioPropietario.Create;
  Buscar;
end;

procedure TFrmCondominioPropietario.Obtener;
begin
  with Clase do
  begin
    SetCodigo(TxtCodigo.Text);
    SetPrefijoRif(CmbPrefijoRif.Text);
    SetRif(TxtRif.Text);
    SetNit(TxtNit.Text);
    SetSexo(CmbSexo.Text[1]);
    SetFechaNacimiento(DatFechaNacimiento.Date);
    SetRazonSocial(TxtRazonSocial.Text);
    SetTelefonos(TxtTelefonos.Text);
    SetFax(TxtFax.Text);
    SetEmail(TxtEmail.Text);
    SetWebsite(TxtWebsite.Text);
    SetFechaIngreso(DatFechaIngreso.Date);
    SetContacto(TxtContacto.Text);
    SetDireccionFiscal1(TxtDireccionFiscal1.Text);
    SetDireccionFiscal2(TxtDireccionFiscal2.Text);
    SetDireccionDomicilio1(TxtDireccionDomicilio1.Text);
    SetDireccionDomicilio2(TxtDireccionDomicilio2.Text);
  end;
end;

procedure TFrmCondominioPropietario.Mover;
var
  DSTemp: TDataSet;
begin
  if (DS.DataSet.RecordCount > 0) then
  begin
    DSTemp := DS.DataSet;
    with Clase do
    begin
      SetCodigo(DSTemp.FieldValues['codigo']);
      SetPrefijoRif(DSTemp.FieldValues['prefijo_rif']);
      SetRif(DSTemp.FieldValues['rif']);
      SetNit(DSTemp.FieldValues['nit']);
      SetRazonSocial(DSTemp.FieldValues['razon_social']);
      SetSexo(DSTemp.FieldByName('sexo').AsString[1]);
      SetFechaNacimiento(DSTemp.FieldByName('fecha_nacimiento').AsDateTime);
      SetDireccionFiscal1(DSTemp.FieldValues['direccion_fiscal_1']);
      SetDireccionFiscal2(DSTemp.FieldValues['direccion_fiscal_2']);
      SetDireccionDomicilio1(DSTemp.FieldValues['direccion_domicilio_1']);
      SetDireccionDomicilio2(DSTemp.FieldValues['direccion_domicilio_2']);
      SetTelefonos(DSTemp.FieldValues['telefonos']);
      SetFax(DSTemp.FieldValues['fax']);
      SetEmail(DSTemp.FieldValues['email']);
      SetWebsite(DSTemp.FieldValues['website']);
      SetFechaIngreso(DSTemp.FieldByName('fecha_ingreso').AsDateTime);
      SetContacto(DSTemp.FieldValues['contacto']);
    end;
    Asignar;
  end
  else
  begin
    Limpiar;
  end;
end;

function TFrmCondominioPropietario.Validar: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  (* Validando Informacion del Cliente *)
  //Validando Codigo
  if (Length(Trim(TxtCodigo.Text)) = 0) then
  begin
    ListaErrores.Add('El C�digo No Puede Estar En Blanco!!!');
  end;
  //Validando Prefijo
  if (Length(Trim(CmbPrefijoRif.Text)) = 0) then
  begin
    ListaErrores.Add('Debe Asignar un Prefijo Ej. "E" o "V"!!!');
  end;
  //Validando Cedula
  if (Length(Trim(TxtRif.Text)) = 0) then
  begin
    ListaErrores.Add('La C�dula No Puede Estar En Blanco!!!');
  end;
  //Validando Nit
  if (Length(Trim(TxtNit.Text)) = 0) then
  begin
    ListaErrores.Add('El Nit No Puede Estar En Blanco!!!');
  end;
  //Validando Sexo
  if (Length(Trim(CmbSexo.Text)) = 0) then
  begin
    ListaErrores.Add('El Campo Sexo No Puede Estar En Blanco!!!');
  end;
  //Validando Razon Social
  if (Length(Trim(TxtRazonSocial.Text)) = 0) then
  begin
    ListaErrores.Add('La Raz�n Social No Puede Estar En Blanco!!!');
  end;
  //Validando Telefonos
  if (Length(Trim(TxtTelefonos.Text)) = 0) then
  begin
    ListaErrores.Add('Los Tel�fonos No Puede Estar En Blanco!!!');
  end;
  //Validando Direccion Fiscal
  if (Length(Trim(TxtDireccionFiscal1.Text)) = 0) then
  begin
    ListaErrores.Add('La Direcci�n Fiscal No Puede Estar En Blanco!!!');
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
