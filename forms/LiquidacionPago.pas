unit LiquidacionPago;

{$I sophie.inc}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GenericoDetalle, DB, DBClient, ImgList, XPMan, ComCtrls, Grids,
  DBGrids, ExtCtrls, ToolWin, rxCurrEdit, StdCtrls, Buttons, Mask, rxToolEdit,
  RxLookup, Utils, ClsLiquidacionPago, ClsDetLiquidacionPago,
  ClsTipoRetencion, ClsAnticipoProveedor, ClsFacturacionProveedor,
  ClsNotaDebitoProveedor, ClsNotaCreditoProveedor, ClsProveedor, ClsZona,
  ClsFormaPago, ClsCuentaBancaria, ClsMoneda, ClsBusquedaLiquidacionPago,
  ClsPagoPagado, ClsCuentaxPagar, ClsOperacionBancaria, GenericoBuscador,
  ClsReporte, ClsParametro{$IFDEF DELPHI10}, ImageList{$ENDIF};

type

  TFrmLiquidacionPago = class(TFrmGenericoDetalle)
    TabObservaciones: TTabSheet;
    TxtObservaciones: TRichEdit;
    Dataid_cobranza: TIntegerField;
    Dataorden_cobranza: TIntegerField;
    Dataabonado: TFloatField;
    Datadesde: TStringField;
    Datanumero_documento: TStringField;
    Datanumero_cuota: TIntegerField;
    Datamonto_base: TFloatField;
    Datap_iva: TFloatField;
    Dataimpuesto: TFloatField;
    Datatotal: TFloatField;
    Datacodigo_retencion_iva: TStringField;
    Datadescripcion_retencion_iva: TStringField;
    Datamonto_retencion_iva: TFloatField;
    Datacodigo_retencion_1: TStringField;
    Datadescripcion_retencion_1: TStringField;
    Datamonto_retencion_1: TFloatField;
    Datacodigo_retencion_2: TStringField;
    Datadescripcion_retencion_2: TStringField;
    Datamonto_retencion_2: TFloatField;
    Datacodigo_retencion_3: TStringField;
    Datadescripcion_retencion_3: TStringField;
    Datamonto_retencion_3: TFloatField;
    Datacodigo_retencion_4: TStringField;
    Datadescripcion_retencion_4: TStringField;
    Datamonto_retencion_4: TFloatField;
    Datacodigo_retencion_5: TStringField;
    Datadescripcion_retencion_5: TStringField;
    Datamonto_retencion_5: TFloatField;
    DSProveedor: TDataSource;
    DSZona: TDataSource;
    DSMoneda: TDataSource;
    DSFormaPago: TDataSource;
    DSCuentaBancaria: TDataSource;
    LblNumero: TLabel;
    LblCodigoProveedor: TLabel;
    LblCodigoCuentaBancaria: TLabel;
    LblCodigoZona: TLabel;
    LblCodigoEsquemaPago: TLabel;
    LblMoneda: TLabel;
    LblFechaIngreso: TLabel;
    LblMontoNeto: TLabel;
    LblNumeroFormaPago: TLabel;
    LblFechaBancaria: TLabel;
    LblNumeroDocumentoBancario: TLabel;
    LblTotalRetencion: TLabel;
    LblTotal: TLabel;
    LblFechaContable: TLabel;
    TxtNumero: TEdit;
    CmbCodigoProveedor: TRxDBLookupCombo;
    DatFechaIngreso: TDateEdit;
    CmbCodigoCuentaBancaria: TRxDBLookupCombo;
    DatFechaContable: TDateEdit;
    CmbCodigoZona: TRxDBLookupCombo;
    CmbCodigoFormaPago: TRxDBLookupCombo;
    CmbCodigoMoneda: TRxDBLookupCombo;
    BtnBuscarProveedor: TBitBtn;
    BtnBuscarCuentaBancaria: TBitBtn;
    BtnBuscarZona: TBitBtn;
    BtnBuscarFormaPago: TBitBtn;
    BtnBuscarMoneda: TBitBtn;
    TxtNumeroFormaPago: TEdit;
    DatFechaBancaria: TDateEdit;
    TxtNumeroDocumentoBancario: TEdit;
    TxtTotalRetencion: TCurrencyEdit;
    TxtMontoNeto: TCurrencyEdit;
    TxtTotal: TCurrencyEdit;
    TBtnImprimirCheque: TToolButton;
    procedure TBtnImprimirChequeClick(Sender: TObject);
    procedure GridDetalleKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure GridDetalleDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormCreate(Sender: TObject);
    procedure DataAfterPost(DataSet: TDataSet);
    procedure BtnBuscarZonaClick(Sender: TObject);
    procedure BtnBuscarMonedaClick(Sender: TObject);
    procedure BtnBuscarFormaPagoClick(Sender: TObject);
    procedure BtnBuscarCuentaBancariaClick(Sender: TObject);
    procedure BtnBuscarProveedorClick(Sender: TObject);
    procedure CmbCodigoProveedorChange(Sender: TObject);
    procedure CmbCodigoProveedorExit(Sender: TObject);
    procedure TBtnEliminarClick(Sender: TObject);
  private
    { Private declarations }
    Clase: TLiquidacionPago;
    DetLiquidacionPago: TDetLiquidacionPago;
    BuscadorLiquidacionPago: TBusquedaLiquidacionPago;
    Proveedor: TProveedor;
    Zona: TZona;
    Moneda: TMoneda;
    FormaPago: TFormaPago;
    OperacionBancaria: TOperacionBancaria;
    CuentaBancaria: TCuentaBancaria;
    AnticipoProveedor: TAnticipoProveedor;
    FacturaProveedor: TFacturacionProveedor;
    NotaDebitoProveedor: TNotaDebitoProveedor;
    NotaCreditoProveedor: TNotaCreditoProveedor;
    TipoRetencion: TTipoRetencion;
    CuentaxPagar: TCuentaxPagar;
    PagoPagado: TPagoPagado;
    Reporte: TReporte;
    Parametro: TParametro;
    DataBuscadorLiquidacionPago: TClientDataSet;
    DataProveedor: TClientDataSet;
    DataZona: TClientDataSet;
    DataMoneda: TClientDataSet;
    DataFormaPago: TClientDataSet;
    DataCuentaBancaria: TClientDataSet;
    procedure Agregar;
    procedure Editar;
    procedure Eliminar;
    procedure Guardar;
    procedure Cancelar;
    procedure Refrescar;
    procedure Imprimir;
    procedure Buscar;
    procedure Limpiar;
    procedure Asignar;
    procedure Obtener;
    procedure Mover;
    procedure GuardarDetalles;
    procedure GuardarOperacionesBancarias;
    procedure CalcularMontos;
    procedure ImprimirCheque;
  protected
    { Protected declarations }
    function Validar: Boolean; override;
    procedure Detalle; override;
  public
    { Public declarations }
  end;

var
  FrmLiquidacionPago: TFrmLiquidacionPago;

implementation

uses
  ActividadSophie,
  PModule,
  ClsGenericoBD,
  AgregarDetalleLiquidacionPago,
  Math,
  Types
  {$IFDEF USE_RAVE_REPORTS}
  , DMRR
  {$ENDIF}
  {$IFDEF USE_FAST_REPORTS}
  , DMFR
  {$ENDIF}
  ;

{$R *.dfm}

{ TFrmLiquidacionPago }

procedure TFrmLiquidacionPago.Agregar;
begin
  //Verificando
  Limpiar;
  //Asignando Valores por Defecto
  CmbCodigoZona.Value := Parametro.ObtenerValor('codigo_zona');
  CmbCodigoMoneda.Value := Parametro.ObtenerValor('codigo_moneda');
  CmbCodigoProveedor.SetFocus;
end;

procedure TFrmLiquidacionPago.Asignar;
var
  DataDetalle: TClientDataSet;
  Campo: string;
  i: Integer;

  function BuscarDescripcionRetencion(varCodigo: string): string;
  begin
    Result := '';
    if (Length(Trim(varCodigo)) > 0) then
    begin
      TipoRetencion.SetCodigo(varCodigo);
      TipoRetencion.Buscar;
      Result := TipoRetencion.GetDescripcion;
    end;       
  end;

begin
  with Clase do
  begin
    TxtNumero.Text := GetNumero;
    CmbCodigoMoneda.KeyValue := GetCodigoMoneda;
    CmbCodigoProveedor.KeyValue := GetCodigoProveedor;
    CmbCodigoZona.KeyValue := GetCodigoZona;
    DatFechaIngreso.Date := GetFechaIngreso;
    DatFechaContable.Date := GetFechaContable;
    TxtObservaciones.Text := GetObservaciones;
    TxtTotal.Value := GetTotal;
    TxtTotalRetencion.Value := GetTotalRetencion;
    CmbCodigoFormaPago.KeyValue := GetCodigoFormaPago;
    TxtNumeroFormaPago.Text := GetNumeroFormaPago;
    CmbCodigoCuentaBancaria.KeyValue := GetCodigoCuentaBancaria;
    TxtNumeroDocumentoBancario.Text := GetNumeroDocumentoBancario;
    DatFechaBancaria.Date := GetFechaOperacionBancaria;
    //Cargando DataSet Secundario "Data"
    DataDetalle := TClientDataSet.Create(Self);
    DataDetalle.CloneCursor(DetLiquidacionPago.ObtenerIdLiquidacionPago(GetIdLiquidacionPago),False,True);
    Data.EmptyDataSet;
    DataDetalle.First;
    while (not DataDetalle.Eof) do
    begin
      Data.Insert;
      //Asignando Campos Similares
      for i := 0 to DataDetalle.FieldCount - 1 do
      begin
        Campo := DataDetalle.FieldDefs[i].Name;
        if (Data.FieldDefs.IndexOf(Campo) <> -1) then
        begin
          Data.FieldValues[Campo] := DataDetalle.FieldValues[Campo];
        end;        
      end;
      PagoPagado.SetIdPago(DataDetalle.FieldValues['id_pago']);
      PagoPagado.SetNumeroCuota(DataDetalle.FieldValues['numero_cuota']);
      PagoPagado.SetOrdenPago(DataDetalle.FieldValues['orden_pago']);
      PagoPagado.Buscar;
      Data.FieldValues['desde'] := PagoPagado.GetDesde;
      Data.FieldValues['numero_documento'] := PagoPagado.GetNumeroDocumento;
      //Asignando Campos Faltantes
      if (PagoPagado.GetDesde = 'AP') then
      begin //Anticipo de Proveedor
        AnticipoProveedor.SetNumero(PagoPagado.GetNumeroDocumento);
        AnticipoProveedor.BuscarNumero;
        Data.FieldValues['monto_base'] := AnticipoProveedor.GetMontoNeto;
        Data.FieldValues['p_iva'] := 0;
        Data.FieldValues['impuesto'] := 0;
        Data.FieldValues['total'] := AnticipoProveedor.GetMontoNeto;
      end
      else
      begin
        if (PagoPagado.GetDesde = 'NDP') then
        begin //Nota Debito de Proveedor
          NotaDebitoProveedor.SetNumero(PagoPagado.GetNumeroDocumento);
          NotaDebitoProveedor.BuscarNumero;
          Data.FieldValues['monto_base'] := NotaDebitoProveedor.GetMontoNeto;
          Data.FieldValues['p_iva'] := NotaDebitoProveedor.GetPIva;
          Data.FieldValues['impuesto'] := NotaDebitoProveedor.GetImpuesto;
          Data.FieldValues['total'] := NotaDebitoProveedor.GetTotal;
        end
        else
        begin
          if (PagoPagado.GetDesde = 'NCP') then
          begin //Nota Credito de Proveedor
            NotaCreditoProveedor.SetNumero(PagoPagado.GetNumeroDocumento);
            NotaCreditoProveedor.BuscarNumero;
            Data.FieldValues['monto_base'] := NotaCreditoProveedor.GetMontoNeto;
            Data.FieldValues['p_iva'] := NotaCreditoProveedor.GetPIva;
            Data.FieldValues['impuesto'] := NotaCreditoProveedor.GetImpuesto;
            Data.FieldValues['total'] := NotaCreditoProveedor.GetTotal;
          end
          else
          begin //Factura de Proveedor
            FacturaProveedor.SetNumero(PagoPagado.GetNumeroDocumento);
            FacturaProveedor.BuscarNumero;
            Data.FieldValues['monto_base'] := FacturaProveedor.GetMontoNeto;
            Data.FieldValues['p_iva'] := FacturaProveedor.GetPIva;
            Data.FieldValues['impuesto'] := FacturaProveedor.GetImpuesto;
            Data.FieldValues['total'] := FacturaProveedor.GetTotal;
          end;
        end;
      end;
      Data.FieldValues['descripcion_retencion_iva'] := BuscarDescripcionRetencion(DataDetalle.FieldValues['codigo_retencion_iva']);
      Data.FieldValues['descripcion_retencion_1'] := BuscarDescripcionRetencion(DataDetalle.FieldValues['codigo_retencion_1']);
      Data.FieldValues['descripcion_retencion_2'] := BuscarDescripcionRetencion(DataDetalle.FieldValues['codigo_retencion_2']);
      Data.FieldValues['descripcion_retencion_3'] := BuscarDescripcionRetencion(DataDetalle.FieldValues['codigo_retencion_3']);
      Data.FieldValues['descripcion_retencion_4'] := BuscarDescripcionRetencion(DataDetalle.FieldValues['codigo_retencion_4']);
      Data.FieldValues['descripcion_retencion_5'] := BuscarDescripcionRetencion(DataDetalle.FieldValues['codigo_retencion_5']);
      Data.Post;
      DataDetalle.Next;
    end;
    DS.DataSet := Data;
  end;
end;

procedure TFrmLiquidacionPago.BtnBuscarMonedaClick(Sender: TObject);
begin
  Buscador(Self,Moneda,'codigo','Buscar Moneda',nil,DataMoneda);
  CmbCodigoMoneda.KeyValue := DataMoneda.FieldValues['codigo'];
end;

procedure TFrmLiquidacionPago.BtnBuscarZonaClick(Sender: TObject);
begin
  Buscador(Self,Zona,'codigo','Buscar Zona',nil,DataZona);
  CmbCodigoZona.KeyValue := DataZona.FieldValues['codigo'];
end;

procedure TFrmLiquidacionPago.BtnBuscarCuentaBancariaClick(Sender: TObject);
begin
  Buscador(Self,CuentaBancaria,'codigo','Buscar Cuenta Bancaria',nil,DataCuentaBancaria);
  CmbCodigoCuentaBancaria.KeyValue := DataCuentaBancaria.FieldValues['codigo'];
end;

procedure TFrmLiquidacionPago.BtnBuscarFormaPagoClick(Sender: TObject);
begin
  Buscador(Self,FormaPago,'codigo','Buscar Forma de Pago',nil,DataFormaPago);
  CmbCodigoFormaPago.KeyValue := DataFormaPago.FieldValues['codigo'];
end;

procedure TFrmLiquidacionPago.BtnBuscarProveedorClick(Sender: TObject);
begin
  Buscador(Self,Proveedor,'codigo','Buscar Proveedor',nil,DataProveedor);
  CmbCodigoProveedor.KeyValue := DataProveedor.FieldValues['codigo'];
  CmbCodigoProveedorExit(Sender);
end;

procedure TFrmLiquidacionPago.Buscar;
begin
  if (Buscador(Self,BuscadorLiquidacionPago,'numero','Buscar Liquidaci�n',nil,DataBuscadorLiquidacionPago)) then
  begin
    Clase.SetNumero(DataBuscadorLiquidacionPago.FieldValues['numero']);
    Clase.BuscarNumero;
    Asignar;
    //Buscando y Habilitando Boton Imprimir Cheques
    FormaPago.SetCodigo(Clase.GetCodigoFormaPago);
    FormaPago.Buscar;
    TBtnImprimirCheque.Enabled := FormaPago.GetCheque
  end;
end;

procedure TFrmLiquidacionPago.CalcularMontos;
var
  Neto: Currency;
  TotalRetencion: Currency;
begin
  Neto := 0;
  TotalRetencion := 0;
  with Data do
  begin
    DisableControls;
    First;
    while (not Eof) do
    begin
      if ((FieldValues['desde'] = 'AP') or (FieldValues['desde'] = 'NCP')) then
      begin
        Neto := Neto + (FieldValues['abonado'] * -1);
      end
      else
      begin
        Neto := Neto + FieldValues['abonado'];
      end;
      TotalRetencion := TotalRetencion + (FieldValues['monto_retencion_iva'] +
                                          FieldValues['monto_retencion_1'] +
                                          FieldValues['monto_retencion_2'] +
                                          FieldValues['monto_retencion_3'] +
                                          FieldValues['monto_retencion_4'] +
                                          FieldValues['monto_retencion_5']);
      Next;
    end;
    EnableControls;
  end;
  //Asignando Montos
  TxtMontoNeto.Value := Neto;
  TxtTotal.Value := Neto - TotalRetencion;
  TxtTotalRetencion.Value := TotalRetencion;
end;

procedure TFrmLiquidacionPago.Cancelar;
begin
  if (Agregando) then
  begin
    Limpiar;
  end
  else
  begin
    Clase.SetNumero(TxtNumero.Text);
    Clase.BuscarNumero;
    Asignar;
  end;
end;

procedure TFrmLiquidacionPago.CmbCodigoProveedorChange(Sender: TObject);
begin
  SB.Panels[0].Text := DataProveedor.FieldValues['razon_social'];
end;

procedure TFrmLiquidacionPago.CmbCodigoProveedorExit(Sender: TObject);
begin
  if (Length(Trim(CmbCodigoProveedor.Text)) > 0) then
  begin
    if (Agregando) then
    begin
      Proveedor.SetCodigo(CmbCodigoProveedor.Value);
      Proveedor.BuscarCodigo;
      CmbCodigoZona.Value := Proveedor.GetCodigoZona;
    end;
  end;
end;

procedure TFrmLiquidacionPago.DataAfterPost(DataSet: TDataSet);
begin
  CalcularMontos;
end;

procedure TFrmLiquidacionPago.Detalle;
begin
  inherited;
  FrmAgregarDetalleLiquidacionPago := TFrmAgregarDetalleLiquidacionPago.CrearDetalle(Self,Data);
  FrmAgregarDetalleLiquidacionPago.CodigoProveedor := CmbCodigoProveedor.Text;
  FrmAgregarDetalleLiquidacionPago.ShowModal;
  FreeAndNil(FrmAgregarDetalleLiquidacionPago);
end;

procedure TFrmLiquidacionPago.Editar;
begin
  if (Length(Trim(TxtNumero.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end
  else
  begin
    MessageBox(Self.Handle, 'Las Liquidaciones de Pagos No Pueden Ser Editadas!!!', PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_APPLMODAL);
    Abort;
  end;
end;

procedure TFrmLiquidacionPago.Eliminar;
var
  MontoNeto: Double;
  sw: Boolean;
begin
  //Verificando Que La Liquidacion de Pago Pueda Ser Eliminada!!!
  //Verificando Orden de Pago por cada Item
  sw := False;
  with Data do
  begin
    First;
    while (not Eof) do
    begin
      PagoPagado.SetIdPago(FieldValues['id_pago']);
      PagoPagado.SetNumeroCuota(FieldValues['numero_cuota']);
      PagoPagado.SetOrdenPago(FieldValues['orden_pago']);
      if (PagoPagado.VerificarOrdenPago) then
      begin
        //Existe Una Pago Mas Reciente!!!
        sw := True;
        Break;
      end;
      Next;
    end;
  end;
  //Consultando Si Puede Eliminarse La Liquidacion de Pago
  if (not sw) then
  begin
    //////////////////////////////////
    // Reversando Items de Pago //
    //////////////////////////////////
    with Data do
    begin
      First;
      while (not Eof) do
      begin
        //Ubicando CobranzaCobrada
        PagoPagado.SetIdPago(FieldValues['id_pago']);
        PagoPagado.SetNumeroCuota(FieldValues['numero_cuota']);
        PagoPagado.SetOrdenPago(FieldValues['orden_pago']);
        PagoPagado.Buscar;
        //Verificando Si Existe Una Cuenta x Pagar Pendiente
        CuentaxPagar.SetDesde(FieldValues['desde']);
        CuentaxPagar.SetNumeroDocumento(FieldValues['numero_documento']);
        CuentaxPagar.SetNumeroCuota(FieldValues['numero_cuota']);
        if (CuentaxPagar.BuscarPago) then
        begin
          //Eliminado CuentaxPagar Pendiente
          CuentaxPagar.Eliminar;
          //Moviendo PagoPagado a CuentaxPagar (Solo Lo Necesario)
          CuentaxPagar.SetOrdenPago(CuentaxPagar.GetOrdenPago);
          CuentaxPagar.SetTotal(CuentaxPagar.GetTotal + PagoPagado.GetTotal);
          //Calculando Nuevo Monto Neto e Impuesto
          MontoNeto := CuentaxPagar.GetTotal / (1 + (CuentaxPagar.GetPIva / 100));
          MontoNeto := Trunc(MontoNeto * Power(10,4)) / Power(10,4);
          CuentaxPagar.SetMontoNeto(MontoNeto);
          CuentaxPagar.SetImpuesto(CuentaxPagar.GetTotal - MontoNeto);
          CuentaxPagar.Insertar;
          //Borrando PagoPagado
          PagoPagado.Eliminar;
        end
        else
        begin
          //Moviendo PagoPagado a CuentaxPagar
          //Moviendo Entre Clases
          CuentaxPagar.SetDesde(PagoPagado.GetDesde);
          CuentaxPagar.SetNumeroDocumento(PagoPagado.GetNumeroDocumento);
          CuentaxPagar.SetCodigoMoneda(PagoPagado.GetCodigoMoneda);
          CuentaxPagar.SetCodigoProveedor(PagoPagado.GetCodigoProveedor);
          CuentaxPagar.SetCodigoZona(PagoPagado.GetCodigoZona);
          CuentaxPagar.SetFechaIngreso(PagoPagado.GetFechaIngreso);
          CuentaxPagar.SetFechaRecepcion(PagoPagado.GetFechaRecepcion);
          CuentaxPagar.SetFechaVencIngreso(PagoPagado.GetFechaVencIngreso);
          CuentaxPagar.SetFechaVencRecepcion(PagoPagado.GetFechaVencRecepcion);
          CuentaxPagar.SetFechaLibros(PagoPagado.GetFechaLibros);
          CuentaxPagar.SetFechaContable(PagoPagado.GetFechaContable);
          CuentaxPagar.SetFechaPago(PagoPagado.GetFechaPago);
          CuentaxPagar.SetNumeroCuota(PagoPagado.GetNumeroCuota);
          CuentaxPagar.SetTotalCuotas(PagoPagado.GetTotalCuotas);
          CuentaxPagar.SetOrdenPago(PagoPagado.GetOrdenPago);
          CuentaxPagar.SetMontoNeto(PagoPagado.GetMontoNeto);
          CuentaxPagar.SetImpuesto(PagoPagado.GetImpuesto);
          CuentaxPagar.SetTotal(PagoPagado.GetTotal);
          CuentaxPagar.SetPIva(PagoPagado.GetPIva);
          CuentaxPagar.SetIdPago(PagoPagado.GetIdPago);
          CuentaxPagar.SetIdContadoPago(PagoPagado.GetIdContadoPago);
          CuentaxPagar.Insertar;
          //Borrando PagoPagado
          PagoPagado.Eliminar;
        end;
        Next;
      end;
    end;
    ///////////////////////////////
    // Procesando Eliminacion!!! //
    ///////////////////////////////
    //Verificando Si Se Elimina La Operacion Bancaria
    if (Clase.GetIdOperacionBancaria <> -1) then
    begin
      OperacionBancaria.SetIdOperacionBancaria(Clase.GetIdOperacionBancaria);
      OperacionBancaria.BuscarIdOperacionBancaria;
      OperacionBancaria.Eliminar;
    end;
    //Se Busca La Liquidacion de Pago
    Clase.SetNumero(TxtNumero.Text);
    Clase.BuscarNumero;
    //Se Eliminan Los Detalles
    DetLiquidacionPago.EliminarIdLiquidacionPago(Clase.GetIdLiquidacionPago);
    Clase.Eliminar;
    MessageBox(Self.Handle, 'La Liquidaci�n de Pago Se Ha Eliminado Con Exito!!!',PChar(MsgTituloInformacion), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Refrescar;
    Limpiar;
  end
  else
  begin
    MessageBox(Self.Handle, 'La Liquidaci�n de Pago No Puede Ser Eliminada!!!'+#13+#10+''+#13+#10+'Para Poder Eliminar Una Liquidaci�n de Pago Se Debe Hacer Desde La Ultima Liquidaci�n Generada Asociada A Los Documentos Presentados En Esta Liquidaci�n!', PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_APPLMODAL);
    Abort;
  end;
end;

procedure TFrmLiquidacionPago.FormCreate(Sender: TObject);
begin
  inherited;
  ProcAgregar := Agregar;
  ProcEditar := Editar;
  ProcEliminar := Eliminar;
  ProcGuardar := Guardar;
  ProcCancelar := Cancelar;
  ProcRefrescar := Refrescar;
  ProcImprimir := Imprimir;
  ProcBuscar := Buscar;
  FrmActividadSophie.Show;
  FrmActividadSophie.Actividad('Cargando N�cleo...');
  Clase := TLiquidacionPago.Create;
  //Inicializando DataSets Secundarios
  BuscadorLiquidacionPago := TBusquedaLiquidacionPago.Create;
  Proveedor := TProveedor.Create;
  Zona := TZona.Create;
  Moneda := TMoneda.Create;
  FormaPago := TFormaPago.Create;
  OperacionBancaria := TOperacionBancaria.Create;
  CuentaBancaria := TCuentaBancaria.Create;
  AnticipoProveedor := TAnticipoProveedor.Create;
  FacturaProveedor := TFacturacionProveedor.Create;
  NotaDebitoProveedor := TNotaDebitoProveedor.Create;
  NotaCreditoProveedor := TNotaCreditoProveedor.Create;
  TipoRetencion := TTipoRetencion.Create;
  CuentaxPagar := TCuentaxPagar.Create;
  PagoPagado := TPagoPagado.Create;
  Reporte := TReporte.Create;
  Parametro := TParametro.Create;
  DetLiquidacionPago := TDetLiquidacionPago.Create;
  FrmActividadSophie.Actividad('Cargando Proveedores...');
  DataProveedor := Proveedor.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Zonas...');
  DataZona := Zona.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Monedas...');
  DataMoneda := Moneda.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Formas de Pagos...');
  DataFormaPago := FormaPago.ObtenerComboPago;
  FrmActividadSophie.Actividad('Cargando Cuentas Bancarias...');
  DataCuentaBancaria := CuentaBancaria.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Resto del N�cleo...');
  DataBuscadorLiquidacionPago := BuscadorLiquidacionPago.ObtenerLista;
  DataProveedor.First;
  DataZona.First;
  DataMoneda.First;
  DataFormaPago.First;
  DataCuentaBancaria.First;
  DataBuscadorLiquidacionPago.First;
  DSProveedor.DataSet := DataProveedor;
  DSZona.DataSet := DataZona;
  DSMoneda.DataSet := DataMoneda;
  DSFormaPago.DataSet := DataFormaPago;
  DSCuentaBancaria.DataSet := DataCuentaBancaria;
  ConfigurarRxDBLookupCombo(CmbCodigoProveedor,DSProveedor,'codigo','codigo;razon_social');
  ConfigurarRxDBLookupCombo(CmbCodigoZona,DSZona,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoMoneda,DSMoneda,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoFormaPago,DSFormaPago,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoCuentaBancaria,DSCuentaBancaria,'codigo','codigo;entidad;numero');
  DS.DataSet := Data;
  GridDetalle.DataSource := DS;
  FrmActividadSophie.Close;
  //Buscar;
end;

procedure TFrmLiquidacionPago.GridDetalleDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if ((Data.FieldByName('desde').AsString = 'AP') or (Data.FieldByName('desde').AsString = 'NCP')) then
  begin
    GridDetalle.Canvas.Font.Color := clRed;
  end
  else
  begin
    GridDetalle.Canvas.Font.Color := clBlue;
  end;
  GridDetalle.DefaultDrawColumnCell(Rect,DataCol,Column,State);
end;

procedure TFrmLiquidacionPago.GridDetalleKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if (Agregando or Editando) then
  begin
    if (((Key = VK_INSERT) or (Key = VK_DELETE)) and (Length(Trim(CmbCodigoProveedor.Text)) = 0)) then
    begin
      MessageBox(Self.Handle, 'Seleccione Primero un Proveedor!!!', PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      CmbCodigoProveedor.SetFocus;
      CmbCodigoProveedor.DropDown;
      Abort;
    end;
  end;
  //Llamando a Evento Padre
  inherited;
end;

procedure TFrmLiquidacionPago.Guardar;
begin
  CalcularMontos;
  Obtener;
  if (Agregando) then
  begin
    //Agregando...
    //Guardando Encabezado
    Clase.Insertar;
    TxtNumero.Text := Clase.GetNumero;
    Application.ProcessMessages;
    //Guardando Detalles
    GuardarDetalles;
    //Guardando Operaciones Bancarias
    GuardarOperacionesBancarias;
    MessageBox(Self.Handle, 'La Liquidaci�n de Pago Ha Sido Agregada Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    //Se Imprime La Liquidacion
    MessageBox(Self.Handle, 'Presione "Aceptar" Para Proceder A Imprimir La Liquidaci�n!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    with Reporte do
    begin
      AgregarParametro('Numero',Clase.GetNumero);
      AgregarParametro('MontoLetra',NumeroALetras(Clase.GetTotal));
      EmiteReporte('LiquidacionPago',trGeneral);
    end;
  end
  else
  begin
    //Editando...
    Clase.Modificar;
    //Actualizando Detalles (Eliminando Anteriores y Cargando los Nuevos)
    DetLiquidacionPago.EliminarIdLiquidacionPago(Clase.GetIdLiquidacionPago);
    //Guardando Detalles
    GuardarDetalles;
    //Guardando Operaciones Bancarias
    GuardarOperacionesBancarias;
    MessageBox(Self.Handle, 'La Liquidaci�n de Pago Ha Sido Editada Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end;
  //Actualizando Dataset de Busqueda
  DataBuscadorLiquidacionPago := BuscadorLiquidacionPago.ObtenerLista;
end;

procedure TFrmLiquidacionPago.GuardarDetalles;
var
  Total: Double;
begin
  with Data do
  begin
    DisableControls;
    First;
    while (not Eof) do
    begin
      //Guardando Detalle del Pago
      with DetLiquidacionPago do
      begin
        SetIdLiquidacionPago(Clase.GetIdLiquidacionPago);
        SetIdPago(FieldValues['id_pago']);
        SetNumeroCuota(FieldValues['numero_cuota']);
        SetOrdenPago(FieldValues['orden_pago']);
        SetAbonado(FieldValues['abonado']);
        SetCodigoRetencionIva(FieldValues['codigo_retencion_iva']);
        SetMontoRetencionIva(FieldValues['monto_retencion_iva']);
        SetCodigoRetencion1(FieldValues['codigo_retencion_1']);
        SetMontoRetencion1(FieldValues['monto_retencion_1']);
        SetCodigoRetencion2(FieldValues['codigo_retencion_2']);
        SetMontoRetencion2(FieldValues['monto_retencion_2']);
        SetCodigoRetencion3(FieldValues['codigo_retencion_3']);
        SetMontoRetencion3(FieldValues['monto_retencion_3']);
        SetCodigoRetencion4(FieldValues['codigo_retencion_4']);
        SetMontoRetencion4(FieldValues['monto_retencion_4']);
        SetCodigoRetencion5(FieldValues['codigo_retencion_5']);
        SetMontoRetencion5(FieldValues['monto_retencion_5']);
        Insertar;
      end;
      //Actualizando Los Pagos (CuentasxPagar y PagoPagado)
      //Se Busca el Renglon de la Cuenta por Pagar
      CuentaxPagar.SetDesde(FieldValues['desde']);
      CuentaxPagar.SetNumeroDocumento(FieldValues['numero_documento']);
      CuentaxPagar.SetNumeroCuota(FieldValues['numero_cuota']);
      CuentaxPagar.BuscarPago;
      if (SameValue(FieldValues['abonado'],CuentaxPagar.GetTotal)) then
      begin //Caso de Abonar Completo
        //Se Mueve la Cuenta x Pagar directamente a la Cobranza Cobrada
        PagoPagado.SetDesde(CuentaxPagar.GetDesde);
        PagoPagado.SetNumeroDocumento(CuentaxPagar.GetNumeroDocumento);
        PagoPagado.SetCodigoMoneda(CuentaxPagar.GetCodigoMoneda);
        PagoPagado.SetCodigoProveedor(CuentaxPagar.GetCodigoProveedor);
        PagoPagado.SetCodigoZona(CuentaxPagar.GetCodigoZona);
        PagoPagado.SetFechaIngreso(CuentaxPagar.GetFechaIngreso);
        PagoPagado.SetFechaRecepcion(CuentaxPagar.GetFechaRecepcion);
        PagoPagado.SetFechaVencIngreso(CuentaxPagar.GetFechaVencIngreso);
        PagoPagado.SetFechaVencRecepcion(CuentaxPagar.GetFechaVencRecepcion);
        PagoPagado.SetFechaLibros(CuentaxPagar.GetFechaLibros);
        PagoPagado.SetFechaContable(CuentaxPagar.GetFechaContable);
        PagoPagado.SetFechaPago(Date);
        PagoPagado.SetNumeroCuota(CuentaxPagar.GetNumeroCuota);
        PagoPagado.SetTotalCuotas(CuentaxPagar.GetTotalCuotas);
        PagoPagado.SetOrdenPago(CuentaxPagar.GetOrdenPago);
        PagoPagado.SetMontoNeto(CuentaxPagar.GetMontoNeto);
        PagoPagado.SetImpuesto(CuentaxPagar.GetImpuesto);
        PagoPagado.SetTotal(CuentaxPagar.GetTotal);
        PagoPagado.SetPIva(CuentaxPagar.GetPIva);
        PagoPagado.SetIdPago(CuentaxPagar.GetIdPago);
        PagoPagado.SetIdContadoPago(CuentaxPagar.GetIdContadoPago);
        PagoPagado.Insertar;
        //Se Elimina La Cuenta x Pagar
        CuentaxPagar.Eliminar;
      end
      else
      begin //Caso de Abonar Incompleto
        //Se Registra La Cuenta x Pagar en Pago Pagado Segun Abono
        PagoPagado.SetDesde(CuentaxPagar.GetDesde);
        PagoPagado.SetNumeroDocumento(CuentaxPagar.GetNumeroDocumento);
        PagoPagado.SetCodigoMoneda(CuentaxPagar.GetCodigoMoneda);
        PagoPagado.SetCodigoProveedor(CuentaxPagar.GetCodigoProveedor);
        PagoPagado.SetCodigoZona(CuentaxPagar.GetCodigoZona);
        PagoPagado.SetFechaIngreso(CuentaxPagar.GetFechaIngreso);
        PagoPagado.SetFechaRecepcion(CuentaxPagar.GetFechaRecepcion);
        PagoPagado.SetFechaVencIngreso(CuentaxPagar.GetFechaVencIngreso);
        PagoPagado.SetFechaVencRecepcion(CuentaxPagar.GetFechaVencRecepcion);
        PagoPagado.SetFechaLibros(CuentaxPagar.GetFechaLibros);
        PagoPagado.SetFechaContable(CuentaxPagar.GetFechaContable);
        PagoPagado.SetFechaPago(Date);
        PagoPagado.SetNumeroCuota(CuentaxPagar.GetNumeroCuota);
        PagoPagado.SetTotalCuotas(CuentaxPagar.GetTotalCuotas);
        PagoPagado.SetOrdenPago(CuentaxPagar.GetOrdenPago);
        PagoPagado.SetMontoNeto(FieldValues['abonado'] / (1 + (CuentaxPagar.GetPIva / 100)));
        PagoPagado.SetImpuesto(FieldValues['abonado'] - PagoPagado.GetMontoNeto);
        PagoPagado.SetTotal(FieldValues['abonado']);
        PagoPagado.SetPIva(CuentaxPagar.GetPIva);
        PagoPagado.SetIdPago(CuentaxPagar.GetIdPago);
        PagoPagado.SetIdContadoPago(CuentaxPagar.GetIdContadoPago);
        PagoPagado.Insertar;
        //Se Elimina La Cuenta x Pagar
        CuentaxPagar.Eliminar;
        //Se Registra La Cuenta x Pagar Pendiente Segun Abono
        //Se Actualiza El Orden de la Cobranza y Los Montos
        Total := CuentaxPagar.GetTotal;
        CuentaxPagar.SetOrdenPago(CuentaxPagar.GetOrdenPago + 1);
        CuentaxPagar.SetMontoNeto((Total - FieldValues['abonado']) / (1 + (CuentaxPagar.GetPIva / 100)));
        CuentaxPagar.SetImpuesto((Total - FieldValues['abonado']) - CuentaxPagar.GetMontoNeto);
        CuentaxPagar.SetTotal(Total - FieldValues['abonado']);
        CuentaxPagar.Insertar;
      end;
      Next;
    end;
    EnableControls;
  end;
end;

procedure TFrmLiquidacionPago.GuardarOperacionesBancarias;
begin
  if (DataFormaPago['operacion_bancaria']) then
  begin
    with OperacionBancaria do
    begin
      SetCodigoCuenta(CmbCodigoCuentaBancaria.Text);
      SetCodigoTipoOperacion(DataFormaPago['codigo_tipo_operacion_bancaria']);
      SetNumero(TxtNumeroDocumentoBancario.Text);
      SetFecha(DatFechaBancaria.Date);
      SetFechaContable(DatFechaBancaria.Date);
      SetMonto(Clase.GetTotal);
      SetConcepto('Liquidaci�n de Pago Generada N� ' + Clase.GetNumero);
      SetObservaciones('');
      SetDiferido(False);
      SetConciliado(False);
      SetFechaConciliacion(Date);
      SetBeneficiario('');
      SetLoginUsuario(P.User.GetLoginUsuario);
      Insertar;
    end;
    //Actualizando IdOperacionBancaria en la Liquidacion
    with Clase do
    begin
      SetIdOperacionBancaria(OperacionBancaria.GetIdOperacionBancaria);
      Modificar;
    end;
  end;
end;

procedure TFrmLiquidacionPago.Imprimir;
begin
  if (Length(Trim(TxtNumero.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
  with Reporte do
  begin
    AgregarParametro('Numero',Clase.GetNumero);
    AgregarParametro('MontoLetra',NumeroALetras(Clase.GetTotal));
    EmiteReporte('LiquidacionPago',trGeneral);
  end;
end;

procedure TFrmLiquidacionPago.ImprimirCheque;
begin
  if (MessageBox(Self.Handle, '�Desea Imprimir en Formato de Cheque?', PChar(MsgTituloInformacion), MB_ICONQUESTION or MB_YESNO or MB_TOPMOST or MB_TASKMODAL) = mrYes) then
  begin
    with Reporte do
    begin
      AgregarParametro('Monto',TxtTotal.Text);
      //Buscando el Proveedor
      Proveedor.SetCodigo(Clase.GetCodigoProveedor);
      Proveedor.BuscarCodigo;
      AgregarParametro('Beneficiario',Proveedor.GetRazonSocial);
      AgregarParametro('MontoLetra',NumeroALetras(Clase.GetTotal));
      AgregarParametro('FechaCheque',DatFechaBancaria.Text);
      EmiteReporte('Cheque',trGeneral);
    end;
  end;
end;

procedure TFrmLiquidacionPago.Limpiar;
begin
  TxtNumero.Text := '';
  CmbCodigoFormaPago.KeyValue := '';
  TxtNumeroFormaPago.Text := '';
  CmbCodigoCuentaBancaria.KeyValue := '';
  TxtNumeroDocumentoBancario.Text := '';
  CmbCodigoProveedor.KeyValue := '';
  CmbCodigoZona.KeyValue := '';
  DatFechaIngreso.Date := Date;
  DatFechaContable.Date := Date;
  DatFechaBancaria.Date := Date;
  CmbCodigoMoneda.KeyValue := '';
  TxtMontoNeto.Value := 0;
  TxtTotalRetencion.Value := 0;
  TxtTotal.Value := 0;
  TxtObservaciones.Text := '';
  //Status Bar
  SB.Panels[0].Text := '';
  Data.EmptyDataSet;
end;

procedure TFrmLiquidacionPago.Mover;
begin

end;

procedure TFrmLiquidacionPago.Obtener;
begin
  with Clase do
  begin
    SetNumero(TxtNumero.Text);
    SetCodigoMoneda(CmbCodigoMoneda.Text);
    SetCodigoProveedor(CmbCodigoProveedor.Text);
    SetCodigoZona(CmbCodigoZona.Text);
    SetFechaIngreso(DatFechaIngreso.Date);
    SetFechaContable(DatFechaContable.Date);
    SetObservaciones(TxtObservaciones.Text);
    SetMontoNeto(TxtMontoNeto.Value);
    SetTotalRetencion(TxtTotalRetencion.Value);
    SetTotal(TxtTotal.Value);
    SetCodigoFormaPago(CmbCodigoFormaPago.Text);
    SetNumeroFormaPago(TxtNumeroFormaPago.Text);
    SetCodigoCuentaBancaria(CmbCodigoCuentaBancaria.Text);
    SetNumeroDocumentoBancario(TxtNumeroDocumentoBancario.Text);
    SetFechaOperacionBancaria(DatFechaBancaria.Date);
    SetIdLiquidacionPago(-1);
    SetIdOperacionBancaria(-1);
  end;
end;

procedure TFrmLiquidacionPago.Refrescar;
begin
  //Actualizando Dataset de Busqueda
  DataBuscadorLiquidacionPago := BuscadorLiquidacionPago.ObtenerLista;
  //Se Actualizar Los demas Componentes
  DataProveedor := Proveedor.ObtenerCombo;
  DataZona := Zona.ObtenerCombo;
  DataMoneda := Moneda.ObtenerCombo;
  DataFormaPago := FormaPago.ObtenerComboPago;
  DataCuentaBancaria := CuentaBancaria.ObtenerCombo;
  DataBuscadorLiquidacionPago := BuscadorLiquidacionPago.ObtenerLista;
  DataProveedor.First;
  DataZona.First;
  DataMoneda.First;
  DataFormaPago.First;
  DataCuentaBancaria.First;
  DataBuscadorLiquidacionPago.First;
  DSProveedor.DataSet := DataProveedor;
  DSZona.DataSet := DataZona;
  DSMoneda.DataSet := DataMoneda;
  DSFormaPago.DataSet := DataFormaPago;
  DSCuentaBancaria.DataSet := DataCuentaBancaria;
end;


procedure TFrmLiquidacionPago.TBtnEliminarClick(Sender: TObject);
begin
  if (Length(Trim(TxtNumero.Text)) > 0) then
  begin
    inherited;
  end
  else
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
end;

procedure TFrmLiquidacionPago.TBtnImprimirChequeClick(Sender: TObject);
begin
  ImprimirCheque;
end;

function TFrmLiquidacionPago.Validar: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  //Validando Proveedor
  if (Length(Trim(CmbCodigoProveedor.Text)) = 0) then
  begin
    ListaErrores.Add('El Proveedor No Puede Estar En Blanco!!!');
  end;
  //Validando Zona
  if (Length(Trim(CmbCodigoZona.Text)) = 0) then
  begin
    ListaErrores.Add('La Zona No Puede Estar En Blanco!!!');
  end;
  //Validando Moneda
  if (Length(Trim(CmbCodigoMoneda.Text)) = 0) then
  begin
    ListaErrores.Add('La Moneda No Puede Estar En Blanco!!!');
  end;
  //Validando Forma de Pago
  if (Length(Trim(CmbCodigoFormaPago.Text)) = 0) then
  begin
    ListaErrores.Add('La Forma de Pago No Puede Estar En Blanco!!!');
  end
  else
  begin
    //Validando Nota de Debito
    if ((not DataFormaPago.FieldValues['operacion_bancaria']) AND ((DataFormaPago.FieldValues['nd']) OR (DataFormaPago.FieldValues['nc']) OR (DataFormaPago.FieldValues['anticipo']) OR (DataFormaPago.FieldValues['cheque']))) then
    begin
      if (Length(Trim(TxtNumeroFormaPago.Text)) = 0) then
      begin
        ListaErrores.Add('Debe Definir El Numero del Documento Asociado a la Forma de Pago!!!');
      end;
    end;
    //Validando Datos de Operaciones Bancarias
    if (DataFormaPago.FieldValues['operacion_bancaria']) then
    begin
      //Validando Cuenta Bancaria
      if (Length(Trim(CmbCodigoCuentaBancaria.Text)) = 0) then
      begin
        ListaErrores.Add('La Cuenta Bancaria No Puede Estar En Blanco!!!');
      end;
      if (Length(Trim(TxtNumeroDocumentoBancario.Text)) = 0) then
      begin
        ListaErrores.Add('El Numero de la Operacion Bancaria No Puede Estar En Blanco!!!');
      end;
    end;
  end;
  //Validando Monto
  if (TxtTotal.Value <= 0) then
  begin
    ListaErrores.Add('El Total Debe Ser Positivo!!!');
  end;
  //Validando Fechas
  if (DatFechaIngreso.Date > DatFechaContable.Date) then
  begin
    ListaErrores.Add('La Fecha de Ingreso Debe Ser Menor o Igual a la Fecha Contable!!!');
  end;
  //Validando Detalle
  if (Data.RecordCount = 0) then
  begin
    ListaErrores.Add('El Detalle De La Liquidaci�n No Puede Estar Vacio!!!');
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
