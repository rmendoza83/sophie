unit ReporteGeneral;

{$I sophie.inc}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, XPMan, Buttons, DB, DBClient, Mask, rxToolEdit,
  ClsReporte, ClsUser, DBCtrls, RxLookup, ClsCliente, ClsProveedor, ClsProducto,
  ClsDivision, ClsLinea, ClsFamilia, ClsClase, ClsManufactura, ClsArancel,
  ClsEstacion, ClsZona, ClsMoneda, ClsCuentaBancaria, ClsVendedor, RXCtrls;

type
  TFrmReporteGeneral = class(TForm)
    TemaXP: TXPManifest;
    GrpListadoReportes: TRadioGroup;
    GrpParametrosReporte: TGroupBox;
    PanelBotones: TPanel;
    BtnEmitir: TBitBtn;
    BtnInicializar: TBitBtn;
    BtnCerrar: TBitBtn;
    DSClienteDesde: TDataSource;
    DSClienteHasta: TDataSource;
    DSProveedorHasta: TDataSource;
    DSProveedorDesde: TDataSource;
    DSProductoDesde: TDataSource;
    DSProductoHasta: TDataSource;
    DSClasif1: TDataSource;
    DSClasif2: TDataSource;
    DSClasif3: TDataSource;
    DSClasif4: TDataSource;
    DSClasif5: TDataSource;
    DSClasif6: TDataSource;
    DSEstacion: TDataSource;
    DSZona: TDataSource;
    DSMoneda: TDataSource;
    DSCuentaBancaria: TDataSource;
    GrpPeriodo: TGroupBox;
    LblFechaAl: TLabel;
    LblFechaDesde: TLabel;
    LblFechaHasta: TLabel;
    DatFechaAl: TDateEdit;
    DatFechaDesde: TDateEdit;
    DatFechaHasta: TDateEdit;
    GrpRangoProveedor: TGroupBox;
    LblProveedorDesde: TLabel;
    LblProveedorHasta: TLabel;
    CmbProveedorDesde: TRxDBLookupCombo;
    CmbProveedorHasta: TRxDBLookupCombo;
    GrpRangoProducto: TGroupBox;
    LblProductoDesde: TLabel;
    LblProductoHasta: TLabel;
    CmbProductoDesde: TRxDBLookupCombo;
    CmbProductoHasta: TRxDBLookupCombo;
    GrpClasificacionesProducto: TGroupBox;
    LblClasif1: TLabel;
    LblClasif2: TLabel;
    LblClasif3: TLabel;
    LblClasif4: TLabel;
    LblClasif5: TLabel;
    LblClasif6: TLabel;
    CmbClasif1: TRxDBLookupCombo;
    CmbClasif2: TRxDBLookupCombo;
    CmbClasif3: TRxDBLookupCombo;
    CmbClasif4: TRxDBLookupCombo;
    CmbClasif5: TRxDBLookupCombo;
    CmbClasif6: TRxDBLookupCombo;
    GrpOtros: TGroupBox;
    LblEstacion: TLabel;
    LblZona: TLabel;
    LblMoneda: TLabel;
    LblCuentaBancaria: TLabel;
    CmbEstacion: TRxDBLookupCombo;
    CmbZona: TRxDBLookupCombo;
    CmbMoneda: TRxDBLookupCombo;
    CmbCuentaBancaria: TRxDBLookupCombo;
    GrpTipoReporte: TGroupBox;
    CmbTipoReporte: TComboBox;
    GrpClaseReporte: TGroupBox;
    CmbClaseReporte: TComboBox;
    SLinea: TShape;
    LblRangoCliente: TRxLabel;
    GrpRangoCliente: TGroupBox;
    LblClienteDesde: TLabel;
    LblClienteHasta: TLabel;
    CmbClienteDesde: TRxDBLookupCombo;
    CmbClienteHasta: TRxDBLookupCombo;
    LblPeriodo: TRxLabel;
    LblRangoProveedor: TRxLabel;
    LblRangoProducto: TRxLabel;
    LblClasificacionProducto: TRxLabel;
    LblOtros: TRxLabel;
    LblTipoReporte: TRxLabel;
    LblClaseReporte: TRxLabel;
    LblVendedor: TLabel;
    CmbVendedor: TRxDBLookupCombo;
    DsVendedor: TDataSource;
    LblTipoOrdenamiento: TRxLabel;
    GrpTipoOrdenamiento: TGroupBox;
    CmbTipoOrdenamiento: TComboBox;
    procedure CmbClasif6Change(Sender: TObject);
    procedure CmbClasif5Change(Sender: TObject);
    procedure CmbClasif4Change(Sender: TObject);
    procedure CmbClasif3Change(Sender: TObject);
    procedure CmbClasif2Change(Sender: TObject);
    procedure CmbClasif1Change(Sender: TObject);
    procedure LblOtrosDblClick(Sender: TObject);
    procedure LblVendedorDblClick(Sender: TObject);
    procedure LblCuentaBancariaDblClick(Sender: TObject);
    procedure LblMonedaDblClick(Sender: TObject);
    procedure LblZonaDblClick(Sender: TObject);
    procedure LblEstacionDblClick(Sender: TObject);
    procedure LblClasif6DblClick(Sender: TObject);
    procedure LblClasif5DblClick(Sender: TObject);
    procedure LblClasif4DblClick(Sender: TObject);
    procedure LblClasif3DblClick(Sender: TObject);
    procedure LblClasif2DblClick(Sender: TObject);
    procedure LblClasif1DblClick(Sender: TObject);
    procedure LblProductoHastaDblClick(Sender: TObject);
    procedure LblProductoDesdeDblClick(Sender: TObject);
    procedure LblProveedorHastaDblClick(Sender: TObject);
    procedure LblProveedorDesdeDblClick(Sender: TObject);
    procedure LblClienteHastaDblClick(Sender: TObject);
    procedure LblClienteDesdeDblClick(Sender: TObject);
    procedure CmbProductoDesdeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CmbProductoDesdeCloseUp(Sender: TObject);
    procedure CmbProveedorDesdeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CmbProveedorDesdeCloseUp(Sender: TObject);
    procedure CmbClienteDesdeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CmbClienteDesdeCloseUp(Sender: TObject);
    procedure LblClasificacionProductoDblClick(Sender: TObject);
    procedure LblRangoProductoDblClick(Sender: TObject);
    procedure LblRangoProveedorDblClick(Sender: TObject);
    procedure LblPeriodoDblClick(Sender: TObject);
    procedure LblRangoClienteDblClick(Sender: TObject);
    procedure BtnCerrarClick(Sender: TObject);
    procedure BtnInicializarClick(Sender: TObject);
    procedure BtnEmitirClick(Sender: TObject);
    procedure GrpListadoReportesClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CmbTipoReporteClick(Sender: TObject);
  private
    { Private declarations }
    Reporte: TReporte;
    User: TUser;
    (* Clases Temporales a Utilizar para la Seleccion de Parametros *)
    Cliente: TCliente;
    Proveedor: TProveedor;
    Producto: TProducto;
    Division: TDivision;
    Linea: TLinea;
    Familia: TFamilia;
    Clase: TClase;
    Manufactura: TManufactura;
    Arancel: TArancel;
    Estacion: TEstacion;
    Zona: TZona;
    Vendedor : TVendedor;
    Moneda: TMoneda;
    CuentaBancaria: TCuentaBancaria;
    (* Fin Clases Temporales *)
    (* Controles Adicionales *)
    DataClienteDesde: TClientDataSet;
    DataClienteHasta: TClientDataSet;
    DataProveedorDesde: TClientDataSet;
    DataProveedorHasta: TClientDataSet;
    DataProductoDesde: TClientDataSet;
    DataProductoHasta: TClientDataSet;
    DataClasif1: TClientDataSet;
    DataClasif2: TClientDataSet;
    DataClasif3: TClientDataSet;
    DataClasif4: TClientDataSet;
    DataClasif5: TClientDataSet;
    DataClasif6: TClientDataSet;
    DataEstacion: TClientDataSet;
    DataZona: TClientDataSet;
    DataMoneda: TClientDataSet;
    DataCuentaBancaria: TClientDataSet;
    DataVendedor: TClientDataSet;
    (* Fin Controles Adicionales *)
    procedure SeleccionReporte;
    procedure Inicializar;
    procedure CargarParametros;
    procedure EmitirReporte;
    procedure Deshabilitar;
    function Validar: Boolean;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent; ALoginUsuario: String); reintroduce;
  end;

var
  FrmReporteGeneral: TFrmReporteGeneral;

implementation

uses
  ActividadSophie,
  GenericoBuscador,
  Utils
  {$IFDEF USE_RAVE_REPORTS}
  , DMRR
  {$ENDIF}
  {$IFDEF USE_FAST_REPORTS}
  , DMFR
  {$ENDIF}
  ;

{$R *.dfm}

procedure TFrmReporteGeneral.SeleccionReporte;
begin
  Deshabilitar;
  //Activacion de Controles Segun Convenga
  DatFechaAl.Date := Date;
  DatFechaDesde.Date := Date;
  DatFechaHasta.Date := Date;
  case GrpListadoReportes.ItemIndex of
    0: //Reporte Balance de Caja  (Ingresos)
    begin
      LblPeriodo.Enabled := True;
      LblFechaAl.Enabled := False; DatFechaAl.Enabled := False;
      LblFechaDesde.Enabled := True; DatFechaDesde.Enabled := True;
      LblFechaHasta.Enabled := True; DatFechaHasta.Enabled := True;
      LblTipoReporte.Enabled := True;
      CmbTipoReporte.Enabled := True;
      CmbTipoReporte.Items.Clear;
      CmbTipoReporte.Items.Add('Detallado');
      CmbTipoReporte.Items.Add('Resumido');
      CmbTipoReporte.ItemIndex := 0;
      LblClaseReporte.Enabled := True;
      CmbClaseReporte.Enabled := True;
      CmbClaseReporte.Items.Clear;
      CmbClaseReporte.Items.Add('Cobranzas');
      CmbClaseReporte.ItemIndex := 0;
    end;
    1: //Reporte Balance de Caja  (Egresos)
    begin
      LblPeriodo.Enabled := True;
      LblFechaAl.Enabled := False; DatFechaAl.Enabled := False;
      LblFechaDesde.Enabled := True; DatFechaDesde.Enabled := True;
      LblFechaHasta.Enabled := True; DatFechaHasta.Enabled := True;
      LblTipoReporte.Enabled := True;
      CmbTipoReporte.Enabled := True;
      CmbTipoReporte.Items.Clear;
      CmbTipoReporte.Items.Add('Detallado');
      CmbTipoReporte.Items.Add('Resumido');
      CmbTipoReporte.ItemIndex := 0;
      LblClaseReporte.Enabled := True;
      CmbClaseReporte.Enabled := True;
      CmbClaseReporte.Items.Clear;
      CmbClaseReporte.Items.Add('Pagos');
      CmbClaseReporte.ItemIndex := 0;
    end;
    2: //Estados de Cuentas Bancarios
    begin
      LblPeriodo.Enabled := True;
      LblFechaDesde.Enabled := True; DatFechaDesde.Enabled := True;
      LblFechaHasta.Enabled := True; DatFechaHasta.Enabled := True;
      LblOtros.Enabled := True;
      LblCuentaBancaria.Enabled := True;
      CmbCuentaBancaria.Enabled := True;
      DataCuentaBancaria.First;
      CmbCuentaBancaria.KeyValue := DataCuentaBancaria.FieldValues['codigo'];
      LblTipoReporte.Enabled := True;
      CmbTipoReporte.Enabled := True;
      CmbTipoReporte.Items.Clear;
      CmbTipoReporte.Items.Add('Estandar');
      CmbTipoReporte.Items.Add('Consolidado de Cuentas');
      CmbTipoReporte.ItemIndex := 0;
    end;
    3: //Estado de Cuentas de Clientes
    begin
      LblPeriodo.Enabled := True;
      LblFechaAl.Enabled := False; DatFechaAl.Enabled := False;
      LblFechaDesde.Enabled := True; DatFechaDesde.Enabled := True;
      LblFechaHasta.Enabled := True; DatFechaHasta.Enabled := True;
      LblRangoCliente.Enabled := True;
      LblClienteDesde.Enabled := True; CmbClienteDesde.Enabled := True;
      LblClienteHasta.Enabled := True; CmbClienteHasta.Enabled := True;
      LblOtros.Enabled := True;
      LblZona.Enabled := True; CmbZona.Enabled := True;
      LblTipoReporte.Enabled := True;
      CmbTipoReporte.Enabled := True;
      CmbTipoReporte.Items.Clear;
      CmbTipoReporte.Items.Add('Detallado');
      CmbTipoReporte.Items.Add('Resumido');
      CmbTipoReporte.Items.Add('Analisis de Vencimiento Detallado');
      CmbTipoReporte.Items.Add('Analisis de Vencimiento Resumido');
      CmbTipoReporte.Items.Add('Analisis de Vencimiento Detallado (Modelo D�as)');
      CmbTipoReporte.Items.Add('Analisis de Vencimiento Resumido (Modelo D�as)');
      CmbTipoReporte.ItemIndex := 0;
    end;
    4: //Estado de Cuentas de Proveedores
    begin
      LblPeriodo.Enabled := True;
      LblFechaAl.Enabled := False; DatFechaAl.Enabled := False;
      LblFechaDesde.Enabled := True; DatFechaDesde.Enabled := True;
      LblFechaHasta.Enabled := True; DatFechaHasta.Enabled := True;
      LblRangoProveedor.Enabled := True;
      LblProveedorDesde.Enabled := True; CmbProveedorDesde.Enabled := True;
      LblProveedorHasta.Enabled := True; CmbProveedorHasta.Enabled := True;
      LblOtros.Enabled := True;
      LblZona.Enabled := True; CmbZona.Enabled := True;
      LblTipoReporte.Enabled := True;
      CmbTipoReporte.Enabled := True;
      CmbTipoReporte.Items.Clear;
      CmbTipoReporte.Items.Add('Detallado');
      CmbTipoReporte.Items.Add('Resumido');
      CmbTipoReporte.Items.Add('Analisis de Vencimiento Detallado');
      CmbTipoReporte.Items.Add('Analisis de Vencimiento Resumido');
      CmbTipoReporte.ItemIndex := 0;
    end;
    5: //Comisiones Vendedores
    begin
      LblPeriodo.Enabled := True;
      LblFechaAl.Enabled := False; DatFechaAl.Enabled := False;
      LblFechaDesde.Enabled := True; DatFechaDesde.Enabled := True;
      LblFechaHasta.Enabled := True; DatFechaHasta.Enabled := True;
      LblVendedor.Enabled := True;
      CmbVendedor.Enabled := True;
      LblOtros.Enabled := True;
      LblZona.Enabled := True; CmbZona.Enabled := True;
      LblTipoReporte.Enabled := True;
      CmbTipoReporte.Enabled := True;
      CmbTipoReporte.Items.Clear;
      CmbTipoReporte.Items.Add('Detallado');
      CmbTipoReporte.Items.Add('Resumido');
      CmbTipoReporte.ItemIndex := 0;
      LblClaseReporte.Enabled := False;
      CmbClaseReporte.Enabled := False;
      CmbClaseReporte.Items.Clear;
      CmbClaseReporte.Items.Add('Individual');
      CmbClaseReporte.Items.Add('Global');
      CmbClaseReporte.ItemIndex := 0;
    end;
    6: //Comisiones Vendedores por Pagar
    begin
      LblPeriodo.Enabled := True;
      LblFechaAl.Enabled := False; DatFechaAl.Enabled := False;
      LblFechaDesde.Enabled := True; DatFechaDesde.Enabled := True;
      LblFechaHasta.Enabled := True; DatFechaHasta.Enabled := True;
      LblVendedor.Enabled := True;
      CmbVendedor.Enabled := True;
      LblOtros.Enabled := True;
      LblZona.Enabled := True; CmbZona.Enabled := True;
      LblTipoReporte.Enabled := True;
      CmbTipoReporte.Enabled := True;
      CmbTipoReporte.Items.Clear;
      CmbTipoReporte.Items.Add('Detallado');
      CmbTipoReporte.Items.Add('Resumido');
      CmbTipoReporte.ItemIndex := 0;
      LblClaseReporte.Enabled := False;
      CmbClaseReporte.Enabled := False;
      CmbClaseReporte.Items.Clear;
      CmbClaseReporte.Items.Add('Individual');
      CmbClaseReporte.Items.Add('Global');
      CmbClaseReporte.ItemIndex := 0;
    end;
    7: //Productos Vendidos
    begin
      LblPeriodo.Enabled := True;
      LblFechaAl.Enabled := False; DatFechaAl.Enabled := False;
      LblFechaDesde.Enabled := True; DatFechaDesde.Enabled := True;
      LblFechaHasta.Enabled := True; DatFechaHasta.Enabled := True;
      LblRangoCliente.Enabled := True;
      LblClienteDesde.Enabled := True; CmbClienteDesde.Enabled := True;
      LblClienteHasta.Enabled := False; CmbClienteHasta.Enabled := False;
      LblRangoProducto.Enabled := True;
      LblProductoDesde.Enabled := True; CmbProductoDesde.Enabled := True;
      LblProductoHasta.Enabled := True; CmbProductoHasta.Enabled := True;
      LblClasificacionProducto.Enabled := True;
      LblClasif1.Enabled := True; CmbClasif1.Enabled := True;
      LblClasif2.Enabled := True; CmbClasif2.Enabled := True;
      LblClasif3.Enabled := True; CmbClasif3.Enabled := True;
      LblClasif4.Enabled := True; CmbClasif4.Enabled := True;
      LblClasif5.Enabled := True; CmbClasif5.Enabled := True;
      LblClasif6.Enabled := True; CmbClasif6.Enabled := True;
      LblOtros.Enabled := True;
      LblEstacion.Enabled := True; CmbEstacion.Enabled := True;
      LblZona.Enabled := True; CmbZona.Enabled := True;
      LblTipoReporte.Enabled := True;
      CmbTipoReporte.Enabled := True;
      CmbTipoReporte.Items.Clear;
      CmbTipoReporte.Items.Add('Detallado General');
      CmbTipoReporte.Items.Add('Detallado por Division');
      CmbTipoReporte.Items.Add('Detallado por Linea');
      CmbTipoReporte.Items.Add('Detallado por Familia');
      CmbTipoReporte.Items.Add('Detallado por Clase');
      CmbTipoReporte.Items.Add('Detallado por Manufactura');
      CmbTipoReporte.Items.Add('Resumido por Division');
      CmbTipoReporte.Items.Add('Resumido por Linea');
      CmbTipoReporte.Items.Add('Resumido por Familia');
      CmbTipoReporte.Items.Add('Resumido por Clase');
      CmbTipoReporte.Items.Add('Resumido por Manufactura');
      CmbTipoReporte.Items.Add('Resumido por Producto');
      CmbTipoReporte.ItemIndex := 0;
      LblClaseReporte.Enabled := True;
      CmbClaseReporte.Enabled := True;
      CmbClaseReporte.Items.Clear;
      CmbClaseReporte.Items.Add('Por Ventas');
      CmbClaseReporte.Items.Add('Por Devoluciones');
      CmbClaseReporte.Items.Add('Por Ventas y Devoluciones');
      CmbClaseReporte.ItemIndex := 0;
    end;
    8: //Productos Comprados
    begin
      LblPeriodo.Enabled := True;
      LblFechaAl.Enabled := False; DatFechaAl.Enabled := False;
      LblFechaDesde.Enabled := True; DatFechaDesde.Enabled := True;
      LblFechaHasta.Enabled := True; DatFechaHasta.Enabled := True;
      LblRangoProveedor.Enabled := True;
      LblProveedorDesde.Enabled := True; CmbProveedorDesde.Enabled := True;
      LblProveedorHasta.Enabled := False; CmbProveedorHasta.Enabled := False;
      LblRangoProducto.Enabled := True;
      LblProductoDesde.Enabled := True; CmbProductoDesde.Enabled := True;
      LblProductoHasta.Enabled := True; CmbProductoHasta.Enabled := True;
      LblClasificacionProducto.Enabled := True;
      LblClasif1.Enabled := True; CmbClasif1.Enabled := True;
      LblClasif2.Enabled := True; CmbClasif2.Enabled := True;
      LblClasif3.Enabled := True; CmbClasif3.Enabled := True;
      LblClasif4.Enabled := True; CmbClasif4.Enabled := True;
      LblClasif5.Enabled := True; CmbClasif5.Enabled := True;
      LblClasif6.Enabled := True; CmbClasif6.Enabled := True;
      LblOtros.Enabled := True;
      LblEstacion.Enabled := True; CmbEstacion.Enabled := True;
      LblZona.Enabled := True; CmbZona.Enabled := True;
      LblTipoReporte.Enabled := True;
      CmbTipoReporte.Enabled := True;
      CmbTipoReporte.Items.Clear;
      CmbTipoReporte.Items.Add('Detallado General');
      CmbTipoReporte.Items.Add('Detallado por Division');
      CmbTipoReporte.Items.Add('Detallado por Linea');
      CmbTipoReporte.Items.Add('Detallado por Familia');
      CmbTipoReporte.Items.Add('Detallado por Clase');
      CmbTipoReporte.Items.Add('Detallado por Manufactura');
      CmbTipoReporte.Items.Add('Resumido por Division');
      CmbTipoReporte.Items.Add('Resumido por Linea');
      CmbTipoReporte.Items.Add('Resumido por Familia');
      CmbTipoReporte.Items.Add('Resumido por Clase');
      CmbTipoReporte.Items.Add('Resumido por Manufactura');
      CmbTipoReporte.Items.Add('Resumido por Producto');
      CmbTipoReporte.ItemIndex := 0;
      LblClaseReporte.Enabled := False;
      CmbClaseReporte.Enabled := False;
    end;
    9://Libro de Compras
    begin
      LblPeriodo.Enabled := True;
      LblFechaAl.Enabled := False; DatFechaAl.Enabled := False;
      LblFechaDesde.Enabled := True; DatFechaDesde.Enabled := True;
      LblFechaHasta.Enabled := True; DatFechaHasta.Enabled := True;
      LblRangoCliente.Enabled := False;
      LblClienteDesde.Enabled := False; CmbClienteDesde.Enabled := False;
      LblClienteHasta.Enabled := False; CmbClienteHasta.Enabled := False;
      LblRangoProveedor.Enabled := False;
      LblProveedorDesde.Enabled := False; CmbProveedorDesde.Enabled := False;
      LblProveedorHasta.Enabled := False; CmbProveedorHasta.Enabled := False;
      LblRangoProducto.Enabled := False;
      LblProductoDesde.Enabled := False; CmbProductoDesde.Enabled := False;
      LblProductoHasta.Enabled := False; CmbProductoHasta.Enabled := False;
      LblClasificacionProducto.Enabled := False;
      LblClasif1.Enabled := False; CmbClasif1.Enabled := False;
      LblClasif2.Enabled := False; CmbClasif2.Enabled := False;
      LblClasif3.Enabled := False; CmbClasif3.Enabled := False;
      LblClasif4.Enabled := False; CmbClasif4.Enabled := False;
      LblClasif5.Enabled := False; CmbClasif5.Enabled := False;
      LblClasif6.Enabled := False; CmbClasif6.Enabled := False;
      LblOtros.Enabled := True;
      LblZona.Enabled := True; CmbZona.Enabled := True;
      LblTipoReporte.Enabled := True;
      CmbTipoReporte.Enabled := True;
      CmbTipoReporte.Items.Clear;
      CmbTipoReporte.Items.Add('Ordenado Por Fecha');
      CmbTipoReporte.Items.Add('Ordenado Por Tipo de Documento');
      CmbTipoReporte.ItemIndex := 0;
      LblClaseReporte.Enabled := False;
      CmbClaseReporte.Enabled := False;
    end;
    10://Libro de Ventas
    begin
      LblPeriodo.Enabled := True;
      LblFechaAl.Enabled := False; DatFechaAl.Enabled := False;
      LblFechaDesde.Enabled := True; DatFechaDesde.Enabled := True;
      LblFechaHasta.Enabled := True; DatFechaHasta.Enabled := True;
      LblRangoCliente.Enabled := False;
      LblClienteDesde.Enabled := False; CmbClienteDesde.Enabled := False;
      LblClienteHasta.Enabled := False; CmbClienteHasta.Enabled := False;
      LblRangoProveedor.Enabled := False;
      LblProveedorDesde.Enabled := False; CmbProveedorDesde.Enabled := False;
      LblProveedorHasta.Enabled := False; CmbProveedorHasta.Enabled := False;
      LblRangoProducto.Enabled := False;
      LblProductoDesde.Enabled := False; CmbProductoDesde.Enabled := False;
      LblProductoHasta.Enabled := False; CmbProductoHasta.Enabled := False;
      LblClasificacionProducto.Enabled := False;
      LblClasif1.Enabled := False; CmbClasif1.Enabled := False;
      LblClasif2.Enabled := False; CmbClasif2.Enabled := False;
      LblClasif3.Enabled := False; CmbClasif3.Enabled := False;
      LblClasif4.Enabled := False; CmbClasif4.Enabled := False;
      LblClasif5.Enabled := False; CmbClasif5.Enabled := False;
      LblClasif6.Enabled := False; CmbClasif6.Enabled := False;
      LblOtros.Enabled := True;
      LblZona.Enabled := True; CmbZona.Enabled := True;
      LblTipoReporte.Enabled := True;
      CmbTipoReporte.Enabled := True;
      CmbTipoReporte.Items.Clear;
      CmbTipoReporte.Items.Add('Ordenado Por Fecha');
      CmbTipoReporte.Items.Add('Ordenado Por Tipo de Documento');
      CmbTipoReporte.ItemIndex := 0;
      LblClaseReporte.Enabled := False;
      CmbClaseReporte.Enabled := False;
    end;
    11: //Retencion I.V.A (Compras y Ventas)
    begin
      LblPeriodo.Enabled := True;
      LblFechaAl.Enabled := False; DatFechaAl.Enabled := False;
      LblFechaDesde.Enabled := True; DatFechaDesde.Enabled := True;
      LblFechaHasta.Enabled := True; DatFechaHasta.Enabled := True;
      LblRangoCliente.Enabled := False;
      LblClienteDesde.Enabled := False; CmbClienteDesde.Enabled := False;
      LblClienteHasta.Enabled := False; CmbClienteHasta.Enabled := False;
      LblRangoProveedor.Enabled := True;
      LblProveedorDesde.Enabled := True; CmbProveedorDesde.Enabled := True;
      LblProveedorHasta.Enabled := True; CmbProveedorHasta.Enabled := True;
      LblRangoProducto.Enabled := False;
      LblProductoDesde.Enabled := False; CmbProductoDesde.Enabled := False;
      LblProductoHasta.Enabled := False; CmbProductoHasta.Enabled := False;
      LblClasificacionProducto.Enabled := False;
      LblClasif1.Enabled := False; CmbClasif1.Enabled := False;
      LblClasif2.Enabled := False; CmbClasif2.Enabled := False;
      LblClasif3.Enabled := False; CmbClasif3.Enabled := False;
      LblClasif4.Enabled := False; CmbClasif4.Enabled := False;
      LblClasif5.Enabled := False; CmbClasif5.Enabled := False;
      LblClasif6.Enabled := False; CmbClasif6.Enabled := False;
      LblTipoReporte.Enabled := True;
      CmbTipoReporte.Enabled := True;
      CmbTipoReporte.Items.Clear;
      CmbTipoReporte.Items.Add('Retenci�n I.V.A (Compras y Ventas)');
      CmbTipoReporte.Items.Add('Retenci�n I.V.A (Compras)');
      CmbTipoReporte.Items.Add('Retenci�n I.V.A (Ventas)');
      CmbTipoReporte.Items.Add('Resumen de D�bitos y Cr�ditos');
      CmbTipoReporte.ItemIndex := 0;
      LblTipoOrdenamiento.Enabled := True;
      CmbTipoOrdenamiento.Enabled := True;
      CmbTipoOrdenamiento.Items.Clear;
      CmbTipoOrdenamiento.Items.Add('Fecha Documento');
      CmbTipoOrdenamiento.Items.Add('Fecha Contable');
      CmbTipoOrdenamiento.Items.Add('Fecha Pago');
      CmbTipoOrdenamiento.Items.Add('Consecutivo');
      CmbTipoOrdenamiento.ItemIndex := 0;
      LblClaseReporte.Enabled := False;
      CmbClaseReporte.Enabled := False;
    end;
    12://Impuesto Retenidos a Proveedores
    begin
      LblPeriodo.Enabled := True;
      LblFechaAl.Enabled := False; DatFechaAl.Enabled := False;
      LblFechaDesde.Enabled := True; DatFechaDesde.Enabled := True;
      LblFechaHasta.Enabled := True; DatFechaHasta.Enabled := True;
      LblRangoProveedor.Enabled := True;
      LblProveedorDesde.Enabled := True; CmbProveedorDesde.Enabled := True;
      LblProveedorHasta.Enabled := True; CmbProveedorHasta.Enabled := True;
      LblOtros.Enabled := True;
      LblZona.Enabled := True;
      CmbZona.Enabled := True;
      LblTipoReporte.Enabled := True;
      CmbTipoReporte.Enabled := True;
      CmbTipoReporte.Items.Clear;
      CmbTipoReporte.Items.Add('Por Tipo de Retenci�n');
      CmbTipoReporte.Items.Add('Por Proveedor');
      //CmbTipoReporte.Items.Add('Resumen Anual de Retenciones');
      CmbTipoReporte.ItemIndex := 0;
      LblTipoOrdenamiento.Enabled := True;
      CmbTipoOrdenamiento.Enabled := True;
      CmbTipoOrdenamiento.Items.Clear;
      CmbTipoOrdenamiento.Items.Add('Fecha Pago');
      CmbTipoOrdenamiento.Items.Add('Fecha Contable');
      CmbTipoOrdenamiento.ItemIndex := 0;
    end;
    13://Movimientos de Inventarios
    begin
      LblPeriodo.Enabled := True;
      LblFechaAl.Enabled := False; DatFechaAl.Enabled := False;
      LblFechaDesde.Enabled := True; DatFechaDesde.Enabled := True;
      LblFechaHasta.Enabled := True; DatFechaHasta.Enabled := True;
      LblRangoProducto.Enabled := True;
      LblProductoDesde.Enabled := True; CmbProductoDesde.Enabled := True;
      LblProductoHasta.Enabled := True; CmbProductoHasta.Enabled := True;
      LblClasificacionProducto.Enabled := True;
      LblClasif1.Enabled := True; CmbClasif1.Enabled := True;
      LblClasif2.Enabled := True; CmbClasif2.Enabled := True;
      LblClasif3.Enabled := True; CmbClasif3.Enabled := True;
      LblClasif4.Enabled := True; CmbClasif4.Enabled := True;
      LblClasif5.Enabled := True; CmbClasif5.Enabled := True;
      LblClasif6.Enabled := True; CmbClasif6.Enabled := True;
      LblOtros.Enabled := True;
      LblEstacion.Enabled := True; CmbEstacion.Enabled := True;
      LblZona.Enabled := True; CmbZona.Enabled := True;
      LblTipoReporte.Enabled := True;
      CmbTipoReporte.Enabled := True;
      CmbTipoReporte.Items.Clear;
      CmbTipoReporte.Items.Add('Detallado por Producto');
      CmbTipoReporte.Items.Add('Detallado por Division');
      CmbTipoReporte.Items.Add('Detallado por Linea');
      CmbTipoReporte.Items.Add('Detallado por Familia');
      CmbTipoReporte.Items.Add('Detallado por Clase');
      CmbTipoReporte.Items.Add('Detallado por Manufactura');
      CmbTipoReporte.Items.Add('Resumido por Division');
      CmbTipoReporte.Items.Add('Resumido por Linea');
      CmbTipoReporte.Items.Add('Resumido por Familia');
      CmbTipoReporte.Items.Add('Resumido por Clase');
      CmbTipoReporte.Items.Add('Resumido por Manufactura');
      CmbTipoReporte.Items.Add('Resumido por Producto');
      CmbTipoReporte.ItemIndex := 0;
      LblClaseReporte.Enabled := False;
      CmbClaseReporte.Enabled := False;
    end;
    14://Existencias / Libro Inverntario
    begin
      LblPeriodo.Enabled := True;
      LblFechaDesde.Enabled := True; DatFechaDesde.Enabled := True;
      LblFechaHasta.Enabled := True; DatFechaHasta.Enabled := True;
      LblRangoProducto.Enabled := True;
      LblProductoDesde.Enabled := True; CmbProductoDesde.Enabled := True;
      LblProductoHasta.Enabled := True; CmbProductoHasta.Enabled := True;
      LblClasificacionProducto.Enabled := True;
      LblClasif1.Enabled := True; CmbClasif1.Enabled := True;
      LblClasif2.Enabled := True; CmbClasif2.Enabled := True;
      LblClasif3.Enabled := True; CmbClasif3.Enabled := True;
      LblClasif4.Enabled := True; CmbClasif4.Enabled := True;
      LblClasif5.Enabled := True; CmbClasif5.Enabled := True;
      LblClasif6.Enabled := True; CmbClasif6.Enabled := True;
      LblOtros.Enabled := True;
      LblEstacion.Enabled := True; CmbEstacion.Enabled := True;
      LblTipoReporte.Enabled := True;
      CmbTipoReporte.Enabled := True;
      CmbTipoReporte.Items.Clear;
      CmbTipoReporte.Items.Add('Detallado por Producto');
      CmbTipoReporte.Items.Add('Detallado por Division');
      CmbTipoReporte.Items.Add('Detallado por Linea');
      CmbTipoReporte.Items.Add('Detallado por Familia');
      CmbTipoReporte.Items.Add('Detallado por Clase');
      CmbTipoReporte.Items.Add('Detallado por Manufactura');
      CmbTipoReporte.Items.Add('Resumido por Producto');
      CmbTipoReporte.Items.Add('Resumido por Division');
      CmbTipoReporte.Items.Add('Resumido por Linea');
      CmbTipoReporte.Items.Add('Resumido por Familia');
      CmbTipoReporte.Items.Add('Resumido por Clase');
      CmbTipoReporte.Items.Add('Resumido por Manufactura');
      CmbTipoReporte.ItemIndex := 0;
      LblClaseReporte.Enabled := False;
      CmbClaseReporte.Enabled := False;
    end;
  end;
  Application.ProcessMessages;
  SetAutoDropDownWidth(CmbTipoReporte);
  SetAutoDropDownWidth(CmbClaseReporte);
end;

procedure TFrmReporteGeneral.Inicializar;
begin
  GrpListadoReportes.ItemIndex := 0;
  GrpListadoReportesClick(nil);
  LblTipoOrdenamiento.Caption := 'Tipo Ordenamiento:';  
end;

procedure TFrmReporteGeneral.LblClasif1DblClick(Sender: TObject);
begin
  Buscador(Self,Division,'codigo','Buscar Division',nil,DataClasif1);
  CmbClasif1.KeyValue := DataClasif1.FieldValues['codigo'];
end;

procedure TFrmReporteGeneral.LblClasif2DblClick(Sender: TObject);
begin
  Buscador(Self,Linea,'codigo','Buscar Linea',nil,DataClasif2);
  CmbClasif2.KeyValue := DataClasif2.FieldValues['codigo'];
end;

procedure TFrmReporteGeneral.LblClasif3DblClick(Sender: TObject);
begin
  Buscador(Self,Familia,'codigo','Buscar Familia',nil,DataClasif3);
  CmbClasif3.KeyValue := DataClasif3.FieldValues['codigo'];
end;

procedure TFrmReporteGeneral.LblClasif4DblClick(Sender: TObject);
begin
  Buscador(Self,Clase,'codigo','Buscar Clase',nil,DataClasif4);
  CmbClasif4.KeyValue := DataClasif4.FieldValues['codigo'];
end;

procedure TFrmReporteGeneral.LblClasif5DblClick(Sender: TObject);
begin
  Buscador(Self,Manufactura,'codigo','Buscar Manufactura',nil,DataClasif5);
  CmbClasif5.KeyValue := DataClasif5.FieldValues['codigo'];
end;

procedure TFrmReporteGeneral.LblClasif6DblClick(Sender: TObject);
begin
  Buscador(Self,Arancel,'codigo','Buscar Arancel',nil,DataClasif6);
  CmbClasif6.KeyValue := DataClasif6.FieldValues['codigo'];
end;

procedure TFrmReporteGeneral.LblClasificacionProductoDblClick(Sender: TObject);
begin
  CmbClasif1.KeyValue := '';
  CmbClasif2.KeyValue := '';
  CmbClasif3.KeyValue := '';
  CmbClasif4.KeyValue := '';
  CmbClasif5.KeyValue := '';
  CmbClasif6.KeyValue := '';
  //Activando Controles de Rango de Productos
  LblRangoProducto.Enabled := True;
  LblProductoDesde.Enabled := True; CmbProductoDesde.Enabled := True;
  LblProductoHasta.Enabled := True; CmbProductoHasta.Enabled := True;

end;

procedure TFrmReporteGeneral.LblClienteDesdeDblClick(Sender: TObject);
begin
  if (Buscador(Self,Cliente,'codigo','Buscar Cliente',nil,DataClienteDesde)) then
  begin
    CmbClienteDesde.KeyValue := DataClienteDesde.FieldValues['codigo'];
    CmbClienteDesdeCloseUp(Sender);
  end;
end;

procedure TFrmReporteGeneral.LblClienteHastaDblClick(Sender: TObject);
begin
  if (Buscador(Self,Cliente,'codigo','Buscar Cliente',nil,DataClienteHasta)) then
  begin
    CmbClienteHasta.KeyValue := DataClienteHasta.FieldValues['codigo'];
  end;
end;

procedure TFrmReporteGeneral.LblCuentaBancariaDblClick(Sender: TObject);
begin
  if (Buscador(Self,CuentaBancaria,'codigo','Buscar Cuenta Bancaria',nil,DataCuentaBancaria)) then
  begin
    CmbCuentaBancaria.KeyValue := DataCuentaBancaria.FieldValues['codigo'];
  end;
end;

procedure TFrmReporteGeneral.LblEstacionDblClick(Sender: TObject);
begin
  if (Buscador(Self,Estacion,'codigo','Buscar Estacion',nil,DataEstacion)) then
  begin
    CmbEstacion.KeyValue := DataEstacion.FieldValues['codigo'];
  end;
end;

procedure TFrmReporteGeneral.LblMonedaDblClick(Sender: TObject);
begin
  if (Buscador(Self,Moneda,'codigo','Buscar Moneda',nil,DataMoneda)) then
  begin
    CmbMoneda.KeyValue := DataMoneda.FieldValues['codigo'];
  end;
end;

procedure TFrmReporteGeneral.LblOtrosDblClick(Sender: TObject);
begin
  CmbEstacion.KeyValue := '';
  CmbZona.KeyValue := '';
  CmbMoneda.KeyValue := '';
  CmbCuentaBancaria.KeyValue := '';
  CmbVendedor.KeyValue := '';
end;

procedure TFrmReporteGeneral.LblPeriodoDblClick(Sender: TObject);
begin
  DatFechaAl.Date := Date;
  DatFechaDesde.Date := Date;
  DatFechaHasta.Date := Date;
end;

procedure TFrmReporteGeneral.LblProductoDesdeDblClick(Sender: TObject);
begin
  if (Buscador(Self,Producto,'codigo','Buscar Producto',nil,DataProductoDesde)) then
  begin
    CmbProductoDesde.KeyValue := DataProductoDesde.FieldValues['codigo'];
    CmbProductoDesdeCloseUp(Sender);
  end;
end;

procedure TFrmReporteGeneral.LblProductoHastaDblClick(Sender: TObject);
begin
  if (Buscador(Self,Producto,'codigo','Buscar Producto',nil,DataProductoHasta)) then
  begin
    CmbProductoHasta.KeyValue := DataProductoHasta.FieldValues['codigo'];
  end;
end;

procedure TFrmReporteGeneral.LblProveedorDesdeDblClick(Sender: TObject);
begin
  if (Buscador(Self,Proveedor,'codigo','Buscar Proveedor',nil,DataProveedorDesde)) then
  begin
    CmbProveedorDesde.KeyValue := DataProveedorDesde.FieldValues['codigo'];
    CmbProveedorDesdeCloseUp(Sender);
  end;
end;

procedure TFrmReporteGeneral.LblProveedorHastaDblClick(Sender: TObject);
begin
  if (Buscador(Self,Proveedor,'codigo','Buscar Proveedor',nil,DataProveedorHasta)) then
  begin
    CmbProveedorHasta.KeyValue := DataProveedorHasta.FieldValues['codigo'];
  end;
end;

procedure TFrmReporteGeneral.LblRangoClienteDblClick(Sender: TObject);
begin
  CmbClienteDesde.KeyValue := '';
  CmbClienteHasta.KeyValue := '';
end;

procedure TFrmReporteGeneral.LblRangoProductoDblClick(Sender: TObject);
begin
  CmbProductoDesde.KeyValue := '';
  CmbProductoHasta.KeyValue := '';
end;

procedure TFrmReporteGeneral.LblRangoProveedorDblClick(Sender: TObject);
begin
  CmbProveedorDesde.KeyValue := '';
  CmbProveedorHasta.KeyValue := '';
end;

procedure TFrmReporteGeneral.LblVendedorDblClick(Sender: TObject);
begin
  if (Buscador(Self,Vendedor,'codigo','Buscar Vendedor',nil,DataVendedor)) then
  begin
    CmbVendedor.KeyValue := DataVendedor.FieldValues['codigo'];
  end;
end;

procedure TFrmReporteGeneral.LblZonaDblClick(Sender: TObject);
begin
  if (Buscador(Self,Zona,'codigo','Buscar Zona',nil,DataZona)) then
  begin
    CmbZona.KeyValue := DataZona.FieldValues['codigo'];
  end;
end;

procedure TFrmReporteGeneral.CargarParametros;
var
  TituloReporte: String;
  TipoReporte: String;
  ClaseReporte: String;
  TipoOrdenamiento: String;
begin
  with Reporte do
  begin
    AgregarParametro('LoginUsuario',User.GetLoginUsuario);
    AgregarParametro('NombreUsuario',User.GetNombres + ', ' + User.GetApellidos);
    AgregarParametro('TituloReporte','');
    AgregarParametro('FechaAl',UpperCase(FormatDateTime('yyyy-mm-dd',DatFechaAl.Date)));
    AgregarParametro('FechaDesde',UpperCase(FormatDateTime('yyyy-mm-dd',DatFechaDesde.Date)));
    AgregarParametro('FechaHasta',UpperCase(FormatDateTime('yyyy-mm-dd',DatFechaHasta.Date)));
    AgregarParametro('Periodo','Desde ' + FormatDateTime('dd/mm/yyyy',DatFechaDesde.Date) + ' Hasta ' + FormatDateTime('dd/mm/yyyy',DatFechaHasta.Date));
  end;
  case GrpListadoReportes.ItemIndex of
    0: // Balance de Caja (Ingreso)
    begin
      TituloReporte := 'Balance de Caja (Ingresos)';
      TipoReporte := CmbTipoReporte.Text;
      Case CmbTipoReporte.ItemIndex of
        0:
        begin
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        1:
        begin
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
      end;
    end;
    1: // Balance de Caja (Egresos)
    begin
      TituloReporte := 'Balance de Caja (Egresos)';
      Case CmbTipoReporte.ItemIndex of
        0:
        begin
          Reporte.AgregarParametro('FechaAl',FormatDateTime('YYYY-mm-dd',DatFechaAl.Date));
          Reporte.AgregarParametro('Periodo','Al ' + DatFechaAl.Text);
        end;
        1:
        begin
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
      end;
    end;
    2: // Estado de Cuentas Bancarios
    begin
      TituloReporte := 'Estados de Cuentas Bancarios';
      Case CmbTipoReporte.ItemIndex of
        0:
        begin
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        1:
        begin
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
      end;
    end;
    3: // Estado de Cuenta de Clientes
    begin
      TituloReporte := 'Estado de Cuenta de Clientes';
      Reporte.AgregarParametro('ClienteDesde',CmbClienteDesde.Text);
      Reporte.AgregarParametro('ClienteHasta',CmbClienteHasta.Text);
      Case CmbTipoReporte.ItemIndex of
        0:
        begin
          TipoReporte := 'Detallado';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        1:
        begin
          TipoReporte := 'Resumido';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        2:
        begin
          TipoReporte := 'Analisis de Vencimiento Detallado';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('FechaAl',FormatDateTime('YYYY-mm-dd',DatFechaAl.Date));
          Reporte.AgregarParametro('Periodo','Al ' + DatFechaAl.Text);
        end;
        3:
        begin
          TipoReporte := 'Analisis de Vencimiento Resumido';
          Reporte.AgregarParametro('FechaAl',FormatDateTime('YYYY-mm-dd',DatFechaAl.Date));
          Reporte.AgregarParametro('Periodo','Al ' + DatFechaAl.Text);
        end;
        4:
        begin
          TipoReporte := 'Analisis de Vencimiento Detallado (Modelo D�as)';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('FechaAl',FormatDateTime('YYYY-mm-dd',DatFechaAl.Date));
          Reporte.AgregarParametro('Periodo','Al ' + DatFechaAl.Text);
        end;
        5:
        begin
          TipoReporte := 'Analisis de Vencimiento Resumido (Modelo D�as)';
          Reporte.AgregarParametro('FechaAl',FormatDateTime('YYYY-mm-dd',DatFechaAl.Date));
          Reporte.AgregarParametro('Periodo','Al ' + DatFechaAl.Text);
        end;
      end;
    end;
    4: // Estado de Cuenta de Proveedores
    begin
      TituloReporte := 'Estado de Cuenta de Proveedores';
      Reporte.AgregarParametro('ProveedorDesde',CmbProveedorDesde.Text);
      Reporte.AgregarParametro('ProveedorHasta',CmbProveedorHasta.Text);
      Case CmbTipoReporte.ItemIndex of
        0:
        begin
          TipoReporte := 'Detallado';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        1:
        begin
          TipoReporte := 'Resumido';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        2:
        begin
          TipoReporte := 'Analisis de Vencimiento Detallado';
          Reporte.AgregarParametro('FechaAl',FormatDateTime('YYYY-mm-dd',DatFechaAl.Date));
          Reporte.AgregarParametro('Periodo','Al ' + DatFechaAl.Text);
        end;
        3:
        begin
          TipoReporte := 'Analisis de Vencimiento Resumido';
          Reporte.AgregarParametro('FechaAl',FormatDateTime('YYYY-mm-dd',DatFechaAl.Date));
          Reporte.AgregarParametro('Periodo','Al ' + DatFechaAl.Text);
        end;
      end;
    end;
    5:  //Reporte Comisiones Vendedores
    begin
      TituloReporte := 'Reporte de Comisiones';
      if (CmbVendedor.Text = '') then
      begin
        Reporte.AgregarParametro('VendedorDesde','TODOS');
      end
      else
      begin
        Reporte.AgregarParametro('VendedorDesde',CmbVendedor.Text);
      end;
      if (CmbZona.Text = '')  then
      begin
        Reporte.AgregarParametro('Zona','TODAS');
      end
      else
      begin
        Reporte.AgregarParametro('Zona',CmbZona.Text);
      end;
      Case CmbTipoReporte.ItemIndex of
        0:
        begin
          TipoReporte := 'Detallado';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        1:
        begin
          TipoReporte := 'Resumido';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
      end;
    end;
    6: //Reporte Comisiones por Pagar Vendedores
    begin
      TituloReporte := 'Reporte Comisiones por Pagar Vendedores';
      if (CmbVendedor.Text = '') then
      begin
        Reporte.AgregarParametro('VendedorDesde','TODOS');
      end
      else
      begin
        Reporte.AgregarParametro('VendedorDesde',CmbVendedor.Text + ' ' + DataVendedor.FieldValues['descripcion']);
      end;
      if (CmbZona.Text = '')  then
      begin
        Reporte.AgregarParametro('Zona','TODAS');
      end
      else
      begin
        Reporte.AgregarParametro('Zona',CmbZona.Text + ' ' + DataZona.FieldValues['descripcion']);
      end;
      Case CmbTipoReporte.ItemIndex of
        0:
        begin
          TipoReporte := 'Detallado';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        1:
        begin
          TipoReporte := 'Resumido';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
      end;
    end;
    7: //Reporte Productos Vendidos
    begin
      TituloReporte := 'PRODUCTOS VENDIDOS';
      //Evaluando Parametros de Cabecera
      if (CmbZona.Text <> '') then
      begin
        Reporte.AgregarParametro('Zona', CmbZona.Text);
      end
      else
      begin
        Reporte.AgregarParametro('Zona', 'TODAS');
      end;
      if (CmbEstacion.Text <> '') then
      begin
        Reporte.AgregarParametro('Estacion', CmbEstacion.Text);
      end
      else
      begin
        Reporte.AgregarParametro('Estacion', 'TODAS');
      end;
      if (CmbClienteDesde.Text <> '') then
      begin
        Reporte.AgregarParametro('ClienteDesde', CmbClienteDesde.Text);
      end
      else
      begin
        Reporte.AgregarParametro('ClienteDesde', 'TODOS');
      end;
      if (CmbMoneda.Text <> '') then
      begin
        Reporte.AgregarParametro('Moneda', CmbMoneda.Text);
      end
      else
      begin
        Reporte.AgregarParametro('Moneda', 'VEB');
      end;
      if (CmbClasif1.Text <> '') then
      begin
        Reporte.AgregarParametro('Division', CmbClasif1.Text);
      end
      else
      begin
        Reporte.AgregarParametro('Division', 'TODAS');
      end;
      if (CmbClasif2.Text <> '') then
      begin
        Reporte.AgregarParametro('Linea', CmbClasif2.Text);
      end
      else
      begin
        Reporte.AgregarParametro('Linea', 'TODAS');
      end;
      if (CmbClasif3.Text <> '') then
      begin
        Reporte.AgregarParametro('Familia', CmbClasif3.Text);
      end
      else
      begin
        Reporte.AgregarParametro('Familia', 'TODAS');
      end;
      if (CmbClasif4.Text <> '') then
      begin
        Reporte.AgregarParametro('Clase', CmbClasif4.Text);
      end
      else
      begin
        Reporte.AgregarParametro('Clase', 'TODAS');
      end;
      if (CmbClasif5.Text <> '') then
      begin
        Reporte.AgregarParametro('Manufactura', CmbClasif5.Text);
      end
      else
      begin
        Reporte.AgregarParametro('Manufactura', 'TODAS');
      end;
      if (CmbClasif6.Text <> '') then
      begin
        Reporte.AgregarParametro('Producto', CmbClasif6.Text);
      end
      else
      begin
        Reporte.AgregarParametro('Producto', 'TODOS');
      end;
      //Determinando Clase del Reporte
      case CmbClaseReporte.ItemIndex of
        0: // Solo Ventas
        begin
          ClaseReporte := 'Solo Ventas';
        end;
        1: // Solo Devoluciones
        begin
          ClaseReporte := 'Solo Devoluciones';
        end;
        2: // Ventas y Devoluciones
        begin
          ClaseReporte := 'Ventas y Devoluciones';
        end;
      end;
      Case CmbTipoReporte.ItemIndex of
        0: //Reporte Productos Vendidos  Detallado General
        begin
          TipoReporte := 'Detallado General';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        1: //Reporte Productos Vendidos Detallado Division
        begin
          TipoReporte := 'Detallado Division';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        2: //Reporte Productos Vendidos Detallado Linea
        begin
          TipoReporte := 'Detallado Linea';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        3: //Reporte Productos Vendidos Detallado Familia
        begin
          TipoReporte := 'Detallado Familia';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        4: //Reporte Productos Vendidos Detallado Clase
        begin
          TipoReporte := 'Detallado Clase';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        5: //Reporte Productos Vendidos Detallado Manufactura
        begin
          TipoReporte := 'Detallado Manufactura';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        6: //Reporte Productos Vendidos Resumido Division
        begin
          TipoReporte := 'Resumido Division';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        7: //Reporte Productos Vendidos Resumido Linea
        begin
          TipoReporte := 'Resumido Linea';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        8: //Reporte Productos Vendidos Resumido Familia
        begin
          TipoReporte := 'Resumido Familia';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        9: //Reporte Productos Vendidos Resumido Clase
        begin
          TipoReporte := 'Resumido Clase';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        10: //Reporte Productos Vendidos Resumido Manufactura
        begin
          TipoReporte := 'Resumido Manufactura';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        11: //Reporte Productos Vendidos Resumido Producto
        begin
          TipoReporte := 'Resumido por Producto';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
      end;
    end;
    8: // Reporte Productos Comprados
    begin
      TituloReporte := 'PRODUCTOS COMPRADOS';
      //Evaluando Parametros de Cabecera
      if (CmbZona.Text <> '') then
      begin
        Reporte.AgregarParametro('Zona', CmbZona.Text);
      end
      else
      begin
        Reporte.AgregarParametro('Zona', 'TODAS');
      end;
      if (CmbEstacion.Text <> '') then
      begin
        Reporte.AgregarParametro('Estacion', CmbEstacion.Text);
      end
      else
      begin
        Reporte.AgregarParametro('Estacion', 'TODAS');
      end;
      if (CmbProveedorDesde.Text <> '') then
      begin
        Reporte.AgregarParametro('ProveedorDesde', CmbProveedorDesde.Text);
      end
      else
      begin
        Reporte.AgregarParametro('ProveedorDesde', 'TODOS');
      end;
      if (CmbMoneda.Text <> '') then
      begin
        Reporte.AgregarParametro('Moneda', CmbMoneda.Text);
      end
      else
      begin
        Reporte.AgregarParametro('Moneda', 'VEB');
      end;
      if (CmbClasif1.Text <> '') then
      begin
        Reporte.AgregarParametro('Division', CmbClasif1.Text);
      end
      else
      begin
        Reporte.AgregarParametro('Division', 'TODAS');
      end;
      if (CmbClasif2.Text <> '') then
      begin
        Reporte.AgregarParametro('Linea', CmbClasif2.Text);
      end
      else
      begin
        Reporte.AgregarParametro('Linea', 'TODAS');
      end;
      if (CmbClasif3.Text <> '') then
      begin
        Reporte.AgregarParametro('Familia', CmbClasif3.Text);
      end
      else
      begin
        Reporte.AgregarParametro('Familia', 'TODAS');
      end;
      if (CmbClasif4.Text <> '') then
      begin
        Reporte.AgregarParametro('Clase', CmbClasif4.Text);
      end
      else
      begin
        Reporte.AgregarParametro('Clase', 'TODAS');
      end;
      if (CmbClasif5.Text <> '') then
      begin
        Reporte.AgregarParametro('Manufactura', CmbClasif5.Text);
      end
      else
      begin
        Reporte.AgregarParametro('Manufactura', 'TODAS');
      end;
      if (CmbClasif6.Text <> '') then
      begin
        Reporte.AgregarParametro('Producto', CmbClasif6.Text);
      end
      else
      begin
        Reporte.AgregarParametro('Producto', 'TODOS');
      end;
      Case CmbTipoReporte.ItemIndex of
        0: //Reporte Productos Vendidos  Detallado General
        begin
          TipoReporte := 'Detallado General';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        1: //Reporte Productos Vendidos Detallado Division
        begin
          TipoReporte := 'Detallado Division';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        2: //Reporte Productos Vendidos Detallado Linea
        begin
          TipoReporte := 'Detallado Linea';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        3: //Reporte Productos Vendidos Detallado Familia
        begin
          TipoReporte := 'Detallado Familia';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        4: //Reporte Productos Vendidos Detallado Clase
        begin
          TipoReporte := 'Detallado Clase';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        5: //Reporte Productos Vendidos Detallado Manufactura
        begin
          TipoReporte := 'Detallado Manufactura';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        6: //Reporte Productos Vendidos Resumido Division
        begin
          TipoReporte := 'Resumido Division';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        7: //Reporte Productos Vendidos Resumido Linea
        begin
          TipoReporte := 'Resumido Linea';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        8: //Reporte Productos Vendidos Resumido Familia
        begin
          TipoReporte := 'Resumido Familia';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        9: //Reporte Productos Vendidos Resumido Clase
        begin
          TipoReporte := 'Resumido Clase';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        10: //Reporte Productos Vendidos Resumido Manufactura
        begin
          TipoReporte := 'Resumido Manufactura';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        11: //Reporte Productos Vendidos Resumido Producto
        begin
          TipoReporte := 'Resumido por Producto';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
      end;
    end;
    9://Libro Compras
    begin
      TipoReporte := 'LIBRO DE COMPRAS';
      Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
      Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
      Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
    end;
    10://Libro Ventas
    begin
      TipoReporte := 'LIBRO DE VENTAS';
      Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
      Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
      Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
    end;
    11://Retenicion IVA (Compras - Ventas)
    begin
      Reporte.AgregarParametro('ProveedorDesde',CmbProveedorDesde.Text);
      Reporte.AgregarParametro('ProveedorHasta',CmbProveedorHasta.Text);
      Reporte.AgregarParametro('ClienteDesde',CmbClienteDesde.Text);
      Reporte.AgregarParametro('ClienteHasta',CmbClienteHasta.Text);
      Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
      Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
      Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
      Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
      if (CmbProveedorDesde.Text = '') then
      begin
        Reporte.AgregarParametro('ProveedorDesde','TODOS');
        Reporte.AgregarParametro('ProveedorHasta','');
      end;
      Case CmbTipoReporte.ItemIndex of
        0://Retencion IVA Global (Compras - Ventas)
        begin
          TipoReporte := 'RETENCION I.V.A GLOBAL (COMPRAS - VENTAS)';
        end;
        1://Retencion IVA Compras
        begin
          TipoReporte := 'RETENCION I.V.A (COMPRAS)';
        end;
        2://Retencion IVA Ventas
        begin
          TipoReporte := 'RETENCION I.V.A (VENTAS)';
        end;
        3://Retencion IVA Resumen Debitos y Creditos
        begin
          TipoReporte := 'RESUMEN DEBITOS Y CREDITOS';
        end;
      end;
    end;
    12://Impuesto retenido a Proveedores ISLR
    begin
      Reporte.AgregarParametro('ProveedorDesde',CmbProveedorDesde.Text);
      Reporte.AgregarParametro('ProveedorHasta',CmbProveedorHasta.Text);
      Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
      Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
      if (CmbZona.Text <> '') then
      begin
        Reporte.AgregarParametro('Zona', CmbZona.Text);
      end
      else
      begin
        Reporte.AgregarParametro('Zona', 'TODAS');
      end;
      if (CmbProveedorDesde.Text <> '') then
      begin
        Reporte.AgregarParametro('ProveedorDesde', CmbProveedorDesde.Text);
      end
      else
      begin
        Reporte.AgregarParametro('ProveedorDesde', 'TODOS');
      end;
      Case CmbTipoReporte.ItemIndex of
        0:// Por Tipo Retencion
        begin
          TipoReporte := 'Ordenado Por Tipo de Retenci�n';
        end;
        1://Por Proveedor
        begin
          TipoReporte := 'Ordenado Por Proveedor';
        end;
      end;
    end;
    13://Movimientos de Inventario
    begin
      TituloReporte := 'MOVIMIENTOS DE INVENTARIO';
      //Evaluando Parametros de Cabecera
      if (CmbZona.Text <> '') then
      begin
        Reporte.AgregarParametro('Zona', CmbZona.Text);
      end
      else
      begin
        Reporte.AgregarParametro('Zona', 'TODAS');
      end;
      if (CmbEstacion.Text <> '') then
      begin
        Reporte.AgregarParametro('Estacion', CmbEstacion.Text);
      end
      else
      begin
        Reporte.AgregarParametro('Estacion', 'TODAS');
      end;
      if (CmbMoneda.Text <> '') then
      begin
        Reporte.AgregarParametro('Moneda', CmbMoneda.Text);
      end
      else
      begin
        Reporte.AgregarParametro('Moneda', 'VEB');
      end;
      if (CmbClasif1.Text <> '') then
      begin
        Reporte.AgregarParametro('Division', CmbClasif1.Text);
      end
      else
      begin
        Reporte.AgregarParametro('Division', 'TODAS');
      end;
      if (CmbClasif2.Text <> '') then
      begin
        Reporte.AgregarParametro('Linea', CmbClasif2.Text);
      end
      else
      begin
        Reporte.AgregarParametro('Linea', 'TODAS');
      end;
      if (CmbClasif3.Text <> '') then
      begin
        Reporte.AgregarParametro('Familia', CmbClasif3.Text);
      end
      else
      begin
        Reporte.AgregarParametro('Familia', 'TODAS');
      end;
      if (CmbClasif4.Text <> '') then
      begin
        Reporte.AgregarParametro('Clase', CmbClasif4.Text);
      end
      else
      begin
        Reporte.AgregarParametro('Clase', 'TODAS');
      end;
      if (CmbClasif5.Text <> '') then
      begin
        Reporte.AgregarParametro('Manufactura', CmbClasif5.Text);
      end
      else
      begin
        Reporte.AgregarParametro('Manufactura', 'TODAS');
      end;
      if (CmbClasif6.Text <> '') then
      begin
        Reporte.AgregarParametro('Producto', CmbClasif6.Text);
      end
      else
      begin
        Reporte.AgregarParametro('Producto', 'TODOS');
      end;
      Case CmbTipoReporte.ItemIndex of
        0: //Reporte Movimientos Inventario  Detallado General
        begin
          TipoReporte := 'Detallado General';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        1: //Reporte Movimientos Inventario Detallado Division
        begin
          TipoReporte := 'Detallado Division';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        2: //Reporte Movimientos Inventario Detallado Linea
        begin
          TipoReporte := 'Detallado Linea';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        3: //Reporte Movimientos Inventario Detallado Familia
        begin
          TipoReporte := 'Detallado Familia';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        4: //Reporte Movimientos Inventario Detallado Clase
        begin
          TipoReporte := 'Detallado Clase';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        5: //Reporte Movimientos Inventario Detallado Manufactura
        begin
          TipoReporte := 'Detallado Manufactura';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        6: //Reporte Movimientos Inventario Resumido Division
        begin
          TipoReporte := 'Resumido Division';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        7: //Reporte Movimientos Inventario Resumido Linea
        begin
          TipoReporte := 'Resumido Linea';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        8: //Reporte Movimientos Inventario Resumido Familia
        begin
          TipoReporte := 'Resumido Familia';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        9: //Reporte Movimientos Inventario Resumido Clase
        begin
          TipoReporte := 'Resumido Clase';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        10: //Reporte Movimientos Inventario Resumido Manufactura
        begin
          TipoReporte := 'Resumido Manufactura';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        11: //Reporte Movimientos Inventario Resumido Producto
        begin
          TipoReporte := 'Resumido por Producto';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
      end;
    end;
    14://Existencia / Libro Inventario
    begin
      TituloReporte := 'EXIST INVENTARIO - LIBRO INVENTARIO';
      //Evaluando Parametros de Cabecera
      if (CmbZona.Text <> '') then
      begin
        Reporte.AgregarParametro('Zona', CmbZona.Text);
      end
      else
      begin
        Reporte.AgregarParametro('Zona', 'TODAS');
      end;
      if (CmbEstacion.Text <> '') then
      begin
        Reporte.AgregarParametro('Estacion', CmbEstacion.Text);
      end
      else
      begin
        Reporte.AgregarParametro('Estacion', 'TODAS');
      end;
      if (CmbMoneda.Text <> '') then
      begin
        Reporte.AgregarParametro('Moneda', CmbMoneda.Text);
      end
      else
      begin
        Reporte.AgregarParametro('Moneda', 'VEB');
      end;
      if (CmbClasif1.Text <> '') then
      begin
        Reporte.AgregarParametro('Division', CmbClasif1.Text);
      end
      else
      begin
        Reporte.AgregarParametro('Division', 'TODAS');
      end;
      if (CmbClasif2.Text <> '') then
      begin
        Reporte.AgregarParametro('Linea', CmbClasif2.Text);
      end
      else
      begin
        Reporte.AgregarParametro('Linea', 'TODAS');
      end;
      if (CmbClasif3.Text <> '') then
      begin
        Reporte.AgregarParametro('Familia', CmbClasif3.Text);
      end
      else
      begin
        Reporte.AgregarParametro('Familia', 'TODAS');
      end;
      if (CmbClasif4.Text <> '') then
      begin
        Reporte.AgregarParametro('Clase', CmbClasif4.Text);
      end
      else
      begin
        Reporte.AgregarParametro('Clase', 'TODAS');
      end;
      if (CmbClasif5.Text <> '') then
      begin
        Reporte.AgregarParametro('Manufactura', CmbClasif5.Text);
      end
      else
      begin
        Reporte.AgregarParametro('Manufactura', 'TODAS');
      end;
      if (CmbClasif6.Text <> '') then
      begin
        Reporte.AgregarParametro('Producto', CmbClasif6.Text);
      end
      else
      begin
        Reporte.AgregarParametro('Producto', 'TODOS');
      end;
      Case CmbTipoReporte.ItemIndex of
        0: //Reporte Existencia Inventario  Detallado Producto
        begin
          TipoReporte := 'Detallado Producto';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        1: //Reporte Existencia Inventario Detallado Division
        begin
          TipoReporte := 'Detallado Division';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        2: //Reporte Existencia Inventario Detallado Linea
        begin
          TipoReporte := 'Detallado Linea';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        3: //Reporte Existencia Inventario Detallado Familia
        begin
          TipoReporte := 'Detallado Familia';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        4: //Reporte Existencia Inventario Detallado Clase
        begin
          TipoReporte := 'Detallado Clase';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        5: //Reporte Existencia Inventario Detallado Manufactura
        begin
          TipoReporte := 'Detallado Manufactura';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        6: //Reporte Movimientos Inventario Resumido Producto
        begin
          TipoReporte := 'Resumido por Producto';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        7: //Reporte Movimientos Inventario Resumido Division
        begin
          TipoReporte := 'Resumido Division';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        8: //Reporte Movimientos Inventario Resumido Linea
        begin
          TipoReporte := 'Resumido Linea';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        9: //Reporte Movimientos Inventario Resumido Familia
        begin
          TipoReporte := 'Resumido Familia';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        10: //Reporte Movimientos Inventario Resumido Clase
        begin
          TipoReporte := 'Resumido Clase';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
        11: //Reporte Movimientos Inventario Resumido Manufactura
        begin
          TipoReporte := 'Resumido Manufactura';
          Reporte.AgregarParametro('FechaDesde',FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date));
          Reporte.AgregarParametro('FechaHasta',FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date));
          Reporte.AgregarParametro('Periodo','Desde ' + DatFechaDesde.Text + ' Hasta ' + DatFechaHasta.Text);
        end;
      end;
    end;
  end;
  Reporte.AgregarParametro('TituloReporte',TituloReporte);
  Reporte.AgregarParametro('TipoReporte',TipoReporte);
  Reporte.AgregarParametro('ClaseReporte',ClaseReporte);
  Reporte.AgregarParametro('TipoOrdenamiento',TipoOrdenamiento);
end;

procedure TFrmReporteGeneral.EmitirReporte;
var
  NombreReporte: string;
  SQL: TStringList;
  Where: string;
  OrderBy: string;

  function AgregarWhere(Old, New: string): string;
  begin
    if (Length(Trim(Old)) > 0) then
    begin
      Result := Old + ' AND ' + New;
    end
    else
    begin
      Result := New;
    end;
  end;

begin
  Screen.Cursor := crHourGlass;
  SQL := TStringList.Create;
  Where := '';
  OrderBy := '';
  case GrpListadoReportes.ItemIndex of
    0: //Reporte Balance Caja (Ingresos)
    begin
      case CmbTipoReporte.ItemIndex of
        0: // Reporte Detallado
        begin
          if (Validar) then
          begin
            NombreReporte :='ReporteDeCajaDetallado';
          end;
        end;
        1: // Reporte Resumido
        begin
          if (Validar) then
          begin
            NombreReporte :='ReporteDeCajaResumido';
          end;
        end;
      end;
    end;
    1: //Reporte Balance Caja (Egresos)
    begin
      case CmbTipoReporte.ItemIndex of
        0: // Reporte Detallado
        begin
          if (Validar) then
          begin
            NombreReporte :='ReporteDeCajaDetalladoEgreso';
          end;
        end;
        1: // Reporte Resumido
        begin
          if (Validar) then
          begin
            NombreReporte :='ReporteDeCajaResumidoEgreso';
          end;
        end;
      end;
    end;
    2: // Reporte Estado de Cuentas Bancarios
    begin
      case CmbTipoReporte.ItemIndex of
        0: // Reporte Estandar
        begin
          if (Validar) then
          begin
            Where := AgregarWhere(Where, '(fecha BETWEEN ' + QuotedStr(FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date))+ ' AND ' + QuotedStr(FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date)) + ')');
            NombreReporte :='ReporteOperacionesBancarias';
            if (CmbCuentaBancaria.Text = '') then
            begin
              SQL.Add('SELECT *');
              SQL.Add('FROM public.v_rep_operaciones_bancarias');
              SQL.Add('WHERE ' + Where);
              //Where := AgregarWhere(Where, '(fecha BETWEEN ' + QuotedStr(FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date))+ ' AND ' + QuotedStr(FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date)) + ')');
              //SQL.Add('WHERE (fecha BETWEEN ' + QuotedStr(':FechaDesde') + ' AND '+QuotedStr(':FechaHasta')+')');
              Reporte.ModificarSQLDriverDataView('DvrOperacionesBancarias',SQL.Text);
            end
            else
            begin
              Where := AgregarWhere(Where, '(codigo_cuenta = ' + QuotedStr(CmbCuentaBancaria.Text ) + ')' );
              //Modificando Query SQL
              SQL.Add('SELECT *');
              SQL.Add('FROM public.v_rep_operaciones_bancarias');
              if (Length(Trim(Where)) > 0) then
              begin
                SQL.Add('WHERE ' + Where);
              end;
              if (Length(Trim(OrderBy)) > 0) then
              begin
                SQL.Add(OrderBy);
              end;
              Reporte.ModificarSQLDriverDataView('DvrOperacionesBancarias',SQL.Text);
            end;
          end;
        end;
        1: // Reporte Consolidado
        begin
          if (Validar) then
          begin
            Where := AgregarWhere(Where, '(fecha BETWEEN ' + QuotedStr(FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date))+ ' AND ' + QuotedStr(FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date)) + ')');
            NombreReporte :='ReporteOperacionesBancariasConsolidado';
            if (CmbCuentaBancaria.Text = '') then
            begin
              SQL.Add('SELECT *');
              SQL.Add('FROM public.v_rep_operaciones_bancarias');
              SQL.Add('WHERE ' + Where);
              //Where := AgregarWhere(Where, '(fecha BETWEEN ' + QuotedStr(FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date))+ ' AND ' + QuotedStr(FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date)) + ')');
              //SQL.Add('WHERE (fecha BETWEEN ' + QuotedStr(':FechaDesde') + ' AND '+QuotedStr(':FechaHasta')+')');
              Reporte.ModificarSQLDriverDataView('DvrOperacionesBancarias',SQL.Text);
            end
            else
            begin
              Where := AgregarWhere(Where, '(codigo_cuenta = ' + QuotedStr(CmbCuentaBancaria.Text ) + ')' );
              //Modificando Query SQL
              SQL.Add('SELECT *');
              SQL.Add('FROM public.v_rep_operaciones_bancarias');
              if (Length(Trim(Where)) > 0) then
              begin
                SQL.Add('WHERE ' + Where);
              end;
              if (Length(Trim(OrderBy)) > 0) then
              begin
                SQL.Add(OrderBy);
              end;
              Reporte.ModificarSQLDriverDataView('DvrOperacionesBancarias',SQL.Text);
            end;
          end;
        end;
      end;
    end;
    3: // Reporte Estado de Cuentas Clientes
    begin
      case CmbTipoReporte.ItemIndex of
        0: // Reporte Detallado
        begin
          NombreReporte :='EstadoCuentaClientesDetallado';
          //Determinando Query del Reporte para todos los Clientes
          if (CmbClienteDesde.Text = '') and (CmbClienteHasta.Text = '') then
          begin
            SQL.Add('SELECT *');
            SQL.Add('FROM public.v_rep_estado_cuenta_cliente_detallado_resumido');
            SQL.Add('WHERE (cxc_fecha_ingreso BETWEEN ' + QuotedStr(':FechaDesde') + ' AND '+QuotedStr(':FechaHasta')+')');
            SQL.Add('ORDER BY cxc_codigo_cliente, cxc_numero_documento');
            Reporte.ModificarSQLDriverDataView('DvrEstadoCuentaClienteDetalladoResumido',SQL.Text);
          end;
        end;
        1: // Reporte Resumido
        begin
          NombreReporte :='EstadoCuentaClientesResumido';
          //Determinando Query del Reporte para todos los Clientes
          if (CmbClienteDesde.Text = '') and (CmbClienteHasta.Text = '') then
          begin
            SQL.Add('SELECT *');
            SQL.Add('FROM public.v_rep_estado_cuenta_cliente_detallado_resumido');
            SQL.Add('WHERE (cxc_fecha_ingreso BETWEEN ' + QuotedStr(':FechaDesde') + ' AND '+QuotedStr(':FechaHasta')+')');
            SQL.Add('ORDER BY cxc_codigo_cliente, cxc_numero_documento');
            Reporte.ModificarSQLDriverDataView('DvrEstadoCuentaClienteDetalladoResumido',SQL.Text);
          end;
        end;
        2: // Reporte Analisis de Vencimiento Detallado
        begin
          NombreReporte :='EstadoCuentaClientesAnalisisDetallado';
          //Determinando Query del Reporte para todos los Clientes
          if (CmbClienteDesde.Text = '') and (CmbClienteHasta.Text = '') then
          begin
            SQL.Add('SELECT *');
            SQL.Add('FROM public.v_rep_estado_cuenta_cliente_analisis_detallado');
            SQL.Add('WHERE (cxc_fecha_ingreso <= ' + QuotedStr(':FechaAl') + ')');
            SQL.Add('ORDER BY cxc_codigo_cliente, cxc_numero_documento');
            Reporte.ModificarSQLDriverDataView('DvrEstadoCuentaClienteAnalisis',SQL.Text);
          end;
        end;
        3: // Reporte Analisis de Vencimiento  Resumido
        begin
          NombreReporte :='EstadoCuentaClientesAnalisisResumido';
          //Determinando Query del Reporte para todos los Clientes
          if (CmbClienteDesde.Text = '') and (CmbClienteHasta.Text = '') then
          begin
            SQL.Add('SELECT *');
            SQL.Add('FROM public.v_rep_estado_cuenta_cliente_analisis_detallado');
            SQL.Add('WHERE (cxc_fecha_ingreso <= ' + QuotedStr(':FechaAl') + ')');
            SQL.Add('ORDER BY cxc_codigo_cliente, cxc_numero_documento');
            Reporte.ModificarSQLDriverDataView('DvrEstadoCuentaClienteAnalisis',SQL.Text);
          end;
        end;
        4: // Reporte Analisis de Vencimiento Detallado (Modelo Dias)
        begin
          NombreReporte :='EstadoCuentaClientesAnalisisDetalladoDias';
          //Determinando Query del Reporte para todos los Clientes
          if (CmbClienteDesde.Text = '') and (CmbClienteHasta.Text = '') then
          begin
            SQL.Add('SELECT *');
            SQL.Add('FROM public.v_rep_estado_cuenta_cliente_analisis_detallado_dias');
            SQL.Add('WHERE (cxc_fecha_ingreso <= ' + QuotedStr(':FechaAl') + ')');
            SQL.Add('ORDER BY cxc_codigo_cliente, cxc_numero_documento');
            Reporte.ModificarSQLDriverDataView('DvrEstadoCuentaClienteAnalisisDias',SQL.Text);
          end;
        end;
        5: // Reporte Analisis de Vencimiento  Resumido (Modelo Dias)
        begin
          NombreReporte :='EstadoCuentaClientesAnalisisResumidoDias';
          //Determinando Query del Reporte para todos los Clientes
          if (CmbClienteDesde.Text = '') and (CmbClienteHasta.Text = '') then
          begin
            SQL.Add('SELECT *');
            SQL.Add('FROM public.v_rep_estado_cuenta_cliente_analisis_detallado_dias');
            SQL.Add('WHERE (cxc_fecha_ingreso <= ' + QuotedStr(':FechaAl') + ')');
            SQL.Add('ORDER BY cxc_codigo_cliente, cxc_numero_documento');
            Reporte.ModificarSQLDriverDataView('DvrEstadoCuentaClienteAnalisisDias',SQL.Text);
          end;
        end;
      end;
    end;
    4: // Reporte Estados de Cuenta Proveedores
    begin
      case CmbTipoReporte.ItemIndex of
        0: // Reporte Detallado
        begin
          NombreReporte :='EstadoCuentaProveedoresDetallado';
          //Determinando Query del Reporte para todos los Proveedores
          if (CmbProveedorDesde.Text = '') and (CmbProveedorHasta.Text = '') then
          begin
            SQL.Add('SELECT *');
            SQL.Add('FROM public.v_rep_estado_cuenta_proveedor_detallado_resumido');
            SQL.Add('WHERE "from"= ' + QuotedStr('CXP') + ' AND (cxp_fecha_ingreso BETWEEN ' + QuotedStr(':FechaDesde') + ' AND '+QuotedStr(':FechaHasta')+')');
            SQL.Add('ORDER BY cxp_codigo_proveedor, cxp_numero_documento');
            Reporte.ModificarSQLDriverDataView('DvrEstadoCuentaProveedorDetalladoResumido',SQL.Text);
          end;
        end;
        1: // Reporte Resumido
        begin
          NombreReporte :='EstadoCuentaProveedoresResumido';
          //Determinando Query del Reporte para todos los Proveedores
          if (CmbProveedorDesde.Text = '') and (CmbProveedorHasta.Text = '') then
          begin
            SQL.Add('SELECT *');
            SQL.Add('FROM public.v_rep_estado_cuenta_proveedor_detallado_resumido');
            SQL.Add('WHERE "from"= ' + QuotedStr('CXP') + ' AND (cxp_fecha_ingreso BETWEEN ' + QuotedStr(':FechaDesde') + ' AND '+QuotedStr(':FechaHasta')+')');
            SQL.Add('ORDER BY cxp_codigo_proveedor, cxp_numero_documento');
            Reporte.ModificarSQLDriverDataView('DvrEstadoCuentaProveedorDetalladoResumido',SQL.Text);
          end;
        end;
        2: // Reporte Analisis de Vencimiento Detallado
        begin
          NombreReporte :='EstadoCuentaProveedoresAnalisisDetallado';
          //Determinando Query del Reporte para todos los Proveedores
          if (CmbProveedorDesde.Text = '') and (CmbProveedorHasta.Text = '') then
          begin
            SQL.Add('SELECT *');
            SQL.Add('FROM public.v_rep_estado_cuenta_proveedor_analisis');
            SQL.Add('WHERE "from"= ' + QuotedStr('CXP') + ' AND (cxp_fecha_ingreso <= ' + QuotedStr(':FechaAl') + ')');
            SQL.Add('ORDER BY cxp_codigo_proveedor, cxp_numero_documento');
            Reporte.ModificarSQLDriverDataView('DvrEstadoCuentaProveedorAnalisis',SQL.Text);
          end;
        end;
        3: // Reporte Analisis de Vencimiento  Resumido
        begin
          NombreReporte :='EstadoCuentaProveedoresAnalisisResumido';
          //Determinando Query del Reporte para todos los Proveedores
          if (CmbProveedorDesde.Text = '') and (CmbProveedorHasta.Text = '') then
          begin
            SQL.Add('SELECT *');
            SQL.Add('FROM public.v_rep_estado_cuenta_proveedor_analisis');
            SQL.Add('WHERE "from"= ' + QuotedStr('CXP') + ' AND (cxp_fecha_ingreso <= ' + QuotedStr(':FechaAl') + ')');
            SQL.Add('ORDER BY cxp_codigo_proveedor, cxp_numero_documento');
            Reporte.ModificarSQLDriverDataView('DvrEstadoCuentaProveedorAnalisis',SQL.Text);
          end;
        end;
      end;
    end;
    5: // Reporte de Comisiones
    begin
      case CmbTipoReporte.ItemIndex of
        0: // Reporte Detallado
        begin
          NombreReporte :='PagoComisionDetallado';
          //Determinando Query del Reporte para todos los Vendedores
          if (CmbVendedor.Text = '') and (CmbZona.Text <> '') then
          begin
            SQL.Add('SELECT *');
            SQL.Add('FROM public.v_rep_pago_comision');
            SQL.Add('WHERE (fecha_pago BETWEEN ' + QuotedStr(':FechaDesde') + ' AND ' + QuotedStr(':FechaHasta')+')' + ' AND ' + 'codigo_zona = ' + QuotedStr(CmbZona.Text));
            SQL.Add('ORDER BY codigo_zona, codigo_vendedor');
            Reporte.ModificarSQLDriverDataView('DvrPagoComisionPagado',SQL.Text);
          end
          else
          begin
            if (CmbVendedor.Text <> '') and (CmbZona.Text = '') then
            begin
              SQL.Add('SELECT *');
              SQL.Add('FROM public.v_rep_pago_comision');
              SQL.Add('WHERE (fecha_pago BETWEEN ' + QuotedStr(':FechaDesde') + ' AND ' + QuotedStr(':FechaHasta')+')' + ' AND ' + 'codigo_vendedor = ' + QuotedStr(CmbVendedor.Text));
              SQL.Add('ORDER BY codigo_zona, codigo_vendedor');
              Reporte.ModificarSQLDriverDataView('DvrPagoComisionPagado',SQL.Text);
            end
            else
            begin
              if (CmbVendedor.Text = '') and (CmbZona.Text = '') then
              begin
                SQL.Add('SELECT *');
                SQL.Add('FROM public.v_rep_pago_comision');
                SQL.Add('WHERE (fecha_pago BETWEEN ' + QuotedStr(':FechaDesde') + ' AND ' + QuotedStr(':FechaHasta')+')');
                SQL.Add('ORDER BY codigo_zona, codigo_vendedor');
                Reporte.ModificarSQLDriverDataView('DvrPagoComisionPagado',SQL.Text);
              end
              else
              begin
                SQL.Add('SELECT *');
                SQL.Add('FROM public.v_rep_pago_comision');
                SQL.Add('WHERE (fecha_pago BETWEEN ' + QuotedStr(':FechaDesde') + ' AND ' + QuotedStr(':FechaHasta')+')' + ' AND ' + 'codigo_zona = ' + QuotedStr(CmbZona.Text) + ' AND ' + 'codigo_vendedor = ' + QuotedStr(CmbVendedor.Text) );
                SQL.Add('ORDER BY codigo_zona, codigo_vendedor');
                Reporte.ModificarSQLDriverDataView('DvrPagoComisionPagado',SQL.Text);
              end;
            end;
          end;
        end;
        1: // Reporte Resumido
        begin
          NombreReporte :='PagoComisionResumido';
          if (CmbVendedor.Text = '') and (CmbZona.Text <> '') then
          begin
            SQL.Add('SELECT *');
            SQL.Add('FROM public.v_rep_pago_comision');
            SQL.Add('WHERE (fecha_pago BETWEEN ' + QuotedStr(':FechaDesde') + ' AND ' + QuotedStr(':FechaHasta')+')' + ' AND ' + 'codigo_zona = ' + QuotedStr(CmbZona.Text));
            SQL.Add('ORDER BY numero');
            Reporte.ModificarSQLDriverDataView('DvrPagoComisionPagado',SQL.Text);
          end
          else
          begin
            if (CmbVendedor.Text <> '') and (CmbZona.Text = '') then
            begin
              SQL.Add('SELECT *');
              SQL.Add('FROM public.v_rep_pago_comision');
              SQL.Add('WHERE (fecha_pago BETWEEN ' + QuotedStr(':FechaDesde') + ' AND ' + QuotedStr(':FechaHasta')+')' + ' AND ' + 'codigo_vendedor = ' + QuotedStr(CmbVendedor.Text));
              SQL.Add('ORDER BY numero');
              Reporte.ModificarSQLDriverDataView('DvrPagoComisionPagado',SQL.Text);
            end
            else
            begin
              if (CmbVendedor.Text = '') and (CmbZona.Text = '') then
              begin
                SQL.Add('SELECT *');
                SQL.Add('FROM public.v_rep_pago_comision');
                SQL.Add('WHERE (fecha_pago BETWEEN ' + QuotedStr(':FechaDesde') + ' AND ' + QuotedStr(':FechaHasta')+')');
                SQL.Add('ORDER BY numero');
                Reporte.ModificarSQLDriverDataView('DvrPagoComisionPagado',SQL.Text);
              end
              else
              begin
                SQL.Add('SELECT *');
                SQL.Add('FROM public.v_rep_pago_comision');
                SQL.Add('WHERE (fecha_pago BETWEEN ' + QuotedStr(':FechaDesde') + ' AND ' + QuotedStr(':FechaHasta')+')' + ' AND ' + 'codigo_zona = ' + QuotedStr(CmbZona.Text) + ' AND ' + 'codigo_vendedor = ' + QuotedStr(CmbVendedor.Text) );
                SQL.Add('ORDER BY numero');
                Reporte.ModificarSQLDriverDataView('DvrPagoComisionPagado',SQL.Text);
              end;
            end;
          end;
        end;
      end;
    end;
    6: //Comisiones Por Pagar Vendedores
    begin
      case CmbTipoReporte.ItemIndex of
        0: // Reporte Detallado
        begin
          NombreReporte :='PagoComisionXPagarDetallado';
          //Determinando Query del Reporte para todos los Vendedores
          if (CmbVendedor.Text = '') and (CmbZona.Text <> '') then
          begin
            SQL.Add('SELECT *');
            SQL.Add('FROM public.v_rep_pago_comision_x_pagar');
            SQL.Add('WHERE (cc_fecha_cobranza BETWEEN ' + QuotedStr(':FechaDesde') + ' AND ' + QuotedStr(':FechaHasta')+')' + ' AND ' + 'cc_codigo_zona = ' + QuotedStr(CmbZona.Text));
            SQL.Add('ORDER BY cc_codigo_zona, cc_codigo_vendedor');
            Reporte.ModificarSQLDriverDataView('DvrPagoComisionXPagar',SQL.Text);
          end
          else
          begin
            if (CmbVendedor.Text <> '') and (CmbZona.Text = '') then
            begin
              SQL.Add('SELECT *');
              SQL.Add('FROM public.v_rep_pago_comision_x_pagar');
              SQL.Add('WHERE (cc_fecha_cobranza BETWEEN ' + QuotedStr(':FechaDesde') + ' AND ' + QuotedStr(':FechaHasta')+')' + ' AND ' + 'cc_codigo_vendedor = ' + QuotedStr(CmbVendedor.Text));
              SQL.Add('ORDER BY cc_codigo_zona, cc_codigo_vendedor');
              Reporte.ModificarSQLDriverDataView('DvrPagoComisionXPagar',SQL.Text);
            end
            else
            begin
              if (CmbVendedor.Text = '') and (CmbZona.Text = '') then
              begin
                SQL.Add('SELECT *');
                SQL.Add('FROM public.v_rep_pago_comision_x_pagar');
                SQL.Add('WHERE (cc_fecha_cobranza BETWEEN ' + QuotedStr(':FechaDesde') + ' AND ' + QuotedStr(':FechaHasta')+')');
                SQL.Add('ORDER BY cc_codigo_zona, cc_codigo_vendedor');
                Reporte.ModificarSQLDriverDataView('DvrPagoComisionXPagar',SQL.Text);
              end
              else
              begin
                SQL.Add('SELECT *');
                SQL.Add('FROM public.v_rep_pago_comision_x_pagar');
                SQL.Add('WHERE (cc_fecha_cobranza BETWEEN ' + QuotedStr(':FechaDesde') + ' AND ' + QuotedStr(':FechaHasta')+')' + ' AND ' + 'cc_codigo_zona = ' + QuotedStr(CmbZona.Text) + ' AND ' + 'cc_codigo_vendedor = ' + QuotedStr(CmbVendedor.Text) );
                SQL.Add('ORDER BY cc_codigo_zona, cc_codigo_vendedor');
                Reporte.ModificarSQLDriverDataView('DvrPagoComisionXPagar',SQL.Text);
              end;
            end;
          end;
        end;
        1: // Reporte Resumido
        begin
          NombreReporte :='PagoComisionXPagarResumido';
          if (CmbVendedor.Text = '') and (CmbZona.Text <> '') then
          begin
            SQL.Add('SELECT *');
            SQL.Add('FROM public.v_rep_pago_comision_x_pagar');
            SQL.Add('WHERE (cc_fecha_cobranza BETWEEN ' + QuotedStr(':FechaDesde') + ' AND ' + QuotedStr(':FechaHasta')+')' + ' AND ' + 'cc_codigo_zona = ' + QuotedStr(CmbZona.Text));
            SQL.Add('ORDER BY fc_numero_factura');
            Reporte.ModificarSQLDriverDataView('DvrPagoComisionXPagar',SQL.Text);
          end
          else
          begin
            if (CmbVendedor.Text <> '') and (CmbZona.Text = '') then
            begin
              SQL.Add('SELECT *');
              SQL.Add('FROM public.v_rep_pago_comision_x_pagar');
              SQL.Add('WHERE (cc_fecha_cobranza BETWEEN ' + QuotedStr(':FechaDesde') + ' AND ' + QuotedStr(':FechaHasta')+')' + ' AND ' + 'cc_codigo_vendedor = ' + QuotedStr(CmbVendedor.Text));
              SQL.Add('ORDER BY fc_numero_factura');
              Reporte.ModificarSQLDriverDataView('DvrPagoComisionXPagar',SQL.Text);
            end
            else
            begin
              if (CmbVendedor.Text = '') and (CmbZona.Text = '') then
              begin
                SQL.Add('SELECT *');
                SQL.Add('FROM public.v_rep_pago_comision_x_pagar');
                SQL.Add('WHERE (cc_fecha_cobranza BETWEEN ' + QuotedStr(':FechaDesde') + ' AND ' + QuotedStr(':FechaHasta')+')');
                SQL.Add('ORDER BY fc_numero_factura');
                Reporte.ModificarSQLDriverDataView('DvrPagoComisionXPagar',SQL.Text);
              end
              else
              begin
                SQL.Add('SELECT *');
                SQL.Add('FROM public.v_rep_pago_comision_x_pagar');
                SQL.Add('WHERE (cc_fecha_cobranza BETWEEN ' + QuotedStr(':FechaDesde') + ' AND ' + QuotedStr(':FechaHasta')+')' + ' AND ' + 'cc_codigo_zona = ' + QuotedStr(CmbZona.Text) + ' AND ' + 'cc_codigo_vendedor = ' + QuotedStr(CmbVendedor.Text) );
                SQL.Add('ORDER BY fc_numero_factura');
                Reporte.ModificarSQLDriverDataView('DvrPagoComisionXPagar',SQL.Text);
              end;
            end;
          end;
        end;
      end;
    end;
    7: //Productos Vendidos
    begin
      //Configurando WHERE
      //Considerando Zona
      if (CmbZona.Text <> '') then
      begin
        Where := AgregarWhere(Where, '(codigo_zona = ' + QuotedStr(CmbZona.Text) + ')');
      end;
      //Considerando Estacion
      if (CmbEstacion.Text <> '') then
      begin
        Where := AgregarWhere(Where, '(codigo_estacion = ' + QuotedStr(CmbEstacion.Text) + ')');
      end;
      //Considerando Vendedor
      if (CmbVendedor.Text <> '') then
      begin
        Where := AgregarWhere(Where, '(codigo_vendedor = ' + QuotedStr(CmbVendedor.Text) + ')');
      end;
      //Considerando Clasificacion 1 (Division)
      if (CmbClasif1.Text <> '') then
      begin
        Where := AgregarWhere(Where, '(codigo_division = ' + QuotedStr(CmbClasif1.Text) + ')');
      end;
      //Considerando Clasificacion 2 (Linea)
      if (CmbClasif2.Text <> '') then
      begin
        Where := AgregarWhere(Where, '(codigo_linea = ' + QuotedStr(CmbClasif2.Text) + ')');
      end;
      //Considerando Clasificacion  3 (Familia)
      if (CmbClasif3.Text <> '') then
      begin
        Where := AgregarWhere(Where, '(codigo_familia = ' + QuotedStr(CmbClasif3.Text) + ')');
      end;
      //Considerando Clasificacion 4 (Clase)
      if (CmbClasif4.Text <> '') then
      begin
        Where := AgregarWhere(Where, '(codigo_clase = ' + QuotedStr(CmbClasif4.Text) + ')');
      end;
      //Considerando Clasificacion 5 (Manufactura)
      if (CmbClasif5.Text <> '') then
      begin
        Where := AgregarWhere(Where, '(codigo_manufactura = ' + QuotedStr(CmbClasif5.Text) + ')');
      end;
      //Considerando Productos
      if (CmbProductoDesde.Text <> '') and (CmbProductoHasta.Text <> '') then
      begin
        Where := AgregarWhere(Where, '(codigo_producto BETWEEN ' + QuotedStr(CmbProductoDesde.Text) + ' AND ' + QuotedStr(CmbProductoHasta.Text) + ')');
      end;
      //Considerando Clientes
      if (CmbClienteDesde.Text <> '') and (CmbClienteHasta.Text <> '') then
      begin
        Where := AgregarWhere(Where, '(codigo_cliente BETWEEN ' + QuotedStr(CmbClienteDesde.Text) + ' AND ' + QuotedStr(CmbClienteHasta.Text) + ')');
      end;
      //Evaluando Condiciones y Nombre del Reporte
      case CmbTipoReporte.ItemIndex of
        0: // Reporte Detallado General
        begin
          case CmbClaseReporte.ItemIndex of
            0: // Solo Ventas
            begin
              Where := AgregarWhere(Where, '(Desde = ' + QuotedStr('FC')+ ')');
            end;
            1: // Solo Devoluciones
            begin
              Where := AgregarWhere(Where, '(Desde = ' + QuotedStr('NCC')+ ')')
            end;
            2: // Venta y Devoluciones
            begin

            end;
          end;
          NombreReporte := 'ProductosVendidosDetalladoGeneral';
          OrderBy := 'ORDER BY codigo_zona, codigo_estacion, codigo_producto';
        end;
        1: // Reporte Detallado Division
        begin
          case CmbClaseReporte.ItemIndex of
            0: // Solo Ventas
            begin
              Where := AgregarWhere(Where, '(Desde = ' + QuotedStr('FC')+ ')');
            end;
            1: // Solo Devoluciones
            begin
              Where := AgregarWhere(Where, '(Desde = ' + QuotedStr('NCC')+ ')')
            end;
            2: // Venta y Devoluciones
            begin

            end;
          end;
          NombreReporte := 'ProductosVendidosDetalladoDivision';
          OrderBy := 'ORDER BY codigo_zona, codigo_division';
        end;
        2: // Reporte Detallado Linea
        begin
          case CmbClaseReporte.ItemIndex of
            0: // Solo Ventas
            begin
              Where := AgregarWhere(Where, '(Desde = ' + QuotedStr('FC')+ ')');
            end;
            1: // Solo Devoluciones
            begin
              Where := AgregarWhere(Where, '(Desde = ' + QuotedStr('NCC')+ ')')
            end;
            2: // Venta y Devoluciones
            begin

            end;
          end;
          NombreReporte := 'ProductosVendidosDetalladoLinea';
          OrderBy := 'ORDER BY codigo_zona, codigo_linea';
        end;
        3: // Reporte Detallado Familia
        begin
          case CmbClaseReporte.ItemIndex of
            0: // Solo Ventas
            begin
              Where := AgregarWhere(Where, '(Desde = ' + QuotedStr('FC')+ ')');
            end;
            1: // Solo Devoluciones
            begin
              Where := AgregarWhere(Where, '(Desde = ' + QuotedStr('NCC')+ ')')
            end;
            2: // Venta y Devoluciones
            begin

            end;
          end;
          NombreReporte := 'ProductosVendidosDetalladoFamilia';
          OrderBy := 'ORDER BY codigo_zona, codigo_familia';
        end;
        4: // Reporte Detallado Clase
        begin
          case CmbClaseReporte.ItemIndex of
            0: // Solo Ventas
            begin
              Where := AgregarWhere(Where, '(Desde = ' + QuotedStr('FC')+ ')');
            end;
            1: // Solo Devoluciones
            begin
              Where := AgregarWhere(Where, '(Desde = ' + QuotedStr('NCC')+ ')')
            end;
            2: // Venta y Devoluciones
            begin

            end;
          end;
          NombreReporte := 'ProductosVendidosDetalladoClase';
          OrderBy := 'ORDER BY codigo_zona, codigo_clase';
        end;
        5: // Reporte Detallado Manufactura
        begin
          case CmbClaseReporte.ItemIndex of
            0: // Solo Ventas
            begin
              Where := AgregarWhere(Where, '(Desde = ' + QuotedStr('FC')+ ')');
            end;
            1: // Solo Devoluciones
            begin
              Where := AgregarWhere(Where, '(Desde = ' + QuotedStr('NCC')+ ')')
            end;
            2: // Venta y Devoluciones
            begin

            end;
          end;
          NombreReporte := 'ProductosVendidosDetalladoManufactura';
          OrderBy := 'ORDER BY codigo_zona, codigo_manufactura';
        end;
        6: // Reporte Resumido Division
        begin
          case CmbClaseReporte.ItemIndex of
            0: // Solo Ventas
            begin
              Where := AgregarWhere(Where, '(Desde = ' + QuotedStr('FC')+ ')');
            end;
            1: // Solo Devoluciones
            begin
              Where := AgregarWhere(Where, '(Desde = ' + QuotedStr('NCC')+ ')')
            end;
            2: // Venta y Devoluciones
            begin

            end;
          end;
          NombreReporte := 'ProductosVendidosResumidoDivision';
          OrderBy := 'ORDER BY codigo_zona, codigo_division';
        end;
        7: // Reporte Resumido Linea
        begin
          case CmbClaseReporte.ItemIndex of
            0: // Solo Ventas
            begin
              Where := AgregarWhere(Where, '(Desde = ' + QuotedStr('FC')+ ')');
            end;
            1: // Solo Devoluciones
            begin
              Where := AgregarWhere(Where, '(Desde = ' + QuotedStr('NCC')+ ')')
            end;
            2: // Venta y Devoluciones
            begin

            end;
          end;
          NombreReporte := 'ProductosVendidosResumidoLinea';
          OrderBy := 'ORDER BY codigo_zona, codigo_linea';
        end;
        8: // Reporte Resumido Familia
        begin
          case CmbClaseReporte.ItemIndex of
            0: // Solo Ventas
            begin
              Where := AgregarWhere(Where, '(Desde = ' + QuotedStr('FC')+ ')');
            end;
            1: // Solo Devoluciones
            begin
              Where := AgregarWhere(Where, '(Desde = ' + QuotedStr('NCC')+ ')')
            end;
            2: // Venta y Devoluciones
            begin

            end;
          end;
          NombreReporte := 'ProductosVendidosResumidoFamilia';
          OrderBy := 'ORDER BY codigo_zona, codigo_familia';
        end;
        9: // Reporte Resumido Clase
        begin
          case CmbClaseReporte.ItemIndex of
            0: // Solo Ventas
            begin
              Where := AgregarWhere(Where, '(Desde = ' + QuotedStr('FC')+ ')');
            end;
            1: // Solo Devoluciones
            begin
              Where := AgregarWhere(Where, '(Desde = ' + QuotedStr('NCC')+ ')')
            end;
            2: // Venta y Devoluciones
            begin

            end;
          end;
          NombreReporte := 'ProductosVendidosResumidoClase';
          OrderBy := 'ORDER BY codigo_zona, codigo_clase';
        end;
        10: // Reporte Resumido Manufactura
        begin
          case CmbClaseReporte.ItemIndex of
            0: // Solo Ventas
            begin
              Where := AgregarWhere(Where, '(Desde = ' + QuotedStr('FC')+ ')');
            end;
            1: // Solo Devoluciones
            begin
              Where := AgregarWhere(Where, '(Desde = ' + QuotedStr('NCC')+ ')')
            end;
            2: // Venta y Devoluciones
            begin

            end;
          end;
          NombreReporte := 'ProductosVendidosResumidoManufactura';
          OrderBy := 'ORDER BY codigo_zona, codigo_manufactura';
        end;
        11: // Reporte Resumido Producto
        begin
          case CmbClaseReporte.ItemIndex of
            0: // Solo Ventas
            begin
              Where := AgregarWhere(Where, '(Desde = ' + QuotedStr('FC')+ ')');
            end;
            1: // Solo Devoluciones
            begin
              Where := AgregarWhere(Where, '(Desde = ' + QuotedStr('NCC')+ ')')
            end;
            2: // Venta y Devoluciones
            begin

            end;
          end;
          NombreReporte := 'ProductosVendidosResumidoProducto';
          OrderBy := 'ORDER BY codigo_zona, codigo_producto';
        end;
      end;
      //Modificando Query SQL
      SQL.Add('SELECT *');
      SQL.Add('FROM public.v_rep_productos_vendidos');
      if (Length(Trim(Where)) > 0) then
      begin
        SQL.Add('WHERE ' + Where);
      end;
      if (Length(Trim(OrderBy)) > 0) then
      begin
        SQL.Add(OrderBy);
      end;
      Reporte.ModificarSQLDriverDataView('DvrProductosVendidos',SQL.Text);
    end;
    8: // Productos Comprados
    begin
      //Configurando WHERE
      //Considerando Zona
      if (CmbZona.Text <> '') then
      begin
        Where := AgregarWhere(Where, '(codigo_zona = ' + QuotedStr(CmbZona.Text) + ')');
      end;
      //Considerando Estacion
      if (CmbEstacion.Text <> '') then
      begin
        Where := AgregarWhere(Where, '(codigo_estacion = ' + QuotedStr(CmbEstacion.Text) + ')');
      end;
      //Considerando Vendedor
      {if (CmbProveedorDesde.Text <> '') then
      begin
        Where := AgregarWhere(Where, '(codigo_proveedor = ' + QuotedStr(CmbProveedorDesde.Text) + ')');
      end;}
      //Considerando Clasificacion 1 (Division)
      if (CmbClasif1.Text <> '') then
      begin
        Where := AgregarWhere(Where, '(codigo_division = ' + QuotedStr(CmbClasif1.Text) + ')');
      end;
      //Considerando Clasificacion 2 (Linea)
      if (CmbClasif2.Text <> '') then
      begin
        Where := AgregarWhere(Where, '(codigo_linea = ' + QuotedStr(CmbClasif2.Text) + ')');
      end;
      //Considerando Clasificacion  3 (Familia)
      if (CmbClasif3.Text <> '') then
      begin
        Where := AgregarWhere(Where, '(codigo_familia = ' + QuotedStr(CmbClasif3.Text) + ')');
      end;
      //Considerando Clasificacion 4 (Clase)
      if (CmbClasif4.Text <> '') then
      begin
        Where := AgregarWhere(Where, '(codigo_clase = ' + QuotedStr(CmbClasif4.Text) + ')');
      end;
      //Considerando Clasificacion 5 (Manufactura)
      if (CmbClasif5.Text <> '') then
      begin
        Where := AgregarWhere(Where, '(codigo_manufactura = ' + QuotedStr(CmbClasif5.Text) + ')');
      end;
      //Considerando Productos
      if (CmbProductoDesde.Text <> '') and (CmbProductoHasta.Text <> '') then
      begin
        Where := AgregarWhere(Where, '(codigo_producto BETWEEN ' + QuotedStr(CmbProductoDesde.Text) + ' AND ' + QuotedStr(CmbProductoHasta.Text) + ')');
      end;
      //Considerando Clientes
      if (CmbProveedorDesde.Text <> '') and (CmbProveedorHasta.Text <> '') then
      begin
        Where := AgregarWhere(Where, '(codigo_proveedor BETWEEN ' + QuotedStr(CmbProveedorDesde.Text) + ' AND ' + QuotedStr(CmbProveedorHasta.Text) + ')');
      end;
      //Evaluando Condiciones y Nombre del Reporte
      case CmbTipoReporte.ItemIndex of
        0: // Reporte Detallado General
        begin
          case CmbClaseReporte.ItemIndex of
            0: // Solo Ventas
            begin
              Where := AgregarWhere(Where, '(Desde = ' + QuotedStr('FP')+ ')');
            end;
            1: // Solo Devoluciones
            begin
              Where := AgregarWhere(Where, '(Desde = ' + QuotedStr('NCP')+ ')')
            end;
            2: // Venta y Devoluciones
            begin

            end;
          end;
          NombreReporte := 'ProductosCompradosDetalladoGeneral';
          OrderBy := 'ORDER BY codigo_zona, codigo_estacion, codigo_producto';
        end;
        1: // Reporte Detallado Division
        begin
          case CmbClaseReporte.ItemIndex of
            0: // Solo Ventas
            begin
              Where := AgregarWhere(Where, '(Desde = ' + QuotedStr('FC')+ ')');
            end;
            1: // Solo Devoluciones
            begin
              Where := AgregarWhere(Where, '(Desde = ' + QuotedStr('NCC')+ ')')
            end;
            2: // Venta y Devoluciones
            begin

            end;
          end;
          NombreReporte := 'ProductosCompradosDetalladoDivision';
          OrderBy := 'ORDER BY codigo_zona, codigo_division';
        end;
        2: // Reporte Detallado Linea
        begin
          case CmbClaseReporte.ItemIndex of
            0: // Solo Ventas
            begin
              Where := AgregarWhere(Where, '(Desde = ' + QuotedStr('FC')+ ')');
            end;
            1: // Solo Devoluciones
            begin
              Where := AgregarWhere(Where, '(Desde = ' + QuotedStr('NCC')+ ')')
            end;
            2: // Venta y Devoluciones
            begin

            end;
          end;
          NombreReporte := 'ProductosCompradosDetalladoLinea';
          OrderBy := 'ORDER BY codigo_zona, codigo_linea';
        end;
        3: // Reporte Detallado Familia
        begin
          case CmbClaseReporte.ItemIndex of
            0: // Solo Ventas
            begin
              Where := AgregarWhere(Where, '(Desde = ' + QuotedStr('FC')+ ')');
            end;
            1: // Solo Devoluciones
            begin
              Where := AgregarWhere(Where, '(Desde = ' + QuotedStr('NCC')+ ')')
            end;
            2: // Venta y Devoluciones
            begin

            end;
          end;
          NombreReporte := 'ProductosCompradosDetalladoFamilia';
          OrderBy := 'ORDER BY codigo_zona, codigo_familia';
        end;
        4: // Reporte Detallado Clase
        begin
          case CmbClaseReporte.ItemIndex of
            0: // Solo Ventas
            begin
              Where := AgregarWhere(Where, '(Desde = ' + QuotedStr('FC')+ ')');
            end;
            1: // Solo Devoluciones
            begin
              Where := AgregarWhere(Where, '(Desde = ' + QuotedStr('NCC')+ ')')
            end;
            2: // Venta y Devoluciones
            begin

            end;
          end;
          NombreReporte := 'ProductosCompradosDetalladoClase';
          OrderBy := 'ORDER BY codigo_zona, codigo_clase';
        end;
        5: // Reporte Detallado Manufactura
        begin
          case CmbClaseReporte.ItemIndex of
            0: // Solo Ventas
            begin
              Where := AgregarWhere(Where, '(Desde = ' + QuotedStr('FC')+ ')');
            end;
            1: // Solo Devoluciones
            begin
              Where := AgregarWhere(Where, '(Desde = ' + QuotedStr('NCC')+ ')')
            end;
            2: // Venta y Devoluciones
            begin

            end;
          end;
          NombreReporte := 'ProductosCompradosDetalladoManufactura';
          OrderBy := 'ORDER BY codigo_zona, codigo_manufactura';
        end;
        6: // Reporte Resumido Division
        begin
          case CmbClaseReporte.ItemIndex of
            0: // Solo Ventas
            begin
              Where := AgregarWhere(Where, '(Desde = ' + QuotedStr('FC')+ ')');
            end;
            1: // Solo Devoluciones
            begin
              Where := AgregarWhere(Where, '(Desde = ' + QuotedStr('NCC')+ ')')
            end;
            2: // Venta y Devoluciones
            begin

            end;
          end;
          NombreReporte := 'ProductosCompradosResumidoDivision';
          OrderBy := 'ORDER BY codigo_zona, codigo_division';
        end;
        7: // Reporte Resumido Linea
        begin
          case CmbClaseReporte.ItemIndex of
            0: // Solo Ventas
            begin
              Where := AgregarWhere(Where, '(Desde = ' + QuotedStr('FC')+ ')');
            end;
            1: // Solo Devoluciones
            begin
              Where := AgregarWhere(Where, '(Desde = ' + QuotedStr('NCC')+ ')')
            end;
            2: // Venta y Devoluciones
            begin

            end;
          end;
          NombreReporte := 'ProductosCompradosResumidoLinea';
          OrderBy := 'ORDER BY codigo_zona, codigo_linea';
        end;
        8: // Reporte Resumido Familia
        begin
          case CmbClaseReporte.ItemIndex of
            0: // Solo Ventas
            begin
              Where := AgregarWhere(Where, '(Desde = ' + QuotedStr('FC')+ ')');
            end;
            1: // Solo Devoluciones
            begin
              Where := AgregarWhere(Where, '(Desde = ' + QuotedStr('NCC')+ ')')
            end;
            2: // Venta y Devoluciones
            begin

            end;
          end;
          NombreReporte := 'ProductosCompradosResumidoFamilia';
          OrderBy := 'ORDER BY codigo_zona, codigo_familia';
        end;
        9: // Reporte Resumido Clase
        begin
          case CmbClaseReporte.ItemIndex of
            0: // Solo Ventas
            begin
              Where := AgregarWhere(Where, '(Desde = ' + QuotedStr('FC')+ ')');
            end;
            1: // Solo Devoluciones
            begin
              Where := AgregarWhere(Where, '(Desde = ' + QuotedStr('NCC')+ ')')
            end;
            2: // Venta y Devoluciones
            begin

            end;
          end;
          NombreReporte := 'ProductosCompradosResumidoClase';
          OrderBy := 'ORDER BY codigo_zona, codigo_clase';
        end;
        10: // Reporte Resumido Manufactura
        begin
          case CmbClaseReporte.ItemIndex of
            0: // Solo Ventas
            begin
              Where := AgregarWhere(Where, '(Desde = ' + QuotedStr('FC')+ ')');
            end;
            1: // Solo Devoluciones
            begin
              Where := AgregarWhere(Where, '(Desde = ' + QuotedStr('NCC')+ ')')
            end;
            2: // Venta y Devoluciones
            begin

            end;
          end;
          NombreReporte := 'ProductosCompradosResumidoManufactura';
          OrderBy := 'ORDER BY codigo_zona, codigo_manufactura';
        end;
        11: // Reporte Resumido Producto
        begin
          case CmbClaseReporte.ItemIndex of
            0: // Solo Ventas
            begin
              Where := AgregarWhere(Where, '(Desde = ' + QuotedStr('FC')+ ')');
            end;
            1: // Solo Devoluciones
            begin
              Where := AgregarWhere(Where, '(Desde = ' + QuotedStr('NCC')+ ')')
            end;
            2: // Venta y Devoluciones
            begin

            end;
          end;
          NombreReporte := 'ProductosCompradosResumidoProducto';
          OrderBy := 'ORDER BY codigo_zona, codigo_producto';
        end;
      end;
      //Modificando Query SQL
      SQL.Add('SELECT *');
      SQL.Add('FROM public.v_rep_productos_comprados');
      if (Length(Trim(Where)) > 0) then
      begin
        SQL.Add('WHERE ' + Where);
      end;
      if (Length(Trim(OrderBy)) > 0) then
      begin
        SQL.Add(OrderBy);
      end;
      Reporte.ModificarSQLDriverDataView('DvrProductosComprados',SQL.Text);
    end;
    9://Libro Compras
    begin
      Where := AgregarWhere(Where, '(fecha_libros BETWEEN ' + QuotedStr(FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date))+ ' AND ' + QuotedStr(FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date)) + ')');
      if (CmbTipoReporte.ItemIndex = 0) then
      begin
        OrderBy := 'ORDER BY fecha_libros, id_documento';
      end
      else
      begin
        OrderBy := 'ORDER BY id_documento';
      end;
      NombreReporte := 'LibroCompras';
      //Modificando Query SQL
      SQL.Add('SELECT *');
      SQL.Add('FROM public.v_rep_libro_compra');
      if (Length(Trim(Where)) > 0) then
      begin
        SQL.Add('WHERE ' + Where);
      end;
      if (Length(Trim(OrderBy)) > 0) then
      begin
        SQL.Add(OrderBy);
      end;
      Reporte.ModificarSQLDriverDataView('DvrLibroCompra',SQL.Text);
    end;
    10://Libro Ventas
    begin
      Where := AgregarWhere(Where, '(fecha_libros BETWEEN ' + QuotedStr(FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date))+ ' AND ' + QuotedStr(FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date)) + ')');
      if (CmbTipoReporte.ItemIndex = 0) then
      begin
        OrderBy := 'ORDER BY fecha_libros, id_documento';
      end
      else
      begin
        OrderBy := 'ORDER BY id_documento';
      end;
      NombreReporte := 'LibroVentas';
      //Modificando Query SQL
      SQL.Add('SELECT *');
      SQL.Add('FROM public.v_rep_libro_venta');
      if (Length(Trim(Where)) > 0) then
      begin
        SQL.Add('WHERE ' + Where);
      end;
      if (Length(Trim(OrderBy)) > 0) then
      begin
        SQL.Add(OrderBy);
      end;
      Reporte.ModificarSQLDriverDataView('DvrLibroVenta',SQL.Text);
    end;
    11://Retencion IVA (Compras - Ventas)
    begin
      Case CmbTipoReporte.ItemIndex of
        0://Retencion IVA Global (Compras - Ventas)
        begin
          //NombreReporte := 'RetencionIVACompra';
        end;
        1://Retencion IVA Compras
        begin
          Where := AgregarWhere(Where, '(ret_fecha_ingreso BETWEEN ' + QuotedStr(':FechaDesde') + ' AND ' + QuotedStr(':FechaHasta') + ')');
          //Considerando Proveedores
          if (CmbProveedorDesde.Text <> '') and (CmbProveedorHasta.Text <> '') then
          begin
            Where := AgregarWhere(Where, '(ret_codigo_pro BETWEEN ' + QuotedStr(CmbProveedorDesde.Text) + ' AND ' + QuotedStr(CmbProveedorHasta.Text) + ')');
          end;
          NombreReporte := 'RetencionIVACompra';
          case CmbTipoOrdenamiento.ItemIndex of
            0://Fecha Documento
            begin
              OrderBy := 'ORDER BY fecha_documento';
            end;
            1://Fecha Contable
            begin
              OrderBy := 'ORDER BY ret_fecha_contable';
            end;
            2://Fecha Pago
            begin
              OrderBy := 'ORDER BY ret_fecha_ingreso';
            end;
            3://Consecutivo
            begin
              OrderBy := 'ORDER BY ret_numero';
            end;
          end;
          //Modificando Query SQL
          SQL.Add('SELECT *');
          SQL.Add('FROM public.v_rep_retencion_iva_compras');
          if (Length(Trim(Where)) > 0) then
          begin
            SQL.Add('WHERE ' + Where);
          end;
          if (Length(Trim(OrderBy)) > 0) then
          begin
            SQL.Add(OrderBy);
          end;
          Reporte.ModificarSQLDriverDataView('DvrRetencionIVACompras',SQL.Text);
        end;
        2://Retencion IVA Ventas
        begin
          //Considerando Proveedores
          if (CmbClienteDesde.Text <> '') and (CmbClienteHasta.Text <> '') then
          begin
            Where := AgregarWhere(Where, '(ret_codigo_pro BETWEEN ' + QuotedStr(CmbClienteDesde.Text) + ' AND ' + QuotedStr(CmbClienteHasta.Text) + ')');
          end;
          NombreReporte := 'RetencionIVAVenta';
        end;
        3://Retencion IVA Resumen Debitos y Creditos
        begin
          Where := AgregarWhere(Where, '(fecha_ingreso BETWEEN ' + QuotedStr(':FechaDesde') + ' AND ' + QuotedStr(':FechaHasta') + ')');
          NombreReporte := 'Resumen_Debitos_Creditos';
          case CmbTipoOrdenamiento.ItemIndex of
            0://Fecha Documento
            begin
              OrderBy := 'ORDER BY fecha_ingreso';
            end;
            1://Fecha Contable
            begin
              OrderBy := 'ORDER BY fecha_contable';
            end;
            2://Fecha Libro
            begin
              OrderBy := 'ORDER BY fecha_libros';
            end;
          end;
          //Modificando Query SQL
          SQL.Add('SELECT *');
          SQL.Add('FROM public.v_rep_resumen_debito_creditos');
          if (Length(Trim(Where)) > 0) then
          begin
            SQL.Add('WHERE ' + Where);
          end;
          if (Length(Trim(OrderBy)) > 0) then
          begin
            SQL.Add(OrderBy);
          end;
          Reporte.ModificarSQLDriverDataView('DvrResumen_Debitos_Creditos',SQL.Text);
        end;
      end;
    end;
    12://Impuestos Retenidos a Proveedores ISLR
    begin
      Case CmbTipoReporte.ItemIndex of
        0: //Clasificado por Tipo Retencion
        begin
          NombreReporte := 'ImpuestoRetenidoProveedor_ISLR_retencion';
          //Determinando Query del Reporte para todos los Proveedores
          if (CmbProveedorDesde.Text = '') and (CmbProveedorHasta.Text = '') then
          begin
            SQL.Add('SELECT *');
            SQL.Add('FROM public.v_rep_impuesto_retenido_proveedores_islr');
            SQL.Add('WHERE (fecha_ingreso BETWEEN ' + QuotedStr(':FechaDesde') + ' AND '+QuotedStr(':FechaHasta')+')');
            SQL.Add('ORDER BY codigo_retencion_1_det_islr, codigo_retencion_2_det_islr, codigo_retencion_3_det_islr, codigo_retencion_4_det_islr, codigo_retencion_5_det_islr');
          end
          else
          begin
            SQL.Add('SELECT *');
            SQL.Add('FROM public.v_rep_impuesto_retenido_proveedores_islr');
            SQL.Add('WHERE (fecha_ingreso BETWEEN ' + QuotedStr(':FechaDesde') + ' AND '+QuotedStr(':FechaHasta')+') AND (codigo_proveedor BETWEEN ' + QuotedStr(CmbProveedorDesde.Text) + ' AND ' + QuotedStr(CmbProveedorHasta.Text) + ')');
            SQL.Add('ORDER BY codigo_retencion_1_det_islr, codigo_retencion_2_det_islr, codigo_retencion_3_det_islr, codigo_retencion_4_det_islr, codigo_retencion_5_det_islr');
          end;
          Reporte.ModificarSQLDriverDataView('DvrImpuestoRetenidoProveedores',SQL.Text);
        end;
        1://Clasificado por Proveedor
        begin
          //Considerando Proveedores
          if (CmbProveedorDesde.Text <> '') and (CmbProveedorHasta.Text <> '') then
          begin
            Where := AgregarWhere(Where, '(codigo_proveedor BETWEEN ' + QuotedStr(CmbProveedorDesde.Text) + ' AND ' + QuotedStr(CmbProveedorHasta.Text) + ')');
          end;
          OrderBy := 'ORDER BY codigo_proveedor';
          NombreReporte := 'ImpuestoRetenidoProveedor_ISLR_Proveedor';
          //Modificando Query SQL
          SQL.Add('SELECT *');
          SQL.Add('FROM public.v_rep_impuesto_retenido_proveedores_islr');
          if (Length(Trim(Where)) > 0) then
          begin
            SQL.Add('WHERE ' + Where);
          end;
          if (Length(Trim(OrderBy)) > 0) then
          begin
            SQL.Add(OrderBy);
          end;
          Reporte.ModificarSQLDriverDataView('DvrImpuestoRetenidoProveedores',SQL.Text);
        end;
      end;
    end;
    13://Movimientos de Inventario
    begin
      //Configurando WHERE
      //Considerando Zona
      if (CmbZona.Text <> '') then
      begin
        Where := AgregarWhere(Where, '(dmi_codigo_zona = ' + QuotedStr(CmbZona.Text) + ')');
      end;
      //Considerando Estacion
      if (CmbEstacion.Text <> '') then
      begin
        Where := AgregarWhere(Where, '((dmi_codigo_estacion_salida = ' + QuotedStr(CmbEstacion.Text) + ') OR (dmi_codigo_estacion_entrada = ' + QuotedStr(CmbEstacion.Text) + '))');
      end;
      //Considerando Clasificacion 1 (Division)
      if (CmbClasif1.Text <> '') then
      begin
        Where := AgregarWhere(Where, '(pro_codigo_division = ' + QuotedStr(CmbClasif1.Text) + ')');
      end;
      //Considerando Clasificacion 2 (Linea)
      if (CmbClasif2.Text <> '') then
      begin
        Where := AgregarWhere(Where, '(pro_codigo_linea = ' + QuotedStr(CmbClasif2.Text) + ')');
      end;
      //Considerando Clasificacion  3 (Familia)
      if (CmbClasif3.Text <> '') then
      begin
        Where := AgregarWhere(Where, '(pro_codigo_familia = ' + QuotedStr(CmbClasif3.Text) + ')');
      end;
      //Considerando Clasificacion 4 (Clase)
      if (CmbClasif4.Text <> '') then
      begin
        Where := AgregarWhere(Where, '(pro_codigo_clase = ' + QuotedStr(CmbClasif4.Text) + ')');
      end;
      //Considerando Clasificacion 5 (Manufactura)
      if (CmbClasif5.Text <> '') then
      begin
        Where := AgregarWhere(Where, '(pro_codigo_manufactura = ' + QuotedStr(CmbClasif5.Text) + ')');
      end;
      //Considerando Productos
      if (CmbProductoDesde.Text <> '') and (CmbProductoHasta.Text <> '') then
      begin
        Where := AgregarWhere(Where, '(dmi_codigo_producto BETWEEN ' + QuotedStr(CmbProductoDesde.Text) + ' AND ' + QuotedStr(CmbProductoHasta.Text) + ')');
      end;
      //Evaluando Condiciones y Nombre del Reporte
      case CmbTipoReporte.ItemIndex of
        0: // Reporte Detallado General
        begin
          case CmbClaseReporte.ItemIndex of
            0: //
            begin

            end;
            1: //
            begin

            end;
            2: //
            begin

            end;
          end;
          NombreReporte := 'MovimientosInventarioDetalladoProducto';
          //OrderBy := 'ORDER BY codigo_zona, codigo_producto';
          OrderBy := 'ORDER BY dmi_codigo_zona, dmi_codigo_producto';
        end;
        1: // Reporte Detallado Division
        begin
          case CmbClaseReporte.ItemIndex of
            0: //
            begin

            end;
            1: //
            begin

            end;
            2: //
            begin

            end;
          end;
          NombreReporte := 'MovimientosInventarioDetalladoDivision';
          OrderBy := 'ORDER BY dmi_codigo_zona, pro_codigo_division';
        end;
        2: // Reporte Detallado Linea
        begin
          case CmbClaseReporte.ItemIndex of
            0: //
            begin

            end;
            1: //
            begin

            end;
            2: //
            begin

            end;
          end;
          NombreReporte := 'MovimientosInventarioDetalladoLinea';
          OrderBy := 'ORDER BY dmi_codigo_zona, pro_codigo_linea';
        end;
        3: // Reporte Detallado Familia
        begin
          case CmbClaseReporte.ItemIndex of
            0: //
            begin

            end;
            1: //
            begin

            end;
            2: //
            begin

            end;
          end;
          NombreReporte := 'MovimientosInventarioDetalladoFamilia';
          OrderBy := 'ORDER BY dmi_codigo_zona, pro_codigo_familia';
        end;
        4: // Reporte Detallado Clase
        begin
          case CmbClaseReporte.ItemIndex of
            0: //
            begin

            end;
            1: //
            begin

            end;
            2: //
            begin

            end;
          end;
          NombreReporte := 'MovimientosInventarioDetalladoClase';
          OrderBy := 'ORDER BY dmi_codigo_zona, pro_codigo_clase';
        end;
        5: // Reporte Detallado Manufactura
        begin
          case CmbClaseReporte.ItemIndex of
            0: //
            begin

            end;
            1: //
            begin

            end;
            2: //
            begin

            end;
          end;
          NombreReporte := 'MovimientosInventarioDetalladoManufactura';
          OrderBy := 'ORDER BY dmi_codigo_zona, pro_codigo_manufactura';
        end;
        6: // Reporte Resumido Division
        begin
          case CmbClaseReporte.ItemIndex of
            0: //
            begin

            end;
            1: //
            begin

            end;
            2: //
            begin

            end;
          end;
          NombreReporte := 'MovimientosInventarioResumidoDivision';
          OrderBy := 'ORDER BY dmi_codigo_zona, pro_codigo_division';
        end;
        7: // Repote Resumido Linea
        begin
          case CmbClaseReporte.ItemIndex of
            0: //
            begin

            end;
            1: //
            begin

            end;
            2: //
            begin

            end;
          end;
          NombreReporte := 'MovimientosInventarioResumidoLinea';
          OrderBy := 'ORDER BY dmi_codigo_zona, pro_codigo_linea';
        end;
        8: // Reporte Resumido Familia
        begin
          case CmbClaseReporte.ItemIndex of
            0: //
            begin

            end;
            1: //
            begin

            end;
            2: //
            begin

            end;
          end;
          NombreReporte := 'MovimientosInventarioResumidoFamilia';
          OrderBy := 'ORDER BY dmi_codigo_zona, pro_codigo_familia';
        end;
        9: // Reporte Resumido Clase
        begin
          case CmbClaseReporte.ItemIndex of
            0: //
            begin

            end;
            1: //
            begin

            end;
            2: //
            begin

            end;
          end;
          NombreReporte := 'MovimientosInventarioResumidoClase';
          OrderBy := 'ORDER BY dmi_codigo_zona, pro_codigo_clase';
        end;
        10: // Reporte Resumido Manufactura
        begin
          case CmbClaseReporte.ItemIndex of
            0: //
            begin

            end;
            1: //
            begin

            end;
            2: //
            begin

            end;
          end;
          NombreReporte := 'MovimientosInventarioResumidoManufactura';
          OrderBy := 'ORDER BY dmi_codigo_zona, pro_codigo_manufactura';
        end;
        11: // Reporte Resumido Producto
        begin
          case CmbClaseReporte.ItemIndex of
            0: //
            begin

            end;
            1: //
            begin

            end;
            2: //
            begin

            end;
          end;
          NombreReporte := 'MovimientosInventarioResumidoProducto';
          OrderBy := 'ORDER BY dmi_codigo_zona, dmi_codigo_producto';
        end;
      end;
      //Modificando Query SQL
      SQL.Add('SELECT *');
      SQL.Add('FROM public.v_rep_detalle_movimientos_inventario');
      if (Length(Trim(Where)) > 0) then
      begin
        SQL.Add('WHERE ' + Where);
      end;
      if (Length(Trim(OrderBy)) > 0) then
      begin
        SQL.Add(OrderBy);
      end;
      Reporte.ModificarSQLDriverDataView('DvrMovimientosInventario',SQL.Text);
    end;
    14://Exitencia Inventario - Libro Inventario
    begin
      //Configurando WHERE
      Where := AgregarWhere(Where, '(fecha BETWEEN ' + QuotedStr(FormatDateTime('YYYY-mm-dd',DatFechaDesde.Date))+ ' AND ' + QuotedStr(FormatDateTime('YYYY-mm-dd',DatFechaHasta.Date)) + ')');
      //Considerando Zona
      if (CmbZona.Text <> '') then
      begin
        Where := AgregarWhere(Where, '(codigo_zona = ' + QuotedStr(CmbZona.Text) + ')');
      end;
      //Considerando Estacion
      if (CmbEstacion.Text <> '') then
      begin
        Where := AgregarWhere(Where, '((estacion_salida = ' + QuotedStr(CmbEstacion.Text) + ') OR (estacion_entrada = ' + QuotedStr(CmbEstacion.Text) + '))');
      end;
      //Considerando Clasificacion 1 (Division)
      if (CmbClasif1.Text <> '') then
      begin
        Where := AgregarWhere(Where, '(codigo_division = ' + QuotedStr(CmbClasif1.Text) + ')');
      end;
      //Considerando Clasificacion 2 (Linea)
      if (CmbClasif2.Text <> '') then
      begin
        Where := AgregarWhere(Where, '(codigo_linea = ' + QuotedStr(CmbClasif2.Text) + ')');
      end;
      //Considerando Clasificacion  3 (Familia)
      if (CmbClasif3.Text <> '') then
      begin
        Where := AgregarWhere(Where, '(codigo_familia = ' + QuotedStr(CmbClasif3.Text) + ')');
      end;
      //Considerando Clasificacion 4 (Clase)
      if (CmbClasif4.Text <> '') then
      begin
        Where := AgregarWhere(Where, '(codigo_clase = ' + QuotedStr(CmbClasif4.Text) + ')');
      end;
      //Considerando Clasificacion 5 (Manufactura)
      if (CmbClasif5.Text <> '') then
      begin
        Where := AgregarWhere(Where, '(codigo_manufactura = ' + QuotedStr(CmbClasif5.Text) + ')');
      end;
      //Considerando Productos
      if (CmbProductoDesde.Text <> '') and (CmbProductoHasta.Text <> '') then
      begin
        Where := AgregarWhere(Where, '(codigo_producto BETWEEN ' + QuotedStr(CmbProductoDesde.Text) + ' AND ' + QuotedStr(CmbProductoHasta.Text) + ')');
      end;
      //Evaluando Condiciones y Nombre del Reporte
      case CmbTipoReporte.ItemIndex of
        0: // Reporte Detallado General
        begin
          NombreReporte := 'ExistenciaDetalladoProducto';
          //OrderBy := 'ORDER BY codigo_zona, codigo_producto';
          OrderBy := 'ORDER BY codigo_producto';
        end;
        1: // Reporte Detallado Division
        begin
          NombreReporte := 'ExistenciaDetalladoDivision';
          OrderBy := 'ORDER BY codigo_division, codigo_producto';
        end;
        2: // Reporte Detallado Linea
        begin
          NombreReporte := 'ExistenciaDetalladoLinea';
          OrderBy := 'ORDER BY codigo_linea, codigo_producto';
        end;
        3: // Reporte Detallado Familia
        begin
          NombreReporte := 'ExistenciaDetalladoFamilia';
          OrderBy := 'ORDER BY codigo_familia, codigo_producto';
        end;
        4: // Reporte Detallado Clase
        begin
          NombreReporte := 'ExistenciaDetalladoClase';
          OrderBy := 'ORDER BY codigo_clase, codigo_producto';
        end;
        5: // Reporte Detallado Manufactura
        begin
          NombreReporte := 'ExistenciaDetalladoManufactura';
          OrderBy := 'ORDER BY codigo_manufactura, codigo_producto';
        end;
        6: // Reporte Resumido Producto
        begin
          NombreReporte := 'ExistenciaResumidoProducto';
          OrderBy := 'ORDER BY codigo_producto, codigo_producto';
        end;
        7: // Reporte Resumido Division
        begin
          NombreReporte := 'ExistenciaResumidoDivision';
          OrderBy := 'ORDER BY codigo_division, codigo_producto';
        end;
        8: // Repote Resumido Linea
        begin
          NombreReporte := 'ExistenciaResumidoLinea';
          OrderBy := 'ORDER BY codigo_linea, codigo_producto';
        end;
        9: // Reporte Resumido Familia
        begin
          NombreReporte := 'ExistenciaResumidoFamilia';
          OrderBy := 'ORDER BY codigo_familia, codigo_producto';
        end;
        10: // Reporte Resumido Clase
        begin
          NombreReporte := 'ExistenciaResumidoClase';
          OrderBy := 'ORDER BY codigo_clase, codigo_producto';
        end;
        11: // Reporte Resumido Manufactura
        begin
          NombreReporte := 'ExistenciaResumidoManufactura';
          OrderBy := 'ORDER BY codigo_manufactura, codigo_producto';
        end;
      end;
      //Modificando Query SQL
      SQL.Add('SELECT *');
      SQL.Add('FROM public.v_rep_existencias');
      if (Length(Trim(Where)) > 0) then
      begin
        SQL.Add('WHERE ' + Where);
      end;
      if (Length(Trim(OrderBy)) > 0) then
      begin
        SQL.Add(OrderBy);
      end;
      Reporte.ModificarSQLDriverDataView('DvrExistencias',SQL.Text);
    end;
  end;
  CargarParametros;
  if (not Reporte.EmiteReporte(NombreReporte,trGeneral)) then
  begin
    MessageBox(Self.Handle, 'Los Par�metros Seleccionados No Devolvieron Resultados!!!', PChar(MsgTituloInformacion), MB_ICONEXCLAMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end;
  Screen.Cursor := crDefault;
end;

procedure TFrmReporteGeneral.Deshabilitar;
begin
  //Periodos
  LblPeriodo.Enabled := False;
  LblFechaAl.Enabled := False; DatFechaAl.Enabled := False;
  LblFechaDesde.Enabled := False; DatFechaDesde.Enabled := False;
  LblFechaHasta.Enabled := False; DatFechaHasta.Enabled := False;
  DatFechaAl.Date := Date;
  DatFechaDesde.Date := Date;
  DatFechaHasta.Date := Date;
  //Rango de Clientes
  LblRangoCliente.Enabled := False;
  LblClienteDesde.Enabled := False; CmbClienteDesde.Enabled := False;
  LblClienteHasta.Enabled := False; CmbClienteHasta.Enabled := False;
  CmbClienteDesde.KeyValue := '';
  CmbClienteHasta.KeyValue := '';
  //Rango de Proveedores
  LblRangoProveedor.Enabled := False;
  LblProveedorDesde.Enabled := False; CmbProveedorDesde.Enabled := False;
  LblProveedorHasta.Enabled := False; CmbProveedorHasta.Enabled := False;
  CmbProveedorDesde.KeyValue := '';
  CmbProveedorHasta.KeyValue := '';
  //Rango de Productos
  LblRangoProducto.Enabled := False;
  LblProductoDesde.Enabled := False; CmbProductoDesde.Enabled := False;
  LblProductoHasta.Enabled := False; CmbProductoHasta.Enabled := False;
  CmbProductoDesde.KeyValue := '';
  CmbProductoHasta.KeyValue := '';
  //Clasificaciones de Productos
  LblClasificacionProducto.Enabled := False;
  LblClasif1.Enabled := False; CmbClasif1.Enabled := False;
  LblClasif2.Enabled := False; CmbClasif2.Enabled := False;
  LblClasif3.Enabled := False; CmbClasif3.Enabled := False;
  LblClasif4.Enabled := False; CmbClasif4.Enabled := False;
  LblClasif5.Enabled := False; CmbClasif5.Enabled := False;
  LblClasif6.Enabled := False; CmbClasif6.Enabled := False;
  CmbClasif1.KeyValue := '';
  CmbClasif2.KeyValue := '';
  CmbClasif3.KeyValue := '';
  CmbClasif4.KeyValue := '';
  CmbClasif5.KeyValue := '';
  CmbClasif6.KeyValue := '';
  //Otros
  LblOtros.Enabled := False;
  LblEstacion.Enabled := False; CmbEstacion.Enabled := False;
  LblZona.Enabled := False; CmbZona.Enabled := False;
  LblMoneda.Enabled := False; CmbMoneda.Enabled := False;
  LblCuentaBancaria.Enabled := False; CmbCuentaBancaria.Enabled := False;
  LblVendedor.Enabled := False; CmbVendedor.Enabled := False;
  CmbEstacion.KeyValue := '';
  CmbZona.KeyValue := '';
  CmbMoneda.KeyValue := '';
  CmbCuentaBancaria.KeyValue := '';
  CmbVendedor.KeyValue := '';
  //Tipo de Reporte
  LblTipoReporte.Enabled := False;
  CmbTipoReporte.Enabled := False;
  CmbTipoReporte.Clear;
  CmbTipoReporte.Items.Clear;
  //Clase de Reporte
  LblClaseReporte.Enabled := False;
  CmbClaseReporte.Enabled := False;
  CmbClaseReporte.Clear;
  CmbClaseReporte.Items.Clear;
  //Tipo Ordenamiento
  LblTipoOrdenamiento.Enabled := False;
  CmbTipoOrdenamiento.Enabled := False;
  CmbTipoOrdenamiento.Clear;
  CmbTipoOrdenamiento.Items.Clear;
end;

constructor TFrmReporteGeneral.Create(AOwner: TComponent;
  ALoginUsuario: String);
begin
  inherited Create(AOwner);
  User := TUser.Create;
  User.SetLoginUsuario(ALoginUsuario);
  User.Buscar;
  FrmActividadSophie.Show;
  FrmActividadSophie.Actividad('Cargando N�cleo...');
  Reporte := TReporte.Create;
  (* Inicializando Clases Temporales *)
  Cliente := TCliente.Create;
  Proveedor := TProveedor.Create;
  Producto := TProducto.Create;
  Division := TDivision.Create;
  Linea := TLinea.Create;
  Familia := TFamilia.Create;
  Clase := TClase.Create;
  Manufactura := TManufactura.Create;
  Arancel := TArancel.Create;
  Estacion := TEstacion.Create;
  Zona := TZona.Create;
  Moneda := TMoneda.Create;
  CuentaBancaria := TCuentaBancaria.Create;
  Vendedor:= TVendedor.Create;
  (* Inicializando Controles Adicionales *)
  FrmActividadSophie.Actividad('Cargando Clientes...');
  DataClienteDesde := Cliente.ObtenerCombo;
  DataClienteHasta := Cliente.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Proveedores...');
  DataProveedorDesde := Proveedor.ObtenerCombo;
  DataProveedorHasta := Proveedor.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Productos...');
  DataProductoDesde := Producto.ObtenerCombo;
  DataProductoHasta := Producto.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Divisiones...');
  DataClasif1 := Division.ObtenerLista;
  FrmActividadSophie.Actividad('Cargando Lineas...');
  DataClasif2 := Linea.ObtenerLista;
  FrmActividadSophie.Actividad('Cargando Familias...');
  DataClasif3 := Familia.ObtenerLista;
  FrmActividadSophie.Actividad('Cargando Clases...');
  DataClasif4 := Clase.ObtenerLista;
  FrmActividadSophie.Actividad('Cargando Manufacturas...');
  DataClasif5 := Manufactura.ObtenerLista;
  FrmActividadSophie.Actividad('Cargando Arancel...');
  DataClasif6 := Arancel.ObtenerLista;
  FrmActividadSophie.Actividad('Cargando Estaciones...');
  DataEstacion := Estacion.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Zonas...');
  DataZona := Zona.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Monedas...');
  DataMoneda := Moneda.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Cuentas Bancarias...');
  DataCuentaBancaria := CuentaBancaria.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Cuentas Vendedores...');
  DataVendedor := Vendedor.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Resto del N�cleo...');
  DSClienteDesde.DataSet := DataClienteDesde;
  DSClienteHasta.DataSet := DataClienteHasta;
  DSProveedorDesde.DataSet := DataProveedorDesde;
  DSProveedorHasta.DataSet := DataProveedorHasta;
  DSProductoDesde.DataSet := DataProductoDesde;
  DSProductoHasta.DataSet := DataProductoHasta;
  DSClasif1.DataSet := DataClasif1;
  DSClasif2.DataSet := DataClasif2;
  DSClasif3.DataSet := DataClasif3;
  DSClasif4.DataSet := DataClasif4;
  DSClasif5.DataSet := DataClasif5;
  DSClasif6.DataSet := DataClasif6;
  DSEstacion.DataSet := DataEstacion;
  DSZona.DataSet := DataZona;
  DSMoneda.DataSet := DataMoneda;
  DSCuentaBancaria.DataSet := DataCuentaBancaria;
  DsVendedor.DataSet := DataVendedor;
  //Configurando RxDBLookupComboBox
  ConfigurarRxDBLookupCombo(CmbClienteDesde,DSClienteDesde,'codigo','codigo;razon_social');
  ConfigurarRxDBLookupCombo(CmbClienteHasta,DSClienteHasta,'codigo','codigo;razon_social');
  ConfigurarRxDBLookupCombo(CmbProveedorDesde,DSProveedorDesde,'codigo','codigo;razon_social');
  ConfigurarRxDBLookupCombo(CmbProveedorHasta,DSProveedorHasta,'codigo','codigo;razon_social');
  ConfigurarRxDBLookupCombo(CmbProductoDesde,DSProductoDesde,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbProductoHasta,DSProductoHasta,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbClasif1,DSClasif1,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbClasif2,DSClasif2,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbClasif3,DSClasif3,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbClasif4,DSClasif4,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbClasif5,DSClasif5,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbClasif6,DSClasif6,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbEstacion,DSEstacion,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbZona,DSZona,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbMoneda,DSMoneda,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCuentaBancaria,DSCuentaBancaria,'codigo','codigo;entidad;numero');
  ConfigurarRxDBLookupCombo(CmbVendedor,DsVendedor,'codigo','codigo;descripcion');
  FrmActividadSophie.Close;
end;

procedure TFrmReporteGeneral.BtnCerrarClick(Sender: TObject);
begin
  Close;
end;

procedure TFrmReporteGeneral.BtnInicializarClick(Sender: TObject);
begin
  Inicializar;
end;

procedure TFrmReporteGeneral.BtnEmitirClick(Sender: TObject);
begin
  if (Validar) then
  begin
    if (MessageBox(Self.Handle, '�Esta Seguro Que Los Parametros Estan Correctos?', PChar(MsgTituloInformacion), MB_ICONQUESTION or MB_YESNO or MB_TOPMOST or MB_TASKMODAL) = mrYes) then
    begin
      EmitirReporte;
    end;
  end;
end;

procedure TFrmReporteGeneral.GrpListadoReportesClick(Sender: TObject);
begin
  SeleccionReporte;
end;

procedure TFrmReporteGeneral.FormCreate(Sender: TObject);
begin
  Inicializar;
end;

procedure TFrmReporteGeneral.CmbClasif1Change(Sender: TObject);
begin
  case GrpListadoReportes.ItemIndex of
    0:
    begin
    end;
    6:
    begin
      if Length(Trim(CmbClasif1.Text)) > 0 then
      begin
        LblRangoProducto.Enabled := False;
        LblProductoDesde.Enabled := False; CmbProductoDesde.Enabled := False;
        LblProductoHasta.Enabled := False; CmbProductoHasta.Enabled := False;
      end;
    end;
  end;
end;

procedure TFrmReporteGeneral.CmbClasif2Change(Sender: TObject);
begin
  case GrpListadoReportes.ItemIndex of
    0:
    begin
    end;
    6:
    begin
      if Length(Trim(CmbClasif2.Text)) > 0 then
      begin
        LblRangoProducto.Enabled := False;
        LblProductoDesde.Enabled := False; CmbProductoDesde.Enabled := False;
        LblProductoHasta.Enabled := False; CmbProductoHasta.Enabled := False;
      end;
    end;
  end;
end;

procedure TFrmReporteGeneral.CmbClasif3Change(Sender: TObject);
begin
  case GrpListadoReportes.ItemIndex of
    0:
    begin
    end;
    6:
    begin
      if Length(Trim(CmbClasif3.Text)) > 0 then
      begin
        LblRangoProducto.Enabled := False;
        LblProductoDesde.Enabled := False; CmbProductoDesde.Enabled := False;
        LblProductoHasta.Enabled := False; CmbProductoHasta.Enabled := False;
      end;
    end;
  end;
end;

procedure TFrmReporteGeneral.CmbClasif4Change(Sender: TObject);
begin
  case GrpListadoReportes.ItemIndex of
    0:
    begin
    end;
    6:
    begin
      if Length(Trim(CmbClasif4.Text)) > 0 then
      begin
        LblRangoProducto.Enabled := False;
        LblProductoDesde.Enabled := False; CmbProductoDesde.Enabled := False;
        LblProductoHasta.Enabled := False; CmbProductoHasta.Enabled := False;
      end;
    end;
  end;
end;

procedure TFrmReporteGeneral.CmbClasif5Change(Sender: TObject);
begin
  case GrpListadoReportes.ItemIndex of
    0:
    begin
    end;
    6:
    begin
      if Length(Trim(CmbClasif5.Text)) > 0 then
      begin
        LblRangoProducto.Enabled := False;
        LblProductoDesde.Enabled := False; CmbProductoDesde.Enabled := False;
        LblProductoHasta.Enabled := False; CmbProductoHasta.Enabled := False;
      end;
    end;
  end;
end;

procedure TFrmReporteGeneral.CmbClasif6Change(Sender: TObject);
begin
  case GrpListadoReportes.ItemIndex of
    0:
    begin
    end;
    6:
    begin
      if Length(Trim(CmbClasif6.Text)) > 0 then
      begin
        LblRangoProducto.Enabled := False;
        LblProductoDesde.Enabled := False; CmbProductoDesde.Enabled := False;
        LblProductoHasta.Enabled := False; CmbProductoHasta.Enabled := False;
      end;
    end;
  end;
end;

procedure TFrmReporteGeneral.CmbClienteDesdeCloseUp(Sender: TObject);
begin
  CmbClienteHasta.KeyValue := CmbClienteDesde.KeyValue;
end;

procedure TFrmReporteGeneral.CmbClienteDesdeKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if (Key = VK_ESCAPE) then
  begin
    CmbClienteHasta.KeyValue := '';
  end;
end;

procedure TFrmReporteGeneral.CmbProductoDesdeCloseUp(Sender: TObject);
begin
  CmbProductoHasta.KeyValue := CmbProductoDesde.KeyValue;
end;

procedure TFrmReporteGeneral.CmbProductoDesdeKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if (Key = VK_ESCAPE) then
  begin
    CmbProductoHasta.KeyValue := '';
  end;
end;

procedure TFrmReporteGeneral.CmbProveedorDesdeCloseUp(Sender: TObject);
begin
  CmbProveedorHasta.KeyValue := CmbProveedorDesde.KeyValue;
end;

procedure TFrmReporteGeneral.CmbProveedorDesdeKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if (Key = VK_ESCAPE) then
  begin
    CmbProveedorHasta.KeyValue := '';
  end;
end;

procedure TFrmReporteGeneral.CmbTipoReporteClick(Sender: TObject);
begin
  case GrpListadoReportes.ItemIndex of
    0:
    begin
    end;
    1:
    begin
    end;
    2://Estados de Cuentas Bancarios
    begin
      case CmbTipoReporte.ItemIndex of
        0://Estadar
        begin
          DataCuentaBancaria.First;
          CmbCuentaBancaria.KeyValue := DataCuentaBancaria.FieldValues['codigo'];
          LblOtros.Enabled := True;
          LblCuentaBancaria.Enabled := True;
          CmbCuentaBancaria.Enabled := True;
        end;
        1://Cosolidado de Cuentas
        begin
          CmbCuentaBancaria.KeyValue := '';
          LblOtros.Enabled := False;
          LblCuentaBancaria.Enabled := False;
          CmbCuentaBancaria.Enabled := False;
        end;
      end;
    end;
    3://Estado de Cuentas de Clientes
    begin
      Case CmbTipoReporte.ItemIndex of
        0://Detallado
        begin
          LblFechaAl.Enabled := False; DatFechaAl.Enabled := False;
          LblFechaDesde.Enabled := True; DatFechaDesde.Enabled := True;
          LblFechaHasta.Enabled := True; DatFechaHasta.Enabled := True;
          LblTipoOrdenamiento.Caption := 'Utilizar Fecha:';
        end;
        1://Resumido
        begin
          LblFechaAl.Enabled := False; DatFechaAl.Enabled := False;
          LblFechaDesde.Enabled := True; DatFechaDesde.Enabled := True;
          LblFechaHasta.Enabled := True; DatFechaHasta.Enabled := True;
          LblTipoOrdenamiento.Caption := 'Utilizar Fecha:';
        end;
        2://Analisis de Vencimiento Detallado
        begin
          LblFechaAl.Enabled := True; DatFechaAl.Enabled := True;
          LblFechaDesde.Enabled := False; DatFechaDesde.Enabled := False;
          LblFechaHasta.Enabled := False; DatFechaHasta.Enabled := False;
          LblTipoOrdenamiento.Caption := 'Utilizar Fecha:';
        end;
        3://Analisis de Vencimiento Resumido
        begin
          LblFechaAl.Enabled := True; DatFechaAl.Enabled := True;
          LblFechaDesde.Enabled := False; DatFechaDesde.Enabled := False;
          LblFechaHasta.Enabled := False; DatFechaHasta.Enabled := False;
          LblTipoOrdenamiento.Caption := 'Utilizar Fecha:';
        end;
        4://Analisis de Vencimiento Detallado (Modelo Dias)
        begin
          LblFechaAl.Enabled := True; DatFechaAl.Enabled := True;
          LblFechaDesde.Enabled := False; DatFechaDesde.Enabled := False;
          LblFechaHasta.Enabled := False; DatFechaHasta.Enabled := False;
          LblTipoOrdenamiento.Caption := 'Utilizar Fecha:';
        end;
        5://Analisis de Vencimiento Resumido (Modelo Dias)
        begin
          LblFechaAl.Enabled := True; DatFechaAl.Enabled := True;
          LblFechaDesde.Enabled := False; DatFechaDesde.Enabled := False;
          LblFechaHasta.Enabled := False; DatFechaHasta.Enabled := False;
          LblTipoOrdenamiento.Caption := 'Utilizar Fecha:';
        end;
      end;
    end;
    4://Estado de Cuentas de Proveedores
    begin
      Case CmbTipoReporte.ItemIndex of
        0://Detallado
        begin
          LblFechaAl.Enabled := False; DatFechaAl.Enabled := False;
          LblFechaDesde.Enabled := True; DatFechaDesde.Enabled := True;
          LblFechaHasta.Enabled := True; DatFechaHasta.Enabled := True;
          LblTipoOrdenamiento.Caption := 'Utilizar Fecha:';
        end;
        1://Resumido
        begin
          LblFechaAl.Enabled := False; DatFechaAl.Enabled := False;
          LblFechaDesde.Enabled := True; DatFechaDesde.Enabled := True;
          LblFechaHasta.Enabled := True; DatFechaHasta.Enabled := True;
          LblTipoOrdenamiento.Caption := 'Utilizar Fecha:';
        end;
        2://Analisis de Vencimiento Detallado
        begin
          LblFechaAl.Enabled := True; DatFechaAl.Enabled := True;
          LblFechaDesde.Enabled := False; DatFechaDesde.Enabled := False;
          LblFechaHasta.Enabled := False; DatFechaHasta.Enabled := False;
          LblTipoOrdenamiento.Caption := 'Utilizar Fecha:';
        end;
        3://Analisis de Vencimiento Resumido
        begin
          LblFechaAl.Enabled := True; DatFechaAl.Enabled := True;
          LblFechaDesde.Enabled := False; DatFechaDesde.Enabled := False;
          LblFechaHasta.Enabled := False; DatFechaHasta.Enabled := False;
          LblTipoOrdenamiento.Caption := 'Utilizar Fecha:';
        end;
      end;
    end;
    5://Comision Vendedores
    begin
      Case CmbTipoReporte.ItemIndex of
        0://Detallado
        begin
          LblFechaAl.Enabled := False; DatFechaAl.Enabled := False;
          LblFechaDesde.Enabled := True; DatFechaDesde.Enabled := True;
          LblFechaHasta.Enabled := True; DatFechaHasta.Enabled := True;
          LblVendedor.Enabled := True; CmbVendedor.Enabled := True;
          LblZona.Enabled := True; CmbZona.Enabled := True;
        end;
        1://Resumido
        begin
          LblFechaAl.Enabled := False; DatFechaAl.Enabled := False;
          LblFechaDesde.Enabled := True; DatFechaDesde.Enabled := True;
          LblFechaHasta.Enabled := True; DatFechaHasta.Enabled := True;
          LblVendedor.Enabled := True; CmbVendedor.Enabled := True;
          LblZona.Enabled := True; CmbZona.Enabled := True;
        end;
        2://Detallado Todos los Vendedores por Zona
        begin
          LblFechaAl.Enabled := False; DatFechaAl.Enabled := False;
          LblFechaDesde.Enabled := True; DatFechaDesde.Enabled := True;
          LblFechaHasta.Enabled := True; DatFechaHasta.Enabled := True;
          LblVendedor.Enabled := False; CmbVendedor.Enabled := False;
          LblZona.Enabled := True; CmbZona.Enabled := True;
        end;
        3://Resumido  Todos los Vendedores por Zona
        begin
          LblFechaAl.Enabled := False; DatFechaAl.Enabled := False;
          LblFechaDesde.Enabled := True; DatFechaDesde.Enabled := True;
          LblFechaHasta.Enabled := True; DatFechaHasta.Enabled := True;
          LblVendedor.Enabled := False; CmbVendedor.Enabled := False;
          LblZona.Enabled := True; CmbZona.Enabled := True;
        end;
        4://Detallado Todas las Zonas por Vendedores
        begin
          LblFechaAl.Enabled := False; DatFechaAl.Enabled := False;
          LblFechaDesde.Enabled := True; DatFechaDesde.Enabled := True;
          LblFechaHasta.Enabled := True; DatFechaHasta.Enabled := True;
          LblVendedor.Enabled := True; CmbVendedor.Enabled := True;
          LblZona.Enabled := False; CmbZona.Enabled := False;
        end;
        5://Resumido Todas las Zonas por Vendedores
        begin
          LblFechaAl.Enabled := False; DatFechaAl.Enabled := False;
          LblFechaDesde.Enabled := True; DatFechaDesde.Enabled := True;
          LblFechaHasta.Enabled := True; DatFechaHasta.Enabled := True;
          LblVendedor.Enabled := True; CmbVendedor.Enabled := True;
          LblZona.Enabled := False; CmbZona.Enabled := False;
        end;
      end;
    end;
    6:
    begin
    end;
    7:
    begin
    end;
    8:
    begin
    end;
    9:
    begin
    end;
    10://Retencion IVA (Compras - Ventas)
    begin
      Case CmbTipoReporte.ItemIndex of
        0://Retencion IVA Global Compras - Ventas
        begin
          LblFechaAl.Enabled := False; DatFechaAl.Enabled := False;
          LblFechaDesde.Enabled := True; DatFechaDesde.Enabled := True;
          LblFechaHasta.Enabled := True; DatFechaHasta.Enabled := True;
          LblRangoProveedor.Enabled := False;
          LblProveedorDesde.Enabled := False;
          LblProveedorHasta.Enabled := False;
          CmbProveedorDesde.Enabled := False;
          CmbProveedorHasta.Enabled := False;
          LblRangoCliente.Enabled := False;
          LblClienteDesde.Enabled := False;
          LblClienteHasta.Enabled := False;
          CmbClienteDesde.Enabled := False;
          CmbClienteHasta.Enabled := False;
          CmbTipoOrdenamiento.Items.Clear;
          CmbTipoOrdenamiento.Items.Add('Fecha Documento');
          CmbTipoOrdenamiento.Items.Add('Fecha Contable');
          CmbTipoOrdenamiento.Items.Add('Fecha Pago');
          CmbTipoOrdenamiento.Items.Add('Consecutivo');
          CmbTipoOrdenamiento.ItemIndex := 0;
          LblTipoOrdenamiento.Caption := 'Tipo Ordenamiento:';
        end;
        1://Retencion IVA Compras
        begin
          LblFechaAl.Enabled := False; DatFechaAl.Enabled := False;
          LblFechaDesde.Enabled := True; DatFechaDesde.Enabled := True;
          LblFechaHasta.Enabled := True; DatFechaHasta.Enabled := True;
          LblRangoCliente.Enabled := False;
          LblClienteDesde.Enabled := False;
          LblClienteHasta.Enabled := False;
          CmbClienteDesde.Enabled := False;
          CmbClienteHasta.Enabled := False;
          LblRangoProveedor.Enabled := True;
          LblProveedorDesde.Enabled := True;
          LblProveedorHasta.Enabled := True;
          CmbProveedorDesde.Enabled := True;
          CmbProveedorHasta.Enabled := True;
          CmbTipoOrdenamiento.Items.Clear;
          CmbTipoOrdenamiento.Items.Add('Fecha Documento');
          CmbTipoOrdenamiento.Items.Add('Fecha Contable');
          CmbTipoOrdenamiento.Items.Add('Fecha Pago');
          CmbTipoOrdenamiento.Items.Add('Consecutivo');
          CmbTipoOrdenamiento.ItemIndex := 0;
          LblTipoOrdenamiento.Caption := 'Tipo Ordenamiento:';
        end;
        2://Retencion IVA Ventas
        begin
          LblFechaAl.Enabled := False; DatFechaAl.Enabled := False;
          LblFechaDesde.Enabled := True; DatFechaDesde.Enabled := True;
          LblFechaHasta.Enabled := True; DatFechaHasta.Enabled := True;
          LblRangoProveedor.Enabled := False;
          LblProveedorDesde.Enabled := False;
          LblProveedorHasta.Enabled := False;
          CmbProveedorDesde.Enabled := False;
          CmbProveedorHasta.Enabled := False;
          LblRangoCliente.Enabled := True;
          LblClienteDesde.Enabled := True;
          LblClienteHasta.Enabled := True;
          CmbClienteDesde.Enabled := True;
          CmbClienteHasta.Enabled := True;
          CmbTipoOrdenamiento.Items.Clear;
          CmbTipoOrdenamiento.Items.Add('Fecha Documento');
          CmbTipoOrdenamiento.Items.Add('Fecha Contable');
          CmbTipoOrdenamiento.Items.Add('Fecha Pago');
          CmbTipoOrdenamiento.Items.Add('Consecutivo');
          CmbTipoOrdenamiento.ItemIndex := 0;
          LblTipoOrdenamiento.Caption := 'Tipo Ordenamiento:';
        end;
        3:// Resumen Debitos y Creditos
        begin
          LblFechaAl.Enabled := False; DatFechaAl.Enabled := False;
          LblFechaDesde.Enabled := True; DatFechaDesde.Enabled := True;
          LblFechaHasta.Enabled := True; DatFechaHasta.Enabled := True;
          LblRangoProveedor.Enabled := False;
          LblProveedorDesde.Enabled := False;
          LblProveedorHasta.Enabled := False;
          LblRangoCliente.Enabled := False;
          LblClienteDesde.Enabled := False;
          LblClienteHasta.Enabled := False;
          CmbTipoOrdenamiento.Items.Clear;
          CmbTipoOrdenamiento.Items.Add('Fecha Documento');
          CmbTipoOrdenamiento.Items.Add('Fecha Contable');
          CmbTipoOrdenamiento.Items.Add('Fecha Libro');
          CmbTipoOrdenamiento.ItemIndex := 0;
          LblTipoOrdenamiento.Caption := 'Utilizar Fecha:';
        end;
      end;
    end;
  end;
end;

function TFrmReporteGeneral.Validar: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  (* Validando Informaci�n de Reportes Generales *)
  //Validando Fechas
  if (DatFechaDesde.Date > DatFechaHasta.Date) then
  begin
    ListaErrores.Add('La Fecha de Desde debe ser Menor a la Fecha Hasta!!!');
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
