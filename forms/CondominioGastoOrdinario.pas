unit CondominioGastoOrdinario;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GenericoDetalle, DB, DBClient, ImgList, XPMan, ComCtrls, Grids,
  DBGrids, ExtCtrls, ToolWin, StdCtrls, DBCtrls, RxLookup, Utils, 
  ClsCondominioGastoOrdinario, ClsCondominioDetalleGastoOrdinario,
  ClsCondominioConjuntoAdministracion, ClsParametro;

type
  TFrmCondominioGastoOrdinario = class(TFrmGenericoDetalle)
    GrpConjuntoAdministracion: TGroupBox;
    LblCodigoConjuntoAdministracion: TLabel;
    CmbCodigoConjuntoAdministracion: TRxDBLookupCombo;
    TxtCodigoConjuntoAdministracion: TDBText;
    DSConjuntoAdministracion: TDataSource;
    Datacodigo_conjunto_administracion: TStringField;
    Datacodigo_gasto: TStringField;
    Datadescripcion: TStringField;
    Datamonto: TFloatField;
    procedure FormCreate(Sender: TObject);
    procedure CmbCodigoConjuntoAdministracionChange(Sender: TObject);
    procedure TBtnGuardarClick(Sender: TObject);
    procedure TBtnCancelarClick(Sender: TObject);
  private
    { Private declarations }
    Clase: TCondominioGastoOrdinario;
    DetalleGastoOrdinario: TCondominioDetalleGastoOrdinario;
    ConjuntoAdministracion: TCondominioConjuntoAdministracion;
    Parametro: TParametro;
    DataConjuntoAdministracion: TClientDataSet;
    procedure Guardar;
    procedure Cancelar;
    procedure Refrescar;
    procedure Imprimir;
    procedure Buscar;
    procedure Limpiar;
    procedure Asignar;
    procedure Obtener;
    procedure Mover;
    procedure GuardarDetalles;
  protected
    { Protected declarations }
    function Validar: Boolean; override;
    procedure Detalle; override;
  public
    { Public declarations }
  end;

var
  FrmCondominioGastoOrdinario: TFrmCondominioGastoOrdinario;

implementation

uses
  CondominioAgregarDetalleGastoOrdinario;

{$R *.dfm}

procedure TFrmCondominioGastoOrdinario.Asignar;
begin

end;

procedure TFrmCondominioGastoOrdinario.Buscar;
begin

end;

procedure TFrmCondominioGastoOrdinario.Cancelar;
begin
  CmbCodigoConjuntoAdministracionChange(nil);
end;

procedure TFrmCondominioGastoOrdinario.CmbCodigoConjuntoAdministracionChange(
  Sender: TObject);
begin
  Data.CloneCursor(DetalleGastoOrdinario.ObtenerCodigoConjuntoAdministracion(VarToStr(CmbCodigoConjuntoAdministracion.KeyValue)),False,True);
  DS.DataSet := Data;
end;

procedure TFrmCondominioGastoOrdinario.Detalle;
begin
  inherited;
  FrmCondominioAgregarDetalleGastoOrdinario := TFrmCondominioAgregarDetalleGastoOrdinario.CrearDetalle(Self,Data);
  FrmCondominioAgregarDetalleGastoOrdinario.CodigoConjuntoAdministracion := VarToStr(CmbCodigoConjuntoAdministracion.KeyValue);
  FrmCondominioAgregarDetalleGastoOrdinario.ShowModal;
  FreeAndNil(FrmCondominioAgregarDetalleGastoOrdinario);
end;

procedure TFrmCondominioGastoOrdinario.FormCreate(Sender: TObject);
begin
  inherited;
  //Ocultando Opciones de la Barra de Tareas que no se usaran
//  BH.Visible := False;
//  BH.Enabled := False;
  HabilitarEdicion(True);	
  //Activando Panel Maestro
  PanelMaestro.Enabled := True;
  PageDetalles.Enabled := True;
  Agregando := True;
  Editando := True;
  ProcAgregar := nil;
  ProcEditar := nil;
  ProcEliminar := nil;
  ProcGuardar := Guardar;
  ProcCancelar := Cancelar;
  ProcRefrescar := Refrescar;
  ProcImprimir := Imprimir;
  ProcBuscar := nil;
  Clase := TCondominioGastoOrdinario.Create;
  //Inicializando DataSets Secundarios
  DetalleGastoOrdinario := TCondominioDetalleGastoOrdinario.Create;
  ConjuntoAdministracion := TCondominioConjuntoAdministracion.Create;
  Parametro := TParametro.Create;
  DataConjuntoAdministracion := ConjuntoAdministracion.ObtenerLista;
  DataConjuntoAdministracion.First;
  DSConjuntoAdministracion.DataSet := DataConjuntoAdministracion;
  TxtCodigoConjuntoAdministracion.DataSource := DSConjuntoAdministracion;
  TxtCodigoConjuntoAdministracion.DataField := 'descripcion';
  ConfigurarRxDBLookupCombo(CmbCodigoConjuntoAdministracion,DSConjuntoAdministracion,'codigo','codigo;descripcion');
  DS.DataSet := Data;
  GridDetalle.DataSource := DS;
  CmbCodigoConjuntoAdministracion.Value := Parametro.ObtenerValor('codigo_conjunto_administracion');
  CmbCodigoConjuntoAdministracionChange(Self);
  //Buscar;
end;

procedure TFrmCondominioGastoOrdinario.Guardar;
begin
  //Se eliminan los gastos ordinarios anteriormente guardados
  Clase.EliminarCodigoConjuntoAdministracion(VarToStr(CmbCodigoConjuntoAdministracion.KeyValue));
  //Se almacenan los nuevos gastos ordinarios
  with Data do
  begin
    DisableControls;
    First;
    while (not Eof) do
    begin
      Clase.SetCodigoConjuntoAdministracion(VarToStr(CmbCodigoConjuntoAdministracion.KeyValue));
      Clase.SetCodigoGasto(FieldValues['codigo_gasto']);
      Clase.SetMonto(FieldValues['monto']);
      Clase.Insertar;
      Next;
    end;
    EnableControls;    
  end;
  MessageBox(Self.Handle, 'Los Gastos Ordinarios Has Sido Guardados Exitosamente!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
end;

procedure TFrmCondominioGastoOrdinario.GuardarDetalles;
begin

end;

procedure TFrmCondominioGastoOrdinario.Imprimir;
begin

end;

procedure TFrmCondominioGastoOrdinario.Limpiar;
begin

end;

procedure TFrmCondominioGastoOrdinario.Mover;
begin

end;

procedure TFrmCondominioGastoOrdinario.Obtener;
begin

end;

procedure TFrmCondominioGastoOrdinario.Refrescar;
begin

end;

procedure TFrmCondominioGastoOrdinario.TBtnCancelarClick(Sender: TObject);
begin
  inherited;
  HabilitarEdicion(True);
  Agregando := True;
  Editando := True;
end;

procedure TFrmCondominioGastoOrdinario.TBtnGuardarClick(Sender: TObject);
begin
  inherited;
  HabilitarEdicion(True);
  Agregando := True;
  Editando := True;
end;

function TFrmCondominioGastoOrdinario.Validar: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  (*
  //Validando Cliente
  if (Length(Trim(CmbCodigoCliente.Text)) = 0) then
  begin
    ListaErrores.Add('El Cliente No Puede Estar En Blanco!!!');
  end;
  //Validando Vendedor
  if (Length(Trim(CmbCodigoVendedor.Text)) = 0) then
  begin
    ListaErrores.Add('El Vendedor No Puede Estar En Blanco!!!');
  end;
  //Validando Monto
  if (TxtTotal.Value <= 0) then
  begin
    ListaErrores.Add('El Total Debe Ser Positivo!!!');
  end;
  //Validando Fechas
  if ((DatFechaIngreso.Date > DatFechaRecepcion.Date) or (DatFechaIngreso.Date > DatFechaContable.Date) or (DatFechaIngreso.Date > DatFechaLibros.Date)) then
  begin
    ListaErrores.Add('La Fecha de Ingreso Debe Ser Menor a las Fechas de Recepcion, Contable y Libros Respectivamente!!!');
  end;
  //Validando Detalle o Forma Libre
  if (Clase.GetFormaLibre) then
  begin
    if (Length(Trim(TxtConcepto.Text)) = 0) then
    begin
      ListaErrores.Add('El Concepto No Puede Estar En Blanco!!!');
    end;
    if (TxtMontoNeto.Value <= 0) then
    begin
      ListaErrores.Add('El Monto Neto Debe Ser Positivo!!!');
    end;
    if (TxtMontoDescuento.Value < 0) then
    begin
      ListaErrores.Add('El Monto de Descuento No Debe Ser Negativo!!!');
    end;
    if (TxtImpuesto.Value < 0) then
    begin
      ListaErrores.Add('El Impuesto No Debe Ser Negativo!!!');
    end;
  end
  else
  begin
    if (Data.RecordCount = 0) then
    begin
      ListaErrores.Add('El Detalle De La Factura No Puede Estar Vacio!!!');
    end;
  end;
  *)
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
