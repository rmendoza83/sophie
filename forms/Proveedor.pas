unit Proveedor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Generico, XPMan, ImgList, DB, Grids, DBGrids, StdCtrls, Buttons,
  ExtCtrls, ComCtrls, ToolWin, Mask, rxToolEdit, rxCurrEdit, DBCtrls,
  ClsProveedor, ClsTipoProveedor ,ClsZona, ClsEsquemaPago, RxLookup, Utils,
  ClsTipoRetencion, DBClient;

type
  TFrmProveedor = class(TFrmGenerico)
    LblCodigo: TLabel;
    TxtCodigo: TEdit;
    LblRif: TLabel;
    CmbPrefijoRif: TComboBox;
    TxtRif: TEdit;
    LblRazonSocial: TLabel;
    TxtRazonSocial: TEdit;
    LblTelefonos: TLabel;
    TxtTelefonos: TEdit;
    LblEmail: TLabel;
    TxtEmail: TEdit;
    LblNit: TLabel;
    TxtNit: TEdit;
    TxtDireccionDomicilio2: TEdit;
    LblDireccionDomicilio: TLabel;
    TxtDireccionDomicilio1: TEdit;
    TxtDireccionFiscal1: TEdit;
    TxtDireccionFiscal2: TEdit;
    LblDireccionFiscal: TLabel;
    LblFax: TLabel;
    TxtFax: TEdit;
    LblWebsite: TLabel;
    TxtWebsite: TEdit;
    DatFechaIngreso: TDateEdit;
    LblFechaIngreso: TLabel;
    LblContacto: TLabel;
    TxtContacto: TEdit;
    LblTipoProveedor: TLabel;
    LblEsquemaPago: TLabel;
    LblZona: TLabel;
    TxtTipoProveedor: TDBText;
    TxtZona: TDBText;
    TxtEsquemaPago: TDBText;
    TxtUniqueId: TLabel;
    TxtEdad: TLabel;
    CmbTipoProveedor: TRxDBLookupCombo;
    CmbZona: TRxDBLookupCombo;
    CmbEsquemaPago: TRxDBLookupCombo;
    BtnBuscarZona: TBitBtn;
    BtnBuscarVendedor: TBitBtn;
    BtnEsquemaPago: TBitBtn;
    DSTipoProveedor: TDataSource;
    DSZona: TDataSource;
    DSEsquemaPago: TDataSource;
    Label12: TLabel;
    LblPIvaRetencion: TLabel;
    LblObservaciones: TLabel;
    TxtObservaciones: TEdit;
    TxtRetencionIva: TDBText;
    DSRetencionIva: TDataSource;
    CmbRetencionIva: TRxDBLookupCombo;
    BtnRetencionIva: TBitBtn;
    procedure BtnRetencionIvaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtnBuscarZonaClick(Sender: TObject);
    procedure BtnBuscarTipoProveedorClick(Sender: TObject);
    procedure BtnEsquemaPagoClick(Sender: TObject);
    procedure GrdBusquedaCellClick(Column: TColumn);
  private
    { Private declarations }
    Clase: TProveedor;
    TipoProveedor: TTipoProveedor;
    Zona: TZona;
    EsquemaPago: TEsquemaPago;
    RetencionIva: TTipoRetencion;
    Data: TClientDataSet;
    DataTipoProveedor: TClientDataSet;
    DataZona: TClientDataSet;
    DataEsquemaPago: TClientDataSet;
    DataRetencionIva: TClientDataSet;
    procedure Agregar;
    procedure Editar;
    procedure Eliminar;
    procedure Guardar;
    procedure Cancelar;
    procedure Refrescar;
    procedure Imprimir;
    procedure Buscar;
    procedure Limpiar;
    procedure Asignar;
    procedure Obtener;
    procedure Mover;
  protected
    function Validar: Boolean; override;
  public
    { Public declarations }
  end;

var
  FrmProveedor: TFrmProveedor;

implementation

uses
  GenericoBuscador,
  ClsAnticipoProveedor,
  ClsFacturacionProveedor,
  ClsNotaCreditoProveedor,
  ClsNotaDebitoProveedor;

{$R *.dfm}

{ TFrmProveedor }

procedure TFrmProveedor.Agregar;
begin
  //Verificando
  Limpiar;
  TxtCodigo.SetFocus
end;

procedure TFrmProveedor.Asignar;
begin
  with Clase do
  begin
    TxtCodigo.Text := GetCodigo;
    CmbPrefijoRif.ItemIndex := CmbPrefijoRif.Items.IndexOf(GetPrefijoRif);
    TxtRif.Text := GetRif;
    TxtNit.Text := GetNit;
    TxtRazonSocial.Text := GetRazonSocial;
    TxtTelefonos.Text := GetTelefonos;
    TxtFax.Text := GetFax;
    TxtEmail.Text := GetEmail;
    TxtWebsite.Text := GetWebsite;
    DatFechaIngreso.Date := GetFechaIngreso;
    TxtContacto.Text :=GetContacto;
    CmbTipoProveedor.KeyValue := GetCodigoTipoProveedor;
    CmbZona.KeyValue := GetCodigoZona;
    CmbEsquemaPago.KeyValue := GetCodigoEsquemaPago;
    CmbRetencionIva.KeyValue := GetCodigoTipoRetencionIva;
    TxtDireccionFiscal1.Text := GetDireccionFiscal1;
    TxtDireccionFiscal2.Text := GetDireccionFiscal2;
    TxtDireccionDomicilio1.Text := GetDireccionDomicilio1;
    TxtDireccionDomicilio2.Text := GetDireccionDomicilio2;
    DataTipoProveedor.Locate('codigo',CmbTipoProveedor.KeyValue,[loCaseInsensitive]);
    DataZona.Locate('codigo',CmbZona.KeyValue,[loCaseInsensitive]);
    DataEsquemaPago.Locate('codigo',CmbEsquemaPago.KeyValue,[loCaseInsensitive]);
    DataRetencionIva.Locate('codigo',CmbRetencionIva.KeyValue,[loCaseInsensitive]);
    TxtUniqueId.Caption := IntToStr(GetUniqueId);
  end;
end;

procedure TFrmProveedor.BtnBuscarTipoProveedorClick(Sender: TObject);
begin
  Buscador(Self,TipoProveedor,'codigo','Busqueda de Tipo Proveedor',nil,DataTipoProveedor);
  CmbTipoProveedor.KeyValue := DataTipoProveedor.FieldValues['unique_id'];
end;

procedure TFrmProveedor.BtnBuscarZonaClick(Sender: TObject);
begin
  Buscador(Self,Zona,'codigo','Busqueda de Zona',nil,DataZona);
  CmbZona.KeyValue := DataZona.FieldValues['codigo'];
end;

procedure TFrmProveedor.BtnEsquemaPagoClick(Sender: TObject);
begin
  Buscador(Self,EsquemaPago,'codigo','Busqueda de Esquema de Pago',nil,DataEsquemaPago);
  CmbEsquemaPago.KeyValue := DataEsquemaPago.FieldValues['codigo'];
end;

procedure TFrmProveedor.BtnRetencionIvaClick(Sender: TObject);
begin
  Buscador(Self,RetencionIva,'codigo','Busqueda de Codigo Retenci�n de Iva',nil,DataRetencionIva);
  CmbRetencionIva.KeyValue := DataRetencionIva.FieldValues['codigo'];
end;

procedure TFrmProveedor.Buscar;
begin
  Screen.Cursor := crSQLWait;
  if (Length(Trim(TxtCriterio.Text)) > 0) then
  begin
    Data := Clase.ObtenerListaCriterio(Trim(TxtCriterio.Text));
  end
  else
  begin
    Data := Clase.ObtenerLista;
  end;
  Data.First;
  DS.DataSet := Data;
  GrdBusqueda.Refresh;
  Screen.Cursor := crDefault;
end;

procedure TFrmProveedor.Cancelar;
begin
  TxtCodigo.Enabled := True;
end;

procedure TFrmProveedor.Editar;
begin
  //Se Busca el Elemento
  Clase.SetCodigo(Data.FieldValues['codigo']);
  Clase.Buscar;
  Asignar;
  TxtCodigo.Enabled := False;
   CmbPrefijoRif.SetFocus;
end;

procedure TFrmProveedor.Eliminar;
var
  AnticipoProveedor: TAnticipoProveedor;
  FacturacionProveedor: TFacturacionProveedor;
  NotaCreditoProveedor: TNotaCreditoProveedor;
  NotaDebitoProveedor: TNotaDebitoProveedor;
  sw: Boolean;
begin
  //Se Valida Que El Proveedor No Posee Movimientos En El Sistema
  sw := True;
  //Verificando Anticipos
  AnticipoProveedor := TAnticipoProveedor.Create;
  AnticipoProveedor.SetCodigoProveedor(Clase.GetCodigo);
  if (AnticipoProveedor.BuscarCodigoProveedor) then
  begin
    sw := False;    
  end
  else
  begin
    //Verificando Facturas
    FacturacionProveedor := TFacturacionProveedor.Create;
    FacturacionProveedor.SetCodigoProveedor(Clase.GetCodigo);
    if (FacturacionProveedor.BuscarCodigoProveedor) then
    begin
      sw := False;
    end
    else
    begin
      //Verificando Notas de Credito
      NotaCreditoProveedor := TNotaCreditoProveedor.Create;
      NotaCreditoProveedor.SetCodigoProveedor(Clase.GetCodigo);
      if (NotaCreditoProveedor.BuscarCodigoProveedor) then
      begin
        sw := False;
      end
      else
      begin
        //Verificando Notas de Debito
        NotaDebitoProveedor := TNotaDebitoProveedor.Create;
        NotaDebitoProveedor.SetCodigoProveedor(Clase.GetCodigo);
        if (NotaDebitoProveedor.Buscar) then
        begin
          sw := False;
        end;        
      end;
    end;
  end;
  if (sw) then
  begin
    //Se Busca el Elemento
    Clase.SetCodigo(Data.FieldValues['codigo']);
    Clase.Buscar;
    Clase.Eliminar;
    MessageBox(Self.Handle, 'El Proveedor Se Ha Eliminado Con Exito!!!',PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Refrescar;
    Limpiar;
  end
  else
  begin
    MessageBox(Self.Handle, 'El Proveedor Presenta Movimientos En El Sistema y No Puede Ser Eliminado!!!',PChar(MsgTituloAdvertencia), MB_ICONEXCLAMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end;
end;

procedure TFrmProveedor.FormCreate(Sender: TObject);
begin
  inherited;
  ProcAgregar := Agregar;
  ProcEditar := Editar;
  ProcEliminar := Eliminar;
  ProcGuardar := Guardar;
  ProcCancelar := Cancelar;
  ProcRefrescar := Refrescar;
  ProcImprimir := Imprimir;
  ProcBuscar := Buscar;
  Clase := TProveedor.Create;
//Inicializando DataSets Secundarios
  TipoProveedor:= TTipoProveedor.Create;
  Zona:= TZona.Create;
  EsquemaPago:= TEsquemaPago.Create;
  RetencionIva:= TTipoRetencion.Create;
  DataTipoProveedor:= TipoProveedor.ObtenerLista;
  DataZona:= Zona.ObtenerLista;
  DataEsquemaPago:= EsquemaPago.ObtenerLista;
  DataRetencionIva:= RetencionIva.ObtenerLista;
  DataTipoProveedor.First;
  DataZona.First;
  DataEsquemaPago.First;
  DataRetencionIva.First;
  DSTipoProveedor.DataSet:= DataTipoProveedor;
  DSZona.DataSet:= DataZona;
  DSEsquemaPago.DataSet:= DataEsquemaPago;
  DSRetencionIva.DataSet:= DataRetencionIva;
  TxtTipoProveedor.DataSource:= DSTipoProveedor;
  TxtTipoProveedor.DataField:= 'codigo';
  CmbTipoProveedor.LookupSource:= DSTipoProveedor;
  CmbTipoProveedor.LookupField := 'codigo';
  CmbTipoProveedor.LookupDisplay := 'codigo;descripcion';
  TxtZona.DataSource:= DSZona;
  TxtZona.DataField:= 'descripcion';
  CmbZona.LookupSource:= DSZona;
  CmbZona.LookupField := 'codigo';
  CmbZona.LookupDisplay := 'codigo;descripcion';
  TxtEsquemaPago.DataSource:= DSEsquemaPago;
  TxtEsquemaPago.DataField:= 'descripcion';
  CmbEsquemaPago.LookupSource:= DSEsquemaPago;
  CmbEsquemaPago.LookupField := 'codigo';
  CmbEsquemaPago.LookupDisplay := 'codigo;descripcion';
  TxtRetencionIva.DataSource := DSRetencionIva;
  TxtRetencionIva.DataField := 'descripcion';
  CmbRetencionIva.LookupSource := DSRetencionIva;
  CmbRetencionIva.LookupField := 'codigo';
  CmbRetencionIva.LookupDisplay := 'codigo;descripcion';
  Buscar;
end;

procedure TFrmProveedor.GrdBusquedaCellClick(Column: TColumn);
begin
  Mover;
end;

procedure TFrmProveedor.Guardar;
begin
  if (Agregando) then
  begin
    //Agregando...
    //Se Busca el Codigo
    Clase.SetCodigo(TxtCodigo.Text);
    if (Clase.BuscarCodigo) then
    begin
      MessageBox(Self.Handle, 'El C�digo Ya Existe!!!', PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      TxtCodigo.SetFocus;
      Abort;
    end
    else 
    begin
      Obtener;
      Clase.Insertar;
      MessageBox(Self.Handle, 'El Proveedor Ha Sido Agregado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    end;
  end
  else
  begin
    //Editando...
    Obtener;
    Clase.Modificar;
    MessageBox(Self.Handle, 'El Proveedor Ha Sido Editado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    TxtCodigo.Enabled := True;
  end;
  Refrescar;
  Limpiar;
end;

procedure TFrmProveedor.Imprimir;
begin

end;

procedure TFrmProveedor.Limpiar;
begin
  TxtCodigo.Text := '';
  CmbPrefijoRif.Text := '';
  TxtRif.Text :='';
  TxtNit.Text :='';
  TxtRazonSocial.Text := '';
  TxtTelefonos.Text := '';
  TxtFax.Text := '';
  TxtEmail.Text := '';
  TxtWebsite.Text := '';
  DatFechaIngreso.Date := Now;
  TxtContacto.Text :='';
  CmbTipoProveedor.KeyValue :='';
  CmbZona.KeyValue := '';
  CmbEsquemaPago.KeyValue :='';
  CmbRetencionIva.KeyValue :='';
  TxtDireccionFiscal1.Text := '';
  TxtDireccionFiscal2.Text := '';
  TxtDireccionDomicilio1.Text := '';
  TxtDireccionDomicilio2.Text := '';
  TxtUniqueId.Caption := '';
end;

procedure TFrmProveedor.Mover;
var
  DSTemp: TDataSet;
begin
  if (DS.DataSet.RecordCount > 0) then
  begin
    DSTemp := DS.DataSet;
    with Clase do
    begin
      SetCodigo(DSTemp.FieldValues['codigo']);
      SetPrefijoRif(DSTemp.FieldValues['prefijo_rif']);
      SetRif(DSTemp.FieldValues['rif']);
      SetNit(DSTemp.FieldValues['nit']);
      SetRazonSocial(DSTemp.FieldValues['razon_social']);
      SetDireccionFiscal1(DSTemp.FieldValues['direccion_fiscal_1']);
      SetDireccionFiscal2(DSTemp.FieldValues['direccion_fiscal_2']);
      SetDireccionDomicilio1(DSTemp.FieldValues['direccion_domicilio_1']);
      SetDireccionDomicilio2(DSTemp.FieldValues['direccion_domicilio_2']);
      SetTelefonos(DSTemp.FieldValues['telefonos']);
      SetFax(DSTemp.FieldValues['fax']);
      SetEmail(DSTemp.FieldValues['email']);
      SetWebsite(DSTemp.FieldValues['website']);
      SetFechaIngreso(DSTemp.FieldByName('fecha_ingreso').AsDateTime);
      SetContacto(DSTemp.FieldValues['contacto']);
      SetCodigoTipoProveedor(DSTemp.FieldValues['codigo_tipo_proveedor']);
      SetCodigoEsquemaPago(DSTemp.FieldValues['codigo_esquema_pago']);
      SetCodigoTipoRetencionIva(DSTemp.FieldValues['codigo_tipo_retencion_iva']);
      SetCodigoZona(DSTemp.FieldValues['codigo_zona']);
      SetUniqueId(DSTemp.FieldValues['unique_id']);
    end;
    Asignar;
  end
  else
  begin
    Limpiar;
  end;
end;

procedure TFrmProveedor.Obtener;
begin
  with Clase do
  begin
    SetCodigo(TxtCodigo.Text);
    SetPrefijoRif(CmbPrefijoRif.Text);
    SetRif(TxtRif.Text);
    SetNit(TxtNit.Text);
    SetRazonSocial(TxtRazonSocial.Text);
    SetTelefonos(TxtTelefonos.Text);
    SetFax(TxtFax.Text);
    SetEmail(TxtEmail.Text);
    SetWebsite(TxtWebsite.Text);
    SetFechaIngreso(DatFechaIngreso.Date);
    SetContacto(TxtContacto.Text);
    SetCodigoTipoProveedor(CmbTipoProveedor.Text);
    SetCodigoZona(CmbZona.Text);
    SetCodigoEsquemaPago(CmbEsquemaPago.Text);
    SetCodigoTipoRetencionIva(CmbRetencionIva.Text);
    SetDireccionFiscal1(TxtDireccionFiscal1.Text);
    SetDireccionFiscal2(TxtDireccionFiscal2.Text);
    SetDireccionDomicilio1(TxtDireccionDomicilio1.Text);
    SetDireccionDomicilio2(TxtDireccionDomicilio2.Text);
    SetUniqueId(StrToIntDef(TxtUniqueId.caption,0));
  end;
end;

procedure TFrmProveedor.Refrescar;
begin
  TxtCriterio.Text := '';
  Buscar;
  //Se Actualizan Los demas Componentes
  DataTipoProveedor:= TipoProveedor.ObtenerLista;
  DataZona:= Zona.ObtenerLista;
  DataEsquemaPago:= EsquemaPago.ObtenerLista;
  DataRetencionIva:= RetencionIva.ObtenerLista;
  DataTipoProveedor.First;
  DataZona.First;
  DataEsquemaPago.First;
  DataRetencionIva.First;
  DSTipoProveedor.DataSet:= DataTipoProveedor;
  DSZona.DataSet:= DataZona;
  DSEsquemaPago.DataSet:= DataEsquemaPago;
  DSRetencionIva.DataSet:= DataRetencionIva;
end;

function TFrmProveedor.Validar: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  (* Validando Informacion del Cliente *)
  //Validando Codigo
  if (Length(Trim(TxtCodigo.Text)) = 0) then
  begin
    ListaErrores.Add('El C�digo No Puede Estar En Blanco!!!');
  end;
  //Validando Prefijo
  if (Length(Trim(CmbPrefijoRif.Text)) = 0) then
  begin
    ListaErrores.Add('Debe Asignar un Prefijo Ej. "E" o "V"!!!');
  end;
  //Validando Cedula
  if (Length(Trim(TxtRif.Text)) = 0) then
  begin
    ListaErrores.Add('La C�dula No Puede Estar En Blanco!!!');
  end;
  //Validando Nit
  if (Length(Trim(TxtNit.Text)) = 0) then
  begin
    ListaErrores.Add('El Nit No Puede Estar En Blanco!!!');
  end;
  //Validando Razon Social
  if (Length(Trim(TxtRazonSocial.Text)) = 0) then
  begin
    ListaErrores.Add('La Raz�n Social No Puede Estar En Blanco!!!');
  end;
  //Validando Telefonos
  if (Length(Trim(TxtTelefonos.Text)) = 0) then
  begin
    ListaErrores.Add('Los Tel�fonos No Puede Estar En Blanco!!!');
  end;
  //Validando Tipo de Proveedor
  if (Length(Trim(CmbTipoProveedor.Text)) = 0) then
  begin
    ListaErrores.Add('Debe Seleccionar el Tipo de Proveedor!!!');
  end;
  //Validando Codigo Zona
  if (Length(Trim(CmbZona.Text)) = 0) then
  begin
    ListaErrores.Add('Debe Seleccionar La Zona!!!');
  end;
  //Validando Esquema de Pago
  if (Length(Trim(CmbEsquemaPago.Text)) = 0) then
  begin
    ListaErrores.Add('Debe Seleccionar el Esquema de Pago!!!');
  end;
  //Validando Direccion Fiscal
  if (Length(Trim(TxtDireccionFiscal1.Text)) = 0) then
  begin
    ListaErrores.Add('La Direcci�n Fiscal No Puede Estar En Blanco!!!');
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
