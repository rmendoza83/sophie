inherited FrmMonedas: TFrmMonedas
  Caption = 'Sophie - Monedas'
  PixelsPerInch = 96
  TextHeight = 13
  inherited Pag: TPageControl
    ActivePage = TabBusqueda
    inherited TabBusqueda: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 24
      ExplicitWidth = 584
      ExplicitHeight = 304
      inherited GrdBusqueda: TDBGrid
        OnCellClick = GrdBusquedaCellClick
      end
    end
    inherited TabDatos: TTabSheet
      object LblCodigo: TLabel
        Left = 72
        Top = 11
        Width = 41
        Height = 13
        Alignment = taRightJustify
        Caption = 'C'#243'digo:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblDescripcion: TLabel
        Left = 45
        Top = 35
        Width = 68
        Height = 13
        Alignment = taRightJustify
        Caption = 'Descripci'#243'n:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblTasaDolar: TLabel
        Left = 49
        Top = 58
        Width = 63
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tasa Dolar:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object TxtDescripcion: TEdit
        Left = 119
        Top = 32
        Width = 150
        Height = 21
        TabOrder = 1
      end
      object TxtCodigo: TEdit
        Left = 119
        Top = 8
        Width = 150
        Height = 21
        TabOrder = 0
      end
      object TxtTasaDolar: TCurrencyEdit
        Left = 119
        Top = 55
        Width = 81
        Height = 21
        Margins.Left = 4
        Margins.Top = 1
        DisplayFormat = '0.00;'
        TabOrder = 2
      end
    end
  end
end
