unit AgregarDetalleFacturacionCliente;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GenericoAgregarDetalle, DB, StdCtrls, Buttons, ExtCtrls, Mask,
  rxToolEdit, rxCurrEdit, DBCtrls, ClsProducto, ClsDetFacturacionCliente,
  GenericoBuscador, DBClient, Utils, ClsParametro, ClsProductoPrecio;

type
  TFrmAgregarDetalleFacturacionCliente = class(TFrmGenericoAgregarDetalle)
    LblCantidad: TLabel;
    TxtCantidad: TCurrencyEdit;
    TxtDescripcionProducto: TEdit;
    procedure TxtCantidadKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtnBuscarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    Producto: TProducto;
    ProductoPrecio: TProductoPrecio;
    Parametro: TParametro;
  protected
    function Buscar: Boolean; override;
    procedure Agregar; override;
    procedure Limpiar; override;
  public
    { Public declarations }
  end;

var
  FrmAgregarDetalleFacturacionCliente: TFrmAgregarDetalleFacturacionCliente;

implementation

uses
  SeleccionTipoPrecio;

{$R *.dfm}

procedure TFrmAgregarDetalleFacturacionCliente.Agregar;
var
  Precio: Currency;
begin
  //Verificando el Uso del Tipo de Precios
  Parametro.SetNombreParametro('usar_tipos_precios');
  if ((Parametro.Buscar) and (Parametro.ObtenerValorAsBoolean)) then
  begin
    FrmSeleccionTipoPrecio := TFrmSeleccionTipoPrecio.Crear(Self,Producto.GetCodigo);
    FrmSeleccionTipoPrecio.ShowModal;
    ProductoPrecio.SetCodigoProducto(Producto.GetCodigo);
    ProductoPrecio.SetCodigoTipoPrecioProducto(FrmSeleccionTipoPrecio.CodigoTipoPrecioProductoSeleccionado);
    ProductoPrecio.Buscar;
    Precio := ProductoPrecio.GetPrecio;    
    FreeAndNil(FrmSeleccionTipoPrecio);
  end
  else
  begin
    Precio := Producto.GetPrecio;
  end;
  with Data do
  begin
    //Verificando si existe el codigo en el Grid
    if (Locate('codigo_producto',TxtCodigo.Text,[loCaseInsensitive])) then
    begin
      Edit;
      FieldValues['cantidad'] := FieldValues['cantidad'] + TxtCantidad.Value;
      FieldValues['monto_descuento'] := (Precio * FieldValues['cantidad']) * (Producto.GetPDescuento / 100);
      FieldValues['impuesto'] := (Precio * FieldValues['cantidad']) * (Producto.GetPIvaVenta / 100);
      FieldValues['total'] := Precio * FieldValues['cantidad'];
    end
    else
    begin
      Append;
      FieldValues['codigo_producto'] := Producto.GetCodigo;
      FieldValues['referencia'] := Producto.GetReferencia;
      FieldValues['descripcion'] := Producto.GetDescripcion;
      FieldValues['cantidad'] := TxtCantidad.Value;
      FieldValues['precio'] := Precio;
      FieldValues['p_descuento'] := Producto.GetPDescuento;
      FieldValues['monto_descuento'] := (Precio * TxtCantidad.Value) * (Producto.GetPDescuento / 100);
      FieldValues['impuesto'] := (Precio * TxtCantidad.Value) * (Producto.GetPIvaVenta / 100);
      FieldValues['p_iva'] := Producto.GetPIvaVenta;
      FieldValues['total'] := Producto.GetPrecio * TxtCantidad.Value;
      FieldValues['id_facturacion_cliente'] := -1;
    end;
    Post;
  end;
  inherited;
end;

procedure TFrmAgregarDetalleFacturacionCliente.BtnBuscarClick(Sender: TObject);
begin
  inherited;
  Buscador(Self,Producto,'codigo','Buscar Producto',TxtCodigo,nil);
  if (Length(Trim(TxtCodigo.Text)) > 0) then
  begin
    ComponenteFoco.SetFocus;
  end;
end;

function TFrmAgregarDetalleFacturacionCliente.Buscar: Boolean;
begin
  Producto.SetCodigo(TxtCodigo.Text);
  Result := Producto.BuscarCodigo;
  if (Result) then
  begin
    TxtDescripcionProducto.Text := Producto.GetDescripcion;
  end
  else
  begin
    MessageBox(Self.Handle, Cadena('El Codigo No Existe!!!'), PChar(MsgTituloError), MB_ICONSTOP or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Limpiar;
  end;
end;

procedure TFrmAgregarDetalleFacturacionCliente.FormCreate(Sender: TObject);
begin
  inherited;
  ComponenteFoco := (TxtCantidad as TWinControl);
  Producto := TProducto.Create;
  Parametro := TParametro.Create;
  ProductoPrecio := TProductoPrecio.Create;
end;

procedure TFrmAgregarDetalleFacturacionCliente.Limpiar;
begin
  inherited;
  TxtDescripcionProducto.Text := '';
  TxtCantidad.Value := 1;
end;

procedure TFrmAgregarDetalleFacturacionCliente.TxtCantidadKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  case Key of
    VK_RETURN:
      Agregar;    
  end;
end;

end.
