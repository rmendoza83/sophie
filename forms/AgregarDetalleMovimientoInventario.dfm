inherited FrmAgregarDetalleMovimientoInventario: TFrmAgregarDetalleMovimientoInventario
  ClientHeight = 136
  OnCreate = FormCreate
  ExplicitWidth = 350
  ExplicitHeight = 160
  PixelsPerInch = 96
  TextHeight = 13
  object LblCantidad: TLabel [1]
    Left = 11
    Top = 64
    Width = 47
    Height = 13
    Caption = 'Cantidad:'
  end
  inherited PanelBotones: TPanel
    Top = 96
  end
  inherited BtnBuscar: TBitBtn
    OnClick = BtnBuscarClick
  end
  object TxtCantidad: TCurrencyEdit
    Left = 64
    Top = 61
    Width = 75
    Height = 21
    Margins.Left = 4
    Margins.Top = 1
    DecimalPlaces = 4
    DisplayFormat = '#,##0.0000'
    TabOrder = 3
    Value = 1.000000000000000000
    OnKeyDown = TxtCantidadKeyDown
  end
  object TxtDescripcionProducto: TEdit
    Left = 64
    Top = 35
    Width = 249
    Height = 21
    Enabled = False
    TabOrder = 4
    OnKeyDown = TxtCodigoKeyDown
  end
end
