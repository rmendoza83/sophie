unit Presupuesto;

{$I sophie.inc}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GenericoDetalle, DB, DBClient, ImgList, XPMan, ComCtrls, Grids,
  DBGrids, ExtCtrls, ToolWin, StdCtrls, rxCurrEdit, Mask, rxToolEdit, Buttons,
  RxLookup, Utils, ClsPresupuesto, ClsCliente, ClsVendedor, ClsZona,
  ClsEsquemaPago, ClsMoneda, ClsProducto, GenericoBuscador, ClsReporte,
  ClsParametro, ClsDetPresupuesto, ClsBusquedaPresupuesto
  {$IFDEF DELPHI10}, ImageList{$ENDIF};

type
  TFrmPresupuesto = class(TFrmGenericoDetalle)
    LblNumero: TLabel;
    LblCodigoCliente: TLabel;
    LblCodigoVendedor: TLabel;
    LblCodigoZona: TLabel;
    LblMoneda: TLabel;
    LblEsquemaPago: TLabel;
    LblFechaIngreso: TLabel;
    LblRecepcion: TLabel;
    LblMontoNeto: TLabel;
    LblMontoDescuento: TLabel;
    LblSubTotal: TLabel;
    TxtPDescuento: TLabel;
    LblImpuesto: TLabel;
    TxtPIVA: TLabel;
    LblTotal: TLabel;
    TxtNumero: TEdit;
    CmbCodigoCliente: TRxDBLookupCombo;
    BtnBuscarCliente: TBitBtn;
    CmbCodigoVendedor: TRxDBLookupCombo;
    BtnBuscarVendedor: TBitBtn;
    CmbCodigoZona: TRxDBLookupCombo;
    BtnBuscarZona: TBitBtn;
    CmbCodigoMoneda: TRxDBLookupCombo;
    BtnBuscarMoneda: TBitBtn;
    CmbCodigoEsquemaPago: TRxDBLookupCombo;
    BtnBuscarEsquemaPago: TBitBtn;
    DatFechaIngreso: TDateEdit;
    DatFechaVencimiento: TDateEdit;
    TxtMontoNeto: TCurrencyEdit;
    TxtMontoDescuento: TCurrencyEdit;
    TxtSubTotal: TCurrencyEdit;
    TxtImpuesto: TCurrencyEdit;
    TxtTotal: TCurrencyEdit;
    TabConcepto: TTabSheet;
    TabObservaciones: TTabSheet;
    TxtConcepto: TRichEdit;
    TxtObservaciones: TRichEdit;
    Datacodigo_producto: TStringField;
    Datareferencia: TStringField;
    Datadescripcion: TStringField;
    Dataprecio: TFloatField;
    Datap_descuento: TFloatField;
    Datamonto_descuento: TFloatField;
    Datacantidad: TFloatField;
    Dataimpuesto: TFloatField;
    Datap_iva: TFloatField;
    Datatotal: TFloatField;
    Dataid_presupuesto: TIntegerField;
    DSCliente: TDataSource;
    DSVendedor: TDataSource;
    DSZona: TDataSource;
    DSMoneda: TDataSource;
    DSEsquemaPago: TDataSource;
    procedure BtnBuscarClienteClick(Sender: TObject);
    procedure BtnBuscarEsquemaPagoClick(Sender: TObject);
    procedure BtnBuscarMonedaClick(Sender: TObject);
    procedure BtnBuscarVendedorClick(Sender: TObject);
    procedure BtnBuscarZonaClick(Sender: TObject);
    procedure CmbCodigoClienteExit(Sender: TObject);
    procedure CmbCodigoClienteChange(Sender: TObject);
    procedure DataAfterPost(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure LblImpuestoDblClick(Sender: TObject);
    procedure LblMontoDescuentoDblClick(Sender: TObject);
    procedure LblMontoNetoDblClick(Sender: TObject);
    procedure TBtnEliminarClick(Sender: TObject);
    procedure TxtMontoNetoExit(Sender: TObject);
    procedure TxtPDescuentoDblClick(Sender: TObject);
    procedure TxtPIVADblClick(Sender: TObject);
  private
    { Private declarations }
    Clase: TPresupuesto;
    DetPresupuesto: TDetPresupuesto;
    BuscadorPresupuesto: TBusquedaPresupuesto;
    Cliente: TCliente;
    Vendedor: TVendedor;
    Zona: TZona;
    EsquemaPago: TEsquemaPago;
    Moneda: TMoneda;
    Producto: TProducto;
    Reporte: TReporte;
    Parametro: TParametro;
    DataBuscadorPresupuesto: TClientDataSet;
    DataCliente: TClientDataSet;
    DataVendedor: TClientDataSet;
    DataZona: TClientDataSet;
    DataEsquemaPago: TClientDataSet;
    DataMoneda: TClientDataSet;
    procedure Agregar;
    procedure Editar;
    procedure Eliminar;
    procedure Guardar;
    procedure Cancelar;
    procedure Refrescar;
    procedure Imprimir;
    procedure Buscar;
    procedure Limpiar;
    procedure Asignar;
    procedure Obtener;
    procedure Mover;
    procedure GuardarDetalles;
    procedure CalcularMontos;
    procedure CalcularMontosCabecera;
  protected
    function Validar: Boolean; override;
    procedure Detalle; override;
  public
    { Public declarations }
  end;

var
  FrmPresupuesto: TFrmPresupuesto;

implementation

uses
  ActividadSophie,
  PModule,
  AgregarDetallePresupuesto,
  SolicitarMonto
  {$IFDEF USE_RAVE_REPORTS}
  , DMRR
  {$ENDIF}
  {$IFDEF USE_FAST_REPORTS}
  , DMFR
  {$ENDIF}
  ;

{$R *.dfm}

{ TFrmPresupuestoCliente }

procedure TFrmPresupuesto.Agregar;
begin
  //Verificando
  Limpiar;
  //Asignando Valores por Defecto
  CmbCodigoZona.Value := Parametro.ObtenerValor('codigo_zona');
  CmbCodigoEsquemaPago.Value := Parametro.ObtenerValor('codigo_esquema_pago');
  CmbCodigoMoneda.Value := Parametro.ObtenerValor('codigo_moneda');
  CmbCodigoCliente.SetFocus;
end;

procedure TFrmPresupuesto.Asignar;
begin
  if (Clase.GetFormaLibre) then
  begin
    TabDetalle.TabVisible := False;
    PageDetalles.ActivePage := TabConcepto;
    TxtMontoNeto.ReadOnly := False;
  end
  else
  begin
    TabDetalle.TabVisible := True;
    PageDetalles.ActivePage := TabDetalle;
    TxtMontoNeto.ReadOnly := True;
  end;
  with Clase do
  begin
    TxtNumero.Text := GetNumero;
    CmbCodigoCliente.KeyValue := GetCodigoCliente;
    CmbCodigoVendedor.KeyValue := GetCodigoVendedor;
    CmbCodigoZona.KeyValue := GetCodigoZona;
    CmbCodigoEsquemaPago.KeyValue := GetCodigoEsquemaPago;
    CmbCodigoMoneda.KeyValue := GetCodigoMoneda;
    DatFechaIngreso.Date := GetFechaIngreso;
    DatFechaVencimiento.Date := GetFechaVencimiento;
    TxtConcepto.Text := GetConcepto;
    TxtObservaciones.Text := GetObservaciones;
    TxtMontoNeto.Value := GetMontoNeto;
    TxtMontoDescuento.Value := GetMontoDescuento;
    TxtSubTotal.Value := GetSubTotal;
    TxtImpuesto.Value := GetImpuesto;
    TxtTotal.Value := GetTotal;
    TxtPDescuento.Caption := FormatFloat('###0.0000%',GetPDescuento);
    TxtPIVA.Caption := FormatFloat('###0.0000%',GetPIva);
    Data.CloneCursor(DetPresupuesto.ObtenerIdPresupuesto(GetIdPresupuesto),False,True);
    DS.DataSet := Data;
  end;
end;

procedure TFrmPresupuesto.BtnBuscarClienteClick(Sender: TObject);
begin
  Buscador(Self,Cliente,'codigo','Buscar Cliente',nil,DataCliente);
  CmbCodigoCliente.KeyValue := DataCliente.FieldValues['codigo'];
  CmbCodigoClienteExit(Sender);
end;

procedure TFrmPresupuesto.BtnBuscarEsquemaPagoClick(Sender: TObject);
begin
  Buscador(Self,EsquemaPago,'codigo','Buscar Esquema de Pago',nil,DataEsquemaPago);
  CmbCodigoEsquemaPago.KeyValue := DataEsquemaPago.FieldValues['codigo'];
end;

procedure TFrmPresupuesto.BtnBuscarMonedaClick(Sender: TObject);
begin
  Buscador(Self,Moneda,'codigo','Buscar Moneda',nil,DataMoneda);
  CmbCodigoMoneda.KeyValue := DataMoneda.FieldValues['codigo'];
end;

procedure TFrmPresupuesto.BtnBuscarVendedorClick(Sender: TObject);
begin
  Buscador(Self,Vendedor,'codigo','Buscar Vendedor',nil,DataVendedor);
  CmbCodigoVendedor.KeyValue := DataVendedor.FieldValues['codigo'];
end;

procedure TFrmPresupuesto.BtnBuscarZonaClick(Sender: TObject);
begin
  Buscador(Self,Zona,'codigo','Buscar Zona',nil,DataZona);
  CmbCodigoZona.KeyValue := DataZona.FieldValues['codigo'];
end;

procedure TFrmPresupuesto.Buscar;
begin
  if (Buscador(Self,BuscadorPresupuesto,'numero','Buscar Presupuesto',nil,DataBuscadorPresupuesto)) then
  begin
    Clase.SetNumero(DataBuscadorPresupuesto.FieldValues['numero']);
    Clase.BuscarNumero;
    Asignar;
  end;
end;

procedure TFrmPresupuesto.CalcularMontos;
var
  Neto, Descuento, Impuesto: Currency;
begin
  Neto := 0;
  Descuento := 0;
  Impuesto := 0;
  with Data do
  begin
    DisableControls;
    First;
    while (not Eof) do
    begin
      Neto := Neto + (FieldValues['precio'] * FieldValues['cantidad']);
      Descuento := Descuento + FieldValues['monto_descuento'];
      Impuesto := Impuesto + FieldValues['impuesto'];
      Next;
    end;
    EnableControls;
  end;
  //Asignando Montos
  TxtMontoNeto.Value := Neto;
  //Determinando si hay un Descuento Manual
  if ((TxtMontoDescuento.Value > 0) and (StrToFloat(StringReplace(TxtPDescuento.Caption,'%','',[rfReplaceAll])) > 0) and (Descuento = 0)) then
  begin
    Descuento := TxtMontoDescuento.Value;
  end;
  TxtMontoDescuento.Value := Descuento;
  TxtSubTotal.Value := Neto - Descuento;
  TxtImpuesto.Value := Impuesto;
  TxtTotal.Value := (Neto - Descuento) + Impuesto;
end;

procedure TFrmPresupuesto.CalcularMontosCabecera;
begin
  if (Clase.GetFormaLibre) then
  begin
    TxtSubTotal.Value := TxtMontoNeto.Value - TxtMontoDescuento.Value;
    TxtTotal.Value := TxtSubTotal.Value + TxtImpuesto.Value;
  end;
end;

procedure TFrmPresupuesto.Cancelar;
begin
  if (Agregando) then
  begin
    Limpiar;
  end
  else
  begin
    Clase.SetNumero(TxtNumero.Text);
    Clase.BuscarNumero;
    Asignar;
  end;
end;

procedure TFrmPresupuesto.CmbCodigoClienteChange(Sender: TObject);
begin
  SB.Panels[0].Text := DataCliente.FieldValues['razon_social'];
end;

procedure TFrmPresupuesto.CmbCodigoClienteExit(Sender: TObject);
begin
  if (Length(Trim(CmbCodigoCliente.Text)) > 0) then
  begin
    if (Agregando) then
    begin
      Cliente.SetCodigo(CmbCodigoCliente.Value);
      Cliente.BuscarCodigo;
      CmbCodigoVendedor.Value := Cliente.GetCodigoVendedor;
      CmbCodigoZona.Value := Cliente.GetCodigoZona;
      CmbCodigoEsquemaPago.Value := Cliente.GetCodigoEsquemaPago;
//      GridDetalle.SetFocus;
    end;
  end;
end;

procedure TFrmPresupuesto.DataAfterPost(DataSet: TDataSet);
begin
  CalcularMontos;
end;

procedure TFrmPresupuesto.Detalle;
begin
  inherited;
  FrmAgregarDetallePresupuesto := TFrmAgregarDetallePresupuesto.CrearDetalle(Self,Data);
  FrmAgregarDetallePresupuesto.ShowModal;
  FreeAndNil(FrmAgregarDetallePresupuesto);
end;

procedure TFrmPresupuesto.Editar;
begin
  if (Length(Trim(TxtNumero.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
end;

procedure TFrmPresupuesto.Eliminar;
begin
  //Se Busca El Presupuesto!
  Clase.SetNumero(TxtNumero.Text);
  Clase.BuscarNumero;
  //Se Eliminan Los Detalles
  DetPresupuesto.EliminarIdPresupuesto(Clase.GetIdPresupuesto);
  Clase.Eliminar;
  MessageBox(Self.Handle, 'El Presupuesto Se Ha Eliminado Con Exito!!!',PChar(MsgTituloInformacion), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  Refrescar;
  Limpiar;
end;

procedure TFrmPresupuesto.FormCreate(Sender: TObject);
begin
  inherited;
  ProcAgregar := Agregar;
  ProcEditar := Editar;
  ProcEliminar := Eliminar;
  ProcGuardar := Guardar;
  ProcCancelar := Cancelar;
  ProcRefrescar := Refrescar;
  ProcImprimir := Imprimir;
  ProcBuscar := Buscar;
  FrmActividadSophie.Show;
  FrmActividadSophie.Actividad('Cargando N�cleo...');
  Clase := TPresupuesto.Create;
  //Inicializando DataSets Secundarios
  BuscadorPresupuesto := TBusquedaPresupuesto.Create;
  Cliente := TCliente.Create;
  Vendedor := TVendedor.Create;
  Zona := TZona.Create;
  EsquemaPago := TEsquemaPago.Create;
  Moneda := TMoneda.Create;
  Producto := TProducto.Create;
  Reporte := TReporte.Create;
  Parametro := TParametro.Create;
  DetPresupuesto := TDetPresupuesto.Create;
  FrmActividadSophie.Actividad('Cargando Clientes...');
  DataCliente := Cliente.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Vendedores...');
  DataVendedor := Vendedor.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Zonas...');
  DataZona := Zona.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Esquemas de Pago...');
  DataEsquemaPago := EsquemaPago.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Monedas...');
  DataMoneda := Moneda.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Resto del N�cleo...');
  DataBuscadorPresupuesto := BuscadorPresupuesto.ObtenerLista;
  DataCliente.First;
  DataVendedor.First;
  DataZona.First;
  DataEsquemaPago.First;
  DataMoneda.First;
  DataBuscadorPresupuesto.First;
  DSCliente.DataSet := DataCliente;
  DSVendedor.DataSet := DataVendedor;
  DSZona.DataSet := DataZona;
  DSEsquemaPago.DataSet := DataEsquemaPago;
  DSMoneda.DataSet := DataMoneda;
  ConfigurarRxDBLookupCombo(CmbCodigoCliente,DSCliente,'codigo','codigo;razon_social');
  ConfigurarRxDBLookupCombo(CmbCodigoVendedor,DSVendedor,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoZona,DSZona,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoEsquemaPago,DSEsquemaPago,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoMoneda,DSMoneda,'codigo','codigo;descripcion');
  DS.DataSet := Data;
  GridDetalle.DataSource := DS;
  FrmActividadSophie.Close;
  //Buscar;
end;

procedure TFrmPresupuesto.Guardar;
begin
  if (Clase.GetFormaLibre) then
  begin
    CalcularMontosCabecera;
    Data.EmptyDataSet;
  end
  else
  begin
    CalcularMontos;
  end;
  Obtener;
  if (Agregando) then
  begin
    //Agregando...
    //Guardando Encabezado
    Clase.Insertar;
    TxtNumero.Text := Clase.GetNumero;
    Application.ProcessMessages;
    //Guardando Detalles
    GuardarDetalles;
    MessageBox(Self.Handle, 'El Presupuesto Ha Sido Agregado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    //Se Imprime la Factura
    MessageBox(Self.Handle, 'Presione "Aceptar" Para Proceder A Imprimir El Presupuesto!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    with Reporte do
    begin
      AgregarParametro('NumeroPresupuesto',Clase.GetNumero);
      AgregarParametro('MontoLetra',NumeroALetras(Clase.GetTotal));
      if (Clase.GetFormaLibre) then
      begin
        ImprimeReporte('PresupuestoFormaLibre',trGeneral);
      end
      else
      begin
        ImprimeReporte('Presupuesto',trGeneral);
      end;
    end;
  end
  else
  begin
    //Editando...
    Clase.Modificar;
    //Actualizando Detalles (Eliminando Anteriores y Cargando los Nuevos)
    DetPresupuesto.EliminarIdPresupuesto(Clase.GetIdPresupuesto);
    GuardarDetalles;
    //Guardando Cuenta x Cobrar
    MessageBox(Self.Handle, 'El Presupuesto Ha Sido Editado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end;
end;

procedure TFrmPresupuesto.GuardarDetalles;
begin
  with Data do
  begin
    DisableControls;
    First;
    while (not Eof) do
    begin
      with DetPresupuesto do
      begin
        SetCodigoProducto(FieldValues['codigo_producto']);
        SetReferencia(FieldValues['referencia']);
        SetDescripcion(FieldValues['descripcion']);
        SetCantidad(FieldValues['cantidad']);
        SetPrecio(FieldValues['precio']);
        SetPDescuento(FieldValues['p_descuento']);
        SetMontoDescuento(FieldValues['monto_descuento']);
        SetImpuesto(FieldValues['impuesto']);
        SetPIva(FieldValues['p_iva']);
        SetTotal(FieldValues['total']);
        SetIdPresupuesto(Clase.GetIdPresupuesto);
        Insertar;
      end;
      Next;
    end;
    EnableControls;
  end;
end;

procedure TFrmPresupuesto.Imprimir;
begin
  if (Length(Trim(TxtNumero.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
  with Reporte do
  begin
    AgregarParametro('NumeroPresupuesto',Clase.GetNumero);
    AgregarParametro('MontoLetra',NumeroALetras(Clase.GetTotal));
    if (Clase.GetFormaLibre) then
    begin
      EmiteReporte('PresupuestoFormaLibre',trGeneral);
    end
    else
    begin
      EmiteReporte('Presupuesto',trGeneral);
    end;
  end;
end;

procedure TFrmPresupuesto.LblImpuestoDblClick(Sender: TObject);
var
  Aux: Currency;
begin
  if (Clase.GetFormaLibre) then
  begin
    Aux := 0;
    Solicitar(Self,'Impuesto',TxtImpuesto.Value,Aux);
    if (Aux > 0) then
    begin
      TxtImpuesto.Value := Aux;
      CalcularMontosCabecera;
    end;
  end
  else
  begin
    MessageBox(Self.Handle, 'El Presupuesto No Es de Forma "Concepto Libre"', PChar(MsgTituloError), MB_ICONSTOP or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end;
end;

procedure TFrmPresupuesto.LblMontoDescuentoDblClick(Sender: TObject);
var
  Aux: Currency;
begin
  Aux := 0;
  Solicitar(Self,'Monto Descuento',TxtMontoDescuento.Value,Aux);
  if (Aux >= 0) then
  begin
    TxtMontoDescuento.Value := Aux;
    TxtPDescuento.Caption := FormatFloat('#,##0.0000%',Aux * 100 / TxtMontoNeto.Value);
    if (Clase.GetFormaLibre) then
    begin
      CalcularMontosCabecera;
    end
    else
    begin
      CalcularMontos;
    end;
  end;
end;

procedure TFrmPresupuesto.LblMontoNetoDblClick(Sender: TObject);
begin
  if (Data.IsEmpty) then
  begin
    if (MessageBox(Self.Handle, '�Desea Activar Forma Concepto Libre?', PChar(MsgTituloInformacion), MB_ICONQUESTION or MB_YESNO or MB_TOPMOST or MB_TASKMODAL) = mrYes) then
    begin
      Clase.SetFormaLibre(True);
      TabDetalle.TabVisible := False;
      PageDetalles.ActivePage := TabConcepto;
      TxtMontoNeto.ReadOnly := False;
    end
    else
    begin
      Clase.SetFormaLibre(False);
      TabDetalle.TabVisible := True;
      PageDetalles.ActivePage := TabDetalle;
      TxtMontoNeto.ReadOnly := True;
      TxtMontoNeto.Value := 0;
      TxtMontoDescuento.Value := 0;
      TxtPDescuento.Caption := FormatFloat('#,##0.0000%',0);
      TxtSubTotal.Value := 0;
      TxtImpuesto.Value := 0;
      TxtTotal.Value := 0;
    end;
  end;
end;

procedure TFrmPresupuesto.Limpiar;
begin
  TxtNumero.Text := '';
  CmbCodigoCliente.KeyValue := '';
  CmbCodigoVendedor.KeyValue := '';
  CmbCodigoZona.KeyValue := '';
  CmbCodigoEsquemaPago.KeyValue := '';
  CmbCodigoMoneda.KeyValue := '';
  DatFechaIngreso.Date := Date;
  DatFechaVencimiento.Date := Date;
  TxtConcepto.Text := '';
  TxtObservaciones.Text := '';
  TxtMontoNeto.Value := 0;
  TxtMontoDescuento.Value := 0;
  TxtSubTotal.Value := 0;
  TxtImpuesto.Value := 0;
  TxtTotal.Value := 0;
  TxtPDescuento.Caption := FormatFloat('###0.0000%',0);
  TxtPIVA.Caption := FormatFloat('###0.0000%',0);
  //Status Bar
  SB.Panels[0].Text := '';
  Data.EmptyDataSet;
  //Formato Concepto Libre
  TxtMontoNeto.ReadOnly := True;
  TabDetalle.TabVisible := True;
  PageDetalles.ActivePage := TabDetalle;
  Clase.SetFormaLibre(False);
end;

procedure TFrmPresupuesto.Mover;
begin

end;

procedure TFrmPresupuesto.Obtener;
begin
  with Clase do
  begin
    SetNumero(TxtNumero.Text);
    SetCodigoCliente(CmbCodigoCliente.KeyValue);
    SetCodigoVendedor(CmbCodigoVendedor.KeyValue);
    SetCodigoZona(CmbCodigoZona.KeyValue);
    SetCodigoEsquemaPago(CmbCodigoEsquemaPago.KeyValue);
    SetCodigoMoneda(CmbCodigoMoneda.KeyValue);
    SetFechaIngreso(DatFechaIngreso.Date);
    SetFechaVencimiento(DatFechaVencimiento.Date);
    SetConcepto(TxtConcepto.Text);
    SetObservaciones(TxtObservaciones.Text);
    SetMontoNeto(TxtMontoNeto.Value);
    SetMontoDescuento(TxtMontoDescuento.Value);
    SetSubTotal(TxtSubTotal.Value);
    SetImpuesto(TxtImpuesto.Value);
    SetTotal(TxtTotal.Value);
    SetPDescuento(FormatearFloat(StringReplace(TxtPDescuento.Caption,'%','',[rfReplaceAll])));
    SetPIva(FormatearFloat(StringReplace(TxtPIVA.Caption,'%','',[rfReplaceAll])));
    SetTasaDolar(0); //***//
    SetLoginUsuario(P.User.GetLoginUsuario);
    if (Agregando) then
    begin
      SetTiempoIngreso(Now);
    end;
  end;
end;

procedure TFrmPresupuesto.Refrescar;
begin
  //Actualizando Dataset de Busqueda
  DataBuscadorPresupuesto := BuscadorPresupuesto.ObtenerLista;
  //Se Actualizan Los demas Componentes
  DataCliente := Cliente.ObtenerCombo;
  DataVendedor := Vendedor.ObtenerCombo;
  DataZona := Zona.ObtenerCombo;
  DataEsquemaPago := EsquemaPago.ObtenerCombo;
  DataMoneda := Moneda.ObtenerCombo;
  DataCliente.First;
  DataVendedor.First;
  DataZona.First;
  DataEsquemaPago.First;
  DataMoneda.First;
  DataBuscadorPresupuesto.First;
  DSCliente.DataSet := DataCliente;
  DSVendedor.DataSet := DataVendedor;
  DSZona.DataSet := DataZona;
  DSEsquemaPago.DataSet := DataEsquemaPago;
  DSMoneda.DataSet := DataMoneda;
end;

procedure TFrmPresupuesto.TBtnEliminarClick(Sender: TObject);
begin
  if (Length(Trim(TxtNumero.Text)) > 0) then
  begin
    inherited;
  end
  else
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
end;

procedure TFrmPresupuesto.TxtMontoNetoExit(Sender: TObject);
begin
  CalcularMontosCabecera;
end;

procedure TFrmPresupuesto.TxtPDescuentoDblClick(Sender: TObject);
var
  Aux: Currency;
begin
  Aux := 0;
  Solicitar(Self,'(%) Descuento',StrToFloat(StringReplace(TxtPDescuento.Caption,'%','',[rfReplaceAll])),Aux);
  if (Aux >= 0) then
  begin
    TxtPDescuento.Caption := FormatFloat('#,##0.0000%',Aux);
    TxtMontoDescuento.Value := Aux * TxtMontoNeto.Value / 100;
    if (Clase.GetFormaLibre) then
    begin
      CalcularMontosCabecera;
    end
    else
    begin
      CalcularMontos;
    end;
  end;
end;

procedure TFrmPresupuesto.TxtPIVADblClick(Sender: TObject);
var
  Aux: Currency;
  Exentos: Boolean;
  BMark: TBookmark;
begin
  if (Clase.GetFormaLibre) then //Forma Concepto Libre
  begin
    Aux := 0;
    Solicitar(Self,'(%) I.V.A.',StrToFloat(StringReplace(TxtPIVA.Caption,'%','',[rfReplaceAll])),Aux);
    if (Aux >= 0) then
    begin
      TxtPIVA.Caption := FormatFloat('#,##0.0000%',Aux);
      TxtImpuesto.Value := Aux * TxtMontoNeto.Value / 100;
      CalcularMontosCabecera;
    end;
  end
  else
  begin
    Aux := 0;
    Solicitar(Self,'(%) I.V.A.',StrToFloat(StringReplace(TxtPIVA.Caption,'%','',[rfReplaceAll])),Aux);
    if (Aux >= 0) then
    begin
      TxtPIVA.Caption := FormatFloat('#,##0.0000%',Aux);
      if (not Data.IsEmpty) then
      begin
        if (MessageBox(Self.Handle, '�Desea Actualizar El Porcentaje De Impuesto En Todos Los Renglones?', PChar(MsgTituloInformacion), MB_ICONQUESTION or MB_YESNO or MB_TOPMOST or MB_TASKMODAL) = mrYes) then
        begin
          if (MessageBox(Self.Handle, '�Incluir Renglones Exentos de I.V.A.?', PChar(MsgTituloInformacion), MB_ICONQUESTION or MB_YESNO or MB_TOPMOST or MB_TASKMODAL) = mrYes) then
          begin
            Exentos := True;
          end
          else
          begin
            Exentos := False;
          end;
          //Actualizando PIVA en Renglones
          with Data do
          begin
            DisableControls;
            First;
            while (not Eof) do
            begin
              if ((FieldValues['impuesto'] > 0) or (Exentos)) then
              begin
                //Utilizando Punteros de los Registros (Bookmarks)
                //Debido a una deficiencia en la edicion de ClientDataSets
                BMark := GetBookmark;
                Edit;
                FieldValues['p_iva'] := Aux;
                FieldValues['impuesto'] := (FieldValues['precio'] * FieldValues['cantidad']) * (FieldValues['p_iva'] / 100);
                Post;
                GotoBookmark(BMark);
              end;
              Next;
            end;
            EnableControls;
          end;
          MessageBox(Self.Handle, 'Renglones Actualizados Correctamente', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
        end;
      end;
      CalcularMontos;
    end;
  end;
end;

function TFrmPresupuesto.Validar: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  //Validando Cliente
  if (Length(Trim(CmbCodigoCliente.Text)) = 0) then
  begin
    ListaErrores.Add('El Cliente No Puede Estar En Blanco!!!');
  end;
  //Validando Vendedor
  if (Length(Trim(CmbCodigoVendedor.Text)) = 0) then
  begin
    ListaErrores.Add('El Vendedor No Puede Estar En Blanco!!!');
  end;
  //Validando Monto
  if (TxtTotal.Value <= 0) then
  begin
    ListaErrores.Add('El Total Debe Ser Positivo!!!');
  end;
  //Validando Fechas
  if ((DatFechaIngreso.Date > DatFechaVencimiento.Date)) then
  begin
    ListaErrores.Add('La Fecha de Ingreso Debe Ser Menor a la Fecha de Vencimiento..!!!');
  end;
  //Validando Detalle o Forma Libre
  if (Clase.GetFormaLibre) then
  begin
    if (Length(Trim(TxtConcepto.Text)) = 0) then
    begin
      ListaErrores.Add('El Concepto No Puede Estar En Blanco!!!');
    end;
    if (TxtMontoNeto.Value <= 0) then
    begin
      ListaErrores.Add('El Monto Neto Debe Ser Positivo!!!');
    end;
    if (TxtMontoDescuento.Value < 0) then
    begin
      ListaErrores.Add('El Monto de Descuento No Debe Ser Negativo!!!');
    end;
    if (TxtImpuesto.Value < 0) then
    begin
      ListaErrores.Add('El Impuesto No Debe Ser Negativo!!!');
    end;
  end
  else
  begin
    if (Data.RecordCount = 0) then
    begin
      ListaErrores.Add('El Detalle De Presupuesto No Puede Estar Vacio!!!');
    end;
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
