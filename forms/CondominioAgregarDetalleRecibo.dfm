inherited FrmCondominioAgregarDetalleRecibo: TFrmCondominioAgregarDetalleRecibo
  Caption = 'Sophie - Agregar Gasto'
  ClientHeight = 136
  OnCreate = FormCreate
  ExplicitHeight = 160
  PixelsPerInch = 96
  TextHeight = 13
  object LblMonto: TLabel [1]
    Left = 24
    Top = 64
    Width = 34
    Height = 13
    Alignment = taRightJustify
    Caption = 'Monto:'
  end
  inherited PanelBotones: TPanel
    Top = 96
  end
  inherited BtnBuscar: TBitBtn
    OnClick = BtnBuscarClick
  end
  object TxtMonto: TCurrencyEdit
    Left = 65
    Top = 60
    Width = 100
    Height = 21
    Margins.Left = 4
    Margins.Top = 1
    DecimalPlaces = 4
    DisplayFormat = '#,##0.0000'
    TabOrder = 3
    OnKeyDown = TxtMontoKeyDown
  end
  object TxtDescripcionGasto: TEdit
    Left = 64
    Top = 35
    Width = 249
    Height = 21
    Enabled = False
    TabOrder = 4
    OnKeyDown = TxtCodigoKeyDown
  end
end
