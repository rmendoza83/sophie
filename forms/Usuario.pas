unit Usuario;

{$I sophie.inc}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Generico, XPMan, ImgList, DB, Grids, DBGrids, StdCtrls, Buttons,
  ExtCtrls, ComCtrls, ToolWin, ClsUser, DBClient, Utils
  {$IFDEF DELPHI10}, ImageList{$ENDIF};

type
  TFrmUsuario = class(TFrmGenerico)
    LblLogin: TLabel;
    TxtLogin: TEdit;
    LblNombre: TLabel;
    TxtNombre: TEdit;
    LblApellido: TLabel;
    TxtApellido: TEdit;
    LblSuperUsuario: TLabel;
    TxtSuperUsuario: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure GrdBusquedaCellClick(Column: TColumn);
  private
    { Private declarations }
    Clase: TUser;
    Data: TClientDataSet;
    procedure Agregar;
    procedure Editar;
    procedure Eliminar;
    procedure Guardar;
    procedure Cancelar;
    procedure Refrescar;
    procedure Imprimir;
    procedure Buscar;
    procedure Limpiar;
    procedure Asignar;
    procedure Obtener;
    procedure Mover;
  protected
    function Validar: Boolean; override;
  public
    { Public declarations }
  end;

var
  FrmUsuario: TFrmUsuario;

implementation

{$R *.dfm}

procedure TFrmUsuario.FormCreate(Sender: TObject);
begin
  ProcAgregar := Agregar;
  ProcEditar := Editar;
  ProcEliminar := Eliminar;
  ProcGuardar := Guardar;
  ProcCancelar := Cancelar;
  ProcRefrescar := Refrescar;
  ProcImprimir := Imprimir;
  ProcBuscar := Buscar;
  Clase := TUser.Create;
  Buscar;
end;

procedure TFrmUsuario.GrdBusquedaCellClick(Column: TColumn);
begin
  Mover;
end;

procedure TFrmUsuario.Agregar;
begin
  //Verificando
  Limpiar;
  TxtLogin.SetFocus;
end;

procedure TFrmUsuario.Asignar;
begin
  with Clase do
  begin
    TxtLogin.Text := GetLoginUsuario;
    TxtNombre.Text := GetNombres;
    TxtApellido.Text := GetApellidos;
    TxtSuperUsuario.Checked := GetSuperUsuario;
  end;
end;

procedure TFrmUsuario.Buscar;
begin
  Screen.Cursor := crSQLWait;
  if (Length(Trim(TxtCriterio.Text)) > 0) then
  begin
    Data := Clase.ObtenerListaCriterio(Trim(TxtCriterio.Text));
  end
  else
  begin
    Data := Clase.ObtenerLista;
  end;
  Data.First;
  DS.DataSet := Data;
  GrdBusqueda.Refresh;
  Screen.Cursor := crDefault;
end;

procedure TFrmUsuario.Cancelar;
begin
  TxtLogin.Enabled := True;
end;

procedure TFrmUsuario.Editar;
begin
  //Se Busca el Elemento
  Clase.SetLoginUsuario(Data.FieldValues['login_usuario']);
  Clase.Buscar;
  Asignar;
  TxtLogin.Enabled := False;
  TxtNombre.SetFocus;
end;

procedure TFrmUsuario.Eliminar;
begin
  //Se Busca el Elemento
  Clase.SetLoginUsuario(Data.FieldValues['login_usuario']);
  Clase.Buscar;
  Clase.Eliminar;
  MessageBox(Self.Handle, 'El Usuario Ha Sido Eliminado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  Refrescar;
  Limpiar;
end;

procedure TFrmUsuario.Guardar;
begin
  if (Agregando) then
  begin
    //Agregando...
    //Se Busca el Codigo
    Clase.SetLoginUsuario(TxtLogin.Text);
    if (Clase.Buscar) then
    begin
      MessageBox(Self.Handle, 'El C�digo Ya Existe!!!', PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      TxtLogin.SetFocus;
      Abort;
    end
    else
    begin
      Obtener;
      Clase.Insertar;
      MessageBox(Self.Handle, 'El Usuario Ha Sido Agregado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    end;
  end
  else
  begin
    //Editando...
    Obtener;
    Clase.Modificar;
    MessageBox(Self.Handle, 'El Usuario Ha Sido Editado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    TxtLogin.Enabled := True;
  end;
  Refrescar;
  Limpiar;
end;

procedure TFrmUsuario.Imprimir;
begin

end;

procedure TFrmUsuario.Limpiar;
begin
  TxtLogin.Text := '';
  TxtNombre.Text := '';
  TxtApellido.Text :='';
  TxtSuperUsuario.Checked := False;
end;

procedure TFrmUsuario.Mover;
var
  DSTemp: TDataSet;
begin
  if (DS.DataSet.RecordCount > 0) then
  begin
    DSTemp := DS.DataSet;
    with Clase do
    begin
      SetLoginUsuario(DSTemp.FieldValues['login_usuario']);
      SetNombres(DSTemp.FieldValues['nombres']);
      SetApellidos(DSTemp.FieldValues['apellidos']);
      SetSuperUsuario(DSTemp.FieldValues['super_usuario']);
    end;
    Asignar;
  end
  else
  begin
    Limpiar;
  end;
end;

procedure TFrmUsuario.Obtener;
begin
  with Clase do
  begin
    SetLoginUsuario(TxtLogin.Text);
    SetNombres(TxtNombre.Text);
    SetApellidos(TxtApellido.Text);
    SetSuperUsuario(TxtSuperUsuario.Checked);
  end;
end;

procedure TFrmUsuario.Refrescar;
begin
  TxtCriterio.Text := '';
  Buscar;
end;

function TFrmUsuario.Validar: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  (* Validando Informacion de Usuario *)
  //Validando Login
  if (Length(Trim(TxtLogin.Text)) = 0) then
  begin
    ListaErrores.Add('El Login No Puede Estar En Blanco!!!');
  end;
  //Validando Nombre
  if (Length(Trim(TxtNombre.Text)) = 0) then
  begin
    ListaErrores.Add('El Nombre No Puede Estar En Blanco!!!');
  end;
  //Validando Apellido
  if (Length(Trim(TxtApellido.Text)) = 0) then
  begin
    ListaErrores.Add('El Apellido No Puede Estar En Blanco!!!');
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
