unit ValidarLicencia;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, mxProtector, Buttons, PModule, XPMan;

type
  TFrmValidarLicencia = class(TForm)
    LblTitulo: TLabel;
    Label1: TLabel;
    BtnAceptar: TBitBtn;
    BtnRegistrar: TBitBtn;
    LblTiempoRestante: TLabel;
    LblDias: TLabel;
    MXP: TmxProtector;
    LblFechaSistema: TLabel;
    TemaXP: TXPManifest;
    procedure BtnRegistrarClick(Sender: TObject);
    procedure MXPGetRegistryPath(Sender: TObject; var APath: string);
    procedure BtnAceptarClick(Sender: TObject);
    procedure MXPInvalidSystemTime(Sender: TObject);
    procedure MXPRegisteredVersion(Sender: TObject);
    procedure MXPUnRegisteredVersion(Sender: TObject);
    procedure MXPDayTrial(Sender: TObject; DaysRemained: Integer);
    procedure MXPExpiration(Sender: TObject);
    procedure MXPCheckRegistration(Sender: TObject; var UserName,
      SerialNumber: string; var Registered: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmValidarLicencia: TFrmValidarLicencia;

implementation

{$R *.dfm}

uses
  RegistrarLicencia,
  Clipbrd;

procedure TFrmValidarLicencia.BtnAceptarClick(Sender: TObject);
begin
  Close;
end;

procedure TFrmValidarLicencia.BtnRegistrarClick(Sender: TObject);
var
  CB: TClipboard;
begin
  CB := TClipboard.Create;
  CB.AsText := P.GetSophieValidationPath;
  FrmRegistrarLicencia := TFrmRegistrarLicencia.Create(Self);
  FrmRegistrarLicencia.ShowModal;
  FreeAndNil(FrmRegistrarLicencia);
  MXP.CheckRegistration;
  if (MXP.IsRegistered) then
  begin
    Close;
  end;  
end;

procedure TFrmValidarLicencia.MXPCheckRegistration(Sender: TObject;
  var UserName, SerialNumber: string; var Registered: Boolean);
begin
  P.SetSophieRegisteredUser(UserName);
end;

procedure TFrmValidarLicencia.MXPDayTrial(Sender: TObject;
  DaysRemained: Integer);
begin
  if (DaysRemained <= 5) then
  begin
    LblDias.Font.Style := [];
    LblDias.Font.Color := clRed;
  end
  else
  begin
    LblDias.Font.Style := [];
    LblDias.Font.Color := clBlue;
  end;
  LblDias.Caption := IntToStr(DaysRemained) + ' Dias...';
  P.SophieValidado := True;
end;

procedure TFrmValidarLicencia.MXPExpiration(Sender: TObject);
begin
  LblDias.Font.Style := [fsBold];
  LblDias.Font.Color := clRed;
  LblDias.Caption := 'Tiempo Expirado';
  P.SophieValidado := False;
end;

procedure TFrmValidarLicencia.MXPGetRegistryPath(Sender: TObject;
  var APath: string);
begin
  if (MXP.RegistryRootKey = rkLocalMachine) then
  begin
    P.SetSophieValidationPath('HKLM' + APath);
  end
  else
  begin
    P.SetSophieValidationPath('HKCU' + APath);
  end;
end;

procedure TFrmValidarLicencia.MXPInvalidSystemTime(Sender: TObject);
begin
  P.SophieValidado := False;
  LblFechaSistema.Visible := True;
end;

procedure TFrmValidarLicencia.MXPRegisteredVersion(Sender: TObject);
begin
  P.SophieRegistrado := True;
  P.SophieValidado := True;
  BtnRegistrar.Enabled := False;
end;

procedure TFrmValidarLicencia.MXPUnRegisteredVersion(Sender: TObject);
begin
  P.SophieRegistrado := False;
end;

end.
