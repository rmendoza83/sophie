unit AgregarDetalleRadioLiquidacionContrato;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GenericoAgregarDetalle, StdCtrls, Buttons, ExtCtrls, Utils, DBClient,
  ClsBusquedaFacturaRadioContrato, ClsRadioDetLiquidacionContrato,
  ClsRadioContrato, GenericoBuscador, Mask, rxToolEdit, rxCurrEdit;

type
  TFrmAgregarDetalleRadioLiquidacionContrato = class(TFrmGenericoAgregarDetalle)
    LblNumeroContrato: TLabel;
    LblCliente: TLabel;
    LblItem: TLabel;
    LblTotal: TLabel;
    TxtItem: TEdit;
    TxtNumeroContrato: TEdit;
    TxtCodigoCliente: TEdit;
    TxtTotal: TCurrencyEdit;
    LblRazonSocialCliente: TLabel;
    procedure BtnBuscarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    ArrayVariant: TArrayVariant;
    BusquedaFacturaRadioContrato: TBusquedaFacturaRadioContrato;
    RadioDetLiquidacionContrato: TRadioDetLiquidacionContrato;
    RadioContrato: TRadioContrato;
  protected
    { Protected declarations }
    function Buscar: Boolean; override;
    procedure Agregar; override;
    procedure Limpiar; override;
    function Validar: Boolean;
  public
    { Public declarations }
    CodigoCliente: string;
  end;

var
  FrmAgregarDetalleRadioLiquidacionContrato: TFrmAgregarDetalleRadioLiquidacionContrato;

implementation

uses DB;

{$R *.dfm}

procedure TFrmAgregarDetalleRadioLiquidacionContrato.BtnBuscarClick(
  Sender: TObject);
var
  Condicion: string;
begin
  inherited;
  Condicion := '"codigo_cliente" = ' + QuotedStr(CodigoCliente);
  if (Buscador(Self,BusquedaFacturaRadioContrato,'numero_contrato;item','Buscar Distribución de Factura',nil,nil,True,ArrayVariant,Condicion)) then
  begin
    Buscar;
  end
  else
  begin
    SetLength(ArrayVariant,0);
  end;
end;

function TFrmAgregarDetalleRadioLiquidacionContrato.Buscar: Boolean;
begin
  if (Length(ArrayVariant) > 0) then
  begin
    RadioContrato.SetNumero(ArrayVariant[0]);
    RadioContrato.BuscarNumero;
    BusquedaFacturaRadioContrato.SetIdRadioContrato(RadioContrato.GetIdRadioContrato);
    BusquedaFacturaRadioContrato.SetItem(ArrayVariant[1]);
    Result := BusquedaFacturaRadioContrato.Buscar;
    if (Result) then
    begin
      TxtNumeroContrato.Text := BusquedaFacturaRadioContrato.GetNumeroContrato;
      TxtItem.Text := IntToStr(BusquedaFacturaRadioContrato.GetItem) + '/' + IntToStr(BusquedaFacturaRadioContrato.GetTotalItems);
      TxtCodigoCliente.Text := BusquedaFacturaRadioContrato.GetCodigoCliente;
      LblRazonSocialCliente.Caption := BusquedaFacturaRadioContrato.GetRazonSocial;
      TxtTotal.Value := BusquedaFacturaRadioContrato.GetTotal;
    end
    else
    begin
      MessageBox(Self.Handle, Cadena('La Distribución de Factura No Existe!!!'), PChar(MsgTituloError), MB_ICONSTOP or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      Limpiar;
    end;
  end
  else
  begin
    Result := False;
  end;
end;

procedure TFrmAgregarDetalleRadioLiquidacionContrato.FormCreate(
  Sender: TObject);
begin
  inherited;
  ComponenteFoco := nil;
  BusquedaFacturaRadioContrato := TBusquedaFacturaRadioContrato.Create;
  RadioDetLiquidacionContrato := TRadioDetLiquidacionContrato.Create;
  RadioContrato := TRadioContrato.Create;
end;

procedure TFrmAgregarDetalleRadioLiquidacionContrato.Limpiar;
begin
  inherited;
  TxtNumeroContrato.Text := '';
  TxtItem.Text := '';
  TxtCodigoCliente.Text := '';
  LblRazonSocialCliente.Caption := '';
  TxtTotal.Text := '';
end;

function TFrmAgregarDetalleRadioLiquidacionContrato.Validar: Boolean;
var
  ListaErrores: TStringList;
  sw: Boolean;
begin
  ListaErrores := TStringList.Create;
  //Validando Existencia de la Factura a Generar
  sw := false;
  with Data do
  begin
    First;
    while (not Eof) do
    begin
      if ((FieldValues['numero_contrato'] = BusquedaFacturaRadioContrato.GetNumeroContrato) and
          (FieldValues['item'] = BusquedaFacturaRadioContrato.GetItem)) then
      begin
        sw := True;
        Break;
      end;
      Next;
    end;
  end;
  if (sw) then
  begin
    ListaErrores.Add('La Factura Ya Se Encuentra En La Lista!!!');
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

procedure TFrmAgregarDetalleRadioLiquidacionContrato.Agregar;
begin
  if (Validar) then
  begin
    with Data do
    begin
      Append;
      FieldValues['numero_contrato'] := BusquedaFacturaRadioContrato.GetNumeroContrato;
      FieldValues['total_contrato'] := BusquedaFacturaRadioContrato.GetTotalRadioContrato;
      FieldValues['item'] := BusquedaFacturaRadioContrato.GetItem;
      FieldValues['total_items'] := BusquedaFacturaRadioContrato.GetTotalItems;
      FieldValues['fecha_ingreso'] := BusquedaFacturaRadioContrato.GetFechaIngreso;
      FieldValues['total'] := BusquedaFacturaRadioContrato.GetTotal;
      FieldValues['fecha_inicial'] := BusquedaFacturaRadioContrato.GetFechaInicial;
      FieldValues['fecha_final'] := BusquedaFacturaRadioContrato.GetFechaFinal;
      FieldValues['observaciones'] := BusquedaFacturaRadioContrato.GetObservaciones;
      FieldValues['id_radio_contrato'] := BusquedaFacturaRadioContrato.GetIdRadioContrato;
      Post;
    end;
  end
  else
  begin
    ModalResult := mrNone;
  end;
end;

end.
