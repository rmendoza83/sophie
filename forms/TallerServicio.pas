unit TallerServicio;

{$I sophie.inc}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GenericoDetalle, DB, DBClient, ImgList, XPMan, ComCtrls, Grids,
  DBGrids, ExtCtrls, ToolWin, StdCtrls, Buttons, rxCurrEdit, Mask, rxToolEdit,
  RxLookup, Utils, ClsTallerServicio, ClsCliente, ClsVendedor, ClsZona,
  ClsTallerDetServicio, ClsProducto, ClsTallerBusquedaServicio,
  ClsClienteVehiculo, GenericoBuscador, ClsReporte, ClsParametro
  {$IFDEF DELPHI10}, ImageList{$ENDIF};

type
  TFrmTallerServicio = class(TFrmGenericoDetalle)
    LblNumero: TLabel;
    LblCodigoCliente: TLabel;
    LblCodigoVendedor: TLabel;
    LblCodigoZona: TLabel;
    LblFechaIngreso: TLabel;
    LblMontoNeto: TLabel;
    LblMontoDescuento: TLabel;
    LblSubTotal: TLabel;
    LblImpuesto: TLabel;
    LblTotal: TLabel;
    TxtPDescuento: TLabel;
    TxtPIVA: TLabel;
    TxtNumero: TEdit;
    CmbCodigoCliente: TRxDBLookupCombo;
    DatFechaIngreso: TDateEdit;
    TxtMontoNeto: TCurrencyEdit;
    CmbCodigoVendedor: TRxDBLookupCombo;
    CmbCodigoZona: TRxDBLookupCombo;
    TxtMontoDescuento: TCurrencyEdit;
    TxtSubTotal: TCurrencyEdit;
    TxtImpuesto: TCurrencyEdit;
    TxtTotal: TCurrencyEdit;
    BtnBuscarCliente: TBitBtn;
    BtnBuscarVendedor: TBitBtn;
    BtnBuscarZona: TBitBtn;
    DSCliente: TDataSource;
    DSClienteVehiculo: TDataSource;
    DSVendedor: TDataSource;
    DSZona: TDataSource;
    LblClienteVehiculo: TLabel;
    CmbClienteVehiculo: TRxDBLookupCombo;
    BtnBuscarClienteVehiculo: TBitBtn;
    TabConcepto: TTabSheet;
    TabObservaciones: TTabSheet;
    TxtConcepto: TRichEdit;
    TxtObservaciones: TRichEdit;
    Datacodigo_producto: TStringField;
    Datareferencia: TStringField;
    Datadescripcion: TStringField;
    Datacantidad: TFloatField;
    Dataprecio: TFloatField;
    Datap_descuento: TFloatField;
    Datamonto_descuento: TFloatField;
    Dataimpuesto: TFloatField;
    Datap_iva: TFloatField;
    Datatotal: TFloatField;
    Datafacturable: TBooleanField;
    Dataobservaciones: TStringField;
    Dataid_taller_servicio: TIntegerField;
    LblKilometraje: TLabel;
    TxtKilometraje: TCurrencyEdit;
    procedure CmbCodigoClienteExit(Sender: TObject);
    procedure LblMontoNetoDblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TxtMontoNetoExit(Sender: TObject);
    procedure TxtPDescuentoDblClick(Sender: TObject);
    procedure TxtPIVADblClick(Sender: TObject);
    procedure CmbCodigoClienteChange(Sender: TObject);
    procedure LblImpuestoDblClick(Sender: TObject);
    procedure DataAfterPost(DataSet: TDataSet);
    procedure TBtnEliminarClick(Sender: TObject);
    procedure BtnBuscarClienteClick(Sender: TObject);
    procedure BtnBuscarClienteVehiculoClick(Sender: TObject);
    procedure BtnBuscarVendedorClick(Sender: TObject);
    procedure BtnBuscarZonaClick(Sender: TObject);
    procedure LblMontoDescuentoDblClick(Sender: TObject);
  private
    { Private declarations }
    Clase: TTallerServicio;
    TallerDetServicio: TTallerDetServicio;
    BuscadorTallerServicio: TTallerBusquedaServicio;
    Cliente: TCliente;
    ClienteVehiculo: TClienteVehiculo;
    Vendedor: TVendedor;
    Zona: TZona;
    Producto: TProducto;
    Reporte: TReporte;
    Parametro: TParametro;
    DataBuscadorTallerServicio: TClientDataSet;
    DataCliente: TClientDataSet;
    DataClienteVehiculo: TClientDataSet;
    DataVendedor: TClientDataSet;
    DataZona: TClientDataSet;
    procedure Agregar;
    procedure Editar;
    procedure Eliminar;
    procedure Guardar;
    procedure Cancelar;
    procedure Refrescar;
    procedure Imprimir;
    procedure Buscar;
    procedure Limpiar;
    procedure Asignar;
    procedure Obtener;
    procedure Mover;
    procedure GuardarDetalles;
    procedure CalcularMontos;
    procedure CalcularMontosCabecera;
  protected
    { Protected declarations }
    function Validar: Boolean; override;
    procedure Detalle; override;
  public
    { Public declarations }
  end;

var
  FrmTallerServicio: TFrmTallerServicio;

implementation

uses
  ActividadSophie,
  PModule,
  TallerAgregarDetalleServicio,
  SolicitarMonto
  {$IFDEF USE_RAVE_REPORTS}
  , DMRR
  {$ENDIF}
  {$IFDEF USE_FAST_REPORTS}
  , DMFR
  {$ENDIF}
  ;

{$R *.dfm}

{ TFrmTallerServicio }

procedure TFrmTallerServicio.Agregar;
begin
  //Verificando
  Limpiar;
  //Asignando Valores por Defecto
  TxtPIVA.Caption := FormatFloat('#,##0.0000%',Parametro.ObtenerValor('p_iva'));
  Clase.SetPIva(Parametro.ObtenerValor('p_iva'));
  CmbCodigoZona.Value := Parametro.ObtenerValor('codigo_zona');
  CmbCodigoCliente.SetFocus;
end;

procedure TFrmTallerServicio.Asignar;
begin
  if (Clase.GetFormaLibre) then
  begin
    TabDetalle.TabVisible := False;
    PageDetalles.ActivePage := TabConcepto;
    TxtMontoNeto.ReadOnly := False;
  end
  else
  begin
    TabDetalle.TabVisible := True;
    PageDetalles.ActivePage := TabDetalle;
    TxtMontoNeto.ReadOnly := True;
  end;
  with Clase do
  begin
    TxtNumero.Text := GetNumero;
    CmbCodigoCliente.KeyValue := GetCodigoCliente;
    DataClienteVehiculo := ClienteVehiculo.ObtenerComboCodigoCliente(GetCodigoCliente);
    DSClienteVehiculo.DataSet := DataClienteVehiculo;
    CmbClienteVehiculo.KeyValue := GetPlacaVehiculoCliente;
    CmbCodigoVendedor.KeyValue := GetCodigoVendedor;
    CmbCodigoZona.KeyValue := GetCodigoZona;
    DatFechaIngreso.Date := GetFechaIngreso;
    TxtKilometraje.Value := GetKilometraje;
    TxtConcepto.Text := GetConcepto;
    TxtObservaciones.Text := GetObservaciones;
    TxtMontoNeto.Value := GetMontoNeto;
    TxtMontoDescuento.Value := GetMontoDescuento;
    TxtSubTotal.Value := GetSubTotal;
    TxtImpuesto.Value := GetImpuesto;
    TxtTotal.Value := GetTotal;
    TxtPDescuento.Caption := FormatFloat('###0.0000%',GetPDescuento);
    TxtPIVA.Caption := FormatFloat('###0.0000%',GetPIva);
    Data.CloneCursor(TallerDetServicio.ObtenerIdTallerServicio(GetIdTallerServicio),False,True);
    DS.DataSet := Data;
  end;
end;

procedure TFrmTallerServicio.BtnBuscarClienteClick(Sender: TObject);
begin
  Buscador(Self,Cliente,'codigo','Buscar Cliente',nil,DataCliente);
  CmbCodigoCliente.KeyValue := DataCliente.FieldValues['codigo'];
  CmbCodigoClienteExit(Sender);
end;

procedure TFrmTallerServicio.BtnBuscarClienteVehiculoClick(Sender: TObject);
begin
  Buscador(Self,ClienteVehiculo,'placa','Buscar Veh�culo',nil,DataClienteVehiculo,'"codigo_cliente" = ' + QuotedStr(CmbCodigoCliente.Value));
  CmbClienteVehiculo.KeyValue := DataClienteVehiculo.FieldValues['placa'];
end;

procedure TFrmTallerServicio.BtnBuscarVendedorClick(Sender: TObject);
begin
  Buscador(Self,Vendedor,'codigo','Buscar Mec�nico',nil,DataVendedor);
  CmbCodigoVendedor.KeyValue := DataVendedor.FieldValues['codigo'];
end;

procedure TFrmTallerServicio.BtnBuscarZonaClick(Sender: TObject);
begin
  Buscador(Self,Zona,'codigo','Buscar Zona',nil,DataZona);
  CmbCodigoZona.KeyValue := DataZona.FieldValues['codigo'];
end;

procedure TFrmTallerServicio.Buscar;
begin
  if (Buscador(Self,BuscadorTallerServicio,'numero','Buscar Servicio',nil,DataBuscadorTallerServicio)) then
  begin
    Clase.SetNumero(DataBuscadorTallerServicio.FieldValues['numero']);
    Clase.BuscarNumero;
    Asignar;
  end;
end;

procedure TFrmTallerServicio.CalcularMontos;
var
  Neto, Descuento, Impuesto: Currency;
begin
  Neto := 0;
  Descuento := 0;
  Impuesto := 0;
  with Data do
  begin
    DisableControls;
    First;
    while (not Eof) do
    begin
      Neto := Neto + (FieldValues['precio'] * FieldValues['cantidad']);
      Descuento := Descuento + FieldValues['monto_descuento'];
      Impuesto := Impuesto + FieldValues['impuesto'];
      Next;
    end;
    EnableControls;
  end;
  //Asignando Montos
  TxtMontoNeto.Value := Neto;
  //Determinando si hay un Descuento Manual
  if ((TxtMontoDescuento.Value > 0) and (StrToFloat(StringReplace(TxtPDescuento.Caption,'%','',[rfReplaceAll])) > 0) and (Descuento = 0)) then
  begin
    Descuento := TxtMontoDescuento.Value;
  end;
  TxtMontoDescuento.Value := Descuento;
  TxtSubTotal.Value := Neto - Descuento;
  TxtImpuesto.Value := Impuesto;
  TxtTotal.Value := (Neto - Descuento) + Impuesto;
end;

procedure TFrmTallerServicio.CalcularMontosCabecera;
begin
  if (Clase.GetFormaLibre) then
  begin
    TxtSubTotal.Value := TxtMontoNeto.Value - TxtMontoDescuento.Value;
    TxtTotal.Value := TxtSubTotal.Value + TxtImpuesto.Value;
  end;
end;

procedure TFrmTallerServicio.Cancelar;
begin
  if (Agregando) then
  begin
    Limpiar;
  end
  else
  begin
    Clase.SetNumero(TxtNumero.Text);
    Clase.BuscarNumero;
    Asignar;
  end;
end;

procedure TFrmTallerServicio.CmbCodigoClienteChange(Sender: TObject);
begin
  SB.Panels[0].Text := DataCliente.FieldValues['razon_social'];
end;

procedure TFrmTallerServicio.CmbCodigoClienteExit(Sender: TObject);
begin
  if (Length(Trim(CmbCodigoCliente.Text)) > 0) then
  begin
    if (Agregando) then
    begin
      Cliente.SetCodigo(CmbCodigoCliente.Value);
      Cliente.BuscarCodigo;
      CmbCodigoVendedor.Value := Cliente.GetCodigoVendedor;
      CmbCodigoZona.Value := Cliente.GetCodigoZona;
    end;
    //Actualizando Listado de Vehiculos del Cliente
    DataClienteVehiculo := ClienteVehiculo.ObtenerComboCodigoCliente(CmbCodigoCliente.Value);
    DSClienteVehiculo.DataSet := DataClienteVehiculo;
    CmbClienteVehiculo.Value := '';
  end;
end;

procedure TFrmTallerServicio.DataAfterPost(DataSet: TDataSet);
begin
  CalcularMontos;
end;

procedure TFrmTallerServicio.Detalle;
begin
  inherited;
  FrmTallerAgregarDetalle := TFrmTallerAgregarDetalle.CrearDetalle(Self,Data);
  FrmTallerAgregarDetalle.ShowModal;
  FreeAndNil(FrmTallerAgregarDetalle);
end;

procedure TFrmTallerServicio.Editar;
var
  sw: Boolean;
begin
  if (Length(Trim(TxtNumero.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end
  else
  begin
    //Se Valida Que El Servicio Pueda Ser Anulado
    sw := True;
    if (Clase.GetIdFacturacionCliente <> -1) then
    begin
      sw := False;
    end;
    if not (sw) then
    begin
      MessageBox(Self.Handle, 'El Servicio Ya Presenta Una Factura Generada Y No Puede Ser Editada!!!',PChar(MsgTituloAdvertencia), MB_ICONEXCLAMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      Abort;
    end;
    //Se Ubican Los Vehiculos del Cliente
    DataClienteVehiculo := ClienteVehiculo.ObtenerComboCodigoCliente(Clase.GetCodigoCliente);
    DSClienteVehiculo.DataSet := DataClienteVehiculo;
  end;
end;

procedure TFrmTallerServicio.Eliminar;
var
  sw: Boolean;
begin
  //Se Valida Que El Servicio Pueda Ser Anulado
  sw := True;
  if (Clase.GetIdFacturacionCliente <> -1) then //No Puede Anularse
  begin
    sw := False;
  end;
  if (sw) then
  begin
    //Se Busca El Servicio!
    Clase.SetNumero(TxtNumero.Text);
    Clase.BuscarNumero;
    //Se Eliminan Los Detalles
    TallerDetServicio.EliminarIdTallerServicio(Clase.GetIdTallerServicio);
    Clase.Eliminar;
    MessageBox(Self.Handle, 'El Servicio Se Ha Eliminado Con Exito!!!',PChar(MsgTituloInformacion), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Refrescar;
    Limpiar;
  end
  else
  begin
    MessageBox(Self.Handle, 'El Servicio Ya Presenta Una Factura Generada Y No Puede Ser Eliminado!!!',PChar(MsgTituloAdvertencia), MB_ICONEXCLAMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end;
end;

procedure TFrmTallerServicio.FormCreate(Sender: TObject);
begin
  inherited;
  ProcAgregar := Agregar;
  ProcEditar := Editar;
  ProcEliminar := Eliminar;
  ProcGuardar := Guardar;
  ProcCancelar := Cancelar;
  ProcRefrescar := Refrescar;
  ProcImprimir := Imprimir;
  ProcBuscar := Buscar;
  FrmActividadSophie.Show;
  FrmActividadSophie.Actividad('Cargando N�cleo...');
  Clase := TTallerServicio.Create;
  //Inicializando DataSets Secundarios
  BuscadorTallerServicio := TTallerBusquedaServicio.Create;
  Cliente := TCliente.Create;
  ClienteVehiculo := TClienteVehiculo.Create;
  Vendedor := TVendedor.Create;
  Zona := TZona.Create;
  Producto := TProducto.Create;
  Reporte := TReporte.Create;
  Parametro := TParametro.Create;
  TallerDetServicio := TTallerDetServicio.Create;
  FrmActividadSophie.Actividad('Cargando Clientes...');
  DataCliente := Cliente.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Mec�nicos...');
  DataVendedor := Vendedor.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Zonas...');
  DataZona := Zona.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Resto del N�cleo...');
  DataBuscadorTallerServicio := BuscadorTallerServicio.ObtenerLista;
  DataCliente.First;
  DataVendedor.First;
  DataZona.First;
  DataBuscadorTallerServicio.First;
  DSCliente.DataSet := DataCliente;
  DSVendedor.DataSet := DataVendedor;
  DSZona.DataSet := DataZona;
  ConfigurarRxDBLookupCombo(CmbCodigoCliente,DSCliente,'codigo','codigo;razon_social');
  ConfigurarRxDBLookupCombo(CmbClienteVehiculo,DSClienteVehiculo,'placa','placa;marca;modelo');
  ConfigurarRxDBLookupCombo(CmbCodigoVendedor,DSVendedor,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoZona,DSZona,'codigo','codigo;descripcion');
  DS.DataSet := Data;
  GridDetalle.DataSource := DS;
  FrmActividadSophie.Close;
end;

procedure TFrmTallerServicio.Guardar;
begin
  if (Clase.GetFormaLibre) then
  begin
    CalcularMontosCabecera;
    Data.EmptyDataSet;
  end
  else
  begin
    CalcularMontos;
  end;
  Obtener;
  if (Agregando) then
  begin
    //Agregando...
    //Guardando Encabezado
    Clase.Insertar;
    TxtNumero.Text := Clase.GetNumero;
    //Guardando Detalles
    GuardarDetalles;
    MessageBox(Self.Handle, 'El Servicio Ha Sido Agregado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    with Reporte do
    begin
      AgregarParametro('NumeroServicio',Clase.GetNumero);
      AgregarParametro('MontoLetra',NumeroALetras(Clase.GetTotal));
      if (Clase.GetFormaLibre) then
      begin
        ImprimeReporte('TallerServicioFormaLibre',trGeneral);
      end
      else
      begin
        ImprimeReporte('TallerServicio',trGeneral);
      end;
    end;
  end
  else
  begin
    //Editando...
    Clase.Modificar;
    //Actualizando Detalles (Eliminando Anteriores y Cargando los Nuevos)
    TallerDetServicio.EliminarIdTallerServicio(Clase.GetIdTallerServicio);
    //Guardando Detalles
    GuardarDetalles;
    MessageBox(Self.Handle, 'El Servicio Ha Sido Editado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end;
end;

procedure TFrmTallerServicio.GuardarDetalles;
begin
  with Data do
  begin
    DisableControls;
    First;
    while (not Eof) do
    begin
      with TallerDetServicio do
      begin
        SetCodigoProducto(FieldValues['codigo_producto']);
        SetReferencia(FieldValues['referencia']);
        SetDescripcion(FieldValues['descripcion']);
        SetCantidad(FieldValues['cantidad']);
        SetPrecio(FieldValues['precio']);
        SetPDescuento(FieldValues['p_descuento']);
        SetMontoDescuento(FieldValues['monto_descuento']);
        SetImpuesto(FieldValues['impuesto']);
        SetPIva(FieldValues['p_iva']);
        SetTotal(FieldValues['total']);
        SetFacturable(FieldValues['facturable']);
        SetObservaciones(FieldValues['observaciones']);
        SetIdTallerServicio(Clase.GetIdTallerServicio);
        Insertar;
      end;
      Next;
    end;
    EnableControls;
  end;
end;

procedure TFrmTallerServicio.Imprimir;
begin
  if (Length(Trim(TxtNumero.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
  with Reporte do
  begin
    AgregarParametro('NumeroServicio',Clase.GetNumero);
    AgregarParametro('MontoLetra',NumeroALetras(Clase.GetTotal));
    if (Clase.GetFormaLibre) then
    begin
      EmiteReporte('TallerServicioFormaLibre',trGeneral);
    end
    else
    begin
      EmiteReporte('TallerServicio',trGeneral);
    end;
  end;
end;

procedure TFrmTallerServicio.LblImpuestoDblClick(Sender: TObject);
var
  Aux: Currency;
begin
  if (Clase.GetFormaLibre) then
  begin
    Aux := 0;
    Solicitar(Self,'Impuesto',TxtImpuesto.Value,Aux);
    if (Aux >= 0) then
    begin
      TxtImpuesto.Value := Aux;
      CalcularMontosCabecera;
    end;
  end
  else
  begin
    MessageBox(Self.Handle, 'La Factura No Es de Forma "Concepto Libre"', PChar(MsgTituloError), MB_ICONSTOP or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end;
end;

procedure TFrmTallerServicio.LblMontoDescuentoDblClick(Sender: TObject);
var
  Aux: Currency;
begin
  Aux := 0;
  Solicitar(Self,'Monto Descuento',TxtMontoDescuento.Value,Aux);
  if (Aux >= 0) then
  begin
    TxtMontoDescuento.Value := Aux;
    TxtPDescuento.Caption := FormatFloat('#,##0.0000%',Aux * 100 / TxtMontoNeto.Value);
    if (Clase.GetFormaLibre) then
    begin
      CalcularMontosCabecera;
    end
    else
    begin
      CalcularMontos;
    end;
  end;
end;

procedure TFrmTallerServicio.LblMontoNetoDblClick(Sender: TObject);
begin
  if (Data.IsEmpty) then
  begin
    if (MessageBox(Self.Handle, '�Desea Activar Forma Concepto Libre?', PChar(MsgTituloInformacion), MB_ICONQUESTION or MB_YESNO or MB_TOPMOST or MB_TASKMODAL) = mrYes) then
    begin
      Clase.SetFormaLibre(True);
      TabDetalle.TabVisible := False;
      PageDetalles.ActivePage := TabConcepto;
      TxtMontoNeto.ReadOnly := False;
    end
    else
    begin
      Clase.SetFormaLibre(False);
      TabDetalle.TabVisible := True;
      PageDetalles.ActivePage := TabDetalle;
      TxtMontoNeto.ReadOnly := True;
      TxtMontoNeto.Value := 0;
      TxtMontoDescuento.Value := 0;
      TxtPDescuento.Caption := FormatFloat('#,##0.0000%',0);
      TxtSubTotal.Value := 0;
      TxtImpuesto.Value := 0;
      TxtTotal.Value := 0;
    end;
  end;
end;

procedure TFrmTallerServicio.Limpiar;
begin
  TxtNumero.Text := '';
  CmbCodigoCliente.KeyValue := '';
  CmbClienteVehiculo.KeyValue := '';
  CmbCodigoVendedor.KeyValue := '';
  CmbCodigoZona.KeyValue := '';
  DatFechaIngreso.Date := Date;
  TxtKilometraje.Text := '';
  TxtConcepto.Text := '';
  TxtObservaciones.Text := '';
  TxtMontoNeto.Value := 0;
  TxtMontoDescuento.Value := 0;
  TxtSubTotal.Value := 0;
  TxtImpuesto.Value := 0;
  TxtTotal.Value := 0;
  TxtPDescuento.Caption := FormatFloat('###0.0000%',0);
  TxtPIVA.Caption := FormatFloat('###0.0000%',0);
  //Status Bar
  SB.Panels[0].Text := '';
  Data.EmptyDataSet;
  //Formato Concepto Libre
  TxtMontoNeto.ReadOnly := True;
  TabDetalle.TabVisible := True;
  PageDetalles.ActivePage := TabDetalle;
  Clase.SetFormaLibre(False);
end;

procedure TFrmTallerServicio.Mover;
begin

end;

procedure TFrmTallerServicio.Obtener;
begin
  with Clase do
  begin
    SetNumero(TxtNumero.Text);
    SetCodigoCliente(CmbCodigoCliente.KeyValue);
    SetPlacaVehiculoCliente(CmbClienteVehiculo.KeyValue);
    SetCodigoVendedor(CmbCodigoVendedor.KeyValue);
    SetCodigoZona(CmbCodigoZona.KeyValue);
    SetFechaIngreso(DatFechaIngreso.Date);
    SetKilometraje(TxtKilometraje.Value);
    SetConcepto(TxtConcepto.Text);
    SetObservaciones(TxtObservaciones.Text);
    SetMontoNeto(TxtMontoNeto.Value);
    SetMontoDescuento(TxtMontoDescuento.Value);
    SetSubTotal(TxtSubTotal.Value);
    SetImpuesto(TxtImpuesto.Value);
    SetTotal(TxtTotal.Value);
    SetPDescuento(FormatearFloat(StringReplace(TxtPDescuento.Caption,'%','',[rfReplaceAll])));
    SetPIva(FormatearFloat(StringReplace(TxtPIVA.Caption,'%','',[rfReplaceAll])));
    SetLoginUsuario(P.User.GetLoginUsuario);
    if (Agregando) then
    begin
      SetTiempoIngreso(Now);
      SetIdTallerServicio(-1);
      SetIdFacturacionCliente(-1);
    end;
  end;
end;

procedure TFrmTallerServicio.Refrescar;
begin
  //Se Actualiza El DataBuscador
  DataBuscadorTallerServicio := BuscadorTallerServicio.ObtenerLista;
  //Se Actualizan Los demas Componentes
  DataCliente := Cliente.ObtenerCombo;
  if (CmbClienteVehiculo.Text = '') then
  begin
    DataClienteVehiculo := nil;
  end;
  DataVendedor := Vendedor.ObtenerCombo;
  DataZona := Zona.ObtenerCombo;
  DataCliente.First;
  DataVendedor.First;
  DataZona.First;
  DataBuscadorTallerServicio.First;
  DSCliente.DataSet := DataCliente;
  if (CmbClienteVehiculo.Text = '') then
  begin
    DSClienteVehiculo.DataSet := nil;
  end;
  DSVendedor.DataSet := DataVendedor;
  DSZona.DataSet := DataZona;
end;

procedure TFrmTallerServicio.TBtnEliminarClick(Sender: TObject);
begin
  if (Length(Trim(TxtNumero.Text)) > 0) then
  begin
    inherited;
  end
  else
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
end;

procedure TFrmTallerServicio.TxtMontoNetoExit(Sender: TObject);
begin
  CalcularMontosCabecera;
end;

procedure TFrmTallerServicio.TxtPDescuentoDblClick(Sender: TObject);
var
  Aux: Currency;
begin
  Aux := 0;
  Solicitar(Self,'(%) Descuento',StrToFloat(StringReplace(TxtPDescuento.Caption,'%','',[rfReplaceAll])),Aux);
  if (Aux > 0) then
  begin
    TxtPDescuento.Caption := FormatFloat('#,##0.0000%',Aux);
    TxtMontoDescuento.Value := Aux * TxtMontoNeto.Value / 100;
    CalcularMontosCabecera;
  end;
end;

procedure TFrmTallerServicio.TxtPIVADblClick(Sender: TObject);
var
  Aux: Currency;
  Exentos: Boolean;
  BMark: TBookmark;
begin
  if (Clase.GetFormaLibre) then //Forma Concepto Libre
  begin
    Aux := 0;
    Solicitar(Self,'(%) I.V.A.',StrToFloat(StringReplace(TxtPIVA.Caption,'%','',[rfReplaceAll])),Aux);
    if (Aux >= 0) then
    begin
      TxtPIVA.Caption := FormatFloat('#,##0.0000%',Aux);
      TxtImpuesto.Value := Aux * TxtMontoNeto.Value / 100;
      CalcularMontosCabecera;
    end;
  end
  else
  begin
    Aux := 0;
    Solicitar(Self,'(%) I.V.A.',StrToFloat(StringReplace(TxtPIVA.Caption,'%','',[rfReplaceAll])),Aux);
    if (Aux >= 0) then
    begin
      TxtPIVA.Caption := FormatFloat('#,##0.0000%',Aux);
      if (not Data.IsEmpty) then
      begin
        if (MessageBox(Self.Handle, '�Desea Actualizar El Porcentaje De Impuesto En Todos Los Renglones?', PChar(MsgTituloInformacion), MB_ICONQUESTION or MB_YESNO or MB_TOPMOST or MB_TASKMODAL) = mrYes) then
        begin
          if (MessageBox(Self.Handle, '�Incluir Renglones Exentos de I.V.A.?', PChar(MsgTituloInformacion), MB_ICONQUESTION or MB_YESNO or MB_TOPMOST or MB_TASKMODAL) = mrYes) then
          begin
            Exentos := True;
          end
          else
          begin
            Exentos := False;
          end;
          //Actualizando PIVA en Renglones
          with Data do
          begin
            DisableControls;
            First;
            while (not Eof) do
            begin
              if ((FieldValues['impuesto'] > 0) or (Exentos)) then
              begin
                //Utilizando Punteros de los Registros (Bookmarks)
                //Debido a una deficiencia en la edicion de ClientDataSets
                BMark := GetBookmark;
                Edit;
                FieldValues['p_iva'] := Aux;
                FieldValues['impuesto'] := FieldValues['precio'] * (Aux / 100);
                Post;
                GotoBookmark(BMark);
              end;
              Next;
            end;
            EnableControls;
          end;
          MessageBox(Self.Handle, 'Renglones Actualizados Correctamente', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
        end;
      end;
      CalcularMontos;
    end;
  end;
end;

function TFrmTallerServicio.Validar: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  //Validando Cliente
  if (Length(Trim(CmbCodigoCliente.Text)) = 0) then
  begin
    ListaErrores.Add('El Cliente No Puede Estar En Blanco!!!');
  end;
  //Validando Vehiculo del Cliente
  if (Length(Trim(CmbClienteVehiculo.Text)) = 0) then
  begin
    ListaErrores.Add('El Veh�culo del Cliente No Puede Estar En Blanco!!!');
  end;
  //Validando Vendedor
  if (Length(Trim(CmbCodigoVendedor.Text)) = 0) then
  begin
    ListaErrores.Add('El Mec�nico No Puede Estar En Blanco!!!');
  end;
  //Validando Monto
  if (TxtTotal.Value <= 0) then
  begin
    ListaErrores.Add('El Total Debe Ser Positivo!!!');
  end;
  //Validando Kilometraje
  if (TxtKilometraje.Value <= 0) then
  begin
    ListaErrores.Add('El Kilometraje Debe Ser Positivo!!!');
  end;
  //Validando Detalle o Forma Libre
  if (Clase.GetFormaLibre) then
  begin
    if (Length(Trim(TxtConcepto.Text)) = 0) then
    begin
      ListaErrores.Add('El Concepto No Puede Estar En Blanco!!!');
    end;
    if (TxtMontoNeto.Value <= 0) then
    begin
      ListaErrores.Add('El Monto Neto Debe Ser Positivo!!!');
    end;
    if (TxtMontoDescuento.Value < 0) then
    begin
      ListaErrores.Add('El Monto de Descuento No Debe Ser Negativo!!!');
    end;
    if (TxtImpuesto.Value < 0) then
    begin
      ListaErrores.Add('El Impuesto No Debe Ser Negativo!!!');
    end;
  end
  else
  begin
    if (Data.RecordCount = 0) then
    begin
      ListaErrores.Add('El Detalle Del Servicio No Puede Estar Vacio!!!');
    end;
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
