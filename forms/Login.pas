unit Login;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, XPMan, StdCtrls, Buttons, Utils, ClsUser;

const
  //Constantes de Control de Error en el Inicio de Sesion
  NoExiste = 'N';
  ClaveInvalida = 'C';
  CuentaExpirada = 'E';

type

  { TFrmLogin }
  
  TFrmLogin = class(TForm)
    LblTitulo: TLabel;
    LblLogin: TLabel;
    LblPassword: TLabel;
    BtnCancelar: TBitBtn;
    BtnAceptar: TBitBtn;
    TemaXP: TXPManifest;
    TxtLogin: TEdit;
    TxtPassword: TEdit;
    procedure TxtLoginKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure BtnCancelarClick(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
  private
    { Private declarations }
    Intentos: Integer;
    Cerrar: Boolean;
    User: TUser;
    function ValidarUsuario(var CharError: Char): Boolean;
    procedure ProcesarInicio;
  public
    { Public declarations }
    LoginUsuario: String;
  end;

var
  FrmLogin: TFrmLogin;

implementation

{ TFrmLogin }

{$R *.dfm}

procedure TFrmLogin.TxtLoginKeyPress(Sender: TObject; var Key: Char);
begin
  if (Pos(Key,TeclasValidas) = 0) then
  begin
    Key := #0;
  end;
end;

procedure TFrmLogin.FormCreate(Sender: TObject);
begin
  Cerrar := False;
  Intentos := 3;
  User := TUser.Create;
end;

procedure TFrmLogin.FormShow(Sender: TObject);
begin
  TxtLogin.SetFocus; 
end;

procedure TFrmLogin.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose := Cerrar;
end;

procedure TFrmLogin.BtnCancelarClick(Sender: TObject);
begin
  if (MessageBox(Self.Handle,PChar('�Esta seguro que desea Cancelar el Inicio de Sesi�n?'),PChar(MsgTituloAdvertencia),MB_ICONQUESTION or MB_YESNO or MB_TOPMOST or MB_TASKMODAL) = mrYes) then
  begin
    Cerrar := True;
    Close;
  end;
end;

procedure TFrmLogin.BtnAceptarClick(Sender: TObject);
begin
  if (Length(Trim(TxtLogin.Text)) = 0) then
  begin
    MessageBox(Self.Handle,PChar('Debe Indicar el Login del Usuario!!!'),PChar(MsgTituloError),MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    TxtLogin.SetFocus;
  end
  else
  begin
    if (Length(Trim(TxtPassword.Text)) = 0) then
    begin
      MessageBox(Self.Handle,PChar('Debe Indicar la Contrase�a del Usuario!!!'),PChar(MsgTituloError),MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      TxtPassword.SetFocus;
    end
    else
    begin
      ProcesarInicio;
    end;
  end;
end;

procedure TFrmLogin.ProcesarInicio;
var
  CharError: Char;
  StrError: String;
begin
  CharError := #0;
  if (ValidarUsuario(CharError)) then
  begin
    //Mensaje de Bienvenida
    MessageBox(Self.Handle,PChar('Bienvenido, ' + User.GetNombres + ' ' + User.GetApellidos +#13+#10+''+#13+#10+'Presione OK para continuar la Carga del Sistema.'),PChar(MsgTituloInformacion),MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    //Asignando Datos de Inicio de Sesion
    Cerrar := True;
    ModalResult := mrOk;
    LoginUsuario := User.GetLoginUsuario;
  end
  else
  begin
    Intentos := Intentos - 1;
    if (Intentos > 0) then
    begin
      case CharError of
        NoExiste: StrError := 'El Login indicado NO Existe en la Base de Datos.';
        ClaveInvalida: StrError := 'Clave o Contrase�a Inv�lida';
        CuentaExpirada : StrError := 'La Cuenta ha Expirado!!! Consulte con el Administrador del Sistema.';
      end;
      StrError := StrError + #13 + #10 + #13 + #10 + 'Le Quedan ' + IntToStr(Intentos) + ' Intentos...';
      MessageBox(Self.Handle,PChar(StrError),PChar(MsgTituloError),MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      TxtLogin.Text := '';
      TxtPassword.Text := '';
      TxtLogin.SetFocus;
    end
    else
    begin
      MessageBox(Self.Handle,PChar('Por su Seguridad el Sistema ser� Cerrado. Recuerde sus datos y vuelva a intentarlo m�s tarde.'),PChar(MsgTituloError),MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      ModalResult := mrCancel;
      Cerrar := True;
      Close;
    end;
  end;
end;

function TFrmLogin.ValidarUsuario(var CharError: Char): Boolean;
begin
  User.SetLoginUsuario(TxtLogin.Text);
  if (User.Buscar) then
  begin
    if (User.GetPasswordUsuario = MD5String(TxtPassword.Text)) then
    begin
      CharError := #0;
      Result := True;
      //Falta Verificar Cuenta Expirada
    end
    else
    begin
      CharError := ClaveInvalida;
      Result := False;
    end;
  end
  else
  begin
    CharError := NoExiste;
    Result := False;
  end;
end;

end.
