unit AgregarDetalleNotaCreditoProveedor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GenericoAgregarDetalle, Mask, rxToolEdit, rxCurrEdit, StdCtrls,
  Buttons, ExtCtrls, ClsProducto, ClsDetNotaCreditoProveedor, DB,
  GenericoBuscador, DBClient, Utils, AgregarDetalleFacturacionProveedor;

type
  TFrmAgregarDetalleNotaCreditoProveedor = class(TFrmGenericoAgregarDetalle)
    TxtDescripcionProducto: TEdit;
    TxtCantidad: TCurrencyEdit;
    LblCantidad: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure BtnBuscarClick(Sender: TObject);
    procedure TxtCantidadKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    Producto: TProducto;
  protected
    function Buscar: Boolean; override;
    procedure Agregar; override;
    procedure Limpiar; override;
  public
    { Public declarations }
  end;

var
  FrmAgregarDetalleNotaCreditoProveedor: TFrmAgregarDetalleNotaCreditoProveedor;

implementation

{$R *.dfm}

{ TFrmAgregarDetalleNotaCreditoProveedor }

procedure TFrmAgregarDetalleNotaCreditoProveedor.Agregar;
begin
  with Data do
  begin
    //Verificando si existe el codigo en el Grid
    if (Locate('codigo_producto',TxtCodigo.Text,[loCaseInsensitive])) then
    begin
      Edit;
      FieldValues['cantidad'] := FieldValues['cantidad'] + TxtCantidad.Value;
    end
    else
    begin
      Append;
      FieldValues['codigo_producto'] := Producto.GetCodigo;
      FieldValues['referencia'] := Producto.GetReferencia;
      FieldValues['descripcion'] := Producto.GetDescripcion;
      FieldValues['cantidad'] := TxtCantidad.Value;
      FieldValues['precio'] := Producto.GetPrecio;
      FieldValues['p_descuento'] := Producto.GetPDescuento;
      FieldValues['monto_descuento'] := (Producto.GetPrecio * TxtCantidad.Value * Producto.GetPDescuento) / 100;
      FieldValues['impuesto'] := Producto.GetPrecio - (Producto.GetPrecio / (1 + (Producto.GetPIvaVenta / 100)));
      FieldValues['p_iva'] := Producto.GetPIvaVenta;
      FieldValues['total'] := Producto.GetPrecio * TxtCantidad.Value;
      FieldValues['id_nota_credito_proveedor'] := -1;
    end;
    Post;
  end;
  inherited;
end;

procedure TFrmAgregarDetalleNotaCreditoProveedor.BtnBuscarClick(
  Sender: TObject);
begin
  inherited;
  Buscador(Self,Producto,'codigo','Buscar Producto',TxtCodigo,nil);
  if (Length(Trim(TxtCodigo.Text)) > 0) then
  begin
    ComponenteFoco.SetFocus;
  end;
end;

function TFrmAgregarDetalleNotaCreditoProveedor.Buscar: Boolean;
begin
  Producto.SetCodigo(TxtCodigo.Text);
  Result := Producto.BuscarCodigo;
  if (Result) then
  begin
    TxtDescripcionProducto.Text := Producto.GetDescripcion;
  end
  else
  begin
    MessageBox(Self.Handle, Cadena('El Codigo No Existe!!!'), PChar(MsgTituloError), MB_ICONSTOP or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Limpiar;
  end;
end;

procedure TFrmAgregarDetalleNotaCreditoProveedor.FormCreate(Sender: TObject);
begin
  inherited;
  ComponenteFoco := (TxtCantidad as TWinControl);
  Producto := TProducto.Create;
end;

procedure TFrmAgregarDetalleNotaCreditoProveedor.Limpiar;
begin
  inherited;
  TxtDescripcionProducto.Text := '';
  TxtCantidad.Value := 1;
end;

procedure TFrmAgregarDetalleNotaCreditoProveedor.TxtCantidadKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  case Key of
    VK_RETURN:
      Agregar;    
  end;
end;

end.
