inherited FrmCliente: TFrmCliente
  Caption = 'Sophie - Clientes'
  ClientHeight = 509
  ClientWidth = 613
  ExplicitWidth = 619
  ExplicitHeight = 537
  PixelsPerInch = 96
  TextHeight = 13
  inherited BH: TToolBar
    Width = 613
    ExplicitLeft = 0
    ExplicitTop = 0
    ExplicitWidth = 613
  end
  inherited Pag: TPageControl
    Width = 613
    Height = 473
    ActivePage = TabDatos
    ExplicitWidth = 613
    ExplicitHeight = 473
    inherited TabBusqueda: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 24
      ExplicitWidth = 605
      ExplicitHeight = 445
      inherited PanelBusqueda: TPanel
        Width = 605
        ExplicitWidth = 605
      end
      inherited GrdBusqueda: TDBGrid
        Width = 605
        Height = 407
      end
    end
    inherited TabDatos: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 24
      ExplicitWidth = 605
      ExplicitHeight = 445
      object LblCodigo: TLabel
        Left = 37
        Top = 19
        Width = 41
        Height = 13
        Alignment = taRightJustify
        Caption = 'C'#243'digo:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblRif: TLabel
        Left = 5
        Top = 43
        Width = 73
        Height = 13
        Alignment = taRightJustify
        Caption = 'C'#233'dula o RIF:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblRazonSocial: TLabel
        Left = 4
        Top = 92
        Width = 74
        Height = 13
        Alignment = taRightJustify
        Caption = 'Raz'#243'n Social:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblTelefonos: TLabel
        Left = 20
        Top = 116
        Width = 58
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tel'#233'fonos:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblFechaNacimiento: TLabel
        Left = 149
        Top = 67
        Width = 119
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fecha de Nacimiento:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblEmail: TLabel
        Left = 45
        Top = 140
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Email:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblNit: TLabel
        Left = 248
        Top = 43
        Width = 22
        Height = 13
        Alignment = taRightJustify
        Caption = 'NIT:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblSexo: TLabel
        Left = 47
        Top = 67
        Width = 31
        Height = 13
        Alignment = taRightJustify
        Caption = 'Sexo:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblDireccionDomicilio: TLabel
        Left = 18
        Top = 368
        Width = 126
        Height = 13
        Alignment = taRightJustify
        Caption = 'Direcci'#243'n de Domicilio:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblDireccionFiscal: TLabel
        Left = 19
        Top = 302
        Width = 89
        Height = 13
        Alignment = taRightJustify
        Caption = 'Direcci'#243'n Fiscal:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblFax: TLabel
        Left = 247
        Top = 116
        Width = 23
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fax:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblWebsite: TLabel
        Left = 247
        Top = 140
        Width = 49
        Height = 13
        Alignment = taRightJustify
        Caption = 'Website:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblFechaIngreso: TLabel
        Left = 17
        Top = 167
        Width = 100
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fecha de Ingreso:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblContacto: TLabel
        Left = 24
        Top = 193
        Width = 54
        Height = 13
        Alignment = taRightJustify
        Caption = 'Contacto:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblVendedor: TLabel
        Left = 21
        Top = 220
        Width = 57
        Height = 13
        Alignment = taRightJustify
        Caption = 'Vendedor:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblEsquemaPago: TLabel
        Left = 2
        Top = 274
        Width = 102
        Height = 13
        Alignment = taRightJustify
        Caption = 'Esquema de Pago:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LblZona: TLabel
        Left = 47
        Top = 247
        Width = 31
        Height = 13
        Alignment = taRightJustify
        Caption = 'Zona:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object TxtVendedor: TDBText
        Left = 264
        Top = 220
        Width = 62
        Height = 13
        AutoSize = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object TxtZona: TDBText
        Left = 264
        Top = 247
        Width = 40
        Height = 13
        AutoSize = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object TxtEsquemaPago: TDBText
        Left = 264
        Top = 274
        Width = 83
        Height = 13
        AutoSize = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object TxtUniqueId: TLabel
        Left = 451
        Top = 19
        Width = 7
        Height = 13
        Alignment = taRightJustify
        Caption = '0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object TxtEdad: TLabel
        Left = 390
        Top = 67
        Width = 5
        Height = 13
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clHotLight
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object TxtCodigo: TEdit
        Left = 84
        Top = 16
        Width = 148
        Height = 21
        MaxLength = 10
        TabOrder = 0
      end
      object CmbPrefijoRif: TComboBox
        Left = 84
        Top = 40
        Width = 38
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        MaxLength = 1
        TabOrder = 1
        Items.Strings = (
          'V'
          'E'
          'J'
          'G')
      end
      object TxtRif: TEdit
        Left = 124
        Top = 40
        Width = 108
        Height = 21
        MaxLength = 20
        TabOrder = 2
      end
      object TxtRazonSocial: TEdit
        Left = 84
        Top = 89
        Width = 421
        Height = 21
        MaxLength = 80
        TabOrder = 6
      end
      object TxtTelefonos: TEdit
        Left = 84
        Top = 113
        Width = 148
        Height = 21
        MaxLength = 50
        TabOrder = 7
      end
      object DatFechaNacimiento: TDateEdit
        Left = 276
        Top = 64
        Width = 108
        Height = 21
        DefaultToday = True
        DialogTitle = 'Seleccione Fecha'
        NumGlyphs = 2
        YearDigits = dyFour
        TabOrder = 5
        Text = '15/11/2011'
        OnChange = DatFechaNacimientoChange
      end
      object TxtEmail: TEdit
        Left = 84
        Top = 137
        Width = 148
        Height = 21
        MaxLength = 50
        TabOrder = 9
      end
      object TxtNit: TEdit
        Left = 276
        Top = 40
        Width = 108
        Height = 21
        MaxLength = 20
        TabOrder = 3
      end
      object CmbSexo: TComboBox
        Left = 84
        Top = 64
        Width = 48
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        MaxLength = 1
        TabOrder = 4
        Items.Strings = (
          'M'
          'F')
      end
      object TxtDireccionDomicilio2: TEdit
        Left = 16
        Top = 408
        Width = 490
        Height = 21
        MaxLength = 80
        TabOrder = 22
      end
      object TxtDireccionDomicilio1: TEdit
        Left = 16
        Top = 384
        Width = 490
        Height = 21
        MaxLength = 80
        TabOrder = 21
      end
      object TxtDireccionFiscal1: TEdit
        Left = 16
        Top = 318
        Width = 489
        Height = 21
        MaxLength = 80
        TabOrder = 19
      end
      object TxtDireccionFiscal2: TEdit
        Left = 16
        Top = 342
        Width = 489
        Height = 21
        MaxLength = 80
        TabOrder = 20
      end
      object TxtFax: TEdit
        Left = 276
        Top = 113
        Width = 171
        Height = 21
        MaxLength = 40
        TabOrder = 8
      end
      object TxtWebsite: TEdit
        Left = 302
        Top = 137
        Width = 148
        Height = 21
        MaxLength = 50
        TabOrder = 10
      end
      object DatFechaIngreso: TDateEdit
        Left = 124
        Top = 164
        Width = 108
        Height = 21
        DefaultToday = True
        DialogTitle = 'Seleccione Fecha'
        NumGlyphs = 2
        YearDigits = dyFour
        TabOrder = 11
        Text = '15/11/2011'
      end
      object TxtContacto: TEdit
        Left = 84
        Top = 190
        Width = 196
        Height = 21
        MaxLength = 40
        TabOrder = 12
      end
      object CmbVendedor: TRxDBLookupCombo
        Left = 84
        Top = 217
        Width = 148
        Height = 21
        DropDownCount = 20
        DropDownWidth = 400
        TabOrder = 13
      end
      object CmbZona: TRxDBLookupCombo
        Left = 84
        Top = 244
        Width = 148
        Height = 21
        DropDownCount = 20
        DropDownWidth = 400
        TabOrder = 15
      end
      object CmbEsquemaPago: TRxDBLookupCombo
        Left = 108
        Top = 271
        Width = 124
        Height = 21
        DropDownCount = 20
        DropDownWidth = 400
        TabOrder = 17
      end
      object BtnBuscarZona: TBitBtn
        Left = 235
        Top = 244
        Width = 23
        Height = 22
        TabOrder = 16
        OnClick = BtnBuscarZonaClick
        Glyph.Data = {
          36060000424D3606000000000000360400002800000020000000100000000100
          08000000000000020000850E0000850E00000001000000000000000000003300
          00006600000099000000CC000000FF0000000033000033330000663300009933
          0000CC330000FF33000000660000336600006666000099660000CC660000FF66
          000000990000339900006699000099990000CC990000FF99000000CC000033CC
          000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
          0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
          330000333300333333006633330099333300CC333300FF333300006633003366
          33006666330099663300CC663300FF6633000099330033993300669933009999
          3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
          330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
          66006600660099006600CC006600FF0066000033660033336600663366009933
          6600CC336600FF33660000666600336666006666660099666600CC666600FF66
          660000996600339966006699660099996600CC996600FF99660000CC660033CC
          660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
          6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
          990000339900333399006633990099339900CC339900FF339900006699003366
          99006666990099669900CC669900FF6699000099990033999900669999009999
          9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
          990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
          CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
          CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
          CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
          CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
          CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
          FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
          FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
          FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
          FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
          000000808000800000008000800080800000C0C0C00080808000191919004C4C
          4C00B2B2B200E5E5E5005A1E1E00783C3C0096646400C8969600FFC8C800465F
          82005591B9006EB9D7008CD2E600B4E6F000D8E9EC0099A8AC00646F7100E2EF
          F100C56A31000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000EEEEEEEEEEEE
          EEEE0909EE0909EE0909EEEEEEEEEEEEEEEE8181EE8181EE8181EEEEEEEEEEEE
          EEEE1009EE1009EE1009EEEEEEEEEEEEEEEEE281EEE281EEE281EEEEEEEEEEEE
          EEB4D8EEEEEEEEEEEEEEEEEEEEEEEEEEEEE281EEEEEEEEEEEEEEEEEEEEEEEEEE
          B46C6CD8EEEEEEEEEEEEEEEEEEEEEEEEE2818181EEEEEEEEEEEEEEEEEEEEEEEE
          B46CD86CD8EEEEEEEEEEEEEEEEEEEEEEE281818181EEEEEEEEEEEEEEEEEEEEEE
          B46C6CD86CD8EEEEEEEEEEEEEEEEEEEEE28181818181EEEEEEEEEEEEEEEEEED7
          5E6C6C6CB46CD8EEEEEEEEEEEEEEEED781818181E28181EEEEEEEEEEEEEEEED7
          89896CB4B4B46CD8EEEEEEEEEEEEEED7ACAC81E2E2E28181EEEEEEEEEEEED789
          89D7D7B4C7C7C76CEEEEEEEEEEEED7ACACD7D7E2ACACAC81EEEEEEEEEED78989
          D7D7D7D76C6C6CEEEEEEEEEEEED7ACACD7D7D7D7818181EEEEEEEEEED78989D7
          D7D75E5EEEEEEEEEEEEEEEEED7ACACD7D7D78181EEEEEEEEEEEEEED78989D7D7
          D75EEEEEEEEEEEEEEEEEEED7ACACD7D7D781EEEEEEEEEEEEEEEED78989D7D7D7
          5EB4EEEEEEEEEEEEEEEED7ACACD7D7D781E2EEEEEEEEEEEEEEEE5E89D7D7D75E
          B4EEEEEEEEEEEEEEEEEE81ACD7D7D781E2EEEEEEEEEEEEEEEEEEEE5ED7D75EEE
          EEEEEEEEEEEEEEEEEEEEEE81D7D781EEEEEEEEEEEEEEEEEEEEEEEEEE5E5EEEEE
          EEEEEEEEEEEEEEEEEEEEEEEE8181EEEEEEEEEEEEEEEEEEEEEEEE}
        NumGlyphs = 2
      end
      object BtnBuscarVendedor: TBitBtn
        Left = 235
        Top = 217
        Width = 23
        Height = 22
        TabOrder = 14
        OnClick = BtnBuscarVendedorClick
        Glyph.Data = {
          36060000424D3606000000000000360400002800000020000000100000000100
          08000000000000020000850E0000850E00000001000000000000000000003300
          00006600000099000000CC000000FF0000000033000033330000663300009933
          0000CC330000FF33000000660000336600006666000099660000CC660000FF66
          000000990000339900006699000099990000CC990000FF99000000CC000033CC
          000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
          0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
          330000333300333333006633330099333300CC333300FF333300006633003366
          33006666330099663300CC663300FF6633000099330033993300669933009999
          3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
          330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
          66006600660099006600CC006600FF0066000033660033336600663366009933
          6600CC336600FF33660000666600336666006666660099666600CC666600FF66
          660000996600339966006699660099996600CC996600FF99660000CC660033CC
          660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
          6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
          990000339900333399006633990099339900CC339900FF339900006699003366
          99006666990099669900CC669900FF6699000099990033999900669999009999
          9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
          990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
          CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
          CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
          CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
          CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
          CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
          FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
          FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
          FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
          FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
          000000808000800000008000800080800000C0C0C00080808000191919004C4C
          4C00B2B2B200E5E5E5005A1E1E00783C3C0096646400C8969600FFC8C800465F
          82005591B9006EB9D7008CD2E600B4E6F000D8E9EC0099A8AC00646F7100E2EF
          F100C56A31000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000EEEEEEEEEEEE
          EEEE0909EE0909EE0909EEEEEEEEEEEEEEEE8181EE8181EE8181EEEEEEEEEEEE
          EEEE1009EE1009EE1009EEEEEEEEEEEEEEEEE281EEE281EEE281EEEEEEEEEEEE
          EEB4D8EEEEEEEEEEEEEEEEEEEEEEEEEEEEE281EEEEEEEEEEEEEEEEEEEEEEEEEE
          B46C6CD8EEEEEEEEEEEEEEEEEEEEEEEEE2818181EEEEEEEEEEEEEEEEEEEEEEEE
          B46CD86CD8EEEEEEEEEEEEEEEEEEEEEEE281818181EEEEEEEEEEEEEEEEEEEEEE
          B46C6CD86CD8EEEEEEEEEEEEEEEEEEEEE28181818181EEEEEEEEEEEEEEEEEED7
          5E6C6C6CB46CD8EEEEEEEEEEEEEEEED781818181E28181EEEEEEEEEEEEEEEED7
          89896CB4B4B46CD8EEEEEEEEEEEEEED7ACAC81E2E2E28181EEEEEEEEEEEED789
          89D7D7B4C7C7C76CEEEEEEEEEEEED7ACACD7D7E2ACACAC81EEEEEEEEEED78989
          D7D7D7D76C6C6CEEEEEEEEEEEED7ACACD7D7D7D7818181EEEEEEEEEED78989D7
          D7D75E5EEEEEEEEEEEEEEEEED7ACACD7D7D78181EEEEEEEEEEEEEED78989D7D7
          D75EEEEEEEEEEEEEEEEEEED7ACACD7D7D781EEEEEEEEEEEEEEEED78989D7D7D7
          5EB4EEEEEEEEEEEEEEEED7ACACD7D7D781E2EEEEEEEEEEEEEEEE5E89D7D7D75E
          B4EEEEEEEEEEEEEEEEEE81ACD7D7D781E2EEEEEEEEEEEEEEEEEEEE5ED7D75EEE
          EEEEEEEEEEEEEEEEEEEEEE81D7D781EEEEEEEEEEEEEEEEEEEEEEEEEE5E5EEEEE
          EEEEEEEEEEEEEEEEEEEEEEEE8181EEEEEEEEEEEEEEEEEEEEEEEE}
        NumGlyphs = 2
      end
      object BtnEsquemaPago: TBitBtn
        Left = 235
        Top = 271
        Width = 23
        Height = 22
        TabOrder = 18
        OnClick = BtnEsquemaPagoClick
        Glyph.Data = {
          36060000424D3606000000000000360400002800000020000000100000000100
          08000000000000020000850E0000850E00000001000000000000000000003300
          00006600000099000000CC000000FF0000000033000033330000663300009933
          0000CC330000FF33000000660000336600006666000099660000CC660000FF66
          000000990000339900006699000099990000CC990000FF99000000CC000033CC
          000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
          0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
          330000333300333333006633330099333300CC333300FF333300006633003366
          33006666330099663300CC663300FF6633000099330033993300669933009999
          3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
          330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
          66006600660099006600CC006600FF0066000033660033336600663366009933
          6600CC336600FF33660000666600336666006666660099666600CC666600FF66
          660000996600339966006699660099996600CC996600FF99660000CC660033CC
          660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
          6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
          990000339900333399006633990099339900CC339900FF339900006699003366
          99006666990099669900CC669900FF6699000099990033999900669999009999
          9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
          990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
          CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
          CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
          CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
          CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
          CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
          FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
          FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
          FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
          FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
          000000808000800000008000800080800000C0C0C00080808000191919004C4C
          4C00B2B2B200E5E5E5005A1E1E00783C3C0096646400C8969600FFC8C800465F
          82005591B9006EB9D7008CD2E600B4E6F000D8E9EC0099A8AC00646F7100E2EF
          F100C56A31000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000EEEEEEEEEEEE
          EEEE0909EE0909EE0909EEEEEEEEEEEEEEEE8181EE8181EE8181EEEEEEEEEEEE
          EEEE1009EE1009EE1009EEEEEEEEEEEEEEEEE281EEE281EEE281EEEEEEEEEEEE
          EEB4D8EEEEEEEEEEEEEEEEEEEEEEEEEEEEE281EEEEEEEEEEEEEEEEEEEEEEEEEE
          B46C6CD8EEEEEEEEEEEEEEEEEEEEEEEEE2818181EEEEEEEEEEEEEEEEEEEEEEEE
          B46CD86CD8EEEEEEEEEEEEEEEEEEEEEEE281818181EEEEEEEEEEEEEEEEEEEEEE
          B46C6CD86CD8EEEEEEEEEEEEEEEEEEEEE28181818181EEEEEEEEEEEEEEEEEED7
          5E6C6C6CB46CD8EEEEEEEEEEEEEEEED781818181E28181EEEEEEEEEEEEEEEED7
          89896CB4B4B46CD8EEEEEEEEEEEEEED7ACAC81E2E2E28181EEEEEEEEEEEED789
          89D7D7B4C7C7C76CEEEEEEEEEEEED7ACACD7D7E2ACACAC81EEEEEEEEEED78989
          D7D7D7D76C6C6CEEEEEEEEEEEED7ACACD7D7D7D7818181EEEEEEEEEED78989D7
          D7D75E5EEEEEEEEEEEEEEEEED7ACACD7D7D78181EEEEEEEEEEEEEED78989D7D7
          D75EEEEEEEEEEEEEEEEEEED7ACACD7D7D781EEEEEEEEEEEEEEEED78989D7D7D7
          5EB4EEEEEEEEEEEEEEEED7ACACD7D7D781E2EEEEEEEEEEEEEEEE5E89D7D7D75E
          B4EEEEEEEEEEEEEEEEEE81ACD7D7D781E2EEEEEEEEEEEEEEEEEEEE5ED7D75EEE
          EEEEEEEEEEEEEEEEEEEEEE81D7D781EEEEEEEEEEEEEEEEEEEEEEEEEE5E5EEEEE
          EEEEEEEEEEEEEEEEEEEEEEEE8181EEEEEEEEEEEEEEEEEEEEEEEE}
        NumGlyphs = 2
      end
    end
  end
  object DSVendedor: TDataSource
    Left = 120
    Top = 360
  end
  object DSZona: TDataSource
    Left = 176
    Top = 360
  end
  object DSEsquemaPago: TDataSource
    Left = 240
    Top = 360
  end
end
