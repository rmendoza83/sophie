unit MovimientoInventario;

{$I sophie.inc}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GenericoDetalle, DB, ImgList, XPMan, ComCtrls, Grids, DBGrids,
  ExtCtrls, ToolWin, StdCtrls, Mask, rxToolEdit, Buttons, RxLookup, DBClient,
  Utils, ClsMovimientoInventario, ClsEstacion, ClsZona,
  ClsDetMovimientoInventario, ClsProducto, ClsBusquedaMovimientoInventario,
  GenericoBuscador, ClsReporte, ClsParametro{$IFDEF DELPHI10}, ImageList{$ENDIF};

type
  TFrmMovimientoInventario = class(TFrmGenericoDetalle)
    LblNumero: TLabel;
    TxtNumero: TEdit;
    LblCodigoEstacionEntrada: TLabel;
    CmbCodigoEstacionEntrada: TRxDBLookupCombo;
    BtnBuscarEstacionEntrada: TBitBtn;
    LblCodigoZona: TLabel;
    CmbCodigoZona: TRxDBLookupCombo;
    BtnBuscarZona: TBitBtn;
    LblFechaIngreso: TLabel;
    DatFechaIngreso: TDateEdit;
    LblRecepcion: TLabel;
    DatFechaRecepcion: TDateEdit;
    LblFechaContable: TLabel;
    DatFechaContable: TDateEdit;
    LblFechaLibros: TLabel;
    DatFechaLibros: TDateEdit;
    LblCodigoEstacionSalida: TLabel;
    CmbCodigoEstacionSalida: TRxDBLookupCombo;
    BtnBuscarEstacionSalida: TBitBtn;
    TabConcepto: TTabSheet;
    TabObservaciones: TTabSheet;
    DSZona: TDataSource;
    DSEstacionEntrada: TDataSource;
    DSEstacionSalida: TDataSource;
    Datacodigo_producto: TStringField;
    Datacantidad: TFloatField;
    Dataprecio: TFloatField;
    Datap_descuento: TFloatField;
    Datamonto_descuento: TFloatField;
    Dataimpuesto: TFloatField;
    Datap_iva: TFloatField;
    Datatotal: TFloatField;
    Dataid_movimiento_inventario: TIntegerField;
    Datadescripcion: TStringField;
    Datareferencia: TStringField;
    TxtConcepto: TRichEdit;
    TxtObservaciones: TRichEdit;
    procedure TBtnEliminarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtnBuscarEstacionEntradaClick(Sender: TObject);
    procedure BtnBuscarEstacionSalidaClick(Sender: TObject);
    procedure BtnBuscarZonaClick(Sender: TObject);
  private
    { Private declarations }
    Clase: TMovimientoInventario;
    DetMovimientoInventario: TDetMovimientoInventario;
    BuscadorMovimientoInventario: TBusquedaMovimientoInventario;
    Estacion: TEstacion;
    Zona: TZona;
    Producto: TProducto;
    Reporte: TReporte;
    Parametro: TParametro;
    DataBuscadorMovimientoInventario: TClientDataSet;
    DataEstacionEntrada: TClientDataSet;
    DataEstacionSalida: TClientDataSet;
    DataZona: TClientDataSet;
    procedure Agregar;
    procedure Editar;
    procedure Eliminar;
    procedure Guardar;
    procedure Cancelar;
    procedure Refrescar;
    procedure Imprimir;
    procedure Buscar;
    procedure Limpiar;
    procedure Asignar;
    procedure Obtener;
    procedure Mover;
    procedure GuardarDetalles;
  protected
    { Protected declarations }
    function Validar: Boolean; override;
    procedure Detalle; override;
  public
    { Public declarations }
  end;

var
  FrmMovimientoInventario: TFrmMovimientoInventario;

implementation

uses
  ActividadSophie,
  PModule,
  ClsGenericoBD,
  AgregarDetalleMovimientoInventario
  {$IFDEF USE_RAVE_REPORTS}
  , DMRR
  {$ENDIF}
  {$IFDEF USE_FAST_REPORTS}
  , DMFR
  {$ENDIF}
  ;

{$R *.dfm}

procedure TFrmMovimientoInventario.Agregar;
begin
  //Verificando
  Limpiar;
  //Asignando Valores por Defecto
  CmbCodigoZona.Value := Parametro.ObtenerValor('codigo_zona');
end;

procedure TFrmMovimientoInventario.Asignar;
var
  DataDetalle: TClientDataSet;
  Campo: string;
  i: Integer;
begin
  with Clase do
  begin
    TxtNumero.Text := GetNumero;
    //NumeroPedido
    CmbCodigoZona.KeyValue := GetCodigoZona;
    CmbCodigoEstacionSalida.KeyValue := GetCodigoEstacionSalida;
    CmbCodigoEstacionEntrada.KeyValue := GetCodigoEstacionEntrada;
    DatFechaIngreso.Date := GetFechaIngreso;
    DatFechaRecepcion.Date := GetFechaRecepcion;
    DatFechaContable.Date := GetFechaContable;
    DatFechaLibros.Date := GetFechaLibros;
    TxtConcepto.Text := GetConcepto;
    TxtObservaciones.Text := GetObservaciones;
    //Data.CloneCursor(DetMovimientoInventario.ObtenerIdMovimientoInventario(GetIdMovimientoInventario),False,False);
    //DS.DataSet := Data;
    DataDetalle := TClientDataSet.Create(Self);
    DataDetalle.CloneCursor(DetMovimientoInventario.ObtenerIdMovimientoInventario(GetIdMovimientoInventario),False,True);
    Data.EmptyDataSet;
    DataDetalle.First;
    while (not DataDetalle.Eof) do
    begin
      Data.Append;
      //Asignando Campos Similares
      for i := 0 to DataDetalle.FieldCount - 1 do
      begin
        Campo := DataDetalle.FieldDefs[i].Name;
        if (Data.FieldDefs.IndexOf(Campo) <> -1) then
        begin
          Data.FieldValues[Campo] := DataDetalle.FieldValues[Campo];
        end;        
      end;
      //Se Ubica Producto
      Producto.SetCodigo(DataDetalle.FieldValues['codigo_producto']);
      Producto.BuscarCodigo;
      Data.FieldValues['referencia'] := Producto.GetReferencia;
      Data.FieldValues['descripcion'] := Producto.GetDescripcion;
      Data.Post;
      DataDetalle.Next;
    end;
    DS.DataSet := Data;
    DataDetalle.Free;
  end;
end;

procedure TFrmMovimientoInventario.BtnBuscarEstacionEntradaClick(
  Sender: TObject);
begin
  Buscador(Self,Estacion,'codigo','Buscar Estacion',nil,DataEstacionEntrada);
  CmbCodigoEstacionEntrada.KeyValue := DataEstacionEntrada.FieldValues['codigo'];
end;

procedure TFrmMovimientoInventario.BtnBuscarEstacionSalidaClick(
  Sender: TObject);
begin
  Buscador(Self,Estacion,'codigo','Buscar Estacion',nil,DataEstacionSalida);
  CmbCodigoEstacionSalida.KeyValue := DataEstacionSalida.FieldValues['codigo'];
end;

procedure TFrmMovimientoInventario.BtnBuscarZonaClick(Sender: TObject);
begin
  Buscador(Self,Zona,'codigo','Buscar Zona',nil,DataZona);
  CmbCodigoZona.KeyValue := DataZona.FieldValues['codigo'];
end;

procedure TFrmMovimientoInventario.Buscar;
begin
  if (Buscador(Self,BuscadorMovimientoInventario,'numero','Buscar Movimiento',nil,DataBuscadorMovimientoInventario)) then
  begin
    Clase.SetNumero(DataBuscadorMovimientoInventario.FieldValues['numero']);
    Clase.BuscarNumero;
    Asignar;
  end;
end;

procedure TFrmMovimientoInventario.Cancelar;
begin
  if (Agregando) then
  begin
    Limpiar;
  end
  else
  begin
    Clase.SetNumero(TxtNumero.Text);
    Clase.BuscarNumero;
    Asignar;
  end;
end;

procedure TFrmMovimientoInventario.Editar;
begin
  if (Length(Trim(TxtNumero.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
end;

procedure TFrmMovimientoInventario.Eliminar;
var
  sw: Boolean;
begin
  sw := True;
  if (sw) then
  begin
    //Se Busca La Factura!
    Clase.SetNumero(TxtNumero.Text);
    Clase.BuscarNumero;
    //Se Eliminan Los Detalles
    DetMovimientoInventario.EliminarIdMovimientoInventario(Clase.GetIdMovimientoInventario);
    Clase.Eliminar;
    MessageBox(Self.Handle, 'El Movimiento de Inventario Se Ha Eliminado Con Exito!!!',PChar(MsgTituloInformacion), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Refrescar;
    Limpiar;
  end
  else
  begin
    MessageBox(Self.Handle, 'El Movimiento de Inventario No Puede Ser Eliminado!!!',PChar(MsgTituloAdvertencia), MB_ICONEXCLAMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end;
end;

procedure TFrmMovimientoInventario.FormCreate(Sender: TObject);
begin
  inherited;
  ProcAgregar := Agregar;
  ProcEditar := Editar;
  ProcEliminar := Eliminar;
  ProcGuardar := Guardar;
  ProcCancelar := Cancelar;
  ProcRefrescar := Refrescar;
  ProcImprimir := Imprimir;
  ProcBuscar := Buscar;
  FrmActividadSophie.Show;
  FrmActividadSophie.Actividad('Cargando N�cleo...');
  Clase := TMovimientoInventario.Create;
  //Inicializando DataSets Secundarios
  BuscadorMovimientoInventario := TBusquedaMovimientoInventario.Create;
  Estacion := TEstacion.Create;
  Zona := TZona.Create;
  Producto := TProducto.Create;
  Reporte := TReporte.Create;
  Parametro := TParametro.Create;
  DetMovimientoInventario := TDetMovimientoInventario.Create;
  FrmActividadSophie.Actividad('Cargando Estaciones...');
  DataEstacionSalida := Estacion.ObtenerCombo;
  DataEstacionEntrada := Estacion.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Zonas...');
  DataZona := Zona.ObtenerCombo;
  FrmActividadSophie.Actividad('Cargando Resto del N�cleo...');
  DataBuscadorMovimientoInventario := BuscadorMovimientoInventario.ObtenerLista;
  DataEstacionSalida.First;
  DataEstacionEntrada.First;
  DataZona.First;
  DataBuscadorMovimientoInventario.First;
  DSEstacionSalida.DataSet := DataEstacionSalida;
  DSEstacionEntrada.DataSet := DataEstacionEntrada;
  DSZona.DataSet := DataZona;
  ConfigurarRxDBLookupCombo(CmbCodigoEstacionSalida,DSEstacionSalida,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoEstacionEntrada,DSEstacionEntrada,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbCodigoZona,DSZona,'codigo','codigo;descripcion');
  DS.DataSet := Data;
  GridDetalle.DataSource := DS;
  FrmActividadSophie.Close;
  //Buscar;
end;

procedure TFrmMovimientoInventario.Guardar;
begin
  Obtener;
  if (Agregando) then
  begin
    //Agregando...
    //Guardando Encabezado
    Clase.Insertar;
    TxtNumero.Text := Clase.GetNumero;
    //Guardando Detalles
    GuardarDetalles;
    MessageBox(Self.Handle, 'El Movimiento de Inventario Ha Sido Agregado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    //Se Imprime el Movimiento
    MessageBox(Self.Handle, 'Presione "Aceptar" Para Proceder A Imprimir El Movimiento de Inventario!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    //Se Imprime En Formato Estandar del Sistema
    with Reporte do
    begin
      AgregarParametro('NumeroMovimiento',Clase.GetNumero);
      ImprimeReporte('MovimientoInventario',trGeneral);
    end;
  end
  else
  begin
    //Editando...
    Clase.Modificar;
    //Actualizando Detalles (Eliminando Anteriores y Cargando los Nuevos)
    DetMovimientoInventario.EliminarIdMovimientoInventario(Clase.GetIdMovimientoInventario);
    //Guardando Detalles
    GuardarDetalles;
    MessageBox(Self.Handle, 'El Movimiento de Inventario Ha Sido Editado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  end;
end;

procedure TFrmMovimientoInventario.GuardarDetalles;
begin
  with Data do
  begin
    DisableControls;
    First;
    while (not Eof) do
    begin
      Producto.SetCodigo(FieldValues['codigo_producto']);
      Producto.BuscarCodigo;
      if (Producto.GetInventario) then
      begin
        with DetMovimientoInventario do
        begin
          SetDesde('MI');
          SetNumeroDocumento(Clase.GetNumero);
          SetCodigoProducto(Producto.GetCodigo);
          SetCantidad(FieldValues['cantidad']);
          SetEstacionSalida(Clase.GetCodigoEstacionSalida);
          SetEstacionEntrada(Clase.GetCodigoEstacionEntrada);
          SetCodigoZona(Clase.GetCodigoZona);
          SetPrecio(FieldValues['precio']);
          SetPDescuento(FieldValues['p_descuento']);
          SetMontoDescuento(FieldValues['monto_descuento']);
          SetImpuesto(FieldValues['impuesto']);
          SetPIva(FieldValues['p_iva']);
          SetTotal(FieldValues['total']);
          SetCosto(Producto.GetCostoActual);
          SetUsCosto(Producto.GetUsCostoActual);
          SetCostoFob(Producto.GetCostoFob);
          SetFecha(Clase.GetFechaIngreso);
          SetCodigoCliente('');
          SetCodigoProveedor('');
          SetIdMovimientoInventario(Clase.GetIdMovimientoInventario);
          Insertar;
        end;
      end;
(*      with DetMovimientoInventario do
      begin
        SetCodigoProducto(FieldValues['codigo_producto']);
        SetReferencia(FieldValues['referencia']);
        SetDescripcion(FieldValues['descripcion']);
        SetCantidad(FieldValues['cantidad']);
        SetPrecio(FieldValues['precio']);
        SetPDescuento(FieldValues['p_descuento']);
        SetMontoDescuento(FieldValues['monto_descuento']);
        SetImpuesto(FieldValues['impuesto']);
        SetPIva(FieldValues['p_iva']);
        SetTotal(FieldValues['total']);
        SetIdFacturacionCliente(Clase.GetIdFacturacionCliente);
        Insertar;
      end;*)
      Next;
    end;
    EnableControls;
  end;
end;

procedure TFrmMovimientoInventario.Imprimir;
begin
  if (Length(Trim(TxtNumero.Text)) = 0) then
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
  //Se Imprime En Formato Estandar del Sistema
  with Reporte do
  begin
    AgregarParametro('NumeroMovimiento',Clase.GetNumero);
    EmiteReporte('MovimientoInventario',trGeneral);
  end;
end;

procedure TFrmMovimientoInventario.Limpiar;
begin
  TxtNumero.Text := '';
  CmbCodigoEstacionSalida.KeyValue := '';
  CmbCodigoEstacionEntrada.KeyValue := '';
  CmbCodigoZona.KeyValue := '';
  DatFechaIngreso.Date := Date;
  DatFechaRecepcion.Date := Date;
  DatFechaContable.Date := Date;
  DatFechaLibros.Date := Date;
  TxtConcepto.Text := '';
  TxtObservaciones.Text := '';
  //Status Bar
  SB.Panels[0].Text := '';
  Data.EmptyDataSet;
  TabDetalle.TabVisible := True;
  PageDetalles.ActivePage := TabDetalle;
end;

procedure TFrmMovimientoInventario.Mover;
begin

end;

procedure TFrmMovimientoInventario.Obtener;
begin
  with Clase do
  begin
    SetNumero(TxtNumero.Text);
    SetCodigoEstacionSalida(CmbCodigoEstacionSalida.KeyValue);
    SetCodigoEstacionEntrada(CmbCodigoEstacionEntrada.KeyValue);
    SetCodigoZona(CmbCodigoZona.KeyValue);
    SetFechaIngreso(DatFechaIngreso.Date);
    SetFechaRecepcion(DatFechaRecepcion.Date);
    SetFechaContable(DatFechaContable.Date);
    SetFechaLibros(DatFechaLibros.Date);
    SetConcepto(TxtConcepto.Text);
    SetObservaciones(TxtObservaciones.Text);
    SetLoginUsuario(P.User.GetLoginUsuario);
    if (Agregando) then
    begin
      SetTiempoIngreso(Now);
      SetIdMovimientoInventario(-1);
    end;
  end;
end;

procedure TFrmMovimientoInventario.Refrescar;
begin
  //Se Actualiza El DataBuscador
  DataBuscadorMovimientoInventario := BuscadorMovimientoInventario.ObtenerLista;
  //Se Actualizan Los demas Componentes
  DataEstacionSalida := Estacion.ObtenerCombo;
  DataEstacionEntrada := Estacion.ObtenerCombo;
  DataZona := Zona.ObtenerCombo;
  DataEstacionSalida.First;
  DataEstacionEntrada.First;
  DataZona.First;
  DataBuscadorMovimientoInventario.First;
  DSEstacionSalida.DataSet := DataEstacionSalida;
  DSEstacionEntrada.DataSet := DataEstacionEntrada;
  DSZona.DataSet := DataZona;
end;

procedure TFrmMovimientoInventario.TBtnEliminarClick(Sender: TObject);
begin
  if (Length(Trim(TxtNumero.Text)) > 0) then
  begin
    inherited;
  end
  else
  begin
    MessageBox(Self.Handle, 'Primero debe realizar una b�squeda!', MsgTituloAdvertencia, MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Abort;
  end;
end;

function TFrmMovimientoInventario.Validar: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  //Validando Estaciones que no sean Iguales
  if (CmbCodigoEstacionEntrada.KeyValue = CmbCodigoEstacionSalida.KeyValue) then
  begin
    ListaErrores.Add('La Estacion de Salida No Puede Ser Igual a la Estacion de Entrada!!!');
  end;
  //Validando Fechas
  if ((DatFechaIngreso.Date > DatFechaRecepcion.Date) or (DatFechaIngreso.Date > DatFechaContable.Date) or (DatFechaIngreso.Date > DatFechaLibros.Date)) then
  begin
    ListaErrores.Add('La Fecha de Ingreso Debe Ser Menor a las Fechas de Recepcion, Contable y Libros Respectivamente!!!');
  end;
  //Validando Detalles
  if (Data.RecordCount = 0) then
  begin
    ListaErrores.Add('El Detalle De La Factura No Puede Estar Vacio!!!');
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

procedure TFrmMovimientoInventario.Detalle;
begin
  inherited;
  FrmAgregarDetalleMovimientoInventario := TFrmAgregarDetalleMovimientoInventario.CrearDetalle(Self,Data);
  FrmAgregarDetalleMovimientoInventario.ShowModal;
  FreeAndNil(FrmAgregarDetalleMovimientoInventario);
end;

end.

