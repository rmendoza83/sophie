inherited FrmTallerAgregarDetalle: TFrmTallerAgregarDetalle
  ClientHeight = 253
  OnCreate = FormCreate
  ExplicitWidth = 350
  ExplicitHeight = 277
  PixelsPerInch = 96
  TextHeight = 13
  inherited LblCodigo: TLabel
    Alignment = taRightJustify
  end
  object LblCantidad: TLabel [1]
    Left = 11
    Top = 64
    Width = 47
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cantidad:'
  end
  object LblFacturable: TLabel [2]
    Left = 3
    Top = 91
    Width = 55
    Height = 13
    Alignment = taRightJustify
    Caption = 'Facturable:'
  end
  object LblObservaciones: TLabel [3]
    Left = 35
    Top = 118
    Width = 23
    Height = 13
    Alignment = taRightJustify
    Caption = 'Obs:'
  end
  inherited PanelBotones: TPanel
    Top = 213
    TabOrder = 6
    ExplicitTop = 213
  end
  inherited BtnBuscar: TBitBtn
    TabOrder = 1
    OnClick = BtnBuscarClick
  end
  object TxtCantidad: TCurrencyEdit
    Left = 64
    Top = 61
    Width = 75
    Height = 21
    Margins.Left = 4
    Margins.Top = 1
    DecimalPlaces = 4
    DisplayFormat = '#,##0.0000'
    TabOrder = 3
    Value = 1.000000000000000000
    OnKeyDown = TxtCantidadKeyDown
  end
  object TxtDescripcionProducto: TEdit
    Left = 64
    Top = 35
    Width = 249
    Height = 21
    Enabled = False
    TabOrder = 2
    OnKeyDown = TxtCodigoKeyDown
  end
  object ChkFacturable: TCheckBox
    Left = 64
    Top = 92
    Width = 19
    Height = 17
    TabOrder = 4
  end
  object TxtObservaciones: TMemo
    Left = 64
    Top = 115
    Width = 249
    Height = 89
    MaxLength = 255
    TabOrder = 5
    OnKeyDown = TxtCantidadKeyDown
  end
end
