unit Utils;

{$I sophie.inc}

interface

uses
  Classes, SysUtils, Dialogs, Graphics, DateUtils, RxLookup, DB, StdCtrls,
  Messages, Forms;

const
  TeclasValidas = 'ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyz0123456789,;:.-_+*=¿?!¡/'+#13+#8; //Determina las Teclas Validas en Campos de Textos
  MsgTituloAdvertencia = 'Sophie - Advertencia';
  MsgTituloInformacion = 'Sophie - Informaci�n';
  MsgTituloError = 'Sophie - Error';

type
  TArrayString = array of string;
  TArrayDouble = array of double;
  TMatrizDouble = array [0..1, 0..1] of double;
  TArrayVariant = array of variant;
  TProcObject = procedure of object;

function FileToArrayString(FileName: string): TArrayString;
function Explode(Separator: Char; S: string): TArrayString;
function Implode(Separator: string; A: TArrayString): string; overload;
function Implode(Separator: string; A: TArrayVariant): string; overload;
function MD5String(const S: string): string;
function MensajeError(SL: TStringList): string;
function FormatearFloat(N: string): Double;
function FormatearInt(N: string): Integer; overload;
function YearOld(ADate,CurrentDate : TDateTime) : Integer;
function Cadena(const S: string): PChar;
procedure ConfigurarRxDBLookupCombo(Combo: TRxDBLookupCombo; DS: TDataSource; FieldKey: string; FieldsDisplay: string);
function NumeroALetras(Numero: Integer): string; overload;
function NumeroALetras(Numero: Double): string; overload;
procedure SetAutoDropDownWidth(Combo: TComboBox);
function CapitalCase(S: string): string;
function NombreCampoBDToNombreCapital(S: string): string;
//Procedimientos y Funciones especiales para el cambio automatico del DisplayFormat
procedure AsignarDisplayFormat(Component: TComponent; NumeroDecimales: Integer);
{$IFDEF USE_RAVE_REPORTS}
procedure AsignarDisplayFormatRaveReports(Component: TComponent; NumeroDecimales: Integer);
{$ENDIF}
procedure BuscarDisplayFormat(Form: TForm; NumeroDecimales: Integer);
function GetIPAddress: Integer;
function GetIPAddressAsString: string;
function GetHostName: string;
function CheckDebug: Boolean;

implementation

uses
  Controls,
  rxCurrEdit,
  TypInfo,
  {$IFDEF USE_RAVE_REPORTS}
  RvDataField,
  RvCsData,
  {$ENDIF}
  IdGlobal,
  IdHashMessageDigest,
  idHash,
  WinSock,
  Windows;

function FileToArrayString(FileName: string): TArrayString;
var
  F: TextFile;
  str: string;
  R: TArrayString;
begin
  AssignFile(F,FileName);
  Reset(F);
  SetLength(R,0);
  while (not EOF(F)) do
  begin
    ReadLn(F,str);
    SetLength(R,Length(R) + 1);
    R[Length(R) - 1] := str;
  end;
  SetLength(R,Length(R) + 1);
  R[Length(R) - 1] := '';
  Close(F);
  Result := R;
end;

function Explode(Separator: Char; S: string): TArrayString;
var
  i: Integer;
  str: string[255];
  R: TArrayString;
begin
  SetLength(R,0);
  str := '';
  for i := 1 to Length(S) do
  begin
    if ((S[i] = Separator) or (i = Length(S))) then
    begin
      if (i = Length(S)) then
      begin
        str := str + S[i];
      end;
      SetLength(R,Length(R) + 1);
      R[Length(R) - 1] := str;
      str := '';
    end
    else
    begin
      str := str + S[i];
    end;
  end;
  Result := R;
end;

function Implode(Separator: string; A: TArrayString): string;
var
  i: Integer;
  str: string;
begin
  str := '';
  for i := Low(A) to High(A) - 1 do
  begin
    str := str + A[i] + Separator;
  end;
  str := str + A[High(A)];
  Result := str;
end;

function Implode(Separator: string; A: TArrayVariant): string;
var
  i: Integer;
  str: string;
begin
  str := '';
  for i := Low(A) to High(A) - 1 do
  begin
    str := str + A[i] + Separator;
  end;
  str := str + A[High(A)];
  Result := str;
end;

function MD5String(const S: string): string;
var
  idMD5: TIdHashMessageDigest5;
begin
  idMD5 := TIdHashMessageDigest5.Create;
  try
    Result := IndyLowerCase(idMD5.HashStringAsHex(S));
  finally
    idMD5.Free;
  end;
end;

function MensajeError(SL: TStringList): String;
var
  i: Integer;
  S: String;
begin
  S := 'Lista de Errores: ' + chr(10) + chr(13) + chr(10) + chr(13);
  for i := 0 to SL.Count - 1 do
  begin
    S := S + '- ' + SL[i] + '.';
    if (i < (SL.Count - 1)) then
    begin
      S := S + chr(10) + chr(13) + chr(10) + chr(13);
    end;
  end;
  Result := S;
end;

function FormatearFloat(N: String): Double;
begin
  {$IFDEF DELPHI2006}
    Result := StrToFloat(StringReplace(N,ThousandSeparator,'',[rfReplaceAll]));
  {$ELSE}
    {$IFDEF DELPHI10}
      Result := StrToFloat(StringReplace(N,FormatSettings.ThousandSeparator,'',[rfReplaceAll]));
    {$ELSE}
      Result := 0;
    {$ENDIF}
  {$ENDIF}
end;

function FormatearInt(N: String): Integer;
begin
  {$IFDEF DELPHI2006}
    Result := StrToInt(StringReplace(N,ThousandSeparator,'',[rfReplaceAll]));
  {$ELSE}
    {$IFDEF DELPHI10}
      Result := StrToInt(StringReplace(N,FormatSettings.ThousandSeparator,'',[rfReplaceAll]));
    {$ELSE}
      Result := 0;
    {$ENDIF}
  {$ENDIF}
end;

function YearOld(ADate,CurrentDate : TDateTime) : Integer;
var
  aYear,aMonth,aDay : Word;
  TempDate : TDateTime;
begin
  aYear := YearOf(CurrentDate);
  aMonth := MonthOf(ADate);
  aDay := DayOf(ADate);

//se trata de convertir a una fecha v�lida
 if TryEncodeDate(aYear,aMonth,aDay,TempDate) then
  begin
//si la fecha actual es <= a la fecha conformada entonces ya cumpli� a�os
    if CurrentDate <= TempDate then
    Result := YearOf(CurrentDate) - YearOf(ADate)
    else Result := YearOf(CurrentDate) - YearOf(ADate) -1;
  end
  else Result := 0; // 0 sino puede convertir una fecha v�lida
end;

function Cadena(const S: string): PChar;
begin
  Result := PChar(S);
end;

procedure ConfigurarRxDBLookupCombo(Combo: TRxDBLookupCombo; DS: TDataSource; FieldKey: string; FieldsDisplay: string);
begin
  with Combo do
  begin
    LookupSource := DS;
    LookupField := FieldKey;
    LookupDisplay := FieldsDisplay;
  end;
end;

function NumeroALetras(Numero: Integer): string;

  function NumeroALetrasExtendido(Numero: Integer): string;
  const
    aUnidad : array[1..15] of string =
      ('UN','DOS','TRES','CUATRO','CINCO','SEIS',
       'SIETE','OCHO','NUEVE','DIEZ','ONCE','DOCE',
       'TRECE','CATORCE','QUINCE');
    aCentena: array[1..9] of String =
      ('CIENTO','DOSCIENTOS','TRESCIENTOS',
       'CUATROCIENTOS','QUINIENTOS','SEISCIENTOS',
       'SETECIENTOS','OCHOCIENTOS','NOVECIENTOS');
    aDecena : array[1..9] of String =
     ('DIECI','VEINTI','TREINTA','CUARENTA','CINCUENTA',
      'SESENTA','SETENTA','OCHENTA','NOVENTA');
  var
    Centena, Decena, Unidad, Doble: Integer;
    Linea: string;
  begin
    if Numero = 100 then
    begin
      Linea := ' CIEN ';
    end
    else
    begin
      Linea := '';
      Centena := Numero div 100;
      Doble := Numero - (Centena * 100);
      Decena := (Numero div 10) - (Centena * 10);
      Unidad := Numero - (Decena * 10) - (Centena * 100);
      if (Centena > 0) then
      begin
        Linea := Linea + aCentena[Centena] + ' ';
      end;
      if (Doble > 0) then
      begin
        if (Doble = 20) then
        begin
          Linea := Linea + ' VEINTE '
        end
        else
        begin
          if (Doble < 16) then
          begin
            Linea := Linea + aUnidad[Doble];
          end
          else
          begin
            Linea := Linea + ' ' + aDecena[Decena];
            if (Decena > 2) and (Unidad <> 0) then
            begin
              Linea := Linea + ' Y ';
            end;
            if (Unidad > 0) then
            begin
              Linea := Linea + aUnidad[Unidad];
            end;
          end;
        end;
      end;
    end;
    Result := Linea;
  end;

var
  Millones, Miles, Unidades: Integer;
  Linea: string;
begin
  {Inicializamos el string que contendr� las letras seg�n el valor num�rico}
  if (Numero = 0) then
  begin
    Linea := 'CERO';
  end
  else
  begin
    if (Numero < 0) then
    begin
      Linea := 'MENOS '
    end
    else
    begin
      if (Numero = 1) then
      begin
        Linea := 'UN';
        Result := Linea;
        Exit
      end
      else
      begin
        if (Numero > 1) then
        begin
          Linea := '';
        end;
      end;
    end;
  end;
  {Determinamos el N� de millones, miles y unidades de numero en positivo}
  Numero := Abs(Numero);
  Millones := Numero div 1000000;
  Miles := (Numero - (Millones * 1000000)) div 1000;
  Unidades := Numero - ((Millones * 1000000) + (Miles * 1000));
  {Vamos poniendo en el string las cadenas de los n�meros(llamando a subfuncion)}
  if (Millones = 1) then
  begin
    Linea := Linea + ' UN MILLON ';
  end
  else
  begin
    if (Millones > 1) then
    begin
      Linea := Linea + NumeroALetrasExtendido(Millones) + ' MILLONES ';
    end;
  end;
  if (Miles = 1) then
  begin
    Linea:= Linea + ' MIL ';
  end
  else
  begin
    if (Miles > 1) then
    begin
      Linea := Linea + NumeroALetrasExtendido(Miles) + ' MIL ';
    end;
  end;
  if (Unidades > 0) then
  begin
    Linea := Linea + NumeroALetrasExtendido(Unidades);
  end;
  Result := Linea;
end;

function NumeroALetras(Numero: Double): string;
begin
  Result := NumeroALetras(Integer(Trunc(Numero))) + ' CON ' + NumeroALetras(Integer(Round(Frac(Numero) * 100))) + ' CENTIMOS';
end;

procedure SetAutoDropDownWidth(Combo: TComboBox);
var
  i, w: Integer;
begin
  with Combo do
  begin
    w := 0;
    for i := 0 to Items.Count - 1 do
    begin
      if (Canvas.TextWidth(Items[i]) > w) then
      begin
        w := Canvas.TextWidth(Items[i]);
      end;
    end;
  end;
  w := (w - (w mod 10)) + 40;
  Combo.Perform(CB_SETDROPPEDWIDTH,w,0);
end;

function CapitalCase(S: string): string;
var
  AuxStr: string;
begin
  AuxStr := Copy(S,2,Length(S));
  Result := UpperCase(S[1]) + LowerCase(AuxStr);
end;

function NombreCampoBDToNombreCapital(S: string): string;
var
  i: Integer;
  ArrayAuxStr: TArrayString;
begin
  ArrayAuxStr := Explode('_',S);
  for i := Low(ArrayAuxStr) to High(ArrayAuxStr) do
  begin
    ArrayAuxStr[i] := CapitalCase(ArrayAuxStr[i]);
  end;
  Result := Implode(' ',ArrayAuxStr);
end;

//Procedimientos y Funciones especiales para el cambio automatico del DisplayFormat
procedure AsignarDisplayFormat(Component: TComponent; NumeroDecimales: Integer);
var
  Formato: string;
begin
  Formato := '#,##0.' + StringOfChar('0',NumeroDecimales);
  if (Component.ClassName = 'TCurrencyEdit') then
  begin
    (Component as TCurrencyEdit).DisplayFormat := Formato;
    (Component as TCurrencyEdit).DecimalPlaces := NumeroDecimales;
  end;
  if (Component.ClassName = 'TFloatField') then
  begin
    (Component as TFloatField).DisplayFormat := Formato;
  end;
end;

{$IFDEF USE_RAVE_REPORTS}
procedure AsignarDisplayFormatRaveReports(Component: TComponent; NumeroDecimales: Integer);
var
  Formato: string;
begin
  Formato := '#,##0.' + StringOfChar('0',NumeroDecimales);
  if (Component.ClassName = 'TRaveFloatField') then
  begin
    (Component as TRaveFloatField).DisplayFormat := Formato;
  end;
  if (Component.ClassName = 'TRaveCalcOp') then
  begin
    (Component as TRaveCalcOp).DisplayFormat := Formato;
  end;
  if (Component.ClassName = 'TRaveCalcTotal') then
  begin
    (Component as TRaveCalcTotal).DisplayFormat := Formato;
  end;
  if (Component.ClassName = 'TRaveCalcText') then
  begin
    (Component as TRaveCalcText).DisplayFormat := Formato;
  end;
end;
{$ENDIF}

procedure BuscarDisplayFormat(Form: TForm; NumeroDecimales: Integer);
var
  i: Integer;
begin
  for i := 0 to Form.ComponentCount - 1 do
  begin
    if (IsPublishedProp(Form.Components[i],'DisplayFormat')) then
    begin
      AsignarDisplayFormat(Form.Components[i],NumeroDecimales);
    end;
  end;
end;

function GetIPAddress: Integer;
var
  Buffer: array[0..255] of AnsiChar;
  RemoteHost: PHostEnt;
begin
  Winsock.GetHostName(@Buffer, 255);
  RemoteHost := Winsock.GetHostByName(Buffer);
  if RemoteHost = nil then
    Result := winsock.htonl($07000001) { 127.0.0.1 }
  else
    Result := longint(pointer(RemoteHost^.h_addr_list^)^);
    Result := Winsock.ntohl(Result);
end;// function GetIPAddress: Integer;

function GetIPAddressAsString: String;
var
  tempAddress: Integer;
  Buffer: array[0..3] of Byte absolute tempAddress;
begin
  tempAddress := GetIPAddress;
  Result := Format('%d.%d.%d.%d', [Buffer[3], Buffer[2], Buffer[1], Buffer[0]]);
end;

function GetHostName: string;
var
  buffer: array[0..MAX_COMPUTERNAME_LENGTH + 1] of Char;
  Size: Cardinal;
begin
  Size := MAX_COMPUTERNAME_LENGTH + 1;
  GetComputerName(@buffer, Size);
  Result := StrPas(buffer);
end;

function CheckDebug: Boolean;
begin
  Result := (DebugHook = 1)
end;

end.
